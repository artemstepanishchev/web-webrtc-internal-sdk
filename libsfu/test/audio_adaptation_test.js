const chai = require('chai');
const expect = chai.expect;
import AudioAdaptation from '../lib/audio_adaptation';

const aa = new AudioAdaptation(console);
const minBitrate = 16 * 1000;
const defBitrate = 32 * 1000;

describe('AudioAdaptation', () => {
    describe('calcBitrate-args', () => {
        it('test01', () => {
            expect(aa._calcBitrate(-1, -1)).to.equal(defBitrate);
        });
        it('test02', () => {
            expect(aa._calcBitrate(undefined, undefined)).to.equal(defBitrate);
        });
        it('test03', () => {
            expect(aa._calcBitrate(0.1, 0.1)).to.equal(defBitrate);
        });
        it('test04', () => {
            expect(aa._calcBitrate(120, 100 * 1000)).to.equal(minBitrate);
        });
    });
    describe('calcBitrate', () => {
        function calcTest(rtt, maxBitrate, stepBitrate) {
            let bitrate = maxBitrate;
            for (let loss = 2; loss <= 10; loss++) {
                //console.error(`${loss},${rtt} = ${aa._calcBitrate(loss, rtt)} bitrate=${bitrate}`);
                expect(aa._calcBitrate(loss, rtt)).to.equal(Math.floor(bitrate) * 1000);
                bitrate -= stepBitrate;
            }
        }
        it('rtt200', () => {
            calcTest(200, 32, 2);
        });
        it('rtt600', () => {
            calcTest(600, 24, 1);
        });
        it('rtt900', () => {
            calcTest(900, 24, 1);
        });
    });
});
