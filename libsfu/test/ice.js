const assert = require('assert');
const {
    iceState,
    sortedIceStates,
    compareIceStatePriority,
    getPriorityIceState,
} = require('../lib/utils/ice');

describe('compareIceStatePriority', () => {
    it('should return value < 0 when the first ICE state is priority', () => {
        assert(compareIceStatePriority(iceState.CLOSED, iceState.FAILED) < 0);
        assert(compareIceStatePriority(iceState.DISCONNECTED, iceState.CONNECTED) < 0);
    });

    it('should return value > 0 when the second ICE state is priority', () => {
        assert(compareIceStatePriority(iceState.CLOSED, iceState.NONE) > 0);
        assert(compareIceStatePriority(iceState.DISCONNECTED, iceState.FAILED) > 0);
    });

    it('should return 0 if state the same', () => {
        assert.strictEqual(compareIceStatePriority(iceState.CLOSED, iceState.CLOSED), 0);
        assert.strictEqual(compareIceStatePriority(iceState.CONNECTED, iceState.CONNECTED), 0);
    });

    it('should give priority to the existing ICE state over the nonexistent', () => {
        assert(compareIceStatePriority(iceState.CONNECTED, 'nonexistent ICE state') < 0);
        assert(compareIceStatePriority('nonexistent ICE state', iceState.CONNECTED) > 0);
    });

    it('should sort ICE state list by priority', () => {
        const unsorted = [
            iceState.CONNECTED,
            iceState.FAILED,
            iceState.NEW,
            iceState.NONE,
            iceState.DISCONNECTED,
            iceState.CHECKING,
            iceState.COMPLETED,
            iceState.CLOSED,
        ];
        assert.deepStrictEqual(unsorted.sort(compareIceStatePriority), sortedIceStates);
    });
});

describe('getPriorityIceState', () => {
    it('should return priority ICE state', () => {
        assert.strictEqual(getPriorityIceState(iceState.CLOSED, iceState.FAILED), iceState.CLOSED);
        assert.strictEqual(
            getPriorityIceState(iceState.NEW, iceState.DISCONNECTED),
            iceState.DISCONNECTED
        );
    });
});
