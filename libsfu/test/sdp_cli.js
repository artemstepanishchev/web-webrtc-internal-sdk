const chai = require('chai');
const expect = chai.expect;
import SDP from '../lib/sdp_cli';
import testData from './test_data/sdp';
import testUtils from './utils/utils';

describe('SDP', () => {
    describe('constructor', () => {
        it('should accept session as string', () => {
            const sdp = new SDP(testData.sdpNormal);
            expect(testData.sdpNormal).to.equal(sdp.toString());
        });
        it('should fail if null is passed', () => {
            expect(() => new SDP(null)).to.throw('Unexpected options type (object )');
        });
        it('should accept session object as parameter', () => {
            const sdp = new SDP(testData.sessionCustom);
            expect(testData.sessionCustom).to.equal(sdp.getSession());
        });
        it('should create default session if no options passed', () => {
            const sdp = new SDP();
            sdp.getSession().origin.sessionId = testData.sessionDefault.origin.sessionId;
            expect(testData.sessionDefault).to.deep.equal(sdp.getSession());
        });
    });
    describe('fromString', () => {
        it('should throw if m-line has wrong format', () => {
            expect(() => new SDP(testData.sdpBrokenMLine)).to.throw(
                'Bad media line. Cannot parse (video 9 )'
            );
        });
        it('should throw if c-line has wrong format', () => {
            expect(() => new SDP(testData.sdpBrokenCLine)).to.throw(
                'Bad connection data line. Cannot parse (IN IP4 )'
            );
        });
        it('should process c-line on session level', () => {
            const sdp = new SDP(testData.sdpSessionLevelCLine);
            expect(sdp.getSession().connectionData).to.deep.equal(testData.connectionDataNormal);
        });
        it('should throw if t-line is on media level', () => {
            expect(() => new SDP(testData.sdpTLineMediaLevel)).to.throw(
                'Bad time line. (In context of media)'
            );
        });
        it('should throw if t-line has wrong format', () => {
            expect(() => new SDP(testData.sdpBrokenTLine)).to.throw(
                'Bad time line. Cannot parse (0 0 1)'
            );
        });
        it('should throw if s-line repeats', () => {
            expect(() => new SDP(testData.sdpSLineRepeats)).to.throw(
                'Bad session name. (duplicate)'
            );
        });
        it('should throw if o-line has wrong format', () => {
            expect(() => new SDP(testData.sdpBrokenOLine)).to.throw(
                'Bad origin. Cannot parse (- 1464914286032 1464914286032 IN IP4 )'
            );
        });
        it('should throw if version != 0', () => {
            expect(() => new SDP(testData.sdpVersion1)).to.throw(
                'Version 1 of SDP is not supported'
            );
        });
        it('should throw if b-line has wrong format', () => {
            expect(() => new SDP(testData.sdpBrokenBLine)).to.throw(
                'Bad bandwidth line. Cannot parse (AS 2000)'
            );
        });
        it('should process b-line on session level', () => {
            const sdp = new SDP(testData.sdpSessionLevelBLine);
            expect(sdp.getSession().bandwidth).to.deep.equal(testData.bandwidthNormal);
        });
        it('should ignore unknown line types in sdp', () => {
            new SDP(testData.sdpUnknowLine);
        });
    });
    describe('toString', () => {
        it('should put default version if is not specified', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            delete session.version;
            const sdpText = sdp.toString();
            expect(sdpText).to.match(/^v=0\r\n/);
        });
        it('should not throw if origin is is not specified', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            delete session.origin;
            const sdpText = sdp.toString();
            expect(sdpText).to.not.match(/^o=/m);
        });
        it('should not throw if session name is not specified', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            delete session.name;
            const sdpText = sdp.toString();
            expect(sdpText).to.not.match(/^s=/m);
        });
        it('should stringify connectionData on session level', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.connectionData = testData.connectionDataNormal;
            const sdpText = sdp.toString();
            expect(sdpText).to.match(/^c=IN IP4 0\.0\.0\.0/m);
        });
        it('should stringify bandwidth on session level', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.bandwidth = testData.bandwidthNormal;
            const sdpText = sdp.toString();
            expect(sdpText).to.match(/^b=AS:2000/m);
        });
        it('should not throw if no connectionData on media level', () => {
            const sdp = new SDP();
            const media = {};
            sdp.addMedia(media);
            const sdpText = sdp.toString();
            expect(sdpText).to.not.match(/^c=IN IP4 0\.0\.0\.0/m);
        });
        it('should not throw if object prototype is contaminated', () => {
            testUtils.contaminateObjectPrototype();
            const sdp = new SDP();
            const sdpText = sdp.toString();
            expect(sdpText).to.not.include('unexpectedProperty');
            testUtils.resetObjectPrototype();
        });
        it('should throw if attribute has unexpected type', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.attributes.test = null;
            expect(() => sdp.toString()).to.throw(
                'Unexpected attribute value (name=test typeof value=object)'
            );
        });
    });
    describe('getOrigin', () => {
        it('should return origin', () => {
            const sdp = new SDP(testData.sessionCustom);
            expect(sdp.getOrigin()).to.equal(testData.sessionCustom.origin);
        });
    });
    describe('getAttribute', () => {
        it('should return single value attribute as array', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.attributes.test = 1;
            expect(sdp.getAttribute('test')).to.deep.equal([1]);
        });
    });
    describe('getFirstAttribute', () => {
        it('should return first attribute', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.attributes.test = [1, 2, 3];
            expect(sdp.getFirstAttribute('test')).to.equal(1);
        });
    });
    describe('setFirstAttribute', () => {
        it('should set first attribute and override existing attributes', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.attributes.test = [1, 2, 3];
            sdp.setFirstAttribute('test', 'foo');
            expect(sdp.getFirstAttribute('test')).to.equal('foo');
        });
    });
    describe('getFirstMediaAttributeWithFallback', () => {
        it('should return first media attribute if it is exist', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia()[0];
            expect(sdp.getFirstMediaAttributeWithFallback(media, 'mid')).to.equal(
                media.attributes.mid
            );
        });
        it('should return first session attribute if it not exist on media level', () => {
            const sdp = new SDP(testData.sdpNormal);
            const session = sdp.getSession();
            session.attributes.test = [1, 2, 3];
            const media = sdp.getMedia()[0];
            expect(sdp.getFirstMediaAttributeWithFallback(media, 'test')).to.equal(1);
        });
    });
    describe('setAttribute', () => {
        it('should set attribute to session', () => {
            const sdp = new SDP();
            sdp.setAttribute('test', 'foo');
            expect(sdp.getFirstAttribute('test')).to.equal('foo');
        });
        it('should set attribute to session even if attributes are not defined yet', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            delete session.attributes;
            sdp.setAttribute('test', 'foo');
            expect(sdp.getFirstAttribute('test')).to.equal('foo');
        });
        it('should set multiple attributes with the same name to session', () => {
            const sdp = new SDP();
            sdp.setAttribute('test', 'foo');
            sdp.setAttribute('test', 'bar');
            expect(sdp.getAttribute('test')).to.deep.equal(['foo', 'bar']);
        });
        it('should add array of new values to existing single value of an attribute of the session', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.attributes.test = 'foo';
            sdp.setAttribute('test', ['bar', 'baz']);
            expect(sdp.getAttribute('test')).to.deep.equal(['foo', 'bar', 'baz']);
        });
        it('should add array of new values to existing array of values of an attribute of the session', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.attributes.test = ['foo', 'bar'];
            sdp.setAttribute('test', ['baz', 'foo2']);
            expect(sdp.getAttribute('test')).to.deep.equal(['foo', 'bar', 'baz', 'foo2']);
        });
    });
    describe('deleteAttribute', () => {
        it('should delete attribute from session', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.attributes.test = [1, 2, 3];
            sdp.deleteAttribute('test');
            expect(sdp.getFirstAttribute('test')).to.equal(undefined);
        });
    });
    describe('deleteMediaAttribute', () => {
        it('should delete attribute from media', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia('audio')[0];
            media.attributes = {
                test: [1, 2, 3],
            };
            sdp.deleteMediaAttribute(media, 'test');
            expect(sdp.getFirstMediaAttribute(media, 'test')).to.equal(undefined);
        });
    });
    describe('getMediaConnectionDataWithFallback', () => {
        it('should get connectionData from media', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia('video')[0];
            media.connectionData = testData.connectionDataNormal;
            expect(sdp.getMediaConnectionDataWithFallback(media)).to.equal(
                testData.connectionDataNormal
            );
        });
        it('should get connectionData from session if it doesn\'t exists on media level', () => {
            const sdp = new SDP(testData.sdpNormal);
            const session = sdp.getSession();
            session.connectionData = testData.connectionDataNormal;
            const media = sdp.getMedia('video')[0];
            delete media.connectionData;
            expect(sdp.getMediaConnectionDataWithFallback(media)).to.equal(
                testData.connectionDataNormal
            );
        });
    });
    describe('setAttribute', () => {
        it('should set attribute to media', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia()[0];
            sdp.setMediaAttribute(media, 'test', 'foo');
            expect(sdp.getFirstMediaAttribute(media, 'test')).to.equal('foo');
        });
        it('should set attribute to media even if attributes are not defined yet', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia()[0];
            delete media.attributes;
            sdp.setMediaAttribute(media, 'test', 'foo');
            expect(sdp.getFirstMediaAttribute(media, 'test')).to.equal('foo');
        });
    });
    describe('increaseVersion', () => {
        it('should increase version', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            session.origin.sessionVersion = 100;
            sdp.increaseVersion();
            expect(session.origin.sessionVersion).to.equal(101);
        });
    });
    describe('removeAllCodecs', () => {
        it('should remove all codec-related attributes', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia()[0];
            sdp.removeAllCodecs(media);
            expect(media.attributes['rtcp-fb']).to.equal(undefined);
            expect(media.attributes['rtpmap']).to.equal(undefined);
            expect(media.attributes['fmtp']).to.equal(undefined);
            expect(media.format).to.equal('');
        });
    });
    describe('addCodec', () => {
        it('should add codec to media', () => {
            const sdp = new SDP();
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addMedia(media);
            sdp.addCodec({
                name: SDP.codec.OPUS,
                id: 111,
                rtcp_fb: ['transport-cc', 'pli'], // eslint-disable-line camelcase
                fmtp: ['minptime=10; useinbandfec=1', 'maxpackettime=60'],
            });
            const sdpText = sdp.toString();
            expect(sdpText).to.match(/^a=rtpmap:111 OPUS\/48000\/2$/m);
            expect(sdpText).to.match(/^a=rtcp-fb:111 transport-cc$/m);
            expect(sdpText).to.match(/^a=rtcp-fb:111 pli$/m);
            // due to RCV-29621 we cannot set this attribute according to RFC-4566
            expect(sdpText).to.match(/^a=fmtp:111 minptime=10; useinbandfec=1$/m);
            expect(sdpText).to.match(/^a=fmtp:111 maxpackettime=60$/m);
        });
        it('should add codec to media with empty fmtp', () => {
            const sdp = new SDP();
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addMedia(media);
            sdp.addCodec({
                name: SDP.codec.OPUS,
                id: 111,
                rtcp_fb: ['transport-cc', 'pli'], // eslint-disable-line camelcase
            });
            const sdpText = sdp.toString();
            expect(sdpText).to.match(/^a=rtpmap:111 OPUS\/48000\/2$/m);
            expect(sdpText).not.to.match(/^a=fmtp:111/m);
        });
        it('should trow if codec type is unknown', () => {
            const sdp = new SDP();
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addMedia(media);
            expect(() => {
                sdp.addCodec({
                    name: 'UNKNOWN/90000',
                    id: 111,
                });
            }).to.throw('Unknown codec UNKNOWN/90000');
        });
    });
    describe('getAllCodecsOfMedia', () => {
        it('should get all codecs of the media', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia('video')[0];
            const codecs = sdp.getAllCodecsOfMedia(media);
            expect(codecs).to.deep.equal(testData.codecsVideoNormal);
        });
        it('should ignore broken rtpmap', () => {
            const sdp = new SDP(testData.sdpBrokenOPUSRTPMAP);
            const media = sdp.getMedia('audio')[0];
            const codecs = sdp.getAllCodecsOfMedia(media);
            expect(codecs.length).to.equal(0);
        });
    });
    describe('getAllCodecs', () => {
        it('should get all codecs', () => {
            const sdp = new SDP(testData.sdpNormal);
            const codecs = sdp.getAllCodecs();
            expect(codecs).to.deep.equal(testData.codecsAllNormal);
        });
    });
    describe('addCodecToMedia', () => {
        it('should add codec to media with default codecId if it is not set', () => {
            const sdp = new SDP();
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addMedia(media);
            sdp.addCodecToMedia(media, SDP.codec.OPUS);
            expect(media.format).to.equal('111');
        });
        it('should throw if codecType is not known', () => {
            const sdp = new SDP();
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addMedia(media);
            expect(() => sdp.addCodecToMedia(media, 'UNKNOWN/9000')).to.throw(
                'Unknown codec UNKNOWN/9000'
            );
        });
        it('should throw if codecType is not the same as media type', () => {
            const sdp = new SDP();
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addMedia(media);
            expect(() => sdp.addCodecToMedia(media, SDP.codec.VP8)).to.throw(
                'Media type and codec doesn\'t match (audio and video)'
            );
        });
    });
    describe('removeRTPExtensionFromMedia', function () {
        it('should remove RTP extension from media', function () {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia('audio')[0];
            const extmediaInitial = sdp.getMediaAttribute(media, 'extmap');
            expect(extmediaInitial.length).to.equal(2);
            sdp.removeRTPExtensionFromMedia(media, SDP.rtpExtension.ABSOLUTE_SENDER_TIME);
            expect(sdp.getMediaAttribute(media, 'extmap').length).to.equal(1);
        });
    });
    describe('addRTPExtensionToMedia', () => {
        it('should add RTP extension to media', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addRTPExtensionToMedia(media, 5, 'urn:ietf:params:rtp-hdrext:ssrc-audio-level');
            expect(media.attributes.extmap).to.equal(
                '5 urn:ietf:params:rtp-hdrext:ssrc-audio-level'
            );
        });
        it('should use default extname by id', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = testUtils.clone(testData.mediaAudioNormal);
            sdp.addRTPExtensionToMedia(media, 5);
            expect(media.attributes.extmap).to.equal(
                '5 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01'
            );
        });
        it('should throw if no default extname can be found by id', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = testUtils.clone(testData.mediaAudioNormal);
            expect(() => sdp.addRTPExtensionToMedia(media, 5000000)).to.throw(
                'Unknown extension 5000000'
            );
        });
    });
    describe('getMedia', () => {
        it('should return empy array in case there were no media', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            delete session.media;
            expect(sdp.getMedia().length).to.equal(0);
        });
    });
    describe('getMediaCopy', () => {
        it('should return deep copy of media', () => {
            const sdp = new SDP(testData.sdpNormal);
            const media = sdp.getMedia();
            const mediaCopy = sdp.getMediaCopy();
            expect(media).to.not.equal(mediaCopy);
            expect(media).to.deep.equal(mediaCopy);
        });
        it('should return deep copy of media even if media not set', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            delete session.media;
            const media = sdp.getMedia();
            const mediaCopy = sdp.getMediaCopy();
            expect(media).to.not.equal(mediaCopy);
            expect(media).to.deep.equal(mediaCopy);
        });
    });
    describe('setMedia', () => {
        it('should set media', () => {
            const sdp = new SDP();
            const session = sdp.getSession();
            const media = testUtils.clone(testData.mediaAudioNormal);
            const mediaArray = [media];
            sdp.setMedia(mediaArray);
            expect(session.media).to.equal(mediaArray);
        });
    });
    describe('spread', () => {
        it('should split multiple sdps as string to array of sdps', () => {
            const sdpCombined = testData.sdpNormal + testData.sdpNormal;
            expect(SDP.spread(sdpCombined)).to.deep.equal([testData.sdpNormal, testData.sdpNormal]);
        });
    });
    describe('concat', () => {
        it('should join multiple sdps as array to string of sdps', () => {
            const sdpArray = [testData.sdpNormal, testData.sdpNormal];
            expect(SDP.concat(sdpArray)).to.equal(testData.sdpNormal + testData.sdpNormal);
        });
    });
    describe('prettyPrint', () => {
        it('should pretty print SDP', () => {
            expect(SDP.prettyPrint(testData.sdpNormal)).to.equal(testData.sdpNormalPrettyPrint);
        });
    });
});
