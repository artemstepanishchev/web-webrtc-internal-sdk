/* eslint-disable camelcase */
module.exports = {
    sdpNormal: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
m=audio 9 UDP/TLS/RTP/SAVPF 111\r
c=IN IP4 0.0.0.0\r
b=AS:2000\r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:active\r
a=mid:audio\r
a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=recvonly\r
a=rtcp-mux\r
a=rtpmap:111 opus/48000/2\r
a=fmtp:111 minptime=10; useinbandfec=1\r
a=maxptime:60\r
a=ssrc:1807304862 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1807304862 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve d6609f59-740b-4745-b362-871f70bf7363\r
a=ssrc:1807304862 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1807304862 label:d6609f59-740b-4745-b362-871f70bf7363\r
m=video 9 UDP/TLS/RTP/SAVPF 100 101 116 117 96\r
c=IN IP4 0.0.0.0\r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:actpass\r
a=mid:video\r
a=extmap:2 urn:ietf:params:rtp-hdrext:toffset\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=extmap:4 urn:3gpp:video-orientation\r
a=sendrecv\r
a=rtcp-mux\r
a=rtpmap:100 VP8/90000\r
a=rtpmap:101 VP9/90000\r
a=rtpmap:116 red/90000\r
a=rtpmap:117 ulpfec/90000\r
a=rtpmap:96 rtx/90000\r
a=rtcp-fb:100 ccm fir\r
a=rtcp-fb:100 nack\r
a=rtcp-fb:100 nack pli\r
a=rtcp-fb:100 transport-cc\r
a=rtcp-fb:101 ccm fir\r
a=rtcp-fb:101 nack\r
a=rtcp-fb:101 nack pli\r
a=rtcp-fb:101 transport-cc\r
a=fmtp:96 apt=100\r
a=ssrc-group:FID 1160198556 748112202\r
a=ssrc:1160198556 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1160198556 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve 9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:1160198556 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1160198556 label:9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:748112202 cname:coRWU3CJd1Q3je6H\r
a=ssrc:748112202 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve 9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:748112202 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:748112202 label:9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
`,
    sdpNormalPrettyPrint: `
v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r

m=audio 9 UDP/TLS/RTP/SAVPF 111\r
c=IN IP4 0.0.0.0\r
b=AS:2000\r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:active\r
a=mid:audio\r
a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=recvonly\r
a=rtcp-mux\r
a=rtpmap:111 opus/48000/2\r
a=fmtp:111 minptime=10; useinbandfec=1\r
a=maxptime:60\r
a=ssrc:1807304862 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1807304862 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve d6609f59-740b-4745-b362-871f70bf7363\r
a=ssrc:1807304862 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1807304862 label:d6609f59-740b-4745-b362-871f70bf7363\r

m=video 9 UDP/TLS/RTP/SAVPF 100 101 116 117 96\r
c=IN IP4 0.0.0.0\r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:actpass\r
a=mid:video\r
a=extmap:2 urn:ietf:params:rtp-hdrext:toffset\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=extmap:4 urn:3gpp:video-orientation\r
a=sendrecv\r
a=rtcp-mux\r
a=rtpmap:100 VP8/90000\r
a=rtpmap:101 VP9/90000\r
a=rtpmap:116 red/90000\r
a=rtpmap:117 ulpfec/90000\r
a=rtpmap:96 rtx/90000\r
a=rtcp-fb:100 ccm fir\r
a=rtcp-fb:100 nack\r
a=rtcp-fb:100 nack pli\r
a=rtcp-fb:100 transport-cc\r
a=rtcp-fb:101 ccm fir\r
a=rtcp-fb:101 nack\r
a=rtcp-fb:101 nack pli\r
a=rtcp-fb:101 transport-cc\r
a=fmtp:96 apt=100\r
a=ssrc-group:FID 1160198556 748112202\r
a=ssrc:1160198556 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1160198556 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve 9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:1160198556 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1160198556 label:9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:748112202 cname:coRWU3CJd1Q3je6H\r
a=ssrc:748112202 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve 9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:748112202 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:748112202 label:9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
`,
    sdpBrokenMLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
m=video 9 \r
c=IN IP4 0.0.0.0\r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:actpass\r
a=mid:video\r
a=extmap:2 urn:ietf:params:rtp-hdrext:toffset\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=extmap:4 urn:3gpp:video-orientation\r
a=sendrecv\r
a=rtcp-mux\r
a=rtpmap:100 VP8/90000\r
a=rtpmap:101 VP9/90000\r
a=rtpmap:116 red/90000\r
a=rtpmap:117 ulpfec/90000\r
a=rtpmap:96 rtx/90000\r
a=rtcp-fb:100 ccm fir\r
a=rtcp-fb:100 nack\r
a=rtcp-fb:100 nack pli\r
a=rtcp-fb:100 transport-cc\r
a=rtcp-fb:101 ccm fir\r
a=rtcp-fb:101 nack\r
a=rtcp-fb:101 nack pli\r
a=rtcp-fb:101 transport-cc\r
a=fmtp:96 apt=100\r
a=ssrc-group:FID 1160198556 748112202\r
a=ssrc:1160198556 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1160198556 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve 9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:1160198556 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1160198556 label:9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:748112202 cname:coRWU3CJd1Q3je6H\r
a=ssrc:748112202 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve 9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
a=ssrc:748112202 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:748112202 label:9d5e7b88-98bd-4a99-85ca-ffa01d94b84c\r
`,
    sdpBrokenCLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
m=audio 9 UDP/TLS/RTP/SAVPF 111\r
c=IN IP4 \r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:active\r
a=mid:audio\r
a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=recvonly\r
a=rtcp-mux\r
a=rtpmap:111 opus/48000/2\r
a=fmtp:111 minptime=10; useinbandfec=1\r
a=maxptime:60\r
a=ssrc:1807304862 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1807304862 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve d6609f59-740b-4745-b362-871f70bf7363\r
a=ssrc:1807304862 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1807304862 label:d6609f59-740b-4745-b362-871f70bf7363\r
`,
    sdpSessionLevelCLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
c=IN IP4 0.0.0.0\r
`,
    sdpTLineMediaLevel: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
c=IN IP4 0.0.0.0\r
m=audio 9 UDP/TLS/RTP/SAVPF 111\r
t=0 0\r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:active\r
a=mid:audio\r
a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=recvonly\r
a=rtcp-mux\r
a=rtpmap:111 opus/48000/2\r
a=fmtp:111 minptime=10; useinbandfec=1\r
a=maxptime:60\r
a=ssrc:1807304862 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1807304862 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve d6609f59-740b-4745-b362-871f70bf7363\r
a=ssrc:1807304862 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1807304862 label:d6609f59-740b-4745-b362-871f70bf7363\r
`,
    sdpBrokenTLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0 1\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
`,
    sdpSLineRepeats: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
s=Another Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
`,
    sdpBrokenOLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 \r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
`,
    sdpVersion1: `v=1\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
`,
    sdpBrokenBLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
b=AS 2000\r
`,
    sdpSessionLevelBLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
b=AS:2000\r
`,
    sdpUnknowLine: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
z=unknown parameter\r
`,
    sdpBrokenOPUSRTPMAP: `v=0\r
o=- 1464914286032 1464914286032 IN IP4 127.0.0.1\r
s=TEST Server\r
t=0 0\r
a=group:BUNDLE audio video\r
a=msid-semantic: WMS *\r
m=audio 9 UDP/TLS/RTP/SAVPF 111\r
c=IN IP4 0.0.0.0\r
b=AS:2000\r
a=rtcp:9 IN IP4 0.0.0.0\r
a=ice-ufrag:ymg4BkiPvdNvpjsh\r
a=ice-pwd:PJeOSgUFrJHQNS6jBVy6Ts7x\r
a=fingerprint:sha-256 D2:49:9B:C6:39:A8:B2:42:05:E8:5F:BF:44:B0:55:B4:11:70:E6:50:3A:3C:6D:22:CB:73:6D:A6:85:29:C4:1F\r
a=setup:active\r
a=mid:audio\r
a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r
a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r
a=recvonly\r
a=rtcp-mux\r
a=rtpmap:111 opus/48000/2 X\r
a=fmtp:111 minptime=10; useinbandfec=1\r
a=maxptime:60\r
a=ssrc:1807304862 cname:coRWU3CJd1Q3je6H\r
a=ssrc:1807304862 msid:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve d6609f59-740b-4745-b362-871f70bf7363\r
a=ssrc:1807304862 mslabel:gYFbShQExXWNEfiMhnX2Js3Oaw2ejpKCuTve\r
a=ssrc:1807304862 label:d6609f59-740b-4745-b362-871f70bf7363\r
`,
    sessionDefault: {
        version: 0,
        name: 'RC SFU',
        origin: {
            username: '-',
            sessionId: '',
            sessionVersion: 1,
            netType: 'IN',
            addrType: 'IP4',
            unicastAddress: '127.0.0.1',
        },
        time: {
            start: 0,
            stop: 0,
        },
        attributes: {},
    },
    sessionCustom: {
        version: 0,
        name: 'SESSION CUSTOM',
        origin: {
            username: '-',
            sessionId: '',
            sessionVersion: 1,
            netType: 'IN',
            addrType: 'IP4',
            unicastAddress: '127.0.0.2',
        },
        time: {
            start: 0,
            stop: 0,
        },
        attributes: {},
    },
    connectionDataNormal: {
        netType: 'IN',
        addrType: 'IP4',
        connectionAddress: '0.0.0.0',
    },
    bandwidthNormal: {
        bwtype: 'AS',
        bandwidth: '2000',
    },
    mediaAudioNormal: {
        media: 'audio',
        port: '9',
        proto: 'UDP/TLS/RTP/SAVPF',
        format: '',
    },
    codecsVideoNormal: [
        {
            id: '100',
            name: 'VP8/90000',
            rtcp_fb: ['ccm fir', 'nack', 'nack pli', 'transport-cc'],
            fmtp: [],
            rtxId: '96',
        },
        {
            id: '101',
            name: 'VP9/90000',
            rtcp_fb: ['ccm fir', 'nack', 'nack pli', 'transport-cc'],
            fmtp: [],
        },
        { id: '116', name: 'RED/90000', rtcp_fb: [], fmtp: [] },
        { id: '117', name: 'ULPFEC/90000', rtcp_fb: [], fmtp: [] },
        { id: '96', name: 'RTX/90000', rtcp_fb: [], fmtp: ['apt=100'] },
    ],
    codecsAllNormal: [
        { id: '111', name: 'OPUS/48000/2', rtcp_fb: [], fmtp: ['minptime=10; useinbandfec=1'] },
        {
            id: '100',
            name: 'VP8/90000',
            rtcp_fb: ['ccm fir', 'nack', 'nack pli', 'transport-cc'],
            fmtp: [],
            rtxId: '96',
        },
        {
            id: '101',
            name: 'VP9/90000',
            rtcp_fb: ['ccm fir', 'nack', 'nack pli', 'transport-cc'],
            fmtp: [],
        },
        { id: '116', name: 'RED/90000', rtcp_fb: [], fmtp: [] },
        { id: '117', name: 'ULPFEC/90000', rtcp_fb: [], fmtp: [] },
        { id: '96', name: 'RTX/90000', rtcp_fb: [], fmtp: ['apt=100'] },
    ],
};
