module.exports = {
    contaminateObjectPrototype() {
        Object.prototype.unexpectedProperty = 'unexpectedPropertyValue';
    },
    resetObjectPrototype() {
        delete Object.prototype.unexpectedProperty;
    },
    clone(obj) {
        return JSON.parse(JSON.stringify(obj));
    },
};
