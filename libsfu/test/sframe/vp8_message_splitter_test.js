const chai = require('chai');
const expect = chai.expect;

import LogWrapper from '../../lib/log_wrapper.js';
import { MockMlsSdkClient } from './mock_sdk_client.js';
import { VP8MessageSplitter } from '../../lib/sframe/vp8_message_splitter.js';
import { MessagePart } from '../../lib/sframe/message_part.js';

const logger = new LogWrapper(console, () => 'message_splitter_test:');
const sdk = new MockMlsSdkClient();
sdk.init({ logger: logger });
const messageSplitter = new VP8MessageSplitter(() => sdk);

describe('VP8MessageSplitter', () => {
    describe('splitEncryptedMessage', () => {
        it('should return message split to clear header and encrypted data', () => {
            const clearBlock = new Uint8Array(10); // 10 is like in key frame
            clearBlock[0] = 11;
            clearBlock[clearBlock.length - 1] = 11;

            const encryptedBlock = new Uint8Array(
                sdk.encrypt(new Uint8Array(15), clearBlock.length)
            );

            const wholeMessage = new Uint8Array(encryptedBlock.length + clearBlock.length);
            wholeMessage.set(clearBlock, 0);
            wholeMessage.set(encryptedBlock, clearBlock.length);

            expect(messageSplitter.splitEncryptedMessage(wholeMessage)).to.deep.equal([
                new MessagePart(clearBlock, false),
                new MessagePart(encryptedBlock, true),
            ]);
        });
    });
    describe('splitClearMessage', () => {
        const payload = new Uint8Array(15);
        payload[0] = 222;

        it('should return two parts, 3 byte header and payload, for non keyframe message', () => {
            const header = new Uint8Array(3);
            header[0] = 1;
            header[2] = 111;

            const wholeMessage = new Uint8Array(header.length + payload.length);
            wholeMessage.set(header, 0);
            wholeMessage.set(payload, header.length);

            const result = messageSplitter.splitClearMessage(wholeMessage);
            expect(result).to.deep.equal([
                new MessagePart(header, false),
                new MessagePart(payload, true),
            ]);
            expect(result[0].getContent()[2]).to.equal(111);
        });

        it('should fallback and return whole message as encrypted if header is invalid', () => {
            const wholeMessage = new Uint8Array(9); // should be a key frame but incorrect length
            wholeMessage[0] = 0;

            expect(messageSplitter.splitClearMessage(wholeMessage)).to.deep.equal([
                new MessagePart(wholeMessage, true),
            ]);
        });

        it('should return two parts, 10 byte header and payload, for keyframe message', () => {
            const header = new Uint8Array(10);
            header[0] = 0;
            header[9] = 111;

            const wholeMessage = new Uint8Array(header.length + payload.length);
            wholeMessage.set(header, 0);
            wholeMessage.set(payload, header.length);

            const result = messageSplitter.splitClearMessage(wholeMessage);
            expect(result).to.deep.equal([
                new MessagePart(header, false),
                new MessagePart(payload, true),
            ]);
            expect(result[0].getContent()[9]).to.equal(111);
        });

        it('should fallback and return whole message as encrypted on length less than header (3)', () => {
            const wholeMessage = new Uint8Array(2);

            expect(messageSplitter.splitClearMessage(wholeMessage)).to.deep.equal([
                new MessagePart(wholeMessage, true),
            ]);
        });
    });
});
