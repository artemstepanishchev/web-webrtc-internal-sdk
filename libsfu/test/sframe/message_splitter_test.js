const chai = require('chai');
const expect = chai.expect;
import LogWrapper from '../../lib/log_wrapper.js';
import { MockMlsSdkClient } from './mock_sdk_client.js';
import { MessageSplitter } from '../../lib/sframe/message_splitter.js';
import { MessagePart } from '../../lib/sframe/message_part.js';

const logger = new LogWrapper(console, () => 'message_splitter_test:');
const sdk = new MockMlsSdkClient();
sdk.init({ logger: logger });
const messageSplitter = new MessageSplitter(() => sdk);

describe('MessageSplitter', () => {
    describe('splitClearMessage', () => {
        it('should put any input to single part with encryption', () => {
            const message = new Uint8Array(10);
            expect(messageSplitter.splitClearMessage(message)).to.deep.equal([
                new MessagePart(message, true),
            ]);
        });
    });
    describe('splitEncryptedMessage', () => {
        it('should return empty list of parts on empty message', () => {
            const message = new Uint8Array(0);
            expect(messageSplitter.splitEncryptedMessage(message)).to.deep.equal([]);
        });
        it('should return one part without encryption from simple message without encrypted blocks', () => {
            const message = new Uint8Array(10);
            expect(messageSplitter.splitEncryptedMessage(message)).to.deep.equal([
                new MessagePart(message, false),
            ]);
        });
        it('should return one encrypted part when whole input is one encrypted block', () => {
            const message = new Uint8Array(sdk.encrypt(new Uint8Array(10), 0));
            expect(messageSplitter.splitEncryptedMessage(message)).to.deep.equal([
                new MessagePart(message, true),
            ]);
        });

        const clearBlock = new Uint8Array(10);
        clearBlock[0] = 11;
        clearBlock[clearBlock.length - 1] = 11;

        it('should return one clear and then one encrypted block', () => {
            const encryptedBlock = new Uint8Array(
                sdk.encrypt(new Uint8Array(15), clearBlock.length)
            );
            const wholeMessage = new Uint8Array(encryptedBlock.length + clearBlock.length);
            wholeMessage.set(clearBlock, 0);
            wholeMessage.set(encryptedBlock, clearBlock.length);

            expect(messageSplitter.splitEncryptedMessage(wholeMessage)).to.deep.equal([
                new MessagePart(clearBlock, false),
                new MessagePart(encryptedBlock, true),
            ]);
        });

        it('should return one encrypted and then one clear block', () => {
            const encryptedBlock = sdk.encrypt(new Uint8Array(15), 0);
            const wholeMessage = new Uint8Array(encryptedBlock.length + clearBlock.length);
            wholeMessage.set(encryptedBlock, 0);
            wholeMessage.set(clearBlock, encryptedBlock.length);

            expect(messageSplitter.splitEncryptedMessage(wholeMessage)).to.deep.equal([
                new MessagePart(encryptedBlock, true),
                new MessagePart(clearBlock, false),
            ]);
        });

        it('should return one clear block when offset in signature does not match', () => {
            const encryptedBlock = sdk.encrypt(new Uint8Array(15), 3); // no signature at offset 3
            const wholeMessage = new Uint8Array(encryptedBlock.length + clearBlock.length);
            wholeMessage.set(clearBlock, 0);
            wholeMessage.set(encryptedBlock, clearBlock.length);

            expect(messageSplitter.splitEncryptedMessage(wholeMessage)).to.deep.equal([
                new MessagePart(wholeMessage, false),
            ]);
        });

        it('should return four blocks encrypted after clear', () => {
            const encryptedBlock1 = sdk.encrypt(new Uint8Array(15), clearBlock.length);
            const encryptedBlock2 = sdk.encrypt(
                new Uint8Array(8),
                clearBlock.length * 2 + encryptedBlock1.length
            );
            const wholeMessage = new Uint8Array(
                encryptedBlock1.length + encryptedBlock2.length + clearBlock.length * 2
            );
            wholeMessage.set(clearBlock, 0);
            wholeMessage.set(encryptedBlock1, clearBlock.length);
            wholeMessage.set(clearBlock, clearBlock.length + encryptedBlock1.length);
            wholeMessage.set(encryptedBlock2, clearBlock.length * 2 + encryptedBlock1.length);

            expect(messageSplitter.splitEncryptedMessage(wholeMessage)).to.deep.equal([
                new MessagePart(clearBlock, false),
                new MessagePart(encryptedBlock1, true),
                new MessagePart(clearBlock, false),
                new MessagePart(encryptedBlock2, true),
            ]);
        });
    });
});
