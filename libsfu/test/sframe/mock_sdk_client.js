class MockSFrameMessage {
    SetContent(value) {
        this.data = value;
    }

    GetContent() {
        return this.data;
    }
}

export class MockMlsSdkClient {
    constructor() {
        this._initialized = false;
        this._singature = 0xe2ee;
    }

    getSignatureSize() {
        return 2;
    }

    getOffsetSize() {
        return 4;
    }

    addSignature(dataView, offset) {
        dataView.setUint16(offset, this._singature);
    }

    checkSignature(dataView, offset) {
        if (offset < 0 || offset + this.getSignatureSize() > dataView.byteLength) return false;
        // invalid offset
        else return dataView.getUint16(offset) === this._singature;
    }

    connect() {}

    disconnect() {}

    init(config) {
        if (!this._initialized) {
            this._logger = config.logger;
            this._overheadSize = 28;
            this._initialized = true;
        }
    }

    encrypt(frame, dataOffset) {
        const newData = new Uint8Array(
            frame.length + this._overheadSize + this.getSignatureSize() * 2 + this.getOffsetSize()
        );
        if (this._initialized) {
            const newView = new DataView(newData.buffer);
            this.addSignature(newView, 0);

            const msg = new MockSFrameMessage();
            msg.SetContent(frame);

            const encryptedMsgResult = this.mockSFrameEncrypt(msg);
            const errorCode = encryptedMsgResult.ErrorCode;
            if (errorCode.value !== 0) {
                const error = new Error(
                    `Encryption failed with status code ${errorCode.constructor.name}`
                );
                error.mlsCode = errorCode;
                throw error;
            }
            const encryptedMsg = encryptedMsgResult.GetValue();
            newData.set(encryptedMsg.GetContent(), this.getSignatureSize());

            // Append signature and data offset
            this.addSignature(newView, frame.length + this._overheadSize + this.getSignatureSize());
            newView.setUint32(
                frame.length + this._overheadSize + this.getSignatureSize() * 2,
                dataOffset
            );
        }

        return newData;
    }

    decrypt(data) {
        if (this._initialized) {

            const dataToDecrypt = data.subarray(
                this.getSignatureSize(),
                data.byteLength - (this.getSignatureSize() + this.getOffsetSize())
            );

            const dataSize = dataToDecrypt.length - this._overheadSize;
            if (dataSize < 0) {
                this._logger.error(
                    `Wrong sized frame received (got ${dataToDecrypt.length}, but should be more than ${this._overheadSize}), which expected to be encrypted`
                );
                return null;
            } else if (dataSize === 0) {
                // Some frames have 0 bytes useful data, we agreed to skip them
                return null;
            }

            const newData = new Uint8Array(dataSize);

            const msg = new MockSFrameMessage();
            msg.SetContent(dataToDecrypt);
            const decryptedMsgResult = this.mockSFrameDecrypt(msg);
            const errorCode = decryptedMsgResult.ErrorCode;
            if (errorCode.value !== 0) {
                const error = new Error(
                    `Decryption failed with status code ${errorCode.constructor.name}`
                );
                error.mlsCode = errorCode;
                throw error;
            }
            const decryptedMsg = decryptedMsgResult.GetValue();
            newData.set(decryptedMsg.GetContent(), 0);

            return newData;
        } else return null;
    }

    mockSFrameEncrypt(message) {
        const sourceArray = message.GetContent();
        const result = new Uint8Array(sourceArray.length + 28);

        for (let i = 0; i < 12; i++) {
            result[i] = i;
        }
        result.set(sourceArray, 12);
        for (let i = result.length - 16; i < result.length; i++) {
            result[i] = result.length - i;
        }

        const resultMessage = new MockSFrameMessage();
        resultMessage.SetContent(result);

        return {
            ErrorCode: {
                value: 0,
            },
            GetValue: () => resultMessage,
        };
    }

    mockSFrameDecrypt(message) {
        const sourceArray = message.GetContent();
        let code = 0;

        for (let i = 0; i < 12; i++) {
            if (sourceArray[i] !== i) {
                code = 1;
            }
        }
        const result = sourceArray.subarray(12, sourceArray.length - 16);
        for (let i = sourceArray.length - 16; i < result.length; i++) {
            if (sourceArray[i] !== i) {
                code = 1;
            }
        }

        const resultMessage = new MockSFrameMessage();
        resultMessage.SetContent(result);

        return {
            ErrorCode: {
                value: code,
            },
            GetValue: () => resultMessage,
        };
    }
}
