const chai = require('chai');
const expect = chai.expect;

import LogWrapper from '../../lib/log_wrapper.js';
import { MockMlsSdkClient } from './mock_sdk_client.js';
import { H264MessageSplitter } from '../../lib/sframe/h264_message_splitter.js';
import { MessagePart } from '../../lib/sframe/message_part.js';

const logger = new LogWrapper(console, () => 'message_splitter_test:');
const sdk = new MockMlsSdkClient();
sdk.init({ logger: logger });
const messageSplitter = new H264MessageSplitter(() => sdk);

describe('H264MessageSplitter', () => {
    const clearNALU1 = new Uint8Array(10);
    clearNALU1[3] = 1; // start code
    clearNALU1[4] = 6; // nalu type
    clearNALU1[9] = 111; // content to check

    const clearNALU2 = new Uint8Array(9);
    clearNALU2[2] = 1;
    clearNALU2[3] = 8;
    clearNALU2[8] = 222;

    const encNALU1 = new Uint8Array(40);
    encNALU1[3] = 1;
    encNALU1[4] = 1;
    encNALU1[39] = 111;

    const encNALU2 = new Uint8Array(30);
    encNALU2[2] = 1;
    encNALU2[3] = 5;
    encNALU2[29] = 222;

    describe('splitClearMessage', () => {
        it('should return four parts first three clear, then encrypted', () => {
            const wholeMessage = new Uint8Array(clearNALU1.length + encNALU2.length);
            wholeMessage.set(clearNALU1, 0);
            wholeMessage.set(encNALU2, clearNALU1.length);

            const result = messageSplitter.splitClearMessage(wholeMessage);
            expect(result).to.deep.equal([
                new MessagePart(clearNALU1.subarray(0, clearNALU1.length), false),
                new MessagePart(encNALU2.subarray(0, 4), false),
                new MessagePart(encNALU2.subarray(4, encNALU2.length), true),
            ]);
            expect(result[0].getContent()[9]).to.equal(111);
        });
        it('should return four parts first clear, then encrypted, and two clear', () => {
            const wholeMessage = new Uint8Array(encNALU1.length + clearNALU2.length);
            wholeMessage.set(encNALU1, 0);
            wholeMessage.set(clearNALU2, encNALU1.length);

            const result = messageSplitter.splitClearMessage(wholeMessage);
            expect(result).to.deep.equal([
                new MessagePart(encNALU1.subarray(0, 5), false),
                new MessagePart(encNALU1.subarray(5, encNALU1.length), true),
                new MessagePart(clearNALU2.subarray(0, clearNALU2.length), false),
            ]);
            expect(result[2].getContent()[8]).to.equal(222);
        });
        it('should return six parts first three clear, then encrypted, then two clear', () => {
            const wholeMessage = new Uint8Array(
                clearNALU1.length + encNALU2.length + clearNALU2.length
            );
            wholeMessage.set(clearNALU1, 0);
            wholeMessage.set(encNALU2, clearNALU1.length);
            wholeMessage.set(clearNALU2, clearNALU1.length + encNALU2.length);

            const result = messageSplitter.splitClearMessage(wholeMessage);
            expect(result).to.deep.equal([
                new MessagePart(clearNALU1.subarray(0, clearNALU1.length), false),
                new MessagePart(encNALU2.subarray(0, 4), false),
                new MessagePart(encNALU2.subarray(4, encNALU2.length), true),
                new MessagePart(clearNALU2.subarray(0, clearNALU2.length), false),
            ]);
            expect(result[0].getContent()[9]).to.equal(111);
            expect(result[3].getContent()[8]).to.equal(222);
        });
        it('should return six parts first clear, then encrypted, then three clear, then encrypted', () => {
            const wholeMessage = new Uint8Array(
                encNALU1.length + clearNALU2.length + encNALU2.length
            );
            wholeMessage.set(encNALU1, 0);
            wholeMessage.set(clearNALU2, encNALU1.length);
            wholeMessage.set(encNALU2, encNALU1.length + clearNALU2.length);

            const result = messageSplitter.splitClearMessage(wholeMessage);
            expect(result).to.deep.equal([
                new MessagePart(encNALU1.subarray(0, 5), false),
                new MessagePart(encNALU1.subarray(5, encNALU1.length), true),
                new MessagePart(clearNALU2.subarray(0, clearNALU2.length), false),
                new MessagePart(encNALU2.subarray(0, 4), false),
                new MessagePart(encNALU2.subarray(4, encNALU2.length), true),
            ]);
            expect(result[2].getContent()[8]).to.equal(222);
        });
    });
});
