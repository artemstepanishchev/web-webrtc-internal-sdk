import { expect } from 'chai';
import { getStreamsBitrates } from '../lib/utils/peer_connection';

describe('getStreamsBitrates', () => {
    it('should return defaults with no arguments', () => {
        expect(getStreamsBitrates()).to.eql({
            nonSimulcastStream: 1500000,
            simulcastSSH264: [500000, 1500000, 0],
            simulcastSSVP8: [1000000, 2000000, 0],
            simulcastSSVP8Tab: [1000000, 2000000, 0],
            simulcastTrack: [150000, 500000, 1300000],
        });
    });

    it('should multiply every bitrate with common multiplier', () => {
        expect(getStreamsBitrates({ total: 2 })).to.eql({
            nonSimulcastStream: 3000000,
            simulcastSSH264: [1000000, 3000000, 0],
            simulcastSSVP8: [2000000, 4000000, 0],
            simulcastSSVP8Tab: [2000000, 4000000, 0],
            simulcastTrack: [300000, 1000000, 2600000],
        });
    });

    it('should multiply sharing bitrates with sharing multiplier', () => {
        expect(getStreamsBitrates({ sharing: 2 })).to.eql({
            nonSimulcastStream: 1500000,
            simulcastSSH264: [1000000, 3000000, 0],
            simulcastSSVP8: [2000000, 4000000, 0],
            simulcastSSVP8Tab: [2000000, 4000000, 0],
            simulcastTrack: [150000, 500000, 1300000],
        });
    });

    it('should multiply video bitrates with video multiplier', () => {
        expect(getStreamsBitrates({ video: 2 })).to.eql({
            nonSimulcastStream: 3000000,
            simulcastSSH264: [500000, 1500000, 0],
            simulcastSSVP8: [1000000, 2000000, 0],
            simulcastSSVP8Tab: [1000000, 2000000, 0],
            simulcastTrack: [300000, 1000000, 2600000],
        });
    });

    it('should multiply sharing H264 bitrate with sharing H264 multiplier', () => {
        expect(getStreamsBitrates({ sharing264: 2 })).to.eql({
            nonSimulcastStream: 1500000,
            simulcastSSH264: [1000000, 3000000, 0],
            simulcastSSVP8: [1000000, 2000000, 0],
            simulcastSSVP8Tab: [1000000, 2000000, 0],
            simulcastTrack: [150000, 500000, 1300000],
        });
    });

    it('should multiply sharing VP8 bitrates with sharing VP8 multiplier', () => {
        expect(getStreamsBitrates({ sharingVP8: 2 })).to.eql({
            nonSimulcastStream: 1500000,
            simulcastSSH264: [500000, 1500000, 0],
            simulcastSSVP8: [2000000, 4000000, 0],
            simulcastSSVP8Tab: [2000000, 4000000, 0],
            simulcastTrack: [150000, 500000, 1300000],
        });
    });

    it('should multiply sharing VP8Tab bitrate with sharing VP8Tab multiplier', () => {
        expect(getStreamsBitrates({ sharingVP8Tab: 2 })).to.eql({
            nonSimulcastStream: 1500000,
            simulcastSSH264: [500000, 1500000, 0],
            simulcastSSVP8: [1000000, 2000000, 0],
            simulcastSSVP8Tab: [2000000, 4000000, 0],
            simulcastTrack: [150000, 500000, 1300000],
        });
    });
});
