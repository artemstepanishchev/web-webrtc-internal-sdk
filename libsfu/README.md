# libSFU

Client library to interact with RC SFU

Protocol: https://docs.google.com/document/d/1BowdqsTU7GfyOTpFs2BjbYaeV1OUXqfBMNGwHOiRY1w


### Merge process

Please use `MR approved DEV` label for simple MR code review approvals
and `MR approved (DEV 1)` / `MR approved (DEV 2)`. After QA part is finished
please set `MR approved (QA)` label, and merge.

### Release cut-off

Please contact someone from RCV web core team to make release branch
and use it in RWC if it hasn't been done.
