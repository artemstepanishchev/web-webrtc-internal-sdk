/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

var $protobuf = require('protobufjs/minimal.js');

// Common aliases
var $Reader = $protobuf.Reader,
    $Writer = $protobuf.Writer,
    $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots['default'] || ($protobuf.roots['default'] = {});

$root.nqi = (function () {
    /**
     * Namespace nqi.
     * @exports nqi
     * @namespace
     */
    var nqi = {};

    nqi.StatDetails = (function () {
        /**
         * Properties of a StatDetails.
         * @memberof nqi
         * @interface IStatDetails
         * @property {number|null} [loss] StatDetails loss
         * @property {number|null} [rtt] StatDetails rtt
         * @property {number|null} [jitter] StatDetails jitter
         */

        /**
         * Constructs a new StatDetails.
         * @memberof nqi
         * @classdesc Represents a StatDetails.
         * @implements IStatDetails
         * @constructor
         * @param {nqi.IStatDetails=} [properties] Properties to set
         */
        function StatDetails(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }

        /**
         * StatDetails loss.
         * @member {number} loss
         * @memberof nqi.StatDetails
         * @instance
         */
        StatDetails.prototype.loss = 0;

        /**
         * StatDetails rtt.
         * @member {number} rtt
         * @memberof nqi.StatDetails
         * @instance
         */
        StatDetails.prototype.rtt = 0;

        /**
         * StatDetails jitter.
         * @member {number} jitter
         * @memberof nqi.StatDetails
         * @instance
         */
        StatDetails.prototype.jitter = 0;

        /**
         * Creates a new StatDetails instance using the specified properties.
         * @function create
         * @memberof nqi.StatDetails
         * @static
         * @param {nqi.IStatDetails=} [properties] Properties to set
         * @returns {nqi.StatDetails} StatDetails instance
         */
        StatDetails.create = function create(properties) {
            return new StatDetails(properties);
        };

        /**
         * Encodes the specified StatDetails message. Does not implicitly {@link nqi.StatDetails.verify|verify} messages.
         * @function encode
         * @memberof nqi.StatDetails
         * @static
         * @param {nqi.IStatDetails} message StatDetails message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StatDetails.encode = function encode(message, writer) {
            if (!writer) writer = $Writer.create();
            if (message.loss != null && Object.hasOwnProperty.call(message, 'loss'))
                writer.uint32(/* id 1, wireType 5 =*/ 13).float(message.loss);
            if (message.rtt != null && Object.hasOwnProperty.call(message, 'rtt'))
                writer.uint32(/* id 2, wireType 0 =*/ 16).int32(message.rtt);
            if (message.jitter != null && Object.hasOwnProperty.call(message, 'jitter'))
                writer.uint32(/* id 3, wireType 0 =*/ 24).int32(message.jitter);
            return writer;
        };

        /**
         * Encodes the specified StatDetails message, length delimited. Does not implicitly {@link nqi.StatDetails.verify|verify} messages.
         * @function encodeDelimited
         * @memberof nqi.StatDetails
         * @static
         * @param {nqi.IStatDetails} message StatDetails message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        StatDetails.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a StatDetails message from the specified reader or buffer.
         * @function decode
         * @memberof nqi.StatDetails
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {nqi.StatDetails} StatDetails
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StatDetails.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length,
                message = new $root.nqi.StatDetails();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                    case 1:
                        message.loss = reader.float();
                        break;
                    case 2:
                        message.rtt = reader.int32();
                        break;
                    case 3:
                        message.jitter = reader.int32();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                }
            }
            return message;
        };

        /**
         * Decodes a StatDetails message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof nqi.StatDetails
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {nqi.StatDetails} StatDetails
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        StatDetails.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader)) reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a StatDetails message.
         * @function verify
         * @memberof nqi.StatDetails
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        StatDetails.verify = function verify(message) {
            if (typeof message !== 'object' || message === null) return 'object expected';
            if (message.loss != null && message.hasOwnProperty('loss'))
                if (typeof message.loss !== 'number') return 'loss: number expected';
            if (message.rtt != null && message.hasOwnProperty('rtt'))
                if (!$util.isInteger(message.rtt)) return 'rtt: integer expected';
            if (message.jitter != null && message.hasOwnProperty('jitter'))
                if (!$util.isInteger(message.jitter)) return 'jitter: integer expected';
            return null;
        };

        /**
         * Creates a StatDetails message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof nqi.StatDetails
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {nqi.StatDetails} StatDetails
         */
        StatDetails.fromObject = function fromObject(object) {
            if (object instanceof $root.nqi.StatDetails) return object;
            var message = new $root.nqi.StatDetails();
            if (object.loss != null) message.loss = Number(object.loss);
            if (object.rtt != null) message.rtt = object.rtt | 0;
            if (object.jitter != null) message.jitter = object.jitter | 0;
            return message;
        };

        /**
         * Creates a plain object from a StatDetails message. Also converts values to other types if specified.
         * @function toObject
         * @memberof nqi.StatDetails
         * @static
         * @param {nqi.StatDetails} message StatDetails
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        StatDetails.toObject = function toObject(message, options) {
            if (!options) options = {};
            var object = {};
            if (options.defaults) {
                object.loss = 0;
                object.rtt = 0;
                object.jitter = 0;
            }
            if (message.loss != null && message.hasOwnProperty('loss'))
                object.loss =
                    options.json && !isFinite(message.loss) ? String(message.loss) : message.loss;
            if (message.rtt != null && message.hasOwnProperty('rtt')) object.rtt = message.rtt;
            if (message.jitter != null && message.hasOwnProperty('jitter'))
                object.jitter = message.jitter;
            return object;
        };

        /**
         * Converts this StatDetails to JSON.
         * @function toJSON
         * @memberof nqi.StatDetails
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        StatDetails.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return StatDetails;
    })();

    nqi.NQIStat = (function () {
        /**
         * Properties of a NQIStat.
         * @memberof nqi
         * @interface INQIStat
         * @property {number|null} [nqi] NQIStat nqi
         * @property {number|null} [txNqi] NQIStat txNqi
         * @property {number|null} [rxNqi] NQIStat rxNqi
         * @property {Object.<string,nqi.IStatDetails>|null} [txStat] NQIStat txStat
         * @property {Object.<string,nqi.IStatDetails>|null} [rxStat] NQIStat rxStat
         */

        /**
         * Constructs a new NQIStat.
         * @memberof nqi
         * @classdesc Represents a NQIStat.
         * @implements INQIStat
         * @constructor
         * @param {nqi.INQIStat=} [properties] Properties to set
         */
        function NQIStat(properties) {
            this.txStat = {};
            this.rxStat = {};
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }

        /**
         * NQIStat nqi.
         * @member {number} nqi
         * @memberof nqi.NQIStat
         * @instance
         */
        NQIStat.prototype.nqi = 0;

        /**
         * NQIStat txNqi.
         * @member {number} txNqi
         * @memberof nqi.NQIStat
         * @instance
         */
        NQIStat.prototype.txNqi = 0;

        /**
         * NQIStat rxNqi.
         * @member {number} rxNqi
         * @memberof nqi.NQIStat
         * @instance
         */
        NQIStat.prototype.rxNqi = 0;

        /**
         * NQIStat txStat.
         * @member {Object.<string,nqi.IStatDetails>} txStat
         * @memberof nqi.NQIStat
         * @instance
         */
        NQIStat.prototype.txStat = $util.emptyObject;

        /**
         * NQIStat rxStat.
         * @member {Object.<string,nqi.IStatDetails>} rxStat
         * @memberof nqi.NQIStat
         * @instance
         */
        NQIStat.prototype.rxStat = $util.emptyObject;

        /**
         * Creates a new NQIStat instance using the specified properties.
         * @function create
         * @memberof nqi.NQIStat
         * @static
         * @param {nqi.INQIStat=} [properties] Properties to set
         * @returns {nqi.NQIStat} NQIStat instance
         */
        NQIStat.create = function create(properties) {
            return new NQIStat(properties);
        };

        /**
         * Encodes the specified NQIStat message. Does not implicitly {@link nqi.NQIStat.verify|verify} messages.
         * @function encode
         * @memberof nqi.NQIStat
         * @static
         * @param {nqi.INQIStat} message NQIStat message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        NQIStat.encode = function encode(message, writer) {
            if (!writer) writer = $Writer.create();
            if (message.nqi != null && Object.hasOwnProperty.call(message, 'nqi'))
                writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.nqi);
            if (message.txNqi != null && Object.hasOwnProperty.call(message, 'txNqi'))
                writer.uint32(/* id 2, wireType 0 =*/ 16).int32(message.txNqi);
            if (message.rxNqi != null && Object.hasOwnProperty.call(message, 'rxNqi'))
                writer.uint32(/* id 3, wireType 0 =*/ 24).int32(message.rxNqi);
            if (message.txStat != null && Object.hasOwnProperty.call(message, 'txStat'))
                for (var keys = Object.keys(message.txStat), i = 0; i < keys.length; ++i) {
                    writer
                        .uint32(/* id 4, wireType 2 =*/ 34)
                        .fork()
                        .uint32(/* id 1, wireType 2 =*/ 10)
                        .string(keys[i]);
                    $root.nqi.StatDetails.encode(
                        message.txStat[keys[i]],
                        writer.uint32(/* id 2, wireType 2 =*/ 18).fork()
                    )
                        .ldelim()
                        .ldelim();
                }
            if (message.rxStat != null && Object.hasOwnProperty.call(message, 'rxStat'))
                for (var keys = Object.keys(message.rxStat), i = 0; i < keys.length; ++i) {
                    writer
                        .uint32(/* id 5, wireType 2 =*/ 42)
                        .fork()
                        .uint32(/* id 1, wireType 2 =*/ 10)
                        .string(keys[i]);
                    $root.nqi.StatDetails.encode(
                        message.rxStat[keys[i]],
                        writer.uint32(/* id 2, wireType 2 =*/ 18).fork()
                    )
                        .ldelim()
                        .ldelim();
                }
            return writer;
        };

        /**
         * Encodes the specified NQIStat message, length delimited. Does not implicitly {@link nqi.NQIStat.verify|verify} messages.
         * @function encodeDelimited
         * @memberof nqi.NQIStat
         * @static
         * @param {nqi.INQIStat} message NQIStat message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        NQIStat.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a NQIStat message from the specified reader or buffer.
         * @function decode
         * @memberof nqi.NQIStat
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {nqi.NQIStat} NQIStat
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        NQIStat.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length,
                message = new $root.nqi.NQIStat(),
                key,
                value;
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                    case 1:
                        message.nqi = reader.int32();
                        break;
                    case 2:
                        message.txNqi = reader.int32();
                        break;
                    case 3:
                        message.rxNqi = reader.int32();
                        break;
                    case 4:
                        if (message.txStat === $util.emptyObject) message.txStat = {};
                        var end2 = reader.uint32() + reader.pos;
                        key = '';
                        value = null;
                        while (reader.pos < end2) {
                            var tag2 = reader.uint32();
                            switch (tag2 >>> 3) {
                                case 1:
                                    key = reader.string();
                                    break;
                                case 2:
                                    value = $root.nqi.StatDetails.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag2 & 7);
                                    break;
                            }
                        }
                        message.txStat[key] = value;
                        break;
                    case 5:
                        if (message.rxStat === $util.emptyObject) message.rxStat = {};
                        var end2 = reader.uint32() + reader.pos;
                        key = '';
                        value = null;
                        while (reader.pos < end2) {
                            var tag2 = reader.uint32();
                            switch (tag2 >>> 3) {
                                case 1:
                                    key = reader.string();
                                    break;
                                case 2:
                                    value = $root.nqi.StatDetails.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag2 & 7);
                                    break;
                            }
                        }
                        message.rxStat[key] = value;
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                }
            }
            return message;
        };

        /**
         * Decodes a NQIStat message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof nqi.NQIStat
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {nqi.NQIStat} NQIStat
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        NQIStat.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader)) reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a NQIStat message.
         * @function verify
         * @memberof nqi.NQIStat
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        NQIStat.verify = function verify(message) {
            if (typeof message !== 'object' || message === null) return 'object expected';
            if (message.nqi != null && message.hasOwnProperty('nqi'))
                if (!$util.isInteger(message.nqi)) return 'nqi: integer expected';
            if (message.txNqi != null && message.hasOwnProperty('txNqi'))
                if (!$util.isInteger(message.txNqi)) return 'txNqi: integer expected';
            if (message.rxNqi != null && message.hasOwnProperty('rxNqi'))
                if (!$util.isInteger(message.rxNqi)) return 'rxNqi: integer expected';
            if (message.txStat != null && message.hasOwnProperty('txStat')) {
                if (!$util.isObject(message.txStat)) return 'txStat: object expected';
                var key = Object.keys(message.txStat);
                for (var i = 0; i < key.length; ++i) {
                    var error = $root.nqi.StatDetails.verify(message.txStat[key[i]]);
                    if (error) return 'txStat.' + error;
                }
            }
            if (message.rxStat != null && message.hasOwnProperty('rxStat')) {
                if (!$util.isObject(message.rxStat)) return 'rxStat: object expected';
                var key = Object.keys(message.rxStat);
                for (var i = 0; i < key.length; ++i) {
                    var error = $root.nqi.StatDetails.verify(message.rxStat[key[i]]);
                    if (error) return 'rxStat.' + error;
                }
            }
            return null;
        };

        /**
         * Creates a NQIStat message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof nqi.NQIStat
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {nqi.NQIStat} NQIStat
         */
        NQIStat.fromObject = function fromObject(object) {
            if (object instanceof $root.nqi.NQIStat) return object;
            var message = new $root.nqi.NQIStat();
            if (object.nqi != null) message.nqi = object.nqi | 0;
            if (object.txNqi != null) message.txNqi = object.txNqi | 0;
            if (object.rxNqi != null) message.rxNqi = object.rxNqi | 0;
            if (object.txStat) {
                if (typeof object.txStat !== 'object')
                    throw TypeError('.nqi.NQIStat.txStat: object expected');
                message.txStat = {};
                for (var keys = Object.keys(object.txStat), i = 0; i < keys.length; ++i) {
                    if (typeof object.txStat[keys[i]] !== 'object')
                        throw TypeError('.nqi.NQIStat.txStat: object expected');
                    message.txStat[keys[i]] = $root.nqi.StatDetails.fromObject(
                        object.txStat[keys[i]]
                    );
                }
            }
            if (object.rxStat) {
                if (typeof object.rxStat !== 'object')
                    throw TypeError('.nqi.NQIStat.rxStat: object expected');
                message.rxStat = {};
                for (var keys = Object.keys(object.rxStat), i = 0; i < keys.length; ++i) {
                    if (typeof object.rxStat[keys[i]] !== 'object')
                        throw TypeError('.nqi.NQIStat.rxStat: object expected');
                    message.rxStat[keys[i]] = $root.nqi.StatDetails.fromObject(
                        object.rxStat[keys[i]]
                    );
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a NQIStat message. Also converts values to other types if specified.
         * @function toObject
         * @memberof nqi.NQIStat
         * @static
         * @param {nqi.NQIStat} message NQIStat
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        NQIStat.toObject = function toObject(message, options) {
            if (!options) options = {};
            var object = {};
            if (options.objects || options.defaults) {
                object.txStat = {};
                object.rxStat = {};
            }
            if (options.defaults) {
                object.nqi = 0;
                object.txNqi = 0;
                object.rxNqi = 0;
            }
            if (message.nqi != null && message.hasOwnProperty('nqi')) object.nqi = message.nqi;
            if (message.txNqi != null && message.hasOwnProperty('txNqi'))
                object.txNqi = message.txNqi;
            if (message.rxNqi != null && message.hasOwnProperty('rxNqi'))
                object.rxNqi = message.rxNqi;
            var keys2;
            if (message.txStat && (keys2 = Object.keys(message.txStat)).length) {
                object.txStat = {};
                for (var j = 0; j < keys2.length; ++j)
                    object.txStat[keys2[j]] = $root.nqi.StatDetails.toObject(
                        message.txStat[keys2[j]],
                        options
                    );
            }
            if (message.rxStat && (keys2 = Object.keys(message.rxStat)).length) {
                object.rxStat = {};
                for (var j = 0; j < keys2.length; ++j)
                    object.rxStat[keys2[j]] = $root.nqi.StatDetails.toObject(
                        message.rxStat[keys2[j]],
                        options
                    );
            }
            return object;
        };

        /**
         * Converts this NQIStat to JSON.
         * @function toJSON
         * @memberof nqi.NQIStat
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        NQIStat.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return NQIStat;
    })();

    nqi.NQIMessage = (function () {
        /**
         * Properties of a NQIMessage.
         * @memberof nqi
         * @interface INQIMessage
         * @property {nqi.NQIMessage.Command|null} [cmd] NQIMessage cmd
         * @property {string|null} [version] NQIMessage version
         * @property {number|null} [maxStat] NQIMessage maxStat
         * @property {Array.<string>|null} [pinned] NQIMessage pinned
         * @property {nqi.INQIStat|null} [own] NQIMessage own
         * @property {Object.<string,nqi.INQIStat>|null} [peers] NQIMessage peers
         */

        /**
         * Constructs a new NQIMessage.
         * @memberof nqi
         * @classdesc Represents a NQIMessage.
         * @implements INQIMessage
         * @constructor
         * @param {nqi.INQIMessage=} [properties] Properties to set
         */
        function NQIMessage(properties) {
            this.pinned = [];
            this.peers = {};
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
        }

        /**
         * NQIMessage cmd.
         * @member {nqi.NQIMessage.Command} cmd
         * @memberof nqi.NQIMessage
         * @instance
         */
        NQIMessage.prototype.cmd = 0;

        /**
         * NQIMessage version.
         * @member {string} version
         * @memberof nqi.NQIMessage
         * @instance
         */
        NQIMessage.prototype.version = '';

        /**
         * NQIMessage maxStat.
         * @member {number} maxStat
         * @memberof nqi.NQIMessage
         * @instance
         */
        NQIMessage.prototype.maxStat = 0;

        /**
         * NQIMessage pinned.
         * @member {Array.<string>} pinned
         * @memberof nqi.NQIMessage
         * @instance
         */
        NQIMessage.prototype.pinned = $util.emptyArray;

        /**
         * NQIMessage own.
         * @member {nqi.INQIStat|null|undefined} own
         * @memberof nqi.NQIMessage
         * @instance
         */
        NQIMessage.prototype.own = null;

        /**
         * NQIMessage peers.
         * @member {Object.<string,nqi.INQIStat>} peers
         * @memberof nqi.NQIMessage
         * @instance
         */
        NQIMessage.prototype.peers = $util.emptyObject;

        /**
         * Creates a new NQIMessage instance using the specified properties.
         * @function create
         * @memberof nqi.NQIMessage
         * @static
         * @param {nqi.INQIMessage=} [properties] Properties to set
         * @returns {nqi.NQIMessage} NQIMessage instance
         */
        NQIMessage.create = function create(properties) {
            return new NQIMessage(properties);
        };

        /**
         * Encodes the specified NQIMessage message. Does not implicitly {@link nqi.NQIMessage.verify|verify} messages.
         * @function encode
         * @memberof nqi.NQIMessage
         * @static
         * @param {nqi.INQIMessage} message NQIMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        NQIMessage.encode = function encode(message, writer) {
            if (!writer) writer = $Writer.create();
            if (message.cmd != null && Object.hasOwnProperty.call(message, 'cmd'))
                writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.cmd);
            if (message.version != null && Object.hasOwnProperty.call(message, 'version'))
                writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.version);
            if (message.maxStat != null && Object.hasOwnProperty.call(message, 'maxStat'))
                writer.uint32(/* id 3, wireType 0 =*/ 24).int32(message.maxStat);
            if (message.pinned != null && message.pinned.length)
                for (var i = 0; i < message.pinned.length; ++i)
                    writer.uint32(/* id 4, wireType 2 =*/ 34).string(message.pinned[i]);
            if (message.own != null && Object.hasOwnProperty.call(message, 'own'))
                $root.nqi.NQIStat.encode(
                    message.own,
                    writer.uint32(/* id 5, wireType 2 =*/ 42).fork()
                ).ldelim();
            if (message.peers != null && Object.hasOwnProperty.call(message, 'peers'))
                for (var keys = Object.keys(message.peers), i = 0; i < keys.length; ++i) {
                    writer
                        .uint32(/* id 6, wireType 2 =*/ 50)
                        .fork()
                        .uint32(/* id 1, wireType 2 =*/ 10)
                        .string(keys[i]);
                    $root.nqi.NQIStat.encode(
                        message.peers[keys[i]],
                        writer.uint32(/* id 2, wireType 2 =*/ 18).fork()
                    )
                        .ldelim()
                        .ldelim();
                }
            return writer;
        };

        /**
         * Encodes the specified NQIMessage message, length delimited. Does not implicitly {@link nqi.NQIMessage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof nqi.NQIMessage
         * @static
         * @param {nqi.INQIMessage} message NQIMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        NQIMessage.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a NQIMessage message from the specified reader or buffer.
         * @function decode
         * @memberof nqi.NQIMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {nqi.NQIMessage} NQIMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        NQIMessage.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length,
                message = new $root.nqi.NQIMessage(),
                key,
                value;
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                    case 1:
                        message.cmd = reader.int32();
                        break;
                    case 2:
                        message.version = reader.string();
                        break;
                    case 3:
                        message.maxStat = reader.int32();
                        break;
                    case 4:
                        if (!(message.pinned && message.pinned.length)) message.pinned = [];
                        message.pinned.push(reader.string());
                        break;
                    case 5:
                        message.own = $root.nqi.NQIStat.decode(reader, reader.uint32());
                        break;
                    case 6:
                        if (message.peers === $util.emptyObject) message.peers = {};
                        var end2 = reader.uint32() + reader.pos;
                        key = '';
                        value = null;
                        while (reader.pos < end2) {
                            var tag2 = reader.uint32();
                            switch (tag2 >>> 3) {
                                case 1:
                                    key = reader.string();
                                    break;
                                case 2:
                                    value = $root.nqi.NQIStat.decode(reader, reader.uint32());
                                    break;
                                default:
                                    reader.skipType(tag2 & 7);
                                    break;
                            }
                        }
                        message.peers[key] = value;
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                }
            }
            return message;
        };

        /**
         * Decodes a NQIMessage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof nqi.NQIMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {nqi.NQIMessage} NQIMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        NQIMessage.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader)) reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a NQIMessage message.
         * @function verify
         * @memberof nqi.NQIMessage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        NQIMessage.verify = function verify(message) {
            if (typeof message !== 'object' || message === null) return 'object expected';
            if (message.cmd != null && message.hasOwnProperty('cmd'))
                switch (message.cmd) {
                    default:
                        return 'cmd: enum value expected';
                    case 0:
                    case 1:
                        break;
                }
            if (message.version != null && message.hasOwnProperty('version'))
                if (!$util.isString(message.version)) return 'version: string expected';
            if (message.maxStat != null && message.hasOwnProperty('maxStat'))
                if (!$util.isInteger(message.maxStat)) return 'maxStat: integer expected';
            if (message.pinned != null && message.hasOwnProperty('pinned')) {
                if (!Array.isArray(message.pinned)) return 'pinned: array expected';
                for (var i = 0; i < message.pinned.length; ++i)
                    if (!$util.isString(message.pinned[i])) return 'pinned: string[] expected';
            }
            if (message.own != null && message.hasOwnProperty('own')) {
                var error = $root.nqi.NQIStat.verify(message.own);
                if (error) return 'own.' + error;
            }
            if (message.peers != null && message.hasOwnProperty('peers')) {
                if (!$util.isObject(message.peers)) return 'peers: object expected';
                var key = Object.keys(message.peers);
                for (var i = 0; i < key.length; ++i) {
                    var error = $root.nqi.NQIStat.verify(message.peers[key[i]]);
                    if (error) return 'peers.' + error;
                }
            }
            return null;
        };

        /**
         * Creates a NQIMessage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof nqi.NQIMessage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {nqi.NQIMessage} NQIMessage
         */
        NQIMessage.fromObject = function fromObject(object) {
            if (object instanceof $root.nqi.NQIMessage) return object;
            var message = new $root.nqi.NQIMessage();
            switch (object.cmd) {
                case 'SETUP':
                case 0:
                    message.cmd = 0;
                    break;
                case 'NQI_DATA':
                case 1:
                    message.cmd = 1;
                    break;
            }
            if (object.version != null) message.version = String(object.version);
            if (object.maxStat != null) message.maxStat = object.maxStat | 0;
            if (object.pinned) {
                if (!Array.isArray(object.pinned))
                    throw TypeError('.nqi.NQIMessage.pinned: array expected');
                message.pinned = [];
                for (var i = 0; i < object.pinned.length; ++i)
                    message.pinned[i] = String(object.pinned[i]);
            }
            if (object.own != null) {
                if (typeof object.own !== 'object')
                    throw TypeError('.nqi.NQIMessage.own: object expected');
                message.own = $root.nqi.NQIStat.fromObject(object.own);
            }
            if (object.peers) {
                if (typeof object.peers !== 'object')
                    throw TypeError('.nqi.NQIMessage.peers: object expected');
                message.peers = {};
                for (var keys = Object.keys(object.peers), i = 0; i < keys.length; ++i) {
                    if (typeof object.peers[keys[i]] !== 'object')
                        throw TypeError('.nqi.NQIMessage.peers: object expected');
                    message.peers[keys[i]] = $root.nqi.NQIStat.fromObject(object.peers[keys[i]]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a NQIMessage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof nqi.NQIMessage
         * @static
         * @param {nqi.NQIMessage} message NQIMessage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        NQIMessage.toObject = function toObject(message, options) {
            if (!options) options = {};
            var object = {};
            if (options.arrays || options.defaults) object.pinned = [];
            if (options.objects || options.defaults) object.peers = {};
            if (options.defaults) {
                object.cmd = options.enums === String ? 'SETUP' : 0;
                object.version = '';
                object.maxStat = 0;
                object.own = null;
            }
            if (message.cmd != null && message.hasOwnProperty('cmd'))
                object.cmd =
                    options.enums === String
                        ? $root.nqi.NQIMessage.Command[message.cmd]
                        : message.cmd;
            if (message.version != null && message.hasOwnProperty('version'))
                object.version = message.version;
            if (message.maxStat != null && message.hasOwnProperty('maxStat'))
                object.maxStat = message.maxStat;
            if (message.pinned && message.pinned.length) {
                object.pinned = [];
                for (var j = 0; j < message.pinned.length; ++j)
                    object.pinned[j] = message.pinned[j];
            }
            if (message.own != null && message.hasOwnProperty('own'))
                object.own = $root.nqi.NQIStat.toObject(message.own, options);
            var keys2;
            if (message.peers && (keys2 = Object.keys(message.peers)).length) {
                object.peers = {};
                for (var j = 0; j < keys2.length; ++j)
                    object.peers[keys2[j]] = $root.nqi.NQIStat.toObject(
                        message.peers[keys2[j]],
                        options
                    );
            }
            return object;
        };

        /**
         * Converts this NQIMessage to JSON.
         * @function toJSON
         * @memberof nqi.NQIMessage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        NQIMessage.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Command enum.
         * @name nqi.NQIMessage.Command
         * @enum {number}
         * @property {number} SETUP=0 SETUP value
         * @property {number} NQI_DATA=1 NQI_DATA value
         */
        NQIMessage.Command = (function () {
            var valuesById = {},
                values = Object.create(valuesById);
            values[(valuesById[0] = 'SETUP')] = 0;
            values[(valuesById[1] = 'NQI_DATA')] = 1;
            return values;
        })();

        return NQIMessage;
    })();

    return nqi;
})();

module.exports = $root;
