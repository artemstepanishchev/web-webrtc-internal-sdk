/**
 * @typedef {Object} Logger
 * @property {function} log
 * @property {function} error
 * @property {function} warn
 * @property {function} debug
 * @property {function} [groupCollapsed]
 * @property {function} [groupEnd]
 */

export default class LogWrapper {
    /**
     *
     * @param {Logger} logger
     * @param {function} getPrefix
     */
    constructor(logger, getPrefix) {
        this._getPrefix = getPrefix;
        this._logger = logger instanceof LogWrapper ? logger.getLogger() : logger;
        this.logCollapsed =
            typeof this._logger.groupCollapsed === 'function' &&
            typeof this._logger.groupEnd === 'function'
                ? this._logCollapsedNative
                : this._logCollapsedEmulated;

        if (localStorage.getItem('ls_realconsole')) {
            return getRealConsoleLogger(getPrefix);
        }
    }

    getLogger() {
        return this._logger;
    }

    get prefix() {
        return this._getPrefix() + ': ';
    }

    log(text) {
        this._logger.log(this.prefix + text);
    }

    _logCollapsedNative(text, label) {
        this._logger.groupCollapsed(this.prefix + label);
        this.log(text);
        this._logger.groupEnd();
    }

    _logCollapsedEmulated(text, label) {
        this.log(label);
        this.log(text);
    }

    logError(text) {
        this._logger.error(this.prefix + text);
    }

    logWarn(text) {
        this._logger.warn(this.prefix + text);
    }

    logDebug(text) {
        this._logger.debug(this.prefix + text);
    }
}

function getRealConsoleLogger(getPrefix) {
    const prefix = parseInt.bind(undefined, 1);
    prefix.toString = getPrefix;

    return {
        log: console.log.bind(console, '%s: ', prefix),
        logError: console.error.bind(console, '%s: ', prefix),
        logWarn: console.warn.bind(console, '%s: ', prefix),
        logDebug: console.debug.bind(console, '%s: ', prefix),
        logCollapsed: console.log.bind(console, '%s:\n', prefix),
    };
}
