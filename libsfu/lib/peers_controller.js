import EventEmitter from './events.js';
import BasePeerConnection from './base_peer_connection.js';
import MediaPeerConnection from './media_peer_connection.js';
import LogWrapper from './log_wrapper.js';
import ClientCapabilities from './client_capabilities.js';
import SDP from './sdp_cli.js';
import { getStunServersOnly } from './utils/peer_connection.js';
import { createSequence } from './utils/misc.js';
import PeerConnectionRoles from './utils/peer_connection_roles.js';
import { compareIceStatePriority, getPriorityIceState } from './utils/ice.js';

const seq = createSequence();

class PeersController extends EventEmitter {
    constructor(options) {
        super();
        this._options = options;
        this._seq = seq();
        this._isP2PSupported = options.isP2PSupported;
        this._pcs = [];
        this._logger = new LogWrapper(
            options.logger,
            () => `pcs_crl:${this._seq}-${this._sessionId}`
        );
        this._sessionId = options.sessionId;
        this._mainPC = null;
        this._closedCaptionsPC = null;
        this._controlChannel = options.controlChannel;
        this._transportCC = options.transportCC;
        this._blacklistedCodecs = options.blacklistedCodecs;
        this._codec = options.codec;
        this._codecForP2P = options.codecForP2P;
        this._blacklistedConsumers = {};
        this._closed = false;
        this._setSendBitrate = options.setSendBitrate;
        this._noMediaPingTimeout = options.noMediaPingTimeout;
        this._mixAudioMode = options.mixAudioMode;
        this._denoise = options.denoise;
        this._agc = options.agc;
        this._noiseGate = options.noiseGate;
        this._negotiateVideoMode = options.negotiateVideoMode;
        this._bitrateMultipliers = options.bitrateMultipliers || {};
        this._meetingId = options.meetingId;
        this._e2ee = options.e2ee;
        this._allowAudioRed = options.allowAudioRed || false;
        this._disableDSCP = options.disableDSCP;
    }

    setMixAudioMode(mode) {
        this._mixAudioMode = mode;
        this._mainPC?.setMixAudioMode(mode);
    }

    setDenoise(newValue) {
        this._denoise = newValue;
        this._mainPC?.setDenoise(newValue);
    }

    setAGC(newValue) {
        this._agc = newValue;
        this._mainPC?.setAGC(newValue);
    }

    setNoiseGate(newValue) {
        this._noiseGate = newValue;
        this._mainPC?.setNoiseGate(newValue);
    }

    setNegotiateVideoMode(mode) {
        this._negotiateVideoMode = mode;
        this._mainPC?.setNegotiateVideoMode(mode);
    }

    blacklistConsumer(consumerId) {
        this._blacklistedConsumers[consumerId] = true;
        this._logger.logWarn(`Consumer '${consumerId} blacklisted'`);
    }
    isConsumerBlacklisted(consumerId) {
        return this._blacklistedConsumers[consumerId];
    }
    getSessionId() {
        return this._sessionId;
    }
    addLocalStream({
        stream,
        useSimulcast,
        dedicatedConnection,
        codec,
        streamTapId,
        width,
        height,
        getDimensionsPromise,
    }) {
        const pcRole = dedicatedConnection
            ? PeersController.role.ADDITIONAL
            : PeersController.role.MAIN;

        let pc;
        if (dedicatedConnection) {
            pc = this.getSFUPC(pcRole, streamTapId);
            if (!pc) {
                pc = this._createMediaPC({ role: pcRole, streamTapId });
            }
        } else {
            pc = this.getMainPC();
        }
        pc.addLocalStream({
            stream,
            useSimulcast,
            codec,
            streamTapId,
            width,
            height,
            getDimensionsPromise,
        });
    }
    getAllP2PPCsForStream(streamTapId) {
        return this._getMediaPCs().filter(pc => pc.getStreamTapId() === streamTapId && pc.isP2P());
    }
    removeLocalStream(stream) {
        const pcs = this.getPCSByLocalStream(stream);
        if (pcs.length > 0) {
            pcs.forEach(pc => pc.removeLocalStream(stream));
        } else {
            this._logger.logError('Cannot remove non-existing stream id=' + stream.id);
        }
    }

    removeLocalStreamTrackSenders(stream, track) {
        const pcs = this.getPCSByLocalStream(stream);
        if (pcs.length > 0) {
            pcs.forEach(pc => pc.removeLocalTracksSenders([track]));
        } else {
            this._logger.logError('Cannot remove track for non-existing stream id=' + stream.id);
        }
    }

    close() {
        this._logger.log('Close peer controller');
        this._closed = true;
        this._pcs.forEach(pc => pc.clearLock());
        //We need to clone array here, because while
        //we be closing peer connections this._pcs will be changing
        //At the same pcsToClose will remain the same
        //So we can iterate over it
        const pcsToClose = this._pcs.slice(0);
        pcsToClose.forEach(pc => {
            pc.close();
        });
        // TODO: remove?
        // these links to connections should clear themselves
        // in the corresponding listener of PC close event
        this._mainPC = null;
        this._closedCaptionsPC = null;
        this.removeAllListeners();
    }
    isClosed() {
        return this._closed;
    }
    initMainPC() {
        if (!this._mainPC) {
            this.getMainPC();
        }
    }
    forEachPC(handler, desc) {
        return new Promise((resolve, reject) => {
            let inProgress = 0;
            let finished = false;
            this._pcs.forEach((pc, index) => {
                if (pc.isClosed()) {
                    return;
                }
                inProgress++;
                pc.getLock(desc)
                    .then(releaseLock => {
                        const pcSeq = pc.getSeq();
                        this._logger.log(`pc:${pcSeq} lock taken`);
                        handler(pc, index).then(
                            () => {
                                this._logger.log(`pc:${pcSeq} lock released`);
                                releaseLock();
                                inProgress--;
                                if (finished && inProgress === 0) {
                                    resolve();
                                }
                            },
                            err => {
                                this._logger.logError(`pc:${pcSeq} lock error`);
                                this._logger.logError(err.stack);
                                releaseLock(err);
                                reject(err);
                            }
                        );
                    })
                    .catch(err => {
                        this._logger.logError('Failed to get lock on peer connection');
                        reject(err);
                    });
            });
            finished = true;
        });
    }
    setLocalDescription(descriptions) {
        return Promise.all(
            descriptions.map(description =>
                this._getPCByLocalDescription(description.sdp).setLocalDescription(description)
            )
        );
    }
    isSignalingStable() {
        return !this._pcs.find(pc => !pc.isSignalingStable() && !pc.isP2P());
    }
    _waitPCWithSessionId(pcId, timeout) {
        return new Promise((resolve, reject) => {
            const pc = this._getPCByRemoteSDPSessionId(pcId);
            let timer;
            const self = this;
            if (pc) {
                resolve(pc);
            } else {
                timer = setTimeout(() => {
                    this.removeListener(
                        PeersController.event.NEW_REMOTE_SESSION_ID,
                        newSessionIdHandler
                    );
                    const errMessage = `Cannot find PC with session id=${pcId}`;
                    this._logger.logError(errMessage);
                    reject(new Error(errMessage));
                }, timeout);

                this.on(PeersController.event.NEW_REMOTE_SESSION_ID, newSessionIdHandler);
            }

            function newSessionIdHandler(newPC) {
                if (newPC.getRemoteSDPSessionId() === pcId) {
                    self._logger.log(`Found PC with session id=${pcId} seq=${newPC.getSeq()}`);
                    clearTimeout(timer);
                    self.removeListener(
                        PeersController.event.NEW_REMOTE_SESSION_ID,
                        newSessionIdHandler
                    );
                    resolve(newPC);
                }
            }
        });
    }
    addIceCandidate(pcId, candidate) {
        return Promise.resolve()
            .then(() => {
                return typeof pcId === 'undefined'
                    ? this.getMainPC()
                    : this._waitPCWithSessionId(pcId, 1000);
            })
            .then(pc => {
                return pc
                    .getLock('Add ICE Candidate')
                    .then(releaseLock =>
                        pc.addIceCandidate(candidate).then(releaseLock, releaseLock)
                    );
            });
    }
    getVideoBandwidthEstimation() {
        return this._getMediaPCs().map(pc => pc.getVideoBandwidthEstimation() || {});
    }
    getStatForRemoteStream(streamTapId) {
        const pc = this._getMediaPCs().find(pc => pc.hasRemoteStreamTapId(streamTapId));
        return pc && pc.getStatForRemoteStreamByTapId(streamTapId);
    }
    getStatForLocalStream(streamId) {
        const pc = this._getPCByStreamId(streamId);
        return (pc && pc.getStatForLocalStream(streamId)) || { video: {}, audio: {} };
    }
    updateStats(callback) {
        return Promise.all(this._getMediaPCs().map(pc => pc.updateStats())).then(
            () => callback && callback()
        );
    }
    getICEstate() {
        return (
            this._pcs
                .filter(pc => !pc.isP2P())
                .map(pc => pc.getICEstate())
                .sort(compareIceStatePriority)[0] || BasePeerConnection.iceState.NONE
        );
    }
    _getPCByLocalAndRemoteSessionId(localSDPSessionId, remoteSDPSessionId) {
        return this._pcs.find(
            pc =>
                pc.getLocalSDPSessionId() === localSDPSessionId &&
                pc.getRemoteSDPSessionId() === remoteSDPSessionId
        );
    }
    _getPCByLocalSDPSessionId(sdpSessionId) {
        return this._pcs.find(pc => pc.getLocalSDPSessionId() === sdpSessionId);
    }
    _getPCByRemoteSDPSessionId(sdpSessionId) {
        return this._pcs.find(pc => pc.getRemoteSDPSessionId() === sdpSessionId);
    }
    getPCByRemoteDescription(description) {
        let match = description.match(/a=rcv-pc-role:\s*([^ \n\r]+)/);
        let pc;
        let role;
        let remoteParty;
        let remoteSDPSessionId;
        let localSDPSessionId;
        if (match) {
            role = match[1];
            match = description.match(/a=rcv-local-party:\s*([^ \n\r]+)/);
            if (match) {
                remoteParty = match[1];
            }
            if (role === PeersController.role.MAIN && !remoteParty) {
                pc = this.getMainPC();
            } else if (role === PeersController.role.ADDITIONAL || remoteParty) {
                remoteSDPSessionId = SDP.getSessionId(description);

                match = description.match(/a=rcv-remote-session:([^ \n\r]+)/);
                if (match) {
                    localSDPSessionId = match[1];
                }
                if (localSDPSessionId) {
                    if (remoteSDPSessionId) {
                        pc = this._getPCByLocalAndRemoteSessionId(
                            localSDPSessionId,
                            remoteSDPSessionId
                        );
                    }
                    if (!pc) {
                        this._logger.logError(`Cannot find session with id=${localSDPSessionId}`);
                    }
                } else {
                    if (remoteSDPSessionId) {
                        pc = this._getPCByRemoteSDPSessionId(remoteSDPSessionId);
                    }
                    if (!pc) {
                        pc = this._createMediaPC({ role, remoteParty });
                        if (remoteSDPSessionId) {
                            pc.setRemoteSDPSessionId(remoteSDPSessionId);
                        }
                    }
                }
            } else {
                this._logger.logError(`Unknown peer connection role ${role}`);
            }
        } else {
            pc = this.getMainPC();
        }
        return {
            pc: pc,
            remoteParty: remoteParty,
            remoteSessionId: remoteSDPSessionId,
            localSessionId: localSDPSessionId,
            role: role,
        };
    }

    getPCByChannelLabel(label) {
        // TODO: move cc channel creation into cc connection in case of dedicated control channel?
        if (ClientCapabilities.dedicatedControlChannels) {
            return this.getMainPC();
        }

        switch (label) {
            case MediaPeerConnection.dataChannelLabel.CLOSED_CAPTIONS:
                return this.getClosedCaptionsPC();
            default:
                return this.getMainPC();
        }
    }

    _getPCByLocalDescription(description) {
        const match = description.match(/o=[^ ]+ ([^ ]+)/);
        let pc;
        if (match) {
            const localSDPSessionId = match[1];
            pc = this._getPCByLocalSDPSessionId(localSDPSessionId);
            if (!pc) {
                throw new Error(`Cannot find session with id=${localSDPSessionId}`);
            }
        } else {
            throw new Error('Cannot find o= in description');
        }
        return pc;
    }

    getMainPC() {
        if (this._closed) {
            return;
        }
        if (!this._mainPC) {
            this._mainPC = this._createMediaPC({ role: PeersController.role.MAIN });
            if (this._mainPC) {
                this._mainPC.on(MediaPeerConnection.event.BEFORE_CLOSE, () => {
                    this._mainPC = null;
                });
                this.emit(PeersController.event.NEW_MAIN_PC, this._mainPC);
            }
        }
        return this._mainPC;
    }

    isMainConnectionEstablished() {
        return (
            this.getMainPC()?.getPeerConnection()?.connectionState ===
            BasePeerConnection.connectionState.CONNECTED
        );
    }

    resetRemoteStream(remoteStreamTapId) {
        if (this._mainPC) {
            this._mainPC.resetRemoteStream(remoteStreamTapId);
            return true;
        } else {
            return false;
        }
    }
    getPCByRemoteStream(remoteStream) {
        return this._getMediaPCs().find(pc => pc.hasRemoteStream(remoteStream));
    }
    getRemoteStreamIdByStream(remoteStream) {
        const pc = this.getPCByRemoteStream(remoteStream);
        if (pc) {
            return pc.getRemoteStreamIdByStream(remoteStream);
        } else {
            this._logger.logWarn(`Stream id not found for stream ${remoteStream.id}`);
            this._logger.logWarn(remoteStream);
            return remoteStream.id;
        }
    }
    _getPCByStreamId(streamId) {
        return this._getMediaPCs().find(pc => pc.hasLocalStreamId(streamId));
    }
    getPCSByLocalStream(stream) {
        return this._getMediaPCs().filter(pc => pc.hasLocalStream(stream));
    }

    /**
     * @param {{ role: string, remoteParty?: string }} opts
     * @returns {Object}
     */
    _getDefaultPCOptions({ role, remoteParty }) {
        const isP2P = Boolean(remoteParty);
        // is it still relevant or this._controlChannel is enough?
        const isControlChannelUsed =
            isP2P || (!isP2P && this._isP2PSupported) || this._controlChannel;
        const { iceServers } = this._options;

        return {
            role,
            remoteParty,
            sessionId: this._sessionId,
            iceServers: isP2P ? getStunServersOnly(iceServers) : iceServers,
            isP2PSupported: this._isP2PSupported,
            preferUnifiedPlan: this._options.preferUnifiedPlan,
            controlChannel: isControlChannelUsed,
            dedicatedControlChannels: isControlChannelUsed && role === PeersController.role.MAIN,
            noMediaPingTimeout: this._noMediaPingTimeout,
            logger: this._logger,
            maxFramerate: this._maxFramerate,
            meetingId: this._meetingId,
            e2ee: this._e2ee,
        };
    }

    /**
     * @param {{ role: string, remoteParty?: string, streamTapId?: string }} opts
     * @returns {BasePeerConnection}
     */
    _createBasePC(opts) {
        if (this._closed) return null;

        const pcsNum = this._pcs.length;
        this._logger.logWarn(
            `Create base PC: ${JSON.stringify(opts)}\nNum of pcs: ${pcsNum} -> ${pcsNum + 1}`
        );

        const isP2P = !!opts.remoteParty;
        const pc = new BasePeerConnection({ ...this._getDefaultPCOptions(opts), ...opts });

        this._addPCBaseListeners(pc);

        // Now this works with the base connection only,
        // because in case of media connection the local_stream is responsible for negotiation start
        pc.once(BasePeerConnection.event.NEGOTIATION_NEEDED, () => {
            this.emit(BasePeerConnection.event.NEGOTIATION_NEEDED, pc);
        });

        this._pcs.push(pc);

        this.emit(PeersController.event.ICE_STATE_CHANGED, this.getICEstate());

        if (isP2P) {
            this.emit(PeersController.event.NEW_P2P_CONNECTION, pc);
        }

        return pc;
    }

    /**
     * @param {{ role: string, remoteParty?: string, streamTapId?: string }} opts
     * @returns {MediaPeerConnection}
     */
    _createMediaPC(opts) {
        if (this._closed) return null;

        const pcsNum = this._pcs.length;
        this._logger.logWarn(
            `Create media PC: ${JSON.stringify(opts)}.\nNum of pcs: ${pcsNum} -> ${pcsNum + 1}`
        );

        const isMainRole = opts.role === PeersController.role.MAIN;
        const isP2P = Boolean(opts.remoteParty);

        const pc = new MediaPeerConnection({
            ...this._getDefaultPCOptions(opts),
            transportCC: this._transportCC,
            codec: isP2P ? this._codecForP2P : this._codec,
            blacklistedCodecs: this._blacklistedCodecs,
            audioRate: isMainRole ? this._options.audioRate : this._options.audioRateAdditional,
            audioBitrate: isMainRole
                ? this._options.audioBitrate
                : this._options.audioBitrateAdditional,
            setSendBitrate: this._setSendBitrate,
            mixAudioMode: this._mixAudioMode,
            denoise: this._denoise && opts.role === PeersController.role.MAIN,
            agc: this._agc && opts.role === PeersController.role.MAIN,
            noiseGate: this._noiseGate && opts.role === PeersController.role.MAIN,
            negotiateVideoMode: this._negotiateVideoMode,
            bitrateMultipliers: this._bitrateMultipliers,
            enableQ1HalfFps: this._options.enableQ1HalfFps,
            allowAudioRed: this._allowAudioRed,
            disableDSCP: this._disableDSCP,
            ...opts,
        });

        this._addPCBaseListeners(pc);

        if (isMainRole && !isP2P) {
            this._addPCStatsListeners(pc);
        }

        this._addPCMediaListeners(pc);

        this._pcs.push(pc);

        this.emit(PeersController.event.ICE_STATE_CHANGED, this.getICEstate());

        if (isP2P) {
            this.emit(PeersController.event.NEW_P2P_CONNECTION, pc);
        }

        return pc;
    }

    getClosedCaptionsPC() {
        if (!this._closedCaptionsPC) {
            this._closedCaptionsPC = this._createBasePC({
                role: PeersController.role.ADDITIONAL,
                controlChannel: false,
            });

            this._closedCaptionsPC?.on(BasePeerConnection.event.BEFORE_CLOSE, () => {
                this._closedCaptionsPC = null;
            });
        }

        return this._closedCaptionsPC;
    }

    getSFUPC(role, streamTapId) {
        return this._pcs.find(
            pc =>
                !pc.getRemoteParty() &&
                pc.getRole() === role &&
                pc.supportsMediaStreams() &&
                pc.getStreamTapId() === streamTapId
        );
    }
    getP2PPC(role, remoteParty, streamTapId) {
        return this._pcs.find(
            pc =>
                pc.getRemoteParty() === remoteParty &&
                pc.getRole() === role &&
                pc.supportsMediaStreams() &&
                pc.getStreamTapId() === streamTapId
        );
    }

    createP2PPC(role, remoteParty, streamTapId) {
        return this._createMediaPC({ role, remoteParty, streamTapId });
    }
    _destroyPC(pc) {
        const index = this._pcs.indexOf(pc);
        if (index > -1) {
            this._pcs.splice(index, 1);
        }
    }

    getLocalStreams() {
        return this._getMediaPCs().flatMap(pc => pc.getLocalStreams());
    }

    getRemoteStreamByStreamId(streamId) {
        return this._getMediaPCs()
            .map(pc => pc.getRemoteStreamByStreamId(streamId))
            .find(Boolean);
    }

    isAnyP2PPC() {
        return !!this._pcs.find(pc => pc.isP2P());
    }
    getAllP2PPCs() {
        return this._pcs.filter(pc => pc.isP2P());
    }
    getAllPCs() {
        return this._pcs;
    }

    canPCExistWithoutStreams(pc) {
        const isControlChannelUsedToKeepConnection =
            pc.isControlChannelUsed() && (pc.isP2P() || pc.getRole() === PeersController.role.MAIN);

        return isControlChannelUsedToKeepConnection || !pc.supportsMediaStreams();
    }

    _getMediaPCs() {
        return this._pcs.filter(pc => pc.supportsMediaStreams());
    }

    maybeClosePCWithoutChannels(pc) {
        if (pc && (!pc.supportsMediaStreams() || !this._pcs.includes(pc))) {
            this._logger.log(
                `Closing pc without channels (remote session id: ${pc.getRemoteSDPSessionId()})`
            );
            pc.close();
        }
    }

    // *************************** LISTENER SETS *************************** //

    _addPCBaseListeners(pc) {
        pc.on(BasePeerConnection.event.NEW_REMOTE_SESSION_ID, newRemoteSessionId => {
            this._logger.log(`New Remote Session Id=${newRemoteSessionId}`);
            this.emit(PeersController.event.NEW_REMOTE_SESSION_ID, pc);
        });

        pc.on(BasePeerConnection.event.LOCAL_CANDIDATE, candidate => {
            this.emit(PeersController.event.LOCAL_CANDIDATE, pc, candidate);
        });

        pc.on(BasePeerConnection.event.END_OF_LOCAL_CANDIDATES, () => {
            this.emit(PeersController.event.END_OF_LOCAL_CANDIDATES, pc);
        });

        pc.on(BasePeerConnection.event.NO_INCOMING_DATA_TIMEOUT, timeSinceLastData => {
            this.emit(PeersController.event.NO_INCOMING_DATA_TIMEOUT, timeSinceLastData);
        });

        pc.on(BasePeerConnection.event.CONTROL_CHANNEL_CLOSED_UNEXPECTEDLY, () => {
            if (
                pc.getRole() === PeersController.role.ADDITIONAL &&
                pc.supportsMediaStreams() &&
                pc.getLocalStreams()?.length === 0
            ) {
                // but this is fine for additional pc where stream already removed
                return this._logger.log('Control channel closed before additional connection');
            }

            this._logger.logError('Control channel closed while connection is not.');
            this.emit(PeersController.event.CONTROL_CHANNEL_CLOSED_UNEXPECTEDLY, pc);
        });

        pc.once(BasePeerConnection.event.BEFORE_CLOSE, () => {
            this.emit(PeersController.event.BEFORE_CLOSE, pc);
            this._destroyPC(pc);
        });

        if (!pc.isP2P()) {
            pc.on(BasePeerConnection.event.ICE_STATE_CHANGED, newIceState => {
                this.emit(
                    PeersController.event.ICE_STATE_CHANGED,
                    getPriorityIceState(newIceState, this.getICEstate())
                );
            });
        }
    }

    _addPCMediaListeners(pc) {
        pc.on(MediaPeerConnection.event.REMOTE_STREAM_ADDED, (stream, streamId) => {
            this.emit(PeersController.event.REMOTE_STREAM_ADDED, pc, stream, streamId);
        });

        pc.on(MediaPeerConnection.event.REMOTE_STREAM_BEFORE_REMOVE, (stream, streamId) => {
            this.emit(PeersController.event.REMOTE_STREAM_BEFORE_REMOVE, pc, stream, streamId);
        });

        pc.on(MediaPeerConnection.event.REMOTE_STREAM_REMOVED, (stream, streamId) => {
            this.emit(PeersController.event.REMOTE_STREAM_REMOVED, pc, stream, streamId);
        });

        if (!pc.isP2P()) {
            pc.on(MediaPeerConnection.event.LOCAL_STREAM_ADDED, (stream, id, index) => {
                this.emit(PeersController.event.LOCAL_STREAM_ADDED, pc, stream, id, index);
            });

            pc.on(MediaPeerConnection.event.LOCAL_STREAM_REMOVED, (stream, id, index) => {
                this.emit(PeersController.event.LOCAL_STREAM_REMOVED, pc, stream, id, index);
            });
        }

        pc.once(MediaPeerConnection.event.BEFORE_CLOSE, () => {
            if (pc.isP2P()) {
                pc.removeAllRemoteStreams();
            }
        });
    }

    _addPCStatsListeners(pc) {
        pc.on(MediaPeerConnection.event.SFU_STAT_RECEIVED, stat => {
            this.emit(PeersController.event.SFU_STAT_RECEIVED, pc, stat);
        });

        pc.on(MediaPeerConnection.event.CONSUMERS_UPDATED, consumers => {
            this.emit(PeersController.event.CONSUMERS_UPDATED, consumers);
        });

        pc.on(MediaPeerConnection.event.MIX_AUDIO_MODE_STATE_CHANGED, mixAudioModeEnabled => {
            this.emit(PeersController.event.MIX_AUDIO_MODE_STATE_CHANGED, mixAudioModeEnabled);
        });

        pc.on(
            MediaPeerConnection.event.NEGOTIATE_VIDEO_MODE_STATE_CHANGED,
            negotiateVideoModeEnabled => {
                this.emit(
                    MediaPeerConnection.event.NEGOTIATE_VIDEO_MODE_STATE_CHANGED,
                    negotiateVideoModeEnabled
                );
            }
        );

        pc.on(MediaPeerConnection.event.NETWORK_TYPE_CHANGED, networkType => {
            this.emit(PeersController.event.NETWORK_TYPE_CHANGED, networkType);
        });
    }
}

PeersController.event = {
    ...BasePeerConnection.event,
    ...MediaPeerConnection.event,
    NEW_P2P_CONNECTION: 'new_p2p_connection',
    NEW_MAIN_PC: 'new_main_pc',
};

PeersController.role = PeerConnectionRoles;

PeersController.iceState = BasePeerConnection.iceState;
PeersController.mixAudioMode = MediaPeerConnection.mixAudioMode;
PeersController.negotiateVideoMode = MediaPeerConnection.negotiateVideoMode;

export default PeersController;
