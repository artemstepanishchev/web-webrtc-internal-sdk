import EventEmitter from './events.js';
import SDP from './sdp_cli.js';
import Lock from './lock.js';
import ControlChannel from './control_channel';
import ClientCapabilities from './client_capabilities.js';
import LogWrapper from './log_wrapper.js';
import { rejectErr, makePromiseAbortable, delay } from './utils/async.js';
import { getAreTransceiversSupported, stringifyPCState } from './utils/peer_connection.js';
import { createSequence } from './utils/misc.js';
import { SDP_SEMANTICS, getSdpSemantics } from './utils/sdp.js';
import { iceState } from './utils/ice.js';

const seq = createSequence();
const RTCPeerConnection =
    window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;

const PC_CLOSING_TIMEOUT = 10000;
const FAILED_ICE_STATE_DELAY = 3000;

export default class BasePeerConnection extends EventEmitter {
    constructor(options = {}) {
        super();
        this._init(options);
    }

    _init(options) {
        this._options = options;
        this._e2ee = options.e2ee || {};

        this._seq = seq();

        this._unifiedPlan = undefined;
        this._planB = undefined;

        this._transceiversSupported = false;

        this._sessionId = options.sessionId;

        this._remoteSDP = undefined;

        this._confirmedDeliveredSDP = undefined;

        this._pendingLocalDescription = undefined;

        this._logger = new LogWrapper(
            options.logger,
            () =>
                `${options.prefix || 'pc:base'}:${this._seq}-${this._sessionId}${
                    this._remoteParty ? '-' + this._remoteParty : ''
                }`
        );

        this._sdpSessionId = undefined;

        this._role = options.role;

        this._remoteParty = options.remoteParty;

        this._lock = new Lock({
            timeout: 10000,
            logger: this._logger,
            sessionId: this._sessionId,
        });

        this._isP2PSupported = options.isP2PSupported;

        this._abortController = new AbortController();

        this._wasConnected = false;

        this._closed = false;

        this._controlChannel = null;
        this._dedicatedControlChannels = null;

        this._noMediaPingTimeout = options.noMediaPingTimeout;

        this._iceFailedAbortController = null;

        this._peerConnection = this._createPeerConnection();

        this._ensureControlChannel();
        this._ensureDedicatedControlChannels();
    }

    // *********************************************************************** //

    // ******************************* PRIVATE ******************************* //
    // *********************************************************************** //

    _createPeerConnection(opts) {
        const rtcOptions = {
            iceServers: this._options.iceServers,
            sdpSemantics: getSdpSemantics(this._options.preferUnifiedPlan),
            ...opts,
        };

        const pc = new RTCPeerConnection(rtcOptions, {
            optional: this.OPTIONAL_CONSTRAINTS,
        });

        this._transceiversSupported = getAreTransceiversSupported(rtcOptions);

        this._unifiedPlan = rtcOptions.sdpSemantics === SDP_SEMANTICS.UNIFIED_PLAN;
        this._planB = !this._unifiedPlan;

        this.emit(BasePeerConnection.event.ICE_STATE_CHANGED, pc.iceConnectionState);

        pc.onicecandidate = event => {
            this._logger.log('pc.onicecandidate');
            if (event.candidate) {
                if (event.candidate.candidate.match(/fe80|tcptype/)) {
                    return;
                }
                this.emit(BasePeerConnection.event.LOCAL_CANDIDATE, event.candidate);
            } else {
                this._logger.log('End of candidates.');
                this.emit(BasePeerConnection.event.END_OF_LOCAL_CANDIDATES);
            }
        };
        pc.onconnecting = () => this._logger.log('pc.onconnecting');
        pc.onopen = () => this._logger.log('pc.onopen');
        pc.ondatachannel = e => this._logger.log('pc.ondatachannel', e);
        pc.onidentityresult = () => this._logger.log('pc.onidentityresult', pc.peerIdentity);
        pc.onnegotiationneeded = () => {
            this._logger.log('pc.onnegotiationneeded ' + stringifyPCState(pc));
            this.emit(BasePeerConnection.event.NEGOTIATION_NEEDED);
        };
        pc.oniceconnectionstatechange = () => {
            this._logger.log('pc.oniceconnectionstatechange ' + stringifyPCState(pc));
            if (pc.iceConnectionState === BasePeerConnection.iceState.CONNECTED) {
                this._onConnected();
            }

            if (this._unifiedPlan) {
                this._useSimulatedIceFailedEvent(pc);
            }

            if (!this._closed) {
                this.emit(BasePeerConnection.event.ICE_STATE_CHANGED, pc.iceConnectionState);
            }
        };
        pc.onsignalingstatechange = () =>
            this._logger.log('pc.onsignalingstatechange ' + stringifyPCState(pc));

        return pc;
    }

    _onConnected() {
        if (!this._wasConnected) {
            this._wasConnected = true;
            this.emit(BasePeerConnection.event.CONNECTED);
        }
        this._ensureControlChannel();
    }

    /**
     * If the real ICE state is disconnected for some time, emits FAILED ICE state.
     * https://docs.google.com/document/d/1-ZfikoUtoJa9k-GZG1daN0BU3IjIanQ_JSscHxQesvU/edit#heading=h.xy9xt6bk24e
     */
    _useSimulatedIceFailedEvent(pc) {
        if (pc.iceConnectionState !== BasePeerConnection.iceState.DISCONNECTED) {
            this._logger.log('Abort timeout for simulated ICE failed state');
            this._iceFailedAbortController?.abort();
            this._iceFailedAbortController = null;
            return;
        }

        if (this._iceFailedAbortController || this._closed) return;

        this._iceFailedAbortController = new AbortController();

        makePromiseAbortable(
            makePromiseAbortable(
                delay(FAILED_ICE_STATE_DELAY),
                this._iceFailedAbortController.signal
            ),
            this._abortController.signal
        ).then(() => {
            this._logger.log(
                `Emitting simulated ICE failed state after ${FAILED_ICE_STATE_DELAY} of disconnection`
            );
            this.emit(
                BasePeerConnection.event.ICE_STATE_CHANGED,
                BasePeerConnection.iceState.FAILED
            );
        });
    }

    // Unfortunately createOffer/createAnswer can never resolve or reject
    // in case peer connection being close during the operation
    // So, we should manually reject in case we close PC during create offer/answer
    _safeCreateOffer(constrains) {
        if (this._closed) {
            return rejectErr(`Cannot create offer. ${stringifyPCState(this._peerConnection)}`);
        }

        return makePromiseAbortable(
            this._peerConnection.createOffer(constrains),
            this._abortController.signal
        );
    }

    _safeCreateAnswer() {
        if (this._closed) {
            return rejectErr(`Cannot create answer. ${stringifyPCState(this._peerConnection)}`);
        }

        return makePromiseAbortable(
            this._peerConnection.createAnswer(),
            this._abortController.signal
        );
    }

    _setLocalDescriptionBeforeRemote(remoteDescription) {
        return Promise.resolve().then(() => {
            if (remoteDescription.type !== 'answer') return null;

            if (!this._pendingLocalDescription) {
                const errDescription = 'Unexpected answer';
                this._logger.logError(errDescription);
                this._logger.logError(remoteDescription.sdp);
                return rejectErr(errDescription);
            }

            return this.setLocalDescription(this._pendingLocalDescription)
                .then(() => {
                    this.cancelPendingLocalDescription();
                })
                .catch(err => {
                    this._logger.logError('Failed description(local):');
                    this._logger.logError(SDP.prettyPrint(this._pendingLocalDescription.sdp));
                    this.cancelPendingLocalDescription();
                    const errDescription = `Failed to set local description: ${err}`;
                    this._logger.logError(errDescription);
                    return rejectErr(errDescription);
                });
        });
    }

    _isConnectedSDP(sdp) {
        return sdp.getMedia().some(m => m.media === SDP.mediaType.DATA);
    }

    // *************************** CUSTOMIZING SDP *************************** //

    _mangleLocalDescription(description, isAnswer, prevDescription) {
        const localSDP = new SDP(description.sdp);
        this._addCustomSDPAttributes(localSDP, isAnswer, prevDescription);

        description.sdp = localSDP.toString();
        return description;
    }

    _addCustomSDPAttributes(localSDP) {
        if (this._remoteSDP) {
            localSDP.setAttribute('rcv-remote-session', this.getRemoteSDPSessionId());
        }
        localSDP.setAttribute('rcv-pc-role', this._role);

        if (this.isP2P()) {
            localSDP.setAttribute('rcv-remote-party', this.getRemoteParty());
            localSDP.setAttribute('rcv-local-party', this._sessionId);
        } else if (this._isP2PSupported) {
            localSDP.setAttribute('rcv-p2p');
        }
    }

    // *************************** CONTROL CHANNEL *************************** //

    _ensureControlChannel() {
        if (this.isControlChannelUsed() && this._peerConnection && !this._controlChannel) {
            this._controlChannel = new ControlChannel({
                pc: this,
                label: BasePeerConnection.dataChannelLabel.CONTROL,
                logger: this._logger,
                noMediaPingTimeout: this._noMediaPingTimeout,
                onDataTimeout: timeSinceLastDataReceived => {
                    this.emit(
                        BasePeerConnection.event.NO_INCOMING_DATA_TIMEOUT,
                        timeSinceLastDataReceived
                    );
                },
                onClose: () => {
                    this._logger.log('Control channel closed');
                    if (this._closed || !this.isConnected()) return;
                    this.emit(BasePeerConnection.event.CONTROL_CHANNEL_CLOSED_UNEXPECTEDLY);
                },
            });
        }
    }

    // TODO: move to FirefoxPeerConnection or just remove?
    _ensureDedicatedControlChannels() {
        if (
            ClientCapabilities.dedicatedControlChannels &&
            this._options.dedicatedControlChannels &&
            !this._dedicatedControlChannels
        ) {
            const pc = this._peerConnection;
            this._dedicatedControlChannels = [
                pc.createDataChannel(BasePeerConnection.dataChannelLabel.ANNOTATIONS, null),
                pc.createDataChannel(BasePeerConnection.dataChannelLabel.CLOSED_CAPTIONS, null),
                pc.createDataChannel(BasePeerConnection.dataChannelLabel.NQI, null),
            ];
        }
    }

    // *********************************************************************** //

    // ******************************* PUBLIC ******************************** //
    // *********************************************************************** //

    getSeq() {
        return this._seq;
    }

    isP2P() {
        return Boolean(this._remoteParty);
    }

    isClosed() {
        return this._closed;
    }

    isSignalingStable() {
        return !this._pendingLocalDescription;
    }

    isConnected() {
        const st = BasePeerConnection.iceState;
        const iceState = this.getICEstate();

        return (
            [st.COMPLETED, st.CONNECTED].indexOf(iceState) > -1 ||
            (iceState === st.CHECKING && this._wasConnected)
        );
    }

    isControlChannelUsed() {
        return this._options.controlChannel;
    }

    isTransceiversUsed() {
        return this._transceiversSupported;
    }

    isLocked() {
        return this._lock.isLocked();
    }

    isMaster() {
        return this.isP2P() && this._sessionId < this._remoteParty;
    }

    hasRemoteSDP() {
        return !!this._remoteSDP;
    }

    supportsMediaStreams() {
        return false;
    }

    getPeerConnection() {
        return this._peerConnection;
    }

    getState() {
        return (this._peerConnection && this._peerConnection.signalingState) || 'none';
    }

    getICEstate() {
        return this._peerConnection
            ? this._peerConnection.iceConnectionState
            : BasePeerConnection.iceState.NONE;
    }

    getLock(desc) {
        return this._lock.getLock(desc);
    }

    getRole() {
        return this._role;
    }

    getRemoteParty() {
        return this._remoteParty;
    }

    getDataChannelByLabel(label) {
        return this._dedicatedControlChannels?.find(ch => ch.label === label);
    }

    getLocalSDPSessionId() {
        return this._sdpSessionId;
    }

    getRemoteSDPSessionId() {
        return this._remoteSDPSessionId;
    }

    setRemoteDescription(description) {
        if (!this._peerConnection) {
            return rejectErr(BasePeerConnection.error.PC_DOESNT_EXIST);
        }

        //Mangle remote SDP
        //Force OPUS parameters set on client
        const mangledSDP = description.sdp;
        const descriptionInit = { sdp: mangledSDP, type: description.type };

        return Promise.resolve()
            .then(() => this._setLocalDescriptionBeforeRemote(descriptionInit))
            .then(() => {
                this._remoteSDP = new SDP(descriptionInit.sdp);
                this.setRemoteSDPSessionId(this._remoteSDP.getSession().origin.sessionId);

                if (!this._isConnectedSDP(this._remoteSDP)) {
                    return this.close();
                }
                if (!this._peerConnection) {
                    return rejectErr(BasePeerConnection.error.PC_DOESNT_EXIST);
                }

                return this._peerConnection.setRemoteDescription(descriptionInit);
            });
    }

    setRemoteSDPSessionId(newRemoteSDPSessionId) {
        if (newRemoteSDPSessionId !== this._remoteSDPSessionId) {
            this._remoteSDPSessionId = newRemoteSDPSessionId;
            this.emit(BasePeerConnection.event.NEW_REMOTE_SESSION_ID, newRemoteSDPSessionId);
        }
    }

    setLocalDescription(description) {
        return this._peerConnection.signalingState !==
            BasePeerConnection.signalingState.HAVE_LOCAL_OFFER
            ? this._peerConnection.setLocalDescription(description)
            : Promise.resolve();
    }

    rollbackOffer() {
        if (
            this._peerConnection.signalingState ===
                BasePeerConnection.signalingState.HAVE_LOCAL_OFFER &&
            ClientCapabilities.rollbackRTCDescription
        ) {
            return this._peerConnection.setLocalDescription({ type: 'rollback', sdp: '' });
        }
        return Promise.resolve();
    }

    createOffer(rtcOfferOptions = {}) {
        let flow = Promise.resolve()
            .then(() => {
                if (!this._peerConnection) {
                    return rejectErr(BasePeerConnection.error.CANNOT_CREATE_OFFER);
                }
            })
            .then(() => this._safeCreateOffer(rtcOfferOptions))
            .then(description => {
                const isP2PText = this.isP2P() ? '(p2p)' : '';
                this._logger.logCollapsed(
                    SDP.prettyPrint(description.sdp),
                    `original offer SDP${isP2PText}`
                );

                this._sdpSessionId = SDP.getSessionId(description.sdp);

                description = this._mangleLocalDescription(
                    description,
                    false,
                    this._peerConnection.localDescription
                );

                this._logger.logCollapsed(
                    SDP.prettyPrint(description.sdp),
                    `amended offer SDP${isP2PText}`
                );

                this._pendingLocalDescription = description;

                return description;
            })
            .catch(err => {
                const errDescription = `Failed to create offer: ${err.message}`;
                this._logger.logError(errDescription);
                this._logger.logError(err.stack);
                throw new Error(errDescription);
            });

        if (this._transceiversSupported) {
            flow = flow
                .then(() => {
                    if (ClientCapabilities.rollbackRTCDescription) {
                        //We should set local description only
                        //if browser supports SDP rollback in case of SDP Glare
                        return this._peerConnection.setLocalDescription(
                            this._pendingLocalDescription
                        );
                    }
                })
                .then(() => this._pendingLocalDescription)
                .catch(err => {
                    this._logger.logError('Failed to set pending local description:');
                    this._logger.logError(err);
                    return Promise.reject(err);
                });
        }

        return flow;
    }

    createAnswer() {
        if (!this._peerConnection) {
            return rejectErr('Cannot create answer, because peer connection is closed.');
        }
        const prevLocalDescription = this._peerConnection.localDescription;

        return this._safeCreateAnswer().then(localDescription => {
            this._sdpSessionId = SDP.getSessionId(localDescription.sdp);
            this._logger.logCollapsed(
                SDP.prettyPrint(localDescription.sdp),
                'original answer SDP:'
            );
            localDescription = this._mangleLocalDescription(
                localDescription,
                true,
                prevLocalDescription
            );
            this._logger.logCollapsed(SDP.prettyPrint(localDescription.sdp), 'amended answer SDP:');
            return localDescription;
        });
    }

    addIceCandidate(candidate) {
        if (this._closed) {
            return Promise.resolve();
        }
        return this._peerConnection.addIceCandidate(candidate);
    }

    isSDPDelivered(fullSDPAsString) {
        return Boolean(
            this._confirmedDeliveredSDP &&
                SDP.areSDPsEqual(this._confirmedDeliveredSDP, fullSDPAsString)
        );
    }

    minifyLocalSDP(fullSDPAsString) {
        if (!this.isSDPDelivered(fullSDPAsString)) return fullSDPAsString;

        let minifiedSDP = 'v=0\r\n' + fullSDPAsString.match(/^o=.*/m)[0] + '\r\na=rcv-minified\r\n';
        ['rcv-pc-role', 'rcv-remote-session'].forEach(attribute => {
            const match = fullSDPAsString.match(new RegExp(`^a=${attribute}.*`, 'm'));
            if (match) {
                minifiedSDP += match[0] + '\r\n';
            }
        });
        return minifiedSDP;
    }

    confirmSDPDelivered(sdp) {
        this._confirmedDeliveredSDP = sdp;
    }

    createDataChannel(id) {
        return this._peerConnection.createDataChannel(id);
    }

    cancelPendingLocalDescription() {
        this._pendingLocalDescription = undefined;
    }

    reset() {
        this._remoteSDP = undefined;
    }

    clearLock() {
        this._lock.clear();
    }

    close() {
        if (!this._closed) {
            this.emit(BasePeerConnection.event.BEFORE_CLOSE);
            this._closed = true;
            if (this._peerConnection) {
                this._logger.logWarn('Closing peer connection...');
                const pc = this._peerConnection;
                //We should not synchronously close PeerConnection, because
                //it could be called from event generated by peer connection (e.g. ice disconnected)
                //In this case Chrome tab stuck on pc.close() line (probably https://bugs.chromium.org/p/webrtc/issues/detail?id=430)
                setTimeout(() => {
                    pc.close();
                    this._logger.log(`PC closed after timeout: ${PC_CLOSING_TIMEOUT}`);
                }, PC_CLOSING_TIMEOUT);

                this._logger.log(`Timeout to close pc set: ${PC_CLOSING_TIMEOUT}`);

                this._abortController.abort();
            }
            delete this._peerConnection;
            this.emit(BasePeerConnection.event.CLOSED);
            this.removeAllListeners();
        }
        this._logger.logDebug('Exit pc.close()');
    }

    get OPTIONAL_CONSTRAINTS() {
        return [{ googDscp: this._options.disableDSCP ? false : true }];
    }
}

BasePeerConnection.event = {
    LOCAL_CANDIDATE: 'local_candidate',
    END_OF_LOCAL_CANDIDATES: 'end_of_candidates',
    ICE_STATE_CHANGED: 'nice_state_changed',
    CLOSED: 'closed',
    BEFORE_CLOSE: 'before_close',
    NEW_REMOTE_SESSION_ID: 'new_remote_session_id',
    CONNECTED: 'connected',
    NEGOTIATION_NEEDED: 'negotiation_needed',
    NO_INCOMING_DATA_TIMEOUT: 'no_incoming_data_timeout',
};

// none + real ice states
BasePeerConnection.iceState = iceState;

BasePeerConnection.signalingState = {
    HAVE_LOCAL_OFFER: 'have-local-offer',
    HAVE_REMOTE_OFFER: 'have-remote-offer',
    STABLE: 'stable',
};

BasePeerConnection.connectionState = {
    CONNECTED: 'connected',
};

BasePeerConnection.dataChannelLabel = {
    CONTROL: 'controlChannel',
    ANNOTATIONS: 'annotations',
    CLOSED_CAPTIONS: 'cc',
    NQI: 'nqi',
};

BasePeerConnection.dataChannelId = {
    NQI: 'nqi',
};

BasePeerConnection.error = {
    PC_DOESNT_EXIST:
        "Peer connection doesn't exists. Probably media was disconnected on the client side.",
    CANNOT_CREATE_OFFER: 'Cannot create offer, because peer connection closed',
};
