import BasePeerConnection from './base_peer_connection.js';
import SDP, { SdpProcessor } from './sdp_cli.js';
import Consumer from './consumer.js';
import ClientCapabilities from './client_capabilities.js';
import { getStatsRegister } from './peer_connection_stats/index.js';
import {
    getCodecsComparator,
    getFreeCodecId,
    isCodecAllowed,
    isCodecSupportedBySFU,
    getCodecsPriority,
    areCodecsEqual,
    CODECS,
} from './utils/codecs.js';
import { getControllablePromise, makePromiseAbortable, rejectErr } from './utils/async.js';
import { getStreamsBitrates } from './utils/peer_connection.js';
import {
    fixPlanBMSIDs,
    initializeSimulcast,
    fillSsrcPair,
    modifySDPMedia,
    getSdpVideoTracksInfo,
} from './utils/simulcast.js';
import { stringifyPlainObj } from './utils/misc.js';
import { SFrameClient } from './sframe/sframe_client';
import { getSenderByTrack, setSenderBitrate } from './utils/encodings.js';
import PeerConnectionRoles from './utils/peer_connection_roles.js';

function dropTransportCcExtFromAudio(sdp) {
    const audioMedia = sdp.getMedia('audio')?.[0];
    if (!audioMedia) return false;

    sdp.removeRTPExtensionFromMedia(audioMedia, SDP.rtpExtension.TRANSPORT_CC);

    return true;
}

/**
 * @typedef {object} LocalStreamInfo
 * @property {MediaStream} stream
 * @property {boolean} useSimulcast
 * @property {string} id
 * @property {string} tapId
 * @property {string} codec
 * @property {number} width
 * @property {number} height
 * @property {Promise} getDimensionPromise
 */

class MediaPeerConnection extends BasePeerConnection {
    constructor(options = {}) {
        super({ ...options, prefix: options.prefix || 'pc:media' });
    }

    _init(options) {
        this._e2ee = options.e2ee || {};
        this._setSendBitrate = options.setSendBitrate !== false;

        this._denoise = options.denoise || false;

        this._agc = options.agc || false;

        this._noiseGate = options.noiseGate || false;

        this._mixAudioMode = options.e2ee.enabled
            ? MediaPeerConnection.mixAudioMode.NEVER
            : options.mixAudioMode;

        this._negotiateVideoMode =
            options.negotiateVideoMode || MediaPeerConnection.negotiateVideoMode.ALWAYS;
        this._mixAudioModeEnabled = undefined;
        this._negotiateVideoModeEnabled = undefined;
        this._meetingId = options.meetingId;

        // streams
        this._streamTapId = options.streamTapId;
        /**
         * @type {LocalStreamInfo[]}
         */
        this._localStreams = [];
        this._pendingAddLocalStreams = [];
        this._remoteStreams = [];
        this._remoteStreamsMap = {};
        this._pendingResetRemoteStream = [];
        this._videoSSRCbyStreamId = {};
        this._audioSSRCbyStreamId = {};
        this._videoSSRCbyLocalStreamId = {};
        this._audioSSRCbyLocalStreamId = {};

        // this is to add and remove some insignificant RTP extension on every remote SDP update in order to reset
        // internal Chrome state that causes video freeze with our SFU
        this._fakeResetExtState = false;

        this._transportCC = options.transportCC !== false;

        this._supportedCodecs = undefined;
        this._codec = options.codec || SDP.codec.VP8;
        this._allowAudioRed =
            options.allowAudioRed &&
            this._canEnableAudioRed() &&
            // Audio RED allowed only for "main" media peer connection at the moment - RCV-38759.
            options.role === PeerConnectionRoles.MAIN;
        this._audioRedEnabled = false;

        this._audioRate = options.audioRate;
        this._audioBitrate = options.audioBitrate;
        this._isP2PSupported = options.isP2PSupported;

        this._blacklistedCodecs = options.blacklistedCodecs || [];

        const bitrates = getStreamsBitrates(options.bitrateMultipliers || {});
        this._bitratePerSimulcastTrack = bitrates.simulcastTrack;
        this._bitratePerSimulcastSSH264 = bitrates.simulcastSSH264;
        this._bitratePerSimulcastSSVP8 = bitrates.simulcastSSVP8;
        this._bitratePerSimulcastSSVP8Tab = bitrates.simulcastSSVP8Tab;
        this._bitrateNonSimulcastStream = bitrates.nonSimulcastStream;

        this._waitLocalStreamsAdded = Promise.resolve();

        this._updateCodecsPriority();

        this._msidMapping = {};
        this._msidRemapping = null;

        this._maxBitrateByVideoTrackId = {};
        this._controlConnected = null;

        super._init(options);
        if (ClientCapabilities.incorrectPlanBMSIDs) {
            this._peerConnection.addTransceiver('audio');
            this._peerConnection.addTransceiver('video');
        }
        this._ensureControlChannel();

        this._initStatsRegister();
    }

    _initStatsRegister() {
        const onNetworkTypeChange = networkType =>
            this.emit(MediaPeerConnection.event.NETWORK_TYPE_CHANGED, networkType);

        this._statsRegister = getStatsRegister({ onNetworkTypeChange });
    }

    supportsMediaStreams() {
        return true;
    }

    setMixAudioMode(mode) {
        this._mixAudioMode = mode;
    }

    setDenoise(newValue) {
        this._denoise = newValue;
    }

    setAGC(newValue) {
        this._agc = newValue;
    }

    setNoiseGate(newValue) {
        this._noiseGate = newValue;
    }

    setNegotiateVideoMode(mode) {
        this._negotiateVideoMode = mode;
    }

    confirmSDPDelivered(sdp) {
        this._confirmedDeliveredSDP = sdp;
    }

    isAudioRedEnabled() {
        return this._audioRedEnabled;
    }

    _updateCodecsPriority() {
        let codecsByPriority = getCodecsPriority(this._codec);

        // Put Audio RED on the top if need.
        if (this._allowAudioRed) {
            codecsByPriority = [
                CODECS.RED_AUDIO,
                ...codecsByPriority.filter(codec => codec !== CODECS.RED_AUDIO),
            ];

            this._audioRedEnabled = true;
        }

        this._codecsComparator = getCodecsComparator(codecsByPriority);
    }

    getStreamTapId() {
        return this._streamTapId;
    }

    setStreamTapId(newTapId) {
        this._streamTapId = newTapId;
    }

    hasLocalStream(targetStream) {
        return !!this._localStreams.find(streamInfo => streamInfo.stream === targetStream);
    }

    hasLocalStreamId(targetStreamId) {
        return !!this._localStreams.find(streamInfo => streamInfo.id === targetStreamId);
    }

    _canEnableAudioRed() {
        // TODO Audio RED is temporary disabled for e2ee because mThor 21.3.10 has no capability to decode RED and will cause audio issue.
        return ClientCapabilities.isAudioRedSupported && !this._e2ee.enabled;
    }

    close() {
        super.close();
        delete this._localStreams;
        delete this._remoteStreamsMap;
        delete this._remoteStreams;
    }

    isClosed() {
        return this._closed;
    }

    _getFrameRateConstraint(track) {
        const { frameRate } = track.getSettings();
        if (!frameRate) return 0;

        return typeof frameRate === 'number'
            ? frameRate
            : frameRate.ideal || frameRate.exact || frameRate.max || 0;
    }

    _reduceMainVideoQ1FrameRate(encodings, track) {
        const q1 = encodings[ClientCapabilities.encodingsQ3toQ1 ? encodings.length - 1 : 0];
        const baseFrameRate = this._getFrameRateConstraint(track);
        const frameRate = baseFrameRate >= 20 ? Math.round(baseFrameRate / 2) : baseFrameRate;

        if (frameRate > 0) {
            this._logger.log(`Set Q1 ${stringifyPlainObj({ frameRate, baseFrameRate })}`);
            // chrome supports maxFramerate since m83
            // it is safe to set maxFramerate, even if it is not supported - it will be ignored
            q1.maxFramerate = frameRate;
        }
    }

    addLocalStream({
        stream,
        useSimulcast,
        codec,
        streamTapId,
        width,
        height,
        getDimensionsPromise,
    }) {
        this._localStreams.push({
            stream: stream,
            useSimulcast: useSimulcast,
            id: stream.id,
            tapId: streamTapId,
            codec: codec,
            width,
            height,
            getDimensionsPromise,
        });
        if (
            this._localStreams.length === 1 &&
            typeof codec !== 'undefined' &&
            this._codec !== codec
        ) {
            this._codec = codec;
            this._updateCodecsPriority();
        }
        if (this._transceiversSupported) {
            const transceivers = this._peerConnection.getTransceivers();
            stream.getTracks().forEach(track => {
                const freeTransceiver = transceivers.find(
                    tr =>
                        !tr.stopped &&
                        tr.direction.indexOf('send') !== 0 &&
                        tr.receiver.track.kind === track.kind
                );
                if (freeTransceiver) {
                    freeTransceiver.direction = 'sendrecv';
                    this._waitLocalStreamsAdded = this._waitLocalStreamsAdded.then(() => {
                        freeTransceiver.sender.replaceTrack(track);
                        freeTransceiver.sender.setStreams(stream);
                        this._frameClient?.setupEncryption(freeTransceiver.sender, codec);
                    });
                } else {
                    try {
                        const transceiver = this._peerConnection.addTransceiver(track, {
                            streams: [stream],
                            sendEncodings: [
                                {
                                    networkPriority: 'high',
                                },
                            ],
                        });
                        this._frameClient?.setupEncryption(transceiver.sender, codec);
                    } catch (err) {
                        this._logger.logWarn(err);
                    }
                }
            });
        } else if (typeof this._peerConnection.addTrack === 'function') {
            stream.getTracks().forEach(track => {
                try {
                    if (ClientCapabilities.incorrectPlanBMSIDs) {
                        this._peerConnection.addTransceiver(track, {
                            streams: [stream],
                            sendEncodings: [
                                {
                                    networkPriority: 'high',
                                },
                            ],
                        });
                        this._msidMapping[track.id] = stream.id;
                    } else {
                        const sender = this._peerConnection.addTrack(track, stream);
                        this._frameClient?.setupEncryption(sender, codec);
                    }
                } catch (err) {
                    //Track could already exists in PeerConnection
                    //Happens when switching SFU->P2P->SFU too fast
                    this._logger.logWarn(err.message);
                }
            });
        } else {
            //Deprecated
            if (stream.getTracks().length > 0) {
                this._peerConnection.addStream(stream);
            }
        }
        this._pendingAddLocalStreams.push(stream.id);
    }

    isLocalStreamAdded(streamId) {
        return (
            this.hasLocalStreamId(streamId) &&
            !(this._pendingAddLocalStreams.indexOf(streamId) > -1)
        );
    }

    removeAllLocalStreams() {
        if (this._closed) {
            return;
        }
        this._localStreams.forEach(localStream => this.removeLocalStream(localStream.stream));
    }

    removeAllRemoteStreams() {
        this._remoteStreams.forEach(remoteStream => {
            this.emit(
                MediaPeerConnection.event.REMOTE_STREAM_REMOVED,
                remoteStream,
                remoteStream.id
            );
        });
        this._remoteStreams = [];
    }

    removeLocalTracksSenders(tracks) {
        this._peerConnection.getSenders().forEach(sender => {
            if (tracks.includes(sender.track)) {
                // TODO: should switch transceiver to the recvonly mode?
                this._peerConnection.removeTrack(sender);
            }
        });
    }

    removeLocalStream(streamToRemove) {
        let removedIndex = -1;
        this._localStreams = this._localStreams.filter((stream, index) => {
            if (stream.id === streamToRemove.id) {
                removedIndex = index;
                return false;
            } else {
                return true;
            }
        });
        this._pendingAddLocalStreams = this._pendingAddLocalStreams.filter(
            streamId => streamId !== streamToRemove.id
        );
        if (removedIndex === -1) {
            return;
        }

        if (this._peerConnection.signalingState !== 'closed') {
            if (typeof this._peerConnection.removeTrack === 'function') {
                const tracksToRemove = streamToRemove.getTracks();
                this.removeLocalTracksSenders(tracksToRemove);
            } else {
                this._peerConnection.removeStream(streamToRemove);
            }
        }
        this.emit(
            MediaPeerConnection.event.LOCAL_STREAM_REMOVED,
            streamToRemove,
            streamToRemove.id,
            removedIndex
        );
    }

    getLocalStreams() {
        return this._localStreams;
    }

    updateStats(callback) {
        if (!callback) {
            return new Promise(resolve => {
                this.updateStats(resolve);
            });
        }

        if (!this._peerConnection) {
            return callback();
        }

        this._statsRegister
            .updateStats({
                pc: this._peerConnection,
                remoteSDP: this._remoteSDP,
                isStreamRemote: this._isStreamRemote,
                getRemoteVideoSSRCByStream: this._getRemoteVideoSSRCByStream,
                getLocalStreamsStreamsWithSSRCs: this._getLocalStreamsStreamsWithSSRCs,
            })
            .then(callback)
            .catch(err => {
                this._logger.logError(err.message);
                callback();
            });
    }

    _getRemoteVideoSSRCByStream = stream => {
        const remoteStreamId = Object.keys(this._remoteStreamsMap).find(
            streamId => this._remoteStreamsMap[streamId].stream === stream
        );
        return this._videoSSRCbyStreamId[remoteStreamId];
    };

    _isStreamRemote = stream => this._remoteStreams.some(s => s === stream);

    _getLocalStreamsStreamsWithSSRCs = () =>
        this._localStreams.map(stream => [stream, this._videoSSRCbyLocalStreamId[stream.id]]);

    getVideoBandwidthEstimation() {
        return this._statsRegister.getVideoBandwidthEstimation();
    }

    _getRemoteStreamIdByStreamTapId(streamTapId) {
        for (const remoteStreamId in this._remoteStreamsMap) {
            if (this._remoteStreamsMap.hasOwnProperty(remoteStreamId)) {
                if (this._remoteStreamsMap[remoteStreamId].id === streamTapId) {
                    return remoteStreamId;
                }
            }
        }
    }

    getRemoteStreamIdByStream(remoteStream) {
        if (this._transceiversSupported) {
            for (const remoteStreamId in this._remoteStreamsMap) {
                if (this._remoteStreamsMap.hasOwnProperty(remoteStreamId)) {
                    if (this._remoteStreamsMap[remoteStreamId].stream === remoteStream) {
                        return remoteStreamId;
                    }
                }
            }
        }
        return remoteStream.id;
    }

    hasRemoteStreamTapId(streamTapId) {
        const remoteStreamId = this._getRemoteStreamIdByStreamTapId(streamTapId);
        return !!(
            this._videoSSRCbyStreamId[remoteStreamId] || this._audioSSRCbyStreamId[remoteStreamId]
        );
    }

    hasRemoteStream(stream) {
        if (this._closed) {
            return false;
        }
        return this._remoteStreams.indexOf(stream) > -1;
    }

    getMaxBitrateByLocalVideoTrack(videoTrack) {
        return this._maxBitrateByVideoTrackId[videoTrack.id];
    }

    _setSenderQuality(track, maxQuality, isScreenSharing, codec) {
        const peerConnection = this._peerConnection;
        if (!ClientCapabilities.hasSenderParameters || !this._setSendBitrate) {
            return;
        }
        const videoSender = getSenderByTrack(peerConnection, track);
        if (!videoSender) {
            this._logger.logWarn('setSenderQuality unable to find video sender');
            return;
        }

        const parameters = videoSender.getParameters();
        const encodings = parameters.encodings;
        if (encodings.length === 0) {
            return;
        }
        const isSimulcastStream = encodings.length > 1;
        const isSingleEncodingStream = encodings.length === 1;

        let bitratePerSimulcast = this._bitratePerSimulcastTrack;
        if (isScreenSharing) {
            // displaySurface shall be supported in chrome since m71
            // see: https://developer.mozilla.org/en-US/docs/Web/API/MediaTrackSettings
            const surface = track.getSettings?.().displaySurface;
            switch (codec) {
                case SDP.codec.VP8:
                    bitratePerSimulcast = this._bitratePerSimulcastSSVP8;
                    if (ClientCapabilities.chromeCompatible && surface === 'browser') {
                        // use custom bitrate setup (1000 Kbps for Q1) for vp8 for chrome tab, see RCV-22141
                        bitratePerSimulcast = this._bitratePerSimulcastSSVP8Tab;
                    }
                    break;
                case SDP.codec.H264:
                    bitratePerSimulcast = this._bitratePerSimulcastSSH264;
                    break;
                default:
                    bitratePerSimulcast = this._bitratePerSimulcastSSVP8;
            }
        }

        let maxBitrate = 0;
        if (isSimulcastStream) {
            const { enableQ1HalfFps } = this._options;
            encodings.forEach((encoding, index) => {
                //Chrome starts encodings with Q1, Firefox starts with Q3
                const normalizedIndex = ClientCapabilities.encodingsQ3toQ1
                    ? encodings.length - index - 1
                    : index;
                encoding.maxBitrate = bitratePerSimulcast[normalizedIndex];
                const encodingQuality = normalizedIndex + 1;
                encoding.active = maxQuality >= encodingQuality && encoding.maxBitrate > 0;

                if (encoding.active) {
                    maxBitrate += encoding.maxBitrate;
                }
            });
            if (enableQ1HalfFps && !isScreenSharing) {
                this._reduceMainVideoQ1FrameRate(encodings, track);
            }
        } else if (isSingleEncodingStream) {
            const encoding = encodings[0];
            encoding.maxBitrate = this._bitrateNonSimulcastStream;
            encoding.active = maxQuality > 0;
            if (encoding.active) {
                maxBitrate += encoding.maxBitrate;
            }
        } else {
            return;
        }
        if (typeof videoSender.setParameters === 'function') {
            videoSender
                .setParameters(parameters)
                .then(() => {
                    this._maxBitrateByVideoTrackId[track.id] = maxBitrate;
                })
                .catch(err => {
                    this._logger.logError(err);
                });
        }
    }

    setSenderQuality(track, maxQuality, isScreenSharing, codec) {
        this._ensureConnected()
            .then(() => this._setSenderQuality(track, maxQuality, isScreenSharing, codec))
            .catch(err => {
                this._logger.logWarn(`Cannot set sender quality, because: ${err.message}`);
            });
    }

    _ensureConnected() {
        if (this.isClosed()) {
            return rejectErr('Peer connection closed');
        }
        if (this.isConnected()) {
            return Promise.resolve();
        }

        if (!this._controlConnected) {
            this._controlConnected = getControllablePromise();
        }

        return makePromiseAbortable(
            this._controlConnected.promise,
            this._abortController.signal
        ).finally(() => {
            this._controlConnected = null;
        });
    }

    _updateRemoteStreamIdToSSRCMapping() {
        this._videoSSRCbyStreamId = {};
        this._audioSSRCbyStreamId = {};
        const media = this._remoteSDP.getMedia();
        for (let i = 0; i < media.length; i++) {
            const m = media[i];
            const type = m.media;
            const ssrcByStreamId =
                type === 'video' ? this._videoSSRCbyStreamId : this._audioSSRCbyStreamId;
            const secondarySSRCs = [];

            this._remoteSDP.getMediaAttribute(m, 'ssrc-group').forEach(line => {
                const match = line.match(/(FID|FEC-FR) [^ ]+ ([^ ]+)/);
                if (match) {
                    secondarySSRCs.push(match[2]);
                }
            });
            if (this._unifiedPlan) {
                let streamId;
                if (this._transceiversSupported) {
                    const mid = this._remoteSDP.getFirstMediaAttribute(m, 'mid');
                    for (const remoteStreamId in this._remoteStreamsMap) {
                        if (this._remoteStreamsMap.hasOwnProperty(remoteStreamId)) {
                            const remoteStreamInfo = this._remoteStreamsMap[remoteStreamId];
                            if (
                                Array.isArray(remoteStreamInfo.mids) &&
                                remoteStreamInfo.mids.includes(mid)
                            ) {
                                streamId = remoteStreamId;
                                break;
                            }
                        }
                    }
                }
                if (!streamId) {
                    const streamIdLine = this._remoteSDP.getFirstMediaAttribute(m, 'msid');
                    if (streamIdLine) {
                        streamId = (streamIdLine.match(/^([^ ]+) /) || [])[1];
                    }
                }

                if (streamId) {
                    const ssrcs = this._remoteSDP.getMediaAttribute(m, 'ssrc');
                    for (let j = 0; j < ssrcs.length; j++) {
                        let ssrc = ssrcs[j];
                        if (ssrc && !secondarySSRCs.includes(ssrc)) {
                            ssrc = ssrc.split(' ', 1)[0];
                            ssrcByStreamId[streamId] = ssrc;
                            break;
                        }
                    }
                }
            } else if (this._planB) {
                let streamId;

                const ssrcs = this._remoteSDP.getMediaAttribute(m, 'ssrc');
                for (let j = 0; j < ssrcs.length; j++) {
                    const ssrcLine = ssrcs[j];
                    const match = ssrcLine.match(/^([^ ]+) msid:([^ \r\n]+)/);
                    if (match) {
                        const ssrc = match[1];
                        if (secondarySSRCs.indexOf(ssrc) < 0) {
                            streamId = match[2];
                            ssrcByStreamId[streamId] = ssrc;
                        }
                    }
                }
            } else {
                this._logger.logError('Both this._unifiedPlan and this._planB are false');
            }
        }
    }

    _createPeerConnection() {
        let pc;
        if (this._e2ee.enabled) {
            pc = super._createPeerConnection({ encodedInsertableStreams: true });
            const config = {
                peerConnection: pc,
                meetingId: this._meetingId,
                participantId: this._sessionId,
                logger: this._logger,
                getMlsSdkClient: this._e2ee.getMlsSdkClient,
            };
            this._frameClient = new SFrameClient(config);
        } else {
            pc = super._createPeerConnection();
        }

        //Deprecated
        pc.onremovestream = event => {
            const streamToRemove = event.stream;
            this._logger.log('pc.onremovestream ' + streamToRemove.id);
            this._removeRemoteStream(streamToRemove);
        };
        pc.ontrack = e => {
            this._logger.log('pc.ontrack', e);
            const streams = e.streams;
            if (streams.length > 0) {
                const stream = e.streams[0];
                this._addRemoteStream(stream);
            }
            this._frameClient?.setupDecryption(e.receiver);
        };

        const handleConnectionStateChange = () => {
            if (pc.connectionState !== 'connecting') {
                this._statsRegister.markConnectionPassed();
                pc.removeEventListener('connectionstatechange', handleConnectionStateChange);
            }
        };

        pc.addEventListener('connectionstatechange', handleConnectionStateChange);

        return pc;
    }

    _onConnected() {
        super._onConnected();
        if (!this._wasConnected) {
            this._controlConnected?.resolve();
        }
    }

    _addRemoteStream(stream, streamToAddId = stream.id) {
        if (this._remoteStreams.indexOf(stream) < 0) {
            this._remoteStreams.push(stream);
            this.emit(MediaPeerConnection.event.REMOTE_STREAM_ADDED, stream, streamToAddId);
        }
    }

    _removeRemoteStream(streamToRemove, streamToRemoveId = streamToRemove.id) {
        if (
            Array.isArray(this._remoteStreams) &&
            this._remoteStreams.indexOf(streamToRemove) > -1
        ) {
            this._ensureControlChannel();
            this._remoteStreams = this._remoteStreams.filter(stream => streamToRemove !== stream);
            delete this._remoteStreamsMap[streamToRemoveId];
            this._logger.log('REMOTE_STREAM_REMOVED id=' + streamToRemoveId);
            this.emit(
                MediaPeerConnection.event.REMOTE_STREAM_REMOVED,
                streamToRemove,
                streamToRemoveId
            );
        }
    }

    getStatForRemoteStreamByTapId(streamTapId) {
        const streamId = this._getRemoteStreamIdByStreamTapId(streamTapId);
        const audioSSRC = this._audioSSRCbyStreamId[streamId];
        const videoSSRC = this._videoSSRCbyStreamId[streamId];
        let audioStat;
        let videoStat;
        if (audioSSRC) {
            audioStat = this._statsRegister.getStatBySSRC(audioSSRC);
        }
        if (videoSSRC) {
            videoStat = this._statsRegister.getStatBySSRC(videoSSRC);
        }
        return {
            p2p: this.isP2P(),
            audio: audioStat || {
                bytes: 0,
            },
            video: videoStat || {
                bytes: 0,
            },
        };
    }

    getSSRCsByRemoteStreamId(streamId) {
        return {
            audio: this._audioSSRCbyStreamId[streamId],
            video: this._videoSSRCbyStreamId[streamId],
        };
    }

    getRemoteStreamByStreamId(streamId) {
        return this._remoteStreams.find(stream => stream.id === streamId);
    }

    getStatForLocalStream(streamId) {
        const videoSSRC = this._videoSSRCbyLocalStreamId[streamId];
        const audioSSRC = this._audioSSRCbyLocalStreamId[streamId];

        const originalVideoStat = this._statsRegister.getStatBySSRC(videoSSRC);
        const videoStat = videoSSRC && MediaPeerConnection.cleanAndCloneStat(originalVideoStat);

        const originalAudioStat = this._statsRegister.getStatBySSRC(audioSSRC);
        const audioStat = audioSSRC && MediaPeerConnection.cleanAndCloneStat(originalAudioStat);

        return { p2p: this.isP2P(), video: videoStat || {}, audio: audioStat || {} };
    }

    static cleanAndCloneStat(stat) {
        if (!stat) return stat;
        const result = {};
        Object.keys(stat)
            .filter(key => key[0] !== '_')
            .forEach(key => (result[key] = stat[key]));
        return result;
    }

    _getStreamsByMID() {
        const streams = {};
        if (this._transceiversSupported) {
            const transceivers = this._peerConnection.getTransceivers();
            transceivers.forEach(tr => {
                if (['sendrecv', 'sendonly'].includes(tr.direction) && typeof tr.mid === 'string') {
                    const track = tr.sender.track;
                    const stream = this._localStreams.find(localStreamInfo => {
                        return !!localStreamInfo.stream
                            .getTracks()
                            .find(streamTrack => streamTrack === track);
                    });
                    if (stream) {
                        streams[tr.mid] = stream.id;
                    }
                }
            });
        }
        return streams;
    }

    _updateLocalStreamIdToSSRCMapping() {
        this._videoSSRCbyLocalStreamId = {};
        this._audioSSRCbyLocalStreamId = {};
        if (this._peerConnection) {
            const localSDPText = (this._peerConnection.localDescription || { sdp: '' }).sdp;
            for (let i = this._pendingAddLocalStreams.length - 1; i >= 0; i--) {
                const streamId = this._pendingAddLocalStreams[i];
                if (localSDPText.indexOf(streamId) > -1) {
                    const localStreamInfo = this._localStreams.find(
                        streamInfo => streamInfo.id === streamId
                    );
                    if (localStreamInfo) {
                        const index = this._localStreams.indexOf(localStreamInfo);
                        this.emit(
                            MediaPeerConnection.event.LOCAL_STREAM_ADDED,
                            localStreamInfo.stream,
                            localStreamInfo.id,
                            index
                        );
                    }
                    this._pendingAddLocalStreams.splice(i, 1);
                }
            }

            const localSDP = new SDP(localSDPText);
            const media = localSDP.getMedia();
            const secondarySSRCs = [];

            const streamsByMID = this._getStreamsByMID();

            media.forEach(m => {
                const type = m.media;
                if (type === SDP.mediaType.DATA) {
                    return;
                }
                const msidLine = localSDP.getFirstMediaAttribute(m, 'msid');

                if (this._unifiedPlan) {
                    const mid = localSDP.getFirstMediaAttribute(m, 'mid');

                    const streamId = streamsByMID[mid];
                    if (!streamId) {
                        return;
                    }

                    localSDP.getMediaAttribute(m, 'ssrc-group').forEach(line => {
                        const match = line.match(/(FID|FEC-FR) [^ ]+ ([^ ]+)/);
                        if (match) {
                            secondarySSRCs.push(match[2]);
                        }
                    });

                    if (type === SDP.mediaType.VIDEO) {
                        localSDP
                            .getMediaAttribute(m, 'ssrc')
                            .reverse()
                            .forEach(line => {
                                const ssrc = line.match(/^\d+/)[0];
                                if (ssrc && secondarySSRCs.indexOf(ssrc) < 0) {
                                    this._videoSSRCbyLocalStreamId[streamId] = ssrc;
                                }
                            });
                    } else if (type === SDP.mediaType.AUDIO) {
                        localSDP.getMediaAttribute(m, 'ssrc').forEach(line => {
                            const ssrc = line.match(/^\d+/)[0];
                            if (ssrc && secondarySSRCs.indexOf(ssrc) < 0) {
                                this._audioSSRCbyLocalStreamId[streamId] = ssrc;
                            }
                        });
                    }
                } else if (ClientCapabilities.isSDPPlanB()) {
                    let msid;
                    if (msidLine) {
                        msid = msidLine.match(/^[^ ]+/)[0];
                    }
                    if (type === 'video') {
                        localSDP.getMediaAttribute(m, 'ssrc-group').forEach(line => {
                            const match = line.match(/(FID|FEC-FR) [^ ]+ ([^ ]+)/);
                            if (match) {
                                secondarySSRCs.push(match[2]);
                            }
                        });
                        localSDP
                            .getMediaAttribute(m, 'ssrc')
                            .reverse()
                            .forEach(line => {
                                const match = line.match(/(\d+) msid:([^ ]+)/);
                                if (match) {
                                    const ssrc = match[1];
                                    if (secondarySSRCs.indexOf(ssrc) < 0) {
                                        this._videoSSRCbyLocalStreamId[match[2]] = ssrc;
                                    }
                                } else if (msid) {
                                    const ssrc = line.match(/^\d+/)[0];
                                    if (ssrc && secondarySSRCs.indexOf(ssrc) < 0) {
                                        this._videoSSRCbyLocalStreamId[msid] = ssrc;
                                    }
                                }
                            });
                    } else if (type === 'audio') {
                        localSDP.getMediaAttribute(m, 'ssrc').forEach(line => {
                            const match = line.match(/(\d+) msid:([^ ]+)/);
                            if (match) {
                                this._audioSSRCbyLocalStreamId[match[2]] = match[1];
                            } else if (msid) {
                                const ssrc = line.match(/^\d+/)[0];
                                if (ssrc && secondarySSRCs.indexOf(ssrc) < 0) {
                                    this._audioSSRCbyLocalStreamId[msid] = ssrc;
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    _updateAudioVideoModes() {
        const mixAudioSDP = this._remoteSDP.getFirstAttribute('rcv-mix-audio');
        if (typeof mixAudioSDP === 'string') {
            const mixAudioEnabled =
                SDP.getSemicolonSeparatedParameterValueByName(mixAudioSDP, 'enabled') === '1';
            if (mixAudioEnabled !== this._mixAudioModeEnabled) {
                this.emit(MediaPeerConnection.event.MIX_AUDIO_MODE_STATE_CHANGED, mixAudioEnabled);
                this._mixAudioModeEnabled = mixAudioEnabled;
            }
        }
        const negotiateVideoSDP = this._remoteSDP.getFirstAttribute('rcv-negotiate-video');
        if (typeof negotiateVideoSDP === 'string') {
            const negotiateVideoEnabled =
                SDP.getSemicolonSeparatedParameterValueByName(negotiateVideoSDP, 'enabled') === '1';
            if (negotiateVideoEnabled !== this._negotiateVideoModeEnabled) {
                this.emit(
                    MediaPeerConnection.event.NEGOTIATE_VIDEO_MODE_STATE_CHANGED,
                    negotiateVideoEnabled
                );
                this._negotiateVideoModeEnabled = negotiateVideoEnabled;
            }
        }
    }
    _getRemovedStreams(newRemoteStreamsMap) {
        const removedStreams = [];
        for (const streamId in this._remoteStreamsMap) {
            if (this._remoteStreamsMap.hasOwnProperty(streamId)) {
                const streamInfo = newRemoteStreamsMap.find(
                    remoteStreamInfo => remoteStreamInfo.msid === streamId
                );
                if (!streamInfo) {
                    const stream =
                        this._remoteStreamsMap[streamId].stream ||
                        this._remoteStreams.find(stream => stream.id === streamId);
                    removedStreams.push({ stream, streamId });
                }
            }
        }
        return removedStreams;
    }

    _ensureDSCPForLocalTracks() {
        if (this._transceiversSupported || !this._peerConnection) {
            //in case transceivers supported - networkPriority will be set on addTransceiver,
            //so we don't need to wait setLocalDescription and update it
            return;
        }
        try {
            const senders = this._peerConnection.getSenders();
            senders.forEach(sender => {
                if (typeof sender.getParameters !== 'function') {
                    //Old browsers like Chrome 61 doesn't support getParameters
                    //just to prevent printing error
                    //DSCP may still work, but with hardcoded-in-Chrome value for all streams
                    return;
                }
                const params = sender.getParameters();
                const { encodings } = params;
                // Encoding parameters that are per-sender should only contain value at
                // index 0.
                // Currently (Chrome 79) networkPriority is
                // implemented "per-sender," meaning that these encoding parameters
                // are used for the RtpSender as a whole, not for a specific encoding layer.
                // This is done by setting these encoding parameters at index 0 of
                // RtpParameters.encodings.
                const encoding = encodings[0];
                if (
                    encoding &&
                    typeof encoding.networkPriority === 'string' &&
                    encoding.networkPriority !== 'high'
                ) {
                    encoding.networkPriority = 'high';
                    sender.setParameters(params).catch(err => {
                        this._logger.logError('Failed to ensure DSCP for local tracks');
                        this._logger.logError(err.stack);
                    });
                }
            });
        } catch (err) {
            this._logger.logError('Failed to ensure DSCP for local tracks');
            this._logger.logError(err.stack);
        }
    }

    _syncRemoteStreams(descriptionInit, remoteStreamsMap, removedStreams) {
        if (this._closed) return;

        if (!this._transceiversSupported) {
            this._remoteStreamsMap = {};
            remoteStreamsMap.forEach(newRemoteStreamInfo => {
                const msid = newRemoteStreamInfo.msid;
                this._remoteStreamsMap[msid] = Object.assign({}, newRemoteStreamInfo);
            });

            //Safari by some reason doesn't support onstreamremove or ontrackremove events
            //https://github.com/webrtc/samples/issues/921
            //https://github.com/w3c/webrtc-pc/issues/1161
            this._remoteStreams
                .filter(stream => descriptionInit.sdp.indexOf(`msid:${stream.id}`) < 0)
                .forEach(stream => this._removeRemoteStream(stream));

            return;
        }

        const transceivers = this._peerConnection.getTransceivers();

        //Remove old remote streams
        removedStreams.forEach(removedStreamInfo => {
            this._removeRemoteStream(removedStreamInfo.stream, removedStreamInfo.streamId);
        });

        //Add new remote streams and update existing
        const media = this._remoteSDP.getMedia();
        remoteStreamsMap.forEach(newRemoteStreamInfo => {
            const streamId = newRemoteStreamInfo.msid;
            const existingRemoteStreamInfo = this._remoteStreamsMap[streamId];
            let newMIDs = newRemoteStreamInfo.mids;
            if (!Array.isArray(newMIDs) && this.isP2P()) {
                //For old clients web and mobile which can p2p
                //They can not have mids array in streams map,
                //so we can recreate it, since they are using plan-b
                const msidRE = new RegExp(` msid:${streamId}`);
                newMIDs = media
                    .filter(m => {
                        const ssrcs = this._remoteSDP.getMediaAttribute(m, 'ssrc');
                        return ssrcs.find(ssrcLine => ssrcLine.match(msidRE));
                    })
                    .map(m => this._remoteSDP.getFirstMediaAttribute(m, 'mid'));
                newRemoteStreamInfo.mids = newMIDs;
            }
            if (!Array.isArray(newMIDs)) return;

            if (existingRemoteStreamInfo) {
                //Existing stream updated/not changed
                this._remoteStreamsMap[streamId] = Object.assign(
                    { stream: existingRemoteStreamInfo.stream },
                    newRemoteStreamInfo
                );
                const oldMIDs = existingRemoteStreamInfo.mids;
                if (!newMIDs.every(mid => oldMIDs.includes(mid))) {
                    //stream updated
                    const newTracks = newMIDs.map(mid => transceivers.find(tr => tr.mid === mid));
                    const existingStream = existingRemoteStreamInfo.stream;
                    const existingTracks = existingStream.getTracks();
                    existingTracks.forEach(existingTrack => {
                        if (!newTracks.includes(existingTrack)) {
                            existingStream.removeTrack(existingTrack);
                        }
                    });
                    newTracks.forEach(newTrack => {
                        //If the track has already been added to the MediaStream object, nothing happens.
                        existingStream.addTrack(newTrack);
                    });
                }
            } else {
                //New Stream added
                if (newMIDs.length > 0) {
                    this._remoteStreamsMap[streamId] = Object.assign({}, newRemoteStreamInfo);
                    const newTracks = newMIDs.map(
                        mid => transceivers.find(tr => tr.mid === mid).receiver.track
                    );
                    //This doesn't work in Chrome 70, it creates media stream with 1 track only (video)
                    //While creating empty media stream and then adding two tracks works as expected
                    //const newStream = new MediaStream(newTracks);
                    const newStream = new MediaStream();
                    newTracks.forEach(track => newStream.addTrack(track));
                    this._remoteStreamsMap[streamId].stream = newStream;
                    this._addRemoteStream(newStream, streamId);
                }
            }
        });
        this._updateRemoteStreamIdToSSRCMapping();
    }

    setRemoteDescription(description, remoteStreamsMap) {
        if (!this._peerConnection) {
            return rejectErr(MediaPeerConnection.error.PC_DOESNT_EXIST);
        }

        const sdpProcessor = new SdpProcessor(description.sdp);
        if (ClientCapabilities.dropTransportCcFromAudio) {
            sdpProcessor.thru(dropTransportCcExtFromAudio);
        }
        if (this._unifiedPlan) {
            this._fakeResetExtState = !this._fakeResetExtState;
            sdpProcessor.thru(sdp => {
                const videoSections = sdp.getMedia('video');
                if (!videoSections || videoSections.length === 0) return false;
                videoSections.forEach(videoMedia => {
                    if (this._fakeResetExtState) {
                        sdp.addRTPExtensionToMedia(videoMedia, SDP.rtpExtension.VIDEO_CONTENT_TYPE);
                    } else {
                        sdp.removeRTPExtensionFromMedia(
                            videoMedia,
                            SDP.rtpExtension.VIDEO_CONTENT_TYPE
                        );
                    }
                });

                return true;
            });
        }

        const descriptionInit = { sdp: sdpProcessor.getResult(), type: description.type };

        return Promise.resolve()
            .then(() => super._setLocalDescriptionBeforeRemote(descriptionInit))
            .then(() => {
                this._pendingResetRemoteStream = [];
                this._ensureDSCPForLocalTracks();
            })
            .then(() => {
                this._remoteSDP = new SDP(descriptionInit.sdp);
                this._updateAudioVideoModes();
                this._updateRemoteStreamIdToSSRCMapping();
                this.setRemoteSDPSessionId(this._remoteSDP.getSession().origin.sessionId);

                const consumers = this._remoteSDP.getAttribute('rcv-consumer').map(val => {
                    const parts = val.split(' ');
                    return new Consumer({
                        id: parts[0],
                        consumeVideo: parts.length > 1 && parts[1].indexOf('v') > -1,
                        consumeAudio: parts.length > 1 && parts[1].indexOf('a') > -1,
                        canP2P: parts.length > 1 && parts[1].indexOf('p') > -1,
                    });
                });
                this.emit(MediaPeerConnection.event.CONSUMERS_UPDATED, consumers);

                if (!this._isConnectedSDP(this._remoteSDP)) {
                    return this.close();
                }

                if (!this._peerConnection) {
                    return rejectErr(MediaPeerConnection.error.PC_DOESNT_EXIST);
                }

                const removedStreams = this._getRemovedStreams(remoteStreamsMap);
                //Notify upper layers that we going to remove this streams to give time to preserve last frame
                //if needed
                removedStreams.forEach(removedStreamInfo => {
                    this.emit(
                        MediaPeerConnection.event.REMOTE_STREAM_BEFORE_REMOVE,
                        removedStreamInfo.stream,
                        removedStreamInfo.streamId
                    );
                });

                return this._peerConnection
                    .setRemoteDescription(descriptionInit)
                    .then(() =>
                        this._syncRemoteStreams(descriptionInit, remoteStreamsMap, removedStreams)
                    );
            });
    }

    _isConnectedSDP(sdp) {
        return !!sdp.getMedia().find(m => {
            if ([SDP.mediaType.VIDEO, SDP.mediaType.AUDIO].indexOf(m.media) > -1) {
                return !this._remoteSDP.getFirstMediaAttribute(m, 'inactive');
            } else if (m.media === SDP.mediaType.DATA) {
                return true;
            } else {
                throw new Error(`Unepected media type: ${m.media}`);
            }
        });
    }

    setLocalDescription(description) {
        return this._peerConnection.signalingState ===
            MediaPeerConnection.signalingState.HAVE_LOCAL_OFFER
            ? Promise.resolve(this._updateLocalStreamIdToSSRCMapping())
            : this._peerConnection.setLocalDescription(description).then(() => {
                  this._updateLocalStreamIdToSSRCMapping();
                  this._ensureDSCPForLocalTracks();
              });
    }

    _getLocalTrackMID(track, localDescriptionOrLocalSDP) {
        if (this._transceiversSupported) {
            const transceiversMIDs = this._getTransceiversMIDs(localDescriptionOrLocalSDP);
            const transceivers = this._peerConnection.getTransceivers();
            const transceiverIndex = transceivers.findIndex(tr => track === tr.sender.track);
            if (transceiverIndex > -1) {
                return transceiversMIDs[transceiverIndex];
            }
        } else {
            this._logger.logError(
                '_getLocalTrackMID expected to be called only in case transceivers is supported'
            );
        }
    }

    _getTransceiversMIDs(localDescriptionOrLocalSDP) {
        const transceivers = this._peerConnection.getTransceivers();
        const transceiversMIDs = transceivers.map(tr => tr.mid);
        const localSDP =
            localDescriptionOrLocalSDP instanceof SDP
                ? localDescriptionOrLocalSDP
                : new SDP(localDescriptionOrLocalSDP);
        const media = localSDP.getMedia();
        transceiversMIDs.forEach((mid, index) => {
            const tr = transceivers[index];
            if (!mid && !tr.stopped && ['sendrecv', 'sendonly'].includes(tr.direction)) {
                const trType = tr.sender.track.kind;
                let trMID;
                //No easy way to find mid for new transceivers
                //We trying to match every not stopped transceiver without mid
                //with m line in offer
                for (let i = 0; i < media.length; i++) {
                    const m = media[i];
                    const mLineMID = localSDP.getFirstMediaAttribute(m, 'mid');
                    const isSameDirection = !!localSDP.getFirstMediaAttribute(m, tr.direction);
                    if (
                        m.media === trType &&
                        isSameDirection &&
                        transceiversMIDs.indexOf(mLineMID) < 0
                    ) {
                        trMID = mLineMID;
                    }
                }
                transceiversMIDs[index] = trMID;
            }
        });
        return transceiversMIDs;
    }

    getStreamsMIDs(localDescription) {
        const mids = {};
        if (this._transceiversSupported) {
            const transceivers = this._peerConnection.getTransceivers();
            const transceiversMIDs = this._getTransceiversMIDs(localDescription);
            transceivers.forEach((tr, index) => {
                if (['sendrecv', 'sendonly'].includes(tr.direction) && !tr.stopped) {
                    const track = tr.sender.track;
                    const stream = this._localStreams.find(localStreamInfo => {
                        return !!localStreamInfo.stream
                            .getTracks()
                            .find(streamTrack => streamTrack === track);
                    });
                    if (stream) {
                        const mid = transceiversMIDs[index];
                        mids[stream.id] = mids[stream.id] || [];
                        mids[stream.id].push(mid);
                    }
                }
            });
        }
        return mids;
        /*
            {
                "<local stream msid>": [
                    "<mid-audio-track>",
                    "<mid-video-track>"
                ]
            }
         */
    }

    _getIsFlexFECSupported() {
        return (
            !this._blacklistedCodecs.includes(SDP.codec.FLEXFEC_03) &&
            this._supportedCodecs.some(codecInfo => codecInfo.name === SDP.codec.FLEXFEC_03)
        );
    }

    _makeSimSSRCs(localSDP, media, msidRemapping) {
        const isFlexFECSupported = this._getIsFlexFECSupported();
        const isRTXEnabled = this._isRTXEnabled();

        if (ClientCapabilities.simulcast && ClientCapabilities.chromeLikeSimulcast) {
            if (this._planB) {
                const videoTracksInfo = getSdpVideoTracksInfo(localSDP, media);

                localSDP.deleteMediaAttribute(media, 'ssrc');
                localSDP.deleteMediaAttribute(media, 'ssrc-group');

                if (ClientCapabilities.incorrectPlanBMSIDs) {
                    fixPlanBMSIDs(videoTracksInfo, msidRemapping);
                }

                this._localStreams.forEach(localStreamInfo => {
                    const msid = localStreamInfo.id;
                    const videoTrackInfo = videoTracksInfo[msid];
                    if (!videoTrackInfo) {
                        return; //It could be if there is no video track in the stream
                    }
                    if (localStreamInfo.useSimulcast && !videoTrackInfo.SIM) {
                        initializeSimulcast(videoTrackInfo, isFlexFECSupported);
                    } else {
                        //Non-simulcast
                        if (!videoTrackInfo.FID) {
                            const mediaSsrcs = videoTrackInfo.ssrcs;
                            videoTrackInfo.FID = mediaSsrcs.map(fillSsrcPair(videoTrackInfo.ssrcs));
                        }
                    }

                    modifySDPMedia(localSDP, media, videoTrackInfo, msid, isRTXEnabled);
                });
            } else if (this._unifiedPlan) {
                const mid = localSDP.getFirstMediaAttribute(media, 'mid');
                const transceivers = this._peerConnection.getTransceivers();
                const localStreamInfo = this._localStreams.find((localStreamInfo, index) => {
                    const videoTrack = localStreamInfo.stream.getVideoTracks()[0];
                    if (videoTrack) {
                        const transceiver = transceivers.find(tr => tr.sender.track === videoTrack);
                        //If transceiver doesn't have mid
                        //it means it is for new local track
                        //So, we checking if this local track is latest in the list (just added)
                        //This of course will not work if multiple simulcast tracks will be added
                        //at once and then controlClient.connectMedia() called
                        //But we don't expect that
                        return (
                            (!transceiver.mid && index === this._localStreams.length - 1) ||
                            transceiver.mid === mid
                        );
                    }
                });
                if (!localStreamInfo) {
                    return media;
                }

                const msid = localStreamInfo.id;
                const videoTracksInfo = getSdpVideoTracksInfo(localSDP, media);
                const videoTrackInfo = videoTracksInfo[msid];
                if (videoTrackInfo) {
                    if (localStreamInfo.useSimulcast && (videoTrackInfo.SIM?.length ?? 0) < 1) {
                        initializeSimulcast(videoTrackInfo, isFlexFECSupported);
                    }

                    localSDP.deleteMediaAttribute(media, 'ssrc');
                    localSDP.deleteMediaAttribute(media, 'ssrc-group');

                    modifySDPMedia(localSDP, media, videoTrackInfo, msid, isRTXEnabled);
                }
            }
            if (ClientCapabilities.googFlags) {
                media.attributes['x-google-flag'] = 'conference';
            }
        }
        return media;
    }

    _isRTXEnabled() {
        return (
            !!this._supportedCodecs.find(codec => codec.name === SDP.codec.RTX) &&
            isCodecAllowed({ name: SDP.codec.RTX }, this._blacklistedCodecs)
        );
    }

    _mangleLocalDescription(description, isAnswer, prevDescription) {
        const mangledDescription = super._mangleLocalDescription(
            description,
            isAnswer,
            prevDescription
        );

        let sdpAsString = mangledDescription.sdp;

        if (ClientCapabilities.incorrectPlanBMSIDs && this._msidRemapping) {
            this._msidRemapping.forEach(replacePare => {
                const re = new RegExp(replacePare.from, 'ig');
                sdpAsString = sdpAsString.replace(re, replacePare.to);
                sdpAsString = sdpAsString.replace(
                    `${replacePare.to} ${replacePare.to}`,
                    replacePare.to
                );
            });
        }

        this._msidRemapping = null;
        mangledDescription.sdp = sdpAsString;
        return mangledDescription;
    }

    _addCustomSDPAttributes(localSDP, ...args) {
        if (!this._supportedCodecs) {
            this._supportedCodecs = localSDP.getAllCodecs();
        }

        super._addCustomSDPAttributes(localSDP, ...args);
        localSDP.setAttribute('rcv-denoise', `enabled=${this._denoise ? 1 : 0}`);
        localSDP.setAttribute('rcv-agc', `enabled=${this._agc ? 1 : 0}`);
        localSDP.setAttribute('rcv-noise-gate', `enabled=${this._noiseGate ? 1 : 0}`);

        if (this.isP2P()) {
            this._addSDPAttributesForP2P(localSDP, ...args);
        } else {
            this._addSDPAttributesForNonP2P(localSDP, ...args);
        }
    }

    _addSDPAttributesForP2P(localSDP, isAnswer) {
        const media = localSDP.getMedia();
        media.forEach(m => {
            if (isAnswer) {
                localSDP.replaceMediaAttribute(m, 'setup', this.isMaster() ? 'passive' : 'active');
            } else if (m.media === SDP.mediaType.VIDEO) {
                const codecs = localSDP.getAllCodecsOfMedia(m);
                const codecsOrder = codecs
                    .sort(this._codecsComparator)
                    .map(codecInfo => codecInfo.id);
                m.format = codecsOrder.join(' ');
            }
        });

        localSDP.setMedia(media);
    }

    _addSDPAttributesForNonP2P(localSDP, isAnswer, prevDescription) {
        const prevLocalSDP = prevDescription ? new SDP(prevDescription.sdp) : undefined;
        const media = localSDP.getMedia();
        const msidRemapping = [];

        this._pendingResetRemoteStream.forEach(tapId =>
            localSDP.setAttribute('rcv-reset-stream', tapId)
        );

        localSDP.setAttribute('rcv-mix-audio', `recv-mode=${this._mixAudioMode}`);
        localSDP.setAttribute('rcv-negotiate-video', `recv-mode=${this._negotiateVideoMode}`);

        const {
            lsAttributes,
            devicesAudio,
            devicesVideo,
        } = this._getSDPAttributesAndDevicesFromLocalStreams(localSDP);

        lsAttributes.forEach(([attr, val]) => localSDP.setAttribute(attr, val));

        const isRTXEnabled = this._isRTXEnabled();

        media.forEach((m, index) => {
            const recvOnly = localSDP.getMediaAttribute(m, 'recvonly')?.[0];
            // skip receive only sections for video to allow unified plan
            if (recvOnly && m.media === 'video') return;
            if (ClientCapabilities.incorrectPlanBMSIDs) {
                const ssrcLines = localSDP.getMediaAttribute(m, 'ssrc');
                ssrcLines.forEach(ssrcLine => {
                    const match = ssrcLine.match(/\d+ msid:([^ ]+) ([^ ]+)/);
                    if (match) {
                        const trackId = match[2];
                        const msid = match[1];
                        const correctMSID = this._msidMapping[trackId];
                        if (correctMSID && correctMSID !== msid) {
                            msidRemapping.push({ from: msid, to: this._msidMapping[trackId] });
                        }
                    }
                });
            }
            if (m.media === 'video') {
                if (
                    localSDP.getFirstMediaAttribute(m, 'sendonly') ||
                    localSDP.getFirstMediaAttribute(m, 'sendrecv')
                ) {
                    m = this._makeSimSSRCs(localSDP, m, msidRemapping);
                }
                if (
                    isAnswer &&
                    ClientCapabilities.simulcast &&
                    this._unifiedPlan &&
                    prevLocalSDP &&
                    ClientCapabilities.firefoxLikeSimulcast
                ) {
                    const prevM = prevLocalSDP.getMedia()[index];
                    if (prevM) {
                        const prevRID = prevLocalSDP.getMediaAttribute(prevM, 'rid');
                        const rid = localSDP.getMediaAttribute(m, 'rid');
                        if (rid.length === 0 && prevRID.length > 0) {
                            localSDP.setMediaAttribute(m, 'rid', prevRID);
                        }
                        const prevSimulcast = prevLocalSDP.getFirstMediaAttribute(
                            prevM,
                            'simulcast'
                        );
                        const simulcast = localSDP.getFirstMediaAttribute(m, 'simulcast');
                        if (
                            typeof prevSimulcast !== 'undefined' &&
                            typeof simulcast === 'undefined'
                        ) {
                            localSDP.setMediaAttribute(m, 'simulcast', prevSimulcast);
                        }
                    }
                }
            }

            if (['video', 'audio'].indexOf(m.media) > -1) {
                const mediaCodecs = localSDP.getAllCodecsOfMedia(m);
                localSDP.removeAllCodecs(m);

                if (this._planB) {
                    if (m.media === SDP.mediaType.AUDIO) {
                        Object.keys(devicesAudio).forEach(trackUID => {
                            localSDP.setMediaAttribute(
                                m,
                                'rcv-device',
                                trackUID + ' "' + devicesAudio[trackUID] + '"'
                            );
                        });
                    }
                    if (m.media === SDP.mediaType.VIDEO) {
                        Object.keys(devicesVideo).forEach(trackUID => {
                            localSDP.setMediaAttribute(
                                m,
                                'rcv-device',
                                trackUID + ' "' + devicesVideo[trackUID] + '"'
                            );
                        });
                    }
                } else if (this._unifiedPlan) {
                    const mid = localSDP.getFirstMediaAttribute(m, 'mid');
                    if (m.media === SDP.mediaType.AUDIO && mid) {
                        localSDP.setMediaAttribute(
                            m,
                            'rcv-device',
                            mid + ' "' + devicesAudio[mid] + '"'
                        );
                    }
                    if (m.media === SDP.mediaType.VIDEO && mid) {
                        localSDP.setMediaAttribute(
                            m,
                            'rcv-device',
                            mid + ' "' + devicesVideo[mid] + '"'
                        );
                    }
                }

                if (!this._transportCC) {
                    localSDP.removeRTPExtensionFromMedia(m, SDP.rtpExtension.TRANSPORT_CC);
                }

                const codecsInfo = this._supportedCodecs
                    .filter(
                        codec =>
                            m.media === SDP.codecType[codec.name] &&
                            isCodecAllowed(codec, this._blacklistedCodecs) &&
                            isCodecSupportedBySFU(codec) &&
                            ((codec.name === SDP.codec.RED_AUDIO && this._audioRedEnabled) ||
                                ![SDP.codec.RTX, SDP.codec.RED_AUDIO].includes(codec.name))
                    )
                    .sort(this._codecsComparator);

                const takenCodecIds = [];
                codecsInfo.forEach(codecInfo => {
                    takenCodecIds.push(Number(codecInfo.id));
                    if (codecInfo.rtxId && isRTXEnabled) {
                        takenCodecIds.push(Number(codecInfo.rtxId));
                    }
                });

                codecsInfo.forEach(codecInfo => {
                    const codecInfoCopy = JSON.parse(JSON.stringify(codecInfo));
                    // eslint-disable-next-line camelcase
                    codecInfoCopy.rtcp_fb = codecInfoCopy.rtcp_fb.filter(
                        feedback => !feedback.includes('goog-remb')
                    );

                    if (!this._transportCC) {
                        // eslint-disable-next-line camelcase
                        codecInfoCopy.rtcp_fb = codecInfoCopy.rtcp_fb.filter(
                            feedback => !feedback.includes('transport-cc')
                        );
                    }

                    const generatedCodecInfo = mediaCodecs.find(gCodecInfo =>
                        areCodecsEqual(gCodecInfo, codecInfoCopy)
                    );
                    if (generatedCodecInfo) {
                        codecInfoCopy.id = generatedCodecInfo.id;
                        codecInfoCopy.rtxId = generatedCodecInfo.rtxId;
                    } else if (isAnswer) {
                        //Since it is answer and we cannot find such codec in offer
                        //we should not add it into answer
                        return;
                    } else {
                        //In case it is offer and we want to add more codecs,
                        //than browser propose, we should make sure they not
                        //conflicting by ids
                        codecInfoCopy.id = getFreeCodecId(takenCodecIds);
                        takenCodecIds.push(Number(codecInfoCopy.id));
                        if (codecInfoCopy.rtxId && isRTXEnabled) {
                            codecInfoCopy.rtxId = getFreeCodecId(takenCodecIds);
                            takenCodecIds.push(Number(codecInfoCopy.rtxId));
                        }
                    }
                    const cc = SDP.codec;
                    switch (codecInfoCopy.name) {
                        case cc.OPUS:
                            codecInfoCopy.fmtp = [
                                [
                                    'minptime=10',
                                    // Disable FEC if Audio RED is first codec in the list.
                                    `useinbandfec=${
                                        codecsInfo?.[0]?.name === CODECS.RED_AUDIO ? 0 : 1
                                    }`,
                                    `maxplaybackrate=${this._audioRate}`,
                                    `sprop-maxcapturerate=${this._audioRate}`,
                                    `maxaveragebitrate=${this._audioBitrate}`,
                                    'stereo=0',
                                    'usedtx=1',
                                ].join(';'),
                            ];
                            break;
                        case cc.VP8:
                        case cc.H264:
                        case cc.VP9:
                            if (ClientCapabilities.googFlags) {
                                codecInfoCopy.fmtp.push('x-google-start-bitrate=800');
                            }
                            break;
                    }
                    localSDP.addCodecToMedia(
                        m,
                        codecInfoCopy.name,
                        codecInfoCopy.id,
                        codecInfoCopy.rtcp_fb,
                        codecInfoCopy.fmtp
                    );
                    if (
                        isRTXEnabled &&
                        SDP.rtxForCodec[codecInfoCopy.name] &&
                        codecInfoCopy.rtxId
                    ) {
                        const rtxCodecInfo = {
                            name: SDP.codec.RTX,
                            id: codecInfoCopy.rtxId,
                            fmtp: [`apt=${codecInfoCopy.id}`],
                            rtcp_fb: [], // eslint-disable-line camelcase
                        };
                        localSDP.addCodecToMedia(
                            m,
                            rtxCodecInfo.name,
                            rtxCodecInfo.id,
                            rtxCodecInfo.rtcp_fb,
                            rtxCodecInfo.fmtp
                        );
                    }
                });
            }
        });

        this._msidRemapping = msidRemapping;
        localSDP.setMedia(media);
    }

    _getSDPAttributesAndDevicesFromLocalStreams(localSDP) {
        const lsAttributes = [];
        const devicesAudio = {};
        const devicesVideo = {};

        this._localStreams.forEach(localStreamInfo => {
            const audioTrack = localStreamInfo.stream.getAudioTracks()[0];
            const videoTrack = localStreamInfo.stream.getVideoTracks()[0];
            const useMids = this._unifiedPlan && this._transceiversSupported;
            if (audioTrack) {
                const audioTrackUID = useMids
                    ? this._getLocalTrackMID(audioTrack, localSDP)
                    : audioTrack.id;

                if (audioTrackUID) {
                    devicesAudio[audioTrackUID] = audioTrack.label.replace(/["]/g, '\\"');
                }
            }

            if (
                videoTrack &&
                typeof localStreamInfo.width !== 'undefined' &&
                typeof localStreamInfo.height !== 'undefined'
            ) {
                const videoTrackUID = useMids
                    ? this._getLocalTrackMID(videoTrack, localSDP)
                    : videoTrack.id;

                if (videoTrackUID) {
                    lsAttributes.push([
                        'rcv-track',
                        videoTrackUID + ' ' + localStreamInfo.width + ' ' + localStreamInfo.height,
                    ]);

                    devicesVideo[videoTrackUID] = videoTrack.label.replace(/["]/g, '\\"');
                }
            }
            lsAttributes.push(['rcv-stream', localStreamInfo.tapId + ' ' + localStreamInfo.id]);
        });

        return { lsAttributes, devicesAudio, devicesVideo };
    }

    forceAudioBitrate() {
        if (!Number(this._audioBitrate)) return;

        const audioSender = this._peerConnection
            ?.getSenders?.()
            .find(sender => sender.track && sender.track.kind === 'audio');
        if (!audioSender) return;
        return setSenderBitrate(audioSender, Number(this._audioBitrate));
    }

    createOffer() {
        if (!this._peerConnection) {
            return rejectErr(MediaPeerConnection.error.CANNOT_CREATE_OFFER);
        }
        let streamsWithoutDimensions;
        return this._waitLocalStreamsAdded
            .then(() => {
                if (this.getRole() === PeerConnectionRoles.ADDITIONAL) {
                    this.forceAudioBitrate();
                }
            })
            .then(() => {
                if (
                    ClientCapabilities.simulcast &&
                    this._unifiedPlan &&
                    ClientCapabilities.firefoxLikeSimulcast
                ) {
                    this._localStreams.forEach(streamInfo => {
                        const videoSenders = this._peerConnection.getSenders().filter(sender => {
                            return (
                                sender.track !== null &&
                                sender.track.kind === 'video' &&
                                streamInfo.stream
                                    .getTracks()
                                    .find(track => track.id === sender.track.id)
                            );
                        });
                        let senderParameters;
                        if (streamInfo.useSimulcast) {
                            senderParameters = {
                                encodings: [
                                    {
                                        rid: '1',
                                        active: true,
                                        priority: 'high',
                                        maxBitrate: this._bitratePerSimulcastTrack[2],
                                    },
                                    {
                                        rid: '2',
                                        active: true,
                                        priority: 'medium',
                                        scaleResolutionDownBy: 2,
                                        maxBitrate: this._bitratePerSimulcastTrack[1],
                                    },
                                    {
                                        rid: '3',
                                        active: true,
                                        priority: 'low',
                                        scaleResolutionDownBy: 4,
                                        maxBitrate: this._bitratePerSimulcastTrack[0],
                                    },
                                ],
                            };
                        } else {
                            senderParameters = {
                                encodings: [
                                    {
                                        active: true,
                                        maxBitrate: this._bitrateNonSimulcastStream,
                                    },
                                ],
                            };
                        }
                        videoSenders.forEach(sender => {
                            const oldSenderParameters = sender.getParameters();
                            this._logger.log(
                                `Old parameters: ${JSON.stringify(oldSenderParameters)}`
                            );
                            if (oldSenderParameters) {
                                if (Array.isArray(oldSenderParameters.encodings)) {
                                    const oldEncodings = oldSenderParameters.encodings;
                                    senderParameters.encodings.forEach((encoding, index) => {
                                        if (index >= oldEncodings.length) {
                                            oldEncodings.push(encoding);
                                        } else {
                                            for (const property in encoding) {
                                                //We should skip active property as it could be changed by
                                                // setSenderQuality call
                                                if (
                                                    encoding.hasOwnProperty(property) &&
                                                    property !== 'active'
                                                ) {
                                                    oldEncodings[property] = encoding[property];
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    oldSenderParameters.encodings = senderParameters.encodings;
                                }
                            }
                            this._logger.log(
                                `New parameters: ${JSON.stringify(oldSenderParameters)}`
                            );
                            sender
                                .setParameters(oldSenderParameters)
                                .then(() => {
                                    this._logger.log('Parameters set');
                                })
                                .catch(err => {
                                    this._logger.logError(err);
                                });
                        });
                    });
                }
            })
            .then(() => {
                streamsWithoutDimensions = this._localStreams.filter(
                    localStream =>
                        (typeof localStream.width === 'undefined' ||
                            typeof localStream.height === 'undefined') &&
                        localStream.stream.getVideoTracks().length > 0
                );
                const getDimensions = streamsWithoutDimensions.map(
                    localStream => localStream.getDimensionsPromise
                );
                return Promise.all(getDimensions);
            })
            .then(dimensions => {
                streamsWithoutDimensions.forEach((localStream, index) => {
                    if (dimensions[index].width > 2 && dimensions[index].height > 2) {
                        localStream.width = dimensions[index].width;
                        localStream.height = dimensions[index].height;
                    }
                });
            })
            .then(() =>
                // TODO RCV-31775: these options are legacy, probably should be removed along with plan-b
                super.createOffer({
                    offerToReceiveAudio: true,
                    offerToReceiveVideo: true,
                })
            );
    }

    resetRemoteStream(remoteStreamTapId) {
        if (this._pendingResetRemoteStream.indexOf(remoteStreamTapId) < 0) {
            this._pendingResetRemoteStream.push(remoteStreamTapId);
        }
    }

    getPeerConnection() {
        return this._peerConnection;
    }
}

MediaPeerConnection.event = {
    ...BasePeerConnection.event,
    REMOTE_STREAM_ADDED: 'remote_stream_added',
    REMOTE_STREAM_BEFORE_REMOVE: 'remote_stream_before_remove',
    REMOTE_STREAM_REMOVED: 'remote_stream_removed',
    LOCAL_STREAM_ADDED: 'local_stream_added',
    LOCAL_STREAM_REMOVED: 'local_stream_removed',
    CONSUMERS_UPDATED: 'consumers_updated',
    MIX_AUDIO_MODE_STATE_CHANGED: 'mix_audio_mode_state_changed',
    NEGOTIATE_VIDEO_MODE_STATE_CHANGED: 'negotiate_video_mode_state_changed',
    NETWORK_TYPE_CHANGED: 'network_type_changed',
};

MediaPeerConnection.mixAudioMode = {
    ALWAYS: 'always',
    NEVER: 'never',
    AUTO: 'auto',
};

MediaPeerConnection.negotiateVideoMode = {
    ALWAYS: 'always',
    NEVER: 'never',
    AUTO: 'auto',
};

export default MediaPeerConnection;
