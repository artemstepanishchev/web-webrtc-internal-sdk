import LogWrapper from './log_wrapper.js';

//Lock constructor exists in Chrome with some native implementation
//But no information on the Internet about it

class Lock {
    constructor(options) {
        options = options || {};
        this._queue = [];
        this._stuckLockTimer = undefined;
        this._version = 0;
        this._pos = undefined;
        this._timeout = options.timeout || 5000;
        this._logger = new LogWrapper(options.logger, () => `lk: ${this._sessionId}`);
        this._sessionId = options.sessionId;
    }

    getLock(desc, callback, timeoutCallback) {
        if (typeof callback === 'undefined') {
            return this._getLockP(desc);
        }
        this._queue.push({ cb: callback, tcb: timeoutCallback, sk: new Error().stack, desc: desc });
        if (this._queue.length === 1) {
            this._releaseLock();
        }
    }

    _getLockP(desc) {
        return new Promise((resolve, reject) => {
            this._queue.push({ cb: resolve, tcb: reject, sk: new Error().stack, desc: desc });
            if (this._queue.length === 1) {
                this._releaseLock();
            }
        });
    }

    clear() {
        this._queue = [];
        clearTimeout(this._stuckLockTimer);
        this._stuckLockTimer = undefined;
        this._version++;
    }

    _releaseLock() {
        if (this._queue.length > 0) {
            this._logger.log(`Lock taken for ${this._queue[0].desc}`);
            const version = this._version;

            const item = this._queue[0];

            this._stuckLockTimer = setTimeout(() => {
                if (this._queue[0] !== item) {
                    this._logger.logError('Timer stuck with changed item in queue');
                    return;
                }
                const err = new Error(
                    `Lock was not released after ${this._timeout}ms. Taken for ${this._queue[0].desc}`
                );
                this._logger.logError(err.message);
                this._logger.logError(this._queue[0].sk);
                if (this._pos) {
                    this._logger.logError(this._pos.stack);
                }
                if (typeof this._queue[0].tcb === 'function') {
                    try {
                        this._queue[0].tcb(err);
                        if (version === this._version) {
                            this._queue.shift();
                            this._releaseLock();
                        }
                    } catch (err) {
                        this._logger.logError('Error while calling timeout function');
                        this._logger.logError(err);
                    }
                } else {
                    // eslint-disable-next-line no-debugger
                    debugger;
                }
            }, this._timeout);

            item.cb(() => {
                if (version === this._version) {
                    if (this._queue[0] !== item) {
                        this._logger.logWarn('Duplicate call of release lock');
                    } else {
                        this._logger.log(`Lock released after ${this._queue[0].desc}`);
                        clearTimeout(this._stuckLockTimer);
                        this._queue.shift();
                        this._releaseLock();
                    }
                }
            });
        }
    }

    isLocked() {
        return this._queue.length > 0;
    }

    setPos() {
        this._pos = new Error();
    }
}

export default Lock;
