import assert from './assert.js';

class EventEmitter {
    constructor() {
        EventEmitter.init.call(this);
        this._events = undefined;
        this._maxListeners = undefined;
        this.on = EventEmitter.prototype.addListener;
        this.listenerCount = listenerCount;
    }

    // Obviously not all Emitters should be limited to 10. This function allows
    // that to be increased. Set to zero for unlimited.
    setMaxListeners(n) {
        if (typeof n !== 'number' || n < 0 || isNaN(n))
            throw new TypeError('n must be a positive number');
        this._maxListeners = n;
        return this;
    }

    getMaxListeners() {
        return $getMaxListeners(this);
    }

    emit(type) {
        let er, args, i;
        let doError = type === 'error';

        const events = this._events;
        if (events) doError = doError && events.error === null;
        else if (!doError) return false;

        // If there is no 'error' event listener then throw.
        if (doError) {
            er = arguments[1];
            if (er instanceof Error) {
                throw er; // Unhandled 'error' event
            } else {
                // At least give some kind of context to the user
                const err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
                err.context = er;
                throw err;
            }
        }

        const handler = events[type];

        if (!handler) return false;

        const isFn = typeof handler === 'function';
        const len = arguments.length;
        switch (len) {
            // fast cases
            case 1:
                emitNone(handler, isFn, this);
                break;
            case 2:
                emitOne(handler, isFn, this, arguments[1]);
                break;
            case 3:
                emitTwo(handler, isFn, this, arguments[1], arguments[2]);
                break;
            case 4:
                emitThree(handler, isFn, this, arguments[1], arguments[2], arguments[3]);
                break;
            // slower
            default:
                args = new Array(len - 1);
                for (i = 1; i < len; i++) args[i - 1] = arguments[i];
                emitMany(handler, isFn, this, args);
        }

        return true;
    }

    addListener(type, listener) {
        let m;
        let events;
        let existing;

        if (typeof listener !== 'function') throw new TypeError('listener must be a function');

        events = this._events;
        if (!events) {
            events = this._events = {};
            this._eventsCount = 0;
        } else {
            // To avoid recursion in the case that type === "newListener"! Before
            // adding it to the listeners, first emit "newListener".
            if (events.newListener) {
                this.emit('newListener', type, listener.listener ? listener.listener : listener);

                // Re-assign `events` because a newListener handler could have caused the
                // this._events to be assigned to a new object
                events = this._events;
            }
            existing = events[type];
        }

        if (!existing) {
            // Optimize the case of one listener. Don't need the extra array object.
            existing = events[type] = listener;
            ++this._eventsCount;
        } else {
            if (typeof existing === 'function') {
                // Adding the second element, need to change to array.
                existing = events[type] = [existing, listener];
            } else {
                // If we've already got an array, just append.
                existing.push(listener);
            }

            // Check for listener leak
            if (!existing.warned) {
                m = $getMaxListeners(this);
                if (m && m > 0 && existing.length > m) {
                    existing.warned = true;
                    // eslint-disable-next-line no-console
                    console.error(
                        '(node) warning: possible EventEmitter memory ' +
                            'leak detected. %d %s listeners added. ' +
                            'Use emitter.setMaxListeners() to increase limit.',
                        existing.length,
                        type
                    );
                    // eslint-disable-next-line no-console
                    console.trace();
                }
            }
        }

        return this;
    }

    once(type, listener) {
        if (typeof listener !== 'function') throw new TypeError('listener must be a function');

        let fired = false;

        const g = () => {
            this.removeListener(type, g);

            if (!fired) {
                fired = true;
                listener.apply(this, arguments);
            }
        };

        g.listener = listener;
        this.on(type, g);

        return this;
    }

    // emits a 'removeListener' event iff the listener was removed
    removeListener(type, listener) {
        let position, i;

        if (typeof listener !== 'function') throw new TypeError('listener must be a function');

        const events = this._events;
        if (!events) return this;

        const list = events[type];
        if (!list) return this;

        if (list === listener || (list.listener && list.listener === listener)) {
            if (--this._eventsCount === 0) this._events = {};
            else {
                delete events[type];
                if (events.removeListener) this.emit('removeListener', type, listener);
            }
        } else if (typeof list !== 'function') {
            position = -1;

            for (i = list.length; i-- > 0; ) {
                if (list[i] === listener || (list[i].listener && list[i].listener === listener)) {
                    position = i;
                    break;
                }
            }

            if (position < 0) return this;

            if (list.length === 1) {
                list[0] = undefined;
                if (--this._eventsCount === 0) {
                    this._events = {};
                    return this;
                } else {
                    delete events[type];
                }
            } else {
                spliceOne(list, position);
            }

            if (events.removeListener) this.emit('removeListener', type, listener);
        }

        return this;
    }

    removeAllListeners(type) {
        const events = this._events;
        if (!events) return this;

        // not listening for removeListener, no need to emit
        if (!events.removeListener) {
            if (arguments.length === 0) {
                this._events = {};
                this._eventsCount = 0;
            } else if (events[type]) {
                if (--this._eventsCount === 0) this._events = {};
                else delete events[type];
            }
            return this;
        }

        // emit removeListener for all listeners on all events
        if (arguments.length === 0) {
            const keys = Object.keys(events);
            for (let i = 0, key; i < keys.length; ++i) {
                key = keys[i];
                if (key === 'removeListener') continue;
                this.removeAllListeners(key);
            }
            this.removeAllListeners('removeListener');
            this._events = {};
            this._eventsCount = 0;
            return this;
        }

        const listeners = events[type];

        if (typeof listeners === 'function') {
            this.removeListener(type, listeners);
        } else if (listeners) {
            // LIFO order
            do {
                this.removeListener(type, listeners[listeners.length - 1]);
            } while (listeners[0]);
        }

        return this;
    }

    listeners(type) {
        let evlistener;
        let ret;
        const events = this._events;

        if (!events) ret = [];
        else {
            evlistener = events[type];
            if (!evlistener) ret = [];
            else if (typeof evlistener === 'function') ret = [evlistener];
            else ret = arrayClone(evlistener, evlistener.length);
        }

        return ret;
    }

    forwardEvents(forwardMap, forwardTo) {
        forwardMap.forEach(
            function (mapping) {
                assert(mapping.length === 2);
                const sourceEvent = mapping[0];
                const forwardEvent = mapping[1];
                this.on(sourceEvent, function () {
                    forwardTo.emit.apply(
                        forwardTo,
                        [forwardEvent].concat(Array.prototype.slice.call(arguments))
                    );
                });
            }.bind(this)
        );
    }

    static init() {
        if (!this._events || this._events === Object.getPrototypeOf(this)._events) {
            this._events = {};
            this._eventsCount = 0;
        }

        this._maxListeners = this._maxListeners || undefined;
    }

    static listenerCount(emitter, type) {
        if (typeof emitter.listenerCount === 'function') {
            return emitter.listenerCount(type);
        } else {
            return listenerCount.call(emitter, type);
        }
    }
}

EventEmitter.usingDomains = false;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

export default EventEmitter;

export function $getMaxListeners(that) {
    if (that._maxListeners === undefined) return EventEmitter.defaultMaxListeners;
    return that._maxListeners;
}

// These standalone emit* functions are used to optimize calling of event
// handlers for fast cases because emit() itself often has a variable number of
// arguments and can be deoptimized because of that. These functions always have
// the same number of arguments and thus do not get deoptimized, so the code
// inside them can execute faster.
export function emitNone(handler, isFn, self) {
    if (isFn) handler.call(self);
    else {
        const len = handler.length;
        const listeners = arrayClone(handler, len);
        for (let i = 0; i < len; ++i) listeners[i].call(self);
    }
}

export function emitOne(handler, isFn, self, arg1) {
    if (isFn) handler.call(self, arg1);
    else {
        const len = handler.length;
        const listeners = arrayClone(handler, len);
        for (let i = 0; i < len; ++i) listeners[i].call(self, arg1);
    }
}

export function emitTwo(handler, isFn, self, arg1, arg2) {
    if (isFn) handler.call(self, arg1, arg2);
    else {
        const len = handler.length;
        const listeners = arrayClone(handler, len);
        for (let i = 0; i < len; ++i) listeners[i].call(self, arg1, arg2);
    }
}

export function emitThree(handler, isFn, self, arg1, arg2, arg3) {
    if (isFn) handler.call(self, arg1, arg2, arg3);
    else {
        const len = handler.length;
        const listeners = arrayClone(handler, len);
        for (let i = 0; i < len; ++i) listeners[i].call(self, arg1, arg2, arg3);
    }
}

export function emitMany(handler, isFn, self, args) {
    if (isFn) handler.apply(self, args);
    else {
        const len = handler.length;
        const listeners = arrayClone(handler, len);
        for (let i = 0; i < len; ++i) listeners[i].apply(self, args);
    }
}

export function listenerCount(type) {
    const events = this._events;

    if (events) {
        const evlistener = events[type];

        if (typeof evlistener === 'function') {
            return 1;
        } else if (evlistener) {
            return evlistener.length;
        }
    }

    return 0;
}

// About 1.5x faster than the two-arg version of Array#splice().
export function spliceOne(list, index) {
    for (let i = index, k = i + 1, n = list.length; k < n; i += 1, k += 1) list[i] = list[k];
    list.pop();
}

export function arrayClone(arr, i) {
    const copy = new Array(i);
    while (i--) copy[i] = arr[i];
    return copy;
}
