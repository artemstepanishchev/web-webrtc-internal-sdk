import EventEmitter from './events.js';
import ControlMessage from './crl_message_cli.js';
import LogWrapper from './log_wrapper.js';
import assert from './assert.js';
import { createSequence } from './utils/misc.js';

const seq = createSequence();
const DEFAULT_REQUEST_TIMEOUT = 15000;
const baseTime = Date.now();

class ControlSession extends EventEmitter {
    constructor(options) {
        super();

        options = options || {};
        this._seq = seq();
        this._server = options.server || window.location.hostname + ':' + window.location.port;
        this._serverPath = options.serverPath || '';
        this._defaultRequestTimeout = options._defaultRequestTimeout || DEFAULT_REQUEST_TIMEOUT;
        this._protocol =
            options.protocol || (window.location.protocol === 'https:' ? 'wss:' : 'ws:');
        this._subProtocol = options.subProtocol || 'vcvas';
        this._logger = new LogWrapper(options.logger, () => `cs:${this._seq}-${this._sessionId}`);
        this._id = (Math.floor(Math.random() * 0xf0000000) + 0x10000000).toString(16);
        this._requestCallbacks = {};
        this._responseCallbacks = {};
        this._sessionId = options.sessionId;
        this._req_src = options.src; // eslint-disable-line camelcase
        this._req_seq = options.req_seq || 0; // eslint-disable-line camelcase
        this._keepAliveInterval =
            typeof options.keepAliveInterval != 'undefined' ? options.keepAliveInterval : 0;
        this._instanceId = options.instanceId;
        this._minReqSeq = this._req_seq;
        this._logger.logDebug('Connecting...');
        const createWebSocket = options.createWebSocket
            ? options.createWebSocket
            : (...args) => new WebSocket(...args);
        this._ws = createWebSocket(
            this._protocol + '//' + this._server + '/' + this._serverPath,
            this._subProtocol
        );
        this._closed = false;
        this._closedEventSent = false;

        this._ws.onerror = function (error) {
            //WS Will not give us any useful information (see developer console)
            //Because of security issues
            this._logger.logError('ws error');
            this.emit(ControlSession.event.ERROR, error);
            if (!this._closed) {
                this.close();
                this._notifyAboutClose();
            }
        }.bind(this);

        this._ws.onopen = function () {
            this._logger.log('open');
            this._restartKeepAliveTimer();
            this.emit(ControlSession.event.CONNECTED);
        }.bind(this);

        this._ws.onclose = function (event) {
            this._closed = true;
            this._logger.log('close: code:' + event.code + ' message: ' + event.reason);
            this._notifyAboutClose();
        }.bind(this);

        this._ws.onmessage = message => {
            const messageTS = Date.now() - baseTime;
            const controlMessage = new ControlMessage(message.data);
            if (controlMessage.getEvent() === ControlMessage.event.KEEP_ALIVE_RES) {
                this._logger.logDebug(controlMessage.toString());
            } else {
                this._logger.logCollapsed(
                    controlMessage.toString(),
                    `recv-${controlMessage.getRequestSeq()}: ${controlMessage.getEvent()}`
                );
            }
            if (controlMessage.isResponse() && typeof controlMessage.isSuccess() != 'boolean') {
                this._logger.logError("Broken response: no 'success' field");
            }
            if (controlMessage.getRequestSrc() === this._req_src) {
                const reqSeq = controlMessage.getRequestSeq();
                if (typeof reqSeq == 'number' && reqSeq < this._minReqSeq) {
                    //TODO This check could be re-done based on version field
                    //if version < this._instanceId then discard
                    this._logger.logWarn(
                        'Discarding message, since req_seq is too small(' +
                            reqSeq +
                            ' < ' +
                            this._minReqSeq +
                            '). Most likely we got response on request sent before connection reset.'
                    );
                    return;
                }
            }
            controlMessage.setReceivedTimestamp(messageTS);
            const reqId = this._getRequestId(controlMessage);
            const callback = this._requestCallbacks[reqId] || this._responseCallbacks[reqId];

            controlMessage.setConnection(this._instanceId);

            this._restartKeepAliveTimer();

            if (controlMessage.getType() === ControlMessage.type.CONNECTION) {
                //We don't need to bother upper level with KEEP_ALIVE_REQ/RES
                if (controlMessage.getEvent() === ControlMessage.event.KEEP_ALIVE_REQ) {
                    this.respondOK(controlMessage);
                } else if (controlMessage.getEvent() === ControlMessage.event.REGISTER_REQ) {
                    this.respondOK(controlMessage, {
                        body: {
                            // eslint-disable-next-line camelcase
                            confirmed_instance:
                                controlMessage.getBody().instance || String(Math.random()),
                        },
                    });
                }
            } else {
                try {
                    this.emit(ControlSession.event.MESSAGE, controlMessage);
                } catch (e) {
                    this._logger.logError(e.message);
                    this._logger.logError(e.stack);
                }
            }
            if (callback) {
                delete this._requestCallbacks[reqId];
                delete this._responseCallbacks[reqId];
                callback(null, controlMessage);
            }
        };
    }

    _notifyAboutClose() {
        if (!this._closedEventSent) {
            this._closedEventSent = true;
            this.emit(ControlSession.event.CLOSE);
        }
    }

    setInstanceId(newInstanceId) {
        if (this._instanceId !== newInstanceId) {
            this._instanceId = newInstanceId;
            this._requestCallbacks = {};
            this._responseCallbacks = {};
            this._minReqSeq = this._req_seq;
        }
    }

    getInstanceId() {
        return this._instanceId;
    }

    setSessionId(newSessionId) {
        this._sessionId = newSessionId;
    }

    _restartKeepAliveTimer() {
        clearTimeout(this._keepAliveTimer);
        if (this._keepAliveInterval > 0) {
            this._keepAliveTimer = setTimeout(() => {
                if (this.isConnected()) {
                    this.request(
                        {
                            event: ControlMessage.event.KEEP_ALIVE_REQ,
                            id: this._id,
                            type: ControlMessage.type.CONNECTION,
                        },
                        // eslint-disable-next-line no-unused-vars
                        (err, response) => {}
                    );
                }
            }, this._keepAliveInterval);
        }
    }

    send(crlMessage, callback) {
        let errMessage;
        const messageConnectionInstanceId = crlMessage.getConnection();
        if (
            typeof messageConnectionInstanceId != 'undefined' &&
            messageConnectionInstanceId !== this._instanceId
        ) {
            this._logger.logCollapsed(
                crlMessage.toString(),
                `send-${crlMessage.getRequestSeq()}: ${crlMessage.getEvent()}`
            );
            errMessage = 'Cannot send message using different connection instance.';
            this._logger.logError(errMessage);
            return callback && callback(new Error(errMessage));
        }

        if (!crlMessage.getType()) {
            crlMessage.setType(ControlMessage.type.SESSION);
        }

        if (crlMessage.getType() === ControlMessage.type.SESSION) {
            if (!crlMessage.getVersion()) {
                crlMessage.setVersion(this._instanceId);
            }

            if (!crlMessage.getId()) {
                crlMessage.setId(this._sessionId);
            }
        }

        const readyState = this._ws.readyState;
        if (readyState === ControlSession.readyState.OPEN) {
            if (crlMessage.getEvent() === ControlMessage.event.KEEP_ALIVE_REQ) {
                this._logger.logDebug(crlMessage.toString());
            } else {
                this._logger.logCollapsed(
                    crlMessage.toString(),
                    `send-${crlMessage.getRequestSeq()}: ${crlMessage.getEvent()}`
                );
            }
            crlMessage.setSentTimestamp(Date.now() - baseTime);
            this._ws.send(crlMessage.toString());
            callback && callback(null);
        } else if (readyState === ControlSession.readyState.CONNECTING) {
            setTimeout(() => {
                this._logger.logCollapsed(
                    crlMessage.toString(),
                    `pending send-${crlMessage.getRequestSeq()}: ${crlMessage.getEvent()}`
                );
                this.send(crlMessage, callback);
            }, 1000);
        } else {
            this._logger.logCollapsed(
                crlMessage.toString(),
                `send-${crlMessage.getRequestSeq()}: ${crlMessage.getEvent()}`
            );

            errMessage =
                'Cannot send control message, because web socket not ready. WS State: ' +
                ControlSession.readyStateDescription[readyState];
            this._logger.logError(errMessage);
            callback && callback(new Error(errMessage));
            if (readyState === ControlSession.readyState.CLOSED && !this._closed) {
                //This is very strange case, but sometimes onclose event
                //doesn't fire on native websocket, so we can fix it here
                this._logger.logDebug('Emulating CLOSE event on websocket.');
                this.close();
                this._notifyAboutClose();
            }
        }
    }

    request(request, callback) {
        //Callback will be called in case if we receive RES event related to this request
        //callback (err, resMessage)

        assert(typeof request == 'object' && typeof request.event == 'string');

        request.req_src = this._req_src; // eslint-disable-line camelcase
        request.req_seq = this._req_seq; // eslint-disable-line camelcase

        this._req_seq++;

        const requestId = this._getRequestId(request);

        const timer = setTimeout(() => {
            if (typeof callback == 'function') {
                delete this._requestCallbacks[requestId];
                callback(
                    new Error(
                        `WS request ${requestId} timed out, socket state: ${this._ws.readyState}`
                    )
                );
            }
        }, this._defaultRequestTimeout);

        if (typeof callback == 'function') {
            this._requestCallbacks[requestId] = (...args) => {
                clearTimeout(timer);
                callback(...args);
            };
        }

        this.send(new ControlMessage(request), err => {
            clearTimeout(timer);
            if (err && this._requestCallbacks[requestId]) {
                delete this._requestCallbacks[requestId];
                callback && callback(err);
            }
        });
    }

    respond(request, response, callback) {
        //Callback will be called in case if we receive ACK event related to this response
        //Or if there is no ACK response expected
        //callback (err, ackMessage)

        let requestId;
        if (request) {
            response.event = ControlMessage.responseEvent[request.getEvent()];

            response.req_seq = request.getRequestSeq(); // eslint-disable-line camelcase
            response.req_src = request.getRequestSrc(); // eslint-disable-line camelcase
            response.rx_ts = request.getReceivedTime(); // eslint-disable-line camelcase
            response.type = request.getType();
            response.id = request.getId();
            response.partition = request.getPartition();

            response.version = request.getVersion();

            requestId = this._getRequestId(request);
        } else {
            requestId = this._getRequestId(response);
        }

        if (typeof callback == 'function' && ControlMessage.ackEvent[response.event]) {
            this._responseCallbacks[requestId] = callback;
        }

        const message = new ControlMessage(response);
        if (request) {
            message.setConnection(request.getConnection());
        }

        this.send(
            message,
            function (err) {
                if (err || !ControlMessage.ackEvent[response.event]) {
                    delete this._responseCallbacks[requestId];
                    callback && callback(err);
                }
            }.bind(this)
        );
    }

    respondOK(request, response, callback) {
        response = response || {};
        response.success = true;

        this.respond(request, response, callback);
    }

    respondError(request, errCode, errMessage, callback) {
        assert(typeof errCode == 'number');
        errMessage = errMessage || ControlMessage.errMessage[errCode];

        this.respond(
            request,
            {
                success: false,
                err_code: errCode, // eslint-disable-line camelcase
                err_message: errMessage, // eslint-disable-line camelcase
            },
            callback
        );
    }

    respondACK(response, ack, callback) {
        //Callback will be called as soon as we send ack, since there is no response on ACK
        //callback(err)

        ack.event = ControlMessage.ackEvent[response.getEvent()];
        ack.body = response.body || {};

        ack.req_src = response.getRequestSrc(); // eslint-disable-line camelcase
        ack.req_seq = response.getRequestSeq(); // eslint-disable-line camelcase
        ack.rx_ts = response.getReceivedTime(); // eslint-disable-line camelcase
        ack.version = response.getVersion();
        ack.type = response.getType();
        ack.id = response.getId();

        const ackMessage = new ControlMessage(ack);
        ackMessage.setConnection(response.getConnection());
        this.send(ackMessage, callback);
    }

    respondACKOK(response, ack, callback) {
        ack = ack || { body: {} };
        ack.success = true;
        this.respondACK(response, ack, callback);
    }

    respondACKError(response, errCode, errMessage, callback) {
        assert(typeof errCode == 'number');
        errMessage = errMessage || ControlMessage.errMessage[errCode];

        this.respondACK(
            response,
            {
                success: false,
                err_code: errCode, // eslint-disable-line camelcase
                err_message: errMessage, // eslint-disable-line camelcase
            },
            callback
        );
    }

    close() {
        if (!this._closed) {
            this._closed = true;

            this._logger.logDebug('Closing ws connection...');
            clearTimeout(this._keepAliveTimer);
            this._ws.close();
        } else {
            this._logger.log('ws already closed.');
        }
    }

    isConnected() {
        return !this._closed && this._ws.readyState === ControlSession.readyState.OPEN;
    }

    isConnecting() {
        return !this._closed && this._ws.readyState === ControlSession.readyState.CONNECTING;
    }

    getRequestSeq() {
        return this._req_seq;
    }

    _getRequestId(message) {
        if (message instanceof ControlMessage) {
            message = message.toObject();
        }
        return message.req_src + (message.id || this._sessionId) + message.req_seq;
    }
}

ControlSession.event = {
    ERROR: 'ERROR',
    CLOSE: 'CLOSE',
    MESSAGE: 'MESSAGE',
    CONNECTED: 'CONNECTED',
};

ControlSession.readyState = {
    CONNECTING: 0,
    OPEN: 1,
    CLOSING: 2,
    CLOSED: 3,
};

ControlSession.readyStateDescription = {
    0: 'CONNECTING',
    1: 'OPEN',
    2: 'CLOSING',
    3: 'CLOSED',
};

export default ControlSession;
