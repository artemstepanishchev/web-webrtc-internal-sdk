export default class ControlChannel {
    /**
     * @param {{ pc: RTCPeerConnection, label: string, onClose: Function, onDataTimeout: Function, logger: LogWrapper}} options
     */
    constructor(options) {
        this._options = options;
        this._pingTimer = null;
        this._lastIncomingDataTimestamp = null;
        this._sendMediaPingTimeout = 1000;
        this._noIncomingDataEventRaised = false;
        this._pingCount = 0;
        this._noMediaPingTimeout = this._calculateNoPingTimeout(options);

        this._init();
    }

    _calculateNoPingTimeout({ noMediaPingTimeout }) {
        const timeout =
            typeof noMediaPingTimeout === 'number' ? Math.max(0, noMediaPingTimeout) : 12000;

        return timeout > 0 ? Math.max(this._sendMediaPingTimeout + 100, timeout) : timeout;
    }

    _init() {
        const { logger, pc, onClose, label } = this._options;
        logger.log('create controlChannel');

        this._channel = pc.createDataChannel(label);

        this._channel.onopen = () => {
            this._lastIncomingDataTimestamp = Date.now();
            this._noIncomingDataEventRaised = false;
            logger.log('controlChannel.onopen');
            clearTimeout(this._pingTimer);
            this._makeDelayedPing();
        };

        this._channel.onclose = () => {
            logger.log('controlChannel.onclose');
            clearTimeout(this._pingTimer);
            this._channel = null;
            onClose();
        };

        this._channel.onmessage = () => {
            this._lastIncomingDataTimestamp = Date.now();
        };
    }

    _makeDelayedPing() {
        clearTimeout(this._pingTimer);
        this._pingTimer = setTimeout(this._sendPing, this._sendMediaPingTimeout);
    }

    _sendPing = () => {
        try {
            this._pingCount++;
            this._channel.send(`ping ${this._pingCount}`);
        } catch (e) {
            this._options.logger.logWarn(`Failed to send ping ${this._pingCount}: ${e}`);
        }

        this._makeDelayedPing();
        this.checkPingResponse();
    };

    checkPingResponse() {
        const { logger, onDataTimeout, isP2P } = this._options;

        if (isP2P || this._noIncomingDataEventRaised) {
            return;
        }

        const timeSinceLastDataReceived = Date.now() - this._lastIncomingDataTimestamp;
        if (
            this._noMediaPingTimeout !== 0 &&
            timeSinceLastDataReceived > this._noMediaPingTimeout
        ) {
            logger.logWarn(`No incoming data, ${timeSinceLastDataReceived}ms passed`);
            this._noIncomingDataEventRaised = true;
            onDataTimeout(timeSinceLastDataReceived);
        }
    }
}
