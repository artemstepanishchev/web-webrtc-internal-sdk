import EventEmitter from './events.js';

class StreamRotate extends EventEmitter {
    constructor({ stream }) {
        super();
        this._stream = stream;
        this._rotationId = StreamRotate.rotation.NO;

        this._fps = 30;
        this._timerStep = Math.round(1000 / this._fps);

        this._lastSourceWidth = undefined;
        this._lastSourceHeight = undefined;
        this._video = undefined;
        this._canvas = undefined;
        this._updateTimer = undefined;
        this._animationFrameRequest = undefined;

        this._normalizedStream = undefined;

        this._switchToOriginalStream();
    }
    setRotation(rotationId) {
        if (this._rotationId !== rotationId) {
            this._rotationId = rotationId;
            this._switchToOriginalStream();
            if (rotationId !== StreamRotate.rotation.NO) {
                this._video = document.createElement('video');
                this._video.muted = true;
                this._video.srcObject = this._stream;
                const pass = () => void 0;
                this._video.play().then(pass, pass);
                this._canvas = document.createElement('canvas');
                this._drawFrame();
            }
        }
    }
    getNormalizedStream() {
        return this._normalizedStream;
    }
    _drawFrame() {
        if (this._video) {
            const sourceWidth = this._video.videoWidth;
            const sourceHeight = this._video.videoHeight;
            if (sourceWidth > 2 && sourceHeight > 2) {
                if (
                    this._normalizedStream === this._stream ||
                    this._lastSourceWidth !== sourceWidth ||
                    this._lastSourceHeight !== sourceHeight
                ) {
                    const PI = Math.PI;
                    const halfPI = PI / 2;
                    const angle = [0, halfPI, PI, -halfPI][this._rotationId];
                    let destinationWidth, destinationHeight;

                    if (
                        [StreamRotate.rotation.NO, StreamRotate.rotation.CCW_180].includes(
                            this._rotationId
                        )
                    ) {
                        destinationWidth = sourceWidth;
                        destinationHeight = sourceHeight;
                    } else {
                        // noinspection JSSuspiciousNameCombination
                        destinationWidth = sourceHeight;
                        // noinspection JSSuspiciousNameCombination
                        destinationHeight = sourceWidth;
                    }
                    this._lastSourceWidth = sourceWidth;
                    this._lastSourceHeight = sourceHeight;
                    this._canvas.width = destinationWidth;
                    this._canvas.height = destinationHeight;
                    this._context = this._canvas.getContext('2d');
                    this._context.translate(destinationWidth / 2, destinationHeight / 2);
                    this._context.rotate(angle);
                    this._normalizedStream = this._canvas.captureStream();
                    this._stream.getAudioTracks().forEach(audioTrack => {
                        this._normalizedStream.addTrack(audioTrack);
                    });
                    this.emit(StreamRotate.event.STREAM_UPDATED, this._normalizedStream);
                }
                this._context.drawImage(
                    this._video,
                    -sourceWidth / 2,
                    -sourceHeight / 2,
                    sourceWidth,
                    sourceHeight
                );
            } else {
                this._switchToOriginalStream();
            }
            clearTimeout(this._updateTimer);
            this._updateTimer = setTimeout(() => {
                this._animationFrameRequest = window.requestAnimationFrame(() => {
                    this._drawFrame();
                });
            }, this._timerStep);
        }
    }
    _switchToOriginalStream() {
        if (this._normalizedStream !== this._stream) {
            clearTimeout(this._updateTimer);
            cancelAnimationFrame(this._animationFrameRequest);
            this._context = undefined;
            this._canvas = undefined;
            this._video = undefined;
            this._lastSourceWidth = undefined;
            this._lastSourceHeight = undefined;
            this._normalizedStream = this._stream;
            this.emit(StreamRotate.event.STREAM_UPDATED, this._normalizedStream);
        }
    }
    destroy() {
        clearTimeout(this._updateTimer);
        cancelAnimationFrame(this._animationFrameRequest);
        this._context = undefined;
        this._canvas = undefined;
        this._video = undefined;
        this._lastSourceWidth = undefined;
        this._lastSourceHeight = undefined;
        this._normalizedStream = this._stream;
        this.removeAllListeners();
    }
}

StreamRotate.event = {
    STREAM_UPDATED: 'stream_updated',
};

StreamRotate.rotation = {
    //  Rotation         : Action to normalize
    NO: 0, //   0° rotation     : None
    CCW_90: 1, //  90° CCW rotation :  90° CW  rotation
    CCW_180: 2, // 180° rotation     : 180° CW  rotation
    CCW_270: 3, // 270° CCW rotation :  90° CCW rotation
};

export default StreamRotate;
