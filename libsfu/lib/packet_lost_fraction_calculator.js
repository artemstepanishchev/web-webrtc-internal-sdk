class PacketLossFractionCalculator {
    constructor() {
        this._lastUpdate = undefined;
        this._totalPackets = 0;
        this._totalLost = 0;
        this._fractionLost = 0;
        this._minInterval = 3000;
    }
    update({ totalPackets, totalLost }) {
        const updateTime = Date.now();
        if (typeof this._lastUpdate === 'undefined') {
            this._totalLost = totalLost;
            this._totalPackets = totalPackets;
            this._lastUpdate = updateTime;
        } else {
            const timeSinceLastUpdate = updateTime - this._lastUpdate;
            if (timeSinceLastUpdate >= this._minInterval) {
                if (totalPackets < this._totalPackets) {
                    this._totalPackets = 0;
                    this._totalLost = 0;
                }
                if (this._totalLost > totalLost) {
                    //This can happens if nack recovered some packets
                    this._totalLost = totalLost;
                }
                const newPackets = totalPackets - this._totalPackets;
                const newLost = Math.min(totalLost - this._totalLost, newPackets);

                if (newPackets <= 0) {
                    this._fractionLost = 0;
                } else {
                    this._fractionLost = newLost / newPackets;
                }
                this._lastUpdate = updateTime;
                this._totalPackets = totalPackets;
                this._totalLost = totalLost;
            }
        }
    }
    getFractionLost() {
        return this._fractionLost;
    }
}

export default PacketLossFractionCalculator;
