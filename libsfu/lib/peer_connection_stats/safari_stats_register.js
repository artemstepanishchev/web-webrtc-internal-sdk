import { AbstractStatsRegister } from './abstract_stats_register.js';
import PacketLossFractionCalculator from '../packet_lost_fraction_calculator.js';
import { getPrefferedCodecNameBySSRC } from '../utils/codecs.js';

export class SafariStatsRegister extends AbstractStatsRegister {
    /** @param {UpdateStatsParams} params */
    updateStats({ pc, remoteSDP }) {
        return pc.getStats().then(response => {
            const newTime = Date.now();
            const newStat = {};
            const ssrcByTrackId = {};
            const mediaTypeByTrackId = {};
            const trackReports = [];
            response.forEach(report => {
                if (report.type === 'inbound-rtp' || report.type === 'outbound-rtp') {
                    const match = report.id.match(
                        /^RTC(Outbound|Inbound)RTP(Audio|Video)Stream_(\d+)$/
                    );
                    if (match) {
                        const mediaType = match[2].toLowerCase();
                        const ssrc = match[3];
                        const oldStat = this._statBySSRC[ssrc] || {};
                        const oldTime = oldStat.lastUpdate;
                        const oldBytes = oldStat.bytes;
                        const newBytes = Number(report.bytesReceived) || Number(report.bytesSent);
                        const newPackets =
                            Number(report.packetsReceived) || Number(report.packetsSent);
                        const newPacketsLost = Number(report.packetsLost);
                        const jitter = report.jitter * 1000 || 0;
                        const fractionLostCalculator =
                            oldStat._fractionLostCalculator || new PacketLossFractionCalculator();
                        fractionLostCalculator.update({
                            totalPackets: newPackets,
                            totalLost: newPacketsLost,
                        });

                        newStat[ssrc] = Object.assign(newStat[ssrc] || {}, {
                            bytes: newBytes,
                            speed: this._getKBPerSecond(oldBytes, newBytes, oldTime, newTime),
                            codecName: getPrefferedCodecNameBySSRC(remoteSDP, ssrc),
                            lastUpdate: newTime,
                            packetsLost: report.packetsLost,
                            packets: newPackets,
                            packetsLostFraction: fractionLostCalculator.getFractionLost(),
                            currentDelay: jitter,
                            jitter: jitter,
                            _fractionLostCalculator: fractionLostCalculator,
                        });
                        if (mediaType === 'video') {
                            newStat[ssrc] = Object.assign(newStat[ssrc] || {}, {
                                firs: report.firCount || 0,
                                plis: report.pliCount || 0,
                                nacks: report.nackCount || 0,
                            });
                        }
                        const trackId = report.trackId;
                        if (typeof trackId === 'string') {
                            ssrcByTrackId[trackId] = ssrc;
                            mediaTypeByTrackId[trackId] = mediaType;
                        }
                    }
                } else if (report.type === 'track') {
                    const match = report.id.match(
                        /^RTCMediaStreamTrack_remote_(audio|video)_(.*)_(\d+)$/
                    );
                    if (match) {
                        const ssrc = match[3];
                        const oldStat = this._statBySSRC[ssrc] || {};
                        const oldTime = oldStat.lastUpdate;
                        const mediaType = match[1];
                        if (mediaType === 'audio') {
                            newStat[ssrc] = newStat[ssrc] || {};
                            newStat[ssrc].level = report.audioLevel;
                        } else if (mediaType === 'video') {
                            newStat[ssrc] = Object.assign(newStat[ssrc] || {}, {
                                frameWidth: report.frameWidth,
                                frameHeight: report.frameHeight,
                                //report.framesPerSecond exists, but always =0
                                frameRateOut: this._getValuePerSecond(
                                    oldStat.framesDecoded,
                                    report.framesDecoded,
                                    oldTime,
                                    newTime
                                ),
                                framesDecoded: report.framesDecoded,
                            });
                        }
                    } else {
                        trackReports.push(report);
                    }
                }
            });

            trackReports.forEach(report => {
                const ssrc = ssrcByTrackId[report.id];
                if (typeof ssrc === 'string') {
                    const oldStat = this._statBySSRC[ssrc] || {};
                    const oldTime = oldStat.lastUpdate;
                    const mediaType = mediaTypeByTrackId[report.id];
                    if (mediaType === 'audio') {
                        newStat[ssrc] = newStat[ssrc] || {};
                        newStat[ssrc].level = report.audioLevel;
                    } else if (mediaType === 'video') {
                        newStat[ssrc] = Object.assign(newStat[ssrc] || {}, {
                            frameWidth: report.frameWidth,
                            frameHeight: report.frameHeight,
                            frameRateOut: this._getValuePerSecond(
                                oldStat.framesDecoded,
                                report.framesDecoded,
                                oldTime,
                                newTime
                            ),
                            framesDecoded: report.framesDecoded,
                        });
                    }
                }
            });

            this._statBySSRC = newStat;
        });
    }
}
