import { AbstractStatsRegister } from './abstract_stats_register';
import PacketLossFractionCalculator from '../packet_lost_fraction_calculator.js';
import { createNetQualityCalculator } from '../net_quality.js';

export class ChromeStatsRegister extends AbstractStatsRegister {
    /** @param {UpdateStatsParams} params */
    updateStats({ pc }) {
        return new Promise((resolve, reject) => {
            pc.getStats(resolve, null, reject);
        }).then(response => {
            const newTime = Date.now();
            const newStat = {};
            let connectedLocalCandidateId;
            const localCandidates = {};
            response.result().forEach(report => {
                if (report.type === 'ssrc') {
                    const ssrc = report.stat('ssrc');
                    if (ssrc) {
                        const oldStat = this._statBySSRC[ssrc] || {};
                        const oldTime = oldStat.lastUpdate;
                        const oldBytes = oldStat.bytes;
                        const newBytes =
                            Number(report.stat('bytesReceived')) ||
                            Number(report.stat('bytesSent'));
                        const newPackets =
                            Number(report.stat('packetsReceived')) ||
                            Number(report.stat('packetsSent'));
                        const newPacketsLost = Number(report.stat('packetsLost'));
                        const fractionLostCalculator =
                            oldStat._fractionLostCalculator || new PacketLossFractionCalculator();
                        fractionLostCalculator.update({
                            totalPackets: newPackets,
                            totalLost: newPacketsLost,
                        });
                        const nqCalculator =
                            oldStat._nqCalculator ||
                            createNetQualityCalculator('chromeLikeStat', report.stat('mediaType'));
                        const fullStat = report.names().reduce((acc, name) => {
                            acc[name] = report.stat(name);
                            return acc;
                        }, {});

                        const connectionState = pc?.connectionState;

                        const netQuality = this._isInitialConnectionPassed
                            ? nqCalculator.update(fullStat, connectionState)
                            : oldStat.netQuality;

                        //TODO We need something like max Packet Lost in last 10 seconds
                        //Otherwise sendQuality is too sesitive to fast changing packet lost

                        newStat[ssrc] = {
                            bytes: newBytes,
                            packets: newPackets,
                            speed: this._getKBPerSecond(oldBytes, newBytes, oldTime, newTime),
                            codecName: report.stat('googCodecName'),
                            lastUpdate: newTime,
                            encodeUsage: report.stat('googEncodeUsagePercent'),
                            packetsLost: newPacketsLost,
                            packetsLostFraction: fractionLostCalculator.getFractionLost(),
                            currentDelay: Number(report.stat('googCurrentDelayMs')),
                            rtt: Number(report.stat('googRtt')),
                            netQuality,
                            nqiDetails: nqCalculator.details(),
                            _fractionLostCalculator: fractionLostCalculator,
                            _nqCalculator: nqCalculator,
                        };

                        const mediaType = report.stat('mediaType');
                        if (mediaType === 'audio') {
                            newStat[ssrc].level =
                                Number(report.stat('audioInputLevel')) ||
                                Number(report.stat('audioOutputLevel'));
                            newStat[ssrc].jitter = Number(report.stat('googJitterReceived')) || 0;
                        } else if (mediaType === 'video') {
                            newStat[ssrc] = Object.assign(newStat[ssrc], {
                                // jitter stat for video SSRCs is not available, see: https://docs.google.com/document/d/1z-D4SngG36WPiMuRvWeTMN7mWQXrf1XKZwVl3Nf1BIE/edit
                                jitter: 0,
                                limResCpu: report.stat('googCpuLimitedResolution') === 'true',
                                limResBw: report.stat('googBandwidthLimitedResolution') === 'true',
                                firs:
                                    Number(report.stat('googFirsReceived')) ||
                                    Number(report.stat('googFirsSent')) ||
                                    0,
                                plis:
                                    Number(report.stat('googPlisReceived')) ||
                                    Number(report.stat('googPlisSent')) ||
                                    0,
                                nacks:
                                    Number(report.stat('googNacksReceived')) ||
                                    Number(report.stat('googNacksSent')) ||
                                    0,
                                frameWidth:
                                    Number(report.stat('googFrameWidthReceived')) ||
                                    Number(report.stat('googFrameWidthSent')) ||
                                    0,
                                frameHeight:
                                    Number(report.stat('googFrameHeightReceived')) ||
                                    Number(report.stat('googFrameHeightSent')) ||
                                    0,
                                frameRateOut:
                                    Number(report.stat('googFrameRateReceived')) ||
                                    Number(report.stat('googFrameRateInput')) ||
                                    0,
                                frameRateIn:
                                    Number(report.stat('googFrameRateSent')) ||
                                    Number(report.stat('googFrameRateOutput')) ||
                                    0,
                            });
                        }
                    }
                } else if (report.type === 'VideoBwe') {
                    this._videoBwe = {
                        semb: Number(report.stat('googAvailableSendBandwidth')),
                    };
                } else if (report.type === 'googCandidatePair') {
                    if (report.stat('googActiveConnection') === 'true') {
                        connectedLocalCandidateId = report.stat('localCandidateId');
                    }
                } else if (report.type === 'localcandidate') {
                    localCandidates[report.id] = report;
                }
            });

            if (
                connectedLocalCandidateId &&
                typeof localCandidates[connectedLocalCandidateId] === 'object'
            ) {
                const legacyNetworkType = localCandidates[connectedLocalCandidateId].stat(
                    'networkType'
                );
                const networkType =
                    {
                        lan: 'ethernet',
                        wlan: 'wifi',
                        wwan: 'cellular',
                        vpn: 'vpn',
                        loopback: 'unknown',
                        wildcard: 'unknown',
                    }[legacyNetworkType] || legacyNetworkType;
                this._setNetworkType(networkType);
            }
            this._statBySSRC = newStat;
        });
    }
}
