import { AbstractStatsRegister } from './abstract_stats_register.js';
import PacketLossFractionCalculator from '../packet_lost_fraction_calculator.js';
import { getPrefferedCodecNameBySSRC } from '../utils/codecs.js';

export class FirefoxStatsRegister extends AbstractStatsRegister {
    /** @param {UpdateStatsParams} params */
    updateStats({
        pc,
        remoteSDP,
        isStreamRemote,
        getRemoteVideoSSRCByStream,
        getLocalStreamsStreamsWithSSRCs,
    }) {
        return pc.getStats(null).then(report => {
            const newTime = Date.now();
            const newStat = {};
            const normalizedReport = [];
            if (typeof report.values === 'function') {
                for (const stat of report.values()) {
                    normalizedReport.push(stat);
                }
            } else {
                for (const id in report) {
                    if (report.hasOwnProperty(id)) {
                        const stat = report.get(id);
                        normalizedReport.push(stat);
                    }
                }
            }
            normalizedReport.forEach(item => {
                let ssrc = item.ssrc;
                if (
                    typeof ssrc !== 'undefined' &&
                    item.type === 'inbound-rtp' &&
                    typeof item.bytesReceived === 'number'
                ) {
                    ssrc = String(ssrc);
                    const oldStat = this._statBySSRC[ssrc] || {};
                    const newBytesReceived = Number(item.bytesReceived);
                    const oldBytesReceived = oldStat.bytes;
                    const oldTime = oldStat.lastUpdate;
                    const speed = this._getKBPerSecond(
                        oldBytesReceived,
                        newBytesReceived,
                        oldTime,
                        newTime
                    );
                    const jitter = item.jitter;
                    const newPackets = Number(item.packetsReceived) || Number(item.packetsSent);
                    const newPacketsLost = item.packetsLost;
                    const fractionLostCalculator =
                        oldStat._fractionLostCalculator || new PacketLossFractionCalculator();
                    fractionLostCalculator.update({
                        totalPackets: newPackets,
                        totalLost: newPacketsLost,
                    });

                    newStat[ssrc] = {
                        bytes: newBytesReceived,
                        codecName: getPrefferedCodecNameBySSRC(remoteSDP, ssrc),
                        frameRateOut:
                            typeof item.framerateMean !== 'undefined'
                                ? Math.round(Number(item.framerateMean))
                                : 30,
                        speed: speed,
                        lastUpdate: newTime,
                        packetsLost: newPacketsLost,
                        packets: newPackets,
                        packetsLostFraction: fractionLostCalculator.getFractionLost(),
                        currentDelay: jitter,
                        jitter: jitter,
                        firs: Number(item.firCount) || 0,
                        plis: Number(item.pliCount) || 0,
                        nacks: Number(item.nackCount) || 0,
                        _fractionLostCalculator: fractionLostCalculator,
                    };
                }
            });

            Array.from(document.querySelectorAll('video')).forEach(video => {
                if (!isStreamRemote(video.srcObject)) return;

                const videoSSRC = getRemoteVideoSSRCByStream(video.srcObject);
                newStat[videoSSRC] = newStat[videoSSRC] || {};
                newStat[videoSSRC].frameWidth = video.videoWidth;
                newStat[videoSSRC].frameHeight = video.videoHeight;
            });

            getLocalStreamsStreamsWithSSRCs().forEach(([localStreamInfo, videoSSRC]) => {
                newStat[videoSSRC] = newStat[videoSSRC] || {};
                newStat[videoSSRC].frameWidth = localStreamInfo.width;
                newStat[videoSSRC].frameHeight = localStreamInfo.height;
            });

            this._statBySSRC = newStat;
        });
    }
}
