import { AbstractStatsRegister } from './abstract_stats_register.js';
import PacketLossFractionCalculator from '../packet_lost_fraction_calculator.js';

export class ChromeStandardStatsRegister extends AbstractStatsRegister {
    /** @param {UpdateStatsParams} params */
    updateStats({ pc }) {
        return pc.getStats().then(response => {
            const newTime = Date.now();
            const newStat = {};
            const ssrcByTrackId = {};
            const mediaTypeByTrackId = {};
            const codecNameById = {};
            const trackReports = [];
            let connectedLocalCandidateId;
            const localCandidates = {};
            response.forEach(report => {
                if (report.type === 'codec') {
                    //mimeType has format: "video/H264"
                    codecNameById[report.id] = report.mimeType.split('/')[1];
                }
                if (report.type === 'inbound-rtp' || report.type === 'outbound-rtp') {
                    const match = report.id.match(
                        /^RTC(Outbound|Inbound)RTP(Audio|Video)Stream_(\d+)$/
                    );
                    if (match) {
                        const mediaType = match[2].toLowerCase();
                        const ssrc = match[3];
                        const oldStat = this._statBySSRC[ssrc] || {};
                        const oldTime = oldStat.lastUpdate;
                        const oldBytes = oldStat.bytes;
                        const newBytes = Number(report.bytesReceived) || Number(report.bytesSent);
                        const newPackets =
                            Number(report.packetsReceived) || Number(report.packetsSent);
                        const newPacketsLost = Number(report.packetsLost) || 0;

                        const fractionLostCalculator =
                            oldStat._fractionLostCalculator || new PacketLossFractionCalculator();
                        fractionLostCalculator.update({
                            totalPackets: newPackets,
                            totalLost: newPacketsLost,
                        });

                        newStat[ssrc] = Object.assign(newStat[ssrc] || {}, {
                            bytes: newBytes,
                            speed: this._getKBPerSecond(oldBytes, newBytes, oldTime, newTime),
                            codecName: 'N/A',
                            codecId: report.codecId,
                            lastUpdate: newTime,
                            packets: newPackets,
                            packetsLost: newPacketsLost,
                            packetsLostFraction: fractionLostCalculator.getFractionLost(),
                            _fractionLostCalculator: fractionLostCalculator,
                        });

                        if (mediaType === 'video') {
                            newStat[ssrc] = Object.assign(newStat[ssrc] || {}, {
                                limResCpu: report.qualityLimitationReason === 'cpu',
                                limResBw: report.googBandwidthLimitedResolution === 'bandwidth',
                                firs: report.firCount || 0,
                                plis: report.pliCount || 0,
                                nacks: report.nackCount || 0,
                            });
                        }
                        const trackId = report.trackId;
                        if (typeof trackId === 'string') {
                            ssrcByTrackId[trackId] = ssrc;
                            mediaTypeByTrackId[trackId] = mediaType;
                        }
                    }
                } else if (report.type === 'track') {
                    trackReports.push(report);
                } else if (report.type === 'candidate-pair') {
                    if (report.state === 'succeeded' && report.nominated) {
                        this._videoBwe.semb = report.availableOutgoingBitrate;
                        connectedLocalCandidateId = report.localCandidateId;
                    }
                } else if (report.type === 'local-candidate') {
                    localCandidates[report.id] = report;
                }
            });
            if (
                connectedLocalCandidateId &&
                typeof localCandidates[connectedLocalCandidateId] === 'object'
            ) {
                this._setNetworkType(localCandidates[connectedLocalCandidateId].networkType);
            }

            trackReports.forEach(report => {
                const ssrc = ssrcByTrackId[report.id];
                if (typeof ssrc === 'string') {
                    const oldStat = this._statBySSRC[ssrc] || {};
                    const oldTime = oldStat.lastUpdate;
                    const mediaType = mediaTypeByTrackId[report.id];
                    //const jitter = report.jitterBufferDelay || 0;
                    const oldJitterBufferEmittedCount = oldStat.jitterBufferEmittedCount || 0;
                    const newJitterBufferEmittedCount = report.jitterBufferEmittedCount || 0;
                    const oldJitterBufferDelay = oldStat.jitterBufferDelay || 0;
                    const newJitterBufferDelay = report.jitterBufferDelay || 0;
                    const jitterBufferEmittedCountChange =
                        newJitterBufferEmittedCount - oldJitterBufferEmittedCount;
                    let jitter;
                    if (jitterBufferEmittedCountChange > 0) {
                        const jitterBufferDelayChange = newJitterBufferDelay - oldJitterBufferDelay;
                        jitter = jitterBufferDelayChange / jitterBufferEmittedCountChange;
                        //jitter is in seconds, so we should convert to ms (to be compatible with old stat)
                        jitter = Math.floor(jitter * 1000);
                    } else {
                        jitter = 0;
                    }
                    newStat[ssrc].jitterBufferDelay = newJitterBufferDelay;
                    newStat[ssrc].jitterBufferEmittedCount = newJitterBufferEmittedCount;
                    newStat[ssrc].jitter = jitter;
                    newStat[ssrc].currentDelay = jitter;
                    if (mediaType === 'audio') {
                        newStat[ssrc] = newStat[ssrc] || {};
                        newStat[ssrc].level = report.audioLevel;
                    } else if (mediaType === 'video') {
                        newStat[ssrc] = Object.assign(newStat[ssrc] || {}, {
                            frameWidth: report.frameWidth,
                            frameHeight: report.frameHeight,
                            frameRateOut: this._getValuePerSecond(
                                oldStat.framesDecoded,
                                report.framesDecoded,
                                oldTime,
                                newTime
                            ),
                            framesDecoded: report.framesDecoded,
                        });
                    }
                }
            });
            Object.keys(newStat).forEach(ssrc => {
                const newStatBySSRC = newStat[ssrc];
                const { codecId } = newStatBySSRC;
                newStatBySSRC.codecName = codecNameById[codecId];
            });

            this._statBySSRC = newStat;
        });
    }
}
