import { FirefoxStatsRegister } from './firefox_stats_register.js';
import { ChromeStatsRegister } from './chrome_stats_register.js';
import { ChromeStandardStatsRegister } from './chrome_standard_stats_register.js';
import { SafariStatsRegister } from './safari_stats_register.js';
import { AbstractStatsRegister } from './abstract_stats_register.js';
import ClientCapabilities from '../client_capabilities.js';

const getBrowserSpecificStatsRegister = () => {
    switch (true) {
        case ClientCapabilities.firefoxLikeStat:
            return FirefoxStatsRegister;

        case ClientCapabilities.chromeStandardLikeStat:
            return ChromeStandardStatsRegister;

        case ClientCapabilities.chromeLikeStat:
            return ChromeStatsRegister;

        case ClientCapabilities.safariLikeStat:
            return SafariStatsRegister;

        default:
            return AbstractStatsRegister;
    }
};

/** @param {StatsRegisterParams} params */
export const getStatsRegister = params => {
    const Register = getBrowserSpecificStatsRegister();
    return new Register(params);
};
