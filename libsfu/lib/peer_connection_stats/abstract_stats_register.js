import { rejectErr } from '../utils/async.js';

/**
 * @typedef {Object} StatsRegisterParams
 * @property {Function} onNetworkTypeChange
 * /

/**
 * @typedef {Object} UpdateStatsParams
 * @property {RTCPeerConnection} pc
 * @property {SDP} remoteSDP
 * // following callbacks are needed only for firefox:
 * @property {Function} isStreamRemote
 * @property {Function} getRemoteVideoSSRCByStream
 * @property {Function} getLocalStreamsStreamsWithSSRCs
 */

export class AbstractStatsRegister {
    /** @param {StatsRegisterParams} params */
    constructor(params) {
        this._params = params;
        this._statBySSRC = {};
        this._videoBwe = {};
        this._networkType = null;
        this._isInitialConnectionPassed = false;
    }

    _getKBPerSecond(oldBytes, newBytes, oldTime, newTime) {
        if (newBytes < oldBytes) {
            return -1;
        }
        const timeDelta = oldTime ? newTime - oldTime : 0;
        const bytesDelta = oldBytes ? newBytes - oldBytes : 0;
        return timeDelta > 0
            ? Math.round((bytesDelta / (timeDelta / 1000) / 1024) * 1000) / 1000
            : -1;
    }

    _getValuePerSecond(oldValue, newValue, oldTime, newTime) {
        const timeDelta = oldTime ? newTime - oldTime : 0;
        const valueDelta = oldValue ? newValue - oldValue : 0;
        return timeDelta > 0 ? Math.round(valueDelta / (timeDelta / 1000)) : 0;
    }

    _setNetworkType(type) {
        if (typeof type === 'string' && type !== this._networkType) {
            this._networkType = type;
            this._params.onNetworkTypeChange(type);
        }
    }

    getStatBySSRC(ssrc) {
        return this._statBySSRC[ssrc];
    }

    getVideoBandwidthEstimation() {
        return this._videoBwe;
    }

    markConnectionPassed() {
        this._isInitialConnectionPassed = true;
    }

    /**
     * @abstract
     * @param {UpdateStatsParams} params
     */
    updateStats() {
        return rejectErr(
            'updateStats method should be implemented in the browser specific stats calculator'
        );
    }
}
