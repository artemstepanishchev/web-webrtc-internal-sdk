import EventEmitter from './events.js';
import LogWrapper from './log_wrapper.js';

class Recovery extends EventEmitter {
    constructor(options) {
        super();

        options = options || {};

        this._sessionId = options.sessionId;
        this._logger = new LogWrapper(options.logger, () => 'rr:' + this._sessionId);
        this._streamRecoveryTimeout = options.streamRecoveryTimeout || 30000;
        this._streamRecoveryTimer = undefined;
        this._recoveryStarted = false;
        this._pendingStreams = {};
        this._remoteStreams = {};
        this._iceConnected = true;
        this._streamsInRecovery = {};
        this._backOffStartInterval = 100;
        this._backOffInterval = this._backOffStartInterval;
        this._backOffMaxInterval = 2000;
        this._backOffTimerReject = undefined;
        this._mediaExist = undefined;
    }

    waitBackOffInterval() {
        if (this._backOffTimerReject) {
            this._backOffTimerReject();
        }
        return new Promise((resolve, reject) => {
            this._logger.log(`Waiting back-off interval ${this._backOffInterval}ms...`);

            const backOffTimer = setTimeout(() => {
                this._backOffTimerReject = undefined;
                resolve();
            }, this._backOffInterval);

            this._backOffTimerReject = () => {
                this._backOffTimerReject = undefined;
                clearTimeout(backOffTimer);
                reject(new Error('Another wait started'));
            };
        });
    }

    setMediaExist(mediaExist) {
        this._mediaExist = mediaExist;
    }

    getMediaExist() {
        return this._mediaExist;
    }

    setSessionId(newSessionId) {
        this._sessionId = newSessionId;
    }

    isRecoveryStarted() {
        return this._recoveryStarted;
    }

    start(remoteStreams) {
        this._backOffInterval = Math.min(this._backOffInterval * 2, this._backOffMaxInterval);
        this._remoteStreams = remoteStreams;
        this._logger.logError('Enter recovery mode. Will try to restore session.');
        clearTimeout(this._streamRecoveryTimer);
        if (!this._recoveryStarted) {
            this.emit(Recovery.event.RECOVERY_STARTED);
            this._recoveryStarted = true;
        }
        this._forEachRemoteStream((stream, id, tapId) => {
            this.emit(Recovery.event.REMOTE_STREAM_RECOVERY_STARTED, stream, id, tapId);
        });
    }

    complete(isMediaRestored) {
        if (this._recoveryStarted) {
            if (!isMediaRestored) {
                this._putRemoteStreamsInPendingRecovery();
            } else {
                this._forEachRemoteStream((stream, id, tapId) => {
                    this.emit(Recovery.event.REMOTE_STREAM_RECOVERY_COMPLETED, stream, id, tapId);
                });
            }
            this._recoveryStarted = false;
            this.emit(Recovery.event.RECOVERY_COMPLETED);
        }
    }

    setSessionStatus(options, remoteStreams) {
        const affectedStreams = options.streams;
        if (!Array.isArray(affectedStreams)) {
            return;
        }

        const mediaState = options.media;
        //We don't want indicate recovery if media is working fine, so we ignore here signaling state
        const event = mediaState
            ? Recovery.event.REMOTE_STREAM_RECOVERY_COMPLETED
            : Recovery.event.REMOTE_STREAM_RECOVERY_STARTED;

        affectedStreams.forEach(tapId => {
            const stream = remoteStreams[tapId];
            if (stream) {
                this.emit(event, stream, stream.id, tapId);
                if (mediaState) {
                    delete this._streamsInRecovery[tapId];
                } else {
                    this._streamsInRecovery[tapId] = true;
                }
            }
        });
    }

    removeRemoteStream(tapId) {
        delete this._streamsInRecovery[tapId];
    }

    isStreamInRecovery(tapId) {
        return this._recoveryStarted || !!this._streamsInRecovery[tapId];
    }

    setMediaState(iceState, remoteStreams) {
        this._remoteStreams = remoteStreams;
        if (iceState === 'disconnected') {
            this._iceConnected = false;
            this._forEachRemoteStream((stream, id, tapId) => {
                this.emit(Recovery.event.REMOTE_STREAM_RECOVERY_STARTED, stream, id, tapId);
            });
        } else if (!this._iceConnected && (iceState === 'connected' || iceState === 'completed')) {
            this._backOffInterval = this._backOffStartInterval;
            this._iceConnected = true;
            this._forEachRemoteStream((stream, id, tapId) => {
                this.emit(Recovery.event.REMOTE_STREAM_RECOVERY_COMPLETED, stream, id, tapId);
            });
        } else if (iceState === 'failed') {
            this.emit(Recovery.event.MEDIA_DISCONNECTED);
        }
    }

    getPendingStream(tapId) {
        return this._pendingStreams[tapId];
    }

    recoverPendingStream(tapId) {
        delete this._pendingStreams[tapId];
    }

    _forEachRemoteStream(callback) {
        for (const tapId in this._remoteStreams) {
            if (this._remoteStreams.hasOwnProperty(tapId)) {
                const stream = this._remoteStreams[tapId];
                callback(stream, stream.id, tapId);
            }
        }
    }

    _putRemoteStreamsInPendingRecovery() {
        clearTimeout(this._streamRecoveryTimer);
        this._streamRecoveryTimer = setTimeout(
            this.removeAllPendingStreams.bind(this),
            this._streamRecoveryTimeout
        );

        //Move all remote streams to oldRemoteStreams
        //We will use this oldRemoteStreams to find out if some new stream is a replacement of old one
        for (const tapId in this._remoteStreams) {
            if (this._remoteStreams.hasOwnProperty(tapId)) {
                if (typeof this._pendingStreams[tapId] != 'undefined') {
                    this._logger.log('Removing potentially orphan stream');
                    const stream = this._pendingStreams[tapId];
                    const streamId = stream.id;
                    this._removeRemoteStream(stream, streamId, tapId);
                }
                this._pendingStreams[tapId] = this._remoteStreams[tapId];
            }
        }
        this.emit(Recovery.event.REMOTE_STREAMS_RECOVERY_PENDING, this._remoteStreams);
        this._remoteStreams = {};
    }

    removeAllPendingStreams() {
        this._logger.log('removing old streams which failed to recover');
        clearTimeout(this._streamRecoveryTimer);
        //clean all old streams on old peer connection(s)
        for (const tapId in this._pendingStreams) {
            if (this._pendingStreams.hasOwnProperty(tapId)) {
                const stream = this._pendingStreams[tapId];
                const streamId = stream.id;
                this._removeRemoteStream(stream, streamId, tapId);
            }
        }
        this._pendingStreams = {};
    }

    _removeRemoteStream(stream, streamId, tapId) {
        this.emit(Recovery.event.REMOTE_STREAM_REMOVED, stream, streamId, tapId);
    }
}

Recovery.event = {
    RECOVERY_STARTED: 'recovery_started',
    RECOVERY_COMPLETED: 'recovery_completed',
    REMOTE_STREAM_RECOVERY_STARTED: 'recovery_stream_started',
    REMOTE_STREAM_RECOVERY_COMPLETED: 'recovery_stream_completed',
    REMOTE_STREAM_REMOVED: 'remote_stream_removed',
    REMOTE_STREAMS_RECOVERY_PENDING: 'remote_streams_recovery_pending',
    MEDIA_DISCONNECTED: 'media_disconnected',
};

export default Recovery;
