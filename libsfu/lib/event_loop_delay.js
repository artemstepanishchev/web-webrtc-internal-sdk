const DEFAULT_INTERVAL = 200;
const DEFAULT_REPORT_INTERVAL = 10000;
const OVERFLOW_TIME_RESERVE = 5000;

class EventLoopDelay {
    constructor({ interval = DEFAULT_INTERVAL, reportInterval = DEFAULT_REPORT_INTERVAL }) {
        if (!(typeof interval === 'number' && interval >= 50)) {
            interval = DEFAULT_INTERVAL;
        }
        if (!(typeof reportInterval === 'number' && reportInterval >= interval)) {
            reportInterval = DEFAULT_REPORT_INTERVAL;
        }
        this._interval = interval;
        this._timer = undefined;
        this._maxValues = Math.floor(reportInterval + OVERFLOW_TIME_RESERVE / interval);
        this._values = new Array(this._maxValues);
    }
    _reset() {
        this._currentIndex = 0;
        this._count = 0;
    }
    _measure() {
        clearTimeout(this._timer);
        const interval = this._interval;
        const start = Date.now();
        this._timer = setTimeout(() => {
            const delay = Math.max(Date.now() - start - interval, 0);
            this._values[this._currentIndex] = delay;
            this._currentIndex = (this._currentIndex + 1) % this._maxValues;
            if (this._count < this._maxValues) {
                this._count++;
            }
            this._measure();
        }, interval);
    }
    getResults() {
        let avg,
            max = 0;

        if (this._count > 0) {
            let sum = 0;
            for (let i = 0; i < this._count; i++) {
                const delay = this._values[i];
                sum += delay;
                max = Math.max(max, delay);
            }
            avg = sum / this._count;
            avg = Math.floor(avg * 100) / 100;
        } else {
            avg = 0;
        }

        this._reset();

        return {
            max,
            avg,
        };
    }
    start() {
        this._reset();
        this._measure();
    }
    stop() {
        clearTimeout(this._timer);
    }
}

export default EventLoopDelay;
