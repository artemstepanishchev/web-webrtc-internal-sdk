import EventEmitter from './events.js';
import PeersController from './peers_controller.js';
import ClientCapabilities from './client_capabilities.js';
import AudioAdaptation from './audio_adaptation.js';
import LogWrapper from './log_wrapper.js';
import { createSequence } from './utils/misc.js';
import LocalStreamTypes from './utils/localstream_types.js';

const seq = createSequence();
const bitrateMapper = simulcastFormat => ({
    pixels: simulcastFormat.width * simulcastFormat.height,
    bitrate: simulcastFormat.target,
});

const SHARING_ADAPT_OPTIONS = {
    bitrateConstraints: [
        [250, 128],
        [500, 80],
        [800, 64],
        [+Infinity, 32],
    ],
    minBitrate: 24,
};

class LocalStream extends EventEmitter {
    constructor({
        id,
        msid,
        stream,
        useSimulcast,
        allowAudioRed,
        type,
        peerController,
        codec,
        codecForP2P,
        logger,
        maxP2PConnections,
        consumers,
    }) {
        super();
        this._id = id;
        this._msid = msid;
        this._seq = seq();
        this._stream = stream;
        this._loss = 0;
        this._smoothedLoss = 0;
        this._smoothedRtt = 0;
        this._useSimulcast = useSimulcast;
        this._allowAudioRed = allowAudioRed;
        this._codec = codec;
        this._codecForP2P = codecForP2P;
        this._type = type;
        const isScreensharing = this._type === LocalStream.type.VIDEO_SCREENSHARING;
        this._useDedicatedPC = isScreensharing;
        this._targetDeliveryMode = LocalStream.mode.SFU;
        this._mode = LocalStream.mode.NONE;
        this._logger = logger;
        this._logger = new LogWrapper(
            logger,
            () =>
                `ls:${this._seq}-${
                    this._peerController ? this._peerController.getSessionId() : '<no crl>'
                }`
        );
        this._maxP2PConnections = maxP2PConnections || 0;
        this._destroyed = false;
        this._p2pConnectionsBanTimer = undefined;
        this._p2pConnectionsEnabled = true;
        this._p2pConnectionsByRemoteParty = {};
        this._desiredQuality = -1;
        this._lastTimeDesiredQualityIncreased = Date.now();
        this._minVideoBitrate = 150 * 1000;
        this._startVideoBitrate = 800 * 1000; // goog-start-bitrate
        this._lastBitrateOnQualityIncrease = this._startVideoBitrate;
        this._width = undefined;
        this._height = undefined;
        this._getDimensionsPromise = undefined;
        this._nqiDetails = {};

        this._simulcastFormats = [
            { width: 1920, height: 1080, layers: 3, max: 5000, target: 4000, min: 800 }, // 0
            { width: 1280, height: 720, layers: 3, max: 2500, target: 2500, min: 600 }, // 1
            { width: 960, height: 540, layers: 3, max: 900, target: 900, min: 450 }, // 2
            { width: 640, height: 360, layers: 2, max: 700, target: 500, min: 150 }, // 3
            { width: 480, height: 270, layers: 2, max: 450, target: 350, min: 150 }, // 4
            { width: 320, height: 180, layers: 1, max: 200, target: 150, min: 30 }, // 5
        ];

        this._maxTargetBitrate = 2000; //TODO take it from b=AS:2000 from remote SDP

        this._maxDefaultVideoBitrate = [
            bitrateMapper(this._simulcastFormats[5]),
            bitrateMapper(this._simulcastFormats[3]),
            bitrateMapper(this._simulcastFormats[2]),
            { bitrate: this._maxTargetBitrate },
        ];

        this.setStream(this._stream);
        this.setConsumers(consumers || []);
        this.setPeerController(peerController);

        if (ClientCapabilities.hasSenderParameters) {
            // audio bitrate adaptation
            const audioAdaptOptions = isScreensharing ? SHARING_ADAPT_OPTIONS : {};
            this._audioAdaptation = new AudioAdaptation(logger, audioAdaptOptions);
            this._audioAdaptationInterval = 1000;
            this._audioAdaptationTimer = setTimeout(
                () => this._adaptAudio(),
                this._audioAdaptationInterval
            );
        }
    }
    setId(newId) {
        //Update all existing p2p connections with old ID to newId
        if (this._isPeerControllerReady()) {
            const p2pPCs = this._peerController.getAllP2PPCsForStream(this._id);
            p2pPCs.forEach(pc => pc.setStreamTapId(newId));
        }
        this._id = newId;
    }
    setUseSimulcast(newUseSimulcast) {
        this._useSimulcast = newUseSimulcast;
    }
    setMaxP2PConnections(newMaxP2PConnections) {
        if (newMaxP2PConnections !== this._maxP2PConnections) {
            this._maxP2PConnections = newMaxP2PConnections;
            if (this._isPeerControllerReady()) {
                this._calculateRouting();
            }
        }
    }
    _isPeerControllerReady() {
        return this._peerController && !this._peerController.isClosed();
    }
    destroy() {
        if (!this._destroyed) {
            this._destroyed = true;
            clearTimeout(this._audioAdaptationTimer);
            if (!this._isPeerControllerReady()) {
                return;
            }
            if (this._useDedicatedPC) {
                const pcs = this._peerController.getPCSByLocalStream(this._stream);
                if (pcs.length > 0) {
                    pcs.forEach(pc => pc.removeAllLocalStreams());
                }
            } else {
                const mainPC = this._peerController.getMainPC();
                if (typeof this._mainCloseHandler === 'function') {
                    mainPC.removeListener(PeersController.event.CLOSED, this._mainCloseHandler);
                    delete this._mainCloseHandler;
                }
                if (typeof this._localStreamAddedToMainHandler === 'function') {
                    mainPC.removeListener(
                        PeersController.event.LOCAL_STREAM_ADDED,
                        this._localStreamAddedToMainHandler
                    );
                    delete this._localStreamAddedToMainHandler;
                }
                if (mainPC.hasLocalStream(this._stream)) {
                    mainPC.removeLocalStream(this._stream);
                }
            }
            if (typeof this._newP2PHandler === 'function') {
                this._peerController.removeListener(
                    PeersController.event.NEW_P2P_CONNECTION,
                    this._newP2PHandler
                );
                delete this._newP2PHandler;
            }
            if (this._maxP2PConnections > 0) {
                const pcs = this._peerController.getAllP2PPCsForStream(this._id);
                pcs.forEach(pc => {
                    pc.removeAllLocalStreams();
                    pc.close();
                });
            }
        }
    }
    getPeerConnections() {
        if (this._isPeerControllerReady()) {
            return this._peerController.getPCSByLocalStream(this._stream);
        } else {
            return [];
        }
    }
    getType() {
        return this._type;
    }
    getMSID() {
        return this._msid;
    }
    getStream() {
        return this._stream;
    }
    setStream(newStream) {
        this._width = undefined;
        this._height = undefined;
        if (newStream.getVideoTracks().length > 0) {
            this._getDimensionsPromise = this._getStreamSize(newStream).then(dimensions => {
                this._width = dimensions.width;
                this._height = dimensions.height;
                return dimensions;
            });
        } else {
            this._getDimensionsPromise = Promise.resolve();
        }

        if (this._isPeerControllerReady()) {
            const pcs = this._peerController.getPCSByLocalStream(this._stream);
            pcs.forEach(pc => {
                pc.removeLocalStream(this._stream);
                if (pc.isP2P()) {
                    pc.addLocalStream({
                        stream: newStream,
                        useSimulcast: false,
                        codec: this._codecForP2P,
                        streamTapId: this._id,
                        width: this._width,
                        height: this._height,
                        getDimensionsPromise: this._getDimensionsPromise,
                    });
                } else {
                    pc.addLocalStream({
                        stream: newStream,
                        useSimulcast: this._useSimulcast,
                        codec: this._codec,
                        streamTapId: this._id,
                        width: this._width,
                        height: this._height,
                        getDimensionsPromise: this._getDimensionsPromise,
                    });
                }
            });
        }
        this._stream = newStream;
        this._msid = newStream.id;
        this._calculateRouting();
    }
    getId() {
        return this._id;
    }
    setPeerController(newPeerController) {
        if (this._peerController === newPeerController) {
            return;
        }
        this._peerController = newPeerController;
        if (this._mode === LocalStream.mode.SFU) {
            this._mode = LocalStream.mode.NONE;
        }
        if (this._isPeerControllerReady()) {
            if (!this._useDedicatedPC) {
                const mainPC = this._peerController.getMainPC();
                this._localStreamAddedToMainHandler = (stream, streamId) => {
                    if (this._mode === LocalStream.mode.SFU && streamId === this._msid) {
                        this._closeAllP2PPCs();
                    }
                };
                this._mainCloseHandler = () => {
                    mainPC.removeListener(
                        PeersController.event.LOCAL_STREAM_ADDED,
                        this._localStreamAddedToMainHandler
                    );
                    delete this._mainCloseHandler;
                };
                mainPC.on(
                    PeersController.event.LOCAL_STREAM_ADDED,
                    this._localStreamAddedToMainHandler
                );
                mainPC.once(PeersController.event.CLOSED, this._mainCloseHandler);
                mainPC.once(PeersController.event.CONNECTED, () => this._calculateRouting());
            }
            this._newP2PHandler = pc => {
                if (this._destroyed) {
                    return;
                }
                if (this._maxP2PConnections > 0) {
                    if (!this._peerController.isConsumerBlacklisted(pc.getRemoteParty())) {
                        if (
                            !pc.isMaster() &&
                            pc.getRole() === PeersController.role.MAIN &&
                            !pc.getStreamTapId()
                        ) {
                            pc.setStreamTapId(this._id);

                            this._maybeSwitchMedia();
                        }

                        const iceState = PeersController.iceState;

                        pc.on(PeersController.event.ICE_STATE_CHANGED, newState => {
                            if ([iceState.FAILED, iceState.DISCONNECTED].indexOf(newState) > -1) {
                                pc.close();
                            } else {
                                this._maybeSwitchMedia();
                            }
                        });
                        pc.on(PeersController.event.CLOSED, () => {
                            const remoteParty = pc.getRemoteParty();
                            if (this._p2pConnectionsByRemoteParty[remoteParty] === pc) {
                                delete this._p2pConnectionsByRemoteParty[remoteParty];
                            }
                            //We should ban P2P connection for a while
                            //In order to prevent blinking and too many requests
                            //in case of recovery/dc switchower/unstable network
                            this._p2pConnectionsEnabled = false;
                            this._calculateRouting();
                            clearTimeout(this._p2pConnectionsBanTimer);
                            this._p2pConnectionsBanTimer = setTimeout(() => {
                                this._p2pConnectionsEnabled = true;
                                this._calculateRouting();
                            }, Math.floor(Math.random() * 4000) + 1000); //1s - 5s
                        });

                        pc.on(PeersController.event.LOCAL_STREAM_ADDED, (stream, streamId) => {
                            if (this._mode === LocalStream.mode.P2P && streamId === this._msid) {
                                const pcs = this._peerController.getAllP2PPCsForStream(this._id);
                                const isStreamAddedToAllPC = !pcs.find(pc =>
                                    pc.isLocalStreamAdded(this._id)
                                );
                                const mainPC = this._peerController.getMainPC();
                                if (isStreamAddedToAllPC && mainPC.hasLocalStream(stream)) {
                                    mainPC.removeLocalStream(this._stream);
                                    this.emit(LocalStream.event.P2P_RENEGOTIATION_REQUIRED);
                                }
                            }
                        });
                        setTimeout(() => {
                            if ([iceState.NEW, iceState.NONE].indexOf(pc.getICEstate()) > -1) {
                                pc.close();
                            }
                        }, 5000);

                        const remoteParty = pc.getRemoteParty();
                        if (this._p2pConnectionsByRemoteParty[remoteParty]) {
                            //It means remote party closed p2p connection and created another one
                            //but we still didn't detected that
                            //So we should close old p2p connection and start using this new one
                            //This of course will limit 1 p2p connection per 2 parties (max 1 stream by p2p between 2 parties)
                            const existingPC = this._p2pConnectionsByRemoteParty[remoteParty];
                            this._logger.log(
                                `We already have active p2p connection with remoteParty=${remoteParty} localSDPSessionId=${pc.getLocalSDPSessionId()}.`
                            );
                            existingPC.close();
                        }
                        this._p2pConnectionsByRemoteParty[pc.getRemoteParty()] = pc;
                    } else {
                        this._logger.logError('Consumer is blacklisted so we close pc.');
                        pc.close();
                    }
                }
            };

            this._peerController.on(PeersController.event.NEW_P2P_CONNECTION, this._newP2PHandler);
            this._calculateRouting();

            //We can already have remote p2p connection, so we need re-check existing
            this._peerController.getAllP2PPCs().forEach(pc => this._newP2PHandler(pc));
        }
    }
    hasVideo() {
        return this._stream.getVideoTracks().length > 0;
    }
    hasAudio() {
        return this._stream.getAudioTracks().length > 0;
    }
    getMode() {
        return this._mode;
    }
    _setTargetDeliveryMode(newMode) {
        this._logger.log(`Target delivery mode is ${newMode}`);
        this._targetDeliveryMode = newMode;
    }
    setConsumers(consumers) {
        if (this._destroyed) {
            return;
        }
        if (JSON.stringify(consumers) === JSON.stringify(this._consumers)) {
            return;
        }
        this._consumers = consumers;
        if (!this._isPeerControllerReady()) {
            return;
        }
        this._logger.log(`New consumers: ${JSON.stringify(consumers)}`);
        const pcs = this._peerController.getAllP2PPCsForStream(this._id);
        pcs.forEach(pc => {
            const remoteParty = pc.getRemoteParty();
            const consumer = this._consumers.find(consumer => consumer.getId() === remoteParty);
            if (!consumer) {
                this._logger.log(`P2P connection removed since consumer ${remoteParty} left`);
                pc.clearLock();
                pc.close();
            }
        });

        this._calculateRouting();
    }
    setDesiredQuality({ desiredQuality = this._desiredQuality, force = false }) {
        if ((this._desiredQuality !== desiredQuality || force) && desiredQuality >= 0) {
            const isQualityIncreased = desiredQuality > this._desiredQuality;
            if (isQualityIncreased) {
                this._lastTimeDesiredQualityIncreased = Date.now();
            }
            this._desiredQuality = desiredQuality;
            this._logger.log(`stream tapId=${this._id}: *desired_quality=${desiredQuality}`);
            const mainPC = this._peerController.getMainPC();
            if (!mainPC) {
                return;
            }
            if (!this._stream) {
                return;
            }
            const videoTrack = this._stream.getVideoTracks()[0];
            if (!videoTrack) {
                return;
            }
            let pc;
            let isScreenSharing = false;
            if (mainPC.hasLocalStream(this._stream)) {
                pc = mainPC;
            } else {
                // additional stream, find its MediaPeerConnection
                const pcs = this._peerController.getPCSByLocalStream(this._stream);
                if (pcs) {
                    pc = pcs[0];
                    if (this._type === LocalStream.type.VIDEO_SCREENSHARING) {
                        isScreenSharing = true;
                    }
                }
            }
            if (pc) {
                if (isQualityIncreased) {
                    const videoStat = pc.getStatForLocalStream(this._msid).video;
                    this._lastBitrateOnQualityIncrease =
                        typeof videoStat.speed === 'number' && videoStat.speed >= 0
                            ? videoStat.speed * 1024 * 8
                            : this._startVideoBitrate;
                }
                pc.setSenderQuality(videoTrack, this._desiredQuality, isScreenSharing, this._codec);
            }
        }
    }
    getSendQuality() {
        //Get pc which is currently used to send the stream
        let pcs = this.getPeerConnections();
        if (this._mode === LocalStream.mode.P2P) {
            pcs = pcs.filter(pc => pc.isP2P());
        } else {
            pcs = pcs.filter(pc => !pc.isP2P());
        }
        const pc = pcs[0];

        if (!pc) {
            //Don't have connection? => quality=0
            return 0;
        }

        const isVideoQualityBasedCalculation =
            this._isVideoActive() && this._type === LocalStream.type.VIDEO_MAIN;

        const isPacketLossBasedCalculation = !isVideoQualityBasedCalculation;
        let quality = 100;

        if (isVideoQualityBasedCalculation) {
            const idealBitrate = this._getIdealVideoBitrate(pc);
            const videoStat = pc.getStatForLocalStream(this._msid).video;
            if (videoStat.hasOwnProperty('netQuality') && videoStat.netQuality !== null) {
                this._nqiDetails = videoStat.nqiDetails;
                return videoStat.netQuality;
            }
            this._nqiDetails = {};
            let actualBitrate;
            if (typeof videoStat.speed === 'number') {
                actualBitrate = videoStat.speed * 1024 * 8;
                if (actualBitrate < 0) {
                    actualBitrate = idealBitrate;
                }
            } else {
                actualBitrate = idealBitrate;
            }
            let packetLossFraction;
            if (typeof videoStat.packetsLostFraction === 'number') {
                packetLossFraction = videoStat.packetsLostFraction;
            } else {
                packetLossFraction = 0;
            }
            quality = (100 * actualBitrate) / idealBitrate;
            if (packetLossFraction >= 0.1) {
                quality = Math.min(quality, 30);
            }
        } else if (isPacketLossBasedCalculation) {
            const stat = pc.getStatForLocalStream(this._msid);

            let mediaStat;
            if (this._type === LocalStream.type.VIDEO_SCREENSHARING) {
                //In case of screen sharing we check video packet loss
                mediaStat = stat.video;
            } else {
                //In case of web cam stream we check audio packet loss
                if (this.hasAudio()) {
                    mediaStat = stat.audio;
                } else if (this.hasVideo()) {
                    mediaStat = stat.video;
                } else {
                    mediaStat = undefined;
                }
            }
            if (
                mediaStat &&
                mediaStat.hasOwnProperty('netQuality') &&
                mediaStat.netQuality !== null
            ) {
                this._nqiDetails = mediaStat.nqiDetails;
                return mediaStat.netQuality;
            }
            this._nqiDetails = {};
            let packetLossFraction;
            if (
                typeof mediaStat === 'object' &&
                typeof mediaStat.packetsLostFraction === 'number'
            ) {
                packetLossFraction = mediaStat.packetsLostFraction;
            } else {
                packetLossFraction = 0;
            }
            if (packetLossFraction <= 0.02) {
                quality = 100;
            } else if (packetLossFraction <= 0.04) {
                quality = 70;
            } else if (packetLossFraction <= 0.06) {
                quality = 50;
            } else if (packetLossFraction <= 0.08) {
                quality = 30;
            } else if (packetLossFraction <= 0.12) {
                quality = 10;
            } else {
                quality = 0;
            }
        }

        return Math.min(Math.max(quality, 0), 100);
    }
    getNQIDetails() {
        return Object.assign({ msid: this._msid, id: this._id }, this._nqiDetails);
    }
    _getIdealVideoBitrate(pc) {
        let idealBitrate = 0;
        if (this._desiredQuality === 0) {
            return 0; //It should not happen, because in this case we will not use getIdealVideoBitrate
            //But just to be consistent
        }
        const videoTrack = this._stream.getVideoTracks()[0];

        const maxBitrate = pc.getMaxBitrateByLocalVideoTrack(videoTrack);
        if (typeof maxBitrate === 'number') {
            //Try to first take actual bitrate settings from peer connection
            idealBitrate = maxBitrate;
        } else {
            //Otherwise use default values taken from Chrome codebase
            if (this._useSimulcast) {
                let targetHeight = this._height;
                for (let i = 3; i > this._desiredQuality; i--) {
                    targetHeight = targetHeight / 2;
                }
                const simulcastFormatIndex = this._simulcastFormats.findIndex(
                    format => format.height <= targetHeight
                );
                if (simulcastFormatIndex) {
                    for (let i = simulcastFormatIndex; i < this._simulcastFormats.length; i += 2) {
                        const simulcastFormat = this._simulcastFormats[i] || { target: 0 };
                        idealBitrate += simulcastFormat.target * 1000;
                    }
                }
            } else {
                const pixels = this._width * this._height;
                for (let i = 0; i < this._maxDefaultVideoBitrate.length; i++) {
                    const bitrateSettings = this._maxDefaultVideoBitrate[i];
                    if (!bitrateSettings.pixels || pixels <= bitrateSettings.pixels) {
                        idealBitrate = bitrateSettings.bitrate * 1000;
                        break;
                    }
                }
            }
            idealBitrate = Math.min(idealBitrate, this._maxTargetBitrate * 1000);
        }
        const timeSinceDesiredQualityIncreased = Date.now() - this._lastTimeDesiredQualityIncreased;
        if (timeSinceDesiredQualityIncreased < 60000) {
            idealBitrate = Math.min(
                idealBitrate,
                Math.max(this._lastBitrateOnQualityIncrease, this._minVideoBitrate) *
                    Math.pow(1.08, timeSinceDesiredQualityIncreased / 1000)
            );
        }
        return idealBitrate;
    }
    _isVideoActive() {
        if (
            !this._stream ||
            this._desiredQuality < 1 ||
            (typeof this._width === 'undefined' && typeof this._height === 'undefined')
        ) {
            return false;
        }
        const videoTracks = this._stream.getVideoTracks();
        if (videoTracks.length === 0) {
            return false;
        }
        const videoTrack = videoTracks[0];

        return !(
            videoTrack.enabled === false ||
            videoTrack.muted === true ||
            videoTrack.readyState === 'ended'
        );
    }
    _getStreamSize(stream) {
        const timeout = 3000;
        const defaultWidth = 1280;
        const defaultHeight = 720;
        return new Promise(resolve => {
            const self = this;
            const v = document.createElement('video');
            const getDimensionTimer = setTimeout(getSize, timeout);
            v.addEventListener('loadedmetadata', getSize);
            v.muted = true;
            v.srcObject = stream;

            function getSize() {
                if (!v.videoWidth) {
                    self._logger.logWarn(
                        `Failed to detect resolution of the stream during ${timeout}ms. Will use default: ${defaultWidth}x${defaultHeight}`
                    );
                }
                clearTimeout(getDimensionTimer);
                v.removeEventListener('loadedmetadata', getSize);
                v.remove();
                const dimensions = {
                    width: v.videoWidth || defaultWidth,
                    height: v.videoHeight || defaultHeight,
                };
                v.srcObject = null;
                resolve(dimensions);
            }
        });
    }
    _calculateRouting() {
        if (this._destroyed || !this._isPeerControllerReady()) {
            return;
        }
        this._logger.log(`Calculating routing for ${this._id}...`);
        let videoEncodings = 0;
        let audioEncodings = 0;
        let videoToSFU = false;
        let audioToSFU = false;
        const hasVideo = this.hasVideo();
        const hasAudio = this.hasAudio();

        let targetDeliveryMode;
        const mainPC = this._peerController.getMainPC();
        if (this._maxP2PConnections > 0 && mainPC && mainPC.isConnected()) {
            const MAX_VIDEO_ENCODINGS = this._maxP2PConnections;
            const MAX_AUDIO_ENCODINGS = this._maxP2PConnections;

            for (let i = 0; i < this._consumers.length; i++) {
                const consumer = this._consumers[i];
                if (hasVideo && consumer.consumeVideo()) {
                    if (consumer.canP2P()) {
                        videoEncodings++;
                        if (videoEncodings > MAX_VIDEO_ENCODINGS) {
                            break;
                        }
                    } else {
                        videoToSFU = true;
                    }
                }
                if (hasAudio && consumer.consumeAudio()) {
                    if (consumer.canP2P()) {
                        audioEncodings++;
                        if (audioEncodings > MAX_AUDIO_ENCODINGS) {
                            break;
                        }
                    } else {
                        audioToSFU = true;
                    }
                }
            }
            audioEncodings += audioToSFU ? 1 : 0;
            videoEncodings += videoToSFU ? 1 : 0;
            targetDeliveryMode =
                this._consumers.length > 0 &&
                videoEncodings <= MAX_VIDEO_ENCODINGS &&
                audioEncodings <= MAX_AUDIO_ENCODINGS
                    ? LocalStream.mode.P2P
                    : LocalStream.mode.SFU;
        } else {
            targetDeliveryMode = LocalStream.mode.SFU;
        }
        const prevTargetMode = this._targetDeliveryMode;
        this._setTargetDeliveryMode(targetDeliveryMode);

        if (targetDeliveryMode === LocalStream.mode.P2P) {
            if (this._p2pConnectionsEnabled) {
                this._consumers.forEach(consumer => {
                    const role =
                        this._type === LocalStream.type.VIDEO_MAIN
                            ? PeersController.role.MAIN
                            : PeersController.role.ADDITIONAL;
                    if (
                        consumer.canP2P() &&
                        !this._peerController.isConsumerBlacklisted(consumer.getId())
                    ) {
                        this._ensureP2PConnection(consumer, role);
                    }
                });
            }
        } else {
            if (prevTargetMode === LocalStream.mode.P2P && this._mode === LocalStream.mode.SFU) {
                //While we tried to switch to P2P we failed
                //and now we don't even want to switch to P2P
                //So, we need remove all p2p connections
                this._closeAllP2PPCs();
            }
        }

        this._maybeSwitchMedia();
    }
    _maybeSwitchMedia() {
        if (this._destroyed) {
            return;
        }
        if (!this._isPeerControllerReady()) {
            return;
        }
        this._logger.log(
            `Maybe switch media: streamTapId=${this._id}, targetDeliveryMode=${this._targetDeliveryMode}, mode=${this._mode}.`
        );
        if (this._targetDeliveryMode === LocalStream.mode.P2P) {
            const pcs = this._peerController.getAllP2PPCsForStream(this._id);
            let connectedConsumers = 0;
            let totalConsumers = 0;
            this._consumers.forEach(consumer => {
                if (!consumer.canP2P()) {
                    return;
                }
                totalConsumers++;
                const consumerId = consumer.getId();

                const pc = pcs.find(pc => pc.getRemoteParty() === consumerId && pc.isConnected());
                if (pc) {
                    connectedConsumers++;
                }
            });
            this._logger.log(
                `Total consumers: ${totalConsumers}, connected: ${connectedConsumers}`
            );

            if (totalConsumers > 0) {
                const isAllPCConnected = connectedConsumers === totalConsumers;
                if (isAllPCConnected) {
                    this._switchToP2P();
                } else {
                    this._switchToSFU();
                }
            } else {
                this._switchToSFU();
            }
        } else {
            this._switchToSFU();
        }
    }
    _switchToP2P() {
        if (this._mode === LocalStream.mode.P2P) {
            return;
        }
        this._mode = LocalStream.mode.P2P;
        this._logger.logWarn('SWITCH TO P2P');
        const pcs = this._peerController.getAllP2PPCsForStream(this._id);
        pcs.forEach(pc => {
            const remoteParty = pc.getRemoteParty();
            const consumer = this._consumers.find(consumer => consumer.getId() === remoteParty);
            if (consumer) {
                pc.removeAllLocalStreams();
                if (consumer.consumeAudio()) {
                    pc.addLocalStream({
                        stream: this._stream,
                        useSimulcast: false,
                        codec: this._codecForP2P,
                        streamTapId: this._id,
                        width: this._width,
                        height: this._height,
                        getDimensionsPromise: this._getDimensionsPromise,
                    });
                }
            } else {
                this._logger.logError(`Consumer not found: ${remoteParty}. Closing pc.`);
                pc.close();
            }
        });
        this.emit(LocalStream.event.P2P_RENEGOTIATION_REQUIRED);
        this.emit(LocalStream.event.MODE_CHANGED, this._mode);
    }
    _switchToSFU() {
        if (this._mode === LocalStream.mode.SFU) {
            return;
        }
        this._logger.logWarn('SWITCH TO SFU');
        this._mode = LocalStream.mode.SFU;
        let streamWasAddedToDifferentPC = false;
        if (this._useDedicatedPC) {
            const pcs = this._peerController.getPCSByLocalStream(this._stream);
            if (pcs.length < 1) {
                this._peerController.addLocalStream({
                    stream: this._stream,
                    useSimulcast: this._useSimulcast,
                    dedicatedConnection: this._useDedicatedPC,
                    codec: this._codec,
                    streamTapId: this._id,
                    width: this._width,
                    height: this._height,
                    getDimensionsPromise: this._getDimensionsPromise,
                });
                streamWasAddedToDifferentPC = true;
            }
        } else {
            const mainPC = this._peerController.getMainPC();
            if (mainPC.hasLocalStream(this._stream)) {
                this._closeAllP2PPCs();
            } else {
                mainPC.addLocalStream({
                    stream: this._stream,
                    useSimulcast: this._useSimulcast,
                    codec: this._codec,
                    streamTapId: this._id,
                    width: this._width,
                    height: this._height,
                    getDimensionsPromise: this._getDimensionsPromise,
                });
                streamWasAddedToDifferentPC = true;
            }
        }
        if (streamWasAddedToDifferentPC) {
            this.setDesiredQuality({ force: true });
            this.emit(LocalStream.event.P2P_RENEGOTIATION_REQUIRED);
        }
        this.emit(LocalStream.event.MODE_CHANGED, this._mode);
    }
    _closeAllP2PPCs() {
        const pcs = this._peerController.getAllP2PPCsForStream(this._id);
        pcs.forEach(pc => {
            pc.removeAllLocalStreams();
            pc.close();
        });
    }
    _ensureP2PConnection(consumer, role) {
        if (this._destroyed) {
            return;
        }
        if (!this._isPeerControllerReady()) {
            return;
        }
        const remoteParty = consumer.getId();
        const isMaster = this._peerController.getSessionId() < remoteParty;

        if (isMaster) {
            const pc = this._peerController.getP2PPC(role, remoteParty, this._id);
            if (!pc && !this._peerController.isClosed()) {
                this._peerController.createP2PPC(role, remoteParty, this._id);

                this.emit(LocalStream.event.P2P_RENEGOTIATION_REQUIRED);
            }
        }
    }
    // adapt audio bitrate to network - loss, rtt
    _adaptAudio() {
        const pcs = this.getPeerConnections();
        const pc = pcs[0];
        const track = this._stream.getAudioTracks()[0];
        if (pc && track && track.enabled) {
            const success = this._retrieveStatsForPC(pc);
            // Skip audio adaptation for PC in case when Audio RED is enabled - RCV-38759.
            if (success && !pc.isAudioRedEnabled()) {
                // Adapt audio if need.
                this._audioAdaptation.adapt(
                    pc.getPeerConnection(),
                    track,
                    this._loss,
                    this._smoothedLoss,
                    this._smoothedRtt
                );
            }
        }
        clearTimeout(this._audioAdaptationTimer);
        this._audioAdaptationTimer = setTimeout(
            () => this._adaptAudio(),
            this._audioAdaptationInterval
        );
    }

    _retrieveStatsForPC(pc) {
        const stat = pc.getStatForLocalStream(this._msid).audio;
        if (stat) {
            this._loss = (Number(stat.packetsLostFraction) || 0) * 100; // measure loss in percents 0..100
            const rtt = Number(stat.rtt) || 0;
            // Smooth stats.
            this._smoothedLoss = (this._smoothedLoss * 7 + this._loss) / 8;
            this._smoothedRtt = this._smoothedRtt === 0 ? rtt : (this._smoothedRtt * 7 + rtt) / 8;

            return true;
        }
        return false;
    }
}

LocalStream.mode = {
    NONE: 'none',
    P2P: 'p2p',
    SFU: 'sfu',
};

LocalStream.type = LocalStreamTypes;

LocalStream.event = {
    P2P_RENEGOTIATION_REQUIRED: 'p2p_renegotiation_required',
    MODE_CHANGED: 'mode_changed',
};

LocalStream.id = {
    PLACEHOLDER: '<placeholder>',
};

export default LocalStream;
