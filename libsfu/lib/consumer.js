export default class Consumer {
    constructor({ canP2P, consumeVideo, consumeAudio, id }) {
        this._canP2P = canP2P;
        this._consumeVideo = consumeVideo;
        this._consumeAudio = consumeAudio;
        this._id = id;
    }
    getId() {
        return this._id;
    }
    canP2P() {
        return this._canP2P;
    }
    consumeVideo() {
        return this._consumeVideo;
    }
    consumeAudio() {
        return this._consumeAudio;
    }
}
