import LogWrapper from './log_wrapper.js';
import { getSenderByTrack, getBitrateBySender, setSenderBitrate } from './utils/encodings.js';
import { fitToRange } from './utils/misc.js';

// RTT defines max bitrate: 0..250: 64; 250-800: 40; 800+: 28
const DEFAULT_BITRATE_CONSTRAINTS = [
    [250, 32],
    [800, 24],
    [+Infinity, 24],
];

const MIN_BITRATE = 16;

// Class is intended to adapt audio track to current network conditions
// - Using existing audio stats to understand network conditions
// - Regulate audio bitrate
//
// Algorithm description:
// - Smooth packet loss and RTT (moving average in 8 sec window)
// - Calculate target bitrate, based on loss, rtt
// - If target bitrate != current, try to set new bitrate
// - To avoid oscillation:
//   - For each switch DOWN, algorithm increases bad karma
//   - Before switching UP, applies ban (timeout), which is calculated from karma
//
// - It is assumed, that algorithm is called every second, all timings are measured in these units (this._counter)
//
// Bitrate calculation:
// - minBitrate = 16
// - maxBitrate depends on RTT band: 0..250: 64  250...800: 40  800+: 28
// - bitrate is calculated in minBitrate..maxBitrate range depending on loss linearly
// - loss effective range 2%..10%
// - set encoding.maxBitrate
//
// Notes:
// - maxBitrate defines maximum possible. We use OPUS with variable bitrate, so actual bitrate varies a lot.
//
class AudioAdaptation {
    constructor(logger, options) {
        this._id = '';
        this._logger = new LogWrapper(logger, () => 'aa:' + this._id);
        if (!options) {
            options = {};
        }
        this._counter = 0;
        this._lastSwitch = 0;
        this._banCounter = 0;
        this._lastDirection = 0;
        this._karma = 0;
        this._banLimit = options.banLimit || 2 * 60; // 2 minutes by default
        this._bitrateConstraints = options.bitrateConstraints ?? DEFAULT_BITRATE_CONSTRAINTS;
        this._minBitrate = options.minBitrate ?? MIN_BITRATE;
    }

    adapt(pc, track, loss, smoothedLoss, smoothedRtt) {
        if (!track || !track.enabled) {
            return;
        }
        const trackId = track.id;
        if (trackId) {
            this._id = trackId.substr(0, 8);
        }

        // check parameters
        const sender = getSenderByTrack(pc, track);
        if (!sender) {
            this._logger.logWarn(`Unable to find sender for the track id=${track?.id}`);
            return;
        }

        const oldBitrate = getBitrateBySender(sender);
        if (oldBitrate === undefined) return;

        // adaptation algorithm
        let newBitrate = this._calcBitrate(smoothedLoss, smoothedRtt);

        this._logger.logDebug(
            `loss=${Math.round(loss)} ${Math.round(smoothedLoss)} rtt=${Math.round(
                smoothedRtt
            )} bitrate=${oldBitrate} karma=${this._karma} ban=${this._banCounter}`
        );
        // dont switch faster than once in 4 seconds
        if (newBitrate === oldBitrate || this._counter - this._lastSwitch <= 3) {
            this._counter++;
            return;
        }

        let canSwitch = true;
        let direction = 0;
        if (newBitrate < oldBitrate) {
            direction = -1;
            if (this._lastDirection >= 0) {
                this._karma++;
            }
            this._banCounter = Math.min(this._karma * this._karma * 16, this._banLimit);
            this._logger.logDebug(
                `direction=${direction} karma=${this._karma} set ban=${this._banCounter}`
            );
        } else {
            direction = 1;
            // increase bw by small steps to avoid congestion
            newBitrate = Math.min(oldBitrate + 4 * 1000, newBitrate);
            if (this._banCounter > 0) {
                this._banCounter--;
            }
            canSwitch = this._banCounter === 0;
        }

        if (canSwitch) {
            setSenderBitrate(sender, newBitrate);
            this._lastDirection = direction;
            this._lastSwitch = this._counter;
            this._logger.log(`SET audio bitrate: ${newBitrate}`);
        }
        this._counter++;
    }

    _calcBitrate(alossAvg, artt) {
        // TODO: get maxAverageBitrate from remote SDP
        // Note: all bitrates inside the function are in Kbps
        alossAvg = Number(alossAvg) || 0;
        artt = Number(artt) || 0;
        const rtt = fitToRange(0, 800, Math.floor(artt));
        const [, maxBitrate] = this._bitrateConstraints.find(([maxRtt]) => rtt < maxRtt) || [];
        // normalize loss from 2-10% to to 0..8%
        const loss = fitToRange(0, 8, Math.round(alossAvg) - 2);
        // Assuming: normalized loss 0..8 returns linear bitrate in selected rtt band
        // coef = 1.0 - loss/8;
        // bitrate = min + (max-min)*coef
        const coeff = Math.max(1.0 - loss / 8, 0);
        const bitrate = Math.floor(this._minBitrate + (maxBitrate - this._minBitrate) * coeff);

        return bitrate * 1000;
    }
}

export default AudioAdaptation;
