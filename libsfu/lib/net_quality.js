import ProtobufMessages from '../protobuf/messages.js';
import AtomicUpdate from './atomic_update.js';
import EventEmitter from './events.js';
import assert from './assert.js';
import Channel from './channel.js';
import LocalStream from './local_stream.js';

const { NQIMessage } = ProtobufMessages.nqi;

// [ 0%,   1%) - disconnected
// [ 1%,  34%) - bad
// [34%,  67%) - average
// [67%, 100%] - good
const sendQualityGroups = [0, 1, 34, 67, 101];
// groups for hystersis
const sendQualityGroupsOut = [
    [0, 0],
    [1, 1],
    [1, 39],
    [29, 72],
    [62, 101],
];

class NetQuality extends EventEmitter {
    constructor({ commitSendQualityFunction }) {
        super();

        this._sendQuality = NetQuality.quality.MIN;

        this._sendQualityState = new AtomicUpdate(
            {
                quality: NetQuality.quality.MIN,
            },
            {
                //Each time we set quality we override previous value
                nextStateOverridesPrevious: true,
            }
        );
        this._lastUpdate = 0;
        this._prevQualityGroup = 0;
        this._commitSendQualityFunction = commitSendQualityFunction;
    }
    getSendQuality() {
        return this._sendQuality;
    }
    static getAggregatedQuality(sendQualities) {
        let newQuality;
        if (sendQualities.length === 1) {
            newQuality = sendQualities[0];
        } else if (sendQualities.length > 0) {
            newQuality = Math.min.apply(null, sendQualities);
        } else {
            newQuality = NetQuality.quality.MAX;
        }
        return newQuality;
    }
    setSendQuality(newQuality) {
        assert(typeof newQuality === 'number', 'Quality should be a number');
        const now = Date.now();
        const delta = now - this._lastUpdate;
        newQuality = Math.floor(newQuality);
        newQuality = Math.min(newQuality, NetQuality.quality.MAX);
        newQuality = Math.max(newQuality, NetQuality.quality.MIN);
        const prevQuality = this._sendQuality;
        if (newQuality > prevQuality && delta < 5000) {
            // Prevent change quality to fast in upper
            return;
        }
        if (prevQuality === newQuality) {
            return;
        }
        this._sendQuality = newQuality;
        const prevGroup = this._prevQualityGroup;
        const curMin = sendQualityGroupsOut[prevGroup][0];
        const curMax = sendQualityGroupsOut[prevGroup][1];
        if (curMin <= newQuality && newQuality <= curMax) {
            // Stay in the same group
            return;
        }
        // Find new group
        const newQualityGroup = NetQuality._getQualityGroup(this._sendQuality);
        this._lastUpdate = now;
        this._prevQualityGroup = newQualityGroup;
        this._sendQualityState.update(() => ({ quality: this._sendQuality }));
        this.emit(NetQuality.event.SEND_QUALITY_CHANGED, prevQuality, this._sendQuality);
    }
    commitSendQuality() {
        this._sendQualityState.commit((localState, remoteState, success, fail) => {
            if (localState) {
                this._commitSendQualityFunction(localState, success, fail);
            } else {
                success();
            }
        });
    }
    static _getQualityGroup(quality) {
        return sendQualityGroups.findIndex(maxQuality => quality < maxQuality);
    }
}

NetQuality.quality = {
    MIN: 0,
    MAX: 100,
};

NetQuality.event = {
    SEND_QUALITY_CHANGED: 'send_quality_changed',
};

const NQIChannelSetupMessage = NQIMessage.encode({
    cmd: NQIMessage.Command.SETUP,
    version: '1.0',
    maxStat: 16,
    pinned: [],
}).finish();

export class ServersideNetworkQuality extends EventEmitter {
    constructor({ channel }) {
        super();
        this._channel = channel;
        this._ownNQIData = {};

        this._channel.on(
            Channel.event.READY_STATE_CHANGED,
            this._readyStateChangedHandler.bind(this)
        );
        this._channel.on(Channel.event.MESSAGE, this._onMessageHandler.bind(this));
    }

    _getChannel() {
        return this._channel;
    }

    _readyStateChangedHandler(isReady) {
        if (isReady) {
            this._getChannel().send(NQIChannelSetupMessage);
        }
    }

    _decodeNQIDataMessage(message) {
        return NQIMessage.toObject(NQIMessage.decode(new Uint8Array(message)), {
            defaults: true,
        });
    }

    _mergeNQIStat(oldStat, newStat) {
        return {
            ...(newStat ?? {}),
            txStat: { ...oldStat?.txStat, ...newStat?.txStat },
            rxStat: { ...oldStat?.rxStat, ...newStat?.rxStat },
        };
    }

    _onMessageHandler(message) {
        const data = this._decodeNQIDataMessage(message);
        this.setOwnNQIData(this._mergeNQIStat(this._ownNQIData, data.own));

        this.emit(ServersideNetworkQuality.event.NQI_DATA, data);
    }

    getOwnNQIData() {
        return this._ownNQIData;
    }

    setOwnNQIData(data) {
        this._ownNQIData = data;
    }

    getLocalNQI() {
        return this.getOwnNQIData()?.nqi;
    }

    getMediaTypeByLocalStreamType(localStreamType, isVideoActive) {
        switch (localStreamType) {
            case LocalStream.type.VIDEO_MAIN:
                return isVideoActive
                    ? ServersideNetworkQuality.mediaType.VIDEO
                    : ServersideNetworkQuality.mediaType.AUDIO;
            case LocalStream.type.VIDEO_SCREENSHARING:
                return ServersideNetworkQuality.mediaType.SHARING;
            default:
                return null;
        }
    }

    getLocalNQIUpstreamDetails() {
        return this.getOwnNQIData()?.rxStat;
    }

    getLocalNQIDownstreamDetails() {
        return this.getOwnNQIData()?.txStat;
    }

    getLocalNQIStatDetailsByMediaType(mediaType) {
        const stat = this.getOwnNQIData()?.rxStat?.[mediaType];

        return stat
            ? {
                  rtt: stat.rtt,
                  packetLoss: stat.loss,
                  jitter: stat.jitter,
              }
            : {};
    }
}

ServersideNetworkQuality.event = {
    NQI_DATA: 'nqi_data',
};

ServersideNetworkQuality.mediaType = {
    AUDIO: 'audio',
    VIDEO: 'video',
    SHARING: 'sharing',
};

class Stat {
    constructor(statType) {
        this._statType = statType;
        this._prevUpdate = null;
        this.stat = {};
        this.deltas = {};
    }
    calc(inStat) {
        const now = Date.now();
        const prevUpdate = this._prevUpdate;
        const prevStat = this.stat;
        const stat = Stat.adjust(inStat);
        const interval = (now - prevUpdate) / 1000;
        if (prevUpdate === null || interval <= 0) {
            return { stat, now };
        }
        const deltas = {};
        const rates = {};
        Object.keys(stat).forEach(key => {
            if (typeof stat[key] === 'number' && typeof prevStat[key] === 'number') {
                deltas[key] = stat[key] - prevStat[key];
                rates[key] = deltas[key] / interval;
            }
        });
        return { stat, deltas, rates, now, interval };
    }

    update({ stat, now }) {
        this._prevUpdate = now;
        this.stat = stat;
        return true;
    }

    static adjust(stat) {
        const transformed = {};
        Object.keys(stat).forEach(key => {
            transformed[key] = stat[key];
            switch (typeof stat[key]) {
                case 'number':
                    break;
                case 'string': {
                    const v = parseInt(stat[key]);
                    if (!isNaN(v)) {
                        transformed[key] = v;
                    }
                    break;
                }
                default:
                    break;
            }
        });
        return transformed;
    }
    static median(array) {
        const sorted = [].concat(array).sort((a, b) => a - b);
        if (sorted.length % 2 === 1) {
            return sorted[Math.floor(sorted.length / 2)];
        } else {
            const mid = sorted.length / 2;
            return (sorted[mid - 1] + sorted[mid]) / 2;
        }
    }
    static limit(netQ) {
        return Math.max(1, Math.min(NetQuality.quality.MAX, netQ));
    }
}

class AudioNetQuality {
    constructor(statType) {
        this._audioPpsThreshold = 10;
        this._minPacketsThreshold = 50;
        this._ratingsHistSize = 5;
        this._stat = new Stat(statType);
        this._ratings = [];
        this._netQ = 100;
        this._details = { codec: '', packetLoss: null, rtt: null, jitter: null };
    }
    update(inStat, peerConnectionState) {
        if (inStat.hasOwnProperty('packetsReceived')) {
            return null;
        }
        if (peerConnectionState !== 'connected') {
            return NetQuality.quality.MIN;
        }
        const data = this._stat.calc(inStat);

        this._details.codec = data.stat.googCodecName;
        this._details.rtt = data.stat.googRtt;
        this._details.jitter = data.stat.googJitterReceived;

        if (!data.hasOwnProperty('rates') || !data.deltas.hasOwnProperty('packetsLost')) {
            this._stat.update(data);
            return this._netQ;
        }
        if (data.deltas.packetsSent < this._minPacketsThreshold) {
            return this._netQ;
        }
        this._stat.update(data);
        if (data.rates.packetsSent < this._audioPpsThreshold) {
            // If we do not send packets we slowly increase quality
            this._netQ = this._netQ + (100 - this._netQ) / 8;
            this._details.packetLoss = null;
            return this._netQ;
        }
        const packetLoss = data.deltas.packetsLost / data.deltas.packetsSent;
        this._ratings.push(AudioNetQuality.audioCodecRating(data.stat.googCodecName, packetLoss));
        while (this._ratings.length > this._ratingsHistSize) {
            this._ratings.shift();
        }
        const median = Stat.median(this._ratings);
        this._details.packetLoss = packetLoss;
        this._netQ = Stat.limit(Math.floor(median));
        return this._netQ;
    }
    details() {
        return this._details;
    }
    static audioCodecRating(codec, pLoss) {
        switch (codec) {
            case 'OPUS': {
                const minPloss = 0.1;
                const maxPloss = 0.4;
                return AudioNetQuality.audioRating(pLoss, minPloss, maxPloss);
            }
            default: {
                const minPloss = 0.05;
                const maxPloss = 0.2;
                return AudioNetQuality.audioRating(pLoss, minPloss, maxPloss);
            }
        }
    }
    static audioRating(pLoss, minPloss, maxPloss) {
        if (pLoss < minPloss) {
            return NetQuality.quality.MAX;
        }
        if (pLoss > maxPloss) {
            return NetQuality.quality.MIN;
        }
        return (
            NetQuality.quality.MAX -
            ((pLoss - minPloss) * (NetQuality.quality.MAX - NetQuality.quality.MIN)) /
                (maxPloss - minPloss)
        );
    }
}

class VideoNetQuality {
    constructor(statType) {
        this._stat = new Stat(statType);
        this._minPacketsThreshold = 50;
        this._minInterval = 1;
        this._ploss = [];
        this._rtt = [];
        this._plossHistSize = 5;
        this._netQ = 100;
        this._rttHistSize = 5;
        this._netQhist = [];
        this._netQhistLen = 5;
        this._details = { codec: '', packetLoss: null, rtt: null, jitter: null };
    }
    update(inStat, peerConnectionState) {
        if (inStat.hasOwnProperty('packetsReceived')) {
            return null;
        }
        if (peerConnectionState !== 'connected') {
            return NetQuality.quality.MIN;
        }
        const data = this._stat.calc(inStat);
        if (!data.hasOwnProperty('rates') || !data.deltas.hasOwnProperty('packetsLost')) {
            this._stat.update(data);
            return this._netQ;
        }
        if (data.deltas.packetsSent < this._minPacketsThreshold) {
            return this._netQ;
        }
        if (data.interval < this._minInterval) {
            return this._netQ;
        }
        this._stat.update(data);

        const packetLossSample = data.deltas.packetsLost / data.deltas.packetsSent;
        this._ploss.push(packetLossSample);
        while (this._ploss.length > this._plossHistSize) {
            this._ploss.shift();
        }
        let ploss = Stat.median(this._ploss);
        if (ploss < 0) ploss = 0;
        this._details.packetLoss = ploss;

        const rttSample = data.stat.googRtt;
        this._rtt.push(rttSample);
        while (this._rtt.length > this._rttHistSize) {
            this._rtt.shift();
        }
        const rtt = Stat.median(this._rtt);
        this._details.rtt = rtt;
        this._details.codec = data.stat.googCodecName;

        const { pMin, pMax, rttMin, rttMax } = VideoNetQuality.thresholds(
            data.stat.googContentType
        );
        let netQ;
        if (ploss < pMin - (rtt * pMin) / rttMin) {
            netQ = NetQuality.quality.MAX;
        } else if (ploss < pMax - (rtt * pMax) / rttMin) {
            netQ =
                NetQuality.quality.MAX *
                ((rtt / rttMin) * (pMin / (pMin - pMax)) -
                    ploss / (pMax - pMin) +
                    pMax / (pMax - pMin));
        } else if (ploss < pMax - (rtt * pMax) / rttMax) {
            netQ =
                NetQuality.quality.MAX *
                (rtt / (rttMin - rttMax) -
                    ((ploss / pMax) * rttMax) / (rttMax - rttMin) +
                    rttMax / (rttMax - rttMin));
        } else {
            netQ = NetQuality.quality.MIN;
        }
        netQ = Stat.limit(netQ);
        this._netQhist.push(Math.floor(netQ));
        while (this._netQhist.length > this._netQhistLen) {
            this._netQhist.shift();
        }
        this._netQ = Stat.median(this._netQhist);
        return this._netQ;
    }
    details() {
        return this._details;
    }
    static thresholds(contentType) {
        switch (contentType) {
            case 'screen':
                return { pMin: 0.1, pMax: 0.25, rttMin: 200, rttMax: 1500 };
            default:
                return { pMin: 0.05, pMax: 0.2, rttMin: 200, rttMax: 1500 };
        }
    }
}

class FakeNetQuality {
    constructor() {}
    update() {
        return null;
    }
    details() {
        return {};
    }
    getFractionLost() {}
}

export function createNetQualityCalculator(statType, mediaType) {
    switch (mediaType) {
        case 'audio':
            return new AudioNetQuality(statType);
        case 'video':
            return new VideoNetQuality(statType);
        default:
            return new FakeNetQuality();
    }
}

export default NetQuality;
