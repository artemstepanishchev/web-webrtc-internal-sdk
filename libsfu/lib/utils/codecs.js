export const CODECS = {
    OPUS: 'OPUS/48000/2',
    VP8: 'VP8/90000',
    VP9: 'VP9/90000',
    G722: 'G722/8000',
    PCMU: 'PCMU/8000',
    PCMA: 'PCMA/8000',
    H264: 'H264/90000',
    CN8000: 'CN/8000',
    CN16000: 'CN/16000',
    CN32000: 'CN/32000',
    ULPFEC: 'ULPFEC/90000',
    RED: 'RED/90000',
    RED_AUDIO: 'RED/48000/2',
    FLEXFEC_03: 'FLEXFEC-03/90000',
    RTX: 'RTX/90000',
};

const CODECS_IN_PRIORITY_ORDER = [
    CODECS.OPUS,
    CODECS.G722,
    CODECS.PCMU,
    CODECS.PCMA,
    CODECS.CN8000,
    CODECS.CN16000,
    CODECS.CN32000,
    CODECS.VP8,
    CODECS.H264,
    CODECS.VP9,
    CODECS.ULPFEC,
    CODECS.RED,
    CODECS.RED_AUDIO,
    CODECS.FLEXFEC_03,
];

export const getCodecsPriority = activeCodec => [
    activeCodec,
    ...CODECS_IN_PRIORITY_ORDER.filter(codec => codec !== activeCodec),
];

export const isCodecSupportedBySFU = codecInfo => {
    const codecName = codecInfo.name.toUpperCase();

    if (codecName === CODECS.H264) {
        const h264Params = getH246Params(codecInfo);
        if (h264Params.profileLevelId.length !== 6) {
            return false;
        }
        const level = parseInt(h264Params.profileLevelId.substr(4), 16);

        return h264Params.profileLevelId.match(/^42/) && level <= 31;
    }

    if (codecName === CODECS.VP9) {
        const vp9Params = getVP9Params(codecInfo);
        return vp9Params.profileId === 0;
    }

    return codecName === CODECS.RTX || CODECS_IN_PRIORITY_ORDER.includes(codecName);
};

const getH246Params = codecInfo => {
    const fmtp = codecInfo.fmtp;
    //If no profile-level-id is present, the Baseline profile,
    //without additional constraints at Level 1, MUST be inferred.
    let profileLevelId = '42000a',
        levelAsymmetryAllowed = 0,
        packetizationMode = 0;
    if (Array.isArray(fmtp)) {
        for (let i = 0; i < fmtp.length; i++) {
            const params = fmtp[i];
            let match = params.match(/profile-level-id=([^;$]+)/);
            if (match) {
                profileLevelId = match[1];
            }
            match = params.match(/level-asymmetry-allowed=(\d+)/);
            if (match) {
                levelAsymmetryAllowed = Number(match[1]);
            }
            match = params.match(/packetization-mode=(\d+)/);
            if (match) {
                packetizationMode = Number(match[1]);
            }
        }
    }
    return {
        profileLevelId,
        levelAsymmetryAllowed,
        packetizationMode,
    };
};

const getVP9Params = codecInfo => {
    //If no profile-id specified we are using profile 0
    const fmtp = codecInfo.fmtp;
    let profileId = 0;
    if (Array.isArray(fmtp)) {
        for (let i = 0; i < fmtp.length; i++) {
            const params = fmtp[i];
            const match = params.match(/profile-id=(\d+)/);
            if (match) {
                profileId = Number(match[1]);
            }
        }
    }
    return {
        profileId,
    };
};

export const getCodecsComparator = codecsPriority => (codecA, codecB) => {
    let indexA = codecsPriority.indexOf(codecA.name);
    let indexB = codecsPriority.indexOf(codecB.name);
    // Here we avoiding the bug:
    // https://bugs.chromium.org/p/chromium/issues/detail?id=818371
    // We put all not-known to SFU codecs to the end of the list,
    // Specifically H264 4d0032, and H264 640032
    if (indexA < 0 || !isCodecSupportedBySFU(codecA)) {
        indexA = Infinity;
    }
    if (indexB < 0 || !isCodecSupportedBySFU(codecB)) {
        indexB = Infinity;
    }
    if (indexA < indexB) return -1;

    if (indexA > indexB) return 1;

    return 0;
};

export function getFreeCodecId(takenIds) {
    //https://www.iana.org/assignments/rtp-parameters/rtp-parameters.xhtml
    const MIN_DYNAMIC_PAYLOAD_TYPE = 96;
    const MAX_DYNAMIC_PAYLOAD_TYPE = 127;
    for (let codecId = MIN_DYNAMIC_PAYLOAD_TYPE; codecId <= MAX_DYNAMIC_PAYLOAD_TYPE; codecId++) {
        if (!takenIds.includes(codecId)) {
            return String(codecId);
        }
    }
    throw new Error(
        `All payload types ${MIN_DYNAMIC_PAYLOAD_TYPE}-${MAX_DYNAMIC_PAYLOAD_TYPE} are taken`
    );
}

export const getPrefferedCodecNameBySSRC = (sdp, ssrc) => {
    const ssrcRE = new RegExp(`^${ssrc} `);
    const m = sdp.getMedia().find(m => {
        const ssrcs = sdp.getMediaAttribute(m, 'ssrc');
        return !!ssrcs.find(ssrc => ssrc.match(ssrcRE));
    });
    if (m) {
        const codecId = m.format.split(' ')[0];
        if (codecId) {
            const rtpmap = sdp.getMediaAttribute(m, 'rtpmap');
            const rtpmapRE = new RegExp(`^${codecId} ([^/]+)/`);
            for (let i = 0; i < rtpmap.length; i++) {
                const map = rtpmap[i];
                const match = map.match(rtpmapRE);
                if (match) {
                    return match[1];
                }
            }
        }
    }
};

export const isCodecAllowed = (codecInfo, blacklist) => {
    const codecName = codecInfo.name.toUpperCase();
    return blacklist.indexOf(codecName) < 0;
};

export const areCodecsEqual = (codecA, codecB) => {
    if (codecA.name !== codecB.name) {
        return false;
    }
    if (codecA.name === CODECS.H264) {
        const codecAParams = getH246Params(codecA);
        const codecBParams = getH246Params(codecB);
        return (
            codecAParams.profileLevelId === codecBParams.profileLevelId &&
            codecAParams.packetizationMode === codecBParams.packetizationMode &&
            codecAParams.levelAsymmetryAllowed === codecBParams.levelAsymmetryAllowed
        );
    } else if (codecA.name === CODECS.VP9) {
        const codecAParams = getVP9Params(codecA);
        const codecBParams = getVP9Params(codecB);
        return codecAParams.profileId === codecBParams.profileId;
    } else {
        return true;
    }
};
