/**
 * @param {RTCPeerConnection} pc
 * @param {MediaStreamTrack} track
 * @returns {RTCRtpSender|undefined}
 */
export const getSenderByTrack = (pc, track) => pc
    .getSenders()
    .find(sender => sender.track === track);

const DEFAULT_BITRATE = 32000;

/**
 * Gets bitrate from the first sender encoding if present
 * @param {RTCRtpSender} sender
 * @returns {number|undefined}
 */
export function getBitrateBySender(sender) {
    if (!sender) return;

    const params = sender.getParameters?.();
    const encodings = params?.encodings;
    if (!encodings || encodings.length === 0) {
        return;
    }

    return encodings[0].maxBitrate ?? DEFAULT_BITRATE;
}

/**
 * Sets bitrate for the first sender encoding
 * @param {RTCRtpSender} sender
 * @param {number} bitrate
 * @returns {Promise|undefined}
 */
export function setSenderBitrate(sender, bitrate) {
    if (!sender) return;
    const params = sender.getParameters();
    if (!params.encodings[0]) return;
    params.encodings[0].maxBitrate = bitrate;

    return sender.setParameters(params);
}
