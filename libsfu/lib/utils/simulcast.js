const getIgnoreRtxSsrc = (videoTrackInfo, ssrcId, isRTXEnabled) =>
    videoTrackInfo.FID &&
    !!videoTrackInfo.FID.find(pair => pair[1] === ssrcId) &&
    !isRTXEnabled;

const MAX_SSRC_ID = 4294967295;
const generateSsrcId = () => Math.floor(Math.random() * MAX_SSRC_ID);

export const fillSsrcPair = (ssrcList = []) => ssrcId => {
    const pairSsrcList = generateSsrcId();
    ssrcList.push(pairSsrcList);
    return [ssrcId, pairSsrcList];
};

function setSDPSsrcGroups(localSDP, videoTrackInfo, media, isRTXEnabled) {
    if (videoTrackInfo.SIM && videoTrackInfo.SIM.length > 0) {
        localSDP.setMediaAttribute(media, 'ssrc-group', `SIM ${videoTrackInfo.SIM.join(' ')}`);
    }
    if (videoTrackInfo.FID && isRTXEnabled) {
        videoTrackInfo.FID.forEach(fidSSRC => {
            localSDP.setMediaAttribute(media, 'ssrc-group', `FID ${fidSSRC.join(' ')}`);
        });
    }
    if (videoTrackInfo['FEC-FR']) {
        videoTrackInfo['FEC-FR'].forEach(fecSSRC => {
            localSDP.setMediaAttribute(media, 'ssrc-group', `FEC-FR ${fecSSRC.join(' ')}`);
        });
    }
}

export function modifySDPMedia(localSDP, media, videoTrackInfo, msid, isRTXEnabled) {
    videoTrackInfo.ssrcs.forEach(ssrcId => {
        if (getIgnoreRtxSsrc(videoTrackInfo, ssrcId, isRTXEnabled)) {
            return;
        }
        localSDP.setMediaAttribute(media, 'ssrc', `${ssrcId} cname:${videoTrackInfo.info.cname}`);
        localSDP.setMediaAttribute(media, 'ssrc', `${ssrcId} msid:${msid} ${videoTrackInfo.info.trackId}`);
        localSDP.setMediaAttribute(media, 'ssrc', `${ssrcId} mslabel:${msid}`);
        localSDP.setMediaAttribute(media, 'ssrc', `${ssrcId} label:${videoTrackInfo.info.trackId}`);
    });

    setSDPSsrcGroups(localSDP, videoTrackInfo, media, isRTXEnabled);
}

export function fixPlanBMSIDs(videoTracksInfo, msidRemapping) {
    for (const msid in videoTracksInfo) {
        if (videoTracksInfo.hasOwnProperty(msid)) {
            const remap = msidRemapping.find(remap => remap.from === msid);
            if (remap) {
                videoTracksInfo[remap.to] = videoTracksInfo[msid];
            }
        }
    }
}

export function initializeSimulcast(videoTrackInfo, isFlexFECSupported) {
    videoTrackInfo.SIM = [0, 0, 0].map(generateSsrcId);
    videoTrackInfo.ssrcs = [...videoTrackInfo.SIM];
    const makePair = fillSsrcPair(videoTrackInfo.ssrcs);
    videoTrackInfo.FID = videoTrackInfo.SIM.map(makePair);
    if (isFlexFECSupported) {
        videoTrackInfo['FEC-FR'] = videoTrackInfo.SIM.map(makePair);
    }
}

export function getSdpVideoTracksInfo(localSDP, media) {
    const videoTracksInfo = {};
    const ssrcInfo = {};

    const ssrcs = localSDP.getMediaAttribute(media, 'ssrc');
    const unifiedPlanMsidLine = localSDP.getFirstMediaAttribute(media, 'msid');
    const unifiedPlanMsidData = unifiedPlanMsidLine?.split(' ');
    ssrcs.forEach(ssrcLine => {
        const match = ssrcLine.match(/^(\d+) (.*)/);
        if (!match) {
            return;
        }
        const [, ssrcId, info] = match;
        if (!ssrcInfo.hasOwnProperty(ssrcId)) {
            ssrcInfo[ssrcId] = {};
        }

        const cnameMatch = info.match(/cname:(.*)/);
        if (cnameMatch) {
            ssrcInfo[ssrcId].cname = cnameMatch[1];
        }

        const msidMatch = info.match(/msid:([^ ]+) (.*)/);
        if (msidMatch || unifiedPlanMsidData) {
            ssrcInfo[ssrcId].trackId = msidMatch?.[2] ?? unifiedPlanMsidData[1];
            const msid = msidMatch?.[1] ?? unifiedPlanMsidData[0];
            if (!videoTracksInfo.hasOwnProperty(msid)) {
                videoTracksInfo[msid] = {
                    info: {},
                    ssrcs: [],
                };
            }
            videoTracksInfo[msid].info = ssrcInfo[ssrcId];
            videoTracksInfo[msid].ssrcs.push(ssrcId);
        }
    });

    const groups = localSDP.getMediaAttribute(media, 'ssrc-group');
    groups.forEach(groupLine => {
        const match = groupLine.match(/(FID|SIM|FEC-FR) (.*)/);
        if (!match) {
            return;
        }

        const groupType = match[1];
        const groupSsrcList = match[2].split(' ');

        for (const msid in videoTracksInfo) {
            if (videoTracksInfo.hasOwnProperty(msid)) {
                if (videoTracksInfo[msid].ssrcs.includes(groupSsrcList[0])) {
                    if (groupType === 'FID' || groupType === 'FEC-FR') {
                        videoTracksInfo[msid][groupType] = videoTracksInfo[msid][groupType] || [];
                        videoTracksInfo[msid][groupType].push(groupSsrcList);
                    } else if (groupType === 'SIM') {
                        videoTracksInfo[msid][groupType] = groupSsrcList;
                    }
                    break;
                }
            }
        }
    });

    return videoTracksInfo;
}
