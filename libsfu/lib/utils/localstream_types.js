const LocalStreamTypes = {
    VIDEO_MAIN: 'video/main',
    VIDEO_SCREENSHARING: 'video/screensharing',
};

export default LocalStreamTypes;
