import { toArray } from './misc';
import ClientCapabilities from '../client_capabilities.js';

export const SDP_SEMANTICS = {
    UNIFIED_PLAN: 'unified-plan',
    PLAN_B: 'plan-b',
};

export function getMaybeMultiProp(root, propName) {
    const curValue = root[propName];

    return typeof curValue === 'undefined' ? [] : toArray(curValue);
}

function getSdpConnectionDataLine(root) {
    const c = root.connectionData;
    if (typeof c !== 'undefined') {
        return `c=${c.netType} ${c.addrType} ${c.connectionAddress}`;
    }
}

function makeSdpAttributeLine(name, value) {
    if (typeof value === 'boolean' && value) {
        return `a=${name}`;
    }
    if (typeof value === 'string' || typeof value === 'number') {
        return `a=${name}:${value}`;
    }
    throw new Error(`Unexpected attribute value (name=${name} typeof value=${typeof value})`);
}

function getAttributeLines(root) {
    const lines = [];
    for (const attrName in root.attributes) {
        if (!root.attributes.hasOwnProperty(attrName)) {
            continue;
        }

        const a = getMaybeMultiProp(root.attributes, attrName);
        a.forEach(attrValue => lines.push(makeSdpAttributeLine(attrName, attrValue)));
    }
    return lines;
}

/**
 *
 * @param {object} root
 * @returns {String[]}
 */
export function makeSdpBlock(root) {
    const result = [];

    const c = getSdpConnectionDataLine(root);
    if (c) {
        result.push(c);
    }

    const b = root.bandwidth;
    if (b) {
        result.push(`b=${b.bwtype}:${b.bandwidth}`);
    }

    result.push(...getAttributeLines(root));

    return result;
}

export const getSdpSemantics = preferUnifiedPlan =>
    ClientCapabilities.isSDPUnifiedPlanOnly() || preferUnifiedPlan
        ? SDP_SEMANTICS.UNIFIED_PLAN
        : SDP_SEMANTICS.PLAN_B;
