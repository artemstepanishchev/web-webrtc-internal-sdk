export const iceState = {
    NONE: 'none',
    NEW: 'new',
    CHECKING: 'checking',
    CONNECTED: 'connected',
    COMPLETED: 'completed',
    DISCONNECTED: 'disconnected',
    FAILED: 'failed',
    CLOSED: 'closed',
};

export const sortedIceStates = [
    iceState.NONE,
    iceState.CLOSED,
    iceState.FAILED,
    iceState.DISCONNECTED,
    iceState.NEW,
    iceState.CHECKING,
    iceState.CONNECTED,
    iceState.COMPLETED,
];

// ICE state priority by index (lower index - higher priority)
const iceStatePriority = Object.fromEntries(sortedIceStates.map((state, i) => [state, i + 1]));

/**
 * Compares two given ICE states by their priority.
 * @param {string} iceStateA
 * @param {string} iceStateB
 * @returns {number} value < 0 if the first state has higher priority, 0 - if priority the same.
 */
export const compareIceStatePriority = (iceStateA, iceStateB) =>
    (iceStatePriority[iceStateA] ?? Infinity) - (iceStatePriority[iceStateB] ?? Infinity);

/** Returns ICE state with higher priority */
export const getPriorityIceState = (iceStateA, iceStateB) =>
    compareIceStatePriority(iceStateA, iceStateB) < 0 ? iceStateA : iceStateB;
