import { stringifyPlainObj } from './misc.js';

export const getStunServersOnly = iceServers => {
    if (!Array.isArray(iceServers)) {
        return;
    }
    const res = [];
    iceServers.forEach(iceServer => {
        if (typeof iceServer.urls === 'string') {
            if (iceServer.urls.startsWith('stun:')) {
                res.push(iceServer);
            }
        } else if (Array.isArray(iceServer.urls)) {
            const stunOnlyURLS = iceServer.urls.filter(url => url.startsWith('stun:'));
            if (stunOnlyURLS.length > 0) {
                const stunOnlyIceServer = JSON.parse(JSON.stringify(iceServer));
                stunOnlyIceServer.urls = stunOnlyURLS;
                res.push(stunOnlyIceServer);
            }
        }
    });

    return res;
};

export function getSupportsUnifiedPlanOnly() {
    let pc;

    try {
        pc = new RTCPeerConnection({ sdpSemantics: 'plan-b' });
    } catch (e) {
        return true;
    }

    try {
        pc.addTransceiver('audio');
        // if it adds transceiver it is actually unified plan, and sdpSemantics value is ignored
        return true;
    } catch (e) {
        return false;
    } finally {
        pc.close();
    }
}

export function getAreTransceiversSupported(rtcOptions) {
    const tempPC = new RTCPeerConnection(rtcOptions);
    let result;

    if (typeof tempPC.addTransceiver === 'function') {
        // Safari supports addTransceiver() but not Unified Plan when currentDirection is not defined.
        // eslint-disable-next-line no-undef
        if (!('currentDirection' in RTCRtpTransceiver.prototype)) {
            result = false;
        } else {
            //We call addTransceiver with wrong parameter type here
            //because Chrome has this function, but depends on which SDP semantic is chosen
            //it will raise TypeError (transceivers supported) or
            // Error (plan B semantic cannot use transceivers)
            try {
                tempPC.addTransceiver('audio');
                result = true;
            } catch (err) {
                result = false;
            }
        }
    } else {
        result = false;
    }
    tempPC.close();

    return result;
}

export const stringifyPCState = pc => {
    const { signalingState, connectionState, iceConnectionState, iceGatheringState } = pc;
    return stringifyPlainObj({
        signalingState,
        connectionState,
        iceConnectionState,
        iceGatheringState,
    });
};

const SIMULCAST_TRACK_BITRATES = [150 * 1000, 500 * 1000, 1300 * 1000];
// 0 means that layer is disabled
const SIMULCAST_SHARING_H264_BITRATES = [500 * 1000, 1500 * 1000, 0];
const SIMULCAST_SHARING_VP8_BITRATES = [1000 * 1000, 2000 * 1000, 0];
const SIMULCAST_SHARING_VP8TAB_BITRATES = [1000 * 1000, 2000 * 1000, 0];
const NONSIMULCAST_BITRATE = 1500 * 1000;

/**
 * @typedef BitratesMultiplierConfig
 * @type {Object}
 * @property {number} [total]
 * @property {number} [video]
 * @property {number} [sharing]
 * @property {number} [sharing264]
 * @property {number} [sharingVP8]
 * @property {number} [sharingVP8]
 * @property {number} [sharingVP8Tab]
 */

/**
 * Generates bitrate data for different streams using config (options order from the most specific to generic ones)
 * @param {BitratesMultiplierConfig} [options]
 * @return {{ simulcastSSVP8Tab: number[], simulcastSSVP8: number[], simulcastSSH264: number[], nonSimulcastStream: number, simulcastTrack: number[]} }
 */
export function getStreamsBitrates(options = {}) {
    const videoMultiplier = options.video || options.total || 1;
    const sharingMultiplier = options.sharing || options.total || 1;
    const sharingH264Multiplier = options.sharing264 || sharingMultiplier;
    const sharingVP8Multiplier = options.sharingVP8 || sharingMultiplier;
    const sharingVP8TabMultiplier = options.sharingVP8Tab || sharingVP8Multiplier;

    const simulcastTrack = SIMULCAST_TRACK_BITRATES.map(v => v * videoMultiplier);
    const simulcastSSH264 = SIMULCAST_SHARING_H264_BITRATES.map(v => v * sharingH264Multiplier);
    const simulcastSSVP8 = SIMULCAST_SHARING_VP8_BITRATES.map(v => v * sharingVP8Multiplier);
    // chrome tab sharing often does not work if bitrate for layer is less than 1000 Kbps
    const simulcastSSVP8Tab = SIMULCAST_SHARING_VP8TAB_BITRATES.map(
        v => v * sharingVP8TabMultiplier
    );

    const nonSimulcastStream = NONSIMULCAST_BITRATE * videoMultiplier;

    return {
        simulcastTrack,
        simulcastSSH264,
        simulcastSSVP8,
        simulcastSSVP8Tab,
        nonSimulcastStream,
    };
}

/**
 * Extracts IP addresses from array of collected ICE candidates.
 */
export function extractIceCandidatesIpAddresses(peerConnectionIceCandidates) {
    /**
     * Checks for private IP (IPv4 & IPv6).
     * @see {@link https://stackoverflow.com/a/11327345}
     */
    const regExpPrivateIP = /(^127\.)|(^192\.168\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^::1$)|(^[fF][cCdD])/;

    // Extract public and private IPs.
    const ipAddresses = {
        private: [],
        public: [],
    };

    peerConnectionIceCandidates.forEach(candidate => {
        const address = candidate.address;
        if (!address) return;

        const ipType = regExpPrivateIP.test(address) ? 'private' : 'public';

        // Check for duplicates, because peer connection can has several ice candidates with same IP addresses.
        if (!ipAddresses[ipType].includes(address)) {
            ipAddresses[ipType].push(address);
        }
    });

    return ipAddresses;
}
