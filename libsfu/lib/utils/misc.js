const getPlainObjStringifier = (keyValSeparator = ': ', entrySeparator = ', ') => obj =>
    Object.entries(obj)
        .map(([key, val]) => `${key}${keyValSeparator}${val}`)
        .join(entrySeparator);

export const stringifyPlainObj = getPlainObjStringifier();

export const toArray = value => (Array.isArray(value) ? value : [value]);

export const createSequence = (seq = 0) => () => seq++;

export const fitToRange = (min, max, val) => Math.max(min, Math.min(val, max));

/** A decorator that calls given function only once */
export const once = f => {
    let wasCalled = false;
    let result;
    return (...args) => {
        if (!wasCalled) {
            result = f(...args);
            wasCalled = true;
        }
        return result;
    }
}
