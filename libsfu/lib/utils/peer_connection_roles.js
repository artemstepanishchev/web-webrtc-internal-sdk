const PeerConnectionRoles = {
    MAIN: 'main',
    ADDITIONAL: 'additional',
};

export default PeerConnectionRoles;
