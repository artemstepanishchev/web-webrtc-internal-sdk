export const rejectErr = msg => Promise.reject(msg instanceof Error ? msg : new Error(msg));

/**
 * Returns a promise, along with its resolve and reject callbacks
 * @returns {{ promise: Promise, resolve: Function, reject: Function }}
 */
export function getControllablePromise() {
    const wrap = {};

    wrap.promise = new Promise((resolve, reject) => {
        Object.assign(wrap, { resolve, reject });
    });

    return wrap;
}

export const ABORT_ERROR = new DOMException('Aborted', 'AbortError');

/**
 * Make promise abortable using AbortController.signal
 * @param {Promise} promise
 * @param {AbortSignal} signal
 * @return {Promise}
 */
export function makePromiseAbortable(promise, signal) {
    if (signal.aborted) return Promise.reject(ABORT_ERROR);

    const { promise: signalPromise, reject } = getControllablePromise();

    const abortHandler = () => reject(ABORT_ERROR);

    signal.addEventListener('abort', abortHandler);

    return Promise.race([promise, signalPromise]).finally(() =>
        signal.removeEventListener('abort', abortHandler)
    );
}

export const delay = ms =>
    new Promise(resolve => {
        setTimeout(resolve, ms);
    });
