class ComfortNoise {
    constructor(options) {
        options = options || {};

        try {
            this._audioContext =
                options.audioContext || new (window.AudioContext || window.webkitAudioContext)();
        } catch (e) {
            // eslint-disable-next-line no-console
            console.warn(
                'ComfortNoise: Failed to create AudioContext, please create one and pass to the library.'
            );
            this._enabled = false;
            return this;
        }
        this._enabled = true;
        this._pinkNoise = this._audioContext.createBufferSource();
        this._pinkNoise.buffer = this._getBuffer();
        this._pinkNoise.loop = true;

        this._gain = this._audioContext.createGain();
        this._pinkNoise.connect(this._gain);
        this._gain.gain.value = options.volume || 0.001;
        this._started = false;
    }

    start() {
        if (!this._enabled) {
            return;
        }
        if (!this._started) {
            this._started = true;
            this._pinkNoise.start(0);
        }
        this._gain.connect(this._audioContext.destination);
    }

    stop() {
        if (!this._enabled) {
            return;
        }
        this._gain.disconnect();
    }

    _getBuffer() {
        const bufferSize = 2 * this._audioContext.sampleRate;
        const noiseBuffer = this._audioContext.createBuffer(
            1,
            bufferSize,
            this._audioContext.sampleRate
        );
        const output = noiseBuffer.getChannelData(0);
        let b0, b1, b2, b3, b4, b5, b6;
        b0 = b1 = b2 = b3 = b4 = b5 = b6 = 0.0;
        for (let i = 0; i < bufferSize; i++) {
            const white = Math.random() * 2 - 1;
            b0 = 0.99886 * b0 + white * 0.0555179;
            b1 = 0.99332 * b1 + white * 0.0750759;
            b2 = 0.969 * b2 + white * 0.153852;
            b3 = 0.8665 * b3 + white * 0.3104856;
            b4 = 0.55 * b4 + white * 0.5329522;
            b5 = -0.7616 * b5 - white * 0.016898;
            output[i] = b0 + b1 + b2 + b3 + b4 + b5 + b6 + white * 0.5362;
            output[i] *= 0.11; // (roughly) compensate for gain
            b6 = white * 0.115926;
        }
        return noiseBuffer;
    }
}

export default ComfortNoise;
