import EventEmitter from './events.js';
import ControlMessage from './crl_message_cli.js';
import ControlSession from './crl_session_cli.js';
import LogWrapper from './log_wrapper.js';
import { createSequence } from './utils/misc.js';

const seq = createSequence();

class InSession extends EventEmitter {
    constructor(options) {
        super();

        options = options || {};

        this._seq = seq();
        this._server = options.server;
        this._serverPath = options.serverPath || '';
        this._serverProtocol = options.serverProtocol;
        this._serverSubProtocol = options.serverSubProtocol;
        this._logger = new LogWrapper(options.logger, () => `is:${this._seq}-${this._sessionId}`);
        this._sessionId = options.sessionId;
        this._keepAliveInterval = options.keepAliveInterval;
        this._operationTimeout = options.operationTimeout;

        this._signalingSessionCreated = false;
        this._signalingSessionPendingCreation = false;
        this._signalingSessionPendingDeletion = false;
        this._crlSessionInstanceId = 0;
        this._src = 'webcli';
        this._crlSession = undefined;
        this._status = InSession.status.DISCONNECTED;
        this._onBeforeSessionCreate = options.onBeforeSessionCreate;
        this._pendingRequests = {};
        this._pendingRequestId = 0;
        this._createWebSocket = options.createWebSocket;
    }

    initSignalingConnection(sessionSettings, callback) {
        const self = this;
        let connected = false;
        let sessionCreated;
        const reason =
            sessionSettings &&
            sessionSettings.meta &&
            typeof sessionSettings.meta.reason === 'string'
                ? sessionSettings.meta.reason
                : undefined;

        this._destroySession(reason).then(() => {
            this._crlSessionInstanceId++;
            const reqSeq = this._crlSession ? this._crlSession.getRequestSeq() : 0;
            this._disconnectCrlSession();
            const crlSession = (this._crlSession = new ControlSession({
                server: this._server,
                serverPath: this._serverPath,
                protocol: this._serverProtocol,
                subProtocol: this._serverSubProtocol,
                logger: this._logger,
                sessionId: this._sessionId,
                keepAliveInterval: this._keepAliveInterval,
                req_seq: reqSeq, // eslint-disable-line camelcase
                src: this._src,
                instanceId: this._crlSessionInstanceId,
                createWebSocket: this._createWebSocket,
            }));

            this._crlSession.on(ControlSession.event.CLOSE, () => {
                if (crlSession !== this._crlSession || this._signalingSessionPendingDeletion) {
                    this._logger.log('Connection closed, but we already working with a new one');
                    return;
                }
                this._signalingSessionPendingCreation = false;
                this._updateStatus();
                if (!connected) {
                    callback &&
                        (callback(new Error('Connection failed')) || (callback = undefined));
                } else if (!sessionCreated) {
                    callback &&
                        (callback(new Error('Connection dropped before session created')) ||
                            (callback = undefined));
                } else if (this._signalingSessionCreated) {
                    this._signalingSessionCreated = false;
                    this._updateStatus();
                    this._logger.logDebug(
                        'Signaling session disconnected, because connection dropped'
                    );
                    //                                           normalClose, forcedByServer, signalingMessage,   reason
                    this.emit(
                        InSession.event.SESSION_DESTROYED,
                        false,
                        false,
                        undefined,
                        'Signaling session disconnected, because connection dropped'
                    );
                } else {
                    this._logger.logWarn(
                        `Unexpected close: connected=${connected} sessionCreated=${sessionCreated} this._signalingSessionCreated=${this._signalingSessionCreated}`
                    );
                }
            });

            this._crlSession.on(ControlSession.event.MESSAGE, message => {
                if (crlSession !== this._crlSession) {
                    this._logger.log(
                        'Got message from connection, but we already working with a new one'
                    );
                    return;
                }
                const self = this;
                //We assume that we can get message only from current crlSession
                const event = message.getEvent();

                if (event === ControlMessage.event.KEEP_ALIVE_REQ) {
                    this.respondOK(message);
                    return;
                }

                if (
                    [
                        ControlMessage.event.CREATE_RES,
                        ControlMessage.event.MEDIA_STAT_RES,
                        ControlMessage.event.INFO_RES,
                    ].includes(event)
                ) {
                    return;
                }

                if (this._signalingSessionCreated) {
                    if (message.getErrCode() === ControlMessage.errCode.SESSION_NOT_FOUND) {
                        closeSession('server respond: SESSION_NOT_FOUND', false, false, message);
                        return;
                    } else if (message.getEvent() === ControlMessage.event.DESTROY_REQ) {
                        this.respondOK(
                            // request, TODO this was undefined
                            undefined,
                            {},
                            function () {
                                closeSession('server sent: destroy_req', true, true, message);
                            }.bind(this)
                        );
                        return;
                    } else if (message.getEvent() === ControlMessage.event.DESTROY_RES) {
                        let normalClose;
                        if (message.getRequestSrc() !== this._src) {
                            normalClose = message.isSuccess();
                            const forced =
                                !normalClose &&
                                [
                                    ControlMessage.errCode.INVALID_TOKEN,
                                    ControlMessage.errCode.SESSION_REMOVED,
                                ].indexOf(message.getErrCode()) > -1;
                            closeSession('server sent destroy_resp', normalClose, forced, message);
                        }
                        return;
                    }
                    this.emit(InSession.event.MESSAGE, message);
                }

                function closeSession(reason, normalClose, forced, signalingMessage) {
                    self._signalingSessionCreated = false;
                    self._logger.logDebug('Signaling session disconnected, because: ' + reason);
                    self._disconnectCrlSession();
                    self.emit(
                        InSession.event.SESSION_DESTROYED,
                        normalClose,
                        forced,
                        signalingMessage,
                        reason
                    );
                }
            });

            this._crlSession.once(ControlSession.event.CONNECTED, () => {
                if (crlSession !== this._crlSession) {
                    this._logger.log(
                        'Connection established, but we already working with a new one'
                    );
                    return;
                }
                this._signalingSessionPendingCreation = false;
                onConnection();
            });

            function onConnection() {
                connected = true;
                if (!self._signalingSessionPendingCreation) {
                    self._crlSession.setInstanceId(self._crlSessionInstanceId);
                    self._signalingSessionPendingCreation = true;
                    self._updateStatus();
                    if (typeof self._onBeforeSessionCreate === 'function') {
                        self._onBeforeSessionCreate(() => {
                            self._onBeforeSessionCreate = undefined;
                            sendCreateRequest();
                        });
                    } else {
                        sendCreateRequest();
                    }
                } else {
                    self._updateStatus();
                    self._logger.logWarn(
                        "Will not send 'create_req', because didn't get response on previous one."
                    );
                }

                function sendCreateRequest() {
                    self._crlSession.request(
                        {
                            event: ControlMessage.event.CREATE_REQ,
                            body: sessionSettings,
                        },
                        function (err, response) {
                            self._signalingSessionPendingCreation = false;
                            if (!err && response.isSuccess()) {
                                sessionCreated = true;
                                if (!self._signalingSessionCreated) {
                                    self._signalingSessionCreated = true;
                                    self._logger.logDebug('Signaling session connected');
                                    self.emit(InSession.event.SESSION_CREATED);
                                }
                                self.respondACKOK(response);
                            }
                            self._updateStatus();
                            callback && (callback(err, response) || (callback = undefined));
                        }.bind(this)
                    );
                }
            }
        });
    }

    execute(callback) {
        if (this._signalingSessionCreated) {
            callback(null);
        } else {
            this.once(InSession.event.SESSION_CREATED, () => {
                this.execute(callback);
            });
        }
    }

    request(req, callback) {
        if (typeof callback !== 'function') {
            return new Promise((resolve, reject) => {
                this.request(req, (err, response) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(response);
                    }
                });
            });
        }

        if (!this._crlSession) {
            return callback(
                new Error(
                    `Cannot send request, because signalling session doesn't exist ${JSON.stringify(
                        req
                    )}`
                )
            );
        }

        const raiseError = errMessage => {
            this._logger.log('send: ' + JSON.stringify(req));
            this._logger.logError(errMessage);
            callback?.(new Error(errMessage));
            callback = null;
        };

        if (!this._crlSession.isConnected()) {
            return raiseError('Cannot send request, because signaling socket is disconnected');
        }

        if (!this._signalingSessionCreated) {
            return raiseError('Cannot send request, because signaling session is not established');
        }

        if (this._signalingSessionPendingDeletion) {
            return raiseError('Cannot send request, because signaling session is destroying');
        }

        let timeout;
        this._pendingRequestId++;
        const pendingRequestId = String(this._pendingRequestId);
        if (callback) {
            const currentCrlSession = this._crlSession;
            timeout = setTimeout(() => {
                delete this._pendingRequests[pendingRequestId];
                if (currentCrlSession === this._crlSession) {
                    raiseError('Request is timed out (no response).');
                } else {
                    this._logger.logWarn(
                        'Request is timed out (no response) but control session already re-established:' +
                            JSON.stringify(req)
                    );
                }
            }, this._operationTimeout);

            this._pendingRequests[pendingRequestId] = {
                close: reason => {
                    delete this._pendingRequests[pendingRequestId];
                    clearTimeout(timeout);
                    raiseError(
                        `Pending request closed manually (id = ${pendingRequestId}): ${reason}`
                    );
                },
            };
        }
        this._crlSession.request(req, (err, response) => {
            if (timeout) {
                clearTimeout(timeout);
            }
            delete this._pendingRequests[pendingRequestId];
            callback?.(err, response);
            callback = null;
        });
    }

    _closePendingRequests(reason) {
        for (const id in this._pendingRequests) {
            if (this._pendingRequests.hasOwnProperty(id)) {
                this._pendingRequests[id].close(`Cannot send request, because ${reason}`);
            }
        }
        this._pendingRequests = {};
    }

    close(reason = 'Requested to close') {
        this._destroySession(reason).then(result => {
            if (result.requestSent) {
                this.emit(InSession.event.SESSION_DESTROYED, true, false, undefined, reason);
            }
            this._disconnectCrlSession();
        });
    }

    _destroySession(reason) {
        const crlSession = this._crlSession;
        this._closePendingRequests(`Destroying session, reason: ${reason}`);

        if (this._crlSession && this._crlSession.isConnected() && this._signalingSessionCreated) {
            if (!this._signalingSessionPendingDeletion) {
                return new Promise(resolve => {
                    this.request(
                        {
                            event: ControlMessage.event.DESTROY_REQ,
                            body: {
                                reason,
                            },
                        },
                        (err, response) => {
                            if (crlSession === this._crlSession) {
                                if (err || (response && !response.isSuccess())) {
                                    //Since event SESSION_NOT_FOUND will be handled in ON MESSAGE handler we don't need to handle it separately here
                                    if (
                                        err ||
                                        response.getErrCode() !==
                                            ControlMessage.errCode.SESSION_NOT_FOUND
                                    ) {
                                        if (this._crlSession && this._crlSession.isConnected()) {
                                            //If we cannot deliver destroy_req -> close connection
                                            this._disconnectCrlSession();
                                        }
                                    }
                                }
                                this._signalingSessionCreated = false;
                                this._signalingSessionPendingDeletion = false;
                                this._updateStatus();
                            }
                            resolve({
                                requestSent: true,
                            });
                        }
                    );
                    this._signalingSessionPendingDeletion = true;
                });
            } else {
                return Promise.resolve({
                    requestSent: false,
                });
            }
        } else {
            return Promise.resolve({
                requestSent: false,
            });
        }
    }

    _disconnectCrlSession() {
        if (this._crlSession) {
            this._crlSession.close();
        }
    }

    _updateStatus() {
        let newStatus;
        const st = InSession.status;
        if (!this._crlSession || !this._crlSession.isConnected()) {
            newStatus = st.DISCONNECTED;
        } else if (!this._signalingSessionCreated) {
            if (this._signalingSessionPendingCreation) {
                newStatus = st.CONNECTED;
            } else {
                newStatus = st.CONNECTING;
            }
        } else {
            newStatus = st.IN_SESSION;
        }
        if (newStatus !== this._status) {
            this._status = newStatus;
            this.emit(InSession.event.STATUS_CHANGED, this._status);
        }
    }

    getStatus() {
        return this._status;
    }

    isConnected() {
        return this._signalingSessionCreated;
    }

    isDestroyingPreviousSession() {
        return this._signalingSessionPendingDeletion;
    }

    getInstanceId() {
        return this._crlSession.getInstanceId();
    }

    respondOK() {
        this._crlSession.respondOK.apply(this._crlSession, arguments);
    }

    respondError() {
        this._crlSession.respondError.apply(this._crlSession, arguments);
    }

    respondACKOK() {
        this._crlSession.respondACKOK.apply(this._crlSession, arguments);
    }

    respondACKError() {
        this._crlSession.respondACKError.apply(this._crlSession, arguments);
    }

    devDropSignaling() {
        this._disconnectCrlSession();
    }

    setSessionId(newSessionId) {
        this._sessionId = newSessionId;
        this._crlSession.setSessionId(this._sessionId);
    }

    getCrlSession() {
        return this._crlSession;
    }
}

InSession.event = {
    SESSION_DESTROYED: 'session_destroyed',
    SESSION_CREATED: 'session_created',
    MESSAGE: 'message',
    STATUS_CHANGED: 'status_chnaged',
};

InSession.status = {
    DISCONNECTED: 'disconnected',
    CONNECTING: 'connecting',
    CONNECTED: 'connected',
    IN_SESSION: 'in_session',
};

export default InSession;
