import EventEmitter from './events.js';
import ClientCapabilities from './client_capabilities.js';
import LogWrapper from './log_wrapper.js';

class StreamsStat extends EventEmitter {
    constructor(options) {
        super();

        options = options || {};

        this._sfuRemoteOrder = [];
        this._sfuRemoteStat = {};
        this._lastRemoteStreamsSFUStat = [];
        this._lastLocalStreamsSFUStat = [];
        this._sfuLocalStat = {};
        this._lastActivity = {};
        this._logger = new LogWrapper(options.logger, () => 'ss:' + this._sessionId);
        this._sessionId = options.sessionId;
        this._recentActivityDelay =
            typeof options.recentActivityDelay === 'undefined'
                ? 60000
                : options.recentActivityDelay;
        this._lastActive = [];
        this._updateLastActiveTimer = undefined;
        this._getRemoteStreamByTapId = options.getRemoteStreamByTapId;
        this._getLocalStreamByTapId = options.getLocalStreamByTapId;
        this._fakeQualityStream = {};
    }

    setFakeQuality(streamId, opts) {
        this._fakeQualityStream[streamId] = opts;
        this.emit(StreamsStat.event.SFU_STAT_QUALITY_CHANGED, [streamId]);
    }

    cancelFakeQuality(streamId) {
        delete this._fakeQualityStream[streamId];
        this.emit(StreamsStat.event.SFU_STAT_QUALITY_CHANGED, [streamId]);
    }

    getRecentlyActiveStreams() {
        return this._lastActive;
    }

    _updateLastActive() {
        const prevLastActive = this._lastActive;

        const now = Date.now();

        this._lastActive = Object.keys(this._lastActivity)
            //Don't count pstn streams
            .filter(
                tapId =>
                    tapId.indexOf('mixed:') !== 0 &&
                    now - this._lastActivity[tapId] < this._recentActivityDelay
            )
            .sort();

        if (!isArraysAreSame(prevLastActive, this._lastActive)) {
            this.emit(StreamsStat.event.RECENTLY_ACTIVE_CHANGED, this._lastActive);
        }

        if (this._lastActive.length > 0) {
            let delay = this._lastActive
                .map(tapId => this._recentActivityDelay - now + this._lastActivity[tapId])
                .reduce((min, val) => Math.min(val, min), Infinity);

            if (delay < Infinity) {
                delay = Math.max(0, delay);
                clearTimeout(this._updateLastActiveTimer);
                this._updateLastActiveTimer = setTimeout(() => this._updateLastActive(), delay);
            }
        }

        function isArraysAreSame(arrA, arrB) {
            if (arrA.length !== arrB.length) {
                return false;
            }
            for (let i = arrA.length; i--; ) {
                if (arrA[i] !== arrB[i]) {
                    return false;
                }
            }
            return true;
        }
    }

    setSessionId(newSessionId) {
        this._sessionId = newSessionId;
    }

    addRemoteStreamToStat(tapId) {
        if (!this._sfuRemoteStat[tapId]) {
            this._sfuRemoteStat[tapId] = {
                volume: 0,
                quality: 0,
                qualityMAX: 0,
                qualityDesired: 0,
                qualityAvailable: 0,
            };
            this._sfuRemoteOrder.push(tapId);
            this._lastActivity[tapId] = 0;
            this._updateLastActive();
            setTimeout(
                () => this.emit(StreamsStat.event.SFU_STAT_ORDER_CHANGED, this._sfuRemoteOrder),
                0
            );
        }
    }

    getSFURemoteOrder() {
        return this._sfuRemoteOrder;
    }

    addLocalStreamToStat(tapId) {
        this._sfuLocalStat[tapId] = {
            volume: 0,
            quality: 0,
        };
    }

    removeRemoteStreamFromStat(tapId) {
        if (this._sfuRemoteStat[tapId]) {
            delete this._sfuRemoteStat[tapId];
            this._sfuRemoteOrder = this._sfuRemoteOrder.filter(id => id !== tapId);
            delete this._lastActivity[tapId];
            this._updateLastActive();
            this.emit(StreamsStat.event.SFU_STAT_ORDER_CHANGED, this._sfuRemoteOrder);
        }
    }

    removeLocalStreamFromStat(tapId) {
        delete this._sfuLocalStat[tapId];
    }

    updateLocalStat(remoteStreamsStat, localStreamsStat) {
        //If there is no stat for some stream, we will compensate it using
        //last known value from SFU
        this._lastRemoteStreamsSFUStat.forEach(remoteStreamStatSFU => {
            if (
                remoteStreamsStat.findIndex(
                    remoteStreamStatLocal => remoteStreamStatLocal.id === remoteStreamStatSFU.id
                ) < 0
            ) {
                remoteStreamsStat.push(remoteStreamStatSFU);
            }
        });
        this._lastLocalStreamsSFUStat.forEach(localStreamStatSFU => {
            if (
                localStreamsStat.findIndex(
                    localStreamStatLocal => localStreamStatLocal.id === localStreamStatSFU.id
                ) < 0
            ) {
                localStreamsStat.push(localStreamStatSFU);
            }
        });
        this._updateStat(remoteStreamsStat, localStreamsStat);
    }

    updateSFUStat(remoteStreamsStat, localStreamsStat) {
        if (typeof remoteStreamsStat !== 'undefined') {
            this._lastRemoteStreamsSFUStat = remoteStreamsStat;
        }
        if (typeof localStreamsStat !== 'undefined') {
            this._lastLocalStreamsSFUStat = localStreamsStat;
        }
        this._updateStat(remoteStreamsStat, localStreamsStat);
    }

    _updateStat(remoteStreamsStat, localStreamsStat) {
        if (remoteStreamsStat) {
            //Make sure that there is no removed streams in the stat
            remoteStreamsStat = remoteStreamsStat.filter(
                remoteStreamStat => !!this._sfuRemoteStat[remoteStreamStat.id]
            );
        }
        if (localStreamsStat) {
            //Make sure that there is no removed streams in the stat
            localStreamsStat = localStreamsStat.filter(
                localStreamStat => !!this._sfuLocalStat[localStreamStat.id]
            );
        }

        let changes = {
            order: false,
            volume: [],
            quality: [],
            any: [],
            newOrder: [],
            rotate: [],
        };
        let remoteChanges;
        if (remoteStreamsStat) {
            remoteChanges = this._getRemoteStreamsChanges(remoteStreamsStat);

            remoteStreamsStat.forEach(statInfo => {
                if (statInfo.v > 0) {
                    this._lastActivity[statInfo.id] = Date.now();
                }
            });
            this._updateLastActive();
        }
        let localChanges;
        if (localStreamsStat) {
            localChanges = this._getLocalStreamsChanges(localStreamsStat);
        }

        if (localChanges && remoteChanges) {
            changes.volume = remoteChanges.volume.concat(localChanges.volume);
            changes.quality = remoteChanges.quality.concat(localChanges.quality);
            changes.any = remoteChanges.any.concat(localChanges.any);
            changes.order = remoteChanges.order;
            changes.newOrder = remoteChanges.newOrder;
            changes.rotate = remoteChanges.rotate;
        } else if (remoteChanges) {
            changes = remoteChanges;
        } else if (localChanges) {
            changes = localChanges;
        }

        if (changes.order) {
            this._sfuRemoteOrder = changes.newOrder.concat(
                this._sfuRemoteOrder.filter(tapId => changes.newOrder.indexOf(tapId) < 0)
            );

            this.emit(StreamsStat.event.SFU_STAT_ORDER_CHANGED, this._sfuRemoteOrder);
        }
        if (changes.volume.length > 0) {
            this.emit(StreamsStat.event.SFU_STAT_VOLUME_CHANGED, changes.volume);
        }
        if (changes.quality.length > 0) {
            this.emit(StreamsStat.event.SFU_STAT_QUALITY_CHANGED, changes.quality);
        }
        if (changes.any.length > 0) {
            this.emit(StreamsStat.event.SFU_STAT_CHANGED, changes.any);
        }
        if (!ClientCapabilities.rotationExtensionSupport && changes.rotate.length > 0) {
            this.emit(StreamsStat.event.SFU_STAT_ROTATE_CHANGED, changes.rotate);
        }
        if (localChanges && localChanges.qualityDesired.length > 0) {
            this.emit(
                StreamsStat.event.SFU_STAT_LOCAL_STREAMS_DESIRED_QUALITY_CHANGED,
                localChanges.qualityDesired
            );
        }
    }

    _getLocalStreamsChanges(localStreamsStat) {
        if (!Array.isArray(localStreamsStat)) {
            this._logger.logError(
                'Expected array, but got ' + typeof localStreamsStat + ' in _updateSFUStat'
            );
            return;
        }

        const changedVolumesTapIds = [];
        const changedQualitiesTapIds = [];
        const changedTapIds = [];
        const changedQualitiesDesiredTapIds = [];

        const statMap = {};

        localStreamsStat.forEach(statInfo => {
            const tapId = statInfo.id;
            statMap[tapId] = {
                volume: statInfo.v || 0,
                quality: statInfo.q || 0,
                qualityDesired: statInfo.desired_q || 0,
                qualityAvailable: statInfo.available_q || 0,
                rotate: statInfo.rotate || 0,
            };
        });

        for (const tapId in this._sfuLocalStat) {
            if (this._sfuLocalStat.hasOwnProperty(tapId)) {
                const oldStatInfo = this._sfuLocalStat[tapId];
                let newStatInfo = statMap[tapId];
                if (!newStatInfo) {
                    newStatInfo = {
                        volume: 0,
                        quality: 0,
                        qualityDesired: 0,
                        qualityAvailable: 0,
                    };
                }
                let isSomethingChanged = false;
                if (newStatInfo.volume !== oldStatInfo.volume) {
                    changedVolumesTapIds.push(tapId);
                    this._sfuLocalStat[tapId].volume = newStatInfo.volume;
                    isSomethingChanged = true;
                }
                const desiredQualityChanged =
                    newStatInfo.qualityDesired !== oldStatInfo.qualityDesired;

                if (
                    newStatInfo.quality !== oldStatInfo.quality ||
                    newStatInfo.qualityAvailable !== oldStatInfo.qualityAvailable ||
                    desiredQualityChanged
                ) {
                    changedQualitiesTapIds.push(tapId);
                    if (desiredQualityChanged) {
                        changedQualitiesDesiredTapIds.push(tapId);
                    }
                    this._sfuLocalStat[tapId].quality = newStatInfo.quality;
                    this._sfuLocalStat[tapId].qualityAvailable = newStatInfo.qualityAvailable;
                    this._sfuLocalStat[tapId].qualityDesired = newStatInfo.qualityDesired;
                    isSomethingChanged = true;
                }
                if (isSomethingChanged) {
                    changedTapIds.push(tapId);
                }
            }
        }

        return {
            volume: changedVolumesTapIds,
            quality: changedQualitiesTapIds,
            qualityDesired: changedQualitiesDesiredTapIds,
            any: changedTapIds,
            rotate: [],
        };
    }

    _isStreamHasVideo(tapId) {
        const stream = this._getRemoteStreamByTapId(tapId);
        if (stream) {
            return stream.getVideoTracks().length > 0;
        } else {
            return false;
        }
    }

    _getRemoteStreamsChanges(remoteStreamsStat) {
        if (!Array.isArray(remoteStreamsStat)) {
            this._logger.logError(
                'Expected array, but got ' + typeof remoteStreamsStat + ' in _updateSFUStat'
            );
            return;
        }

        const changedVolumesTapIds = [];
        const changedQualitiesTapIds = [];
        const changedRotateTapIds = [];
        const changedTapIds = [];

        const statMap = {};

        remoteStreamsStat.forEach(statInfo => {
            // emulate report
            //let date = new Date();
            //let tmp_rotate = date.getSeconds();
            //tmp_rotate = ~~ (tmp_rotate / 15);
            const tapId = statInfo.id;
            statMap[tapId] = {
                volume: statInfo.v || 0,
                quality: statInfo.q || 0,
                qualityDesired: statInfo.desired_q || 0,
                qualityAvailable: statInfo.available_q || 0,
                rotate: statInfo.rotate || 0,
                //rotate: tmp_rotate
            };
        });

        const newOrder = remoteStreamsStat.map(item => item.id);

        const isOrderChanged = !!newOrder.find((tapId, index) => {
            return this._sfuRemoteOrder.indexOf(tapId) !== index;
        });

        for (const tapId in this._sfuRemoteStat) {
            if (this._sfuRemoteStat.hasOwnProperty(tapId)) {
                const oldStatInfo = this._sfuRemoteStat[tapId];
                let newStatInfo = statMap[tapId];
                if (!newStatInfo) {
                    newStatInfo = {
                        volume: 0,
                        quality: 0,
                        qualityDesired: 0,
                        qualityAvailable: 0,
                        rotate: 0,
                    };
                }
                let isSomethingChanged = false;
                if (newStatInfo.volume !== oldStatInfo.volume) {
                    changedVolumesTapIds.push(tapId);
                    this._sfuRemoteStat[tapId].volume = newStatInfo.volume;
                    isSomethingChanged = true;
                }
                if (
                    newStatInfo.quality !== oldStatInfo.quality ||
                    newStatInfo.qualityAvailable !== oldStatInfo.qualityAvailable ||
                    newStatInfo.qualityDesired !== oldStatInfo.qualityDesired
                ) {
                    changedQualitiesTapIds.push(tapId);
                    this._sfuRemoteStat[tapId].quality = newStatInfo.quality;
                    this._sfuRemoteStat[tapId].qualityAvailable = newStatInfo.qualityAvailable;
                    this._sfuRemoteStat[tapId].qualityDesired = newStatInfo.qualityDesired;
                    isSomethingChanged = true;
                }
                if (newStatInfo.rotate !== oldStatInfo.rotate) {
                    this._sfuRemoteStat[tapId].rotate = newStatInfo.rotate;
                    changedRotateTapIds.push({ tapId: tapId, rotate: newStatInfo.rotate });
                    isSomethingChanged = true;
                }
                if (isSomethingChanged) {
                    changedTapIds.push(tapId);
                }
            }
        }

        return {
            order: isOrderChanged,
            volume: changedVolumesTapIds,
            quality: changedQualitiesTapIds,
            rotate: changedRotateTapIds,
            any: changedTapIds,
            newOrder: newOrder, //Contains only tapIds on top of the list
        };
    }

    getStatForLocalStream(tapId) {
        return this._getStatForStream(tapId, true);
    }

    getStatForRemoteStream(tapId) {
        return this._getStatForStream(tapId, false);
    }

    _getStatForStream(tapId, isLocal) {
        const sfuStat = (isLocal ? this._sfuLocalStat[tapId] : this._sfuRemoteStat[tapId]) || {
            volume: 0,
            quality: 0,
            qualityDesired: 0,
            qualityAvailable: 0,
        };
        let stat;
        if (sfuStat) {
            stat = {
                audio: {
                    volume: sfuStat.volume,
                },
                video: {
                    quality: sfuStat.quality,
                    qualityDesired: sfuStat.qualityDesired,
                    qualityAvailable: sfuStat.qualityAvailable,
                },
            };
        } else {
            stat = {
                audio: {
                    volume: 0,
                },
                video: {
                    quality: 0,
                    qualityDesired: 0,
                    qualityAvailable: 0,
                },
            };
        }
        stat.local = isLocal;
        const fakeStat = this._fakeQualityStream[tapId];
        if (fakeStat) {
            stat.video = Object.assign(stat.video, fakeStat);
        }
        return stat;
    }
}

StreamsStat.event = {
    SFU_STAT_ORDER_CHANGED: 'sfu_stat_order_changed',
    SFU_STAT_ROTATE_CHANGED: 'sfu_stat_rotate_changed',
    SFU_STAT_VOLUME_CHANGED: 'sfu_stat_volume_changed',
    SFU_STAT_QUALITY_CHANGED: 'sfu_stat_quality_changed',
    SFU_STAT_CHANGED: 'sfu_stat_changed',
    SFU_STAT_LOCAL_STREAMS_DESIRED_QUALITY_CHANGED:
        'sfu_stat_local_streams_desired_quality_changed',
    RECENTLY_ACTIVE_CHANGED: 'recently_active_changed',
};

export default StreamsStat;
