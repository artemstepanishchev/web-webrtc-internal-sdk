import EventEmitter from './events.js';
import assert from './assert.js';
import ControlMessage from './crl_message_cli.js';
import SDP, { getMsidToMidMapping } from './sdp_cli.js';
import StreamsStat from './streams_stat.js';
import InSession from './in_session.js';
import Recovery from './recovery.js';
import Preferences from './preferences.js';
import ComfortNoise from './comfort_noise.js';
import PeersController from './peers_controller.js';
import LocalStream from './local_stream.js';
import P2PStatMeter from './p2p_stat_meter.js';
import Channel from './channel.js';
import ClientCapabilities from './client_capabilities.js';
import StreamRotate from './stream_rotate.js';
import NetQuality, { ServersideNetworkQuality } from './net_quality.js';
import PreserveVideo from './preserve_video.js';
import BasePeerConnection from './base_peer_connection.js';
import LogWrapper from './log_wrapper.js';
import EventLoopDelay from './event_loop_delay.js';
import { extractIceCandidatesIpAddresses } from './utils/peer_connection.js';
import { stringifyPlainObj, createSequence } from './utils/misc.js';
import { getSdpSemantics } from './utils/sdp.js';

const RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription;
const seq = createSequence();

class ControlClient extends EventEmitter {
    /**
     * ControlClient class
     * @param {Object} options
     * @param {String} options.sessionId
     * @param {String} [options.sessionToken=""]
     * @param {String} [options.conferenceId=""]
     * @param {Boolean} [options.autoRecovery=false]
     * @param {Number} [options.operationTimeout=5000]
     * @param {Boolean} [options.controlChannel=false]
     * @param {Object} [options.logger=Console]
     * @param {String} [options.codec]
     * @param {String} [options.codecForP2P=SDP.codec.H264]
     * @param {String} [options.codecForScreenSharing=SDP.codec.H264]
     * @param {String} [options.audioRate="48000"]
     * @param {String} [options.audioBitrate="32000"]
     * @param {String} [options.audioRateScreenSharing="48000"]
     * @param {String} [options.audioBitrateScreenSharing="128000"]
     * @param {Object} [options.meta={}]
     * @param {String} [options.meta.meeting_id]
     * @param {String} [options.meta.user_agent]
     * @param {String} [options.meta.username]
     * @param {String} [options.meta.xUserAgent]
     * @param {Object} [options.forceTransport=false]
     * @param {String[]} [options.blacklistedCodecs=[]]
     * @param {Object} [options.recentActivityDelay=60000]
     * @param {String} [options.server=<current host>]
     * @param {String} [options.serverPath=""]
     * @param {String} [options.serverProtocol=<ws: or wss: depends on current protocol>]
     * @param {String} [options.serverSubProtocol="vcsfu"]
     * @param {RTCIceServer[]} [options.iceServers]
     * @param {Number} [options.keepAliveInterval=5000]
     * @param {Number} [options.streamRecoveryTimeout=30000]
     * @param {Object|Number} [options.maxRemoteVideo=1]
     * @param {Number} [options.maxRemoteAudio=1]
     * @param {Number} [options.maxP2PConnections=0]
     * @param {Boolean} [options.enableComfortNoise=false]
     * @param {AudioContext} [options.comfortNoiseAudioContext=<new AudioContext>]
     * @param {Function} [options.onBeforeSessionCreate]
     * @param {Boolean} [options.preferUnifiedPlan=false]
     * @param {Boolean} [options.setSendBitrate=true]
     * @param {String} [options.mixAudioMode="never"]
     * @param {String} [options.negotiateVideoMode="always"]
     * @param {Number} [options.noMediaPingTimeout=5000]
     * @param {Number} [options.eventLoopInterval=200]
     * @param {BitratesMultiplierConfig} [options.bitrateMultipliers]
     * @param {Boolean} [options.agc=true] Automatic Gain Control (audio) on server-side
     * @param {Boolean} [options.noiseGate=true] Noise Gate (audio) on server-side
     * @param {Boolean} [options.enableQ1HalfFps=false]
     * @param {Function} [options.createWebSocket]
     * @param {Boolean} [options.allowAudioRed]
     * @param {Boolean} [options.disableDSCP] force disalbe DSCP
     * @constructor
     */
    constructor(options) {
        super();

        this._options = options;

        this._seq = seq();
        this._sessionId = options.sessionId;
        this._sessionToken = options.sessionToken || '';
        this._conferenceId = options.conferenceId || '';
        this._disableDSCP = options.disableDSCP || false;

        this._setSendBitrate = options.setSendBitrate;

        this._iceServersFromSignalling = null;

        this._localStreams = [];
        this._remoteStreams = {};

        this._tapIdByStreamId = {};
        this._streamIdsByTapId = {};
        this._pendingStreamIdsToDelete = {};

        this._preserveVideo = new PreserveVideo();

        this._streamsRotate = {};

        this._autoRecovery = options.autoRecovery === true;

        this._recoveryRequired = false;

        this._operationTimeout = options.operationTimeout || 10000;

        this._maxUpdateStatInterval = 500;

        this._noMediaPingTimeout = options.noMediaPingTimeout;

        this._connectMediaRequestedAtLeastOnce = false;
        this._connectMediaCallbacks = [];

        this._controlChannel = options.controlChannel === true;

        this._denoise = options.denoise === true;
        this._agc = options.agc === true;
        this._noiseGate = options.noiseGate === true;

        this._mixAudioMode = options.mixAudioMode || ControlClient.mixAudioMode.NEVER;
        this._mixAudioModeEnabled = [ControlClient.mixAudioMode.ALWAYS].includes(
            this._mixAudioMode
        );

        this._negotiateVideoMode =
            options.negotiateVideoMode || ControlClient.negotiateVideoMode.ALWAYS;
        this._negotiateVideoModeEnabled = [
            ControlClient.negotiateVideoMode.AUTO,
            ControlClient.negotiateVideoMode.ALWAYS,
        ].includes(this._negotiateVideoMode);

        //this will include all remote streams forwarded to this client
        //including streams which are not signalled, but participating in mixed audio
        //Items format:
        //{
        // tapId: "remoteStreamTapId",
        // signalled: <true/false>  - if stream present in SDP or not (e.g. sent as part of mixed stream)
        //}
        this._remoteStreamsForwardingInfo = [];

        this._logger = new LogWrapper(
            this._getPreparedLogger(options.logger),
            () => `cc:${this._seq}-${this._sessionId}${this._peerId ? '-' + this._peerId : ''}`
        );

        this._lastError = undefined;

        this._codec = options.codec;
        this._codecForP2P =
            this._getDevSetting(ControlClient.devSettings.P2P_CODEC) ||
            options.codecForP2P ||
            SDP.codec.H264;
        this._codecForScreenSharing =
            this._getDevSetting(ControlClient.devSettings.SS_CODEC) ||
            options.codecForScreenSharing ||
            SDP.codec.H264;

        this._peerId = undefined;

        this._peerConnectionIceCandidates = [];

        this._closed = false;

        this._meta = options.meta || {};

        this._transportCC = options.transportCC;
        this._blacklistedCodecs = options.blacklistedCodecs;

        this._bitrateMultipliers = options.bitrateMultipliers || {};

        this._recentActivityDelay = options.recentActivityDelay;

        this._p2pMeters = [];

        this._streamsStat = new StreamsStat({
            logger: this._logger,
            sessionId: this._sessionId,
            recentActivityDelay: this._recentActivityDelay,
            getRemoteStreamByTapId: tapId => this._remoteStreams[tapId],
        });

        this._streamsStat.forwardEvents(
            [
                [StreamsStat.event.SFU_STAT_CHANGED, ControlClient.event.SFU_STAT_CHANGED],
                [
                    StreamsStat.event.SFU_STAT_ORDER_CHANGED,
                    ControlClient.event.SFU_STAT_ORDER_CHANGED,
                ],
                [
                    StreamsStat.event.SFU_STAT_VOLUME_CHANGED,
                    ControlClient.event.SFU_STAT_VOLUME_CHANGED,
                ],
                [
                    StreamsStat.event.SFU_STAT_QUALITY_CHANGED,
                    ControlClient.event.SFU_STAT_QUALITY_CHANGED,
                ],
                [
                    StreamsStat.event.RECENTLY_ACTIVE_CHANGED,
                    ControlClient.event.RECENTLY_ACTIVE_CHANGED,
                ],
            ],
            this
        );

        this._streamsStat.on(StreamsStat.event.SFU_STAT_ROTATE_CHANGED, rotateItems => {
            rotateItems.forEach(rotateItem => {
                const tapId = rotateItem.tapId;
                const rotate = rotateItem.rotate;
                this._logger.log(`StreamsStat.event.SFU_STAT_ROTATE_CHANGED ${tapId}`);
                const remoteStream = this._remoteStreams[tapId];
                if (remoteStream) {
                    let streamRotate = this._streamsRotate[remoteStream.id];
                    if (!streamRotate) {
                        streamRotate = new StreamRotate({ stream: remoteStream });
                        this._streamsRotate[remoteStream.id] = streamRotate;
                        const remoteStreamId = this._peerController.getRemoteStreamIdByStream(
                            remoteStream
                        );
                        streamRotate.on(StreamRotate.event.STREAM_UPDATED, stream => {
                            this.emit(
                                ControlClient.event.REMOTE_STREAM_REPLACED,
                                stream,
                                remoteStreamId,
                                tapId,
                                remoteStream,
                                remoteStreamId,
                                false, //isMixedAudio
                                false //isFake stream
                            );
                        });
                    }
                    streamRotate.setRotation(rotate);
                }
            });
        });

        this._streamsStat.on(
            StreamsStat.event.SFU_STAT_LOCAL_STREAMS_DESIRED_QUALITY_CHANGED,
            tapIds => {
                tapIds.forEach(tapId => {
                    const localStream = this._getLocalStreamByTapId(tapId);
                    if (localStream) {
                        const desiredQuality = this._streamsStat.getStatForLocalStream(tapId).video
                            .qualityDesired;
                        localStream.setDesiredQuality({ desiredQuality: desiredQuality });
                    }
                });
            }
        );

        this._server = options.server;

        this._inSession = new InSession({
            sessionId: this._sessionId,
            server: options.server,
            serverPath: options.serverPath,
            serverProtocol: options.serverProtocol,
            serverSubProtocol: options.serverSubProtocol,
            keepAliveInterval:
                typeof options.keepAliveInterval === 'undefined'
                    ? 20000
                    : options.keepAliveInterval,
            operationTimeout: this._operationTimeout,
            logger: this._logger,
            onBeforeSessionCreate: options.onBeforeSessionCreate,
            createWebSocket: options.createWebSocket,
        });

        this._inSession.on(
            InSession.event.SESSION_DESTROYED,
            (normalClose, forcedByServer, signalingMessage, reason) => {
                clearTimeout(this._reportMediaTimer);
                this.emit(ControlClient.event.SESSION_DESTROYED);
                this._logger.logWarn(
                    'Control Session closed: ' +
                        stringifyPlainObj({ normalClose, forcedByServer, reason })
                );
                if (normalClose) {
                    this.close();
                } else {
                    if (forcedByServer) {
                        this.close();
                        this.emit(ControlClient.event.SERVER_ERROR, signalingMessage.toError(true));
                    } else {
                        this._lastError = reason;
                        this._maybeRecovery();
                    }
                }
            }
        );

        this._inSession.on(InSession.event.STATUS_CHANGED, status => {
            this.emit(ControlClient.event.SIGNALING_STATUS_CHANGED, status);
        });

        this._inSession.on(InSession.event.MESSAGE, this._processMessage.bind(this));

        this._recovery = new Recovery({
            sessionId: this._sessionId,
            streamRecoveryTimeout: options.streamRecoveryTimeout,
            logger: this._logger,
        });

        this._recovery.on(Recovery.event.REMOTE_STREAMS_RECOVERY_PENDING, () => {
            this._remoteStreams = {};
        });

        this._recovery.on(Recovery.event.REMOTE_STREAM_REMOVED, (stream, streamId, tapId) => {
            this._removeRemoteStream(stream, streamId, tapId);
        });

        this._recovery.on(Recovery.event.MEDIA_DISCONNECTED, () => {
            this._lastError = 'media disconnected';
            this._maybeRecovery(false);
        });

        this._recovery.forwardEvents(
            [
                [Recovery.event.RECOVERY_STARTED, ControlClient.event.RECOVERY_STARTED],
                [Recovery.event.RECOVERY_COMPLETED, ControlClient.event.RECOVERY_COMPLETED],
                [
                    Recovery.event.REMOTE_STREAM_RECOVERY_STARTED,
                    ControlClient.event.REMOTE_STREAM_RECOVERY_STARTED,
                ],
                [
                    Recovery.event.REMOTE_STREAM_RECOVERY_COMPLETED,
                    ControlClient.event.REMOTE_STREAM_RECOVERY_COMPLETED,
                ],
            ],
            this
        );

        this._preferences = new Preferences({
            maxRemoteVideo: options.maxRemoteVideo,
            maxRemoteAudio: options.maxRemoteAudio,
            logger: this._logger,
            sessionId: this._sessionId,
        });

        this._mediaDeviceList = undefined;

        this._enableComfortNoise = options.enableComfortNoise === true;
        this._comfortNoiseAudioContext = options.comfortNoiseAudioContext;

        if (this._enableComfortNoise) {
            this._comfortNoise = new ComfortNoise({
                audioContext: this._comfortNoiseAudioContext,
            });
        }

        this._channels = [];

        this._initSignalingConnection();

        this._consumers = [];

        this._maxP2PConnections = options.maxP2PConnections || 0;

        this._isP2PSupported = ClientCapabilities.p2p && this._maxP2PConnections > 0;

        this._lastStatUpdate = 0;

        this._sfuConnectionStatus = {
            local: [],
            remote: [],
        };

        this._defaultReportMediaInterval = 10000;
        this._minReportMediaInterval = 2000;
        this._reportMediaInterval = this._defaultReportMediaInterval;
        this._reportMediaTimer = undefined;

        this._eventLoopDelay = new EventLoopDelay({
            interval: options.eventLoopInterval,
            reportInterval: this._defaultReportMediaInterval,
        });
        this._eventLoopDelay.start();

        this._networkQuality = new NetQuality({
            commitSendQualityFunction: (localState, success, fail) => {
                this._sendInfoRequest({
                    type: ControlMessage.infoType.NET_QUALITY,
                    value: localState,
                }).then(success, fail);
            },
        });

        this._serversideNetworkQuality = new ServersideNetworkQuality({
            channel: this.createChannel(
                BasePeerConnection.dataChannelId.NQI,
                BasePeerConnection.dataChannelLabel.NQI
            ),
        });

        this._allowAudioRed =
            options.allowAudioRed === true || this._getDevSetting('allowAudioRed');

        this._serversideNetworkQuality.on(ServersideNetworkQuality.event.NQI_DATA, data => {
            this._logger.logCollapsed(
                JSON.stringify(data),
                ServersideNetworkQuality.event.NQI_DATA
            );
            this._networkQuality.setSendQuality(
                this._serversideNetworkQuality.getLocalNQI() ?? NetQuality.quality.MAX
            );
        });

        this._networkQuality.on(NetQuality.event.SEND_QUALITY_CHANGED, (oldQuality, newQuality) => {
            this.emit(ControlClient.event.LOCAL_NETWORK_QUALITY_CHANGED, {
                oldQuality,
                newQuality,
            });
        });

        this._updateSendQualityTimeout = 1000;
        this._updateSendQualityTimer = setTimeout(
            () => this._updateSendQuality(),
            this._updateSendQualityTimeout
        );
        this._networkQualityBySessionId = {};

        //Placeholder stream used to establish p2p connection
        //in cases when we don't have local stream (video/main)
        //Since all connection logic is per stream
        //we need a stream to connect to another peer
        //We use MediaStream without tracks and
        //tapId="<placeholder>"
        //When first local stream added we simply
        //replace placeholder with the new stream
        //When last local stream deleted
        //We replace it with placeholder back
        //This way we keep p2p connection even if
        //main local stream added or removed
        this._placeholderLocalStream = undefined;

        this._createPlaceholderLocalStream();

        this._e2ee = options.e2ee;

        window.cc = this; //For debug purposes
    }

    _getPreparedLogger(optionsLogger = console) {
        const loggerCandidate =
            this._getDevSetting(ControlClient.devSettings.LOG_TO_CONSOLE) === '1'
                ? console
                : optionsLogger;

        /* eslint-disable no-console */
        return loggerCandidate === console &&
            this._getDevSetting(ControlClient.devSettings.COLLAPSE_LOGS) === '0'
            ? {
                  log: console.log,
                  error: console.error,
                  warn: console.warn,
                  debug: console.debug,
              }
            : loggerCandidate;
        /* eslint-enable no-console */
    }

    preserveVideoTrack(streamTapId) {
        this._preserveVideo.addStream(streamTapId);
    }

    releaseVideoTrack(streamTapId) {
        if (!streamTapId) {
            return;
        }
        const stream = this._preserveVideo.getStream(streamTapId);
        if (stream && this._remoteStreams[streamTapId] === stream) {
            this._removeRemoteStream(stream, stream.id, streamTapId);
        }
        this._preserveVideo.removeStream(streamTapId);
    }

    getPreservedFrameCanvas(streamTapId) {
        return this._preserveVideo.getCanvas(streamTapId);
    }

    _createPlaceholderLocalStream() {
        const placeHolderStream = new MediaStream();
        const streamId = placeHolderStream.id;
        this._placeholderLocalStream = new LocalStream({
            id: LocalStream.id.PLACEHOLDER,
            msid: streamId,
            stream: placeHolderStream,
            useSimulcast: true,
            allowAudioRed: this._allowAudioRed,
            type: LocalStream.type.VIDEO_MAIN,
            codec: this._codec,
            codecForP2P: this._codecForP2P,
            peerController: this._peerController,
            logger: this._logger,
            maxP2PConnections: !this._isP2PSupported ? 0 : this._maxP2PConnections,
            consumers: this._consumers,
        });
        this._placeholderLocalStream.on(LocalStream.event.P2P_RENEGOTIATION_REQUIRED, () =>
            this.connectMedia()
        );
    }

    _onPeerConnectionRemoteStreamsMappingUpdate(pc, streamsMap) {
        if (pc.getRole() === PeersController.role.MAIN) {
            //It could be remote streams from different pcs
            //So, we shouldn't override streams from one pc by streams from
            //another pc
            const oldRemoteStreamsForwardingInfo = this._remoteStreamsForwardingInfo;
            const forwardingInfoFromOtherPCs = oldRemoteStreamsForwardingInfo.filter(
                streamInfo => streamInfo.pc !== pc
            );
            this._remoteStreamsForwardingInfo = streamsMap
                .map(streamInfo => ({
                    tapId: streamInfo.id,
                    signalled:
                        (typeof streamInfo.msid === 'string' && streamInfo.msid.length > 0) ||
                        (Array.isArray(streamInfo.mids) && streamInfo.mids.length > 0),
                    pc: pc,
                }))
                .concat(forwardingInfoFromOtherPCs);

            //find streams which changed their signalling status
            const oldSignalledStateByTapId = {};
            oldRemoteStreamsForwardingInfo.forEach(remoteStreamForwarding => {
                oldSignalledStateByTapId[remoteStreamForwarding.tapId] =
                    remoteStreamForwarding.signalled;
            });

            this._remoteStreamsForwardingInfo.forEach(remoteStreamForwarding => {
                const { signalled, tapId } = remoteStreamForwarding;
                const oldSignalled = oldSignalledStateByTapId[tapId];
                if (oldSignalledStateByTapId[tapId] !== signalled) {
                    const isMixedAudio = tapId.indexOf('mixed') === 0;
                    if (!isMixedAudio) {
                        this._streamsStat.addRemoteStreamToStat(tapId);
                    }
                    this.emit(
                        ControlClient.event.REMOTE_STREAM_SIGNALLING_STATUS_CHANGED,
                        tapId,
                        oldSignalled,
                        signalled
                    );
                }
                delete oldSignalledStateByTapId[tapId];
            });

            //at this moment in oldSignalledStateByTapId will be only streams which were removed
            for (const tapId in oldSignalledStateByTapId) {
                if (oldSignalledStateByTapId.hasOwnProperty(tapId)) {
                    const oldSignalled = oldSignalledStateByTapId[tapId];
                    this._streamsStat.removeRemoteStreamFromStat(tapId);
                    this.emit(
                        ControlClient.event.REMOTE_STREAM_SIGNALLING_STATUS_CHANGED,
                        tapId,
                        oldSignalled,
                        undefined
                    );
                }
            }
        }
    }

    _isRemoteStream(tapId) {
        return !!this._remoteStreamsForwardingInfo.find(
            forwardingInfo => forwardingInfo.tapId === tapId
        );
    }

    //This sets modes we prefer
    setMixAudioMode(mode) {
        assert(
            !!Object.keys(ControlClient.mixAudioMode).find(
                key => ControlClient.mixAudioMode[key] === mode
            ),
            `Unknown mixAudioMode: '${mode}'`
        );
        this._mixAudioMode = mode;
        this._peerController?.setMixAudioMode(mode);
    }

    setDenoise(newValue) {
        // If peer controller doesn't exist we can't apply configuration via setDenoise method.
        // But new `denoise` value is saved to the context, so it will be passed to peer controller when it's time to create it.
        this._denoise = newValue;
        this._peerController?.setDenoise(newValue);
    }

    setAGC(newValue) {
        this._agc = newValue;
        this._peerController?.setAGC(newValue);
    }

    setNoiseGate(newValue) {
        this._noiseGate = newValue;
        this._peerController?.setNoiseGate(newValue);
    }

    setNegotiateVideoMode(mode) {
        assert(
            !!Object.keys(ControlClient.negotiateVideoMode).find(
                key => ControlClient.negotiateVideoMode[key] === mode
            ),
            `Unknown negotiateVideoMode: '${mode}'`
        );
        this._negotiateVideoMode = mode;
        this._peerController?.setNegotiateVideoMode(mode);
    }

    //This returns modes we prefer
    getMixAudioMode() {
        return this._mixAudioMode;
    }

    getNegotiateVideoMode() {
        return this._negotiateVideoMode;
    }

    //This returns modes which are actually in use right now
    getMixAudioModeEnabled() {
        return this._mixAudioModeEnabled;
    }

    getNegotiateVideoModeEnabled() {
        return this._negotiateVideoModeEnabled;
    }

    _getHTMLElementMediaProperties(htmlElement, mediaDevices, kind) {
        const mediaProperties = {
            readyState: htmlElement.readyState,
            paused: htmlElement.paused,
            sinkId: htmlElement.sinkId,
        };

        if (htmlElement.tagName === 'VIDEO') {
            Object.assign(mediaProperties, {
                width: htmlElement.videoWidth,
                height: htmlElement.videoHeight,
            });
        }

        const deviceId = htmlElement.sinkId === '' ? 'default' : htmlElement.sinkId;
        const device =
            mediaDevices &&
            mediaDevices.find(device => device.deviceId === deviceId && device.kind === kind);

        if (device) {
            mediaProperties.deviceLabel = device.label;
        }
        return mediaProperties;
    }

    _maybeUpdateStats() {
        //this function controls that getStats will not be called too often.
        //Because too often calls leads to not complete statistics.
        //Packet lost=0 on very short periods (faster than next receive report)
        if (
            this._peerController &&
            Date.now() - this._lastStatUpdate >= this._maxUpdateStatInterval
        ) {
            this._lastStatUpdate = Date.now();
            return new Promise(resolve => {
                if (this._peerController) {
                    this._peerController.updateStats(resolve);
                } else {
                    resolve();
                }
            });
        } else {
            return Promise.resolve();
        }
    }

    _peerStatToMediaStat(peerStat = {}, mediaStatFormat) {
        const peerStatFieldToMediaStatFiledMap = {
            bytes: 'bytes',
            speed: 'speed',
            lost: 'packetsLost',
            delay: 'currentDelay',
            packets: 'packets',
            jitter: 'jitter',
            fir: 'firs',
            pli: 'plis',
            nack: 'nacks',
            width: 'frameWidth',
            height: 'frameHeight',
            fps: 'frameRateOut',
        };

        const videoStatFields = [
            'bytes',
            'speed',
            'lost',
            'delay',
            'jitter',
            'packets',
            'fir',
            'pli',
            'nack',
            'width',
            'height',
            'fps',
        ];
        const audioStatFields = ['bytes', 'speed', 'lost', 'delay', 'jitter', 'packets'];

        return {
            p2p: peerStat.p2p,
            audio: peerStatForMediaTypeToMediaStatArray(
                peerStat.audio,
                mediaStatFormat,
                peerStatFieldToMediaStatFiledMap,
                audioStatFields
            ),
            video: peerStatForMediaTypeToMediaStatArray(
                peerStat.video,
                mediaStatFormat,
                peerStatFieldToMediaStatFiledMap,
                videoStatFields
            ),
        };

        function peerStatForMediaTypeToMediaStatArray(
            peerStatForMedia = {},
            mediaStatFormat,
            peerStatFieldToMediaStatFiledMap,
            mediaStatFields
        ) {
            const mediaStatArray = [];
            mediaStatFields.forEach(mediaStatFieldName => {
                const peerStatFieldName = peerStatFieldToMediaStatFiledMap[mediaStatFieldName];
                const peerStatFieldValue = peerStatForMedia[peerStatFieldName];
                const mediaStatIndex = mediaStatFormat.indexOf(mediaStatFieldName);
                mediaStatArray[mediaStatIndex] = peerStatFieldValue;
            });
            return mediaStatArray;
        }
    }

    _setTrackStateFieldsInMediaStat({ mediaStat, mediaStatFormat, stream }) {
        const audioTracks = stream.getAudioTracks();
        const videoTracks = stream.getVideoTracks();
        const trackStateFields = ['readyState', 'enabled', 'muted'];

        trackStateFields.forEach(trackStateFieldName => {
            this._setTrackStateFieldInMediaStat({
                mediaStat,
                mediaStatFormat,
                mediaType: 'audio',
                trackStateFieldName,
                tracks: audioTracks,
            });
            this._setTrackStateFieldInMediaStat({
                mediaStat,
                mediaStatFormat,
                mediaType: 'video',
                trackStateFieldName,
                tracks: videoTracks,
            });
        });
        return mediaStat;
    }

    _setTrackStateFieldInMediaStat({
        mediaStat,
        mediaStatFormat,
        mediaType,
        trackStateFieldName,
        tracks,
    }) {
        let trackStateRawValue;
        if (tracks && tracks.length > 0) {
            trackStateRawValue = tracks[0][trackStateFieldName];
        } else {
            return;
        }

        let trackStateMediaStatValue;
        if (trackStateFieldName === 'readyState') {
            switch (trackStateRawValue) {
                case 'live':
                    trackStateMediaStatValue = 1;
                    break;
                case 'ended':
                    trackStateMediaStatValue = 0;
                    break;
                default:
                    trackStateMediaStatValue = 2;
            }
        } else if (['enabled', 'muted'].includes(trackStateFieldName)) {
            trackStateMediaStatValue = trackStateRawValue === true ? 1 : 0;
        } else {
            this._logger.logError(`Unknown trackStateFieldName=${trackStateFieldName}`);
        }
        const fieldIndex = mediaStatFormat.indexOf(trackStateFieldName);
        if (fieldIndex > -1) {
            mediaStat[mediaType][fieldIndex] = trackStateMediaStatValue;
        }
    }

    _removeEmptyArraysInMediaStat(mediaStat) {
        mediaStat = removeEmptyArrayForMediaType(mediaStat, 'audio');
        mediaStat = removeEmptyArrayForMediaType(mediaStat, 'video');
        return mediaStat;

        function removeEmptyArrayForMediaType(mediaStat, mediaType) {
            const statArray = mediaStat[mediaType];
            const isEmpty = !statArray.find(item => typeof item === 'number' && item > 0);
            if (isEmpty) {
                mediaStat[mediaType] = [];
            }
            return mediaStat;
        }
    }

    _getMediaStatForStream({ mediaStatFormat, stream, tapId, htmlElements, kind }) {
        const isLocalStream = !this._isRemoteStream(tapId);
        let peerStat;
        if (isLocalStream) {
            peerStat = this._peerController.getStatForLocalStream(stream.id);
        } else {
            peerStat = this._peerController.getStatForRemoteStream(tapId);
        }

        let mediaStat = this._peerStatToMediaStat(peerStat, mediaStatFormat);
        mediaStat = this._setTrackStateFieldsInMediaStat({ mediaStat, stream, mediaStatFormat });
        mediaStat = this._removeEmptyArraysInMediaStat(mediaStat);
        mediaStat.id = tapId;
        mediaStat.htmlElement = {};
        for (let i = 0; i < htmlElements.length; i++) {
            const htmlElement = htmlElements[i];
            if (htmlElement.srcObject === stream) {
                mediaStat.htmlElement = this._getHTMLElementMediaProperties(
                    htmlElement,
                    this._mediaDeviceList,
                    kind
                );
                break;
            }
        }
        return mediaStat;
    }

    _reportMediaStat() {
        this._maybeUpdateStats().then(() => {
            const mediaStatFormat = [
                'bytes',
                'speed',
                'lost',
                'packets',
                'delay',
                'jitter',
                'readyState',
                'enabled',
                'muted',
                'fir',
                'pli',
                'nack',
                'width',
                'height',
                'fps',
            ];

            const stats = {};
            const remoteStreams = [];
            const localStreams = [];

            if (this._peerController && !this._peerController.isClosed()) {
                const htmlElements = Array.from(document.querySelectorAll('video,audio'));

                for (const tapId in this._remoteStreams) {
                    if (this._remoteStreams.hasOwnProperty(tapId)) {
                        const stream = this._remoteStreams[tapId];
                        const kind = 'audiooutput';
                        const mediaStat = this._getMediaStatForStream({
                            mediaStatFormat,
                            stream,
                            tapId,
                            htmlElements,
                            kind,
                        });
                        remoteStreams.push(mediaStat);
                    }
                }

                this._localStreams.forEach(localStream => {
                    const stream = localStream.getStream();
                    const tapId = localStream.getId();
                    const kind = 'audioinput';
                    const mediaStat = this._getMediaStatForStream({
                        mediaStatFormat,
                        stream,
                        tapId,
                        htmlElements,
                        kind,
                    });
                    localStreams.push(mediaStat);
                });
            }
            stats.format = mediaStatFormat;
            stats.remote = remoteStreams;
            stats.local = localStreams;
            stats.p2p = this._peerController.isAnyP2PPC();
            stats.jsDelay = this._eventLoopDelay.getResults();

            this._inSession
                .request({
                    event: ControlMessage.event.MEDIA_STAT_REQ,
                    body: stats,
                })
                .catch(() => {
                    this._logger.logDebug('Ignore send error for MEDIA_STAT_REQ');
                });
            clearTimeout(this._reportMediaTimer);
            this._reportMediaTimer = setTimeout(
                () => this._reportMediaStat(),
                this._reportMediaInterval
            );
        });
    }

    getPeerController() {
        return this._peerController;
    }

    _getDevSetting(settingName) {
        return localStorage && localStorage[settingName];
    }

    _sendInfoRequest({ type, value }) {
        assert(typeof value === 'object' && value !== null, 'value expected to be object');
        return this._inSession.request({
            event: ControlMessage.event.INFO_REQ,
            body: {
                type,
                value,
            },
        });
    }

    setMediaDevices(mediaDeviceList) {
        this._mediaDeviceList = mediaDeviceList;
        this._maybeSendInfoMediaDevices();
    }

    _maybeSendInfoMediaDevices() {
        if (Array.isArray(this._mediaDeviceList)) {
            this._sendInfoRequest({
                type: ControlMessage.infoType.DEVICES,
                value: this._mediaDeviceList,
            }).catch(err => {
                this._logger.logDebug(`Send devices failed: ${err.message}`);
            });
        }
    }

    _sendNetworkType(networkType) {
        let connectionInfo;
        if (NetworkInformation && navigator.connection instanceof NetworkInformation) {
            connectionInfo = navigator.connection;
        } else {
            connectionInfo = {};
        }
        const { downlink, downlinkMax, effectiveType, rtt, saveData, type } = connectionInfo;

        const networkInfo = {
            downlink,
            downlinkMax,
            effectiveType,
            rtt,
            saveData,
            type: networkType || type,
        };
        this._sendInfoRequest({
            type: ControlMessage.infoType.NET_TYPE,
            value: networkInfo,
        }).catch(err => {
            this._logger.logDebug(`Send network info failed: ${err.message}`);
        });
    }

    registerUserAction({ media, action }) {
        this._sendInfoRequest({
            type: ControlMessage.infoType.USER_ACTION,
            value: {
                action: action,
                media: media,
            },
        }).catch(err => {
            this._logger.logDebug(`Send user action failed: ${err.message}`);
        });
    }

    registerStateUpdate({ media, state }) {
        this._sendInfoRequest({
            type: ControlMessage.infoType.STATE_UPDATE,
            value: {
                state: state,
                media: media,
            },
        }).catch(err => {
            this._logger.logDebug(`Send state update failed: ${err.message}`);
        });
    }

    getRecentlyActiveStreams() {
        return this._streamsStat.getRecentlyActiveStreams();
    }

    getServer() {
        return this._server;
    }

    setSessionToken(newSessionToken) {
        this._sessionToken = newSessionToken;
    }

    getSessionToken() {
        return this._sessionToken;
    }

    setSessionId(newSessionId) {
        this._sessionId = newSessionId;
        this._streamsStat.setSessionId(this._sessionId);
        this._inSession.setSessionId(this._sessionId);
        this._recovery.setSessionId(this._sessionId);
    }

    getSessionId() {
        return this._sessionId;
    }

    setConferenceId(newConferenceId) {
        this._conferenceId = newConferenceId;
    }

    getConferenceId() {
        return this._conferenceId;
    }

    setAutoRecovery(newAutoRecovery) {
        this._autoRecovery = newAutoRecovery;
        if (this._autoRecovery && this._recoveryRequired) {
            this._startRecovery();
        }
    }

    _maybeRecovery(mediaExists) {
        if (mediaExists === false) {
            //If multiple errors occurs we want to mark media as disconnected if at
            //least one of them indicates that. If not - it will leave it undefined (SFU decides)
            this._recovery.setMediaExist(mediaExists);
        }
        if (this._inSession.isDestroyingPreviousSession()) {
            //We don't need to start new recovery again as previous one is still working on destroying old session
            //We don't want to disturb connection just to start connecting again (RCV-30935)
            this._logger.logWarn(
                `Ignoring secondary error "${this._lastError}" since we already recovering (destroying old session).`
            );
            return;
        }
        const crlSession = this._inSession.getCrlSession();
        if (crlSession && crlSession.isConnecting()) {
            //We don't need to start new recovery again as previous one is still connecting websocket
            //We don't want to disturb connection just to start connecting again (RCV-30935)
            this._logger.logWarn(
                `Ignoring secondary error "${this._lastError}" since we already recovering (connecting ws).`
            );
            return;
        }
        this._recoveryRequired = true;
        if (this._autoRecovery && !this._closed) {
            this._startRecovery();
        }
    }

    _startRecovery() {
        this._logger.logWarn('Starting recovery...');
        return this._recovery
            .waitBackOffInterval()
            .then(() => {
                this._recovery.start(this._remoteStreams);
                this._initSignalingConnection(this._recovery.getMediaExist());
            })
            .catch(err => {
                this._logger.logWarn(err.message);
            });
    }

    _initSignalingConnection(mediaExistsOnClient) {
        this._peerId = undefined;
        this._iceServersFromSignalling = null;
        const localPreferences = this._preferences.getLocalState();
        this._logger.log('Init Signaling with media=' + mediaExistsOnClient);
        this._logger.log(`last error ${this._lastError}`);

        this._inSession.initSignalingConnection(
            /* eslint-disable camelcase */
            {
                max_remote_audio: localPreferences.maxRemoteAudio,
                max_remote_video: localPreferences.maxRemoteVideo,
                media_exists: mediaExistsOnClient,
                conference_id: this._conferenceId,
                sdp_semantics: getSdpSemantics(this._options.preferUnifiedPlan),
                token: this._sessionToken,
                meta: {
                    meeting_id: this._meta.shortId,
                    user_agent: navigator.userAgent,
                    username: this._meta.username,
                    x_user_agent: this._meta.xUserAgent,
                    important_reports: this._meta.importantReports,
                    reason: this._lastError,
                },
            } /* eslint-enable camelcase */,
            (err, response) => {
                if (err) {
                    this._logger.logError(err.message);
                    this._lastError = err.message;
                    this._maybeRecovery(mediaExistsOnClient);
                } else if (response.isSuccess()) {
                    const body = response.getBody();
                    if (!Array.isArray(body.ice_servers)) {
                        this._logger.logError('ICE servers are missing in create_resp');
                    }
                    const mediaExistsOnServer = body.media_exists;
                    this._reportMediaInterval =
                        typeof body.media_stat_interval === 'number'
                            ? body.media_stat_interval
                            : this._defaultReportMediaInterval;
                    this._reportMediaInterval = Math.max(
                        this._reportMediaInterval,
                        this._minReportMediaInterval
                    );
                    clearTimeout(this._reportMediaTimer);
                    this._reportMediaTimer = setTimeout(
                        () => this._reportMediaStat(),
                        this._reportMediaInterval
                    );
                    this._iceServersFromSignalling = body.ice_servers;
                    this._recovery.complete(mediaExistsOnServer);
                    if (!mediaExistsOnServer) {
                        this._logger.logDebug("Media doesn't exists on the server.");
                        //We can update initial state to local values
                        //of maxRemoteAudio and maxRemoteVideo
                        //because we sent them in create_req and got ok response
                        //other parameters we have to update using additional update_req
                        this._preferences.reset({
                            maxRemoteAudio: localPreferences.maxRemoteAudio,
                            maxRemoteVideo: localPreferences.maxRemoteVideo,
                        });
                        //In case we re-connected after connection drop
                        //this should send default 0% quality connection
                        //to reset any old quality value we reported or failed to report
                        this._networkQuality.setSendQuality(NetQuality.quality.MIN);

                        this._initPeerConnection();
                    }
                    this._commitPreferences();
                    this._networkQuality.commitSendQuality();
                    if (this._connectMediaRequestedAtLeastOnce) {
                        //Once we connected media we should call connectMedia after each re-connect
                        //We not call it first time to give chance the lib user to add streams first and
                        //then connect, to avoid first empty SDP
                        this.connectMedia();
                    }
                    this._recoveryRequired = false;
                    this._recovery.setMediaExist(undefined);
                    this.emit(ControlClient.event.SIGNALING_CONNECTED); //For backward-compatibility
                    this.emit(ControlClient.event.SESSION_CREATED);
                    this._maybeSendInfoMediaDevices();
                } else {
                    const errMessage = `Create session error: code:${response.getErrCode()} ${response.getErrMessage()}`;
                    this._logger.logError(errMessage);
                    this._lastError = errMessage;
                    this._maybeRecovery(mediaExistsOnClient);
                }
            }
        );
    }

    _initPeerConnection() {
        if (this._peerController) {
            this._peerController.close();
        }

        this._peerController = new PeersController({
            controlChannel: this._controlChannel,
            iceServers: this._iceServersFromSignalling || this._options.iceServers,
            logger: this._logger,
            sessionId: this._sessionId,
            transportCC: this._transportCC,
            blacklistedCodecs: this._blacklistedCodecs,
            codec: this._codec,
            codecForP2P: this._codecForP2P,
            audioRate: this._options.audioRate || '48000',
            audioBitrate: this._options.audioBitrate || '32000',
            audioRateAdditional: this._options.audioRateScreenSharing || '48000',
            audioBitrateAdditional: this._options.audioBitrateScreenSharing || '128000',
            isP2PSupported: this._isP2PSupported,
            preferUnifiedPlan: this._options.preferUnifiedPlan,
            setSendBitrate: this._setSendBitrate,
            noMediaPingTimeout: this._noMediaPingTimeout,
            mixAudioMode: this._mixAudioMode,
            denoise: this._denoise,
            agc: this._agc,
            noiseGate: this._noiseGate,
            negotiateVideoMode: this._negotiateVideoMode,
            bitrateMultipliers: this._bitrateMultipliers,
            enableQ1HalfFps: this._options.enableQ1HalfFps,
            meetingId: this._meta.meetingId,
            e2ee: this._e2ee,
            allowAudioRed: this._allowAudioRed,
            disableDSCP: this._disableDSCP,
        });

        this._peerController.on(PeersController.event.NEGOTIATION_NEEDED, () => {
            // todo: negotiation only for specific PC?
            this.connectMedia();
        });

        this._peerController.on(PeersController.event.LOCAL_CANDIDATE, (pc, candidate) => {
            this._inSession
                .request({
                    event: ControlMessage.event.ICE_REQ,
                    body: {
                        candidate: candidate.candidate,
                        mline_index: candidate.sdpMLineIndex, // eslint-disable-line camelcase
                        mid: candidate.sdpMid,
                        origin: pc.getLocalSDPSessionId(),
                    },
                })
                .catch(() => {
                    this._logger.logDebug('Ignore send error for ICE_REQ');
                });

            this._peerConnectionIceCandidates.push(candidate);
        });

        this._peerController.on(PeersController.event.ICE_STATE_CHANGED, iceState => {
            this._logger.log(`ICE state changed: ${iceState}`);
            this.emit(ControlClient.event.MEDIA_STATUS_CHANGED, iceState);

            if (this._peerController && this._autoRecovery) {
                this._recovery.setMediaState(iceState, this._remoteStreams);
            }
            if (this._comfortNoise) {
                if (
                    iceState === PeersController.iceState.COMPLETED ||
                    iceState === PeersController.iceState.CONNECTED
                ) {
                    this._comfortNoise.start();
                } else {
                    this._comfortNoise.stop();
                }
            }
        });

        this._peerController.on(
            PeersController.event.NO_INCOMING_DATA_TIMEOUT,
            timeSinceLastData => {
                const errMessage = `No incoming data for ${timeSinceLastData}ms. Possible one-way ice connection.`;
                this._logger.logError(errMessage);
                this._lastError = errMessage;
                this._maybeRecovery(false);
            }
        );

        // eslint-disable-next-line no-unused-vars
        this._peerController.on(PeersController.event.CONTROL_CHANNEL_CLOSED_UNEXPECTEDLY, pc => {
            const errMessage = 'Control channel closed unexpectedly.';
            this._logger.logError(errMessage);
            this._lastError = errMessage;
            this._maybeRecovery(false);
        });

        this._peerController.on(PeersController.event.END_OF_LOCAL_CANDIDATES, pc => {
            this._inSession
                .request({
                    event: ControlMessage.event.NO_MORE_ICE_REQ,
                    body: {
                        origin: pc.getLocalSDPSessionId(),
                    },
                })
                .catch(() => {
                    this._logger.logDebug('Ignore send error NO_MORE_ICE_REQ');
                });

            const ipAddresses = extractIceCandidatesIpAddresses(this._peerConnectionIceCandidates);
            // Reset current ice candidates list, because we received the last one.
            this._peerConnectionIceCandidates = [];
            this.emit(ControlClient.event.ICE_CANDIDATES_IP_ADDRESSES_UPDATED, ipAddresses);
        });

        this._peerController.on(
            PeersController.event.REMOTE_STREAM_ADDED,
            (pc, stream, streamId) => {
                const tapId = this._tapIdByStreamId[streamId];
                if (typeof tapId === 'undefined') {
                    this._logger.logError(
                        `Remote stream added with streamId=${streamId}, but tapId not found for the stream`
                    );
                    return;
                }
                this._logger.log('New stream tapId=' + tapId + ', streamId=' + streamId);
                let existingStream;

                const pendingStream = this._recovery.getPendingStream(tapId);
                if (!this._remoteStreams[tapId] && pendingStream) {
                    existingStream = pendingStream;
                }
                if (!existingStream && this._remoteStreams[tapId]) {
                    existingStream = this._remoteStreams[tapId];
                }

                this._remoteStreams[tapId] = stream;

                const isMixedAudio =
                    tapId.indexOf('mixed') === 0 && stream.getVideoTracks().length === 0;

                if (existingStream) {
                    this._preserveVideo.clearStream(tapId);
                    this._logger.log('Old Stream Found');
                    const existingStreamId = this._peerController.getRemoteStreamIdByStream(
                        existingStream
                    );
                    try {
                        this.emit(
                            ControlClient.event.REMOTE_STREAM_REPLACED,
                            stream,
                            streamId,
                            tapId,
                            existingStream,
                            existingStreamId,
                            isMixedAudio,
                            false //isFake stream
                        );
                        this._removeP2PMeter(existingStream);
                        if (pc.isP2P()) {
                            this._addP2PMeter(stream, tapId);
                        }
                    } catch (err) {
                        this._logger.logError(err);
                    }
                    if (existingStreamId !== streamId) {
                        this._deleteTapIdMapping(existingStreamId, tapId);
                    }
                    const pcWithOldStream = this._peerController.getPCByRemoteStream(
                        existingStream
                    );
                    if (pcWithOldStream && pcWithOldStream.isP2P() && pc !== pcWithOldStream) {
                        this._logger.log(
                            'Closing P2P connection, because got the same stream on another connection'
                        );
                        pcWithOldStream.close();
                    }
                    this._recovery.recoverPendingStream(tapId);
                } else {
                    this._logger.log('Old Stream Not Found');

                    try {
                        this.emit(
                            ControlClient.event.REMOTE_STREAM_ADDED,
                            stream,
                            streamId,
                            tapId,
                            isMixedAudio
                        );
                        if (pc.isP2P()) {
                            this._addP2PMeter(stream, tapId);
                        }
                    } catch (err) {
                        this._logger.logError(err);
                    }
                }
            }
        );

        this._peerController.on(
            PeersController.event.REMOTE_STREAM_BEFORE_REMOVE,
            (pc, stream, streamId) => {
                const tapId = this._tapIdByStreamId[streamId];
                if (typeof tapId === 'undefined') {
                    return;
                } else if (this._remoteStreams[tapId] !== stream) {
                    return;
                }
                if (this._preserveVideo.hasStream(tapId)) {
                    this._preserveVideo.saveStream(tapId, stream);
                }
            }
        );

        this._peerController.on(
            PeersController.event.REMOTE_STREAM_REMOVED,
            (pc, stream, streamId) => {
                const tapId = this._tapIdByStreamId[streamId];
                if (typeof tapId === 'undefined') {
                    this._logger.logDebug(
                        `Stream id=${streamId} was already replaced. Ignoring remove`
                    );
                    return;
                } else if (this._remoteStreams[tapId] !== stream) {
                    this._logger.logDebug(
                        `Stream id=${streamId} was already replaced. Just deleting mapping`
                    );
                    this._deleteTapIdMapping(streamId, tapId);
                    return;
                }
                const streamIds = this._streamIdsByTapId[tapId] || [];
                const copies = streamIds.filter(id => id !== streamId);
                this._logger.logDebug(`Copies: ${copies}`);
                let copyStream;
                if (copies.length > 0) {
                    const copyStreamId = copies[0];
                    copyStream = this._peerController.getRemoteStreamByStreamId(copyStreamId);
                }
                this._logger.logDebug(`Copy stream: ${copyStream}`);
                const isMixedAudio =
                    tapId.indexOf('mixed') === 0 && stream.getVideoTracks().length === 0;
                if (copyStream) {
                    const copyPC = this._peerController.getPCByRemoteStream(copyStream);
                    const copyStreamId = this._peerController.getRemoteStreamIdByStream(copyStream);
                    this._remoteStreams[tapId] = copyStream;
                    this.emit(
                        ControlClient.event.REMOTE_STREAM_REPLACED,
                        copyStream,
                        copyStreamId,
                        tapId,
                        stream,
                        streamId,
                        isMixedAudio,
                        false //isFake stream
                    );
                    this._removeP2PMeter(stream);
                    if (copyPC.isP2P()) {
                        this._addP2PMeter(copyStream, tapId);
                    }
                } else {
                    const preservedStream = this._preserveVideo.getStream(tapId);
                    if (preservedStream) {
                        this._remoteStreams[tapId] = preservedStream;
                        const preservedStreamId = preservedStream.id;
                        this._addTapIdMapping(preservedStreamId, tapId);
                        this._logger.log(
                            `Removed stream tapId=${tapId}, streamId=${streamId} replaced with preserved streamId=${preservedStreamId}`
                        );
                        this.emit(
                            ControlClient.event.REMOTE_STREAM_REPLACED,
                            preservedStream,
                            preservedStreamId,
                            tapId,
                            stream,
                            streamId,
                            isMixedAudio,
                            true //isFake stream
                        );
                        this._removeP2PMeter(stream);
                    } else {
                        this._logger.log(
                            'Removed stream tapId=' + tapId + ', streamId=' + streamId
                        );
                        this._removeRemoteStream(stream, streamId, tapId);
                    }
                }
            }
        );

        this._peerController.on(PeersController.event.SFU_STAT_RECEIVED, (pc, stat) => {
            this._updateSFUStat(stat);
        });

        this._peerController.on(PeersController.event.CONSUMERS_UPDATED, consumers => {
            this._consumers = consumers;
            this._localStreams.forEach(localStream => localStream.setConsumers(consumers));
            if (this._placeholderLocalStream) {
                this._placeholderLocalStream.setConsumers(consumers);
            }
        });

        this._peerController.on(PeersController.event.BEFORE_CLOSE, pc => {
            this._onPeerConnectionRemoteStreamsMappingUpdate(pc, []);
        });

        this._peerController.on(
            PeersController.event.NEGOTIATE_VIDEO_MODE_STATE_CHANGED,
            negotiateVideoModeEnabled => {
                if (this._negotiateVideoModeEnabled !== negotiateVideoModeEnabled) {
                    this._negotiateVideoModeEnabled = negotiateVideoModeEnabled;
                    this.emit(
                        ControlClient.event.NEGOTIATE_VIDEO_MODE_STATE_CHANGED,
                        this._negotiateVideoModeEnabled
                    );
                }
            }
        );

        this._peerController.on(
            PeersController.event.MIX_AUDIO_MODE_STATE_CHANGED,
            mixAudioModeEnabled => {
                if (this._mixAudioModeEnabled !== mixAudioModeEnabled) {
                    this._mixAudioModeEnabled = mixAudioModeEnabled;
                    this.emit(
                        ControlClient.event.MIX_AUDIO_MODE_STATE_CHANGED,
                        this._mixAudioModeEnabled
                    );
                }
            }
        );

        this._peerController.on(PeersController.event.NETWORK_TYPE_CHANGED, networkType => {
            this._sendNetworkType(networkType);
        });

        this._logger.logDebug(
            'Copying local streams(' + this._localStreams.length + ') to the new peer connection...'
        );
        this._localStreams.forEach(localStream => {
            this._logger.logDebug('Local stream: ' + localStream.getId());
            localStream.setPeerController(this._peerController);
        });
        if (this._placeholderLocalStream) {
            this._placeholderLocalStream.setPeerController(this._peerController);
        }
        this._channels.forEach(channel => channel.setPeersController(this._peerController));

        this.emit(ControlClient.event.MEDIA_STATUS_CHANGED, this.getMediaStatus());
    }

    _addP2PMeter(stream, tapId) {
        const existingMeter = this._p2pMeters.find(meter => meter.getStreamTapId() === tapId);
        if (existingMeter) {
            this._removeP2PMeter(existingMeter.getStream());
        }
        const meter = new P2PStatMeter({
            stream: stream,
            streamTapId: tapId,
            audioContext: this._comfortNoiseAudioContext,
        });
        this._p2pMeters.push(meter);
        if (this._p2pMeters.length === 1) {
            clearInterval(this._localStatTimer);
            this._localStatTimer = setInterval(() => {
                const localStreamsStat = [];
                const remoteStreamsStat = [];

                this._p2pMeters.forEach(meter => {
                    const streamStat = meter.getStat();
                    const streamTapId = streamStat.id;
                    const isRemote = typeof this._remoteStreams[streamTapId] !== 'undefined';
                    if (isRemote) {
                        remoteStreamsStat.push(streamStat);
                    } else {
                        localStreamsStat.push(streamStat);
                    }
                });

                this._streamsStat.updateLocalStat(remoteStreamsStat, localStreamsStat);
            }, 1000);
        }
    }

    _removeP2PMeter(stream) {
        const indexOfMeter = this._p2pMeters.findIndex(meter => meter.getStream() === stream);
        if (indexOfMeter > -1) {
            this._p2pMeters.splice(indexOfMeter, 1);
        }
        if (this._p2pMeters.length < 1) {
            clearInterval(this._localStatTimer);
        }
    }

    _addTapIdMapping(streamId, tapId) {
        this._tapIdByStreamId[streamId] = tapId;
        this._streamIdsByTapId[tapId] = this._streamIdsByTapId[tapId] || [];
        if (this._streamIdsByTapId[tapId].indexOf(streamId) < 0) {
            this._streamIdsByTapId[tapId].push(streamId);
            this._logger.log(`${tapId} map:${this._streamIdsByTapId[tapId].toString()}`);
        }
    }

    _deleteTapIdMapping(streamId, tapId) {
        this._logger.logDebug(`_deleteTapIdMapping ${streamId} ${tapId}`);
        if (this._tapIdByStreamId[streamId] === tapId) {
            //Delete only if it has expected value (not overridden by another stream)
            delete this._tapIdByStreamId[streamId];
        }
        if (this._streamIdsByTapId[tapId]) {
            this._streamIdsByTapId[tapId] = this._streamIdsByTapId[tapId].filter(
                id => id !== streamId
            );
            this._logger.log(`${tapId} map:${this._streamIdsByTapId[tapId].toString()}`);
            if (this._streamIdsByTapId[tapId].length === 0) {
                delete this._streamIdsByTapId[tapId];
            }
        }
    }

    _updateSFUStat(remoteStreamsStat, localStreamsStat) {
        this._streamsStat.updateSFUStat(remoteStreamsStat, localStreamsStat);
    }

    _removeAllRemoteStreams() {
        for (const tapId in this._remoteStreams) {
            if (this._remoteStreams.hasOwnProperty(tapId)) {
                const stream = this._remoteStreams[tapId];
                const streamId = this._peerController.getRemoteStreamIdByStream(stream);
                this._removeRemoteStream(stream, streamId, tapId);
            }
        }
        this._remoteStreams = {};
    }

    updateRemoteStreamsPreferences(streamsPreferences, callback) {
        this._preferences.updateRemoteStreamsPreferences(streamsPreferences);
        this._commitPreferences(callback);
    }

    /**
     * @param {String[]|{id: String, quality: {min:Number, max:Number}}[]} streams
     * @param {function} callback
     */
    setPinnedStreams(streams, callback) {
        this._preferences.setPinnedStreams(streams);
        this._commitPreferences(callback);
    }

    _commitPreferences(callback) {
        this._inSession.execute(err => {
            if (err) {
                return callback && callback(err);
            }
            this._preferences.commit((diff, success, fail) => {
                if (diff === null) {
                    success();
                    callback && callback(null);
                } else {
                    this._inSession.request(
                        {
                            event: ControlMessage.event.UPDATE_REQ,
                            /* eslint-disable camelcase */
                            body: {
                                remote_streams: diff.streamsPreferences,
                                max_remote_video: diff.maxRemoteVideo,
                                max_remote_audio: diff.maxRemoteAudio,
                            } /* eslint-enable camelcase */,
                        },
                        (err, response) => {
                            err = err || response.toError();
                            if (err) {
                                fail();
                            } else {
                                success();
                            }
                            callback && callback(err);
                        }
                    );
                }
            });
        });
    }

    close(reason = ControlClient.closeReason.DEFAULT) {
        this._closed = true;
        this._eventLoopDelay.stop();
        clearTimeout(this._reportMediaTimer);
        clearTimeout(this._updateSendQualityTimer);
        this._inSession.close(reason);
        this._recovery.removeAllPendingStreams();
        this._remoteStreamsForwardingInfo = [];
        this._removeAllRemoteStreams();
        this.removeAllLocalStreams();
        if (this._placeholderLocalStream) {
            this._placeholderLocalStream.destroy();
            delete this._placeholderLocalStream;
        }
        if (this._peerController) {
            this._peerController.close();
            this._peerController = undefined;
        }
        this._connectMediaCallbacks = [];
        this._peerId = undefined;
        this._recoveryRequired = false;
        delete this._networkQualityBySessionId;
        if (this._e2ee) {
            const mlsSdkClient = this._e2ee.getMlsSdkClient();
            if (mlsSdkClient) {
                mlsSdkClient.disconnect();
            }
        }
    }

    setNetworkConditions(conditions) {
        return this._inSession.request({
            event: ControlMessage.event.SET_NETWORK_CONDITIONS_REQ,
            body: conditions,
        });
    }

    setPreferences(options, callback) {
        this._preferences.setSlotsPreferences(options);

        const { pinnedStreams } = options;
        if (pinnedStreams) {
            this._preferences.setPinnedStreams(pinnedStreams);
        }

        this._logger.logDebug('setPreferences ' + JSON.stringify(options));
        this._commitPreferences(callback);
    }

    _processMessage(message) {
        const event = message.getEvent();
        const et = ControlMessage.event;
        const type = message.getType() || ControlMessage.type.SESSION;

        if (type === ControlMessage.type.SESSION) {
            switch (event) {
                case et.UPDATE_REQ:
                    this._onUpdateSessionReq(message);
                    break;
                case et.UPDATE_RES:
                    //should be processed in callback
                    break;
                case et.PSTN_AS_REPORT_REQ:
                    // TODO do nothing for now to avoid error about unknown event and remove it when event 'pstn_as_report_req' will be removed from SFU.
                    break;
                case et.ICE_REQ:
                    this._onRemoteICE(message);
                    break;
                case et.NO_MORE_ICE_REQ:
                    this._inSession.respondOK(message);
                    break;
                case et.STATUS_REQ:
                    this._recovery.setSessionStatus(message.getBody(), this._remoteStreams);
                    this._inSession.respondOK(message);
                    break;
                case et.ICE_RES:
                case et.NO_MORE_ICE_RES:
                case et.UPDATE_ACK:
                case et.SET_NETWORK_CONDITIONS_RES:
                case et.UNBIND_RES:
                    if (!message.isSuccess()) {
                        this._logger.logError(message.getErrMessage());
                    }
                    break;
                case et.ACTIVE_SPEAKERS_REPORT_REQ:
                    this._onActiveSpeakersReportReq(message);
                    break;
                case et.NETWORK_STATUS_REQ:
                    this._onNetworkStatusReq(message);
                    break;
                default:
                    throw new Error("Unknown event for session '" + event + "'");
            }
        } else if (type === ControlMessage.type.CONFERENCE) {
            switch (event) {
                case et.UPDATE_PARTICIPANT_RES:
                case et.DESTROY_PARTICIPANT_REQ:
                case et.UPDATE_STREAM_RES:
                case et.DESTROY_STREAM_RES:
                case et.UPDATE_CHANNEL_RES:
                case et.DESTROY_CHANNEL_RES:
                case et.UPDATE_RECORDING_RES:
                case et.DESTROY_RECORDING_RES:
                    if (!message.isSuccess()) {
                        this._logger.logError(message.getErrMessage());
                    }
                    break;
                default:
                    throw new Error("Unknown event for conference '" + event + "'");
            }
        } else {
            throw new Error("Unsupported message type '" + type + "'");
        }
    }

    _onNetworkStatusReq(request) {
        try {
            const body = request.getBody();
            const update = [];
            for (const groupName in body) {
                if (body.hasOwnProperty(groupName)) {
                    const match = groupName.match(/^s(\d{1,3})/);
                    if (match) {
                        const quality = Number(match[1]);
                        const sessionIds = body[groupName];
                        if (Array.isArray(sessionIds)) {
                            sessionIds.forEach(sessionId => {
                                if (sessionId !== this._sessionId) {
                                    const prevQuality =
                                        typeof this._networkQualityBySessionId[sessionId] ===
                                        'number'
                                            ? this._networkQualityBySessionId[sessionId]
                                            : NetQuality.quality.MAX;
                                    if (prevQuality !== quality) {
                                        this._networkQualityBySessionId[sessionId] = quality;
                                        update.push({
                                            sessionId,
                                            oldQuality: prevQuality,
                                            newQuality: quality,
                                        });
                                    }
                                    if (quality === NetQuality.quality.MAX) {
                                        delete this._networkQualityBySessionId[sessionId];
                                    }
                                }
                            });
                        } else {
                            throw new Error("'sXX' expected to be an array");
                        }
                    }
                }
            }
            if (update.length > 0) {
                this.emit(ControlClient.event.REMOTE_NETWORK_QUALITY_CHANGED, update);
            }
        } catch (e) {
            this._logger.logError(e.stack);
            this._inSession.respondError(request, ControlMessage.errCode.BAD_ARGUMENTS, e.message);
            return;
        }
        this._inSession.respondOK(request);
    }

    _onActiveSpeakersReportReq(request) {
        try {
            const body = request.getBody();
            this._updateSFUStat(body.remote_streams, body.local_streams);
            if (body.cli_send_connections || body.cli_recv_connections) {
                this._sfuConnectionStatus.local =
                    body.cli_send_connections || this._sfuConnectionStatus.local;
                this._sfuConnectionStatus.remote =
                    body.cli_recv_connections || this._sfuConnectionStatus.remote;
            }
        } catch (e) {
            this._logger.logError(e.stack);
            this._inSession.respondError(request, ControlMessage.errCode.BAD_ARGUMENTS, e.message);
            return;
        }
        this._inSession.respondOK(request);
    }

    addLocalStream(options) {
        const tapId = options.tapId;
        const stream = options.stream;
        const useSimulcast = options.useSimulcast || false;
        const type = options.type || ControlClient.streamType.VIDEO_MAIN;

        assert(typeof stream === 'object' && stream !== null && typeof stream.id === 'string');
        assert(typeof useSimulcast === 'boolean');
        assert(
            !this._localStreams.find(localStream => localStream.getId() === tapId),
            `Local stream with tapId=${tapId} already exist`
        );

        const streamId = stream.id;
        if (this._pendingStreamIdsToDelete[streamId]) {
            this._logger.logDebug(
                `Found streamId=${streamId} is in pending delete state. Will clear timer.`
            );
            clearTimeout(this._pendingStreamIdsToDelete[streamId]);
            delete this._pendingStreamIdsToDelete[streamId];
        }

        this._addTapIdMapping(streamId, tapId);

        const isScreenSharing = type === LocalStream.type.VIDEO_SCREENSHARING;

        let localStream;
        if (type === ControlClient.streamType.VIDEO_MAIN && this._placeholderLocalStream) {
            localStream = this._placeholderLocalStream;
            localStream.setId(tapId); //Important to set before calling setStream
            localStream.setUseSimulcast(useSimulcast); //Important to set before calling setStream
            localStream.setStream(stream);
            delete this._placeholderLocalStream;
        } else {
            localStream = new LocalStream({
                id: tapId,
                msid: streamId,
                stream: stream,
                useSimulcast: useSimulcast,
                allowAudioRed: this._allowAudioRed,
                type: type,
                codec: isScreenSharing ? this._codecForScreenSharing : this._codec, //We override codec for screen sharing, because H264 can produce 30FPS
                codecForP2P: this._codecForP2P,
                peerController: this._peerController,
                logger: this._logger,
                maxP2PConnections:
                    !this._isP2PSupported || isScreenSharing ? 0 : this._maxP2PConnections,
                consumers: this._consumers,
            });
            localStream.on(LocalStream.event.P2P_RENEGOTIATION_REQUIRED, () => this.connectMedia());

            localStream.on(LocalStream.event.MODE_CHANGED, mode => {
                //Stream object maybe already changed, so we should take current
                const stream = localStream.getStream();
                if (mode === LocalStream.mode.P2P) {
                    this._addP2PMeter(stream, tapId);
                } else {
                    this._removeP2PMeter(stream);
                }
            });

            if (localStream.getMode() === LocalStream.mode.P2P) {
                this._addP2PMeter(stream, tapId);
            }
        }
        this._localStreams.push(localStream);

        //We need to make sure that we will send p2p only first main stream
        //While it is unusual and impossible on WebClient, it is possible on matrix
        //And maybe used in future to send multiple streams from different cameras
        //Currently p2p supports only one stream, because no indication which stream is sent in P2P
        const mainStreams = this._localStreams.filter(
            localStream => localStream.getType() === LocalStream.type.VIDEO_MAIN
        );
        mainStreams.forEach((mainStream, index) => {
            mainStream.setMaxP2PConnections(
                !this._isP2PSupported || index > 0 ? 0 : this._maxP2PConnections
            );
        });

        this.emit(
            ControlClient.event.LOCAL_STREAM_ADDED,
            stream,
            streamId,
            tapId,
            this._localStreams.length - 1
        );

        this._streamsStat.addLocalStreamToStat(tapId);
    }

    _getLocalStreamByTapId(tapId) {
        return this._localStreams.find(localStream => localStream.getId() === tapId);
    }

    /**
     * @param {LocalStreamTypes.VIDEO_MAIN|LocalStreamTypes.VIDEO_SCREENSHARING} type - type of the local stream
     * @private
     * @return {LocalStream} LocalStream
     */
    _getLocalStreamByType(type) {
        return this._localStreams.find(stream => stream.getType() === type);
    }

    replaceLocalStream(options) {
        const { tapId, stream } = options;
        const localStream = this._getLocalStreamByTapId(tapId);
        if (localStream) {
            const oldStream = localStream.getStream();
            localStream.setStream(stream);
            this._deleteTapIdMapping(oldStream.id, tapId);
            this._addTapIdMapping(stream.id, tapId);
            this.emit(
                ControlClient.event.LOCAL_STREAM_REPLACED,
                stream,
                stream.id,
                tapId,
                this._localStreams.length - 1
            );
            if (localStream.getMode() === LocalStream.mode.P2P) {
                this._removeP2PMeter(oldStream);
                this._addP2PMeter(stream, tapId);
            }
        } else {
            this.addLocalStream(options);
        }
    }

    removeLocalStreamTrack(options) {
        const { tapId, track } = options;

        const stream = this._getLocalStreamByTapId(tapId) || {};
        const mediaStream = stream._stream;

        if (track.kind === 'video') {
            this._onMediaConnected = null;
        }

        if (mediaStream) {
            mediaStream.removeTrack(track);
            if (this._peerController) {
                this._peerController.removeLocalStreamTrackSenders(mediaStream, track);
            }
        }
    }

    addLocalStreamTrack(options) {
        const { tapId, track } = options;

        const stream = this._getLocalStreamByTapId(tapId) || {};
        const mediaStream = stream._stream;

        if (track.kind === 'video') {
            this._onMediaConnected = null;
        }

        if (mediaStream) {
            mediaStream.addTrack(track);
            this.replaceLocalStream({ tapId, stream: mediaStream });

            if (track.kind === 'video') {
                stream.setDesiredQuality({ force: true });
                // if media is not connected setDesiredQuality will not apply due to empty codecs in pc
                this._onMediaConnected = () => {
                    stream.setDesiredQuality({ force: true });
                };
            }
        }
    }

    _updateSendQuality() {
        clearTimeout(this._updateSendQualityTimer);
        return this._maybeUpdateStats().then(() => {
            if (this.getPeerController()?.isMainConnectionEstablished()) {
                const localNQIFromServer = this._serversideNetworkQuality.getLocalNQI();

                const qualities = this._localStreams
                    .map(localStream => localStream.getSendQuality())
                    .filter(Number.isInteger);

                const newQuality = Number.isInteger(localNQIFromServer)
                    ? localNQIFromServer
                    : NetQuality.getAggregatedQuality(qualities);

                this._networkQuality.setSendQuality(newQuality);
                this._networkQuality.commitSendQuality();
            } else {
                this._networkQuality.setSendQuality(NetQuality.quality.MIN);
            }

            this._updateSendQualityTimer = setTimeout(
                () => this._updateSendQuality(),
                this._updateSendQualityTimeout
            );
        });
    }

    getLocalNetworkQuality() {
        return this._networkQuality.getSendQuality();
    }

    getNQIUpstreamDetails() {
        return this.getPeerController()?.isMainConnectionEstablished()
            ? this._serversideNetworkQuality.getLocalNQIUpstreamDetails()
            : {};
    }

    getNQIDownstreamDetails() {
        return this.getPeerController()?.isMainConnectionEstablished()
            ? this._serversideNetworkQuality.getLocalNQIDownstreamDetails()
            : {};
    }

    getRemoteNetworkQuality(sessionId) {
        return typeof this._networkQualityBySessionId[sessionId] === 'number'
            ? this._networkQualityBySessionId[sessionId]
            : NetQuality.quality.MAX;
    }

    getLocalStreams() {
        return this._localStreams.map(localStream => {
            return {
                tapId: localStream.getId(),
                stream: localStream.getStream(),
            };
        });
    }

    getLocalStreamsEx() {
        return this._localStreams;
    }

    removeLocalStream(options) {
        const tapId = options.tapId;
        let stream = options.stream;
        if (typeof stream === 'undefined') {
            const localStream = this._localStreams.find(
                localStream => localStream.getId() === tapId
            );
            if (localStream) {
                stream = localStream.getStream();
            } else {
                return;
            }
        }

        assert(
            typeof stream === 'object' &&
                stream !== null &&
                typeof stream.id === 'string' &&
                typeof tapId !== 'undefined'
        );
        const streamId = stream.id;
        this._removeP2PMeter(stream);

        //Will delete stream mapping with delay, to make sure all
        //delayed events will have access to mapping
        //At the same time stream with the same id could be added again
        //So, we are saving reference to timer, to stop it in this case.
        clearTimeout(this._pendingStreamIdsToDelete[streamId]);
        this._pendingStreamIdsToDelete[streamId] = setTimeout(() => {
            delete this._pendingStreamIdsToDelete[streamId];
            this._deleteTapIdMapping(streamId, tapId);
        }, 5000);

        this._streamsStat.removeLocalStreamFromStat(tapId);
        const localStreamIndex = this._localStreams.findIndex(
            localStream => localStream.getId() === tapId
        );
        if (localStreamIndex > -1) {
            const localStream = this._localStreams.splice(localStreamIndex, 1)[0];

            const isLastMainStream =
                localStream.getType() === LocalStream.type.VIDEO_MAIN &&
                !this._localStreams.find(
                    stream => stream.getType() === LocalStream.type.VIDEO_MAIN
                );
            if (!this._closed && isLastMainStream) {
                const placeHolderStream = new MediaStream();
                localStream.setId(LocalStream.id.PLACEHOLDER); //Important to set before calling setStream
                localStream.setStream(placeHolderStream);
                this._placeholderLocalStream = localStream;
            } else {
                localStream.destroy();
            }

            this.emit(
                ControlClient.event.LOCAL_STREAM_REMOVED,
                stream,
                streamId,
                tapId,
                localStreamIndex
            );
        }
    }

    removeAllLocalStreams() {
        this._localStreams
            .map(localStream => ({
                tapId: localStream.getId(),
                stream: localStream.getStream(),
            }))
            .forEach(localStream => this.removeLocalStream(localStream));
    }

    _getStreamsMap(localDescription, pc, localStreamsTypes) {
        const streamsMap = [];
        if (!pc.supportsMediaStreams()) return streamsMap;
        const isUnifiedPlan = getSdpSemantics(this._options.preferUnifiedPlan) === 'unified-plan';
        if (pc.isTransceiversUsed() || isUnifiedPlan) {
            const streamsMIDs = isUnifiedPlan
                ? getMsidToMidMapping(new SDP(localDescription))
                : pc.getStreamsMIDs(localDescription);
            for (const msid in streamsMIDs) {
                if (!streamsMIDs.hasOwnProperty(msid)) continue;
                const tapId = this._tapIdByStreamId[msid];
                if (!tapId) {
                    this._logger.logError(
                        `Stream msid:${msid} is not present in TAP streams but is in local SDP`
                    );
                    continue;
                }
                streamsMap.push({
                    id: tapId,
                    msid: msid,
                    type: localStreamsTypes[tapId],
                    mids: streamsMIDs[msid],
                });
            }
        } else {
            const match = localDescription.match(/msid:\s*[^\s]+/g);
            const duplicates = {};
            if (match) {
                match.forEach(msidString => {
                    const msid = msidString.substr(5).trim();
                    if (duplicates[msid]) {
                        return;
                    }
                    duplicates[msid] = true;
                    const tapId = this._tapIdByStreamId[msid];
                    streamsMap.push({
                        id: tapId,
                        msid: msid,
                        type: localStreamsTypes[tapId],
                    });
                });
            }
        }
        return streamsMap;
    }

    connectMedia(callback) {
        this._connectMediaRequestedAtLeastOnce = true;
        if (typeof callback === 'function') {
            this._connectMediaCallbacks.push(callback);
        }
        if (
            !this._inSession.isConnected() ||
            !this._peerController ||
            this._peerController.isClosed()
        ) {
            return;
        }
        const callbacks = this._connectMediaCallbacks;
        this._connectMediaCallbacks = [];

        const connectionInstanceId = this._inSession.getInstanceId();

        this._peerController.initMainPC();

        this._peerController
            .forEachPC((pc, index) => {
                let request;
                let response;
                let isAlreadyDelivered = false;

                this._logger.log(`PC${index}: Will create offer`);

                let localDescriptionSDP;
                //We snapshot current types, because they could change
                //during createOffer (e.g. stream removed)
                let localStreamsTypes;
                return Promise.resolve()
                    .then(() => {
                        localStreamsTypes = this._getSnapshotOfLocalStreamTypes();
                        return pc.createOffer();
                    })
                    .then(description => {
                        localDescriptionSDP = description.sdp;
                        if (pc.isSDPDelivered(localDescriptionSDP)) {
                            isAlreadyDelivered = true;
                            pc.cancelPendingLocalDescription();
                            this._logger.log(`PC${index}: no changes detected.`);
                            throw new Error('Already delivered');
                        }
                        this._logger.log(`PC${index}: createOffer got description`);
                        this._logger.log(`PC${index}: will send UPDATE_REQ`);
                        request = {
                            event: ControlMessage.event.UPDATE_REQ,
                            body: {
                                sdp: localDescriptionSDP,
                                streams: this._getStreamsMap(
                                    description.sdp,
                                    pc,
                                    localStreamsTypes
                                ),
                            },
                            connection: connectionInstanceId,
                        };
                        return this._inSession.request(request).catch(err => {
                            this._logger.logError(`Failed to send local SDP: ${err.message}`);
                            return Promise.reject(err);
                        });
                    })
                    .then(res => {
                        response = res;
                        if (response.isSuccess()) {
                            pc.confirmSDPDelivered(localDescriptionSDP);
                            const body = response.getBody();
                            const responseStreams = body.streams || [];
                            let signalledStreamsCount = 0;
                            this._onPeerConnectionRemoteStreamsMappingUpdate(pc, responseStreams);
                            responseStreams.forEach(streamInfo => {
                                if (streamInfo.msid) {
                                    this._addTapIdMapping(streamInfo.msid, streamInfo.id);
                                    signalledStreamsCount++;
                                }
                            });
                            const remoteSDP = body.sdp;

                            if (remoteSDP) {
                                this._logger.logCollapsed(
                                    SDP.prettyPrint(remoteSDP),
                                    'Remote answer SDP'
                                );

                                const parsedSDP = new SDP(remoteSDP);
                                if (!this._peerId) {
                                    this._peerId = parsedSDP.getSession().origin.username;
                                    try {
                                        this.emit(ControlClient.event.GOT_IDENTITY, this._peerId);
                                    } catch (err) {
                                        this._logger.logError(err);
                                    }
                                }
                                if (pc.isP2P()) {
                                    const remoteSDPSessionId = parsedSDP.getSession().origin
                                        .sessionId;
                                    const localSDPSessionId = parsedSDP.getFirstAttribute(
                                        'rcv-remote-session'
                                    );
                                    const pcRemoteSDPSessionId = pc.getRemoteSDPSessionId();
                                    const pcLocalSDPSessionId = pc.getLocalSDPSessionId();
                                    if (
                                        pcRemoteSDPSessionId &&
                                        remoteSDPSessionId !== pcRemoteSDPSessionId
                                    ) {
                                        const err = new Error(
                                            `Unexpected sdp session id (o=) in answer: ${remoteSDPSessionId} instead of ${pcRemoteSDPSessionId}`
                                        );
                                        this._logger.logError(
                                            'Failed to set remote description: ' + err
                                        );
                                        this._inSession.respondACKError(
                                            response,
                                            ControlMessage.errCode.CANNOT_SET_REMOTE_DESCRIPTION,
                                            err.message
                                        );
                                        pc.close();
                                        return;
                                    } else if (
                                        localSDPSessionId &&
                                        localSDPSessionId !== pcLocalSDPSessionId
                                    ) {
                                        const err = new Error(
                                            `Unexpected sdp session id (a=rcv-remote-session) in answer: ${localSDPSessionId} instead of ${pcLocalSDPSessionId}`
                                        );
                                        this._logger.logError(
                                            'Failed to set remote description: ' + err
                                        );
                                        this._inSession.respondACKError(
                                            response,
                                            ControlMessage.errCode.CANNOT_SET_REMOTE_DESCRIPTION,
                                            err.message
                                        );
                                        pc.close();
                                        return;
                                    }
                                }

                                if (
                                    !this._peerController.canPCExistWithoutStreams(pc) &&
                                    signalledStreamsCount === 0 &&
                                    request.body.streams.length === 0
                                ) {
                                    if (
                                        this._localStreams.length === 0 &&
                                        this._peerController.getAllPCs().length === 1
                                    ) {
                                        this._resetPeerConnectionForNoStreamsCase();
                                    } else if (pc.getLocalStreams().length === 0) {
                                        this._logger.log(
                                            'Closing pc, because no streams in SDP and in pc'
                                        );
                                        this._logger.log(
                                            `pc: p2p=${pc.isP2P()} role=${pc.getRole()}`
                                        );
                                        pc.close();
                                    } else {
                                        this._logger.log(
                                            'No streams in SDP, but cannot reset peersController or close pc'
                                        );
                                        this._logger.log(
                                            `Local streams count=${
                                                this._localStreams.length
                                            }, PCs count=${
                                                this._peerController.getAllPCs().length
                                            }, pc has ${pc.getLocalStreams().length} streams`
                                        );
                                        this._logger.log(
                                            `pc: p2p=${pc.isP2P()} role=${pc.getRole()}`
                                        );
                                    }
                                    this._inSession.respondACKOK(response);
                                } else {
                                    const remoteDescription = new RTCSessionDescription({
                                        sdp: remoteSDP,
                                        type: 'answer',
                                    });
                                    if (!pc.isClosed()) {
                                        return pc
                                            .setRemoteDescription(
                                                remoteDescription,
                                                responseStreams
                                            )
                                            .then(() => {
                                                this._logger.log('Remote description set');
                                                this._inSession.respondACKOK(response);
                                            })
                                            .catch(err => {
                                                this._logger.logError(
                                                    'Failed Description(remote):'
                                                );
                                                this._logger.logError(
                                                    SDP.prettyPrint(remoteDescription.sdp)
                                                );
                                                this._inSession.respondACKError(
                                                    response,
                                                    ControlMessage.errCode
                                                        .CANNOT_SET_REMOTE_DESCRIPTION,
                                                    err.message
                                                );
                                                this._logger.logError(
                                                    'Failed to set remote description: ' + err
                                                );
                                                return Promise.reject(err);
                                            });
                                    } else {
                                        this._inSession.respondACKError(
                                            response,
                                            ControlMessage.errCode.CANNOT_SET_REMOTE_DESCRIPTION,
                                            "Peer connection doesn't exists"
                                        );
                                        throw new Error(
                                            "Can't set remote description, because peer connection is closed"
                                        );
                                    }
                                }
                            } else {
                                this._inSession.respondACKError(
                                    response,
                                    ControlMessage.errCode.BAD_ARGUMENTS,
                                    'SDP expected in response, but not received'
                                );
                                return Promise.reject(
                                    new Error('SDP Answer expected from SFU, but was not received')
                                );
                            }
                        } else {
                            if (response.getErrCode() !== ControlMessage.errCode.OFFER_REJECTED) {
                                if (pc.isP2P()) {
                                    this._logger.logError(
                                        'P2P error: ' +
                                            response.getErrCode() +
                                            ': ' +
                                            response.getErrMessage()
                                    );
                                    //this._peerController.blacklistConsumer(pc.getRemoteParty());
                                    pc.close();
                                } else {
                                    this._logger.logError(
                                        'Server error: ' +
                                            response.getErrCode() +
                                            ': ' +
                                            response.getErrMessage()
                                    );
                                    try {
                                        this.emit(
                                            ControlClient.event.SERVER_ERROR,
                                            response.toError(true)
                                        );
                                    } catch (err) {
                                        this._logger.logError(err.message);
                                    }
                                }
                            } else {
                                pc.rollbackOffer()
                                    .then(() => {
                                        this._logger.log('Offer rolled back');
                                    })
                                    .catch(err => {
                                        this._logger.logError(
                                            `Cannot rollback offer: ${err.message}`
                                        );
                                    });
                                this._logger.log(
                                    'Offer was rejected because of pending server offer (SDP Glare).'
                                );
                            }
                        }
                    })
                    .catch(err => {
                        if (isAlreadyDelivered) {
                            return;
                        }
                        if (!pc.isP2P() && !this._closed) {
                            const role = pc.getRole();
                            if (role === PeersController.role.MAIN) {
                                if (pc === this._peerController.getMainPC()) {
                                    this._lastError = err.message;
                                    this._maybeRecovery(false);
                                }
                            } else if (role === PeersController.role.ADDITIONAL) {
                                if (
                                    pc.isClosed() &&
                                    !this._localStreams.includes(pc.getStreamTapId())
                                ) {
                                    this._logger.logWarn(
                                        `Error ignored because screen sharing stream was removed already: ${err.message}`
                                    );
                                } else {
                                    this._lastError = err.message;
                                    this._maybeRecovery(false);
                                }
                            }
                        }
                        throw err;
                    });
            }, 'Send update req')
            .then(
                () => {
                    callbacks.forEach(callback => {
                        try {
                            callback(null);
                        } catch (err) {
                            this._logger.logError('Error in connect media callback');
                            this._logger.logError(err.stack);
                        }
                    });
                    if (this._onMediaConnected) {
                        this._onMediaConnected();
                    }
                },
                err => {
                    this._logger.logError(`Failed to exchange SDPs: ${err.stack}`);
                }
            );
    }

    _onUpdateSessionReq(request) {
        try {
            const body = request.getBody();
            const remoteSDP = body.sdp;
            if (remoteSDP) {
                this._logger.logCollapsed(SDP.prettyPrint(remoteSDP), 'Remote offer SDP');

                const remoteDescription = new RTCSessionDescription({
                    sdp: remoteSDP,
                    type: 'offer',
                });

                const requestStreams = body.streams || [];

                let signalledStreamsCount = 0;
                requestStreams.forEach(streamInfo => {
                    const tapId = streamInfo.id;
                    const msid = streamInfo.msid;

                    if (msid) {
                        signalledStreamsCount++;
                        this._addTapIdMapping(msid, tapId);
                    }
                });

                if (!this._peerController || this._peerController.isClosed()) {
                    throw new Error("Peer doesn't exists to process update.");
                }

                let resendOfferRequired = false;
                let localDescriptionSDP;

                const pcInfo = this._peerController.getPCByRemoteDescription(remoteDescription.sdp);
                const pc = pcInfo.pc;
                this._onPeerConnectionRemoteStreamsMappingUpdate(pc, requestStreams);
                const isP2P = (pc && pc.isP2P()) || pcInfo.remoteParty;
                if (isP2P && !this._isP2PSupported) {
                    this._peerController.blacklistConsumer(pc.getRemoteParty());
                    pc && pc.close();
                    return this._inSession.respondError(
                        request,
                        ControlMessage.errCode.BAD_ARGUMENTS,
                        'P2P connection is not supported'
                    );
                }
                if (!pc) {
                    if (isP2P) {
                        return this._inSession.respondError(
                            request,
                            ControlMessage.errCode.BAD_ARGUMENTS,
                            'P2P connection was closed. Start a new one.'
                        );
                    } else {
                        const errMessage = 'Cannot find pc by remote description => recovery';
                        this._logger.logError(errMessage);
                        this._lastError = errMessage;
                        return this._maybeRecovery(false);
                    }
                }
                if (!pc.isSignalingStable()) {
                    if (pc.isMaster()) {
                        return this._inSession.respondError(
                            request,
                            ControlMessage.errCode.OFFER_REJECTED,
                            'SDP Glare'
                        );
                    } else {
                        resendOfferRequired = true;
                    }
                }

                if (pc.isP2P() && pc.isLocked() && pc.isMaster()) {
                    return this._inSession.respondError(
                        request,
                        ControlMessage.errCode.OFFER_REJECTED,
                        'SDP Glare'
                    );
                }

                const pcLogId = `pc-${pc.getRole()}-${pc.getSeq()}`;
                this._logger.log(`${pcLogId} Will try get lock to Set Remote Description`);
                let releaseLockFn;
                let localStreamsTypes;
                pc.getLock(`${pcLogId} Set Remote Description`)
                    .then(releaseLock => {
                        releaseLockFn = releaseLock;
                        let answer;
                        return pc
                            .setRemoteDescription(remoteDescription, requestStreams)
                            .then(() => {
                                this._logger.log(`${pcLogId} Remote description set`);
                            })
                            .then(() => {
                                //We snapshot current types, because they could change
                                //during createAnswer (e.g. stream removed)
                                localStreamsTypes = this._getSnapshotOfLocalStreamTypes();
                                return pc.createAnswer();
                            })
                            .then(awr => {
                                answer = awr;
                                return pc.setLocalDescription(answer);
                            })
                            .then(() => {
                                this._logger.log(`${pcLogId} Local description set`);
                                return answer;
                            })
                            .catch(err => {
                                this._logger.log(`${pcLogId} Local description failed to set`);
                                this._logger.logError(`${pcLogId} ${err}`);
                                releaseLockFn();
                                return Promise.reject(err);
                            });
                    })
                    .then(answer => {
                        localDescriptionSDP = answer.sdp;
                    })
                    .catch(err => {
                        this._inSession.respondError(
                            request,
                            ControlMessage.errCode.BAD_ARGUMENTS,
                            'Failed process offer: ' + err.message
                        );
                        this._logger.logError('Failed to process offer: ' + err.message);
                        this._logger.logError(err.stack);
                        if (pc.isP2P()) {
                            //this._peerController.blacklistConsumer(pc.getRemoteParty());
                            pc.close();
                            releaseLockFn();
                        } else {
                            releaseLockFn();
                            this._lastError = 'Failed to process offer: ' + err.message;
                            this._maybeRecovery(false);
                        }
                        return Promise.reject(err);
                    })
                    .then(() => {
                        this._inSession.respondOK(
                            request,
                            {
                                body: {
                                    sdp: !pc.isP2P()
                                        ? pc.minifyLocalSDP(localDescriptionSDP)
                                        : localDescriptionSDP,
                                    streams: this._getStreamsMap(
                                        localDescriptionSDP,
                                        pc,
                                        localStreamsTypes
                                    ),
                                },
                            },
                            (err, response) => {
                                if (err) {
                                    releaseLockFn();
                                    this._lastError = err.message;
                                    this._maybeRecovery(false);
                                } else {
                                    err = response.toError();

                                    if (err) {
                                        if (pc.isP2P()) {
                                            //this._peerController.blacklistConsumer(pc.getRemoteParty());
                                            pc.close();
                                            releaseLockFn();
                                        } else {
                                            releaseLockFn();
                                            this._lastError = `Remote party failed to apply our SDP answer: ${err.message}`;
                                            this._maybeRecovery(false);
                                        }
                                    } else {
                                        pc.confirmSDPDelivered(localDescriptionSDP);
                                        if (
                                            this._peerController &&
                                            !this._peerController
                                                .getMainPC()
                                                .isControlChannelUsed() &&
                                            !this._peerController.isAnyP2PPC() &&
                                            signalledStreamsCount === 0 &&
                                            this._localStreams.length === 0
                                        ) {
                                            this._resetPeerConnectionForNoStreamsCase();
                                            releaseLockFn();
                                        }
                                        if (resendOfferRequired) {
                                            releaseLockFn();
                                            this.connectMedia();
                                        }
                                        releaseLockFn();
                                    }
                                }
                            }
                        );
                    })
                    .catch(err => {
                        this._logger.logError('Failed send answer: ' + err.message);
                        this._logger.logError(err.stack);
                        releaseLockFn();
                    });
            } else {
                this._logger.logError(
                    'Update session request from server without SDP. Should not happen.'
                );
            }
        } catch (err) {
            this._inSession.respondError(
                request,
                ControlMessage.errCode.CANNOT_SET_REMOTE_DESCRIPTION,
                'Failed process offer: ' + err.message
            );
            this._logger.logError('Failed to process offer: ' + err.message);
            this._lastError = 'Failed to process offer, set remote description: ' + err.message;
            this._logger.logError(err.stack);
            this._maybeRecovery(false);
        }
    }

    _getSnapshotOfLocalStreamTypes() {
        const res = {};
        this._localStreams.forEach(localStream => {
            const streamTapId = localStream.getId();
            res[streamTapId] = localStream.getType();
        });
        return res;
    }

    _resetPeerConnectionForNoStreamsCase() {
        //We got situation where we don't have any inbound/outbound streams
        //So, we can just reset peer connection and wait for next update
        //Without establishing media (because there is no media to establish)

        this._logger.log('Will reset peer connection. Because there is no media.');

        this._recovery.removeAllPendingStreams();
        this._removeAllRemoteStreams();

        this._tapIdByStreamId = {};
        this._streamIdsByTapId = {};

        this._initPeerConnection();
    }

    _onRemoteICE(request) {
        const body = request.getBody();
        const description = {
            type: 'candidate',
            candidate: body.candidate,
            sdpMLineIndex: body.mline_index,
            sdpMid: body.mid,
        };
        const pcId = body.origin;
        const candidate = new RTCIceCandidate(description);

        if (!this._peerController) {
            this._inSession.respondOK(request);
        } else {
            this._peerController
                .addIceCandidate(pcId, candidate)
                .then(() => {
                    this._inSession.respondOK(request);
                })
                .catch(err => {
                    this._logger.logError(
                        `Failed to addIceCandidate: ${candidate.candidate} ${err.message}`
                    );
                    this._inSession.respondError(
                        request,
                        ControlMessage.errCode.CANNOT_SET_REMOTE_NICE,
                        err.message
                    );
                });
        }
    }

    _removeRemoteStream(stream, streamId, tapId) {
        this._logger.log('Remove remote stream:' + streamId + ':' + tapId);
        delete this._remoteStreams[tapId];
        this._recovery.removeRemoteStream(tapId);
        try {
            this.emit(ControlClient.event.REMOTE_STREAM_REMOVED, stream, streamId, tapId);
            this._removeP2PMeter(stream);
        } catch (err) {
            this._logger.logError(err);
        }
        this._deleteTapIdMapping(streamId, tapId);
        const streamRotate = this._streamsRotate[streamId];
        if (streamRotate) {
            streamRotate.destroy();
        }
        delete this._streamsRotate[streamId];
    }

    getVideoBandwidthEstimation() {
        if (this._peerController) {
            const estimation = this._peerController.getVideoBandwidthEstimation();
            return estimation.map((pcEstimation, index) => {
                const sfuStatLocal = this._sfuConnectionStatus.local[index];
                if (sfuStatLocal) {
                    pcEstimation.availableSendBandwidthSFU = sfuStatLocal.limit_bitrate;
                    pcEstimation.requiredSendBandwidth = sfuStatLocal.required_bitrate;
                    pcEstimation.sendStatus = sfuStatLocal.status;
                    pcEstimation.sendPacketLost = sfuStatLocal.fraction_lost;
                    pcEstimation.sendDelay = sfuStatLocal.delay;
                    pcEstimation.uplinkQuality = sfuStatLocal.quality;
                }
                const sfuStatRemote = this._sfuConnectionStatus.remote[index];
                if (sfuStatRemote) {
                    pcEstimation.availableReceiveBandwidthSFU = sfuStatRemote.limit_bitrate;
                    pcEstimation.requiredReceiveBandwidth = sfuStatRemote.required_bitrate;
                    pcEstimation.receiveStatus = sfuStatRemote.status;
                    pcEstimation.receivePacketLost = sfuStatRemote.fraction_lost;
                    pcEstimation.receiveDelay = sfuStatRemote.delay;
                    pcEstimation.downlinkQuality = sfuStatRemote.quality;
                }
                return pcEstimation;
            });
        } else {
            return [];
        }
    }

    isStreamPinned(tapId) {
        const streamsPreferences = this._preferences.getRemoteState().streamsPreferences;
        let pinned = false;
        if (streamsPreferences && streamsPreferences[tapId]) {
            pinned = streamsPreferences[tapId].pinned;
        }
        return pinned;
    }

    getStatForRemoteStream(tapId) {
        const stream = this._remoteStreams[tapId];
        const sfuStat = this._streamsStat.getStatForRemoteStream(tapId);
        let stat;

        if (
            stream &&
            this._peerController &&
            (stat = this._peerController.getStatForRemoteStream(tapId))
        ) {
            if (stat.p2p) {
                stat.audio.volume = sfuStat.audio.volume;
                stat.video.quality = 3;
                stat.video.qualityDesired = 3;
                stat.video.qualityAvailable = 3;
            } else {
                stat.audio.volume = sfuStat.audio.volume;
                stat.video.quality = sfuStat.video.quality;
                stat.video.qualityDesired = sfuStat.video.qualityDesired;
                stat.video.qualityAvailable = sfuStat.video.qualityAvailable;
            }
        } else {
            stat = sfuStat;
            stat.video.bytes = 0;
            stat.audio.bytes = 0;
        }
        return stat;
    }

    getStatForLocalStream(tapId) {
        const localStream = this._getLocalStreamByTapId(tapId);
        let stat;
        if (localStream) {
            const stream = localStream.getStream();
            const streamId = stream ? stream.id : undefined;
            const sfuStat = this._streamsStat.getStatForLocalStream(tapId);

            if (streamId && this._peerController) {
                stat = this._peerController.getStatForLocalStream(streamId);
                stat.audio.volume = sfuStat.audio.volume;
                stat.video.quality = sfuStat.video.quality;
                stat.video.qualityDesired = sfuStat.video.qualityDesired;
                stat.video.qualityAvailable = sfuStat.video.qualityAvailable;
            } else {
                stat = sfuStat;
            }
        }
        return stat;
    }

    getStatForStream(tapId) {
        if (this._isRemoteStream(tapId)) {
            return this.getStatForRemoteStream(tapId);
        } else {
            return this.getStatForLocalStream(tapId);
        }
    }

    updateStats(callback) {
        this._maybeUpdateStats().then(() => {
            callback && callback();
        });
    }

    _resetRemoteStream(remoteStreamTapId) {
        this._logger.log(`Reset stream ${remoteStreamTapId}`);

        if (this._peerController.resetRemoteStream(remoteStreamTapId)) {
            this.connectMedia();
        }
    }

    devDropSignaling() {
        this._inSession.devDropSignaling();
    }

    devDropMedia() {
        if (this._peerController) {
            this._peerController.close();
        }
        this._recovery.setMediaState('failed', this._remoteStreams);
    }

    devUnbindSession() {
        this._inSession.request({
            event: ControlMessage.event.UNBIND_REQ,
        });
    }

    devUpdateParticipant(participantSettings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.UPDATE_PARTICIPANT_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: participantSettings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devUpdateStream(streamSettings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.UPDATE_STREAM_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: streamSettings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devRemoveStream(streamSettings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.DESTROY_STREAM_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: streamSettings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devRemoveParticipant(participantSettings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.DESTROY_PARTICIPANT_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: participantSettings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devUpdateRecording(recordingSettings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.UPDATE_RECORDING_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: recordingSettings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devRemoveRecording(recordingSettings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.DESTROY_RECORDING_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: recordingSettings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devSendQuit(settings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.DESTROY_SFU,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: settings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devSetRecording(enabled, paused, callback) {
        if (enabled) {
            this.devUpdateRecording(
                {
                    recording: 'dev',
                    storage: 'nfs',
                    destination: {
                        name: 'default',
                        path: '/' + this._conferenceId,
                    },
                    active: true,
                    paused: paused,
                    version: Date.now(),
                },
                callback
            );
        } else {
            this.devRemoveRecording(
                {
                    recording: 'dev',
                },
                callback
            );
        }
    }

    devUpdateChannel(settings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.UPDATE_CHANNEL_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: settings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devRemoveChannel(settings, callback) {
        this._inSession.getCrlSession().request(
            {
                event: ControlMessage.event.DESTROY_CHANNEL_REQ,
                type: ControlMessage.type.CONFERENCE,
                id: this._conferenceId,
                partition: this._conferenceId + '_partition',
                body: settings,
            },
            (err, response) => {
                err = err || response.toError();
                callback && callback(err);
            }
        );
    }

    devSetFakeQuality(streamId, { quality, qualityAvailable, qualityDesired }) {
        this._streamsStat.setFakeQuality(streamId, { quality, qualityAvailable, qualityDesired });
    }

    devCancelFakeQuality(streamId) {
        this._streamsStat.cancelFakeQuality(streamId);
    }

    devSetServersideNetworkQuality(networkQualityData) {
        this._serversideNetworkQuality.setOwnNQIData(networkQualityData);
    }

    getSignalingStatus() {
        return this._inSession.getStatus();
    }

    waitSignalingConnection() {
        return new Promise(resolve => {
            if (this._inSession.getStatus() === InSession.status.IN_SESSION) {
                resolve();
            } else {
                this.once(ControlClient.event.SIGNALING_CONNECTED, resolve);
            }
        });
    }

    getMediaStatus() {
        if (this._peerController) {
            return this._peerController.getICEstate();
        } else {
            return ControlClient.mediaStatus.NONE;
        }
    }

    getIsMainMediaConnected() {
        const mainMediaStatus = this._peerController?.getMainPC()?.getICEstate();

        return mainMediaStatus === ControlClient.mediaStatus.CONNECTED ||
            mainMediaStatus === ControlClient.mediaStatus.COMPLETED;
    }

    getRemoteStreams() {
        return this._remoteStreams;
    }

    _removeChannel(tapId) {
        this._channels = this._channels.filter(ch => ch.getId() !== tapId);
    }

    _getChannelByLabel(label) {
        return this._channels.find(ch => ch.getLabel() === label);
    }

    _getChannelsByPC(pc) {
        return this._channels.filter(ch => ch.getPC() === pc);
    }

    _createDataChannel(tapId, label) {
        const channel = new Channel({
            id: tapId,
            label,
            peersController: this._peerController,
            logger: this._logger,
        });
        this._channels.push(channel);
        return channel;
    }

    createChannel(tapId, label) {
        const channel = this._getChannelByLabel(label);

        if (channel) {
            if (channel.getId() === tapId) return channel;

            this._removeChannel(channel.getId());
            channel.removeAllListeners();
            channel.close(); // emits CLOSED_MANUALLY event
        }

        const newChannel = this._createDataChannel(tapId, label);

        newChannel.once(Channel.event.CLOSED_MANUALLY, () => {
            this._removeChannel(newChannel.getId());
            const otherChannels = this._getChannelsByPC(newChannel.getPC());
            if (otherChannels.length === 0 && this._peerController) {
                this._peerController.maybeClosePCWithoutChannels(newChannel.getPC());
            }
        });

        return newChannel;
    }

    getChannel(tapId) {
        return this._channels.find(channel => channel.getId() === tapId);
    }

    supportsOnlySDPUnifiedPlan() {
        return ClientCapabilities.isSDPUnifiedPlanOnly();
    }

    getSDPSemanticsUsed() {
        return getSdpSemantics(this._options.preferUnifiedPlan);
    }
}

ControlClient.event = {
    REMOTE_STREAM_ADDED: 'remote_stream_added',
    REMOTE_STREAM_REMOVED: 'remote_stream_removed',
    LOCAL_STREAM_ADDED: 'local_stream_added',
    LOCAL_STREAM_REMOVED: 'local_stream_removed',
    LOCAL_STREAM_REPLACED: 'local_stream_replaced',
    SIGNALING_CONNECTED: 'signaling_connected',
    SESSION_CREATED: 'session_created',
    GOT_IDENTITY: 'got_identity',
    SERVER_ERROR: 'server_error',
    RECOVERY_STARTED: 'recovery_started',
    RECOVERY_COMPLETED: 'recovery_completed',
    REMOTE_STREAM_RECOVERY_STARTED: 'recovery_stream_started',
    REMOTE_STREAM_RECOVERY_COMPLETED: 'recovery_stream_completed',
    REMOTE_STREAM_REPLACED: 'remote_stream_replaced',
    MEDIA_STATUS_CHANGED: 'media_status_changed',
    SIGNALING_STATUS_CHANGED: 'signaling_status_changed',
    SFU_STAT_ORDER_CHANGED: 'sfu_stat_order_changed',
    SFU_STAT_VOLUME_CHANGED: 'sfu_stat_volume_changed',
    SFU_STAT_QUALITY_CHANGED: 'sfu_stat_quality_changed',
    SFU_STAT_CHANGED: 'sfu_stat_changed',
    RECENTLY_ACTIVE_CHANGED: 'recently_active_changed',
    SESSION_DESTROYED: 'session_destroyed',
    LOCAL_NETWORK_QUALITY_CHANGED: 'local_network_quality_changed',
    REMOTE_NETWORK_QUALITY_CHANGED: 'remote_network_quality_changed',
    REMOTE_STREAM_SIGNALLING_STATUS_CHANGED: 'remote_stream_signalling_status_changed',
    NEGOTIATE_VIDEO_MODE_STATE_CHANGED: 'negotiate_video_mode_state_changed',
    MIX_AUDIO_MODE_STATE_CHANGED: 'mix_audio_mode_state_changed',
    ICE_CANDIDATES_IP_ADDRESSES_UPDATED: 'ice_candidates_ip_addresses_updated',
};

ControlClient.streamType = {
    VIDEO_MAIN: 'video/main',
    VIDEO_SCREENSHARING: 'video/screensharing',
};

ControlClient.devSettings = {
    LOG_TO_CONSOLE: 'ls_console', //Always write logs to console (1/0)
    COLLAPSE_LOGS: 'ls_collapse_log', //Collapse long messages in logs? (1/0
    P2P_CODEC: 'ls_p2p_codec', //Override codec for p2p ("H264/90000")
    SS_CODEC: 'ls_ss_codec', //Override codec for screen sharing ("H264/90000")
};

ControlClient.userTypeAction = {
    MUTE: 'mute',
    UNMUTE: 'unmute',
    START: 'start',
    STOP: 'stop',
};

ControlClient.userTypeMedia = {
    AUDIO: 'audio',
    VIDEO: 'video',
    SCREENSHARING: 'screensharing',
};

ControlClient.mediaStatus = PeersController.iceState;

ControlClient.signalingStatus = InSession.status;

ControlClient.channelEvent = Channel.event;

ControlClient.closeReason = {
    DEFAULT: 'Requested to close',
    RECONNECT: 'Start recovery before update RCT state',
    STOP_MEDIA_RECOVERY: 'Stop recovery during update RCT state',
    END_MEETING: 'End meeting',
};

ControlClient.mixAudioMode = PeersController.mixAudioMode;

ControlClient.negotiateVideoMode = PeersController.negotiateVideoMode;

export default ControlClient;
