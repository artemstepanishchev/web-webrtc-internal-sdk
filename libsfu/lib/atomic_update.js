class AtomicUpdate {
    constructor(initialState, options = {}) {
        const { nextStateOverridesPrevious } = options;
        this._initialState = initialState || {};
        this._localState = JSON.parse(JSON.stringify(this._initialState));
        this._remoteState = JSON.parse(JSON.stringify(this._initialState));
        this._commitInProgress = false;
        this._updatesQueue = [];
        this._commitsQueue = [];
        this._isChanged = false;
        this._version = 0;
        this._nextStateOverridesPrevious = nextStateOverridesPrevious;
    }

    update(updateFunction) {
        if (this._commitInProgress) {
            if (this._nextStateOverridesPrevious) {
                this._updatesQueue = [updateFunction];
            } else {
                this._updatesQueue.push(updateFunction);
            }
        } else {
            this._localState = updateFunction(this._localState);
            this._isChanged = true;
        }
    }

    commit(commitFunction) {
        if (!this._commitInProgress) {
            this._commitInProgress = true;
        } else {
            if (this._nextStateOverridesPrevious) {
                this._commitsQueue = [commitFunction];
            } else {
                this._commitsQueue.push(commitFunction);
            }
            return;
        }

        const self = this;
        const version = this._version;

        commitFunction(this._isChanged ? this._localState : null, this._remoteState, success, fail);

        function success() {
            if (version !== self._version) {
                return;
            }
            self._commitInProgress = false;
            self._remoteState = JSON.parse(JSON.stringify(self._localState));
            self._isChanged = false;
            self._releaseUpdatesQueue();
            self._releaseCommitsQueue();
        }

        function fail() {
            if (version !== self._version) {
                return;
            }
            self._commitInProgress = false;
            self._releaseUpdatesQueue();
            self._releaseCommitsQueue();
        }
    }

    getRemoteState() {
        return this._remoteState;
    }

    getLocalState() {
        return this._localState;
    }

    updateInitialState(initialStateUpdate) {
        this._initialState = Object.assign({}, this._initialState, initialStateUpdate);
    }

    resetRemoteState() {
        this._version++;
        this._remoteState = JSON.parse(JSON.stringify(this._initialState));
        this._commitInProgress = false;
        this._commitsQueue = [];
        this._releaseUpdatesQueue();
        this._isChanged = true;
    }

    _releaseUpdatesQueue() {
        while (this._updatesQueue.length > 0) {
            const updateFunction = this._updatesQueue.shift();
            this.update(updateFunction);
        }
    }

    _releaseCommitsQueue() {
        if (this._commitsQueue.length > 0) {
            const commitFunction = this._commitsQueue.shift();
            this.commit(commitFunction);
        }
    }
}

export default AtomicUpdate;
