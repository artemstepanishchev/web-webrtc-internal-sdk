import AtomicUpdate from './atomic_update.js';
import LogWrapper from './log_wrapper.js';
import assert from './assert.js';

const defaultQualityRange = {
    min: 0,
    max: 3,
};

class Preferences {
    constructor(options) {
        this._logger = new LogWrapper(options.logger, () => 'pr:' + this._sessionId);
        this._sessionId = options.sessionId;
        this._state = new AtomicUpdate({
            maxRemoteVideo: this._normalizeMaxRemoteVideo(
                typeof options.maxRemoteVideo !== 'undefined' ? options.maxRemoteVideo : 1
            ),
            maxRemoteAudio:
                typeof options.maxRemoteAudio !== 'undefined' ? options.maxRemoteAudio : 1,
        });
    }

    getLocalState() {
        return this._state.getLocalState();
    }

    getRemoteState() {
        return this._state.getRemoteState();
    }

    updateRemoteStreamsPreferences(streamsPreferences) {
        this._state.update(prevState => {
            prevState.streamsPreferences = prevState.streamsPreferences || {};
            const prevStreamPreferences = prevState.streamsPreferences;
            streamsPreferences.forEach(streamPreferences => {
                const tapId = streamPreferences.id;
                if (!tapId) {
                    throw new Error('id should be specified for a stream');
                }
                if (typeof tapId !== 'string') {
                    throw new Error('id should be a string. Got ' + typeof tapId);
                }
                prevStreamPreferences[tapId] = prevStreamPreferences[tapId] || {
                    id: tapId,
                    pinned: false,
                    quality: defaultQualityRange,
                };
                const remoteStreamPreference = prevStreamPreferences[tapId];
                if (typeof streamPreferences.pinned !== 'undefined') {
                    remoteStreamPreference.pinned = streamPreferences.pinned;
                }
                if (typeof streamPreferences.quality !== 'undefined') {
                    remoteStreamPreference.quality = streamPreferences.quality;
                }
            });
            this._logger.logDebug(JSON.stringify(prevState, null, 4));
            return prevState;
        });
    }

    /**
     * @param {String[]|{id: String, quality: {min:Number, max:Number}}[]} pinnedStreams
     */
    setPinnedStreams(pinnedStreams) {
        const badTapIdIdx = pinnedStreams.findIndex(
            stream => typeof stream !== 'string' && typeof stream?.id !== 'string'
        );
        if (badTapIdIdx !== -1) {
            const badTapId =
                typeof pinnedStreams[badTapIdIdx] === 'object' &&
                pinnedStreams[badTapIdIdx] !== null
                    ? pinnedStreams[badTapIdIdx].id
                    : pinnedStreams[badTapIdIdx];
            throw new Error(
                'setPinnedStreams: pinnedStreams expected to be array of tapIds or shape: {id: String, quality: {min:Number, max:Number}}[] . Instead got type: ' +
                    typeof badTapId +
                    ' ,value: ' +
                    badTapId +
                    ' .'
            );
        }

        this._state.update(prevState => {
            prevState.streamsPreferences = prevState.streamsPreferences || {};
            const prevStreamsPreferences = prevState.streamsPreferences;
            //Pin selected with requested quality(if provided)
            pinnedStreams.forEach(pinnedStream => {
                const tapId = pinnedStream?.id ?? pinnedStream;
                const newQuality = pinnedStream?.quality || {};

                prevStreamsPreferences[tapId] = prevStreamsPreferences[tapId] || {
                    id: tapId,
                };

                prevStreamsPreferences[tapId].quality = {
                    ...defaultQualityRange,
                    ...newQuality,
                };

                prevStreamsPreferences[tapId].pinned = true;
            });
            //Unpin others, reset quality
            for (const tapId in prevStreamsPreferences) {
                if (prevStreamsPreferences.hasOwnProperty(tapId)) {
                    if (pinnedStreams.every(stream => (stream?.id ?? stream) !== tapId)) {
                        prevStreamsPreferences[tapId].pinned = false;
                        prevStreamsPreferences[tapId].quality = defaultQualityRange;
                    }
                }
            }

            return prevState;
        });
    }

    setSlotsPreferences(options) {
        assert(
            typeof options.maxRemoteAudio === 'undefined' ||
                typeof options.maxRemoteAudio === 'number',
            'Wrong maxRemoteAudio type' + typeof options.maxRemoteAudio
        );
        assert(
            typeof options.maxRemoteVideo === 'undefined' ||
                typeof options.maxRemoteVideo === 'number' ||
                Array.isArray(options.maxRemoteVideo),
            'Wrong maxRemoteVideo type' + typeof options.maxRemoteVideo
        );

        this._state.update(prevState => {
            if (typeof options.maxRemoteAudio !== 'undefined') {
                prevState.maxRemoteAudio = options.maxRemoteAudio;
            }
            if (typeof options.maxRemoteVideo !== 'undefined') {
                prevState.maxRemoteVideo = this._normalizeMaxRemoteVideo(options.maxRemoteVideo);
            }
            return prevState;
        });
    }

    _normalizeMaxRemoteVideo(maxRemoteVideo) {
        if (typeof maxRemoteVideo === 'undefined' || Array.isArray(maxRemoteVideo)) {
            return maxRemoteVideo;
        } else if (typeof maxRemoteVideo === 'number' && !isNaN(maxRemoteVideo)) {
            return [
                { quality: 3, slots: maxRemoteVideo },
                { quality: 2, slots: 0 },
                { quality: 1, slots: 0 },
            ];
        } else {
            throw new TypeError('maxRemoteVideo should be array or number');
        }
    }

    commit(commitFunction) {
        this._state.commit((localState, remoteState, success, fail) => {
            //Calculate the difference
            let diff;
            if (localState !== null) {
                //null - means there is no change
                diff = {};

                //Remote Streams Settings
                const diffStreamsPreferences = [];
                const localStreamsPreferences = localState.streamsPreferences || {};
                const remoteStreamsPreferences = remoteState.streamsPreferences || {};

                for (const tapId in localStreamsPreferences) {
                    if (localStreamsPreferences.hasOwnProperty(tapId)) {
                        const localStreamPreferences = localStreamsPreferences[tapId];
                        const remoteStreamPreferences = remoteStreamsPreferences[tapId];
                        if (!remoteStreamPreferences) {
                            diffStreamsPreferences.push(localStreamPreferences);
                        } else {
                            const change = {
                                id: tapId,
                            };
                            let isChanged = false;
                            if (localStreamPreferences.pinned !== remoteStreamPreferences.pinned) {
                                change.pinned = localStreamPreferences.pinned;
                                isChanged = true;
                            }
                            if (
                                localStreamPreferences.quality.min !==
                                    remoteStreamPreferences.quality.min ||
                                localStreamPreferences.quality.max !==
                                    remoteStreamPreferences.quality.max
                            ) {
                                change.quality = localStreamPreferences.quality;
                                isChanged = true;
                            }
                            if (isChanged) {
                                diffStreamsPreferences.push(change);
                            }
                        }
                    }
                }
                if (diffStreamsPreferences.length > 0) {
                    diff.streamsPreferences = diffStreamsPreferences;
                }

                //Max slots settings
                const localMaxRemoteAudio = localState.maxRemoteAudio;
                const remoteMaxRemoteAudio = remoteState.maxRemoteAudio;
                const localMaxRemoteVideo = localState.maxRemoteVideo;
                const remoteMaxRemoteVideo = remoteState.maxRemoteVideo;

                if (localMaxRemoteAudio !== remoteMaxRemoteAudio) {
                    diff.maxRemoteAudio = localMaxRemoteAudio;
                }

                if (
                    typeof remoteMaxRemoteVideo === 'undefined' &&
                    typeof localMaxRemoteVideo !== 'undefined'
                ) {
                    diff.maxRemoteVideo = localMaxRemoteVideo;
                } else if (
                    typeof remoteMaxRemoteVideo !== 'undefined' &&
                    typeof localMaxRemoteVideo !== 'undefined'
                ) {
                    const newSorted = localMaxRemoteVideo.sort(byQuality);
                    const currentSorted = remoteMaxRemoteVideo.sort(byQuality);
                    if (JSON.stringify(newSorted) !== JSON.stringify(currentSorted)) {
                        diff.maxRemoteVideo = localMaxRemoteVideo;
                    }
                }
            } else {
                diff = null;
            }

            if (diff !== null && Object.keys(diff).length === 0) {
                diff = null;
            }

            commitFunction(diff, success, fail);

            function byQuality(a, b) {
                return b.quality - a.quality;
            }
        });
    }

    reset(initialStateUpdate) {
        if (initialStateUpdate) {
            this._state.updateInitialState(initialStateUpdate);
        }
        this._state.resetRemoteState(initialStateUpdate);
    }
}

export default Preferences;
