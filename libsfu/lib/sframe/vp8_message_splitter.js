import { MessageSplitter } from './message_splitter.js';
import { MessagePart } from './message_part.js';

/**
 * MessageSplitter implementation for VP8 codec.
 */
export class VP8MessageSplitter extends MessageSplitter {
    _split(message, offset) {
        return [
            new MessagePart(message.subarray(0, offset), false),
            new MessagePart(message.subarray(offset, message.length), true),
        ];
    }

    splitClearMessage(message) {
        //Check size
        if (message.length < 3) {
            //Invalid
            return [new MessagePart(message, true)];
        }

        const isKeyFrame = (message[0] & 0x01) === 0;

        //check if more
        if (isKeyFrame) {
            //Check size
            if (message.length < 10) {
                //Invalid
                return [new MessagePart(message, true)];
            }
            // Split message in two parts
            return this._split(message, 10);
        }

        //No key frame
        return this._split(message, 3);
    }
}
