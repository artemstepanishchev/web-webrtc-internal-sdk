import { MessageSplitter } from './message_splitter.js';
import { MessagePart } from './message_part.js';

/**
 * MessageSplitter implementation for H264 codec, finds NALU blocks in source message and splits it
 * to parts. Payload parts that contain video data and require encryption are returned with e2ee flag set to true.
 */
export class H264MessageSplitter extends MessageSplitter {

    /**
     * Checks NALU header for certain NALU type, only types 1-5 have VCL data and their payload should be encrypted
     * @param naluHeader NALU header byte
     * @returns {boolean} indication whether encryption is needed
     * @private
     */
    _isNeedE2EE(naluHeader) {
        const naluType = naluHeader & 0x1f; // NAL_TYPE_MASK = 0x1f
        return naluType >= 1 && naluType <= 5;
    }

    splitClearMessage(message) {
        const parts = [];
        this._splitH264NALUs(message, (part, naluHeaderOffset) => {
            if(this._isNeedE2EE(part[naluHeaderOffset])) {
                parts.push(new MessagePart(part.subarray(0, naluHeaderOffset + 1), false)); // don't encrypt header for SFU
                parts.push(new MessagePart(part.subarray(naluHeaderOffset + 1, message.length), true));
            }
            else {
                parts.push(new MessagePart(part, false));
            }
        });
        return parts;
    }

    /**
     * Splits H264 payload into NALU blocks and calls handler to process them
     * @param payload input H264 payload
     * @param handler handler to call to process found NALU block
     * @returns {boolean} indication of found NALU blocks
     * @private
     */
    _splitH264NALUs(payload, handler) {
        let currentNaluStart = -1;
        let currentNaluHeader = -1;
        let activeZeroes = 0;
        let result = false;

        for (let i = 0; i < payload.byteLength; i++) {
            if (payload[i] === 0x01 && activeZeroes > 1) {
                if (currentNaluStart >= 0) {
                    handler(payload.subarray(currentNaluStart, i - activeZeroes), currentNaluHeader - currentNaluStart);
                    result = true;
                }
                currentNaluStart = i - activeZeroes;
                currentNaluHeader = i + 1;
            }
            activeZeroes = payload[i] === 0 ? activeZeroes + 1 : 0;
        }

        if (!(currentNaluStart < 0) && currentNaluStart < payload.byteLength) {
            handler(payload.subarray(currentNaluStart, payload.byteLength), currentNaluHeader - currentNaluStart);
            result = true;
        }

        return result;
    }
}
