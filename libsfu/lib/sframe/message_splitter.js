import { MessagePart } from './message_part.js';

/**
 * Base class for message splitting functionality, allows to split clear and encrypted messages to parts for later
 * separate processing. This is needed to split metadata (headers, key frame blocks, etc) from payload, to make
 * it possible to encrypt payload without affecting metadata and allow backend processing of metadata.
 */
export class MessageSplitter {
    /**
     * @param getSdk callback function to get MlsSdkClient instance
     */
    constructor(getSdk) {
        this._getSdk = getSdk;
    }

    /**
     * Splits clear message to list of parts,
     * if not overridden, just returns one part marked as encryption required, for whole input message
     * @param message input clear message to split
     * @returns {MessagePart[]} list of parts (MessagePart) of the message after splitting
     */
    splitClearMessage(message) {
        return [new MessagePart(message, true)];
    }

    /**
     * Splits encrypted message to message parts, finds encrypted blocks by looking for signature amd
     * separates them from clear blocks, indicating that encrypted blocks need decryption
     * @param message input encrypted message to split
     * @returns {MessagePart[]} list of parts (MessagePart) of the message after splitting
     */
    splitEncryptedMessage(message) {
        const result = [];

        const payloadView = new DataView(message.buffer);
        let currentBlockEnd = payloadView.byteLength;

        for (let i = payloadView.byteLength; i > 0; i = i - this._getSdk().getSignatureSize()) {
            // going back, as it is simpler to use signature offset
            // looking for 0xE2EE signature
            if (
                !(
                    this._getSdk().checkSignature(payloadView, i) &&
                    i < payloadView.byteLength - this._getSdk().getOffsetSize()
                )
            )
                continue;

            const recordedOffset = payloadView.getUint32(i + this._getSdk().getSignatureSize());
            if (
                recordedOffset >= 0 &&
                recordedOffset < payloadView.byteLength &&
                this._getSdk().checkSignature(payloadView, recordedOffset)
            ) {
                // offset is valid, we found encrypted block

                // we need to check for data after encrypted block, if there was data - it is a separate unencrypted block
                const encryptedBlockEnd =
                    i + this._getSdk().getOffsetSize() + this._getSdk().getSignatureSize();
                if (currentBlockEnd !== encryptedBlockEnd) {
                    // we still have data block before e2ee block
                    result.unshift(
                        new MessagePart(message.subarray(encryptedBlockEnd, currentBlockEnd), false)
                    );
                }

                result.unshift(
                    new MessagePart(message.subarray(recordedOffset, encryptedBlockEnd), true)
                );

                // reset offsets, new data starts before current block, no e2ee for now
                currentBlockEnd = recordedOffset;
                i = recordedOffset;
            }
        }

        if (currentBlockEnd > 0) {
            // if message starts with clear block we need to add it too
            result.unshift(new MessagePart(message.subarray(0, currentBlockEnd), false));
        }

        return result;
    }
}
