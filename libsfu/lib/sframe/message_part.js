export class MessagePart {
    constructor(partArray, e2ee) {
        this._partArray = partArray;
        this._e2ee = e2ee;
    }

    getContent() {
        return this._partArray;
    }

    setContent(newContentArray) {
        this._partArray = newContentArray;
    }

    isRequireE2EE() {
        return this._e2ee;
    }
}
