import { MessageSplitter } from './message_splitter.js';

/**
 * Class to process incoming frames
 */
export class FrameProcessor {
    /**
     * @param getMlsSdkClient mls sdk retriever
     * @param messageSplitters codec to message splitter (MessageSplitter) map
     */
    constructor(getMlsSdkClient, messageSplitters) {
        this._getMlsSdkClient = getMlsSdkClient;
        this._messageSplitters = messageSplitters;
        this._defaultMessageSplitter = new MessageSplitter(getMlsSdkClient);
    }

    _getSplitter(codec) {
        return this._messageSplitters[codec] ?? this._defaultMessageSplitter;
    }

    /**
     * Combines list of message parts to a single buffer
     * @param parts list of message parts to join
     * @returns {ArrayBufferLike} join result buffer
     * @private
     */
    _combineSingleBuffer(parts) {
        let completeSize = 0;
        for (let i = 0; i < parts.length; i++) {
            completeSize += parts[i].getContent().length;
        }

        const result = new Uint8Array(completeSize);
        let offset = 0;
        for (let i = 0; i < parts.length; i++) {
            const partContent = parts[i].getContent();
            result.set(partContent, offset);
            offset += partContent.length;
        }

        return result.buffer;
    }

    /**
     * Process encryption of frame coming from insertable streams API
     * @param chunk containing frame payload
     * @param codec codec used for the stream
     * @param controller used to enqueue result chunk for processing
     */
    encryptFrame(chunk, codec, controller) {
        const payload = new Uint8Array(chunk.data);
        const parts = this._getSplitter(codec).splitClearMessage(payload);
        let globalPartOffset = 0;
        for (let i = 0; i < parts.length; i++) {
            if (!codec || parts[i].isRequireE2EE()) {
                // if E2EE not required for this part - no need to do anything
                const encrypted = this._getMlsSdkClient().encrypt(
                    parts[i].getContent(),
                    globalPartOffset
                );

                if (encrypted == null) {
                    // this is done per discussion to avoid gray screen and image corruption. We discard all frame if we cannot encrypt
                    return;
                }

                parts[i].setContent(encrypted);
            }

            globalPartOffset += parts[i].getContent().length;
        }

        chunk.data = this._combineSingleBuffer(parts);
        controller.enqueue(chunk);
    }

    /**
     * Process decryption of frame coming from insertable streams API
     * @param chunk containing frame payload
     * @param controller used to enqueue result chunk for processing
     */
    decryptFrame(chunk, controller) {
        const payload = new Uint8Array(chunk.data);
        // for packet decryption we don't need codec info, as we detect encrypted blocks based on signatures
        const parts = this._getSplitter().splitEncryptedMessage(payload);

        for (let i = 0; i < parts.length; i++) {
            if (parts[i].isRequireE2EE()) {
                // if E2EE not required for this part - no need to do anything
                const decrypted = this._getMlsSdkClient().decrypt(parts[i].getContent());

                if (decrypted == null) {
                    // this is done per discussion to avoid gray screen and image corruption. We discard all frame if we cannot decrypt
                    return;
                }

                parts[i].setContent(decrypted);
            }
        }

        chunk.data = this._combineSingleBuffer(parts);
        controller.enqueue(chunk);
    }
}
