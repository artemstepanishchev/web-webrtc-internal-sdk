import { CODECS } from '../utils/codecs.js';
import { VP8MessageSplitter } from './vp8_message_splitter.js';
import { H264MessageSplitter } from './h264_message_splitter.js';
import { FrameProcessor } from './frame_processor.js';

/**
 * Works with insertable streams API and MlsSdKClient to encrypt and decrypt media streams
 */
export class SFrameClient {
    constructor(config) {
        this._logger = config.logger;
        this._config = config;

        this._messageSplitters = {};
        this._messageSplitters[CODECS.VP8] = new VP8MessageSplitter(config.getMlsSdkClient);
        this._messageSplitters[CODECS.H264] = new H264MessageSplitter(config.getMlsSdkClient);

        this._frameProcessor = new FrameProcessor(config.getMlsSdkClient, this._messageSplitters);
    }

    /**
     * Sets up decryption logic to handle decryption of incoming media streams
     * @param receiver RTCRtpReceiver
     */
    setupDecryption(receiver) {
        try {
            const { readable, writable } = receiver.createEncodedStreams();
            // eslint-disable-next-line no-undef
            const transformStream = new TransformStream({
                transform: async (chunk, controller) => {
                    try {
                        this._frameProcessor.decryptFrame(chunk, controller);
                    } catch (err) {
                        this._config
                            .getMlsSdkClient()
                            .getErrorReporter()
                            .add(
                                'DECRYPT_FRAME_EXCEPTION',
                                'Error during frame decryption: ' + err.stack
                            );
                    }
                },
            });
            readable.pipeThrough(transformStream).pipeTo(writable);
        } catch (err) {
            this._logger.logWarn('could not initialize decryption: ' + err.message);
        }
    }

    /**
     * Sets up encryption logic to encrypt outgoing media streams
     * @param sender RTCRtpSender
     * @param codec codec information to help split message with leaving metadata clear
     */
    setupEncryption(sender, codec) {
        try {
            const { readable, writable } = sender.createEncodedStreams();

            const isVideo = codec && sender.track?.kind !== 'audio';
            // eslint-disable-next-line no-undef
            const transformStream = new TransformStream({
                transform: async (chunk, controller) => {
                    if (chunk.data.byteLength > 0) {
                        // added after seeing a lot of empty audio packets
                        try {
                            this._frameProcessor.encryptFrame(
                                chunk,
                                isVideo ? codec : null,
                                controller
                            );
                        } catch (err) {
                            this._config
                                .getMlsSdkClient()
                                .getErrorReporter()
                                .add(
                                    'ENCRYPT_FRAME_EXCEPTION',
                                    'Error during frame encryption: ' + err.stack
                                );
                        }
                    }
                },
            });
            readable.pipeThrough(transformStream).pipeTo(writable);
        } catch (err) {
            this._logger.logWarn('could not initialize encryption: ' + err.message);
        }
    }
}
