import EventEmitter from './events.js';
import PeersController from './peers_controller.js';
import LogWrapper from './log_wrapper.js';
import ClientCapabilities from './client_capabilities.js';

class Channel extends EventEmitter {
    constructor({ id, peersController, logger, label }) {
        super();
        this._id = id;
        this._peersController = null;
        this._pc = null;
        this._dataChannel = null;
        this._closed = false;
        this._ready = false;
        this._label = label;
        this._logger = new LogWrapper(logger, () => `ch: ${label} (tapId: ${id})`);
        if (peersController) {
            this.setPeersController(peersController);
        }
    }

    getLabel() {
        return this._label;
    }

    getId() {
        return this._id;
    }

    getPC() {
        return this._pc;
    }

    _update() {
        if (this._closed) return;
        if (this._dataChannel) return;

        const pc = this._peersController?.getPCByChannelLabel(this._label);
        if (!pc || pc.isClosed()) return;

        this._pc = pc;
        if (ClientCapabilities.dedicatedControlChannels) {
            this._dataChannel = pc.getDataChannelByLabel(this._label);
            this._logger.log('datachannel open');

            if (!this._dataChannel) {
                this._logger.logError('Dedicated data channel not found on mainPC');
                return;
            }
        } else {
            this._logger.log('data channel created');
            this._dataChannel = pc.createDataChannel(this._id);
        }

        const datachannel = this._dataChannel;

        this._dataChannel.onmessage = event => {
            if (datachannel !== this._dataChannel) return;

            const msg = event.data;
            // obsolete negotiation messages
            if (msg === 'READY' || msg === 'PENDING') return;

            this.emit(Channel.event.MESSAGE, msg);
        };

        this._dataChannel.onclose = () => {
            if (datachannel !== this._dataChannel) {
                return;
            }
            this._logger.log('datachannel closed');
            this._dataChannel = null;
            this._setReady(false);
        };

        const onOpen = () => {
            if (datachannel !== this._dataChannel) return;

            this._logger.log('datachannel open');
            this._setReady(true);
        };

        this._dataChannel.onopen = onOpen;

        if (this._dataChannel.readyState === Channel.state.OPEN) {
            setTimeout(onOpen, 0);
        }
    }

    send(data) {
        if (this.isOpen()) {
            this._dataChannel.send(data);
        } else {
            this._logger.logError('Trying to send data to datachannel which is not open');
        }
    }

    close() {
        this._logger.log('manual close');
        this._closed = true;
        this._setReady(false);
        this._closeDataChannel();
        this.emit(Channel.event.CLOSED_MANUALLY);
    }

    _closeDataChannel() {
        if (this._dataChannel) {
            if (!ClientCapabilities.dedicatedControlChannels) {
                //We should not close dedicated channel, as we cannot re-open it in FF
                this._dataChannel.close();
            }
            this._dataChannel = null;
        }
    }

    _setReady(newReadyState) {
        if (newReadyState !== this._ready) {
            this._ready = newReadyState;
            this._logger.log(`new state: ${newReadyState}`);
            this.emit(Channel.event.READY_STATE_CHANGED, newReadyState);
        }
    }

    isReady() {
        return this._ready;
    }

    isOpen() {
        return this._dataChannel?.readyState === Channel.state.OPEN;
    }

    // TODO: change to setPeerConnection? It seems there is no need in controller
    setPeersController(peersController) {
        if (peersController !== this._peersController) {
            this._logger.log('set new peersController');
            this._peersController = peersController;
            this._closeDataChannel();
            this._setReady(false);
            this._update();
            if (!peersController.isClosed()) {
                peersController.on(PeersController.event.NEW_MAIN_PC, () => {
                    this._update();
                });
            }
        }
    }
}

Channel.event = {
    READY_STATE_CHANGED: 'ready_state_changed',
    MESSAGE: 'message',
    CLOSED_MANUALLY: 'closed_manually',
};

Channel.state = {
    OPEN: 'open',
};

export default Channel;
