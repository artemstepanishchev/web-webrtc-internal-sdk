/* eslint-disable camelcase */
class ControlMessage {
    constructor(options) {
        if (typeof options === 'string') {
            this.fromString(options);
        } else if (typeof options === 'object') {
            this._event = options.event;
            this._body = options.body || {};
            this._success = options.success;
            this._err_message = options.err_message;
            this._err_code = options.err_code;
            this._req_seq = options.req_seq;
            this._req_src = options.req_src;
            this._connection = options.connection;
            this._version = options.version;
            this._type = options.type;
            this._partition = options.partition;
            this._id = options.id;
            this._rx_ts = options.rx_ts;
            this._tx_ts = options.tx_ts;
        }
    }

    toError(fatal) {
        if (this._success) {
            return null;
        } else {
            const err = new Error(this._err_message);
            err.code = this._err_code;
            err.fatal = !!fatal;
            return err;
        }
    }

    toObject() {
        return {
            req_src: this._req_src,
            req_seq: this._req_seq,
            rx_ts: this._rx_ts,
            tx_ts: this._tx_ts,
            success: this._success,
            err_code: this._err_code,
            err_message: this._err_message,
            event: this._event,
            body: this._body,
            version: this._version,
            type: this._type,
            id: this._id,
            partition: this._partition,
        };
    }

    toString() {
        return JSON.stringify(this.toObject());
    }

    fromString(msg) {
        const message = JSON.parse(msg);
        this._req_src = message.req_src;
        this._req_seq = message.req_seq;
        this._rx_ts = message.rx_ts;
        this._tx_ts = message.tx_ts;
        this._event = message.event;
        this._body = message.body;
        this._success = message.success;
        this._err_message = message.err_message;
        this._err_code = message.err_code;
        this._connection = undefined;
        this._version = message.version;
        this._type = message.type;
        this._id = message.id;
        this._partition = message.partition;
    }

    getEvent() {
        return this._event;
    }

    getBody() {
        return this._body;
    }

    getRequestSeq() {
        return this._req_seq;
    }

    getRequestSrc() {
        return this._req_src;
    }

    getErrCode() {
        return this._err_code;
    }

    getErrMessage() {
        return this._err_message;
    }

    isSuccess() {
        return this._success;
    }

    setConnection(newConnection) {
        this._connection = newConnection;
    }

    getConnection() {
        return this._connection;
    }

    setVersion(newVersion) {
        this._version = newVersion;
    }

    getVersion() {
        return this._version;
    }

    setId(newId) {
        this._id = newId;
    }

    getId() {
        return this._id;
    }

    setType(newType) {
        this._type = newType;
    }

    getType() {
        return this._type;
    }

    setPartition(newPartition) {
        this._partition = newPartition;
    }

    getPartition() {
        return this._partition;
    }

    getReceivedTime() {
        return this._rx_ts;
    }

    setReceivedTimestamp(ts) {
        this._rx_ts = ts;
    }

    setSentTimestamp(ts) {
        this._tx_ts = ts;
    }

    isResponse() {
        return this._event.endsWith('_resp');
    }
}

ControlMessage.event = {
    CREATE_REQ: 'create_req',
    CREATE_RES: 'create_resp',
    CREATE_ACK: 'create_ack',
    DESTROY_REQ: 'destroy_req',
    DESTROY_RES: 'destroy_resp',
    UPDATE_REQ: 'update_req',
    UPDATE_RES: 'update_resp',
    UPDATE_ACK: 'update_ack',
    STATUS_REQ: 'status_req',
    STATUS_RES: 'status_resp',
    UNBIND_REQ: 'unbind_req',
    UNBIND_RES: 'unbind_resp',

    UPDATE_PARTICIPANT_REQ: 'update_participant_req',
    UPDATE_PARTICIPANT_RES: 'update_participant_resp',
    DESTROY_PARTICIPANT_REQ: 'destroy_participant_req',
    DESTROY_PARTICIPANT_RES: 'destroy_participant_resp',

    UPDATE_STREAM_REQ: 'update_stream_req',
    UPDATE_STREAM_RES: 'update_stream_resp',
    DESTROY_STREAM_REQ: 'destroy_stream_req',
    DESTROY_STREAM_RES: 'destroy_stream_resp',

    UPDATE_CHANNEL_REQ: 'update_channel_req',
    UPDATE_CHANNEL_RES: 'update_channel_resp',
    DESTROY_CHANNEL_REQ: 'destroy_channel_req',
    DESTROY_CHANNEL_RES: 'destroy_channel_resp',

    UPDATE_RECORDING_REQ: 'update_recording_req',
    UPDATE_RECORDING_RES: 'update_recording_resp',
    DESTROY_RECORDING_REQ: 'destroy_recording_req',
    DESTROY_RECORDING_RES: 'destroy_recording_resp',

    ICE_REQ: 'ice_req',
    ICE_RES: 'ice_resp',
    NO_MORE_ICE_REQ: 'no_more_ice_req',
    NO_MORE_ICE_RES: 'no_more_ice_resp',
    ACTIVE_SPEAKERS_REPORT_REQ: 'as_report_req',
    ACTIVE_SPEAKERS_REPORT_RES: 'as_report_resp',

    MEDIA_STAT_REQ: 'media_stat_req',
    MEDIA_STAT_RES: 'media_stat_resp',

    INFO_REQ: 'info_req',
    INFO_RES: 'info_resp',

    PSTN_AS_REPORT_REQ: 'pstn_as_report_req',

    KEEP_ALIVE_REQ: 'keep_alive_req',
    KEEP_ALIVE_RES: 'keep_alive_resp',
    SET_NETWORK_CONDITIONS_REQ: 'set_network_conditions_req',
    SET_NETWORK_CONDITIONS_RES: 'set_network_conditions_resp',

    ADD_PEER_REQ: 'add_peer_req',
    ADD_PEER_RES: 'add_peer_resp',
    GET_PEERS_REQ: 'get_peers_req',
    GET_PEERS_RES: 'get_peers_resp',
    REMOVE_PEER_REQ: 'remove_peer_req',
    REMOVE_PEER_RES: 'remove_peer_resp',
    SET_VOLUME_REQ: 'set_volume_req',
    SET_VOLUME_RES: 'set_volume_resp',
    SET_MUTE_REQ: 'set_mute_req',
    SET_MUTE_RES: 'set_mute_resp',
    GET_STATUS_REQ: 'get_status_req',
    GET_STATUS_RES: 'get_status_resp',
    SET_SLOTS_REQ: 'set_slots_req',
    SET_SLOTS_RES: 'set_slots_resp',
    ADD_STREAM_REQ: 'add_stream_req',
    ADD_STREAM_RES: 'add_stream_resp',
    REMOVE_STREAM_REQ: 'remove_stream_req',
    REMOVE_STREAM_RES: 'remove_stream_resp',

    REGISTER_REQ: 'register_req',
    REGISTER_RES: 'register_resp',

    DESTROY_SFU: 'destroy_sfu',

    NETWORK_STATUS_REQ: 'network_status_req',
    NETWORK_STATUS_RES: 'network_status_resp',
};

ControlMessage.errCode = {
    BAD_ARGUMENTS: 1,
    CANNOT_GENERATE_ANSWER: 2,
    CANNOT_ADD_PEER_TO_HUB: 3,
    CANNOT_UPDATE_PEER_PREFERENCES: 4,
    CANNOT_SET_REMOTE_DESCRIPTION: 5,
    SESSION_ALREADY_EXISTS: 6,
    SESSION_NOT_FOUND: 7,
    OFFER_REJECTED: 8,
    CANNOT_SET_REMOTE_NICE: 9,
    INTERNAL_ERROR: 10,
    CONFERENCE_NOT_FOUND: 11,
    MEDIA_DISCONNECTED: 12,
    CONFERENCE_ALREADY_EXISTS: 17,
    INVALID_TOKEN: 18,
    SESSION_REMOVED: 19,
};

ControlMessage.errMessage = {
    1: 'Bad arguments',
    2: 'Cannot generate answer',
    3: 'Cannot add peer to hub',
    4: 'Cannot update peer preferences',
    5: 'Cannot set remote description',
    6: 'Session with this id is already exists',
    7: 'Session with this id not found',
    8: 'Offer from client rejected because of pending server offer (SDP Glare)',
    9: 'Cannot set remote nice',
    10: 'Internal Error',
    11: 'Conference with this id not found',
    12: 'Media disconnected',
    17: 'Conference already exists',
    18: 'Invalid token',
    19: 'Session removed from the conference',
};

ControlMessage.responseEvent = {};
ControlMessage.ackEvent = {};

ControlMessage.type = {
    SESSION: 'session',
    CONFERENCE: 'conference',
    CONNECTION: 'connection',
};

ControlMessage.infoType = {
    DEVICES: 'devices',
    USER_ACTION: 'user_action',
    STATE_UPDATE: 'state_update',
    NET_QUALITY: 'net_quality',
    PERFORMANCE: 'performance',
    NET_TYPE: 'net_type',
};

//Find appropriate RESPONSES and ACKS for all REQUESTS and fill constants
Object.keys(ControlMessage.event).forEach(key => {
    if (key.match(/_REQ$/)) {
        const res_key = key.replace('_REQ', '_RES');
        const req_value = ControlMessage.event[key];
        const res_value = ControlMessage.event[res_key];
        const ack_key = key.replace('_REQ', '_ACK');
        const ack_value = ControlMessage.event[ack_key];
        if (res_value) {
            ControlMessage.responseEvent[req_value] = res_value;
        }
        if (ack_value) {
            ControlMessage.ackEvent[res_value] = ack_value;
        }
    }
});

export default ControlMessage;
