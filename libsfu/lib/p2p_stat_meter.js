class P2PStatMeter {
    constructor({ audioContext, stream, streamTapId }) {
        this._audioContext =
            audioContext || new (window.AudioContext || window.webkitAudioContext)();
        this._stream = stream;
        this._hasAudio = stream.getAudioTracks().length > 0;
        this._hasVideo = stream.getVideoTracks().length > 0;
        this._tapId = streamTapId;
        if (this._hasAudio) {
            this._setupAudioMeter();
        }
    }
    _setupAudioMeter() {
        const source = this._audioContext.createMediaStreamSource(this._stream);
        this._analyser = this._audioContext.createAnalyser();
        this._analyser.fftSize = 32;
        this._analyser.minDecibels = -90;
        this._analyser.maxDecibels = -10;
        this._analyser.smoothingTimeConstant = 0;
        source.connect(this._analyser);
        this._bufferLength = this._analyser.frequencyBinCount;
        this._dataArray = new Uint8Array(this._bufferLength);
    }
    getStat() {
        let vol;
        if (this._hasAudio) {
            this._analyser.getByteFrequencyData(this._dataArray);
            let sum = 0;
            for (let i = 0; i < this._bufferLength; i += 2) {
                sum += this._dataArray[i];
            }
            vol = Math.min(100, sum / (this._bufferLength / 2));
            vol = vol > 10 ? 1 : 0;
        } else {
            vol = 0;
        }
        let quality;
        if (this._hasVideo) {
            quality = 3;
        } else {
            quality = 0;
        }
        return {
            id: this._tapId,
            v: vol,
            q: quality,
            desired_q: quality, // eslint-disable-line camelcase
            available_q: quality, // eslint-disable-line camelcase
        };
    }
    getStream() {
        return this._stream;
    }
    getStreamTapId() {
        return this._tapId;
    }
}

export default P2PStatMeter;
