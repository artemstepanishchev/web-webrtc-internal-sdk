function getCurrentWindow() {
    var e;
    return (
        (null === (e = window[Symbol.for('rwcInternalApi')]) || void 0 === e
            ? void 0
            : e.externalWindowInstance) || window
    );
}

class PreserveVideo {
    constructor() {
        this._streamsInfo = {};
    }
    hasStream(streamTapId) {
        return typeof this._streamsInfo[streamTapId] === 'object';
    }
    addStream(streamTapId) {
        if (!this._streamsInfo[streamTapId]) {
            this._streamsInfo[streamTapId] = {};
        }
    }
    removeStream(streamTapId) {
        this.clearStream(streamTapId);
        delete this._streamsInfo[streamTapId];
    }
    saveStream(streamTapId, stream) {
        const currentDocument = getCurrentWindow().document;

        const videoElement = Array.from(currentDocument.querySelectorAll('video')).find(
            v => v.srcObject === stream
        );
        if (
            videoElement &&
            typeof this._streamsInfo[streamTapId] === 'object' &&
            typeof videoElement.videoWidth === 'number' &&
            videoElement.videoWidth > 2 &&
            videoElement.videoHeight > 2
        ) {
            const canvasElement = currentDocument.createElement('canvas');
            canvasElement.width = videoElement.videoWidth;
            canvasElement.height = videoElement.videoHeight;
            this._streamsInfo[streamTapId].stream = canvasElement.captureStream();
            this._streamsInfo[streamTapId].canvas = canvasElement;
            const ctx = canvasElement.getContext('2d');
            ctx.drawImage(videoElement, 0, 0);
        }
    }
    clearStream(streamTapId) {
        const streamInfo = this._streamsInfo[streamTapId];
        if (typeof streamInfo === 'object') {
            if (streamInfo.stream) {
                streamInfo.stream.getVideoTracks().forEach(track => track.stop());
            }
            if (streamInfo.canvas) {
                streamInfo.canvas.remove();
            }
        }
    }
    getStream(streamTapId) {
        return (this._streamsInfo[streamTapId] || {}).stream;
    }
    getCanvas(streamTapId) {
        return (this._streamsInfo[streamTapId] || {}).canvas;
    }
}

export default PreserveVideo;
