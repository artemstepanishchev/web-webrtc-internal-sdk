import assert from './assert.js';
import { CODECS } from './utils/codecs.js';
import { getMaybeMultiProp, makeSdpBlock } from './utils/sdp.js';
import { toArray } from './utils/misc.js';

class SDP {
    constructor(options) {
        this._session = undefined;

        if (typeof options === 'string') {
            this.fromString(options);
        } else if (typeof options === 'object' && options !== null) {
            this._session = options;
        } else if (typeof options === 'undefined') {
            this._session = {
                version: 0,
                name: 'RC SFU',
                origin: {
                    username: '-',
                    sessionId: Math.floor(Math.random() * 999999999999999999),
                    sessionVersion: 1,
                    netType: 'IN',
                    addrType: 'IP4',
                    unicastAddress: '127.0.0.1',
                },
                time: {
                    start: 0,
                    stop: 0,
                },
                attributes: {},
            };
        } else {
            throw new Error('Unexpected options type (' + typeof options + ' )');
        }
    }

    fromString(sdp) {
        const session = {
            attributes: {},
            media: [],
        };
        let isSessionContext = true;

        let currentMedia;

        const lines = sdp.replace(/\r\n/g, '\n').split('\n');
        for (let i = 0; i < lines.length; i++) {
            const line = lines[i];
            let match = line.match(/^([vostacmb])=(.*)$/);
            if (match) {
                const type = match[1];
                const value = match[2];

                switch (type) {
                    case 'a': {
                        let attrName;
                        let attrValue;

                        match = value.match(/^([^:]+):(.*)$/);
                        if (match) {
                            attrName = match[1];
                            attrValue = match[2];
                        } else {
                            attrName = value;
                            attrValue = undefined;
                        }
                        if (isSessionContext) {
                            this._addMaybeMultiProp(session.attributes, attrName, attrValue);
                        } else {
                            this._addMaybeMultiProp(currentMedia.attributes, attrName, attrValue);
                        }
                        break;
                    }
                    case 'm':
                        match = value.match(/^(\S+) (\S+) (\S+) (.*)$/);
                        if (match) {
                            currentMedia = {
                                media: match[1],
                                port: match[2],
                                proto: match[3],
                                format: match[4],
                                attributes: {},
                            };
                            isSessionContext = false;
                            session.media.push(currentMedia);
                        } else {
                            throw new Error('Bad media line. Cannot parse (' + value + ')');
                        }
                        break;
                    case 'c':
                        match = value.trim().split(' ');
                        if (match.length === 3) {
                            const connectionData = {
                                netType: match[0],
                                addrType: match[1],
                                connectionAddress: match[2],
                            };
                            if (isSessionContext) {
                                session.connectionData = connectionData;
                            } else {
                                currentMedia.connectionData = connectionData;
                            }
                        } else {
                            throw new Error(
                                'Bad connection data line. Cannot parse (' + value + ')'
                            );
                        }
                        break;
                    case 't':
                        if (isSessionContext) {
                            match = value.match(/^(\S+) (\S+)$/);
                            if (match) {
                                const time = {
                                    start: match[1],
                                    stop: match[2],
                                };
                                this._addMaybeMultiProp(session, 'time', time);
                            } else {
                                throw new Error('Bad time line. Cannot parse (' + value + ')');
                            }
                        } else {
                            throw new Error('Bad time line. (In context of media)');
                        }
                        break;
                    case 's':
                        if (typeof session.name === 'undefined') {
                            session.name = value;
                        } else {
                            throw new Error('Bad session name. (duplicate)');
                        }
                        break;
                    case 'o':
                        match = value.trim().split(' ');
                        if (match.length === 6) {
                            session.origin = {
                                username: match[0],
                                sessionId: match[1],
                                sessionVersion: match[2],
                                netType: match[3],
                                addrType: match[4],
                                unicastAddress: match[5],
                            };
                        } else {
                            throw new Error('Bad origin. Cannot parse (' + value + ')');
                        }
                        break;
                    case 'v':
                        if (value === '0') {
                            session.version = '0';
                        } else {
                            throw new Error('Version ' + value + ' of SDP is not supported');
                        }
                        break;
                    case 'b':
                        match = value.match(/^([^:]+):(.*)$/);
                        if (match) {
                            const bandwidthType = match[1];
                            const bandwidth = match[2];
                            if (isSessionContext) {
                                session.bandwidth = {
                                    bwtype: bandwidthType,
                                    bandwidth: bandwidth,
                                };
                            } else {
                                currentMedia.bandwidth = {
                                    bwtype: bandwidthType,
                                    bandwidth: bandwidth,
                                };
                            }
                        } else {
                            throw new Error('Bad bandwidth line. Cannot parse (' + value + ')');
                        }
                        break;
                }
            }
        }
        this._session = session;

        return session;
    }

    toString() {
        const s = this._session;
        const lines = [];

        lines.push(`v=${s.version || '0'}`);
        const o = s.origin;
        if (o) {
            lines.push(
                `o=${o.username} ${o.sessionId} ${o.sessionVersion} ${o.netType} ${o.addrType} ${o.unicastAddress}`
            );
        }
        if (s.name) {
            lines.push(`s=${s.name}`);
        }

        const t = getMaybeMultiProp(s, 'time');
        t.forEach(time => {
            lines.push(`t=${time.start} ${time.stop}`);
        });

        lines.push(...makeSdpBlock(s));

        const media = s.media || [];

        media.forEach(m => {
            lines.push(`m=${m.media} ${m.port} ${m.proto} ${m.format}`);
            lines.push(...makeSdpBlock(m));
        });

        lines.push('');

        return lines.join('\r\n');
    }

    getOrigin() {
        return this._session.origin;
    }

    getSessionId() {
        return this._session.origin.sessionId;
    }

    getAttribute(attrName) {
        return getMaybeMultiProp(this._session.attributes, attrName);
    }

    getFirstAttribute(attrName) {
        assert(typeof attrName === 'string' && attrName.length > 0);
        const attrs = this.getAttribute(attrName);
        return attrs[0];
    }

    setFirstAttribute(attrName, value) {
        this._session.attributes[attrName] = value;
    }

    getFirstMediaAttributeWithFallback(media, attrName) {
        const res = this.getFirstMediaAttribute(media, attrName);

        return res ? res : this.getFirstAttribute(attrName);
    }

    setAttribute(attrName, attrValue) {
        this._session.attributes = this._session.attributes || {};
        this._addMaybeMultiProp(this._session.attributes, attrName, attrValue);
    }

    deleteAttribute(attrName) {
        delete this._session.attributes[attrName];
    }

    deleteMediaAttribute(media, attrName) {
        assert(typeof media === 'object', 'Media should be an object');
        delete media.attributes[attrName];
    }

    getFirstMediaAttribute(media, attrName) {
        assert(typeof media === 'object', 'Media should be an object');
        const attrs = this.getMediaAttribute(media, attrName);
        return attrs[0];
    }

    getMediaAttribute(media, attrName) {
        assert(typeof media === 'object', 'Media should be an object');
        return getMaybeMultiProp(media.attributes, attrName);
    }

    getMediaConnectionDataWithFallback(media) {
        assert(typeof media === 'object', 'Media should be an object');
        return media.connectionData || this._session.connectionData;
    }

    setMediaAttribute(media, attrName, attrValue) {
        assert(typeof media === 'object', 'Media should be an object');
        media.attributes = media.attributes || {};
        this._addMaybeMultiProp(media.attributes, attrName, attrValue);
    }

    replaceMediaAttribute(media, attrName, attrValue) {
        this.deleteMediaAttribute(media, attrName);
        this.setMediaAttribute(media, attrName, attrValue);
    }

    removeRTPExtensionFromMedia(media, extId) {
        assert(typeof media === 'object', 'Media should be an object');
        const extensionURI = SDP.extMap[extId];
        const extMap = this.getMediaAttribute(media, 'extmap').filter(
            extension => !extension.includes(extensionURI)
        );
        this.replaceMediaAttribute(media, 'extmap', extMap);
    }

    increaseVersion() {
        this._session.origin.sessionVersion++;
    }

    addMedia(media) {
        assert(typeof media === 'object', 'Media should be an object');
        this._session.media = this._session.media || [];
        this._session.media.push(media);
    }

    removeAllCodecs(media) {
        assert(typeof media === 'object', 'Media should be an object');
        media.format = '';
        delete media.attributes['rtcp-fb'];
        delete media.attributes['rtpmap'];
        delete media.attributes['fmtp'];
    }

    // eslint-disable-next-line camelcase
    addCodec({ name, id, rtcp_fb, fmtp }) {
        const codecType = SDP.codecType[name];
        if (!codecType) {
            throw new Error('Unknown codec ' + name);
        }

        const media = this.getMedia(codecType);
        media.forEach(m => {
            this.addCodecToMedia(m, name, id, rtcp_fb, fmtp);
        });
    }

    getAllCodecsOfMedia(media) {
        const codecs = [];
        const rtpmaps = this.getMediaAttribute(media, 'rtpmap');
        const rtcpFB = this.getMediaAttribute(media, 'rtcp-fb');
        const fmtp = this.getMediaAttribute(media, 'fmtp');
        const reRTPMAP = /^(\d+) ([^ ]+)$/;
        for (let j = 0; j < rtpmaps.length; j++) {
            const rtpmap = rtpmaps[j];
            const match = rtpmap.match(reRTPMAP);
            if (match) {
                const codecName = match[2].toUpperCase();
                const codecId = match[1];
                const codecInfo = {
                    id: codecId,
                    name: codecName,
                    rtcp_fb: [], // eslint-disable-line camelcase
                    fmtp: [],
                };

                const reParams = new RegExp(codecId + ' (.*?)$');
                rtcpFB.forEach(line => {
                    const match = line.match(reParams);
                    if (match) {
                        codecInfo.rtcp_fb.push(match[1]);
                    }
                });

                fmtp.forEach(line => {
                    let match = line.match(reParams);
                    if (match) {
                        codecInfo.fmtp.push(match[1]);
                    } else {
                        match = line.match(new RegExp('(\\d+) apt=' + codecId));
                        if (match) {
                            //By some reason Chrome will not take any other number for RTX except 101 for
                            //Codec H264 with rtpmap 100, to avoid such constrains we copy ids generated by browser
                            //And reuse same id for RTX for same codecs
                            codecInfo.rtxId = match[1];
                        }
                    }
                });
                codecs.push(codecInfo);
            }
        }
        return codecs;
    }

    getAllCodecs() {
        let codecs = [];
        const mediaList = this._session.media;
        for (let i = 0; i < mediaList.length; i++) {
            const media = mediaList[i];
            codecs = codecs.concat(this.getAllCodecsOfMedia(media));
        }
        return codecs;
    }

    // eslint-disable-next-line camelcase
    addCodecToMedia(media, codec, codecId, rtcp_fb, fmtp) {
        assert(typeof media === 'object', 'Media should be an object');
        const codecType = SDP.codecType[codec];
        if (!codecType) {
            throw new Error('Unknown codec ' + codec);
        }

        if (media.media !== codecType) {
            throw new Error(`Media type and codec doesn't match (${media.media} and ${codecType})`);
        }

        codecId = codecId || SDP.codecId[codec];
        this.setMediaAttribute(media, 'rtpmap', codecId + ' ' + codec);

        rtcp_fb = rtcp_fb || []; // eslint-disable-line camelcase
        // eslint-disable-next-line camelcase
        rtcp_fb.forEach(fb => {
            this.setMediaAttribute(media, 'rtcp-fb', codecId + ' ' + fb);
        });

        fmtp = fmtp || [];
        // due to RCV-29621 we cannot set this attribute according to RFC-4566
        fmtp.forEach(params => {
            this.setMediaAttribute(media, 'fmtp', codecId + ' ' + params);
        });

        media.format = ((media.format || '') + ' ' + codecId).trim();

        return media;
    }

    addRTPExtensionToMedia(media, extId, extName) {
        assert(typeof media === 'object', 'Media should be an object');

        if (!extName) {
            extName = SDP.extMap[extId];
        }

        if (!extName) {
            throw new Error('Unknown extension ' + extId);
        }

        this.setMediaAttribute(media, 'extmap', extId + ' ' + extName);
    }

    _addMaybeMultiProp(root, propName, propValue) {
        if (typeof propValue === 'undefined') {
            propValue = true;
        }

        root[propName] =
            typeof root[propName] === 'undefined'
                ? propValue
                : toArray(root[propName]).concat(toArray(propValue));
    }

    getMedia(type) {
        return typeof type === 'undefined'
            ? this._session.media || []
            : this._session.media.filter(m => m.media === type);
    }

    getMediaCopy() {
        return JSON.parse(JSON.stringify(this._session.media || []));
    }

    setMedia(media) {
        assert(Array.isArray(media), 'Media should be an array');
        this._session.media = media;
    }

    getSession() {
        return this._session;
    }

    static getSemicolonSeparatedParameterValueByName(expression, paramName) {
        const params = expression.split(';');
        for (let i = 0; i < params.length; i++) {
            const param = params[i];
            const [key, value] = param.split('=', 2);
            if (key.trim() === paramName) {
                return (value || '').trim();
            }
        }
        return undefined;
    }

    static spread(sdpsAsString) {
        const sdpsAsArray = sdpsAsString.split(/^v=0$/m).map(sdp => 'v=0' + sdp);
        sdpsAsArray.shift();
        return sdpsAsArray;
    }

    static concat(sdpsAsArray) {
        return sdpsAsArray.join('');
    }

    static prettyPrint(sdpsAsString) {
        return sdpsAsString.replace(/^([mv])=/gm, '\n$1=');
    }

    /**
     * @param {RTCSessionDescription} description
     * @returns {string=} session id
     */
    static getSessionId(sdp) {
        return sdp?.match(/o=[^ \n\r]+ ([^ \n\r]+)/)?.[1];
    }

    static removeNonImportantParts(sdpAsString) {
        return sdpAsString
            .replace(/^(o=.* )(\d+)( IN)/m, '$1$3')
            .replace(/^a=setup:.*$/gm, '')
            .replace(/^a=ssrc:\d+ (msid|mslabel|label).*$/gm, '')
            .replace(/^b=AS:30$/gm, '')
            .replace(/\r\n/g, '');
    }

    /**
     * @param {string} sdpA
     * @param {string} sdpB
     * @returns {boolean}
     */
    static areSDPsEqual(sdpA, sdpB) {
        return SDP.removeNonImportantParts(sdpA) === SDP.removeNonImportantParts(sdpB);
    }
}

SDP.mediaType = {
    VIDEO: 'video',
    AUDIO: 'audio',
    DATA: 'application',
};

SDP.codec = CODECS;

SDP.codecType = {};
SDP.codecType[SDP.codec.OPUS] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.G722] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.PCMU] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.PCMA] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.CN8000] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.CN16000] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.CN32000] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.VP8] = SDP.mediaType.VIDEO;
SDP.codecType[SDP.codec.VP9] = SDP.mediaType.VIDEO;
SDP.codecType[SDP.codec.H264] = SDP.mediaType.VIDEO;
SDP.codecType[SDP.codec.ULPFEC] = SDP.mediaType.VIDEO;
SDP.codecType[SDP.codec.RED] = SDP.mediaType.VIDEO;
SDP.codecType[SDP.codec.RED_AUDIO] = SDP.mediaType.AUDIO;
SDP.codecType[SDP.codec.FLEXFEC_03] = SDP.mediaType.VIDEO;
SDP.codecType[SDP.codec.RTX] = SDP.mediaType.VIDEO;

SDP.rtxForCodec = {};
SDP.rtxForCodec[SDP.codec.OPUS] = false;
SDP.rtxForCodec[SDP.codec.G722] = false;
SDP.rtxForCodec[SDP.codec.PCMU] = false;
SDP.rtxForCodec[SDP.codec.PCMA] = false;
SDP.rtxForCodec[SDP.codec.CN8000] = false;
SDP.rtxForCodec[SDP.codec.CN16000] = false;
SDP.rtxForCodec[SDP.codec.CN32000] = false;
SDP.rtxForCodec[SDP.codec.VP8] = true;
SDP.rtxForCodec[SDP.codec.VP9] = true;
SDP.rtxForCodec[SDP.codec.H264] = true;
SDP.rtxForCodec[SDP.codec.ULPFEC] = false;
SDP.rtxForCodec[SDP.codec.RED] = true;
SDP.rtxForCodec[SDP.codec.FLEXFEC_03] = false;
SDP.rtxForCodec[SDP.codec.RTX] = false;

SDP.codecId = {};
SDP.codecId[SDP.codec.OPUS] = '111';
SDP.codecId[SDP.codec.PCMU] = '0';
SDP.codecId[SDP.codec.PCMA] = '8';
SDP.codecId[SDP.codec.G722] = '9';
SDP.codecId[SDP.codec.CN8000] = '13';
SDP.codecId[SDP.codec.CN16000] = '105';
SDP.codecId[SDP.codec.CN32000] = '106';
SDP.codecId[SDP.codec.VP8] = '100';
SDP.codecId[SDP.codec.VP9] = '101';
SDP.codecId[SDP.codec.H264] = '107';
SDP.codecId[SDP.codec.ULPFEC] = '127';
SDP.codecId[SDP.codec.RED] = '102';
SDP.codecId[SDP.codec.FLEXFEC_03] = '123';
SDP.codecId[SDP.codec.RTX] = '97';

SDP.rtpExtension = {
    AUDIO_LEVEL: 1,
    TRANSMISSION_TIME_OFFSET: 2,
    ABSOLUTE_SENDER_TIME: 3,
    VIDEO_ORIENTATION: 4,
    TRANSPORT_CC: 5,
    PLAYOUT_DELAY: 6,
    VIDEO_CONTENT_TYPE: 7,
    VIDEO_TIMING: 8,
};

SDP.extMap = {
    1: 'urn:ietf:params:rtp-hdrext:ssrc-audio-level',
    2: 'urn:ietf:params:rtp-hdrext:toffset',
    3: 'http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time',
    4: 'urn:3gpp:video-orientation',
    5: 'http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01',
    6: 'http://www.webrtc.org/experiments/rtp-hdrext/playout-delay',
    7: 'http://www.webrtc.org/experiments/rtp-hdrext/video-content-type',
    8: 'http://www.webrtc.org/experiments/rtp-hdrext/video-timing',
};

export default SDP;

/**
 * @callback SDPMangler
 * @param {SDP} sdp
 * @returns {boolean} false if did nothing with SDP
 */

/**
 * @example
 *  const resultSdpText = (new SdpProcessor(mySdp))
 *     .thru(sdp => sdp.addCodecToMedia(...params))
 *     .thru(sdp => sdp.setAttribute('myattr', value))
 *     .getResult()
 */
export class SdpProcessor {
    /**
     * @param {string} sdp
     */
    constructor(sdp) {
        this._source = sdp;
        this._result = sdp;
        this._touched = false;
    }

    /**
     * Allows mangler callback to modify SDP
     * @param {SDPMangler} mangler
     * @returns {SdpProcessor}
     */
    thru(mangler) {
        if (!this._sdp) {
            this._sdp = new SDP(this._source);
        }
        this._touched = mangler(this._sdp) || this._touched;

        return this;
    }

    /**
     * @returns {string}
     */
    getResult() {
        if (!this._touched || !this._sdp) {
            return this._source;
        }

        return this._sdp.toString();
    }
}

/**
 * Gets local stream IDs and their MIDs out of local SDP
 * @param {SDP} sdp
 * @returns {object} keys are MSIDs and values are arrays of MIDs
 */
export function getMsidToMidMapping(sdp) {
    const streams = {};
    const mediaOutSections = sdp.getMedia().filter(mediaSection => {
        const sendonly = sdp.getFirstMediaAttribute(mediaSection, 'sendonly');
        const sendrecv = sdp.getFirstMediaAttribute(mediaSection, 'sendrecv');

        return sendonly || sendrecv;
    });

    mediaOutSections.forEach(mediaSection => {
        const mid = sdp.getFirstMediaAttribute(mediaSection, 'mid');
        const msid = sdp.getFirstMediaAttribute(mediaSection, 'msid')?.split?.(' ')[0];
        if (!msid) {
            return;
        }

        if (!streams[msid]) {
            streams[msid] = [];
        }
        streams[msid].push(mid);
    });

    return streams;
}
