import { getSupportsUnifiedPlanOnly } from './utils/peer_connection.js';
import { once } from './utils/misc';

export default (() => {
    const userAgent = navigator.userAgent;
    let match = userAgent.match(/Chrom(e|ium)\/(\d+)\./);
    const isChromeCompatible = !!match;
    let chromeVersion;
    if (match) {
        chromeVersion = Number(match[2]);
    }

    match = userAgent.match(/Firefox\/(\d+)\./);
    let firefoxVersion;
    if (match) {
        firefoxVersion = Number(match[1]);
    }
    const isFirefoxCompatible = !!match;

    match = userAgent.match(/Version\/([\d.]+) Safari/);
    const isSafariCompatible = !!match;
    let safariVersion;
    if (match) {
        safariVersion = Number(match[1]);
    }

    const capabilities = {
        chromeCompatible: isChromeCompatible,
        fireFoxCompatible: isFirefoxCompatible,
        isSDPPlanB: () => isChromeCompatible && !capabilities.isSDPUnifiedPlanOnly(),
        // Should be detected only after the plan-b origin trial token injection (RTCExtendDeadlineForPlanBRemoval)
        // https://github.com/GoogleChrome/OriginTrials/blob/gh-pages/developer-guide.md#16-can-i-provide-tokens-by-running-script
        isSDPUnifiedPlanOnly: once(() => isFirefoxCompatible || isSafariCompatible || getSupportsUnifiedPlanOnly()),
        // TODO: is it still relevant?
        p2p: isChromeCompatible,
        chromeLikeStat: isChromeCompatible,
        firefoxLikeStat: isFirefoxCompatible,
        safariLikeStat: isSafariCompatible,
        simulcast: isChromeCompatible || isFirefoxCompatible || isSafariCompatible,
        transportCCForAudio: isChromeCompatible,
        googFlags: isChromeCompatible || isSafariCompatible,
        //Chrome doesn't support SDP rollback: https://bugs.chromium.org/p/webrtc/issues/detail?id=4676
        rollbackRTCDescription:
            (isFirefoxCompatible && firefoxVersion >= 59) ||
            (isChromeCompatible && chromeVersion >= 87),
        encodingsQ1toQ3: isChromeCompatible || isSafariCompatible,
        encodingsQ3toQ1: isFirefoxCompatible,
        chromeLikeSimulcast: isChromeCompatible || isSafariCompatible,
        firefoxLikeSimulcast: isFirefoxCompatible,
        rotationExtensionSupport: isChromeCompatible || isSafariCompatible,
        dedicatedControlChannels: false,
        incorrectPlanBMSIDs: isSafariCompatible && safariVersion >= 12.1,
        chromeStandardLikeStat: isChromeCompatible && chromeVersion > 77,
        dropTransportCcFromAudio: isFirefoxCompatible,
        hasSenderParameters:
            typeof RTCPeerConnection !== 'undefined' &&
            typeof RTCPeerConnection.prototype.getSenders === 'function' &&
            typeof RTCRtpSender.prototype.getParameters === 'function' &&
            typeof RTCRtpSender.prototype.setParameters === 'function',
        isAudioRedSupported:
            typeof RTCRtpSender !== 'undefined' &&
            typeof RTCRtpSender.getCapabilities === 'function' &&
            RTCRtpSender.getCapabilities('audio').codecs.some(
                codec => codec.mimeType === 'audio/red'
            ),
    };

    if (capabilities.chromeStandardLikeStat) {
        //We know for sure that Chrome up to 77 supports old stat
        //This is to check that old stat is still supported for future versions of Chrome
        //If it is supported we will prefer old stat, because it has more data than the standard one as for now
        isPreWebRCT1ChromeStatStillSupported().then(isOldStatStillSupported => {
            if (isOldStatStillSupported) {
                capabilities.chromeStandardLikeStat = false;
                // eslint-disable-next-line no-console
                console.log('Old webrtc stat is still supported. Will use it');
            } else {
                // eslint-disable-next-line no-console
                console.log('Old webrtc stat is not supported. Keep using standard');
            }
        });
    }

    return capabilities;

    function isPreWebRCT1ChromeStatStillSupported() {
        const tempPC = new RTCPeerConnection();
        return new Promise(resolve => {
            try {
                tempPC
                    .getStats(result => {
                        resolve(!!result && result[Symbol.toStringTag] === 'RTCStatsResponse');
                    })
                    .catch(() => {
                        resolve(false);
                    });
            } catch (err) {
                resolve(false);
            }
        }).finally(() => {
            tempPC.close();
        });
    }
})();
