function assert(condition, description) {
    if (!condition) {
        throw new Error(description || 'Assert failed');
    }
}

export default assert;
