function u(e) {
  // n.call(this),
  this.events = {
      beforeLogin: "beforeLogin",
      loginSuccess: "loginSuccess",
      loginError: "loginError",
      beforeRefresh: "beforeRefresh",
      refreshSuccess: "refreshSuccess",
      refreshError: "refreshError",
      beforeLogout: "beforeLogout",
      logoutSuccess: "logoutSuccess",
      logoutError: "logoutError",
      rateLimitError: "rateLimitError"
  },
  e = e || {},
  this._server = e.server,
  this._appKey = e.appKey,
  this._appSecret = e.appSecret,
  this._redirectUri = e.redirectUri || "",
  this._refreshDelayMs = e.refreshDelayMs || 100,
  this._clearCacheOnRefreshError = void 0 === e.clearCacheOnRefreshError || e.clearCacheOnRefreshError,
  this._userAgent = (e.appName ? e.appName + (e.appVersion ? "/" + e.appVersion : "") + " " : "") + "RCJSSDK/" //+ a.version,
  this._externals = e.externals,
  this._cache = e.cache,
  this._client = e.client,
  this._knownPrefixes = e.knownPrefixes || u._knownPrefixes,
  this._refreshPromise = null,
  this._auth = new o({
      cache: this._cache,
      cacheId: u._cacheId,
      refreshHandicapMs: e.refreshHandicapMs
  })
}
u._urlPrefix = "/restapi",
u._apiVersion = "v1.0",
u._knownPrefixes = ["/rcvideo"],
u._tokenEndpoint = "/restapi/oauth/token",
u._revokeEndpoint = "/restapi/oauth/revoke",
u._authorizeEndpoint = "/restapi/oauth/authorize",
u._cacheId = "platform",
// u.prototype = Object.create(n.prototype),
u.prototype.delay = function(e) {
  return new this._externals.Promise((function(t, A) {
      setTimeout((function() {
          t(null)
      }
      ), e)
  }
  ))
}
,
u.prototype.auth = function() {
  return this._auth
}
,
u.prototype.client = function() {
  return this._client
}
,
u.prototype.createUrl = function(e, t) {
  t = t || {};
  var A = ""
    , n = -1 != (e = e || "").indexOf("http://") || -1 != e.indexOf("https://")
    , r = this._knownPrefixes.some((function(t) {
      return 0 === e.indexOf(t)
  }
  ));
  return t.addServer && !n && (A += this._server),
  -1 != e.indexOf(u._urlPrefix) || n || r || (A += u._urlPrefix + "/" + u._apiVersion),
  A += e,
  (t.addMethod || t.addToken) && (A += e.indexOf("?") > -1 ? "&" : "?"),
  t.addMethod && (A += "_method=" + t.addMethod),
  t.addToken && (A += (t.addMethod ? "&" : "") + "access_token=" + this._auth.accessToken()),
  A
}
,
u.prototype.loginUrl = function(e) {
  return e = e || {},
  this.createUrl(u._authorizeEndpoint + "?" + r.stringify({
      response_type: e.implicit ? "token" : "code",
      redirect_uri: e.redirectUri || this._redirectUri,
      client_id: this._appKey,
      state: e.state || "",
      brand_id: e.brandId || "",
      display: e.display || "",
      prompt: e.prompt || ""
  }), {
      addServer: !0
  })
}
,
u.prototype.parseLoginRedirect = function(e) {
  function t(e, t) {
      return e.split(t).reverse()[0]
  }
  var A = 0 === e.indexOf("#") && t(e, "#") || 0 === e.indexOf("?") && t(e, "?") || null;
  if (!A)
      throw new Error("Unable to parse response");
  var n = r.parse(A);
  if (!n)
      throw new Error("Unable to parse response");
  var i = n.error_description || n.error;
  if (i) {
      var o = new Error(i);
      throw o.error = n.error,
      o
  }
  return n
}
,
u.prototype.loginWindow = function(e) {
  return new this._externals.Promise(function(t, A) {
      if ("undefined" == typeof window)
          throw new Error("This method can be used only in browser");
      if (!e.url)
          throw new Error("Missing mandatory URL parameter");
      (e = e || {}).width = e.width || 400,
      e.height = e.height || 600,
      e.origin = e.origin || window.location.origin,
      e.property = e.property || a.authResponseProperty,
      e.target = e.target || "_blank";
      var n = void 0 !== window.screenLeft ? window.screenLeft : screen.left
        , r = void 0 !== window.screenTop ? window.screenTop : screen.top
        , i = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width
        , o = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height
        , s = i / 2 - e.width / 2 + n
        , u = o / 2 - e.height / 2 + r
        , c = window.open(e.url, "_blank", "_blank" == e.target ? "scrollbars=yes, status=yes, width=" + e.width + ", height=" + e.height + ", left=" + s + ", top=" + u : "");
      if (!c)
          throw new Error("Could not open login window. Please allow popups for this site");
      c.focus && c.focus();
      var g = window.addEventListener ? "addEventListener" : "attachEvent"
        , l = "addEventListener" == g ? "removeEventListener" : "detachEvent"
        , f = "addEventListener" == g ? "message" : "onmessage"
        , p = function(n) {
          try {
              if (n.origin != e.origin)
                  return;
              if (!n.data || !n.data[e.property])
                  return;
              c.close(),
              window[l](f, p);
              var r = this.parseLoginRedirect(n.data[e.property]);
              if (!r.code && !r.access_token)
                  throw new Error("No authorization code or token");
              t(r)
          } catch (n) {
              A(n)
          }
      }
      .bind(this);
      window[g](f, p, !1)
  }
  .bind(this))
}
,
u.prototype.loggedIn = function() {
  return this.ensureLoggedIn().then((function() {
      return !0
  }
  )).catch((function() {
      return !1
  }
  ))
}
,
u.prototype.login = function(e) {
  return new this._externals.Promise(function(t) {
      e = e || {},
      this.emit(this.events.beforeLogin);
      var A = {};
      if (e.access_token)
          return t(e);
      e.code ? e.code && (A.grant_type = "authorization_code",
      A.code = e.code,
      A.redirect_uri = e.redirectUri || this._redirectUri) : (A.grant_type = "password",
      A.username = e.username,
      A.password = e.password,
      A.extension = e.extension || ""),
      e.endpointId && (A.endpoint_id = e.endpointId),
      e.accessTokenTtl && (A.access_token_ttl = e.accessTokenTtl),
      e.refreshTokenTtl && (A.refresh_token_ttl = e.refreshTokenTtl),
      t(this._tokenRequest(u._tokenEndpoint, A))
  }
  .bind(this)).then(function(e) {
      var t = e.json ? e : null
        , A = t && t.json() || e;
      return this._auth.setData(A),
      this.emit(this.events.loginSuccess, t),
      t
  }
  .bind(this)).catch(function(e) {
      throw this._clearCacheOnRefreshError && this._cache.clean(),
      this.emit(this.events.loginError, e),
      e
  }
  .bind(this))
}
,
u.prototype._refresh = function() {
  return this.delay(this._refreshDelayMs).then(function() {
      if (this.emit(this.events.beforeRefresh),
      !this._auth.refreshToken())
          throw new Error("Refresh token is missing");
      if (!this._auth.refreshTokenValid())
          throw new Error("Refresh token has expired");
      return this._tokenRequest(u._tokenEndpoint, {
          grant_type: "refresh_token",
          refresh_token: this._auth.refreshToken(),
          access_token_ttl: this._auth.data().expires_in + 1,
          refresh_token_ttl: this._auth.data().refresh_token_expires_in + 1
      })
  }
  .bind(this)).then(function(e) {
      var t = e.json();
      if (!t.access_token)
          throw this._client.makeError(new Error("Malformed OAuth response"), e);
      return this._auth.setData(t),
      this.emit(this.events.refreshSuccess, e),
      e
  }
  .bind(this)).catch(function(e) {
      throw e = this._client.makeError(e),
      this._clearCacheOnRefreshError && this._cache.clean(),
      this.emit(this.events.refreshError, e),
      e
  }
  .bind(this))
}
,
u.prototype.refresh = function() {
  return this._refreshPromise || (this._refreshPromise = this._refresh().then(function(e) {
      return this._refreshPromise = null,
      e
  }
  .bind(this)).catch(function(e) {
      throw this._refreshPromise = null,
      e
  }
  .bind(this))),
  this._refreshPromise
}
,
u.prototype.logout = function() {
  return new this._externals.Promise(function(e) {
      this.emit(this.events.beforeLogout),
      e(this._tokenRequest(u._revokeEndpoint, {
          token: this._auth.accessToken()
      }))
  }
  .bind(this)).then(function(e) {
      return this._cache.clean(),
      this.emit(this.events.logoutSuccess, e),
      e
  }
  .bind(this)).catch(function(e) {
      throw this.emit(this.events.logoutError, e),
      e
  }
  .bind(this))
}
,
u.prototype.inflateRequest = function(e, t) {
  return (t = t || {}).skipAuthCheck ? this._externals.Promise.resolve(e) : this.ensureLoggedIn().then(function() {
      return e.headers.set("X-User-Agent", this._userAgent),
      e.headers.set("Client-Id", this._appKey),
      e.headers.set("Authorization", this._authHeader()),
      e
  }
  .bind(this))
}
,
u.prototype.sendRequest = function(e, t) {
  return this.inflateRequest(e, t).then(function(e) {
      return t = t || {},
      this._client.sendRequest(e)
  }
  .bind(this)).catch(function(e) {
      if (!e.apiResponse || !e.apiResponse.response() || t.retry)
          throw e;
      var A = e.apiResponse.response()
        , n = A.status;
      if (n != s._unauthorizedStatus && n != s._rateLimitStatus)
          throw e;
      t.retry = !0;
      var r = 0;
      if (n == s._unauthorizedStatus && this._auth.cancelAccessToken(),
      n == s._rateLimitStatus) {
          var i = t.handleRateLimit && "boolean" != typeof t.handleRateLimit ? t.handleRateLimit : 60;
          if (r = 1e3 * parseFloat(A.headers.get("retry-after") || i),
          e.retryAfter = r,
          this.emit(this.events.rateLimitError, e),
          !t.handleRateLimit)
              throw e
      }
      return this.delay(r).then(function() {
          return this.sendRequest(this._client.createRequest(t), t)
      }
      .bind(this))
  }
  .bind(this))
}
,
u.prototype.send = function(e) {
  return (e = e || {}).url = this.createUrl(e.url, {
      addServer: !0
  }),
  this.sendRequest(this._client.createRequest(e), e)
}
,
u.prototype.get = function(e, t, A) {
  return this.send(i({}, {
      method: "GET",
      url: e,
      query: t
  }, A))
}
,
u.prototype.post = function(e, t, A, n) {
  return this.send(i({}, {
      method: "POST",
      url: e,
      query: A,
      body: t
  }, n))
}
,
u.prototype.put = function(e, t, A, n) {
  return this.send(i({}, {
      method: "PUT",
      url: e,
      query: A,
      body: t
  }, n))
}
,
u.prototype.delete = function(e, t, A) {
  return this.send(i({}, {
      method: "DELETE",
      url: e,
      query: t
  }, A))
}
,
u.prototype.ensureLoggedIn = function() {
  return this._isAccessTokenValid() ? this._externals.Promise.resolve() : this.refresh()
}
,
u.prototype._tokenRequest = function(e, t) {
  return this.send({
      url: e,
      skipAuthCheck: !0,
      body: t,
      method: "POST",
      headers: {
          Authorization: "Basic " + this._apiKey(),
          "Content-Type": s._urlencodedContentType
      }
  })
}
,
u.prototype._isAccessTokenValid = function() {
  return this._auth.accessTokenValid()
}
,
u.prototype._apiKey = function() {
  var e = this._appKey + ":" + this._appSecret;
  return "function" == typeof btoa ? btoa(e) : new Buffer(e).toString("base64")
}
,
u.prototype._authHeader = function() {
  var e = this._auth.accessToken();
  return this._auth.tokenType() + (e ? " " + e : "")
}
,
e.exports = u