interface RCTConference {
    streams: {
        id: string
        justCreated: boolean
    }[]
    on(event: string, cb: Function): void
    join(opts: { bridgeParams: any, participantParams: any, sessionParams: any, streamParams: any }): ConferenceJoinResponse
}

type ConferenceJoinResponse = {
  meeting: {
      id: string,
      allowDials: boolean,
      allowJoin: boolean,
      allowUnmute: boolean,
      allowUnmuteVideo: boolean,
      allowScreenSharing: boolean,
      allowAnnotations: boolean,
      allowChats: boolean,
      bridgeId: string,
      cloudRecordingsEnabled: boolean,
      durationLimit: number,
      mute: boolean,
      muteVideo: boolean,
      startTime: string,
      wsConnectionUrl: string,
      closedCaptionsEnabled: boolean,
      e2ee: boolean,
      roomId: string,
      roomName: string,
      features: {
          e2ee: boolean,
          chat: boolean,
          transcription: boolean
      }
  },
  participants: {},
  streams: {
      [string]: {
          id: string,
          audio: {
              isActiveIn: boolean,
              isActiveOut: boolean
          },
          participantId: string,
          sessionId: string,
          startTime: string,
          type: string,
          video: {
              isActiveIn: boolean,
              isActiveOut: boolean
          },
          isSessionInactive: boolean
      }
  },
  localParticipant: {
      id: string,
      allowUnmute: boolean,
      allowUnmuteVideo: boolean,
      callerInfo: {
          accountId: string,
          extensionId: string
      },
      deleteReason: null,
      displayName: string,
      joinTime: string,
      host: boolean,
      moderator: boolean,
      noanswerTime: null,
      serverMute: boolean,
      serverMuteVideo: boolean,
      ringing: boolean,
      shortPrtsPin: string,
      localMute: boolean,
      hasActiveSessions: boolean,
      localMuteVideo: boolean,
      joinedAudio: boolean,
      pstnSessionId: null,
      alreadyJoinedAudio: boolean,
      local: boolean
  },
  localSession: {
      id: string,
      localMute: boolean,
      localMuteVideo: boolean,
      notificationToken: string,
      notificationSubKey: string,
      participantId: string,
      pingInfo: {
          pingInterval: number
      },
      serverMute: boolean,
      serverMuteVideo: boolean,
      token: string,
      userAgent: string
  },
  localStreams: [
      {
          id: string,
          audio: {
              isActiveIn: boolean,
              isActiveOut: boolean
          },
          participantId: string,
          sessionId: string,
          startTime: string,
          type: string,
          video: {
              isActiveIn: boolean,
              isActiveOut: boolean
          },
          isSessionInactive: boolean
      }
  ],
  myDemands: null,
  targetedToMeDemands: null,
  recordings: null,
  notifications: null,
  myDials: null,
  channels: null,
  streamsRefs: {
      [string]: string[]
  },
  meetingParticipants: {},
  transportInfo: {
      notifyChannels: {
          meeting: {
              version: number,
              pingInterval: number,
              channel: string
          },
          meetingParticipants: {
              version: number,
              pingInterval: number,
              channel: string
          },
          private: {
              version: number,
              pingInterval: number,
              channel: string
          }
      },
      subscribeKey: string,
      authKey: string
  },
  participantBySessionMap: {
    [string]: string,
  }
}