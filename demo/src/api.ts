import PubNub from "pubnub"
import { ControlClient } from "libsfu"
import Conference from "librct/src/conference"
import * as data from "./data"

const SESSION_CACHE_NAME = "rc-platform"
const USER_CACHE_NAME = "user"

export async function login() {
  let user = JSON.parse(localStorage.getItem(USER_CACHE_NAME))
  let session = JSON.parse(localStorage.getItem(SESSION_CACHE_NAME))

  if (!user) {
    user = await createUser()
    localStorage.setItem(USER_CACHE_NAME, JSON.stringify(user))
  }

  if (session) {
    if (session.expire_time < Date.now())  {
      localStorage.removeItem(SESSION_CACHE_NAME)
      return login()
    }

    return { session, user }
  }
  
  const auth_token = "Basic QUpkS0J6SExSa2llOXBldURhWkxMdzpOREJWODdkblJ2R2Jnek9IaENsSEtBcUNZRGd0YmZSZ3V3Sk9ISktvblBHZw=="

  let endpointId = localStorage.getItem("endpointId")
  if (!endpointId) {
    endpointId = uuid()
    localStorage.setItem("endpointId", endpointId)
  }

  const fd = new FormData()
  fd.append("grant_type", "client_credentials")
  fd.append("brand_id", "1210")
  fd.append("client_id", "AJdKBzHLRkie9peuDaZLLw")
  fd.append("endpoint_id", endpointId)

  const now = Date.now()

  session = await fetch(
    "https://api-meet.ringcentral.com/restapi/oauth/token",
    {
      method: "POST",
      headers: { 'authorization': auth_token },
      body: fd,
    }
  )
  .then(r => r.json())

  session.expire_time = now + session.expires_in * 1000;

  localStorage.setItem(SESSION_CACHE_NAME, JSON.stringify(session))

  return { session, user }
}

async function createUser() {
  return await fetch(
    "https://fakercloud.com/api/v1/schema/xQRKavMh?rows=1",
    { headers: { "X-API-Key": 'O7tNcVO6' } },
  )
  .then(response => response.json())
  .then(response => response.rows[0])
}

function uuid() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (e=>{
    const t = 16 * Math.random() | 0;
    return ("x" === e ? t : 3 & t | 8).toString(16)
  }))
}

const defaultSFUOptions = {
  sessionId: "", // from librct
  sessionToken: "", // from librct
  conferenceId: "", // unused
  autoRecovery: true,
  // operationTimeout: 5000, // default
  controlChannel: true,
  logger: {
    debug() {},
    info() {},
    log() {},
    warn: console.warn,
    error: console.error,
  },
  codec: data.prefs.codec,
  // codecForP2P: SDP.codec.H264, // default
  codecForScreenSharing: data.prefs.sharingCodec,
  // audioRate: "48000", // default
  audioBitrate: data.SFUConfig.audioBitrate,
  // audioRateScreenSharing: "48000", // default
  audioBitrateScreenSharing: data.SFUConfig.audioBitrateScreenSharing,
  meta:{
    shortId: "505754158127",
    username: "Host user",
    xUserAgent: "RCAppRCVWeb/21.3.26.0 (RingCentral; macos/10.15; rev.d2c97357)",
  },
  // forceTransport: false, // deafult
  // blacklistedCodecs: [], // default
  // recentActivityDelay: 60000, //default
  server: "", // from librct
  serverPath: "", // from librct
  serverProtocol: "wss:",
  serverSubProtocol: data.SFUConfig.serverSubProtocol,
  iceServers: data.SFUConfig.iceServers,
  // keepAliveInterval: 5000, // deafult
  // streamRecoveryTimeout: 30000, //default
  maxRemoteVideo: [{ quality: 1, slots: 8 }],
  maxRemoteAudio: 8, 
  maxP2PConnections: 0, // same as default
  // enableComfortNoise: false, //default
  // comfortNoiseAudioContext: <new AudioContext> //default
  // onBeforeSessionCreate // none
  preferUnifiedPlan: false, // same as default
  // setSendBitrate: true // dafault
  mixAudioMode: data.SFUConfig.mixAudioMode,
  negotiateVideoMode: data.SFUConfig.negotiateVideoMode,
  noMediaPingTimeout: data.SFUConfig.noMediaPingTimeout,
  // eventLoopInterval: 200 // deafult
  bitrateMultipliers: {},
  agc: true, // same as default
  noiseGate: true, // same as default
  enableQ1HalfFps: data.SFUConfig.enableQ1HalfFps,
  // createWebSocket, // none
  allowAudioRed: false,
  disableDSCP: false,
  e2ee: {
    enabled: false
  } // undocummented
}

export async function joinMeeting({ bridgeId, displayName }) {
  const conference = new Conference({
    ...data.librctDefaultOpts,
    fetch: fetchWithAccessToken,
    origin: "https://api-meet.ringcentral.com",
    pubnub: PubNub,
    pubnubOrigin: "ringcentral.pubnubapi.com",
    e2eeEnabled: false,
    disablePongs: true,
    isPrivateChatsEnabled: true,
  })

  const response = await conference.join({
    bridgeParams: { shortId: bridgeId },
    participantParams: { displayName },
    sessionParams: {
      userAgent: "rcv/web/0.10",
      operationSystem: "macos",
      localMute: false,
      localMuteVideo: false,
    },
    streamParams: {
      audio: {
        isActiveIn: false,
        isActiveOut: false,
      }
    },
  })

  const wsConnectionUrl = new URL(response.meeting.wsConnectionUrl)

  const options = {
    ...defaultSFUOptions,
    sessionId: response.localSession.id,
    sessionToken: response.localSession.token,
    server: wsConnectionUrl.hostname,
    serverPath: wsConnectionUrl.pathname.replace("/", ""),
  }

  return {
    conference,
    response,
    options,
    stream: response.localStreams[0],
  }
}

export async function startStreaming({
  stream,
  options,
  getStream,
  onLocalStream = noop,
  onRemoteStream = noop,
}: any) {
  const wcam = await getStream()

  const client = new ControlClient(options)
  
  client.on(ControlClient.event.LOCAL_STREAM_ADDED, (...args) => onLocalStream("added", ...args))
  client.on(ControlClient.event.REMOTE_STREAM_ADDED, (...args) =>  onRemoteStream("added", ...args))
  client.on(ControlClient.event.REMOTE_STREAM_REPLACED, (...args) =>  onRemoteStream("replaced", ...args))
  client.on(ControlClient.event.REMOTE_STREAM_REMOVED, (...args) =>  onRemoteStream("removed", ...args))
  
  await new Promise(res => client.on(ControlClient.event.SESSION_CREATED, res))

  client.addLocalStream({
    tapId: stream.id,
    stream: wcam,
    useSimulcast: true,
  })
}

function noop() {}

async function fetchWithAccessToken(url, options) {
  const session = JSON.parse(localStorage.getItem(SESSION_CACHE_NAME))

  if (!session) throw new Error(`No '${SESSION_CACHE_NAME}' is found in local storage`)

  options.headers["Authorization"] = `Bearer ${session.access_token}`
  return fetch(url, options)
}