export const SFUConfig = {
  "url": "ws-rcv.ringcentral.com",
  "maintenancePort": "4443",
  "maintenanceUrl": "ws-rcv.ringcentral.com:4443",
  "serverPath": "sfu",
  "serverProtocol": "wss:",
  "serverSubProtocol": undefined,
  "audioSlotsCnt": 3,
  "videoSlotsCnt": 16,
  "volumeThreshold": 100,
  "useSimulcast": true,
  "useSharingSimulcastWeb": true,
  "useSharingSimulcastApplication": false,
  "mixAudioMode": "auto",
  "negotiateVideoMode": "auto",
  "noMediaPingTimeout": 0,
  "audioBitrate": 32000,
  "audioBitrateScreenSharing": 128000,
  "enableQ1HalfFps": true,
  "iceServers": [
      {
          "urls": [
              "stun:stun1.ringcentral.com:19302",
              "stun:stun2.ringcentral.com:19302"
          ]
      },
      {
          "urls": "stun:stun.l.google.com:19302"
      }
  ]
}

export const prefs = {
  codec: "VP8/90000",
  disableServerAgc: false,
  p2pFactor: 0,
  sharingBitrateMultiplier: null,
  sharingCodec: "VP8/90000",
  useSharingSimulcast: true,
  videoBitrateMultiplier: null,
}

export const rcPlatform = {
  "token_type": "bearer",
  "access_token": "UERYMDhQMjFQQVMwMHxBQUJWSExrc0doQkhtMnpSZEdTOFVVbVQtckx3RzBCS1dNUnBBdTR6eFMzckpLRU1VdDBSZHk3UUo4OGI0VmVNTGxVSE1hY1VPYlk1V0VrYmFjeXdRcGVSSXVab1g0VDVOdGlkV0NDbnBycHEtQlJXZ05JRGtwR3ZlUUlvVlhvOFNhTWxVSzZUbVVOMVFkWXFjdWRMcEQ0RnVPWHRnbHI3aVZva0JER0dzT21aWUxMci1ZaklLT2dLUVRPTlNkSlYyaXhqSDNoRjEyZ2RTNWFJcjVXc28yUGRzQldzZEs0Skt2RTZ3d3xHeldwTmd8SXFENXhjb0paUHFfQ0VYT2FGazQ0UXxBUQ",
  "expires_in": 3600,
  "refresh_token": "UERYMDhQMjFQQVMwMHxBQUJpWlQ5Vng0MjB1SVR3WDltc2ZLRVVqQ3BxU2RhUm15Qzc4SjdzbUNrbUx6cjc0ay1YOENzdGdGMVJ1SW5YbERmM0hOLVRlQ2lJM1BCaWJqY0tGTzYzYVVDWGpkRk9UTUVGOFJ6Qi1yMUJqZ183Y1lfaFlkY0JnMjBsNVVqdUxMcjI4S25SeHcydXdkU3RhTWxIVncwZjZoSjNwOEF4YzFIV3k2YkVBYlNvNnRCMjAxUEc0a21jWmlkeGtfcm5kMWZWNWVnVDVvaC1Ldm5xbmhxUXd3UnE4SDBDcnRQY2lDMEpxQXxHeldwTmd8a0ZqZmpoZVB5WDlITndySlgxcG9rZ3xBUQ",
  "refresh_token_expires_in": 604800,
  "expire_time": 1632401298481,
  "refresh_token_expire_time": 1633002498481,
  "scope": "ReadAccounts ControlWebinars Meetings TeamMessaging Glip SendNotifications SendUsageInfo WebSocket",
  "owner_id": "1240601044",
  "endpoint_id": "OdQi_rZNTDqhSQeKq3cHHA"
}

export const meetingDomain = "https://api-meet.ringcentral.com"

export const librctDefaultOpts = {
  apiVersion: 'v1',
  maintenance: false, // maintenance mode
  maintenancePath: 'maintenance',
  chatsListRecoveryTimeout: 30000,
  chatMessagesRecoveryTimeout: 3 * 60 * 1000,
  isPrivateChatsEnabled: false,
  disablePongs: false,
}