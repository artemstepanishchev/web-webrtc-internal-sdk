var e, t;

function r() {
  return new Promise((e => {
    const t = new FileReader;
    t.onload = () => e(t.result), t.readAsArrayBuffer(this)
  }))
}
"undefined" != typeof File && (null != (e = File.prototype).arrayBuffer || (e.arrayBuffer = r)), "undefined" != typeof Blob && (null != (t = Blob.prototype).arrayBuffer || (t.arrayBuffer = r));
let n = 0;
const o = () => n++;
const a = new("undefined" != typeof window ? class extends Worker {
    constructor() {
      const e = URL.createObjectURL(new Blob(["onmessage = ({ data }) => {\n    const { id, timeout } = data\n    setTimeout(postMessage, timeout, { id })\n}\n"]));
      super(e), URL.revokeObjectURL(e)
    }
  } : class {
    constructor() {
      throw new Error("The environment does not support Worker API")
    }
  }),
  i = new Map,
  u = (e, t) => {
    const r = o();
    return i.set(r, e), a.postMessage({
      id: r,
      timeout: t
    }), r
  };
a.onmessage = ({
  data: e
}) => {
  const {
    id: t
  } = e, r = i.get(t);
  i.delete(t), r()
};
const s = [];
let c = 0;
var l = Object.freeze({
  __proto__: null,
  [Symbol.toStringTag]: "Module",
  setTimeout: u,
  requestAnimationFrame: e => {
    const t = o();
    if (0 === s.length) {
      const e = Date.now();
      u((() => {
        const e = c = Date.now(),
          t = [...s];
        s.length = 0, t.forEach((t => t(e)))
      }), 16.666666666666668 - (e - c) % 16.666666666666668)
    }
    return s.push(e), t
  }
});
const f = () => "visible" === document.visibilityState ? window : l,
  d = (e, t) => f().setTimeout(e, t),
  p = e => f().requestAnimationFrame(e),
  m = new WeakMap,
  h = async e => {
    var t, r;
    const n = null != (t = e.videoWidth) ? t : e.width,
      o = null != (r = e.videoHeight) ? r : e.height;
    m.has(e) || m.set(e, function (e, t) {
      let r;
      const n = !!(null != r ? r : r = v("webgl2", e, t)),
        o = !!(null != r ? r : r = v("webgl", e, t));
      let a, i;
      null != r || (r = v("2d", e, t));
      const u = r;
      if (a = r => u.drawImage(r, 0, 0, e, t), i = () => u.getImageData(0, 0, e, t), !o) return {
        width: e,
        height: t,
        drawImage: a,
        getImageData: i
      };
      let s = new Uint8ClampedArray(e * t * 4);
      const c = r,
        l = c.createTexture(),
        f = c.createFramebuffer();

      function p() {
        return new Promise((e => d(e)))
      }
      if (c.bindTexture(c.TEXTURE_2D, l), c.bindFramebuffer(c.FRAMEBUFFER, f), c.framebufferTexture2D(c.FRAMEBUFFER, c.COLOR_ATTACHMENT0, c.TEXTURE_2D, l, 0), a = e => {
          c.texImage2D(c.TEXTURE_2D, 0, c.RGBA, c.RGBA, c.UNSIGNED_BYTE, e)
        }, i = async () => (await p(), c.readPixels(0, 0, e, t, c.RGBA, c.UNSIGNED_BYTE, s), {
          data: s,
          width: e,
          height: t
        }), !n) return {
        width: e,
        height: t,
        drawImage: a,
        getImageData: i
      };
      const m = c,
        h = m.createBuffer();
      async function g() {
        const e = m.fenceSync(m.SYNC_GPU_COMMANDS_COMPLETE, 0);
        m.flush();
        const t = () => {
          const t = m.clientWaitSync(e, 0, 0);
          return t === m.ALREADY_SIGNALED || t === m.CONDITION_SATISFIED
        };
        for (; !t();) await new Promise((e => d(e)));
        m.deleteSync(e)
      }
      return m.bindBuffer(m.PIXEL_PACK_BUFFER, h), m.bufferData(m.PIXEL_PACK_BUFFER, s.length, m.STREAM_READ), a = e => {
        m.texImage2D(m.TEXTURE_2D, 0, m.RGBA, m.RGBA, m.UNSIGNED_BYTE, e)
      }, i = async () => (m.readPixels(0, 0, e, t, c.RGBA, c.UNSIGNED_BYTE, 0), await g(), m.getBufferSubData(m.PIXEL_PACK_BUFFER, 0, s), {
        data: s,
        width: e,
        height: t
      }), {
        width: e,
        height: t,
        drawImage: a,
        getImageData: i
      }
    }(n, o));
    const a = m.get(e);
    return a.width !== n || a.height !== o ? (m.delete(e), h(e)) : (await a.drawImage(e), await a.getImageData())
  };

function v(e, t, r) {
  let n;
  "undefined" == typeof OffscreenCanvas ? (n = document.createElement("canvas"), n.width = t, n.height = r) : n = new OffscreenCanvas(t, r);
  return n.getContext(e, {
    alpha: !1,
    desynchronized: !0,
    antialias: !1,
    depth: !1,
    powerPreference: "high-performance",
    premultipliedAlpha: !1,
    preserveDrawingBuffer: !1,
    stencil: !1
  })
}
const g = async (e, t = {}) => new Promise((r => {
  const n = document.createElement("video");
  n.muted = !0, n.playsInline = !0, Object.assign(n, t), e instanceof globalThis.MediaStream ? (n.srcObject = e, n.addEventListener("ended", (() => n.srcObject = null))) : (n.src = "string" == typeof e ? e : URL.createObjectURL(e), n.addEventListener("ended", (() => URL.revokeObjectURL(n.src)))), n.style.position = "fixed", n.style.zIndex = "-9999999", n.style.opacity = "0.0000000001", document.body.appendChild(n);
  const o = setInterval((() => n.readyState), 300);
  n.addEventListener("play", (e => clearInterval(o)), {
    once: !0
  }), n.addEventListener("play", (e => r(n)), {
    once: !0
  }), n.addEventListener("loadedmetadata", (e => n.play()), {
    once: !0
  })
}));
class y {
  constructor(e) {
    this._src = "string" == typeof e ? e : URL.createObjectURL(e)
  }
  async *[Symbol.asyncIterator]() {
    const e = document.createElement("img");
    for (await new Promise(((t, r) => {
        e.onload = t, e.onerror = r, e.src = this._src
      }));;) yield h(e)
  }
}
const b = class {
  constructor(e) {
    return this._video = null, this._stream = e, b.cache.has(e) || b.cache.set(e, this), b.cache.get(e)
  }
  async *[Symbol.asyncIterator]() {
    var e;
    const t = null != (e = this._video) ? e : this._video = await g(this._stream);
    for (t.addEventListener("ended", (e => t.remove()), {
        once: !0
      }), this._stream.addEventListener("inactive", (e => t.dispatchEvent(new CustomEvent("ended"))), {
        once: !0
      }); this._stream.active;) yield h(t)
  }
  stop() {
    for (const e of this._stream.getVideoTracks()) e.stop()
  }
};
let w = b;
w.cache = new WeakMap;
const _ = {
  loop: !1
};
class E {
  constructor(e, t) {
    this._video = null, this._options = {
      ..._,
      ...t
    }, this._src = "string" == typeof e ? e : URL.createObjectURL(e)
  }
  async *[Symbol.asyncIterator]() {
    var e;
    const t = null != (e = this._video) ? e : this._video = await g(this._src, this._options);
    for (t.addEventListener("pause", (e => t.remove()), {
        once: !0
      }), t.addEventListener("ended", (e => t.remove()), {
        once: !0
      }); !t.paused && !t.ended;) yield h(t)
  }
  stop() {
    var e;
    null == (e = this._video) || e.pause()
  }
}
const C = "undefined" != typeof screen && screen.height > screen.width,
  k = {
    facingMode: "user",
    width: C ? void 0 : {
      min: 640,
      ideal: 1280,
      max: 1920
    },
    height: C ? void 0 : {
      min: 480,
      ideal: 720,
      max: 1080
    },
    resizeMode: {
      ideal: "crop-and-scale"
    }
  };
class x {
  constructor(e) {
    this._stream = null, this._constraints = {
      ...k,
      ...e
    }
  }
  async start() {
    return null != this._stream || (this._stream = await S(this._constraints)), this
  }
  async *[Symbol.asyncIterator]() {
    var e;
    const t = null != (e = this._stream) ? e : this._stream = await S(this._constraints);
    yield* t
  }
  stop() {
    this._stream && (this._stream.stop(), this._stream = null)
  }
}
const S = async e => {
  if (void 0 === navigator.mediaDevices) throw new Error("SecureContext is required to access webcam\nIt's likely you need to set up HTTPS/TLS for your website\nSee https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia#Encryption_based_security for details ");
  return new w(await navigator.mediaDevices.getUserMedia({
    video: e
  }))
};
var F, D, P, T, A, L = {
  exports: {}
};
L.exports = P = {}, P.parse = function (e, t) {
    for (var r = P.bin.readUshort, n = P.bin.readUint, o = 0, a = {}, i = new Uint8Array(e), u = i.length - 4; 101010256 != n(i, u);) u--;
    o = u, o += 4;
    var s = r(i, o += 4);
    r(i, o += 2);
    var c = n(i, o += 2),
      l = n(i, o += 4);
    o += 4, o = l;
    for (var f = 0; f < s; f++) {
      n(i, o), o += 4, o += 4, o += 4, n(i, o += 4), c = n(i, o += 4);
      var d = n(i, o += 4),
        p = r(i, o += 4),
        m = r(i, o + 2),
        h = r(i, o + 4);
      o += 6;
      var v = n(i, o += 8);
      o += 4, o += p + m + h, P._readLocal(i, v, a, c, d, t)
    }
    return a
  }, P._readLocal = function (e, t, r, n, o, a) {
    var i = P.bin.readUshort,
      u = P.bin.readUint;
    u(e, t), i(e, t += 4), i(e, t += 2);
    var s = i(e, t += 2);
    u(e, t += 2), u(e, t += 4), t += 4;
    var c = i(e, t += 8),
      l = i(e, t += 2);
    t += 2;
    var f = P.bin.readUTF8(e, t, c);
    if (t += c, t += l, a) r[f] = {
      size: o,
      csize: n
    };
    else {
      var d = new Uint8Array(e.buffer, t);
      if (0 == s) r[f] = new Uint8Array(d.buffer.slice(t, t + n));
      else {
        if (8 != s) throw "unknown compression method: " + s;
        var p = new Uint8Array(o);
        P.inflateRaw(d, p), r[f] = p
      }
    }
  }, P.inflateRaw = function (e, t) {
    return P.F.inflate(e, t)
  }, P.inflate = function (e, t) {
    return e[0], e[1], P.inflateRaw(new Uint8Array(e.buffer, e.byteOffset + 2, e.length - 6), t)
  }, P.deflate = function (e, t) {
    null == t && (t = {
      level: 6
    });
    var r = 0,
      n = new Uint8Array(50 + Math.floor(1.1 * e.length));
    n[r] = 120, n[r + 1] = 156, r += 2, r = P.F.deflateRaw(e, n, r, t.level);
    var o = P.adler(e, 0, e.length);
    return n[r + 0] = o >>> 24 & 255, n[r + 1] = o >>> 16 & 255, n[r + 2] = o >>> 8 & 255, n[r + 3] = o >>> 0 & 255, new Uint8Array(n.buffer, 0, r + 4)
  }, P.deflateRaw = function (e, t) {
    null == t && (t = {
      level: 6
    });
    var r = new Uint8Array(50 + Math.floor(1.1 * e.length)),
      n = P.F.deflateRaw(e, r, n, t.level);
    return new Uint8Array(r.buffer, 0, n)
  }, P.encode = function (e, t) {
    null == t && (t = !1);
    var r = 0,
      n = P.bin.writeUint,
      o = P.bin.writeUshort,
      a = {};
    for (var i in e) {
      var u = !P._noNeed(i) && !t,
        s = e[i],
        c = P.crc.crc(s, 0, s.length);
      a[i] = {
        cpr: u,
        usize: s.length,
        crc: c,
        file: u ? P.deflateRaw(s) : s
      }
    }
    for (var i in a) r += a[i].file.length + 30 + 46 + 2 * P.bin.sizeUTF8(i);
    r += 22;
    var l = new Uint8Array(r),
      f = 0,
      d = [];
    for (var i in a) {
      var p = a[i];
      d.push(f), f = P._writeHeader(l, f, i, p, 0)
    }
    var m = 0,
      h = f;
    for (var i in a) p = a[i], d.push(f), f = P._writeHeader(l, f, i, p, 1, d[m++]);
    var v = f - h;
    return n(l, f, 101010256), f += 4, o(l, f += 4, m), o(l, f += 2, m), n(l, f += 2, v), n(l, f += 4, h), f += 4, f += 2, l.buffer
  }, P._noNeed = function (e) {
    var t = e.split(".").pop().toLowerCase();
    return -1 != "png,jpg,jpeg,zip".indexOf(t)
  }, P._writeHeader = function (e, t, r, n, o, a) {
    var i = P.bin.writeUint,
      u = P.bin.writeUshort,
      s = n.file;
    return i(e, t, 0 == o ? 67324752 : 33639248), t += 4, 1 == o && (t += 2), u(e, t, 20), u(e, t += 2, 0), u(e, t += 2, n.cpr ? 8 : 0), i(e, t += 2, 0), i(e, t += 4, n.crc), i(e, t += 4, s.length), i(e, t += 4, n.usize), u(e, t += 4, P.bin.sizeUTF8(r)), u(e, t += 2, 0), t += 2, 1 == o && (t += 2, t += 2, i(e, t += 6, a), t += 4), t += P.bin.writeUTF8(e, t, r), 0 == o && (e.set(s, t), t += s.length), t
  }, P.crc = {
    table: function () {
      for (var e = new Uint32Array(256), t = 0; t < 256; t++) {
        for (var r = t, n = 0; n < 8; n++) 1 & r ? r = 3988292384 ^ r >>> 1 : r >>>= 1;
        e[t] = r
      }
      return e
    }(),
    update: function (e, t, r, n) {
      for (var o = 0; o < n; o++) e = P.crc.table[255 & (e ^ t[r + o])] ^ e >>> 8;
      return e
    },
    crc: function (e, t, r) {
      return 4294967295 ^ P.crc.update(4294967295, e, t, r)
    }
  }, P.adler = function (e, t, r) {
    for (var n = 1, o = 0, a = t, i = t + r; a < i;) {
      for (var u = Math.min(a + 5552, i); a < u;) o += n += e[a++];
      n %= 65521, o %= 65521
    }
    return o << 16 | n
  }, P.bin = {
    readUshort: function (e, t) {
      return e[t] | e[t + 1] << 8
    },
    writeUshort: function (e, t, r) {
      e[t] = 255 & r, e[t + 1] = r >> 8 & 255
    },
    readUint: function (e, t) {
      return 16777216 * e[t + 3] + (e[t + 2] << 16 | e[t + 1] << 8 | e[t])
    },
    writeUint: function (e, t, r) {
      e[t] = 255 & r, e[t + 1] = r >> 8 & 255, e[t + 2] = r >> 16 & 255, e[t + 3] = r >> 24 & 255
    },
    readASCII: function (e, t, r) {
      for (var n = "", o = 0; o < r; o++) n += String.fromCharCode(e[t + o]);
      return n
    },
    writeASCII: function (e, t, r) {
      for (var n = 0; n < r.length; n++) e[t + n] = r.charCodeAt(n)
    },
    pad: function (e) {
      return e.length < 2 ? "0" + e : e
    },
    readUTF8: function (e, t, r) {
      for (var n, o = "", a = 0; a < r; a++) o += "%" + P.bin.pad(e[t + a].toString(16));
      try {
        n = decodeURIComponent(o)
      } catch (i) {
        return P.bin.readASCII(e, t, r)
      }
      return n
    },
    writeUTF8: function (e, t, r) {
      for (var n = r.length, o = 0, a = 0; a < n; a++) {
        var i = r.charCodeAt(a);
        if (0 == (4294967168 & i)) e[t + o] = i, o++;
        else if (0 == (4294965248 & i)) e[t + o] = 192 | i >> 6, e[t + o + 1] = 128 | i >> 0 & 63, o += 2;
        else if (0 == (4294901760 & i)) e[t + o] = 224 | i >> 12, e[t + o + 1] = 128 | i >> 6 & 63, e[t + o + 2] = 128 | i >> 0 & 63, o += 3;
        else {
          if (0 != (4292870144 & i)) throw "e";
          e[t + o] = 240 | i >> 18, e[t + o + 1] = 128 | i >> 12 & 63, e[t + o + 2] = 128 | i >> 6 & 63, e[t + o + 3] = 128 | i >> 0 & 63, o += 4
        }
      }
      return o
    },
    sizeUTF8: function (e) {
      for (var t = e.length, r = 0, n = 0; n < t; n++) {
        var o = e.charCodeAt(n);
        if (0 == (4294967168 & o)) r++;
        else if (0 == (4294965248 & o)) r += 2;
        else if (0 == (4294901760 & o)) r += 3;
        else {
          if (0 != (4292870144 & o)) throw "e";
          r += 4
        }
      }
      return r
    }
  }, P.F = {}, P.F.deflateRaw = function (e, t, r, n) {
    var o = [
        [0, 0, 0, 0, 0],
        [4, 4, 8, 4, 0],
        [4, 5, 16, 8, 0],
        [4, 6, 16, 16, 0],
        [4, 10, 16, 32, 0],
        [8, 16, 32, 32, 0],
        [8, 16, 128, 128, 0],
        [8, 32, 128, 256, 0],
        [32, 128, 258, 1024, 1],
        [32, 258, 258, 4096, 1]
      ][n],
      a = P.F.U,
      i = P.F._goodIndex;
    P.F._hash;
    var u = P.F._putsE,
      s = 0,
      c = r << 3,
      l = 0,
      f = e.length;
    if (0 == n) {
      for (; s < f;) u(t, c, s + (C = Math.min(65535, f - s)) == f ? 1 : 0), c = P.F._copyExact(e, s, C, t, c + 8), s += C;
      return c >>> 3
    }
    var d = a.lits,
      p = a.strt,
      m = a.prev,
      h = 0,
      v = 0,
      g = 0,
      y = 0,
      b = 0,
      w = 0;
    for (f > 2 && (p[w = P.F._hash(e, 0)] = 0), s = 0; s < f; s++) {
      if (b = w, s + 1 < f - 2) {
        w = P.F._hash(e, s + 1);
        var _ = s + 1 & 32767;
        m[_] = p[w], p[w] = _
      }
      if (l <= s) {
        (h > 14e3 || v > 26697) && f - s > 100 && (l < s && (d[h] = s - l, h += 2, l = s), c = P.F._writeBlock(s == f - 1 || l == f ? 1 : 0, d, h, y, e, g, s - g, t, c), h = v = y = 0, g = s);
        var E = 0;
        s < f - 2 && (E = P.F._bestMatch(e, s, m, b, Math.min(o[2], f - s), o[3]));
        var C = E >>> 16,
          k = 65535 & E;
        if (0 != E) {
          k = 65535 & E;
          var x = i(C = E >>> 16, a.of0);
          a.lhst[257 + x]++;
          var S = i(k, a.df0);
          a.dhst[S]++, y += a.exb[x] + a.dxb[S], d[h] = C << 23 | s - l, d[h + 1] = k << 16 | x << 8 | S, h += 2, l = s + C
        } else a.lhst[e[s]]++;
        v++
      }
    }
    for (g == s && 0 != e.length || (l < s && (d[h] = s - l, h += 2, l = s), c = P.F._writeBlock(1, d, h, y, e, g, s - g, t, c), h = 0, v = 0, h = v = y = 0, g = s); 0 != (7 & c);) c++;
    return c >>> 3
  }, P.F._bestMatch = function (e, t, r, n, o, a) {
    var i = 32767 & t,
      u = r[i],
      s = i - u + 32768 & 32767;
    if (u == i || n != P.F._hash(e, t - s)) return 0;
    for (var c = 0, l = 0, f = Math.min(32767, t); s <= f && 0 != --a && u != i;) {
      if (0 == c || e[t + c] == e[t + c - s]) {
        var d = P.F._howLong(e, t, s);
        if (d > c) {
          if (l = s, (c = d) >= o) break;
          s + 2 < d && (d = s + 2);
          for (var p = 0, m = 0; m < d - 2; m++) {
            var h = t - s + m + 32768 & 32767,
              v = h - r[h] + 32768 & 32767;
            v > p && (p = v, u = h)
          }
        }
      }
      s += (i = u) - (u = r[i]) + 32768 & 32767
    }
    return c << 16 | l
  }, P.F._howLong = function (e, t, r) {
    if (e[t] != e[t - r] || e[t + 1] != e[t + 1 - r] || e[t + 2] != e[t + 2 - r]) return 0;
    var n = t,
      o = Math.min(e.length, t + 258);
    for (t += 3; t < o && e[t] == e[t - r];) t++;
    return t - n
  }, P.F._hash = function (e, t) {
    return (e[t] << 8 | e[t + 1]) + (e[t + 2] << 4) & 65535
  }, P.saved = 0, P.F._writeBlock = function (e, t, r, n, o, a, i, u, s) {
    var c, l, f, d, p, m, h, v, g, y = P.F.U,
      b = P.F._putsF,
      w = P.F._putsE;
    y.lhst[256]++, l = (c = P.F.getTrees())[0], f = c[1], d = c[2], p = c[3], m = c[4], h = c[5], v = c[6], g = c[7];
    var _ = 32 + (0 == (s + 3 & 7) ? 0 : 8 - (s + 3 & 7)) + (i << 3),
      E = n + P.F.contSize(y.fltree, y.lhst) + P.F.contSize(y.fdtree, y.dhst),
      C = n + P.F.contSize(y.ltree, y.lhst) + P.F.contSize(y.dtree, y.dhst);
    C += 14 + 3 * h + P.F.contSize(y.itree, y.ihst) + (2 * y.ihst[16] + 3 * y.ihst[17] + 7 * y.ihst[18]);
    for (var k = 0; k < 286; k++) y.lhst[k] = 0;
    for (k = 0; k < 30; k++) y.dhst[k] = 0;
    for (k = 0; k < 19; k++) y.ihst[k] = 0;
    var x = _ < E && _ < C ? 0 : E < C ? 1 : 2;
    if (b(u, s, e), b(u, s + 1, x), s += 3, 0 == x) {
      for (; 0 != (7 & s);) s++;
      s = P.F._copyExact(o, a, i, u, s)
    } else {
      var S, F;
      if (1 == x && (S = y.fltree, F = y.fdtree), 2 == x) {
        P.F.makeCodes(y.ltree, l), P.F.revCodes(y.ltree, l), P.F.makeCodes(y.dtree, f), P.F.revCodes(y.dtree, f), P.F.makeCodes(y.itree, d), P.F.revCodes(y.itree, d), S = y.ltree, F = y.dtree, w(u, s, p - 257), w(u, s += 5, m - 1), w(u, s += 5, h - 4), s += 4;
        for (var D = 0; D < h; D++) w(u, s + 3 * D, y.itree[1 + (y.ordr[D] << 1)]);
        s += 3 * h, s = P.F._codeTiny(v, y.itree, u, s), s = P.F._codeTiny(g, y.itree, u, s)
      }
      for (var T = a, A = 0; A < r; A += 2) {
        for (var L = t[A], M = L >>> 23, j = T + (8388607 & L); T < j;) s = P.F._writeLit(o[T++], S, u, s);
        if (0 != M) {
          var I = t[A + 1],
            R = I >> 16,
            B = I >> 8 & 255,
            O = 255 & I;
          w(u, s = P.F._writeLit(257 + B, S, u, s), M - y.of0[B]), s += y.exb[B], b(u, s = P.F._writeLit(O, F, u, s), R - y.df0[O]), s += y.dxb[O], T += M
        }
      }
      s = P.F._writeLit(256, S, u, s)
    }
    return s
  }, P.F._copyExact = function (e, t, r, n, o) {
    var a = o >>> 3;
    return n[a] = r, n[a + 1] = r >>> 8, n[a + 2] = 255 - n[a], n[a + 3] = 255 - n[a + 1], a += 4, n.set(new Uint8Array(e.buffer, t, r), a), o + (r + 4 << 3)
  }, P.F.getTrees = function () {
    for (var e = P.F.U, t = P.F._hufTree(e.lhst, e.ltree, 15), r = P.F._hufTree(e.dhst, e.dtree, 15), n = [], o = P.F._lenCodes(e.ltree, n), a = [], i = P.F._lenCodes(e.dtree, a), u = 0; u < n.length; u += 2) e.ihst[n[u]]++;
    for (u = 0; u < a.length; u += 2) e.ihst[a[u]]++;
    for (var s = P.F._hufTree(e.ihst, e.itree, 7), c = 19; c > 4 && 0 == e.itree[1 + (e.ordr[c - 1] << 1)];) c--;
    return [t, r, s, o, i, c, n, a]
  }, P.F.getSecond = function (e) {
    for (var t = [], r = 0; r < e.length; r += 2) t.push(e[r + 1]);
    return t
  }, P.F.nonZero = function (e) {
    for (var t = "", r = 0; r < e.length; r += 2) 0 != e[r + 1] && (t += (r >> 1) + ",");
    return t
  }, P.F.contSize = function (e, t) {
    for (var r = 0, n = 0; n < t.length; n++) r += t[n] * e[1 + (n << 1)];
    return r
  }, P.F._codeTiny = function (e, t, r, n) {
    for (var o = 0; o < e.length; o += 2) {
      var a = e[o],
        i = e[o + 1];
      n = P.F._writeLit(a, t, r, n);
      var u = 16 == a ? 2 : 17 == a ? 3 : 7;
      a > 15 && (P.F._putsE(r, n, i, u), n += u)
    }
    return n
  }, P.F._lenCodes = function (e, t) {
    for (var r = e.length; 2 != r && 0 == e[r - 1];) r -= 2;
    for (var n = 0; n < r; n += 2) {
      var o = e[n + 1],
        a = n + 3 < r ? e[n + 3] : -1,
        i = n + 5 < r ? e[n + 5] : -1,
        u = 0 == n ? -1 : e[n - 1];
      if (0 == o && a == o && i == o) {
        for (var s = n + 5; s + 2 < r && e[s + 2] == o;) s += 2;
        (c = Math.min(s + 1 - n >>> 1, 138)) < 11 ? t.push(17, c - 3) : t.push(18, c - 11), n += 2 * c - 2
      } else if (o == u && a == o && i == o) {
        for (s = n + 5; s + 2 < r && e[s + 2] == o;) s += 2;
        var c = Math.min(s + 1 - n >>> 1, 6);
        t.push(16, c - 3), n += 2 * c - 2
      } else t.push(o, 0)
    }
    return r >>> 1
  }, P.F._hufTree = function (e, t, r) {
    var n = [],
      o = e.length,
      a = t.length,
      i = 0;
    for (i = 0; i < a; i += 2) t[i] = 0, t[i + 1] = 0;
    for (i = 0; i < o; i++) 0 != e[i] && n.push({
      lit: i,
      f: e[i]
    });
    var u = n.length,
      s = n.slice(0);
    if (0 == u) return 0;
    if (1 == u) {
      var c = n[0].lit;
      return s = 0 == c ? 1 : 0, t[1 + (c << 1)] = 1, t[1 + (s << 1)] = 1, 1
    }
    n.sort((function (e, t) {
      return e.f - t.f
    }));
    var l = n[0],
      f = n[1],
      d = 0,
      p = 1,
      m = 2;
    for (n[0] = {
        lit: -1,
        f: l.f + f.f,
        l: l,
        r: f,
        d: 0
      }; p != u - 1;) l = d != p && (m == u || n[d].f < n[m].f) ? n[d++] : n[m++], f = d != p && (m == u || n[d].f < n[m].f) ? n[d++] : n[m++], n[p++] = {
      lit: -1,
      f: l.f + f.f,
      l: l,
      r: f
    };
    var h = P.F.setDepth(n[p - 1], 0);
    for (h > r && (P.F.restrictDepth(s, r, h), h = r), i = 0; i < u; i++) t[1 + (s[i].lit << 1)] = s[i].d;
    return h
  }, P.F.setDepth = function (e, t) {
    return -1 != e.lit ? (e.d = t, t) : Math.max(P.F.setDepth(e.l, t + 1), P.F.setDepth(e.r, t + 1))
  }, P.F.restrictDepth = function (e, t, r) {
    var n = 0,
      o = 1 << r - t,
      a = 0;
    for (e.sort((function (e, t) {
        return t.d == e.d ? e.f - t.f : t.d - e.d
      })), n = 0; n < e.length && e[n].d > t; n++) {
      var i = e[n].d;
      e[n].d = t, a += o - (1 << r - i)
    }
    for (a >>>= r - t; a > 0;)(i = e[n].d) < t ? (e[n].d++, a -= 1 << t - i - 1) : n++;
    for (; n >= 0; n--) e[n].d == t && a < 0 && (e[n].d--, a++);
    0 != a && console.log("debt left")
  }, P.F._goodIndex = function (e, t) {
    var r = 0;
    return t[16 | r] <= e && (r |= 16), t[8 | r] <= e && (r |= 8), t[4 | r] <= e && (r |= 4), t[2 | r] <= e && (r |= 2), t[1 | r] <= e && (r |= 1), r
  }, P.F._writeLit = function (e, t, r, n) {
    return P.F._putsF(r, n, t[e << 1]), n + t[1 + (e << 1)]
  }, P.F.inflate = function (e, t) {
    var r = Uint8Array;
    if (3 == e[0] && 0 == e[1]) return t || new r(0);
    var n = P.F,
      o = n._bitsF,
      a = n._bitsE,
      i = n._decodeTiny,
      u = n.makeCodes,
      s = n.codes2map,
      c = n._get17,
      l = n.U,
      f = null == t;
    f && (t = new r(e.length >>> 2 << 3));
    for (var d, p, m = 0, h = 0, v = 0, g = 0, y = 0, b = 0, w = 0, _ = 0, E = 0; 0 == m;)
      if (m = o(e, E, 1), h = o(e, E + 1, 2), E += 3, 0 != h) {
        if (f && (t = P.F._check(t, _ + (1 << 17))), 1 == h && (d = l.flmap, p = l.fdmap, b = 511, w = 31), 2 == h) {
          v = a(e, E, 5) + 257, g = a(e, E + 5, 5) + 1, y = a(e, E + 10, 4) + 4, E += 14;
          for (var C = 0; C < 38; C += 2) l.itree[C] = 0, l.itree[C + 1] = 0;
          var k = 1;
          for (C = 0; C < y; C++) {
            var x = a(e, E + 3 * C, 3);
            l.itree[1 + (l.ordr[C] << 1)] = x, x > k && (k = x)
          }
          E += 3 * y, u(l.itree, k), s(l.itree, k, l.imap), d = l.lmap, p = l.dmap, E = i(l.imap, (1 << k) - 1, v + g, e, E, l.ttree);
          var S = n._copyOut(l.ttree, 0, v, l.ltree);
          b = (1 << S) - 1;
          var F = n._copyOut(l.ttree, v, g, l.dtree);
          w = (1 << F) - 1, u(l.ltree, S), s(l.ltree, S, d), u(l.dtree, F), s(l.dtree, F, p)
        }
        for (;;) {
          var D = d[c(e, E) & b];
          E += 15 & D;
          var T = D >>> 4;
          if (T >>> 8 == 0) t[_++] = T;
          else {
            if (256 == T) break;
            var A = _ + T - 254;
            if (T > 264) {
              var L = l.ldef[T - 257];
              A = _ + (L >>> 3) + a(e, E, 7 & L), E += 7 & L
            }
            var M = p[c(e, E) & w];
            E += 15 & M;
            var j = M >>> 4,
              I = l.ddef[j],
              R = (I >>> 4) + o(e, E, 15 & I);
            for (E += 15 & I, f && (t = P.F._check(t, _ + (1 << 17))); _ < A;) t[_] = t[_++ - R], t[_] = t[_++ - R], t[_] = t[_++ - R], t[_] = t[_++ - R];
            _ = A
          }
        }
      } else {
        0 != (7 & E) && (E += 8 - (7 & E));
        var B = 4 + (E >>> 3),
          O = e[B - 4] | e[B - 3] << 8;
        f && (t = P.F._check(t, _ + O)), t.set(new r(e.buffer, e.byteOffset + B, O), _), E = B + O << 3, _ += O
      } return t.length == _ ? t : t.slice(0, _)
  }, P.F._check = function (e, t) {
    var r = e.length;
    if (t <= r) return e;
    var n = new Uint8Array(Math.max(r << 1, t));
    return n.set(e, 0), n
  }, P.F._decodeTiny = function (e, t, r, n, o, a) {
    for (var i = P.F._bitsE, u = P.F._get17, s = 0; s < r;) {
      var c = e[u(n, o) & t];
      o += 15 & c;
      var l = c >>> 4;
      if (l <= 15) a[s] = l, s++;
      else {
        var f = 0,
          d = 0;
        16 == l ? (d = 3 + i(n, o, 2), o += 2, f = a[s - 1]) : 17 == l ? (d = 3 + i(n, o, 3), o += 3) : 18 == l && (d = 11 + i(n, o, 7), o += 7);
        for (var p = s + d; s < p;) a[s] = f, s++
      }
    }
    return o
  }, P.F._copyOut = function (e, t, r, n) {
    for (var o = 0, a = 0, i = n.length >>> 1; a < r;) {
      var u = e[a + t];
      n[a << 1] = 0, n[1 + (a << 1)] = u, u > o && (o = u), a++
    }
    for (; a < i;) n[a << 1] = 0, n[1 + (a << 1)] = 0, a++;
    return o
  }, P.F.makeCodes = function (e, t) {
    for (var r, n, o, a, i = P.F.U, u = e.length, s = i.bl_count, c = 0; c <= t; c++) s[c] = 0;
    for (c = 1; c < u; c += 2) s[e[c]]++;
    var l = i.next_code;
    for (r = 0, s[0] = 0, n = 1; n <= t; n++) r = r + s[n - 1] << 1, l[n] = r;
    for (o = 0; o < u; o += 2) 0 != (a = e[o + 1]) && (e[o] = l[a], l[a]++)
  }, P.F.codes2map = function (e, t, r) {
    for (var n = e.length, o = P.F.U.rev15, a = 0; a < n; a += 2)
      if (0 != e[a + 1])
        for (var i = a >> 1, u = e[a + 1], s = i << 4 | u, c = t - u, l = e[a] << c, f = l + (1 << c); l != f;) r[o[l] >>> 15 - t] = s, l++
  }, P.F.revCodes = function (e, t) {
    for (var r = P.F.U.rev15, n = 15 - t, o = 0; o < e.length; o += 2) {
      var a = e[o] << t - e[o + 1];
      e[o] = r[a] >>> n
    }
  }, P.F._putsE = function (e, t, r) {
    r <<= 7 & t;
    var n = t >>> 3;
    e[n] |= r, e[n + 1] |= r >>> 8
  }, P.F._putsF = function (e, t, r) {
    r <<= 7 & t;
    var n = t >>> 3;
    e[n] |= r, e[n + 1] |= r >>> 8, e[n + 2] |= r >>> 16
  }, P.F._bitsE = function (e, t, r) {
    return (e[t >>> 3] | e[1 + (t >>> 3)] << 8) >>> (7 & t) & (1 << r) - 1
  }, P.F._bitsF = function (e, t, r) {
    return (e[t >>> 3] | e[1 + (t >>> 3)] << 8 | e[2 + (t >>> 3)] << 16) >>> (7 & t) & (1 << r) - 1
  }, P.F._get17 = function (e, t) {
    return (e[t >>> 3] | e[1 + (t >>> 3)] << 8 | e[2 + (t >>> 3)] << 16) >>> (7 & t)
  }, P.F._get25 = function (e, t) {
    return (e[t >>> 3] | e[1 + (t >>> 3)] << 8 | e[2 + (t >>> 3)] << 16 | e[3 + (t >>> 3)] << 24) >>> (7 & t)
  }, P.F.U = (F = Uint16Array, D = Uint32Array, {
    next_code: new F(16),
    bl_count: new F(16),
    ordr: [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15],
    of0: [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 999, 999, 999],
    exb: [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0, 0, 0, 0],
    ldef: new F(32),
    df0: [1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577, 65535, 65535],
    dxb: [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 0, 0],
    ddef: new D(32),
    flmap: new F(512),
    fltree: [],
    fdmap: new F(32),
    fdtree: [],
    lmap: new F(32768),
    ltree: [],
    ttree: [],
    dmap: new F(32768),
    dtree: [],
    imap: new F(512),
    itree: [],
    rev15: new F(32768),
    lhst: new D(286),
    dhst: new D(30),
    ihst: new D(19),
    lits: new D(15e3),
    strt: new F(65536),
    prev: new F(32768)
  }),
  function () {
    for (var e = P.F.U, t = 0; t < 32768; t++) {
      var r = t;
      r = (4278255360 & (r = (4042322160 & (r = (3435973836 & (r = (2863311530 & r) >>> 1 | (1431655765 & r) << 1)) >>> 2 | (858993459 & r) << 2)) >>> 4 | (252645135 & r) << 4)) >>> 8 | (16711935 & r) << 8, e.rev15[t] = (r >>> 16 | r << 16) >>> 17
    }

    function n(e, t, r) {
      for (; 0 != t--;) e.push(0, r)
    }
    for (t = 0; t < 32; t++) e.ldef[t] = e.of0[t] << 3 | e.exb[t], e.ddef[t] = e.df0[t] << 4 | e.dxb[t];
    n(e.fltree, 144, 8), n(e.fltree, 112, 9), n(e.fltree, 24, 7), n(e.fltree, 8, 8), P.F.makeCodes(e.fltree, 9), P.F.codes2map(e.fltree, 9, e.flmap), P.F.revCodes(e.fltree, 9), n(e.fdtree, 32, 5), P.F.makeCodes(e.fdtree, 5), P.F.codes2map(e.fdtree, 5, e.fdmap), P.F.revCodes(e.fdtree, 5), n(e.itree, 19, 0), n(e.ltree, 286, 0), n(e.dtree, 30, 0), n(e.ttree, 320, 0)
  }();
(A = T || (T = {})).write = (e, t, r) => {
  const n = t.split("/");
  "" === n[0] && n.shift(), n.length > 1 && n.reduce(((t, r) => (A.exists(e, t) || e.mkdir(t), `${t}/${r}`))), e.writeFile(t, r)
}, A.exists = (e, t) => {
  try {
    return e.lstat(t), !0
  } catch (r) {
    if (44 === r.errno || "ENOENT" === r.code) return !1;
    throw r
  }
};
class M extends class {
  static async preload(e) {
    if (Array.isArray(e)) return await Promise.all(e.map((e => this.preload(e))));
    const t = new this(e);
    return await t._load(), t
  }
  constructor(e) {
    this._source = e
  }
  async _load() {
    if (this._data) return this._data;
    let e = this._source;
    if ("string" == typeof e && (e = await fetch(e).then((t => {
        if (t.ok) return t.blob();
        throw new Error(`${t.status}: Failed to fetch ${e}`)
      })).then((e => {
        if (e.size > 0) return e;
        throw new Error(`The source must not be empty. Received ${e.size} bytes size source.`)
      }))), e instanceof Blob) {
      if (!e.type.includes("zip")) throw new TypeError(`The source type must be "application/zip"-like. Received: "${e.type}".`);
      this._data = await e.arrayBuffer().then(L.exports.parse).then((e => Object.entries(e))).then((e => e.filter((([e]) => !e.startsWith("__MACOSX/"))))).then((e => e.filter((([e, t]) => t.length)))).then(Object.fromEntries)
    }
    return this._data
  }
  async _write(e) {
    const t = await this._load();
    Object.entries(t).forEach((([t, r]) => T.write(e, t, r)))
  }
} {
  constructor(e) {
    super(e)
  }
  static async preload(e) {
    return super.preload(e)
  }
  async _write(e) {
    const t = await super._load(),
      [r] = Object.keys(t)[0].split("/");
    return await super._write(e), r
  }
}
/*! (c) Andrea Giammarchi - ISC */
var j = {};
try {
  j.EventTarget = (new EventTarget).constructor
} catch (ee) {
  ! function (e, t) {
    var r = e.create,
      n = e.defineProperty,
      o = a.prototype;

    function a() {
      t.set(this, r(null))
    }

    function i(e, t, r) {
      n(e, t, {
        configurable: !0,
        writable: !0,
        value: r
      })
    }

    function u(e) {
      var t = e.options;
      return t && t.once && e.target.removeEventListener(this.type, e.listener), "function" == typeof e.listener ? e.listener.call(e.target, this) : e.listener.handleEvent(this), this._stopImmediatePropagationFlag
    }
    i(o, "addEventListener", (function (e, r, n) {
      for (var o = t.get(this), a = o[e] || (o[e] = []), i = 0, u = a.length; i < u; i++)
        if (a[i].listener === r) return;
      a.push({
        target: this,
        listener: r,
        options: n
      })
    })), i(o, "dispatchEvent", (function (e) {
      var r = t.get(this)[e.type];
      return r && (i(e, "target", this), i(e, "currentTarget", this), r.slice(0).some(u, e), delete e.currentTarget, delete e.target), !0
    })), i(o, "removeEventListener", (function (e, r) {
      for (var n = t.get(this), o = n[e] || (n[e] = []), a = 0, i = o.length; a < i; a++)
        if (o[a].listener === r) return void o.splice(a, 1)
    })), j.EventTarget = a
  }(Object, new WeakMap)
}
var I = j.EventTarget;
var R, B = (R = "undefined" != typeof document && document.currentScript ? document.currentScript.src : void 0, function (e) {
  var t, r;
  (e = void 0 !== (e = e || {}) ? e : {}).ready = new Promise((function (e, n) {
      t = e, r = n
    })), e.expectedDataFileDownloads || (e.expectedDataFileDownloads = 0), e.expectedDataFileDownloads++,
    function (t) {
      if ("object" == typeof window) window.encodeURIComponent(window.location.pathname.toString().substring(0, window.location.pathname.toString().lastIndexOf("/")) + "/");
      else {
        if ("undefined" == typeof location) throw "using preloaded data can only be done on a web page or in a web worker";
        encodeURIComponent(location.pathname.toString().substring(0, location.pathname.toString().lastIndexOf("/")) + "/")
      }
      var r = "BanubaSDK.data";
      "function" != typeof e.locateFilePackage || e.locateFile || (e.locateFile = e.locateFilePackage, f("warning: you defined Module.locateFilePackage, that has been renamed to Module.locateFile (using your locateFilePackage for now)"));
      var n = e.locateFile ? e.locateFile(r, "") : r,
        o = t.remote_package_size;
      t.package_uuid;
      var a, i, u, s, c = null,
        l = e.getPreloadedPackage ? e.getPreloadedPackage(n, o) : null;

      function d() {
        function r(e, t) {
          if (!e) throw t + (new Error).stack
        }

        function n(e, t, r) {
          this.start = e, this.end = t, this.audio = r
        }
        e.FS_createPath("/", "bnb_js", !0, !0), e.FS_createPath("/", "frx", !0, !0), e.FS_createPath("/", "MoMoFRX", !0, !0), e.FS_createPath("/", "flow", !0, !0), e.FS_createPath("/", "bnb_shaders", !0, !0), e.FS_createPath("/bnb_shaders", "bnb", !0, !0), n.prototype = {
          requests: {},
          open: function (t, r) {
            this.name = r, this.requests[r] = this, e.addRunDependency("fp " + this.name)
          },
          send: function () {},
          onload: function () {
            var e = this.byteArray.subarray(this.start, this.end);
            this.finish(e)
          },
          finish: function (t) {
            e.FS_createDataFile(this.name, null, t, !0, !0, !0), e.removeRunDependency("fp " + this.name), this.requests[this.name] = null
          }
        };
        for (var o = t.files, a = 0; a < o.length; ++a) new n(o[a].start, o[a].end, o[a].audio).open("GET", o[a].filename);

        function i(o) {
          r(o, "Loading data file failed."), r(o instanceof ArrayBuffer, "bad input to processPackageData");
          var a = new Uint8Array(o);
          n.prototype.byteArray = a;
          for (var i = t.files, u = 0; u < i.length; ++u) n.prototype.requests[i[u].filename].onload();
          e.removeRunDependency("datafile_BanubaSDK.data")
        }
        e.addRunDependency("datafile_BanubaSDK.data"), e.preloadResults || (e.preloadResults = {}), e.preloadResults["BanubaSDK.data"] = {
          fromCache: !1
        }, l ? (i(l), l = null) : c = i
      }
      l || (a = n, i = o, u = function (e) {
        c ? (c(e), c = null) : l = e
      }, (s = new XMLHttpRequest).open("GET", a, !0), s.responseType = "arraybuffer", s.onprogress = function (t) {
        var r = a,
          n = i;
        if (t.total && (n = t.total), t.loaded) {
          s.addedTotal ? e.dataFileDownloads[r].loaded = t.loaded : (s.addedTotal = !0, e.dataFileDownloads || (e.dataFileDownloads = {}), e.dataFileDownloads[r] = {
            loaded: t.loaded,
            total: n
          });
          var o = 0,
            u = 0,
            c = 0;
          for (var l in e.dataFileDownloads) {
            var f = e.dataFileDownloads[l];
            o += f.total, u += f.loaded, c++
          }
          o = Math.ceil(o * e.expectedDataFileDownloads / c), e.setStatus && e.setStatus("Downloading data... (" + u + "/" + o + ")")
        } else e.dataFileDownloads || e.setStatus && e.setStatus("Downloading data...")
      }, s.onerror = function (e) {
        throw new Error("NetworkError for: " + a)
      }, s.onload = function (e) {
        if (!(200 == s.status || 304 == s.status || 206 == s.status || 0 == s.status && s.response)) throw new Error(s.statusText + " : " + s.responseURL);
        var t = s.response;
        u(t)
      }, s.send(null)), e.calledRun ? d() : (e.preRun || (e.preRun = []), e.preRun.push(d))
    }({
      files: [{
        filename: "/bnb_js/legacy.js",
        start: 0,
        end: 3886,
        audio: 0
      }, {
        filename: "/frx/frx.js",
        start: 3886,
        end: 6408,
        audio: 0
      }, {
        filename: "/MoMoFRX/edgetopology1.ef",
        start: 6408,
        end: 53648,
        audio: 0
      }, {
        filename: "/MoMoFRX/edgetopology1.ev",
        start: 53648,
        end: 100888,
        audio: 0
      }, {
        filename: "/MoMoFRX/head.3308.DA.c30.tm.pca2.86.blendshapes",
        start: 100888,
        end: 1291768,
        audio: 0
      }, {
        filename: "/MoMoFRX/head.3308.DA.c30.tm.pca2.86.means",
        start: 1291768,
        end: 1331464,
        audio: 0
      }, {
        filename: "/MoMoFRX/head.verts",
        start: 1331464,
        end: 1371160,
        audio: 0
      }, {
        filename: "/MoMoFRX/head1.triangles",
        start: 1371160,
        end: 1448392,
        audio: 0
      }, {
        filename: "/MoMoFRX/head_au.tex",
        start: 1448392,
        end: 1474856,
        audio: 0
      }, {
        filename: "/flow/frx_prior.tflite",
        start: 1474856,
        end: 2243088,
        audio: 0
      }, {
        filename: "/flow/ssd_uint8.tflite",
        start: 2243088,
        end: 2478104,
        audio: 0
      }, {
        filename: "/flow/landmarks_128.tflite",
        start: 2478104,
        end: 4362748,
        audio: 0
      }, {
        filename: "/flow/bg_lite_vert.tflite",
        start: 4362748,
        end: 4951932,
        audio: 0
      }, {
        filename: "/flow/bg_lite_horiz.tflite",
        start: 4951932,
        end: 5541116,
        audio: 0
      }, {
        filename: "/flow/hair_lite_vert.tflite",
        start: 5541116,
        end: 6374016,
        audio: 0
      }, {
        filename: "/flow/hair_lite_horiz.tflite",
        start: 6374016,
        end: 7207228,
        audio: 0
      }, {
        filename: "/flow/lips_segmentation.tflite",
        start: 7207228,
        end: 11739544,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/anim_transform.glsl",
        start: 11739544,
        end: 11739707,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/decode_int1010102.glsl",
        start: 11739707,
        end: 11740896,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/get_bone.glsl",
        start: 11740896,
        end: 11741695,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/get_transform.glsl",
        start: 11741695,
        end: 11743148,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/glsl.frag",
        start: 11743148,
        end: 11743914,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/glsl.vert",
        start: 11743914,
        end: 11744901,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/lut.glsl",
        start: 11744901,
        end: 11748006,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/math.glsl",
        start: 11748006,
        end: 11748379,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/matrix_operations.glsl",
        start: 11748379,
        end: 11751705,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/morph_transform.glsl",
        start: 11751705,
        end: 11752763,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/samplers_declaration.glsl",
        start: 11752763,
        end: 11755411,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/textures_lookup.glsl",
        start: 11755411,
        end: 11758051,
        audio: 0
      }, {
        filename: "/bnb_shaders/bnb/version.glsl",
        start: 11758051,
        end: 11758355,
        audio: 0
      }, {
        filename: "/watermark_blurred.png",
        start: 11758355,
        end: 11851977,
        audio: 0
      }, {
        filename: "/watermark.png",
        start: 11851977,
        end: 11857898,
        audio: 0
      }],
      remote_package_size: 11857898,
      package_uuid: "3256883a-bb28-46c1-8a4f-f01f594adee9"
    });
  var n, o = {};
  for (n in e) e.hasOwnProperty(n) && (o[n] = e[n]);
  var a, i, u = "./this.program",
    s = function (e, t) {
      throw t
    },
    c = "";
  "undefined" != typeof document && document.currentScript && (c = document.currentScript.src), R && (c = R), c = 0 !== c.indexOf("blob:") ? c.substr(0, c.lastIndexOf("/") + 1) : "", a = function (e) {
    var t = new XMLHttpRequest;
    return t.open("GET", e, !1), t.send(null), t.responseText
  }, i = function (e, t, r) {
    var n = new XMLHttpRequest;
    n.open("GET", e, !0), n.responseType = "arraybuffer", n.onload = function () {
      200 == n.status || 0 == n.status && n.response ? t(n.response) : r()
    }, n.onerror = r, n.send(null)
  };
  var l = e.print || console.log.bind(console),
    f = e.printErr || console.warn.bind(console);
  for (n in o) o.hasOwnProperty(n) && (e[n] = o[n]);

  function d(e) {
    d.shown || (d.shown = {}), d.shown[e] || (d.shown[e] = 1, f(e))
  }
  o = null, e.arguments, e.thisProgram && (u = e.thisProgram), e.quit && (s = e.quit);
  var p, m, h = [];

  function v(e, t) {
    if (!p) {
      p = new WeakMap;
      for (var r = 0; r < N.length; r++) {
        var n = N.get(r);
        n && p.set(n, r)
      }
    }
    if (p.has(e)) return p.get(e);
    var o = function () {
      if (h.length) return h.pop();
      try {
        N.grow(1)
      } catch (e) {
        if (!(e instanceof RangeError)) throw e;
        throw "Unable to grow wasm table. Set ALLOW_TABLE_GROWTH."
      }
      return N.length - 1
    }();
    try {
      N.set(o, e)
    } catch (i) {
      if (!(i instanceof TypeError)) throw i;
      var a = function (e, t) {
        if ("function" == typeof WebAssembly.Function) {
          for (var r = {
              i: "i32",
              j: "i64",
              f: "f32",
              d: "f64"
            }, n = {
              parameters: [],
              results: "v" == t[0] ? [] : [r[t[0]]]
            }, o = 1; o < t.length; ++o) n.parameters.push(r[t[o]]);
          return new WebAssembly.Function(n, e)
        }
        var a = [1, 0, 1, 96],
          i = t.slice(0, 1),
          u = t.slice(1),
          s = {
            i: 127,
            j: 126,
            f: 125,
            d: 124
          };
        for (a.push(u.length), o = 0; o < u.length; ++o) a.push(s[u[o]]);
        "v" == i ? a.push(0) : a = a.concat([1, s[i]]), a[1] = a.length - 2;
        var c = new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0].concat(a, [2, 7, 1, 1, 101, 1, 102, 0, 0, 7, 5, 1, 1, 102, 0, 0])),
          l = new WebAssembly.Module(c);
        return new WebAssembly.Instance(l, {
          e: {
            f: e
          }
        }).exports.f
      }(e, t);
      N.set(o, a)
    }
    return p.set(e, o), o
  }
  e.wasmBinary && (m = e.wasmBinary);
  var g, y = e.noExitRuntime || !0;
  "object" != typeof WebAssembly && re("no native wasm support detected");
  var b, w = !1;

  function _(e, t) {
    e || re("Assertion failed: " + t)
  }

  function E(e, t) {
    var r;
    return r = 1 == t ? Qr(e.length) : Br(e.length), e.subarray || e.slice ? A.set(e, r) : A.set(new Uint8Array(e), r), r
  }
  var C = "undefined" != typeof TextDecoder ? new TextDecoder("utf8") : void 0;

  function k(e, t, r) {
    for (var n = t + r, o = t; e[o] && !(o >= n);) ++o;
    if (o - t > 16 && e.subarray && C) return C.decode(e.subarray(t, o));
    for (var a = ""; t < o;) {
      var i = e[t++];
      if (128 & i) {
        var u = 63 & e[t++];
        if (192 != (224 & i)) {
          var s = 63 & e[t++];
          if ((i = 224 == (240 & i) ? (15 & i) << 12 | u << 6 | s : (7 & i) << 18 | u << 12 | s << 6 | 63 & e[t++]) < 65536) a += String.fromCharCode(i);
          else {
            var c = i - 65536;
            a += String.fromCharCode(55296 | c >> 10, 56320 | 1023 & c)
          }
        } else a += String.fromCharCode((31 & i) << 6 | u)
      } else a += String.fromCharCode(i)
    }
    return a
  }

  function x(e, t) {
    return e ? k(A, e, t) : ""
  }

  function S(e, t, r, n) {
    if (!(n > 0)) return 0;
    for (var o = r, a = r + n - 1, i = 0; i < e.length; ++i) {
      var u = e.charCodeAt(i);
      if (u >= 55296 && u <= 57343 && (u = 65536 + ((1023 & u) << 10) | 1023 & e.charCodeAt(++i)), u <= 127) {
        if (r >= a) break;
        t[r++] = u
      } else if (u <= 2047) {
        if (r + 1 >= a) break;
        t[r++] = 192 | u >> 6, t[r++] = 128 | 63 & u
      } else if (u <= 65535) {
        if (r + 2 >= a) break;
        t[r++] = 224 | u >> 12, t[r++] = 128 | u >> 6 & 63, t[r++] = 128 | 63 & u
      } else {
        if (r + 3 >= a) break;
        t[r++] = 240 | u >> 18, t[r++] = 128 | u >> 12 & 63, t[r++] = 128 | u >> 6 & 63, t[r++] = 128 | 63 & u
      }
    }
    return t[r] = 0, r - o
  }

  function F(e, t, r) {
    return S(e, A, t, r)
  }

  function D(e) {
    for (var t = 0, r = 0; r < e.length; ++r) {
      var n = e.charCodeAt(r);
      n >= 55296 && n <= 57343 && (n = 65536 + ((1023 & n) << 10) | 1023 & e.charCodeAt(++r)), n <= 127 ? ++t : t += n <= 2047 ? 2 : n <= 65535 ? 3 : 4
    }
    return t
  }
  var P, T, A, L, M, j, I, B, O, N, U = "undefined" != typeof TextDecoder ? new TextDecoder("utf-16le") : void 0;

  function z(e, t) {
    for (var r = e, n = r >> 1, o = n + t / 2; !(n >= o) && M[n];) ++n;
    if ((r = n << 1) - e > 32 && U) return U.decode(A.subarray(e, r));
    for (var a = "", i = 0; !(i >= t / 2); ++i) {
      var u = L[e + 2 * i >> 1];
      if (0 == u) break;
      a += String.fromCharCode(u)
    }
    return a
  }

  function $(e, t, r) {
    if (void 0 === r && (r = 2147483647), r < 2) return 0;
    for (var n = t, o = (r -= 2) < 2 * e.length ? r / 2 : e.length, a = 0; a < o; ++a) {
      var i = e.charCodeAt(a);
      L[t >> 1] = i, t += 2
    }
    return L[t >> 1] = 0, t - n
  }

  function W(e) {
    return 2 * e.length
  }

  function G(e, t) {
    for (var r = 0, n = ""; !(r >= t / 4);) {
      var o = j[e + 4 * r >> 2];
      if (0 == o) break;
      if (++r, o >= 65536) {
        var a = o - 65536;
        n += String.fromCharCode(55296 | a >> 10, 56320 | 1023 & a)
      } else n += String.fromCharCode(o)
    }
    return n
  }

  function q(e, t, r) {
    if (void 0 === r && (r = 2147483647), r < 4) return 0;
    for (var n = t, o = n + r - 4, a = 0; a < e.length; ++a) {
      var i = e.charCodeAt(a);
      if (i >= 55296 && i <= 57343 && (i = 65536 + ((1023 & i) << 10) | 1023 & e.charCodeAt(++a)), j[t >> 2] = i, (t += 4) + 4 > o) break
    }
    return j[t >> 2] = 0, t - n
  }

  function Q(e) {
    for (var t = 0, r = 0; r < e.length; ++r) {
      var n = e.charCodeAt(r);
      n >= 55296 && n <= 57343 && ++r, t += 4
    }
    return t
  }

  function Y(e) {
    var t = D(e) + 1,
      r = Br(t);
    return r && S(e, T, r, t), r
  }

  function V(t) {
    P = t, e.HEAP8 = T = new Int8Array(t), e.HEAP16 = L = new Int16Array(t), e.HEAP32 = j = new Int32Array(t), e.HEAPU8 = A = new Uint8Array(t), e.HEAPU16 = M = new Uint16Array(t), e.HEAPU32 = I = new Uint32Array(t), e.HEAPF32 = B = new Float32Array(t), e.HEAPF64 = O = new Float64Array(t)
  }
  e.INITIAL_MEMORY;
  var X = [],
    H = [],
    K = [],
    Z = 0,
    J = null;

  function ee(t) {
    Z++, e.monitorRunDependencies && e.monitorRunDependencies(Z)
  }

  function te(t) {
    if (Z--, e.monitorRunDependencies && e.monitorRunDependencies(Z), 0 == Z && J) {
      var r = J;
      J = null, r()
    }
  }

  function re(t) {
    e.onAbort && e.onAbort(t), f(t += ""), w = !0, b = 1, t = "abort(" + t + "). Build with -s ASSERTIONS=1 for more info.";
    var n = new WebAssembly.RuntimeError(t);
    throw r(n), n
  }

  function ne(e) {
    return e.startsWith("data:application/octet-stream;base64,")
  }
  e.preloadedImages = {}, e.preloadedAudios = {};
  var oe, ae, ie, ue, se = "BanubaSDK.wasm";

  function ce(e) {
    try {
      if (e == se && m) return new Uint8Array(m);
      throw "both async and sync fetching of the wasm failed"
    } catch (t) {
      re(t)
    }
  }

  function le(t) {
    for (; t.length > 0;) {
      var r = t.shift();
      if ("function" != typeof r) {
        var n = r.func;
        "number" == typeof n ? void 0 === r.arg ? N.get(n)() : N.get(n)(r.arg) : n(void 0 === r.arg ? null : r.arg)
      } else r(e)
    }
  }

  function fe() {
    return y || !1
  }

  function de(e) {
    return j[Nr() >> 2] = e, e
  }

  function pe(e, t) {
    var r;
    if (0 === e) r = Date.now();
    else {
      if (1 !== e && 4 !== e) return de(28), -1;
      r = ue()
    }
    return j[t >> 2] = r / 1e3 | 0, j[t + 4 >> 2] = r % 1e3 * 1e3 * 1e3 | 0, 0
  }
  ne(se) || (oe = se, se = e.locateFile ? e.locateFile(oe, c) : c + oe), ue = function () {
    return performance.now()
  };
  var me = 0,
    he = 4,
    ve = 8,
    ge = 12,
    ye = 13,
    be = 16,
    we = [];

  function _e(e) {
    this.excPtr = e, this.ptr = e - be, this.set_type = function (e) {
      j[this.ptr + ve >> 2] = e
    }, this.get_type = function () {
      return j[this.ptr + ve >> 2]
    }, this.set_destructor = function (e) {
      j[this.ptr + me >> 2] = e
    }, this.get_destructor = function () {
      return j[this.ptr + me >> 2]
    }, this.set_refcount = function (e) {
      j[this.ptr + he >> 2] = e
    }, this.set_caught = function (e) {
      e = e ? 1 : 0, T[this.ptr + ge >> 0] = e
    }, this.get_caught = function () {
      return 0 != T[this.ptr + ge >> 0]
    }, this.set_rethrown = function (e) {
      e = e ? 1 : 0, T[this.ptr + ye >> 0] = e
    }, this.get_rethrown = function () {
      return 0 != T[this.ptr + ye >> 0]
    }, this.init = function (e, t) {
      this.set_type(e), this.set_destructor(t), this.set_refcount(0), this.set_caught(!1), this.set_rethrown(!1)
    }, this.add_ref = function () {
      var e = j[this.ptr + he >> 2];
      j[this.ptr + he >> 2] = e + 1
    }, this.release_ref = function () {
      var e = j[this.ptr + he >> 2];
      return j[this.ptr + he >> 2] = e - 1, 1 === e
    }
  }

  function Ee(e, t) {
    var r = new Date(1e3 * j[e >> 2]);
    j[t >> 2] = r.getUTCSeconds(), j[t + 4 >> 2] = r.getUTCMinutes(), j[t + 8 >> 2] = r.getUTCHours(), j[t + 12 >> 2] = r.getUTCDate(), j[t + 16 >> 2] = r.getUTCMonth(), j[t + 20 >> 2] = r.getUTCFullYear() - 1900, j[t + 24 >> 2] = r.getUTCDay(), j[t + 36 >> 2] = 0, j[t + 32 >> 2] = 0;
    var n = Date.UTC(r.getUTCFullYear(), 0, 1, 0, 0, 0, 0),
      o = (r.getTime() - n) / 864e5 | 0;
    return j[t + 28 >> 2] = o, Ee.GMTString || (Ee.GMTString = Y("GMT")), j[t + 40 >> 2] = Ee.GMTString, t
  }
  var Ce = {
    splitPath: function (e) {
      return /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/.exec(e).slice(1)
    },
    normalizeArray: function (e, t) {
      for (var r = 0, n = e.length - 1; n >= 0; n--) {
        var o = e[n];
        "." === o ? e.splice(n, 1) : ".." === o ? (e.splice(n, 1), r++) : r && (e.splice(n, 1), r--)
      }
      if (t)
        for (; r; r--) e.unshift("..");
      return e
    },
    normalize: function (e) {
      var t = "/" === e.charAt(0),
        r = "/" === e.substr(-1);
      return (e = Ce.normalizeArray(e.split("/").filter((function (e) {
        return !!e
      })), !t).join("/")) || t || (e = "."), e && r && (e += "/"), (t ? "/" : "") + e
    },
    dirname: function (e) {
      var t = Ce.splitPath(e),
        r = t[0],
        n = t[1];
      return r || n ? (n && (n = n.substr(0, n.length - 1)), r + n) : "."
    },
    basename: function (e) {
      if ("/" === e) return "/";
      var t = (e = (e = Ce.normalize(e)).replace(/\/$/, "")).lastIndexOf("/");
      return -1 === t ? e : e.substr(t + 1)
    },
    extname: function (e) {
      return Ce.splitPath(e)[3]
    },
    join: function () {
      var e = Array.prototype.slice.call(arguments, 0);
      return Ce.normalize(e.join("/"))
    },
    join2: function (e, t) {
      return Ce.normalize(e + "/" + t)
    }
  };

  function ke() {
    if ("object" == typeof crypto && "function" == typeof crypto.getRandomValues) {
      var e = new Uint8Array(1);
      return function () {
        return crypto.getRandomValues(e), e[0]
      }
    }
    return function () {
      re("randomDevice")
    }
  }
  var xe = {
      resolve: function () {
        for (var e = "", t = !1, r = arguments.length - 1; r >= -1 && !t; r--) {
          var n = r >= 0 ? arguments[r] : Pe.cwd();
          if ("string" != typeof n) throw new TypeError("Arguments to path.resolve must be strings");
          if (!n) return "";
          e = n + "/" + e, t = "/" === n.charAt(0)
        }
        return (t ? "/" : "") + (e = Ce.normalizeArray(e.split("/").filter((function (e) {
          return !!e
        })), !t).join("/")) || "."
      },
      relative: function (e, t) {
        function r(e) {
          for (var t = 0; t < e.length && "" === e[t]; t++);
          for (var r = e.length - 1; r >= 0 && "" === e[r]; r--);
          return t > r ? [] : e.slice(t, r - t + 1)
        }
        e = xe.resolve(e).substr(1), t = xe.resolve(t).substr(1);
        for (var n = r(e.split("/")), o = r(t.split("/")), a = Math.min(n.length, o.length), i = a, u = 0; u < a; u++)
          if (n[u] !== o[u]) {
            i = u;
            break
          } var s = [];
        for (u = i; u < n.length; u++) s.push("..");
        return (s = s.concat(o.slice(i))).join("/")
      }
    },
    Se = {
      ttys: [],
      init: function () {},
      shutdown: function () {},
      register: function (e, t) {
        Se.ttys[e] = {
          input: [],
          output: [],
          ops: t
        }, Pe.registerDevice(e, Se.stream_ops)
      },
      stream_ops: {
        open: function (e) {
          var t = Se.ttys[e.node.rdev];
          if (!t) throw new Pe.ErrnoError(43);
          e.tty = t, e.seekable = !1
        },
        close: function (e) {
          e.tty.ops.flush(e.tty)
        },
        flush: function (e) {
          e.tty.ops.flush(e.tty)
        },
        read: function (e, t, r, n, o) {
          if (!e.tty || !e.tty.ops.get_char) throw new Pe.ErrnoError(60);
          for (var a = 0, i = 0; i < n; i++) {
            var u;
            try {
              u = e.tty.ops.get_char(e.tty)
            } catch (s) {
              throw new Pe.ErrnoError(29)
            }
            if (void 0 === u && 0 === a) throw new Pe.ErrnoError(6);
            if (null == u) break;
            a++, t[r + i] = u
          }
          return a && (e.node.timestamp = Date.now()), a
        },
        write: function (e, t, r, n, o) {
          if (!e.tty || !e.tty.ops.put_char) throw new Pe.ErrnoError(60);
          try {
            for (var a = 0; a < n; a++) e.tty.ops.put_char(e.tty, t[r + a])
          } catch (i) {
            throw new Pe.ErrnoError(29)
          }
          return n && (e.node.timestamp = Date.now()), a
        }
      },
      default_tty_ops: {
        get_char: function (e) {
          if (!e.input.length) {
            var t = null;
            if ("undefined" != typeof window && "function" == typeof window.prompt ? null !== (t = window.prompt("Input: ")) && (t += "\n") : "function" == typeof readline && null !== (t = readline()) && (t += "\n"), !t) return null;
            e.input = Ir(t, !0)
          }
          return e.input.shift()
        },
        put_char: function (e, t) {
          null === t || 10 === t ? (l(k(e.output, 0)), e.output = []) : 0 != t && e.output.push(t)
        },
        flush: function (e) {
          e.output && e.output.length > 0 && (l(k(e.output, 0)), e.output = [])
        }
      },
      default_tty1_ops: {
        put_char: function (e, t) {
          null === t || 10 === t ? (f(k(e.output, 0)), e.output = []) : 0 != t && e.output.push(t)
        },
        flush: function (e) {
          e.output && e.output.length > 0 && (f(k(e.output, 0)), e.output = [])
        }
      }
    };

  function Fe(e) {
    for (var t = function (e, t) {
        return t || (t = 16), Math.ceil(e / t) * t
      }(e, 65536), r = Br(t); e < t;) T[r + e++] = 0;
    return r
  }
  var De = {
      ops_table: null,
      mount: function (e) {
        return De.createNode(null, "/", 16895, 0)
      },
      createNode: function (e, t, r, n) {
        if (Pe.isBlkdev(r) || Pe.isFIFO(r)) throw new Pe.ErrnoError(63);
        De.ops_table || (De.ops_table = {
          dir: {
            node: {
              getattr: De.node_ops.getattr,
              setattr: De.node_ops.setattr,
              lookup: De.node_ops.lookup,
              mknod: De.node_ops.mknod,
              rename: De.node_ops.rename,
              unlink: De.node_ops.unlink,
              rmdir: De.node_ops.rmdir,
              readdir: De.node_ops.readdir,
              symlink: De.node_ops.symlink
            },
            stream: {
              llseek: De.stream_ops.llseek
            }
          },
          file: {
            node: {
              getattr: De.node_ops.getattr,
              setattr: De.node_ops.setattr
            },
            stream: {
              llseek: De.stream_ops.llseek,
              read: De.stream_ops.read,
              write: De.stream_ops.write,
              allocate: De.stream_ops.allocate,
              mmap: De.stream_ops.mmap,
              msync: De.stream_ops.msync
            }
          },
          link: {
            node: {
              getattr: De.node_ops.getattr,
              setattr: De.node_ops.setattr,
              readlink: De.node_ops.readlink
            },
            stream: {}
          },
          chrdev: {
            node: {
              getattr: De.node_ops.getattr,
              setattr: De.node_ops.setattr
            },
            stream: Pe.chrdev_stream_ops
          }
        });
        var o = Pe.createNode(e, t, r, n);
        return Pe.isDir(o.mode) ? (o.node_ops = De.ops_table.dir.node, o.stream_ops = De.ops_table.dir.stream, o.contents = {}) : Pe.isFile(o.mode) ? (o.node_ops = De.ops_table.file.node, o.stream_ops = De.ops_table.file.stream, o.usedBytes = 0, o.contents = null) : Pe.isLink(o.mode) ? (o.node_ops = De.ops_table.link.node, o.stream_ops = De.ops_table.link.stream) : Pe.isChrdev(o.mode) && (o.node_ops = De.ops_table.chrdev.node, o.stream_ops = De.ops_table.chrdev.stream), o.timestamp = Date.now(), e && (e.contents[t] = o, e.timestamp = o.timestamp), o
      },
      getFileDataAsTypedArray: function (e) {
        return e.contents ? e.contents.subarray ? e.contents.subarray(0, e.usedBytes) : new Uint8Array(e.contents) : new Uint8Array(0)
      },
      expandFileStorage: function (e, t) {
        var r = e.contents ? e.contents.length : 0;
        if (!(r >= t)) {
          t = Math.max(t, r * (r < 1048576 ? 2 : 1.125) >>> 0), 0 != r && (t = Math.max(t, 256));
          var n = e.contents;
          e.contents = new Uint8Array(t), e.usedBytes > 0 && e.contents.set(n.subarray(0, e.usedBytes), 0)
        }
      },
      resizeFileStorage: function (e, t) {
        if (e.usedBytes != t)
          if (0 == t) e.contents = null, e.usedBytes = 0;
          else {
            var r = e.contents;
            e.contents = new Uint8Array(t), r && e.contents.set(r.subarray(0, Math.min(t, e.usedBytes))), e.usedBytes = t
          }
      },
      node_ops: {
        getattr: function (e) {
          var t = {};
          return t.dev = Pe.isChrdev(e.mode) ? e.id : 1, t.ino = e.id, t.mode = e.mode, t.nlink = 1, t.uid = 0, t.gid = 0, t.rdev = e.rdev, Pe.isDir(e.mode) ? t.size = 4096 : Pe.isFile(e.mode) ? t.size = e.usedBytes : Pe.isLink(e.mode) ? t.size = e.link.length : t.size = 0, t.atime = new Date(e.timestamp), t.mtime = new Date(e.timestamp), t.ctime = new Date(e.timestamp), t.blksize = 4096, t.blocks = Math.ceil(t.size / t.blksize), t
        },
        setattr: function (e, t) {
          void 0 !== t.mode && (e.mode = t.mode), void 0 !== t.timestamp && (e.timestamp = t.timestamp), void 0 !== t.size && De.resizeFileStorage(e, t.size)
        },
        lookup: function (e, t) {
          throw Pe.genericErrors[44]
        },
        mknod: function (e, t, r, n) {
          return De.createNode(e, t, r, n)
        },
        rename: function (e, t, r) {
          if (Pe.isDir(e.mode)) {
            var n;
            try {
              n = Pe.lookupNode(t, r)
            } catch (a) {}
            if (n)
              for (var o in n.contents) throw new Pe.ErrnoError(55)
          }
          delete e.parent.contents[e.name], e.parent.timestamp = Date.now(), e.name = r, t.contents[r] = e, t.timestamp = e.parent.timestamp, e.parent = t
        },
        unlink: function (e, t) {
          delete e.contents[t], e.timestamp = Date.now()
        },
        rmdir: function (e, t) {
          var r = Pe.lookupNode(e, t);
          for (var n in r.contents) throw new Pe.ErrnoError(55);
          delete e.contents[t], e.timestamp = Date.now()
        },
        readdir: function (e) {
          var t = [".", ".."];
          for (var r in e.contents) e.contents.hasOwnProperty(r) && t.push(r);
          return t
        },
        symlink: function (e, t, r) {
          var n = De.createNode(e, t, 41471, 0);
          return n.link = r, n
        },
        readlink: function (e) {
          if (!Pe.isLink(e.mode)) throw new Pe.ErrnoError(28);
          return e.link
        }
      },
      stream_ops: {
        read: function (e, t, r, n, o) {
          var a = e.node.contents;
          if (o >= e.node.usedBytes) return 0;
          var i = Math.min(e.node.usedBytes - o, n);
          if (i > 8 && a.subarray) t.set(a.subarray(o, o + i), r);
          else
            for (var u = 0; u < i; u++) t[r + u] = a[o + u];
          return i
        },
        write: function (e, t, r, n, o, a) {
          if (t.buffer === T.buffer && (a = !1), !n) return 0;
          var i = e.node;
          if (i.timestamp = Date.now(), t.subarray && (!i.contents || i.contents.subarray)) {
            if (a) return i.contents = t.subarray(r, r + n), i.usedBytes = n, n;
            if (0 === i.usedBytes && 0 === o) return i.contents = t.slice(r, r + n), i.usedBytes = n, n;
            if (o + n <= i.usedBytes) return i.contents.set(t.subarray(r, r + n), o), n
          }
          if (De.expandFileStorage(i, o + n), i.contents.subarray && t.subarray) i.contents.set(t.subarray(r, r + n), o);
          else
            for (var u = 0; u < n; u++) i.contents[o + u] = t[r + u];
          return i.usedBytes = Math.max(i.usedBytes, o + n), n
        },
        llseek: function (e, t, r) {
          var n = t;
          if (1 === r ? n += e.position : 2 === r && Pe.isFile(e.node.mode) && (n += e.node.usedBytes), n < 0) throw new Pe.ErrnoError(28);
          return n
        },
        allocate: function (e, t, r) {
          De.expandFileStorage(e.node, t + r), e.node.usedBytes = Math.max(e.node.usedBytes, t + r)
        },
        mmap: function (e, t, r, n, o, a) {
          if (0 !== t) throw new Pe.ErrnoError(28);
          if (!Pe.isFile(e.node.mode)) throw new Pe.ErrnoError(43);
          var i, u, s = e.node.contents;
          if (2 & a || s.buffer !== P) {
            if ((n > 0 || n + r < s.length) && (s = s.subarray ? s.subarray(n, n + r) : Array.prototype.slice.call(s, n, n + r)), u = !0, !(i = Fe(r))) throw new Pe.ErrnoError(48);
            T.set(s, i)
          } else u = !1, i = s.byteOffset;
          return {
            ptr: i,
            allocated: u
          }
        },
        msync: function (e, t, r, n, o) {
          if (!Pe.isFile(e.node.mode)) throw new Pe.ErrnoError(43);
          return 2 & o || De.stream_ops.write(e, t, 0, n, r, !1), 0
        }
      }
    },
    Pe = {
      root: null,
      mounts: [],
      devices: {},
      streams: [],
      nextInode: 1,
      nameTable: null,
      currentPath: "/",
      initialized: !1,
      ignorePermissions: !0,
      trackingDelegate: {},
      tracking: {
        openFlags: {
          READ: 1,
          WRITE: 2
        }
      },
      ErrnoError: null,
      genericErrors: {},
      filesystems: null,
      syncFSRequests: 0,
      lookupPath: function (e, t) {
        if (t = t || {}, !(e = xe.resolve(Pe.cwd(), e))) return {
          path: "",
          node: null
        };
        var r = {
          follow_mount: !0,
          recurse_count: 0
        };
        for (var n in r) void 0 === t[n] && (t[n] = r[n]);
        if (t.recurse_count > 8) throw new Pe.ErrnoError(32);
        for (var o = Ce.normalizeArray(e.split("/").filter((function (e) {
            return !!e
          })), !1), a = Pe.root, i = "/", u = 0; u < o.length; u++) {
          var s = u === o.length - 1;
          if (s && t.parent) break;
          if (a = Pe.lookupNode(a, o[u]), i = Ce.join2(i, o[u]), Pe.isMountpoint(a) && (!s || s && t.follow_mount) && (a = a.mounted.root), !s || t.follow)
            for (var c = 0; Pe.isLink(a.mode);) {
              var l = Pe.readlink(i);
              if (i = xe.resolve(Ce.dirname(i), l), a = Pe.lookupPath(i, {
                  recurse_count: t.recurse_count
                }).node, c++ > 40) throw new Pe.ErrnoError(32)
            }
        }
        return {
          path: i,
          node: a
        }
      },
      getPath: function (e) {
        for (var t;;) {
          if (Pe.isRoot(e)) {
            var r = e.mount.mountpoint;
            return t ? "/" !== r[r.length - 1] ? r + "/" + t : r + t : r
          }
          t = t ? e.name + "/" + t : e.name, e = e.parent
        }
      },
      hashName: function (e, t) {
        for (var r = 0, n = 0; n < t.length; n++) r = (r << 5) - r + t.charCodeAt(n) | 0;
        return (e + r >>> 0) % Pe.nameTable.length
      },
      hashAddNode: function (e) {
        var t = Pe.hashName(e.parent.id, e.name);
        e.name_next = Pe.nameTable[t], Pe.nameTable[t] = e
      },
      hashRemoveNode: function (e) {
        var t = Pe.hashName(e.parent.id, e.name);
        if (Pe.nameTable[t] === e) Pe.nameTable[t] = e.name_next;
        else
          for (var r = Pe.nameTable[t]; r;) {
            if (r.name_next === e) {
              r.name_next = e.name_next;
              break
            }
            r = r.name_next
          }
      },
      lookupNode: function (e, t) {
        var r = Pe.mayLookup(e);
        if (r) throw new Pe.ErrnoError(r, e);
        for (var n = Pe.hashName(e.id, t), o = Pe.nameTable[n]; o; o = o.name_next) {
          var a = o.name;
          if (o.parent.id === e.id && a === t) return o
        }
        return Pe.lookup(e, t)
      },
      createNode: function (e, t, r, n) {
        var o = new Pe.FSNode(e, t, r, n);
        return Pe.hashAddNode(o), o
      },
      destroyNode: function (e) {
        Pe.hashRemoveNode(e)
      },
      isRoot: function (e) {
        return e === e.parent
      },
      isMountpoint: function (e) {
        return !!e.mounted
      },
      isFile: function (e) {
        return 32768 == (61440 & e)
      },
      isDir: function (e) {
        return 16384 == (61440 & e)
      },
      isLink: function (e) {
        return 40960 == (61440 & e)
      },
      isChrdev: function (e) {
        return 8192 == (61440 & e)
      },
      isBlkdev: function (e) {
        return 24576 == (61440 & e)
      },
      isFIFO: function (e) {
        return 4096 == (61440 & e)
      },
      isSocket: function (e) {
        return 49152 == (49152 & e)
      },
      flagModes: {
        r: 0,
        "r+": 2,
        w: 577,
        "w+": 578,
        a: 1089,
        "a+": 1090
      },
      modeStringToFlags: function (e) {
        var t = Pe.flagModes[e];
        if (void 0 === t) throw new Error("Unknown file open mode: " + e);
        return t
      },
      flagsToPermissionString: function (e) {
        var t = ["r", "w", "rw"][3 & e];
        return 512 & e && (t += "w"), t
      },
      nodePermissions: function (e, t) {
        return Pe.ignorePermissions || (!t.includes("r") || 292 & e.mode) && (!t.includes("w") || 146 & e.mode) && (!t.includes("x") || 73 & e.mode) ? 0 : 2
      },
      mayLookup: function (e) {
        var t = Pe.nodePermissions(e, "x");
        return t || (e.node_ops.lookup ? 0 : 2)
      },
      mayCreate: function (e, t) {
        try {
          return Pe.lookupNode(e, t), 20
        } catch (r) {}
        return Pe.nodePermissions(e, "wx")
      },
      mayDelete: function (e, t, r) {
        var n;
        try {
          n = Pe.lookupNode(e, t)
        } catch (a) {
          return a.errno
        }
        var o = Pe.nodePermissions(e, "wx");
        if (o) return o;
        if (r) {
          if (!Pe.isDir(n.mode)) return 54;
          if (Pe.isRoot(n) || Pe.getPath(n) === Pe.cwd()) return 10
        } else if (Pe.isDir(n.mode)) return 31;
        return 0
      },
      mayOpen: function (e, t) {
        return e ? Pe.isLink(e.mode) ? 32 : Pe.isDir(e.mode) && ("r" !== Pe.flagsToPermissionString(t) || 512 & t) ? 31 : Pe.nodePermissions(e, Pe.flagsToPermissionString(t)) : 44
      },
      MAX_OPEN_FDS: 4096,
      nextfd: function (e, t) {
        e = e || 0, t = t || Pe.MAX_OPEN_FDS;
        for (var r = e; r <= t; r++)
          if (!Pe.streams[r]) return r;
        throw new Pe.ErrnoError(33)
      },
      getStream: function (e) {
        return Pe.streams[e]
      },
      createStream: function (e, t, r) {
        Pe.FSStream || (Pe.FSStream = function () {}, Pe.FSStream.prototype = {
          object: {
            get: function () {
              return this.node
            },
            set: function (e) {
              this.node = e
            }
          },
          isRead: {
            get: function () {
              return 1 != (2097155 & this.flags)
            }
          },
          isWrite: {
            get: function () {
              return 0 != (2097155 & this.flags)
            }
          },
          isAppend: {
            get: function () {
              return 1024 & this.flags
            }
          }
        });
        var n = new Pe.FSStream;
        for (var o in e) n[o] = e[o];
        e = n;
        var a = Pe.nextfd(t, r);
        return e.fd = a, Pe.streams[a] = e, e
      },
      closeStream: function (e) {
        Pe.streams[e] = null
      },
      chrdev_stream_ops: {
        open: function (e) {
          var t = Pe.getDevice(e.node.rdev);
          e.stream_ops = t.stream_ops, e.stream_ops.open && e.stream_ops.open(e)
        },
        llseek: function () {
          throw new Pe.ErrnoError(70)
        }
      },
      major: function (e) {
        return e >> 8
      },
      minor: function (e) {
        return 255 & e
      },
      makedev: function (e, t) {
        return e << 8 | t
      },
      registerDevice: function (e, t) {
        Pe.devices[e] = {
          stream_ops: t
        }
      },
      getDevice: function (e) {
        return Pe.devices[e]
      },
      getMounts: function (e) {
        for (var t = [], r = [e]; r.length;) {
          var n = r.pop();
          t.push(n), r.push.apply(r, n.mounts)
        }
        return t
      },
      syncfs: function (e, t) {
        "function" == typeof e && (t = e, e = !1), Pe.syncFSRequests++, Pe.syncFSRequests > 1 && f("warning: " + Pe.syncFSRequests + " FS.syncfs operations in flight at once, probably just doing extra work");
        var r = Pe.getMounts(Pe.root.mount),
          n = 0;

        function o(e) {
          return Pe.syncFSRequests--, t(e)
        }

        function a(e) {
          if (e) return a.errored ? void 0 : (a.errored = !0, o(e));
          ++n >= r.length && o(null)
        }
        r.forEach((function (t) {
          if (!t.type.syncfs) return a(null);
          t.type.syncfs(t, e, a)
        }))
      },
      mount: function (e, t, r) {
        var n, o = "/" === r,
          a = !r;
        if (o && Pe.root) throw new Pe.ErrnoError(10);
        if (!o && !a) {
          var i = Pe.lookupPath(r, {
            follow_mount: !1
          });
          if (r = i.path, n = i.node, Pe.isMountpoint(n)) throw new Pe.ErrnoError(10);
          if (!Pe.isDir(n.mode)) throw new Pe.ErrnoError(54)
        }
        var u = {
            type: e,
            opts: t,
            mountpoint: r,
            mounts: []
          },
          s = e.mount(u);
        return s.mount = u, u.root = s, o ? Pe.root = s : n && (n.mounted = u, n.mount && n.mount.mounts.push(u)), s
      },
      unmount: function (e) {
        var t = Pe.lookupPath(e, {
          follow_mount: !1
        });
        if (!Pe.isMountpoint(t.node)) throw new Pe.ErrnoError(28);
        var r = t.node,
          n = r.mounted,
          o = Pe.getMounts(n);
        Object.keys(Pe.nameTable).forEach((function (e) {
          for (var t = Pe.nameTable[e]; t;) {
            var r = t.name_next;
            o.includes(t.mount) && Pe.destroyNode(t), t = r
          }
        })), r.mounted = null;
        var a = r.mount.mounts.indexOf(n);
        r.mount.mounts.splice(a, 1)
      },
      lookup: function (e, t) {
        return e.node_ops.lookup(e, t)
      },
      mknod: function (e, t, r) {
        var n = Pe.lookupPath(e, {
            parent: !0
          }).node,
          o = Ce.basename(e);
        if (!o || "." === o || ".." === o) throw new Pe.ErrnoError(28);
        var a = Pe.mayCreate(n, o);
        if (a) throw new Pe.ErrnoError(a);
        if (!n.node_ops.mknod) throw new Pe.ErrnoError(63);
        return n.node_ops.mknod(n, o, t, r)
      },
      create: function (e, t) {
        return t = void 0 !== t ? t : 438, t &= 4095, t |= 32768, Pe.mknod(e, t, 0)
      },
      mkdir: function (e, t) {
        return t = void 0 !== t ? t : 511, t &= 1023, t |= 16384, Pe.mknod(e, t, 0)
      },
      mkdirTree: function (e, t) {
        for (var r = e.split("/"), n = "", o = 0; o < r.length; ++o)
          if (r[o]) {
            n += "/" + r[o];
            try {
              Pe.mkdir(n, t)
            } catch (a) {
              if (20 != a.errno) throw a
            }
          }
      },
      mkdev: function (e, t, r) {
        return void 0 === r && (r = t, t = 438), t |= 8192, Pe.mknod(e, t, r)
      },
      symlink: function (e, t) {
        if (!xe.resolve(e)) throw new Pe.ErrnoError(44);
        var r = Pe.lookupPath(t, {
          parent: !0
        }).node;
        if (!r) throw new Pe.ErrnoError(44);
        var n = Ce.basename(t),
          o = Pe.mayCreate(r, n);
        if (o) throw new Pe.ErrnoError(o);
        if (!r.node_ops.symlink) throw new Pe.ErrnoError(63);
        return r.node_ops.symlink(r, n, e)
      },
      rename: function (e, t) {
        var r, n, o = Ce.dirname(e),
          a = Ce.dirname(t),
          i = Ce.basename(e),
          u = Ce.basename(t);
        if (r = Pe.lookupPath(e, {
            parent: !0
          }).node, n = Pe.lookupPath(t, {
            parent: !0
          }).node, !r || !n) throw new Pe.ErrnoError(44);
        if (r.mount !== n.mount) throw new Pe.ErrnoError(75);
        var s, c = Pe.lookupNode(r, i),
          l = xe.relative(e, a);
        if ("." !== l.charAt(0)) throw new Pe.ErrnoError(28);
        if ("." !== (l = xe.relative(t, o)).charAt(0)) throw new Pe.ErrnoError(55);
        try {
          s = Pe.lookupNode(n, u)
        } catch (m) {}
        if (c !== s) {
          var d = Pe.isDir(c.mode),
            p = Pe.mayDelete(r, i, d);
          if (p) throw new Pe.ErrnoError(p);
          if (p = s ? Pe.mayDelete(n, u, d) : Pe.mayCreate(n, u)) throw new Pe.ErrnoError(p);
          if (!r.node_ops.rename) throw new Pe.ErrnoError(63);
          if (Pe.isMountpoint(c) || s && Pe.isMountpoint(s)) throw new Pe.ErrnoError(10);
          if (n !== r && (p = Pe.nodePermissions(r, "w"))) throw new Pe.ErrnoError(p);
          try {
            Pe.trackingDelegate.willMovePath && Pe.trackingDelegate.willMovePath(e, t)
          } catch (m) {
            f("FS.trackingDelegate['willMovePath']('" + e + "', '" + t + "') threw an exception: " + m.message)
          }
          Pe.hashRemoveNode(c);
          try {
            r.node_ops.rename(c, n, u)
          } catch (m) {
            throw m
          } finally {
            Pe.hashAddNode(c)
          }
          try {
            Pe.trackingDelegate.onMovePath && Pe.trackingDelegate.onMovePath(e, t)
          } catch (m) {
            f("FS.trackingDelegate['onMovePath']('" + e + "', '" + t + "') threw an exception: " + m.message)
          }
        }
      },
      rmdir: function (e) {
        var t = Pe.lookupPath(e, {
            parent: !0
          }).node,
          r = Ce.basename(e),
          n = Pe.lookupNode(t, r),
          o = Pe.mayDelete(t, r, !0);
        if (o) throw new Pe.ErrnoError(o);
        if (!t.node_ops.rmdir) throw new Pe.ErrnoError(63);
        if (Pe.isMountpoint(n)) throw new Pe.ErrnoError(10);
        try {
          Pe.trackingDelegate.willDeletePath && Pe.trackingDelegate.willDeletePath(e)
        } catch (a) {
          f("FS.trackingDelegate['willDeletePath']('" + e + "') threw an exception: " + a.message)
        }
        t.node_ops.rmdir(t, r), Pe.destroyNode(n);
        try {
          Pe.trackingDelegate.onDeletePath && Pe.trackingDelegate.onDeletePath(e)
        } catch (a) {
          f("FS.trackingDelegate['onDeletePath']('" + e + "') threw an exception: " + a.message)
        }
      },
      readdir: function (e) {
        var t = Pe.lookupPath(e, {
          follow: !0
        }).node;
        if (!t.node_ops.readdir) throw new Pe.ErrnoError(54);
        return t.node_ops.readdir(t)
      },
      unlink: function (e) {
        var t = Pe.lookupPath(e, {
            parent: !0
          }).node,
          r = Ce.basename(e),
          n = Pe.lookupNode(t, r),
          o = Pe.mayDelete(t, r, !1);
        if (o) throw new Pe.ErrnoError(o);
        if (!t.node_ops.unlink) throw new Pe.ErrnoError(63);
        if (Pe.isMountpoint(n)) throw new Pe.ErrnoError(10);
        try {
          Pe.trackingDelegate.willDeletePath && Pe.trackingDelegate.willDeletePath(e)
        } catch (a) {
          f("FS.trackingDelegate['willDeletePath']('" + e + "') threw an exception: " + a.message)
        }
        t.node_ops.unlink(t, r), Pe.destroyNode(n);
        try {
          Pe.trackingDelegate.onDeletePath && Pe.trackingDelegate.onDeletePath(e)
        } catch (a) {
          f("FS.trackingDelegate['onDeletePath']('" + e + "') threw an exception: " + a.message)
        }
      },
      readlink: function (e) {
        var t = Pe.lookupPath(e).node;
        if (!t) throw new Pe.ErrnoError(44);
        if (!t.node_ops.readlink) throw new Pe.ErrnoError(28);
        return xe.resolve(Pe.getPath(t.parent), t.node_ops.readlink(t))
      },
      stat: function (e, t) {
        var r = Pe.lookupPath(e, {
          follow: !t
        }).node;
        if (!r) throw new Pe.ErrnoError(44);
        if (!r.node_ops.getattr) throw new Pe.ErrnoError(63);
        return r.node_ops.getattr(r)
      },
      lstat: function (e) {
        return Pe.stat(e, !0)
      },
      chmod: function (e, t, r) {
        var n;
        if (!(n = "string" == typeof e ? Pe.lookupPath(e, {
            follow: !r
          }).node : e).node_ops.setattr) throw new Pe.ErrnoError(63);
        n.node_ops.setattr(n, {
          mode: 4095 & t | -4096 & n.mode,
          timestamp: Date.now()
        })
      },
      lchmod: function (e, t) {
        Pe.chmod(e, t, !0)
      },
      fchmod: function (e, t) {
        var r = Pe.getStream(e);
        if (!r) throw new Pe.ErrnoError(8);
        Pe.chmod(r.node, t)
      },
      chown: function (e, t, r, n) {
        var o;
        if (!(o = "string" == typeof e ? Pe.lookupPath(e, {
            follow: !n
          }).node : e).node_ops.setattr) throw new Pe.ErrnoError(63);
        o.node_ops.setattr(o, {
          timestamp: Date.now()
        })
      },
      lchown: function (e, t, r) {
        Pe.chown(e, t, r, !0)
      },
      fchown: function (e, t, r) {
        var n = Pe.getStream(e);
        if (!n) throw new Pe.ErrnoError(8);
        Pe.chown(n.node, t, r)
      },
      truncate: function (e, t) {
        if (t < 0) throw new Pe.ErrnoError(28);
        var r;
        if (!(r = "string" == typeof e ? Pe.lookupPath(e, {
            follow: !0
          }).node : e).node_ops.setattr) throw new Pe.ErrnoError(63);
        if (Pe.isDir(r.mode)) throw new Pe.ErrnoError(31);
        if (!Pe.isFile(r.mode)) throw new Pe.ErrnoError(28);
        var n = Pe.nodePermissions(r, "w");
        if (n) throw new Pe.ErrnoError(n);
        r.node_ops.setattr(r, {
          size: t,
          timestamp: Date.now()
        })
      },
      ftruncate: function (e, t) {
        var r = Pe.getStream(e);
        if (!r) throw new Pe.ErrnoError(8);
        if (0 == (2097155 & r.flags)) throw new Pe.ErrnoError(28);
        Pe.truncate(r.node, t)
      },
      utime: function (e, t, r) {
        var n = Pe.lookupPath(e, {
          follow: !0
        }).node;
        n.node_ops.setattr(n, {
          timestamp: Math.max(t, r)
        })
      },
      open: function (t, r, n, o, a) {
        if ("" === t) throw new Pe.ErrnoError(44);
        var i;
        if (n = void 0 === n ? 438 : n, n = 64 & (r = "string" == typeof r ? Pe.modeStringToFlags(r) : r) ? 4095 & n | 32768 : 0, "object" == typeof t) i = t;
        else {
          t = Ce.normalize(t);
          try {
            i = Pe.lookupPath(t, {
              follow: !(131072 & r)
            }).node
          } catch (d) {}
        }
        var u = !1;
        if (64 & r)
          if (i) {
            if (128 & r) throw new Pe.ErrnoError(20)
          } else i = Pe.mknod(t, n, 0), u = !0;
        if (!i) throw new Pe.ErrnoError(44);
        if (Pe.isChrdev(i.mode) && (r &= -513), 65536 & r && !Pe.isDir(i.mode)) throw new Pe.ErrnoError(54);
        if (!u) {
          var s = Pe.mayOpen(i, r);
          if (s) throw new Pe.ErrnoError(s)
        }
        512 & r && Pe.truncate(i, 0), r &= -131713;
        var c = Pe.createStream({
          node: i,
          path: Pe.getPath(i),
          flags: r,
          seekable: !0,
          position: 0,
          stream_ops: i.stream_ops,
          ungotten: [],
          error: !1
        }, o, a);
        c.stream_ops.open && c.stream_ops.open(c), !e.logReadFiles || 1 & r || (Pe.readFiles || (Pe.readFiles = {}), t in Pe.readFiles || (Pe.readFiles[t] = 1, f("FS.trackingDelegate error on read file: " + t)));
        try {
          if (Pe.trackingDelegate.onOpenFile) {
            var l = 0;
            1 != (2097155 & r) && (l |= Pe.tracking.openFlags.READ), 0 != (2097155 & r) && (l |= Pe.tracking.openFlags.WRITE), Pe.trackingDelegate.onOpenFile(t, l)
          }
        } catch (d) {
          f("FS.trackingDelegate['onOpenFile']('" + t + "', flags) threw an exception: " + d.message)
        }
        return c
      },
      close: function (e) {
        if (Pe.isClosed(e)) throw new Pe.ErrnoError(8);
        e.getdents && (e.getdents = null);
        try {
          e.stream_ops.close && e.stream_ops.close(e)
        } catch (t) {
          throw t
        } finally {
          Pe.closeStream(e.fd)
        }
        e.fd = null
      },
      isClosed: function (e) {
        return null === e.fd
      },
      llseek: function (e, t, r) {
        if (Pe.isClosed(e)) throw new Pe.ErrnoError(8);
        if (!e.seekable || !e.stream_ops.llseek) throw new Pe.ErrnoError(70);
        if (0 != r && 1 != r && 2 != r) throw new Pe.ErrnoError(28);
        return e.position = e.stream_ops.llseek(e, t, r), e.ungotten = [], e.position
      },
      read: function (e, t, r, n, o) {
        if (n < 0 || o < 0) throw new Pe.ErrnoError(28);
        if (Pe.isClosed(e)) throw new Pe.ErrnoError(8);
        if (1 == (2097155 & e.flags)) throw new Pe.ErrnoError(8);
        if (Pe.isDir(e.node.mode)) throw new Pe.ErrnoError(31);
        if (!e.stream_ops.read) throw new Pe.ErrnoError(28);
        var a = void 0 !== o;
        if (a) {
          if (!e.seekable) throw new Pe.ErrnoError(70)
        } else o = e.position;
        var i = e.stream_ops.read(e, t, r, n, o);
        return a || (e.position += i), i
      },
      write: function (e, t, r, n, o, a) {
        if (n < 0 || o < 0) throw new Pe.ErrnoError(28);
        if (Pe.isClosed(e)) throw new Pe.ErrnoError(8);
        if (0 == (2097155 & e.flags)) throw new Pe.ErrnoError(8);
        if (Pe.isDir(e.node.mode)) throw new Pe.ErrnoError(31);
        if (!e.stream_ops.write) throw new Pe.ErrnoError(28);
        e.seekable && 1024 & e.flags && Pe.llseek(e, 0, 2);
        var i = void 0 !== o;
        if (i) {
          if (!e.seekable) throw new Pe.ErrnoError(70)
        } else o = e.position;
        var u = e.stream_ops.write(e, t, r, n, o, a);
        i || (e.position += u);
        try {
          e.path && Pe.trackingDelegate.onWriteToFile && Pe.trackingDelegate.onWriteToFile(e.path)
        } catch (s) {
          f("FS.trackingDelegate['onWriteToFile']('" + e.path + "') threw an exception: " + s.message)
        }
        return u
      },
      allocate: function (e, t, r) {
        if (Pe.isClosed(e)) throw new Pe.ErrnoError(8);
        if (t < 0 || r <= 0) throw new Pe.ErrnoError(28);
        if (0 == (2097155 & e.flags)) throw new Pe.ErrnoError(8);
        if (!Pe.isFile(e.node.mode) && !Pe.isDir(e.node.mode)) throw new Pe.ErrnoError(43);
        if (!e.stream_ops.allocate) throw new Pe.ErrnoError(138);
        e.stream_ops.allocate(e, t, r)
      },
      mmap: function (e, t, r, n, o, a) {
        if (0 != (2 & o) && 0 == (2 & a) && 2 != (2097155 & e.flags)) throw new Pe.ErrnoError(2);
        if (1 == (2097155 & e.flags)) throw new Pe.ErrnoError(2);
        if (!e.stream_ops.mmap) throw new Pe.ErrnoError(43);
        return e.stream_ops.mmap(e, t, r, n, o, a)
      },
      msync: function (e, t, r, n, o) {
        return e && e.stream_ops.msync ? e.stream_ops.msync(e, t, r, n, o) : 0
      },
      munmap: function (e) {
        return 0
      },
      ioctl: function (e, t, r) {
        if (!e.stream_ops.ioctl) throw new Pe.ErrnoError(59);
        return e.stream_ops.ioctl(e, t, r)
      },
      readFile: function (e, t) {
        if ((t = t || {}).flags = t.flags || 0, t.encoding = t.encoding || "binary", "utf8" !== t.encoding && "binary" !== t.encoding) throw new Error('Invalid encoding type "' + t.encoding + '"');
        var r, n = Pe.open(e, t.flags),
          o = Pe.stat(e).size,
          a = new Uint8Array(o);
        return Pe.read(n, a, 0, o, 0), "utf8" === t.encoding ? r = k(a, 0) : "binary" === t.encoding && (r = a), Pe.close(n), r
      },
      writeFile: function (e, t, r) {
        (r = r || {}).flags = r.flags || 577;
        var n = Pe.open(e, r.flags, r.mode);
        if ("string" == typeof t) {
          var o = new Uint8Array(D(t) + 1),
            a = S(t, o, 0, o.length);
          Pe.write(n, o, 0, a, void 0, r.canOwn)
        } else {
          if (!ArrayBuffer.isView(t)) throw new Error("Unsupported data type");
          Pe.write(n, t, 0, t.byteLength, void 0, r.canOwn)
        }
        Pe.close(n)
      },
      cwd: function () {
        return Pe.currentPath
      },
      chdir: function (e) {
        var t = Pe.lookupPath(e, {
          follow: !0
        });
        if (null === t.node) throw new Pe.ErrnoError(44);
        if (!Pe.isDir(t.node.mode)) throw new Pe.ErrnoError(54);
        var r = Pe.nodePermissions(t.node, "x");
        if (r) throw new Pe.ErrnoError(r);
        Pe.currentPath = t.path
      },
      createDefaultDirectories: function () {
        Pe.mkdir("/tmp"), Pe.mkdir("/home"), Pe.mkdir("/home/web_user")
      },
      createDefaultDevices: function () {
        Pe.mkdir("/dev"), Pe.registerDevice(Pe.makedev(1, 3), {
          read: function () {
            return 0
          },
          write: function (e, t, r, n, o) {
            return n
          }
        }), Pe.mkdev("/dev/null", Pe.makedev(1, 3)), Se.register(Pe.makedev(5, 0), Se.default_tty_ops), Se.register(Pe.makedev(6, 0), Se.default_tty1_ops), Pe.mkdev("/dev/tty", Pe.makedev(5, 0)), Pe.mkdev("/dev/tty1", Pe.makedev(6, 0));
        var e = ke();
        Pe.createDevice("/dev", "random", e), Pe.createDevice("/dev", "urandom", e), Pe.mkdir("/dev/shm"), Pe.mkdir("/dev/shm/tmp")
      },
      createSpecialDirectories: function () {
        Pe.mkdir("/proc");
        var e = Pe.mkdir("/proc/self");
        Pe.mkdir("/proc/self/fd"), Pe.mount({
          mount: function () {
            var t = Pe.createNode(e, "fd", 16895, 73);
            return t.node_ops = {
              lookup: function (e, t) {
                var r = +t,
                  n = Pe.getStream(r);
                if (!n) throw new Pe.ErrnoError(8);
                var o = {
                  parent: null,
                  mount: {
                    mountpoint: "fake"
                  },
                  node_ops: {
                    readlink: function () {
                      return n.path
                    }
                  }
                };
                return o.parent = o, o
              }
            }, t
          }
        }, {}, "/proc/self/fd")
      },
      createStandardStreams: function () {
        e.stdin ? Pe.createDevice("/dev", "stdin", e.stdin) : Pe.symlink("/dev/tty", "/dev/stdin"), e.stdout ? Pe.createDevice("/dev", "stdout", null, e.stdout) : Pe.symlink("/dev/tty", "/dev/stdout"), e.stderr ? Pe.createDevice("/dev", "stderr", null, e.stderr) : Pe.symlink("/dev/tty1", "/dev/stderr"), Pe.open("/dev/stdin", 0), Pe.open("/dev/stdout", 1), Pe.open("/dev/stderr", 1)
      },
      ensureErrnoError: function () {
        Pe.ErrnoError || (Pe.ErrnoError = function (e, t) {
          this.node = t, this.setErrno = function (e) {
            this.errno = e
          }, this.setErrno(e), this.message = "FS error"
        }, Pe.ErrnoError.prototype = new Error, Pe.ErrnoError.prototype.constructor = Pe.ErrnoError, [44].forEach((function (e) {
          Pe.genericErrors[e] = new Pe.ErrnoError(e), Pe.genericErrors[e].stack = "<generic error, no stack>"
        })))
      },
      staticInit: function () {
        Pe.ensureErrnoError(), Pe.nameTable = new Array(4096), Pe.mount(De, {}, "/"), Pe.createDefaultDirectories(), Pe.createDefaultDevices(), Pe.createSpecialDirectories(), Pe.filesystems = {
          MEMFS: De
        }
      },
      init: function (t, r, n) {
        Pe.init.initialized = !0, Pe.ensureErrnoError(), e.stdin = t || e.stdin, e.stdout = r || e.stdout, e.stderr = n || e.stderr, Pe.createStandardStreams()
      },
      quit: function () {
        Pe.init.initialized = !1;
        var t = e._fflush;
        t && t(0);
        for (var r = 0; r < Pe.streams.length; r++) {
          var n = Pe.streams[r];
          n && Pe.close(n)
        }
      },
      getMode: function (e, t) {
        var r = 0;
        return e && (r |= 365), t && (r |= 146), r
      },
      findObject: function (e, t) {
        var r = Pe.analyzePath(e, t);
        return r.exists ? r.object : null
      },
      analyzePath: function (e, t) {
        try {
          e = (n = Pe.lookupPath(e, {
            follow: !t
          })).path
        } catch (o) {}
        var r = {
          isRoot: !1,
          exists: !1,
          error: 0,
          name: null,
          path: null,
          object: null,
          parentExists: !1,
          parentPath: null,
          parentObject: null
        };
        try {
          var n = Pe.lookupPath(e, {
            parent: !0
          });
          r.parentExists = !0, r.parentPath = n.path, r.parentObject = n.node, r.name = Ce.basename(e), n = Pe.lookupPath(e, {
            follow: !t
          }), r.exists = !0, r.path = n.path, r.object = n.node, r.name = n.node.name, r.isRoot = "/" === n.path
        } catch (o) {
          r.error = o.errno
        }
        return r
      },
      createPath: function (e, t, r, n) {
        e = "string" == typeof e ? e : Pe.getPath(e);
        for (var o = t.split("/").reverse(); o.length;) {
          var a = o.pop();
          if (a) {
            var i = Ce.join2(e, a);
            try {
              Pe.mkdir(i)
            } catch (u) {}
            e = i
          }
        }
        return i
      },
      createFile: function (e, t, r, n, o) {
        var a = Ce.join2("string" == typeof e ? e : Pe.getPath(e), t),
          i = Pe.getMode(n, o);
        return Pe.create(a, i)
      },
      createDataFile: function (e, t, r, n, o, a) {
        var i = t ? Ce.join2("string" == typeof e ? e : Pe.getPath(e), t) : e,
          u = Pe.getMode(n, o),
          s = Pe.create(i, u);
        if (r) {
          if ("string" == typeof r) {
            for (var c = new Array(r.length), l = 0, f = r.length; l < f; ++l) c[l] = r.charCodeAt(l);
            r = c
          }
          Pe.chmod(s, 146 | u);
          var d = Pe.open(s, 577);
          Pe.write(d, r, 0, r.length, 0, a), Pe.close(d), Pe.chmod(s, u)
        }
        return s
      },
      createDevice: function (e, t, r, n) {
        var o = Ce.join2("string" == typeof e ? e : Pe.getPath(e), t),
          a = Pe.getMode(!!r, !!n);
        Pe.createDevice.major || (Pe.createDevice.major = 64);
        var i = Pe.makedev(Pe.createDevice.major++, 0);
        return Pe.registerDevice(i, {
          open: function (e) {
            e.seekable = !1
          },
          close: function (e) {
            n && n.buffer && n.buffer.length && n(10)
          },
          read: function (e, t, n, o, a) {
            for (var i = 0, u = 0; u < o; u++) {
              var s;
              try {
                s = r()
              } catch (c) {
                throw new Pe.ErrnoError(29)
              }
              if (void 0 === s && 0 === i) throw new Pe.ErrnoError(6);
              if (null == s) break;
              i++, t[n + u] = s
            }
            return i && (e.node.timestamp = Date.now()), i
          },
          write: function (e, t, r, o, a) {
            for (var i = 0; i < o; i++) try {
              n(t[r + i])
            } catch (u) {
              throw new Pe.ErrnoError(29)
            }
            return o && (e.node.timestamp = Date.now()), i
          }
        }), Pe.mkdev(o, a, i)
      },
      forceLoadFile: function (e) {
        if (e.isDevice || e.isFolder || e.link || e.contents) return !0;
        if ("undefined" != typeof XMLHttpRequest) throw new Error("Lazy loading should have been performed (contents set) in createLazyFile, but it was not. Lazy loading only works in web workers. Use --embed-file or --preload-file in emcc on the main thread.");
        if (!a) throw new Error("Cannot load without read() or XMLHttpRequest.");
        try {
          e.contents = Ir(a(e.url), !0), e.usedBytes = e.contents.length
        } catch (t) {
          throw new Pe.ErrnoError(29)
        }
      },
      createLazyFile: function (e, t, r, n, o) {
        function a() {
          this.lengthKnown = !1, this.chunks = []
        }
        if (a.prototype.get = function (e) {
            if (!(e > this.length - 1 || e < 0)) {
              var t = e % this.chunkSize,
                r = e / this.chunkSize | 0;
              return this.getter(r)[t]
            }
          }, a.prototype.setDataGetter = function (e) {
            this.getter = e
          }, a.prototype.cacheLength = function () {
            var e = new XMLHttpRequest;
            if (e.open("HEAD", r, !1), e.send(null), !(e.status >= 200 && e.status < 300 || 304 === e.status)) throw new Error("Couldn't load " + r + ". Status: " + e.status);
            var t, n = Number(e.getResponseHeader("Content-length")),
              o = (t = e.getResponseHeader("Accept-Ranges")) && "bytes" === t,
              a = (t = e.getResponseHeader("Content-Encoding")) && "gzip" === t,
              i = 1048576;
            o || (i = n);
            var u = this;
            u.setDataGetter((function (e) {
              var t = e * i,
                o = (e + 1) * i - 1;
              if (o = Math.min(o, n - 1), void 0 === u.chunks[e] && (u.chunks[e] = function (e, t) {
                  if (e > t) throw new Error("invalid range (" + e + ", " + t + ") or no bytes requested!");
                  if (t > n - 1) throw new Error("only " + n + " bytes available! programmer error!");
                  var o = new XMLHttpRequest;
                  if (o.open("GET", r, !1), n !== i && o.setRequestHeader("Range", "bytes=" + e + "-" + t), "undefined" != typeof Uint8Array && (o.responseType = "arraybuffer"), o.overrideMimeType && o.overrideMimeType("text/plain; charset=x-user-defined"), o.send(null), !(o.status >= 200 && o.status < 300 || 304 === o.status)) throw new Error("Couldn't load " + r + ". Status: " + o.status);
                  return void 0 !== o.response ? new Uint8Array(o.response || []) : Ir(o.responseText || "", !0)
                }(t, o)), void 0 === u.chunks[e]) throw new Error("doXHR failed!");
              return u.chunks[e]
            })), !a && n || (i = n = 1, n = this.getter(0).length, i = n, l("LazyFiles on gzip forces download of the whole file when length is accessed")), this._length = n, this._chunkSize = i, this.lengthKnown = !0
          }, "undefined" != typeof XMLHttpRequest) throw "Cannot do synchronous binary XHRs outside webworkers in modern browsers. Use --embed-file or --preload-file in emcc";
        var i = {
            isDevice: !1,
            url: r
          },
          u = Pe.createFile(e, t, i, n, o);
        i.contents ? u.contents = i.contents : i.url && (u.contents = null, u.url = i.url), Object.defineProperties(u, {
          usedBytes: {
            get: function () {
              return this.contents.length
            }
          }
        });
        var s = {};
        return Object.keys(u.stream_ops).forEach((function (e) {
          var t = u.stream_ops[e];
          s[e] = function () {
            return Pe.forceLoadFile(u), t.apply(null, arguments)
          }
        })), s.read = function (e, t, r, n, o) {
          Pe.forceLoadFile(u);
          var a = e.node.contents;
          if (o >= a.length) return 0;
          var i = Math.min(a.length - o, n);
          if (a.slice)
            for (var s = 0; s < i; s++) t[r + s] = a[o + s];
          else
            for (s = 0; s < i; s++) t[r + s] = a.get(o + s);
          return i
        }, u.stream_ops = s, u
      },
      createPreloadedFile: function (t, r, n, o, a, i, u, s, c, l) {
        ir.init();
        var f = r ? xe.resolve(Ce.join2(t, r)) : t;

        function d(n) {
          function d(e) {
            l && l(), s || Pe.createDataFile(t, r, e, o, a, c), i && i(), te()
          }
          var p = !1;
          e.preloadPlugins.forEach((function (e) {
            p || e.canHandle(f) && (e.handle(n, f, d, (function () {
              u && u(), te()
            })), p = !0)
          })), p || d(n)
        }
        ee(), "string" == typeof n ? ir.asyncLoad(n, (function (e) {
          d(e)
        }), u) : d(n)
      },
      indexedDB: function () {
        return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB
      },
      DB_NAME: function () {
        return "EM_FS_" + window.location.pathname
      },
      DB_VERSION: 20,
      DB_STORE_NAME: "FILE_DATA",
      saveFilesToDB: function (e, t, r) {
        t = t || function () {}, r = r || function () {};
        var n = Pe.indexedDB();
        try {
          var o = n.open(Pe.DB_NAME(), Pe.DB_VERSION)
        } catch (a) {
          return r(a)
        }
        o.onupgradeneeded = function () {
          l("creating db"), o.result.createObjectStore(Pe.DB_STORE_NAME)
        }, o.onsuccess = function () {
          var n = o.result.transaction([Pe.DB_STORE_NAME], "readwrite"),
            a = n.objectStore(Pe.DB_STORE_NAME),
            i = 0,
            u = 0,
            s = e.length;

          function c() {
            0 == u ? t() : r()
          }
          e.forEach((function (e) {
            var t = a.put(Pe.analyzePath(e).object.contents, e);
            t.onsuccess = function () {
              ++i + u == s && c()
            }, t.onerror = function () {
              u++, i + u == s && c()
            }
          })), n.onerror = r
        }, o.onerror = r
      },
      loadFilesFromDB: function (e, t, r) {
        t = t || function () {}, r = r || function () {};
        var n = Pe.indexedDB();
        try {
          var o = n.open(Pe.DB_NAME(), Pe.DB_VERSION)
        } catch (a) {
          return r(a)
        }
        o.onupgradeneeded = r, o.onsuccess = function () {
          var n = o.result;
          try {
            var i = n.transaction([Pe.DB_STORE_NAME], "readonly")
          } catch (a) {
            return void r(a)
          }
          var u = i.objectStore(Pe.DB_STORE_NAME),
            s = 0,
            c = 0,
            l = e.length;

          function f() {
            0 == c ? t() : r()
          }
          e.forEach((function (e) {
            var t = u.get(e);
            t.onsuccess = function () {
              Pe.analyzePath(e).exists && Pe.unlink(e), Pe.createDataFile(Ce.dirname(e), Ce.basename(e), t.result, !0, !0, !0), ++s + c == l && f()
            }, t.onerror = function () {
              c++, s + c == l && f()
            }
          })), i.onerror = r
        }, o.onerror = r
      }
    },
    Te = {
      mappings: {},
      DEFAULT_POLLMASK: 5,
      umask: 511,
      calculateAt: function (e, t, r) {
        if ("/" === t[0]) return t;
        var n;
        if (-100 === e) n = Pe.cwd();
        else {
          var o = Pe.getStream(e);
          if (!o) throw new Pe.ErrnoError(8);
          n = o.path
        }
        if (0 == t.length) {
          if (!r) throw new Pe.ErrnoError(44);
          return n
        }
        return Ce.join2(n, t)
      },
      doStat: function (e, t, r) {
        try {
          var n = e(t)
        } catch (o) {
          if (o && o.node && Ce.normalize(t) !== Ce.normalize(Pe.getPath(o.node))) return -54;
          throw o
        }
        return j[r >> 2] = n.dev, j[r + 4 >> 2] = 0, j[r + 8 >> 2] = n.ino, j[r + 12 >> 2] = n.mode, j[r + 16 >> 2] = n.nlink, j[r + 20 >> 2] = n.uid, j[r + 24 >> 2] = n.gid, j[r + 28 >> 2] = n.rdev, j[r + 32 >> 2] = 0, ie = [n.size >>> 0, (ae = n.size, +Math.abs(ae) >= 1 ? ae > 0 ? (0 | Math.min(+Math.floor(ae / 4294967296), 4294967295)) >>> 0 : ~~+Math.ceil((ae - +(~~ae >>> 0)) / 4294967296) >>> 0 : 0)], j[r + 40 >> 2] = ie[0], j[r + 44 >> 2] = ie[1], j[r + 48 >> 2] = 4096, j[r + 52 >> 2] = n.blocks, j[r + 56 >> 2] = n.atime.getTime() / 1e3 | 0, j[r + 60 >> 2] = 0, j[r + 64 >> 2] = n.mtime.getTime() / 1e3 | 0, j[r + 68 >> 2] = 0, j[r + 72 >> 2] = n.ctime.getTime() / 1e3 | 0, j[r + 76 >> 2] = 0, ie = [n.ino >>> 0, (ae = n.ino, +Math.abs(ae) >= 1 ? ae > 0 ? (0 | Math.min(+Math.floor(ae / 4294967296), 4294967295)) >>> 0 : ~~+Math.ceil((ae - +(~~ae >>> 0)) / 4294967296) >>> 0 : 0)], j[r + 80 >> 2] = ie[0], j[r + 84 >> 2] = ie[1], 0
      },
      doMsync: function (e, t, r, n, o) {
        var a = A.slice(e, e + r);
        Pe.msync(t, a, o, r, n)
      },
      doMkdir: function (e, t) {
        return "/" === (e = Ce.normalize(e))[e.length - 1] && (e = e.substr(0, e.length - 1)), Pe.mkdir(e, t, 0), 0
      },
      doMknod: function (e, t, r) {
        switch (61440 & t) {
          case 32768:
          case 8192:
          case 24576:
          case 4096:
          case 49152:
            break;
          default:
            return -28
        }
        return Pe.mknod(e, t, r), 0
      },
      doReadlink: function (e, t, r) {
        if (r <= 0) return -28;
        var n = Pe.readlink(e),
          o = Math.min(r, D(n)),
          a = T[t + o];
        return F(n, t, r + 1), T[t + o] = a, o
      },
      doAccess: function (e, t) {
        if (-8 & t) return -28;
        var r;
        if (!(r = Pe.lookupPath(e, {
            follow: !0
          }).node)) return -44;
        var n = "";
        return 4 & t && (n += "r"), 2 & t && (n += "w"), 1 & t && (n += "x"), n && Pe.nodePermissions(r, n) ? -2 : 0
      },
      doDup: function (e, t, r) {
        var n = Pe.getStream(r);
        return n && Pe.close(n), Pe.open(e, t, 0, r, r).fd
      },
      doReadv: function (e, t, r, n) {
        for (var o = 0, a = 0; a < r; a++) {
          var i = j[t + 8 * a >> 2],
            u = j[t + (8 * a + 4) >> 2],
            s = Pe.read(e, T, i, u, n);
          if (s < 0) return -1;
          if (o += s, s < u) break
        }
        return o
      },
      doWritev: function (e, t, r, n) {
        for (var o = 0, a = 0; a < r; a++) {
          var i = j[t + 8 * a >> 2],
            u = j[t + (8 * a + 4) >> 2],
            s = Pe.write(e, T, i, u, n);
          if (s < 0) return -1;
          o += s
        }
        return o
      },
      varargs: void 0,
      get: function () {
        return Te.varargs += 4, j[Te.varargs - 4 >> 2]
      },
      getStr: function (e) {
        return x(e)
      },
      getStreamFromFD: function (e) {
        var t = Pe.getStream(e);
        if (!t) throw new Pe.ErrnoError(8);
        return t
      },
      get64: function (e, t) {
        return e
      }
    };

  function Ae(e) {
    switch (e) {
      case 1:
        return 0;
      case 2:
        return 1;
      case 4:
        return 2;
      case 8:
        return 3;
      default:
        throw new TypeError("Unknown type size: " + e)
    }
  }
  var Le = void 0;

  function Me(e) {
    for (var t = "", r = e; A[r];) t += Le[A[r++]];
    return t
  }
  var je = {},
    Ie = {},
    Re = {};

  function Be(e) {
    if (void 0 === e) return "_unknown";
    var t = (e = e.replace(/[^a-zA-Z0-9_]/g, "$")).charCodeAt(0);
    return t >= 48 && t <= 57 ? "_" + e : e
  }

  function Oe(e, t) {
    return e = Be(e), new Function("body", "return function " + e + '() {\n    "use strict";    return body.apply(this, arguments);\n};\n')(t)
  }

  function Ne(e, t) {
    var r = Oe(t, (function (e) {
      this.name = t, this.message = e;
      var r = new Error(e).stack;
      void 0 !== r && (this.stack = this.toString() + "\n" + r.replace(/^Error(:[^\n]*)?\n/, ""))
    }));
    return r.prototype = Object.create(e.prototype), r.prototype.constructor = r, r.prototype.toString = function () {
      return void 0 === this.message ? this.name : this.name + ": " + this.message
    }, r
  }
  var Ue = void 0;

  function ze(e) {
    throw new Ue(e)
  }
  var $e = void 0;

  function We(e) {
    throw new $e(e)
  }

  function Ge(e, t, r) {
    function n(t) {
      var n = r(t);
      n.length !== e.length && We("Mismatched type converter count");
      for (var o = 0; o < e.length; ++o) qe(e[o], n[o])
    }
    e.forEach((function (e) {
      Re[e] = t
    }));
    var o = new Array(t.length),
      a = [],
      i = 0;
    t.forEach((function (e, t) {
      Ie.hasOwnProperty(e) ? o[t] = Ie[e] : (a.push(e), je.hasOwnProperty(e) || (je[e] = []), je[e].push((function () {
        o[t] = Ie[e], ++i === a.length && n(o)
      })))
    })), 0 === a.length && n(o)
  }

  function qe(e, t, r) {
    if (r = r || {}, !("argPackAdvance" in t)) throw new TypeError("registerType registeredInstance requires argPackAdvance");
    var n = t.name;
    if (e || ze('type "' + n + '" must have a positive integer typeid pointer'), Ie.hasOwnProperty(e)) {
      if (r.ignoreDuplicateRegistrations) return;
      ze("Cannot register type '" + n + "' twice")
    }
    if (Ie[e] = t, delete Re[e], je.hasOwnProperty(e)) {
      var o = je[e];
      delete je[e], o.forEach((function (e) {
        e()
      }))
    }
  }

  function Qe(e) {
    if (!(this instanceof at)) return !1;
    if (!(e instanceof at)) return !1;
    for (var t = this.$$.ptrType.registeredClass, r = this.$$.ptr, n = e.$$.ptrType.registeredClass, o = e.$$.ptr; t.baseClass;) r = t.upcast(r), t = t.baseClass;
    for (; n.baseClass;) o = n.upcast(o), n = n.baseClass;
    return t === n && r === o
  }

  function Ye(e) {
    ze(e.$$.ptrType.registeredClass.name + " instance already deleted")
  }
  var Ve = !1;

  function Xe(e) {}

  function He(e) {
    e.count.value -= 1, 0 === e.count.value && function (e) {
      e.smartPtr ? e.smartPtrType.rawDestructor(e.smartPtr) : e.ptrType.registeredClass.rawDestructor(e.ptr)
    }(e)
  }

  function Ke(e) {
    return "undefined" == typeof FinalizationGroup ? (Ke = function (e) {
      return e
    }, e) : (Ve = new FinalizationGroup((function (e) {
      for (var t = e.next(); !t.done; t = e.next()) {
        var r = t.value;
        r.ptr ? He(r) : console.warn("object already deleted: " + r.ptr)
      }
    })), Xe = function (e) {
      Ve.unregister(e.$$)
    }, (Ke = function (e) {
      return Ve.register(e, e.$$, e.$$), e
    })(e))
  }

  function Ze() {
    if (this.$$.ptr || Ye(this), this.$$.preservePointerOnDelete) return this.$$.count.value += 1, this;
    var e, t = Ke(Object.create(Object.getPrototypeOf(this), {
      $$: {
        value: (e = this.$$, {
          count: e.count,
          deleteScheduled: e.deleteScheduled,
          preservePointerOnDelete: e.preservePointerOnDelete,
          ptr: e.ptr,
          ptrType: e.ptrType,
          smartPtr: e.smartPtr,
          smartPtrType: e.smartPtrType
        })
      }
    }));
    return t.$$.count.value += 1, t.$$.deleteScheduled = !1, t
  }

  function Je() {
    this.$$.ptr || Ye(this), this.$$.deleteScheduled && !this.$$.preservePointerOnDelete && ze("Object already scheduled for deletion"), Xe(this), He(this.$$), this.$$.preservePointerOnDelete || (this.$$.smartPtr = void 0, this.$$.ptr = void 0)
  }

  function et() {
    return !this.$$.ptr
  }
  var tt = void 0,
    rt = [];

  function nt() {
    for (; rt.length;) {
      var e = rt.pop();
      e.$$.deleteScheduled = !1, e.delete()
    }
  }

  function ot() {
    return this.$$.ptr || Ye(this), this.$$.deleteScheduled && !this.$$.preservePointerOnDelete && ze("Object already scheduled for deletion"), rt.push(this), 1 === rt.length && tt && tt(nt), this.$$.deleteScheduled = !0, this
  }

  function at() {}
  var it = {};

  function ut(e, t, r) {
    if (void 0 === e[t].overloadTable) {
      var n = e[t];
      e[t] = function () {
        return e[t].overloadTable.hasOwnProperty(arguments.length) || ze("Function '" + r + "' called with an invalid number of arguments (" + arguments.length + ") - expects one of (" + e[t].overloadTable + ")!"), e[t].overloadTable[arguments.length].apply(this, arguments)
      }, e[t].overloadTable = [], e[t].overloadTable[n.argCount] = n
    }
  }

  function st(t, r, n) {
    e.hasOwnProperty(t) ? ((void 0 === n || void 0 !== e[t].overloadTable && void 0 !== e[t].overloadTable[n]) && ze("Cannot register public name '" + t + "' twice"), ut(e, t, t), e.hasOwnProperty(n) && ze("Cannot register multiple overloads of a function with the same number of arguments (" + n + ")!"), e[t].overloadTable[n] = r) : (e[t] = r, void 0 !== n && (e[t].numArguments = n))
  }

  function ct(e, t, r, n, o, a, i, u) {
    this.name = e, this.constructor = t, this.instancePrototype = r, this.rawDestructor = n, this.baseClass = o, this.getActualType = a, this.upcast = i, this.downcast = u, this.pureVirtualFunctions = []
  }

  function lt(e, t, r) {
    for (; t !== r;) t.upcast || ze("Expected null or instance of " + r.name + ", got an instance of " + t.name), e = t.upcast(e), t = t.baseClass;
    return e
  }

  function ft(e, t) {
    if (null === t) return this.isReference && ze("null is not a valid " + this.name), 0;
    t.$$ || ze('Cannot pass "' + Qt(t) + '" as a ' + this.name), t.$$.ptr || ze("Cannot pass deleted object as a pointer of type " + this.name);
    var r = t.$$.ptrType.registeredClass;
    return lt(t.$$.ptr, r, this.registeredClass)
  }

  function dt(e, t) {
    var r;
    if (null === t) return this.isReference && ze("null is not a valid " + this.name), this.isSmartPointer ? (r = this.rawConstructor(), null !== e && e.push(this.rawDestructor, r), r) : 0;
    t.$$ || ze('Cannot pass "' + Qt(t) + '" as a ' + this.name), t.$$.ptr || ze("Cannot pass deleted object as a pointer of type " + this.name), !this.isConst && t.$$.ptrType.isConst && ze("Cannot convert argument of type " + (t.$$.smartPtrType ? t.$$.smartPtrType.name : t.$$.ptrType.name) + " to parameter type " + this.name);
    var n = t.$$.ptrType.registeredClass;
    if (r = lt(t.$$.ptr, n, this.registeredClass), this.isSmartPointer) switch (void 0 === t.$$.smartPtr && ze("Passing raw pointer to smart pointer is illegal"), this.sharingPolicy) {
      case 0:
        t.$$.smartPtrType === this ? r = t.$$.smartPtr : ze("Cannot convert argument of type " + (t.$$.smartPtrType ? t.$$.smartPtrType.name : t.$$.ptrType.name) + " to parameter type " + this.name);
        break;
      case 1:
        r = t.$$.smartPtr;
        break;
      case 2:
        if (t.$$.smartPtrType === this) r = t.$$.smartPtr;
        else {
          var o = t.clone();
          r = this.rawShare(r, Wt((function () {
            o.delete()
          }))), null !== e && e.push(this.rawDestructor, r)
        }
        break;
      default:
        ze("Unsupporting sharing policy")
    }
    return r
  }

  function pt(e, t) {
    if (null === t) return this.isReference && ze("null is not a valid " + this.name), 0;
    t.$$ || ze('Cannot pass "' + Qt(t) + '" as a ' + this.name), t.$$.ptr || ze("Cannot pass deleted object as a pointer of type " + this.name), t.$$.ptrType.isConst && ze("Cannot convert argument of type " + t.$$.ptrType.name + " to parameter type " + this.name);
    var r = t.$$.ptrType.registeredClass;
    return lt(t.$$.ptr, r, this.registeredClass)
  }

  function mt(e) {
    return this.fromWireType(I[e >> 2])
  }

  function ht(e) {
    return this.rawGetPointee && (e = this.rawGetPointee(e)), e
  }

  function vt(e) {
    this.rawDestructor && this.rawDestructor(e)
  }

  function gt(e) {
    null !== e && e.delete()
  }

  function yt(e, t, r) {
    if (t === r) return e;
    if (void 0 === r.baseClass) return null;
    var n = yt(e, t, r.baseClass);
    return null === n ? null : r.downcast(n)
  }

  function bt() {
    return Object.keys(Et).length
  }

  function wt() {
    var e = [];
    for (var t in Et) Et.hasOwnProperty(t) && e.push(Et[t]);
    return e
  }

  function _t(e) {
    tt = e, rt.length && tt && tt(nt)
  }
  var Et = {};

  function Ct(e, t) {
    return t = function (e, t) {
      for (void 0 === t && ze("ptr should not be undefined"); e.baseClass;) t = e.upcast(t), e = e.baseClass;
      return t
    }(e, t), Et[t]
  }

  function kt(e, t) {
    return t.ptrType && t.ptr || We("makeClassHandle requires ptr and ptrType"), !!t.smartPtrType != !!t.smartPtr && We("Both smartPtrType and smartPtr must be specified"), t.count = {
      value: 1
    }, Ke(Object.create(e, {
      $$: {
        value: t
      }
    }))
  }

  function xt(e) {
    var t = this.getPointee(e);
    if (!t) return this.destructor(e), null;
    var r = Ct(this.registeredClass, t);
    if (void 0 !== r) {
      if (0 === r.$$.count.value) return r.$$.ptr = t, r.$$.smartPtr = e, r.clone();
      var n = r.clone();
      return this.destructor(e), n
    }

    function o() {
      return this.isSmartPointer ? kt(this.registeredClass.instancePrototype, {
        ptrType: this.pointeeType,
        ptr: t,
        smartPtrType: this,
        smartPtr: e
      }) : kt(this.registeredClass.instancePrototype, {
        ptrType: this,
        ptr: e
      })
    }
    var a, i = this.registeredClass.getActualType(t),
      u = it[i];
    if (!u) return o.call(this);
    a = this.isConst ? u.constPointerType : u.pointerType;
    var s = yt(t, this.registeredClass, a.registeredClass);
    return null === s ? o.call(this) : this.isSmartPointer ? kt(a.registeredClass.instancePrototype, {
      ptrType: a,
      ptr: s,
      smartPtrType: this,
      smartPtr: e
    }) : kt(a.registeredClass.instancePrototype, {
      ptrType: a,
      ptr: s
    })
  }

  function St(e, t, r, n, o, a, i, u, s, c, l) {
    this.name = e, this.registeredClass = t, this.isReference = r, this.isConst = n, this.isSmartPointer = o, this.pointeeType = a, this.sharingPolicy = i, this.rawGetPointee = u, this.rawConstructor = s, this.rawShare = c, this.rawDestructor = l, o || void 0 !== t.baseClass ? this.toWireType = dt : n ? (this.toWireType = ft, this.destructorFunction = null) : (this.toWireType = pt, this.destructorFunction = null)
  }

  function Ft(t, r, n) {
    e.hasOwnProperty(t) || We("Replacing nonexistant public symbol"), void 0 !== e[t].overloadTable && void 0 !== n ? e[t].overloadTable[n] = r : (e[t] = r, e[t].argCount = n)
  }

  function Dt(t, r, n) {
    return t.includes("j") ? function (t, r, n) {
      var o = e["dynCall_" + t];
      return n && n.length ? o.apply(null, [r].concat(n)) : o.call(null, r)
    }(t, r, n) : N.get(r).apply(null, n)
  }

  function Pt(e, t) {
    var r, n, o, a = (e = Me(e)).includes("j") ? (r = e, n = t, o = [], function () {
      o.length = arguments.length;
      for (var e = 0; e < arguments.length; e++) o[e] = arguments[e];
      return Dt(r, n, o)
    }) : N.get(t);
    return "function" != typeof a && ze("unknown function pointer with signature " + e + ": " + t), a
  }
  var Tt = void 0;

  function At(e) {
    var t = zr(e),
      r = Me(t);
    return Ur(t), r
  }

  function Lt(e, t) {
    var r = [],
      n = {};
    throw t.forEach((function e(t) {
      n[t] || Ie[t] || (Re[t] ? Re[t].forEach(e) : (r.push(t), n[t] = !0))
    })), new Tt(e + ": " + r.map(At).join([", "]))
  }

  function Mt(e, t) {
    if (!(e instanceof Function)) throw new TypeError("new_ called with constructor type " + typeof e + " which is not a function");
    var r = Oe(e.name || "unknownFunctionName", (function () {}));
    r.prototype = e.prototype;
    var n = new r,
      o = e.apply(n, t);
    return o instanceof Object ? o : n
  }

  function jt(e) {
    for (; e.length;) {
      var t = e.pop();
      e.pop()(t)
    }
  }

  function It(e, t, r, n, o) {
    var a = t.length;
    a < 2 && ze("argTypes array size mismatch! Must at least get return value and 'this' types!");
    for (var i = null !== t[1] && null !== r, u = !1, s = 1; s < t.length; ++s)
      if (null !== t[s] && void 0 === t[s].destructorFunction) {
        u = !0;
        break
      } var c = "void" !== t[0].name,
      l = "",
      f = "";
    for (s = 0; s < a - 2; ++s) l += (0 !== s ? ", " : "") + "arg" + s, f += (0 !== s ? ", " : "") + "arg" + s + "Wired";
    var d = "return function " + Be(e) + "(" + l + ") {\nif (arguments.length !== " + (a - 2) + ") {\nthrowBindingError('function " + e + " called with ' + arguments.length + ' arguments, expected " + (a - 2) + " args!');\n}\n";
    u && (d += "var destructors = [];\n");
    var p = u ? "destructors" : "null",
      m = ["throwBindingError", "invoker", "fn", "runDestructors", "retType", "classParam"],
      h = [ze, n, o, jt, t[0], t[1]];
    for (i && (d += "var thisWired = classParam.toWireType(" + p + ", this);\n"), s = 0; s < a - 2; ++s) d += "var arg" + s + "Wired = argType" + s + ".toWireType(" + p + ", arg" + s + "); // " + t[s + 2].name + "\n", m.push("argType" + s), h.push(t[s + 2]);
    if (i && (f = "thisWired" + (f.length > 0 ? ", " : "") + f), d += (c ? "var rv = " : "") + "invoker(fn" + (f.length > 0 ? ", " : "") + f + ");\n", u) d += "runDestructors(destructors);\n";
    else
      for (s = i ? 1 : 2; s < t.length; ++s) {
        var v = 1 === s ? "thisWired" : "arg" + (s - 2) + "Wired";
        null !== t[s].destructorFunction && (d += v + "_dtor(" + v + "); // " + t[s].name + "\n", m.push(v + "_dtor"), h.push(t[s].destructorFunction))
      }
    return c && (d += "var ret = retType.fromWireType(rv);\nreturn ret;\n"), d += "}\n", m.push(d), Mt(Function, m).apply(null, h)
  }

  function Rt(e, t) {
    for (var r = [], n = 0; n < e; n++) r.push(j[(t >> 2) + n]);
    return r
  }

  function Bt(e, t, r) {
    return e instanceof Object || ze(r + ' with invalid "this": ' + e), e instanceof t.registeredClass.constructor || ze(r + ' incompatible with "this" of type ' + e.constructor.name), e.$$.ptr || ze("cannot call emscripten binding method " + r + " on deleted object"), lt(e.$$.ptr, e.$$.ptrType.registeredClass, t.registeredClass)
  }
  var Ot = [],
    Nt = [{}, {
      value: void 0
    }, {
      value: null
    }, {
      value: !0
    }, {
      value: !1
    }];

  function Ut(e) {
    e > 4 && 0 == --Nt[e].refcount && (Nt[e] = void 0, Ot.push(e))
  }

  function zt() {
    for (var e = 0, t = 5; t < Nt.length; ++t) void 0 !== Nt[t] && ++e;
    return e
  }

  function $t() {
    for (var e = 5; e < Nt.length; ++e)
      if (void 0 !== Nt[e]) return Nt[e];
    return null
  }

  function Wt(e) {
    switch (e) {
      case void 0:
        return 1;
      case null:
        return 2;
      case !0:
        return 3;
      case !1:
        return 4;
      default:
        var t = Ot.length ? Ot.pop() : Nt.length;
        return Nt[t] = {
          refcount: 1,
          value: e
        }, t
    }
  }

  function Gt(e, t, r) {
    switch (t) {
      case 0:
        return function (e) {
          var t = r ? T : A;
          return this.fromWireType(t[e])
        };
      case 1:
        return function (e) {
          var t = r ? L : M;
          return this.fromWireType(t[e >> 1])
        };
      case 2:
        return function (e) {
          var t = r ? j : I;
          return this.fromWireType(t[e >> 2])
        };
      default:
        throw new TypeError("Unknown integer type: " + e)
    }
  }

  function qt(e, t) {
    var r = Ie[e];
    return void 0 === r && ze(t + " has unknown type " + At(e)), r
  }

  function Qt(e) {
    if (null === e) return "null";
    var t = typeof e;
    return "object" === t || "array" === t || "function" === t ? e.toString() : "" + e
  }

  function Yt(e, t) {
    switch (t) {
      case 2:
        return function (e) {
          return this.fromWireType(B[e >> 2])
        };
      case 3:
        return function (e) {
          return this.fromWireType(O[e >> 3])
        };
      default:
        throw new TypeError("Unknown float type: " + e)
    }
  }

  function Vt(e, t, r) {
    switch (t) {
      case 0:
        return r ? function (e) {
          return T[e]
        } : function (e) {
          return A[e]
        };
      case 1:
        return r ? function (e) {
          return L[e >> 1]
        } : function (e) {
          return M[e >> 1]
        };
      case 2:
        return r ? function (e) {
          return j[e >> 2]
        } : function (e) {
          return I[e >> 2]
        };
      default:
        throw new TypeError("Unknown integer type: " + e)
    }
  }

  function Xt(e) {
    return e || ze("Cannot use deleted val. handle = " + e), Nt[e].value
  }

  function Ht(e, t) {
    for (var r = new Array(e), n = 0; n < e; ++n) r[n] = qt(j[(t >> 2) + n], "parameter " + n);
    return r
  }
  var Kt = {};

  function Zt(e) {
    var t = Kt[e];
    return void 0 === t ? Me(e) : t
  }
  var Jt = [];

  function er() {
    return "object" == typeof globalThis ? globalThis : Function("return this")()
  }
  var tr = {};

  function rr(e, t) {
    if (ir.mainLoop.timingMode = e, ir.mainLoop.timingValue = t, !ir.mainLoop.func) return 1;
    if (ir.mainLoop.running || (ir.mainLoop.running = !0), 0 == e) ir.mainLoop.scheduler = function () {
      var e = 0 | Math.max(0, ir.mainLoop.tickStartTime + t - ue());
      setTimeout(ir.mainLoop.runner, e)
    }, ir.mainLoop.method = "timeout";
    else if (1 == e) ir.mainLoop.scheduler = function () {
      ir.requestAnimationFrame(ir.mainLoop.runner)
    }, ir.mainLoop.method = "rAF";
    else if (2 == e) {
      if ("undefined" == typeof setImmediate) {
        var r = [],
          n = "setimmediate";
        addEventListener("message", (function (e) {
          e.data !== n && e.data.target !== n || (e.stopPropagation(), r.shift()())
        }), !0), setImmediate = function (e) {
          r.push(e), postMessage(n, "*")
        }
      }
      ir.mainLoop.scheduler = function () {
        setImmediate(ir.mainLoop.runner)
      }, ir.mainLoop.method = "immediate"
    }
    return 0
  }

  function nr(t) {
    ! function (t, r) {
      b = t, r && fe() && 0 === t || (fe() || (e.onExit && e.onExit(t), w = !0), s(t, new Vr(t)))
    }(t)
  }

  function or(e, t, r, n, o) {
    _(!ir.mainLoop.func, "emscripten_set_main_loop: there can only be one main loop function at once: call emscripten_cancel_main_loop to cancel the previous one before setting a new one with different parameters."), ir.mainLoop.func = e, ir.mainLoop.arg = n;
    var a = ir.mainLoop.currentlyRunningMainloop;

    function i() {
      return !(a < ir.mainLoop.currentlyRunningMainloop && (function () {
        if (!fe()) try {
          nr(b)
        } catch (e) {
          if (e instanceof Vr) return;
          throw e
        }
      }(), 1))
    }
    if (ir.mainLoop.running = !1, ir.mainLoop.runner = function () {
        if (!w)
          if (ir.mainLoop.queue.length > 0) {
            var t = Date.now(),
              r = ir.mainLoop.queue.shift();
            if (r.func(r.arg), ir.mainLoop.remainingBlockers) {
              var n = ir.mainLoop.remainingBlockers,
                o = n % 1 == 0 ? n - 1 : Math.floor(n);
              r.counted ? ir.mainLoop.remainingBlockers = o : (o += .5, ir.mainLoop.remainingBlockers = (8 * n + o) / 9)
            }
            if (console.log('main loop blocker "' + r.name + '" took ' + (Date.now() - t) + " ms"), ir.mainLoop.updateStatus(), !i()) return;
            setTimeout(ir.mainLoop.runner, 0)
          } else i() && (ir.mainLoop.currentFrameNumber = ir.mainLoop.currentFrameNumber + 1 | 0, 1 == ir.mainLoop.timingMode && ir.mainLoop.timingValue > 1 && ir.mainLoop.currentFrameNumber % ir.mainLoop.timingValue != 0 ? ir.mainLoop.scheduler() : (0 == ir.mainLoop.timingMode && (ir.mainLoop.tickStartTime = ue()), ir.mainLoop.runIter(e), i() && ("object" == typeof SDL && SDL.audio && SDL.audio.queueNewAudioData && SDL.audio.queueNewAudioData(), ir.mainLoop.scheduler())))
      }, o || (t && t > 0 ? rr(0, 1e3 / t) : rr(1, 1), ir.mainLoop.scheduler()), r) throw "unwind"
  }

  function ar(e, t) {
    if (!w)
      if (t) e();
      else try {
        e()
      } catch (r) {
        if (r instanceof Vr) return;
        if ("unwind" !== r) throw r && "object" == typeof r && r.stack && f("exception thrown: " + [r, r.stack]), r
      }
  }
  var ir = {
      mainLoop: {
        running: !1,
        scheduler: null,
        method: "",
        currentlyRunningMainloop: 0,
        func: null,
        arg: 0,
        timingMode: 0,
        timingValue: 0,
        currentFrameNumber: 0,
        queue: [],
        pause: function () {
          ir.mainLoop.scheduler = null, ir.mainLoop.currentlyRunningMainloop++
        },
        resume: function () {
          ir.mainLoop.currentlyRunningMainloop++;
          var e = ir.mainLoop.timingMode,
            t = ir.mainLoop.timingValue,
            r = ir.mainLoop.func;
          ir.mainLoop.func = null, or(r, 0, !1, ir.mainLoop.arg, !0), rr(e, t), ir.mainLoop.scheduler()
        },
        updateStatus: function () {
          if (e.setStatus) {
            var t = e.statusMessage || "Please wait...",
              r = ir.mainLoop.remainingBlockers,
              n = ir.mainLoop.expectedBlockers;
            r ? r < n ? e.setStatus(t + " (" + (n - r) + "/" + n + ")") : e.setStatus(t) : e.setStatus("")
          }
        },
        runIter: function (t) {
          if (!w) {
            if (e.preMainLoop && !1 === e.preMainLoop()) return;
            ar(t), e.postMainLoop && e.postMainLoop()
          }
        }
      },
      isFullscreen: !1,
      pointerLock: !1,
      moduleContextCreatedCallbacks: [],
      workers: [],
      init: function () {
        if (e.preloadPlugins || (e.preloadPlugins = []), !ir.initted) {
          ir.initted = !0;
          try {
            new Blob, ir.hasBlobConstructor = !0
          } catch (a) {
            ir.hasBlobConstructor = !1, console.log("warning: no blob constructor, cannot create blobs with mimetypes")
          }
          ir.BlobBuilder = "undefined" != typeof MozBlobBuilder ? MozBlobBuilder : "undefined" != typeof WebKitBlobBuilder ? WebKitBlobBuilder : ir.hasBlobConstructor ? null : console.log("warning: no BlobBuilder"), ir.URLObject = "undefined" != typeof window ? window.URL ? window.URL : window.webkitURL : void 0, e.noImageDecoding || void 0 !== ir.URLObject || (console.log("warning: Browser does not support creating object URLs. Built-in browser image decoding will not be available."), e.noImageDecoding = !0);
          var t = {
            canHandle: function (t) {
              return !e.noImageDecoding && /\.(jpg|jpeg|png|bmp)$/i.test(t)
            },
            handle: function (t, r, n, o) {
              var i = null;
              if (ir.hasBlobConstructor) try {
                (i = new Blob([t], {
                  type: ir.getMimetype(r)
                })).size !== t.length && (i = new Blob([new Uint8Array(t).buffer], {
                  type: ir.getMimetype(r)
                }))
              } catch (a) {
                d("Blob constructor present but fails: " + a + "; falling back to blob builder")
              }
              if (!i) {
                var u = new ir.BlobBuilder;
                u.append(new Uint8Array(t).buffer), i = u.getBlob()
              }
              var s = ir.URLObject.createObjectURL(i),
                c = new Image;
              c.onload = function () {
                _(c.complete, "Image " + r + " could not be decoded");
                var o = document.createElement("canvas");
                o.width = c.width, o.height = c.height, o.getContext("2d").drawImage(c, 0, 0), e.preloadedImages[r] = o, ir.URLObject.revokeObjectURL(s), n && n(t)
              }, c.onerror = function (e) {
                console.log("Image " + s + " could not be decoded"), o && o()
              }, c.src = s
            }
          };
          e.preloadPlugins.push(t);
          var r = {
            canHandle: function (t) {
              return !e.noAudioDecoding && t.substr(-4) in {
                ".ogg": 1,
                ".wav": 1,
                ".mp3": 1
              }
            },
            handle: function (t, r, n, o) {
              var i = !1;

              function u(o) {
                i || (i = !0, e.preloadedAudios[r] = o, n && n(t))
              }

              function s() {
                i || (i = !0, e.preloadedAudios[r] = new Audio, o && o())
              }
              if (!ir.hasBlobConstructor) return s();
              try {
                var c = new Blob([t], {
                  type: ir.getMimetype(r)
                })
              } catch (a) {
                return s()
              }
              var l = ir.URLObject.createObjectURL(c),
                f = new Audio;
              f.addEventListener("canplaythrough", (function () {
                u(f)
              }), !1), f.onerror = function (e) {
                i || (console.log("warning: browser could not fully decode audio " + r + ", trying slower base64 approach"), f.src = "data:audio/x-" + r.substr(-3) + ";base64," + function (e) {
                  for (var t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", r = "", n = 0, o = 0, a = 0; a < e.length; a++)
                    for (n = n << 8 | e[a], o += 8; o >= 6;) {
                      var i = n >> o - 6 & 63;
                      o -= 6, r += t[i]
                    }
                  return 2 == o ? (r += t[(3 & n) << 4], r += "==") : 4 == o && (r += t[(15 & n) << 2], r += "="), r
                }(t), u(f))
              }, f.src = l, ir.safeSetTimeout((function () {
                u(f)
              }), 1e4)
            }
          };
          e.preloadPlugins.push(r);
          var n = e.canvas;
          n && (n.requestPointerLock = n.requestPointerLock || n.mozRequestPointerLock || n.webkitRequestPointerLock || n.msRequestPointerLock || function () {}, n.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock || document.webkitExitPointerLock || document.msExitPointerLock || function () {}, n.exitPointerLock = n.exitPointerLock.bind(document), document.addEventListener("pointerlockchange", o, !1), document.addEventListener("mozpointerlockchange", o, !1), document.addEventListener("webkitpointerlockchange", o, !1), document.addEventListener("mspointerlockchange", o, !1), e.elementPointerLock && n.addEventListener("click", (function (t) {
            !ir.pointerLock && e.canvas.requestPointerLock && (e.canvas.requestPointerLock(), t.preventDefault())
          }), !1))
        }

        function o() {
          ir.pointerLock = document.pointerLockElement === e.canvas || document.mozPointerLockElement === e.canvas || document.webkitPointerLockElement === e.canvas || document.msPointerLockElement === e.canvas
        }
      },
      createContext: function (t, r, n, o) {
        if (r && e.ctx && t == e.canvas) return e.ctx;
        var a, i;
        if (r) {
          var u = {
            antialias: !1,
            alpha: !1,
            majorVersion: "undefined" != typeof WebGL2RenderingContext ? 2 : 1
          };
          if (o)
            for (var s in o) u[s] = o[s];
          void 0 !== dr && (i = dr.createContext(t, u)) && (a = dr.getContext(i).GLctx)
        } else a = t.getContext("2d");
        return a ? (n && (r || _(void 0 === Pr, "cannot set in module if GLctx is used, but we are a non-GL context that would replace it"), e.ctx = a, r && dr.makeContextCurrent(i), e.useWebGL = r, ir.moduleContextCreatedCallbacks.forEach((function (e) {
          e()
        })), ir.init()), a) : null
      },
      destroyContext: function (e, t, r) {},
      fullscreenHandlersInstalled: !1,
      lockPointer: void 0,
      resizeCanvas: void 0,
      requestFullscreen: function (t, r) {
        ir.lockPointer = t, ir.resizeCanvas = r, void 0 === ir.lockPointer && (ir.lockPointer = !0), void 0 === ir.resizeCanvas && (ir.resizeCanvas = !1);
        var n = e.canvas;

        function o() {
          ir.isFullscreen = !1;
          var t = n.parentNode;
          (document.fullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || document.webkitFullscreenElement || document.webkitCurrentFullScreenElement) === t ? (n.exitFullscreen = ir.exitFullscreen, ir.lockPointer && n.requestPointerLock(), ir.isFullscreen = !0, ir.resizeCanvas ? ir.setFullscreenCanvasSize() : ir.updateCanvasDimensions(n)) : (t.parentNode.insertBefore(n, t), t.parentNode.removeChild(t), ir.resizeCanvas ? ir.setWindowedCanvasSize() : ir.updateCanvasDimensions(n)), e.onFullScreen && e.onFullScreen(ir.isFullscreen), e.onFullscreen && e.onFullscreen(ir.isFullscreen)
        }
        ir.fullscreenHandlersInstalled || (ir.fullscreenHandlersInstalled = !0, document.addEventListener("fullscreenchange", o, !1), document.addEventListener("mozfullscreenchange", o, !1), document.addEventListener("webkitfullscreenchange", o, !1), document.addEventListener("MSFullscreenChange", o, !1));
        var a = document.createElement("div");
        n.parentNode.insertBefore(a, n), a.appendChild(n), a.requestFullscreen = a.requestFullscreen || a.mozRequestFullScreen || a.msRequestFullscreen || (a.webkitRequestFullscreen ? function () {
          a.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT)
        } : null) || (a.webkitRequestFullScreen ? function () {
          a.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT)
        } : null), a.requestFullscreen()
      },
      exitFullscreen: function () {
        return !!ir.isFullscreen && ((document.exitFullscreen || document.cancelFullScreen || document.mozCancelFullScreen || document.msExitFullscreen || document.webkitCancelFullScreen || function () {}).apply(document, []), !0)
      },
      nextRAF: 0,
      fakeRequestAnimationFrame: function (e) {
        var t = Date.now();
        if (0 === ir.nextRAF) ir.nextRAF = t + 1e3 / 60;
        else
          for (; t + 2 >= ir.nextRAF;) ir.nextRAF += 1e3 / 60;
        var r = Math.max(ir.nextRAF - t, 0);
        setTimeout(e, r)
      },
      requestAnimationFrame: function (e) {
        "function" != typeof requestAnimationFrame ? (0, ir.fakeRequestAnimationFrame)(e) : requestAnimationFrame(e)
      },
      safeRequestAnimationFrame: function (e) {
        return ir.requestAnimationFrame((function () {
          ar(e)
        }))
      },
      safeSetTimeout: function (e, t) {
        return setTimeout((function () {
          ar(e)
        }), t)
      },
      getMimetype: function (e) {
        return {
          jpg: "image/jpeg",
          jpeg: "image/jpeg",
          png: "image/png",
          bmp: "image/bmp",
          ogg: "audio/ogg",
          wav: "audio/wav",
          mp3: "audio/mpeg"
        } [e.substr(e.lastIndexOf(".") + 1)]
      },
      getUserMedia: function (e) {
        window.getUserMedia || (window.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia), window.getUserMedia(e)
      },
      getMovementX: function (e) {
        return e.movementX || e.mozMovementX || e.webkitMovementX || 0
      },
      getMovementY: function (e) {
        return e.movementY || e.mozMovementY || e.webkitMovementY || 0
      },
      getMouseWheelDelta: function (e) {
        var t = 0;
        switch (e.type) {
          case "DOMMouseScroll":
            t = e.detail / 3;
            break;
          case "mousewheel":
            t = e.wheelDelta / 120;
            break;
          case "wheel":
            switch (t = e.deltaY, e.deltaMode) {
              case 0:
                t /= 100;
                break;
              case 1:
                t /= 3;
                break;
              case 2:
                t *= 80;
                break;
              default:
                throw "unrecognized mouse wheel delta mode: " + e.deltaMode
            }
            break;
          default:
            throw "unrecognized mouse wheel event: " + e.type
        }
        return t
      },
      mouseX: 0,
      mouseY: 0,
      mouseMovementX: 0,
      mouseMovementY: 0,
      touches: {},
      lastTouches: {},
      calculateMouseEvent: function (t) {
        if (ir.pointerLock) "mousemove" != t.type && "mozMovementX" in t ? ir.mouseMovementX = ir.mouseMovementY = 0 : (ir.mouseMovementX = ir.getMovementX(t), ir.mouseMovementY = ir.getMovementY(t)), "undefined" != typeof SDL ? (ir.mouseX = SDL.mouseX + ir.mouseMovementX, ir.mouseY = SDL.mouseY + ir.mouseMovementY) : (ir.mouseX += ir.mouseMovementX, ir.mouseY += ir.mouseMovementY);
        else {
          var r = e.canvas.getBoundingClientRect(),
            n = e.canvas.width,
            o = e.canvas.height,
            a = void 0 !== window.scrollX ? window.scrollX : window.pageXOffset,
            i = void 0 !== window.scrollY ? window.scrollY : window.pageYOffset;
          if ("touchstart" === t.type || "touchend" === t.type || "touchmove" === t.type) {
            var u = t.touch;
            if (void 0 === u) return;
            var s = u.pageX - (a + r.left),
              c = u.pageY - (i + r.top),
              l = {
                x: s *= n / r.width,
                y: c *= o / r.height
              };
            if ("touchstart" === t.type) ir.lastTouches[u.identifier] = l, ir.touches[u.identifier] = l;
            else if ("touchend" === t.type || "touchmove" === t.type) {
              var f = ir.touches[u.identifier];
              f || (f = l), ir.lastTouches[u.identifier] = f, ir.touches[u.identifier] = l
            }
            return
          }
          var d = t.pageX - (a + r.left),
            p = t.pageY - (i + r.top);
          d *= n / r.width, p *= o / r.height, ir.mouseMovementX = d - ir.mouseX, ir.mouseMovementY = p - ir.mouseY, ir.mouseX = d, ir.mouseY = p
        }
      },
      asyncLoad: function (e, t, r, n) {
        var o = n ? "" : "al " + e;
        i(e, (function (r) {
          _(r, 'Loading data file "' + e + '" failed (no arrayBuffer).'), t(new Uint8Array(r)), o && te()
        }), (function (t) {
          if (!r) throw 'Loading data file "' + e + '" failed.';
          r()
        })), o && ee()
      },
      resizeListeners: [],
      updateResizeListeners: function () {
        var t = e.canvas;
        ir.resizeListeners.forEach((function (e) {
          e(t.width, t.height)
        }))
      },
      setCanvasSize: function (t, r, n) {
        var o = e.canvas;
        ir.updateCanvasDimensions(o, t, r), n || ir.updateResizeListeners()
      },
      windowedWidth: 0,
      windowedHeight: 0,
      setFullscreenCanvasSize: function () {
        if ("undefined" != typeof SDL) {
          var t = I[SDL.screen >> 2];
          t |= 8388608, j[SDL.screen >> 2] = t
        }
        ir.updateCanvasDimensions(e.canvas), ir.updateResizeListeners()
      },
      setWindowedCanvasSize: function () {
        if ("undefined" != typeof SDL) {
          var t = I[SDL.screen >> 2];
          t &= -8388609, j[SDL.screen >> 2] = t
        }
        ir.updateCanvasDimensions(e.canvas), ir.updateResizeListeners()
      },
      updateCanvasDimensions: function (t, r, n) {
        r && n ? (t.widthNative = r, t.heightNative = n) : (r = t.widthNative, n = t.heightNative);
        var o = r,
          a = n;
        if (e.forcedAspectRatio && e.forcedAspectRatio > 0 && (o / a < e.forcedAspectRatio ? o = Math.round(a * e.forcedAspectRatio) : a = Math.round(o / e.forcedAspectRatio)), (document.fullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || document.webkitFullscreenElement || document.webkitCurrentFullScreenElement) === t.parentNode && "undefined" != typeof screen) {
          var i = Math.min(screen.width / o, screen.height / a);
          o = Math.round(o * i), a = Math.round(a * i)
        }
        ir.resizeCanvas ? (t.width != o && (t.width = o), t.height != a && (t.height = a), void 0 !== t.style && (t.style.removeProperty("width"), t.style.removeProperty("height"))) : (t.width != r && (t.width = r), t.height != n && (t.height = n), void 0 !== t.style && (o != r || a != n ? (t.style.setProperty("width", o + "px", "important"), t.style.setProperty("height", a + "px", "important")) : (t.style.removeProperty("width"), t.style.removeProperty("height"))))
      },
      wgetRequests: {},
      nextWgetRequestHandle: 0,
      getNextWgetRequestHandle: function () {
        var e = ir.nextWgetRequestHandle;
        return ir.nextWgetRequestHandle++, e
      }
    },
    ur = {
      QUEUE_INTERVAL: 25,
      QUEUE_LOOKAHEAD: .1,
      DEVICE_NAME: "Emscripten OpenAL",
      CAPTURE_DEVICE_NAME: "Emscripten OpenAL capture",
      ALC_EXTENSIONS: {
        ALC_SOFT_pause_device: !0,
        ALC_SOFT_HRTF: !0
      },
      AL_EXTENSIONS: {
        AL_EXT_float32: !0,
        AL_SOFT_loop_points: !0,
        AL_SOFT_source_length: !0,
        AL_EXT_source_distance_model: !0,
        AL_SOFT_source_spatialize: !0
      },
      _alcErr: 0,
      alcErr: 0,
      deviceRefCounts: {},
      alcStringCache: {},
      paused: !1,
      stringCache: {},
      contexts: {},
      currentCtx: null,
      buffers: {
        0: {
          id: 0,
          refCount: 0,
          audioBuf: null,
          frequency: 0,
          bytesPerSample: 2,
          channels: 1,
          length: 0
        }
      },
      paramArray: [],
      _nextId: 1,
      newId: function () {
        return ur.freeIds.length > 0 ? ur.freeIds.pop() : ur._nextId++
      },
      freeIds: [],
      scheduleContextAudio: function (e) {
        if (1 !== ir.mainLoop.timingMode || "visible" == document.visibilityState)
          for (var t in e.sources) ur.scheduleSourceAudio(e.sources[t])
      },
      scheduleSourceAudio: function (e, t) {
        if ((1 !== ir.mainLoop.timingMode || "visible" == document.visibilityState) && 4114 === e.state) {
          for (var r = ur.updateSourceTime(e), n = e.bufStartTime, o = e.bufOffset, a = e.bufsProcessed, i = 0; i < e.audioQueue.length; i++) n = (l = e.audioQueue[i])._startTime + l._duration, o = 0, a += l._skipCount + 1;
          t || (t = ur.QUEUE_LOOKAHEAD);
          for (var u = r + t, s = 0; n < u;) {
            if (a >= e.bufQueue.length) {
              if (!e.looping) break;
              a %= e.bufQueue.length
            }
            var c = e.bufQueue[a % e.bufQueue.length];
            if (0 === c.length) {
              if (++s === e.bufQueue.length) break
            } else {
              var l;
              (l = e.context.audioCtx.createBufferSource()).buffer = c.audioBuf, l.playbackRate.value = e.playbackRate, (c.audioBuf._loopStart || c.audioBuf._loopEnd) && (l.loopStart = c.audioBuf._loopStart, l.loopEnd = c.audioBuf._loopEnd);
              var f = 0;
              4136 === e.type && e.looping ? (f = Number.POSITIVE_INFINITY, l.loop = !0, c.audioBuf._loopStart && (l.loopStart = c.audioBuf._loopStart), c.audioBuf._loopEnd && (l.loopEnd = c.audioBuf._loopEnd)) : f = (c.audioBuf.duration - o) / e.playbackRate, l._startOffset = o, l._duration = f, l._skipCount = s, s = 0, l.connect(e.gain), void 0 !== l.start ? (n = Math.max(n, e.context.audioCtx.currentTime), l.start(n, o)) : void 0 !== l.noteOn && (n = Math.max(n, e.context.audioCtx.currentTime), l.noteOn(n)), l._startTime = n, e.audioQueue.push(l), n += f
            }
            o = 0, a++
          }
        }
      },
      updateSourceTime: function (e) {
        var t = e.context.audioCtx.currentTime;
        if (4114 !== e.state) return t;
        isFinite(e.bufStartTime) || (e.bufStartTime = t - e.bufOffset / e.playbackRate, e.bufOffset = 0);
        for (var r = 0; e.audioQueue.length;) {
          var n = e.audioQueue[0];
          if (e.bufsProcessed += n._skipCount, t < (r = n._startTime + n._duration)) break;
          e.audioQueue.shift(), e.bufStartTime = r, e.bufOffset = 0, e.bufsProcessed++
        }
        if (e.bufsProcessed >= e.bufQueue.length && !e.looping) ur.setSourceState(e, 4116);
        else if (4136 === e.type && e.looping)
          if (0 === (c = e.bufQueue[0]).length) e.bufOffset = 0;
          else {
            var o = (t - e.bufStartTime) * e.playbackRate,
              a = c.audioBuf._loopStart || 0,
              i = c.audioBuf._loopEnd || c.audioBuf.duration;
            i <= a && (i = c.audioBuf.duration), e.bufOffset = o < i ? o : a + (o - a) % (i - a)
          }
        else if (e.audioQueue[0]) e.bufOffset = (t - e.audioQueue[0]._startTime) * e.playbackRate;
        else {
          if (4136 !== e.type && e.looping) {
            var u = ur.sourceDuration(e) / e.playbackRate;
            u > 0 && (e.bufStartTime += Math.floor((t - e.bufStartTime) / u) * u)
          }
          for (var s = 0; s < e.bufQueue.length; s++) {
            if (e.bufsProcessed >= e.bufQueue.length) {
              if (!e.looping) {
                ur.setSourceState(e, 4116);
                break
              }
              e.bufsProcessed %= e.bufQueue.length
            }
            var c;
            if ((c = e.bufQueue[e.bufsProcessed]).length > 0) {
              if (t < (r = e.bufStartTime + c.audioBuf.duration / e.playbackRate)) {
                e.bufOffset = (t - e.bufStartTime) * e.playbackRate;
                break
              }
              e.bufStartTime = r
            }
            e.bufOffset = 0, e.bufsProcessed++
          }
        }
        return t
      },
      cancelPendingSourceAudio: function (e) {
        ur.updateSourceTime(e);
        for (var t = 1; t < e.audioQueue.length; t++) e.audioQueue[t].stop();
        e.audioQueue.length > 1 && (e.audioQueue.length = 1)
      },
      stopSourceAudio: function (e) {
        for (var t = 0; t < e.audioQueue.length; t++) e.audioQueue[t].stop();
        e.audioQueue.length = 0
      },
      setSourceState: function (e, t) {
        4114 === t ? (4114 !== e.state && 4116 != e.state || (e.bufsProcessed = 0, e.bufOffset = 0), ur.stopSourceAudio(e), e.state = 4114, e.bufStartTime = Number.NEGATIVE_INFINITY, ur.scheduleSourceAudio(e)) : 4115 === t ? 4114 === e.state && (ur.updateSourceTime(e), ur.stopSourceAudio(e), e.state = 4115) : 4116 === t ? 4113 !== e.state && (e.state = 4116, e.bufsProcessed = e.bufQueue.length, e.bufStartTime = Number.NEGATIVE_INFINITY, e.bufOffset = 0, ur.stopSourceAudio(e)) : 4113 === t && 4113 !== e.state && (e.state = 4113, e.bufsProcessed = 0, e.bufStartTime = Number.NEGATIVE_INFINITY, e.bufOffset = 0, ur.stopSourceAudio(e))
      },
      initSourcePanner: function (e) {
        if (4144 !== e.type) {
          for (var t = ur.buffers[0], r = 0; r < e.bufQueue.length; r++)
            if (0 !== e.bufQueue[r].id) {
              t = e.bufQueue[r];
              break
            } if (1 === e.spatialize || 2 === e.spatialize && 1 === t.channels) {
            if (e.panner) return;
            e.panner = e.context.audioCtx.createPanner(), ur.updateSourceGlobal(e), ur.updateSourceSpace(e), e.panner.connect(e.context.gain), e.gain.disconnect(), e.gain.connect(e.panner)
          } else {
            if (!e.panner) return;
            e.panner.disconnect(), e.gain.disconnect(), e.gain.connect(e.context.gain), e.panner = null
          }
        }
      },
      updateContextGlobal: function (e) {
        for (var t in e.sources) ur.updateSourceGlobal(e.sources[t])
      },
      updateSourceGlobal: function (e) {
        var t = e.panner;
        if (t) switch (t.refDistance = e.refDistance, t.maxDistance = e.maxDistance, t.rolloffFactor = e.rolloffFactor, t.panningModel = e.context.hrtf ? "HRTF" : "equalpower", e.context.sourceDistanceModel ? e.distanceModel : e.context.distanceModel) {
          case 0:
            t.distanceModel = "inverse", t.refDistance = 340282e33;
            break;
          case 53249:
          case 53250:
            t.distanceModel = "inverse";
            break;
          case 53251:
          case 53252:
            t.distanceModel = "linear";
            break;
          case 53253:
          case 53254:
            t.distanceModel = "exponential"
        }
      },
      updateListenerSpace: function (e) {
        var t = e.audioCtx.listener;
        for (var r in t.positionX ? (t.positionX.value = e.listener.position[0], t.positionY.value = e.listener.position[1], t.positionZ.value = e.listener.position[2]) : t.setPosition(e.listener.position[0], e.listener.position[1], e.listener.position[2]), t.forwardX ? (t.forwardX.value = e.listener.direction[0], t.forwardY.value = e.listener.direction[1], t.forwardZ.value = e.listener.direction[2], t.upX.value = e.listener.up[0], t.upY.value = e.listener.up[1], t.upZ.value = e.listener.up[2]) : t.setOrientation(e.listener.direction[0], e.listener.direction[1], e.listener.direction[2], e.listener.up[0], e.listener.up[1], e.listener.up[2]), e.sources) ur.updateSourceSpace(e.sources[r])
      },
      updateSourceSpace: function (e) {
        if (e.panner) {
          var t = e.panner,
            r = e.position[0],
            n = e.position[1],
            o = e.position[2],
            a = e.direction[0],
            i = e.direction[1],
            u = e.direction[2],
            s = e.context.listener,
            c = s.position[0],
            l = s.position[1],
            f = s.position[2];
          if (e.relative) {
            var d = -s.direction[0],
              p = -s.direction[1],
              m = -s.direction[2],
              h = s.up[0],
              v = s.up[1],
              g = s.up[2],
              y = function (e, t, r) {
                var n = Math.sqrt(e * e + t * t + r * r);
                return n < Number.EPSILON ? 0 : 1 / n
              },
              b = y(d, p, m);
            d *= b, p *= b, m *= b;
            var w = (v *= b = y(h, v, g)) * m - (g *= b) * p,
              _ = g * d - (h *= b) * m,
              E = h * p - v * d,
              C = a,
              k = i,
              x = u;
            a = C * (w *= b = y(w, _, E)) + k * (h = p * (E *= b) - m * (_ *= b)) + x * d, i = C * _ + k * (v = m * w - d * E) + x * p, u = C * E + k * (g = d * _ - p * w) + x * m, r = (C = r) * w + (k = n) * h + (x = o) * d, n = C * _ + k * v + x * p, o = C * E + k * g + x * m, r += c, n += l, o += f
          }
          t.positionX ? (t.positionX.value = r, t.positionY.value = n, t.positionZ.value = o) : t.setPosition(r, n, o), t.orientationX ? (t.orientationX.value = a, t.orientationY.value = i, t.orientationZ.value = u) : t.setOrientation(a, i, u);
          var S = e.dopplerShift,
            F = e.velocity[0],
            D = e.velocity[1],
            P = e.velocity[2],
            T = s.velocity[0],
            A = s.velocity[1],
            L = s.velocity[2];
          if (r === c && n === l && o === f || F === T && D === A && P === L) e.dopplerShift = 1;
          else {
            var M = e.context.speedOfSound,
              j = e.context.dopplerFactor,
              I = c - r,
              R = l - n,
              B = f - o,
              O = Math.sqrt(I * I + R * R + B * B),
              N = (I * T + R * A + B * L) / O,
              U = (I * F + R * D + B * P) / O;
            N = Math.min(N, M / j), U = Math.min(U, M / j), e.dopplerShift = (M - j * N) / (M - j * U)
          }
          e.dopplerShift !== S && ur.updateSourceRate(e)
        }
      },
      updateSourceRate: function (e) {
        if (4114 === e.state) {
          ur.cancelPendingSourceAudio(e);
          var t, r = e.audioQueue[0];
          if (!r) return;
          t = 4136 === e.type && e.looping ? Number.POSITIVE_INFINITY : (r.buffer.duration - r._startOffset) / e.playbackRate, r._duration = t, r.playbackRate.value = e.playbackRate, ur.scheduleSourceAudio(e)
        }
      },
      sourceDuration: function (e) {
        for (var t = 0, r = 0; r < e.bufQueue.length; r++) {
          var n = e.bufQueue[r].audioBuf;
          t += n ? n.duration : 0
        }
        return t
      },
      sourceTell: function (e) {
        ur.updateSourceTime(e);
        for (var t = 0, r = 0; r < e.bufsProcessed; r++) t += e.bufQueue[r].audioBuf.duration;
        return t += e.bufOffset
      },
      sourceSeek: function (e, t) {
        var r = 4114 == e.state;
        if (r && ur.setSourceState(e, 4113), null !== e.bufQueue[e.bufsProcessed].audioBuf) {
          for (e.bufsProcessed = 0; t > e.bufQueue[e.bufsProcessed].audioBuf.duration;) t -= e.bufQueue[e.bufsProcessed].audiobuf.duration, e.bufsProcessed++;
          e.bufOffset = t
        }
        r && ur.setSourceState(e, 4114)
      },
      getGlobalParam: function (e, t) {
        if (!ur.currentCtx) return null;
        switch (t) {
          case 49152:
            return ur.currentCtx.dopplerFactor;
          case 49155:
            return ur.currentCtx.speedOfSound;
          case 53248:
            return ur.currentCtx.distanceModel;
          default:
            return ur.currentCtx.err = 40962, null
        }
      },
      setGlobalParam: function (e, t, r) {
        if (ur.currentCtx) switch (t) {
          case 49152:
            if (!Number.isFinite(r) || r < 0) return void(ur.currentCtx.err = 40963);
            ur.currentCtx.dopplerFactor = r, ur.updateListenerSpace(ur.currentCtx);
            break;
          case 49155:
            if (!Number.isFinite(r) || r <= 0) return void(ur.currentCtx.err = 40963);
            ur.currentCtx.speedOfSound = r, ur.updateListenerSpace(ur.currentCtx);
            break;
          case 53248:
            switch (r) {
              case 0:
              case 53249:
              case 53250:
              case 53251:
              case 53252:
              case 53253:
              case 53254:
                ur.currentCtx.distanceModel = r, ur.updateContextGlobal(ur.currentCtx);
                break;
              default:
                return void(ur.currentCtx.err = 40963)
            }
            break;
          default:
            return void(ur.currentCtx.err = 40962)
        }
      },
      getListenerParam: function (e, t) {
        if (!ur.currentCtx) return null;
        switch (t) {
          case 4100:
            return ur.currentCtx.listener.position;
          case 4102:
            return ur.currentCtx.listener.velocity;
          case 4111:
            return ur.currentCtx.listener.direction.concat(ur.currentCtx.listener.up);
          case 4106:
            return ur.currentCtx.gain.gain.value;
          default:
            return ur.currentCtx.err = 40962, null
        }
      },
      setListenerParam: function (e, t, r) {
        if (ur.currentCtx)
          if (null !== r) {
            var n = ur.currentCtx.listener;
            switch (t) {
              case 4100:
                if (!Number.isFinite(r[0]) || !Number.isFinite(r[1]) || !Number.isFinite(r[2])) return void(ur.currentCtx.err = 40963);
                n.position[0] = r[0], n.position[1] = r[1], n.position[2] = r[2], ur.updateListenerSpace(ur.currentCtx);
                break;
              case 4102:
                if (!Number.isFinite(r[0]) || !Number.isFinite(r[1]) || !Number.isFinite(r[2])) return void(ur.currentCtx.err = 40963);
                n.velocity[0] = r[0], n.velocity[1] = r[1], n.velocity[2] = r[2], ur.updateListenerSpace(ur.currentCtx);
                break;
              case 4106:
                if (!Number.isFinite(r) || r < 0) return void(ur.currentCtx.err = 40963);
                ur.currentCtx.gain.gain.value = r;
                break;
              case 4111:
                if (!(Number.isFinite(r[0]) && Number.isFinite(r[1]) && Number.isFinite(r[2]) && Number.isFinite(r[3]) && Number.isFinite(r[4]) && Number.isFinite(r[5]))) return void(ur.currentCtx.err = 40963);
                n.direction[0] = r[0], n.direction[1] = r[1], n.direction[2] = r[2], n.up[0] = r[3], n.up[1] = r[4], n.up[2] = r[5], ur.updateListenerSpace(ur.currentCtx);
                break;
              default:
                return void(ur.currentCtx.err = 40962)
            }
          } else ur.currentCtx.err = 40962
      },
      getBufferParam: function (e, t, r) {
        if (ur.currentCtx) {
          var n = ur.buffers[t];
          if (n && 0 !== t) switch (r) {
            case 8193:
              return n.frequency;
            case 8194:
              return 8 * n.bytesPerSample;
            case 8195:
              return n.channels;
            case 8196:
              return n.length * n.bytesPerSample * n.channels;
            case 8213:
              return 0 === n.length ? [0, 0] : [(n.audioBuf._loopStart || 0) * n.frequency, (n.audioBuf._loopEnd || n.length) * n.frequency];
            default:
              return ur.currentCtx.err = 40962, null
          } else ur.currentCtx.err = 40961
        }
      },
      setBufferParam: function (e, t, r, n) {
        if (ur.currentCtx) {
          var o = ur.buffers[t];
          if (o && 0 !== t)
            if (null !== n) switch (r) {
              case 8196:
                if (0 !== n) return void(ur.currentCtx.err = 40963);
                break;
              case 8213:
                if (n[0] < 0 || n[0] > o.length || n[1] < 0 || n[1] > o.Length || n[0] >= n[1]) return void(ur.currentCtx.err = 40963);
                if (o.refCount > 0) return void(ur.currentCtx.err = 40964);
                o.audioBuf && (o.audioBuf._loopStart = n[0] / o.frequency, o.audioBuf._loopEnd = n[1] / o.frequency);
                break;
              default:
                return void(ur.currentCtx.err = 40962)
            } else ur.currentCtx.err = 40962;
            else ur.currentCtx.err = 40961
        }
      },
      getSourceParam: function (e, t, r) {
        if (!ur.currentCtx) return null;
        var n = ur.currentCtx.sources[t];
        if (!n) return ur.currentCtx.err = 40961, null;
        switch (r) {
          case 514:
            return n.relative;
          case 4097:
            return n.coneInnerAngle;
          case 4098:
            return n.coneOuterAngle;
          case 4099:
            return n.pitch;
          case 4100:
            return n.position;
          case 4101:
            return n.direction;
          case 4102:
            return n.velocity;
          case 4103:
            return n.looping;
          case 4105:
            return 4136 === n.type ? n.bufQueue[0].id : 0;
          case 4106:
            return n.gain.gain.value;
          case 4109:
            return n.minGain;
          case 4110:
            return n.maxGain;
          case 4112:
            return n.state;
          case 4117:
            return 1 === n.bufQueue.length && 0 === n.bufQueue[0].id ? 0 : n.bufQueue.length;
          case 4118:
            return 1 === n.bufQueue.length && 0 === n.bufQueue[0].id || n.looping ? 0 : n.bufsProcessed;
          case 4128:
            return n.refDistance;
          case 4129:
            return n.rolloffFactor;
          case 4130:
            return n.coneOuterGain;
          case 4131:
            return n.maxDistance;
          case 4132:
            return ur.sourceTell(n);
          case 4133:
            return (o = ur.sourceTell(n)) > 0 && (o *= n.bufQueue[0].frequency), o;
          case 4134:
            var o;
            return (o = ur.sourceTell(n)) > 0 && (o *= n.bufQueue[0].frequency * n.bufQueue[0].bytesPerSample), o;
          case 4135:
            return n.type;
          case 4628:
            return n.spatialize;
          case 8201:
            for (var a = 0, i = 0, u = 0; u < n.bufQueue.length; u++) a += n.bufQueue[u].length, 0 !== n.bufQueue[u].id && (i = n.bufQueue[u].bytesPerSample * n.bufQueue[u].channels);
            return a * i;
          case 8202:
            for (a = 0, u = 0; u < n.bufQueue.length; u++) a += n.bufQueue[u].length;
            return a;
          case 8203:
            return ur.sourceDuration(n);
          case 53248:
            return n.distanceModel;
          default:
            return ur.currentCtx.err = 40962, null
        }
      },
      setSourceParam: function (e, t, r, n) {
        if (ur.currentCtx) {
          var o = ur.currentCtx.sources[t];
          if (o)
            if (null !== n) switch (r) {
              case 514:
                if (1 === n) o.relative = !0, ur.updateSourceSpace(o);
                else {
                  if (0 !== n) return void(ur.currentCtx.err = 40963);
                  o.relative = !1, ur.updateSourceSpace(o)
                }
                break;
              case 4097:
                if (!Number.isFinite(n)) return void(ur.currentCtx.err = 40963);
                o.coneInnerAngle = n, o.panner && (o.panner.coneInnerAngle = n % 360);
                break;
              case 4098:
                if (!Number.isFinite(n)) return void(ur.currentCtx.err = 40963);
                o.coneOuterAngle = n, o.panner && (o.panner.coneOuterAngle = n % 360);
                break;
              case 4099:
                if (!Number.isFinite(n) || n <= 0) return void(ur.currentCtx.err = 40963);
                if (o.pitch === n) break;
                o.pitch = n, ur.updateSourceRate(o);
                break;
              case 4100:
                if (!Number.isFinite(n[0]) || !Number.isFinite(n[1]) || !Number.isFinite(n[2])) return void(ur.currentCtx.err = 40963);
                o.position[0] = n[0], o.position[1] = n[1], o.position[2] = n[2], ur.updateSourceSpace(o);
                break;
              case 4101:
                if (!Number.isFinite(n[0]) || !Number.isFinite(n[1]) || !Number.isFinite(n[2])) return void(ur.currentCtx.err = 40963);
                o.direction[0] = n[0], o.direction[1] = n[1], o.direction[2] = n[2], ur.updateSourceSpace(o);
                break;
              case 4102:
                if (!Number.isFinite(n[0]) || !Number.isFinite(n[1]) || !Number.isFinite(n[2])) return void(ur.currentCtx.err = 40963);
                o.velocity[0] = n[0], o.velocity[1] = n[1], o.velocity[2] = n[2], ur.updateSourceSpace(o);
                break;
              case 4103:
                if (1 === n) o.looping = !0, ur.updateSourceTime(o), 4136 === o.type && o.audioQueue.length > 0 && ((a = o.audioQueue[0]).loop = !0, a._duration = Number.POSITIVE_INFINITY);
                else {
                  if (0 !== n) return void(ur.currentCtx.err = 40963);
                  o.looping = !1;
                  var a, i = ur.updateSourceTime(o);
                  4136 === o.type && o.audioQueue.length > 0 && ((a = o.audioQueue[0]).loop = !1, a._duration = o.bufQueue[0].audioBuf.duration / o.playbackRate, a._startTime = i - o.bufOffset / o.playbackRate)
                }
                break;
              case 4105:
                if (4114 === o.state || 4115 === o.state) return void(ur.currentCtx.err = 40964);
                if (0 === n) {
                  for (var u in o.bufQueue) o.bufQueue[u].refCount--;
                  o.bufQueue.length = 1, o.bufQueue[0] = ur.buffers[0], o.bufsProcessed = 0, o.type = 4144
                } else {
                  if (!(d = ur.buffers[n])) return void(ur.currentCtx.err = 40963);
                  for (var u in o.bufQueue) o.bufQueue[u].refCount--;
                  o.bufQueue.length = 0, d.refCount++, o.bufQueue = [d], o.bufsProcessed = 0, o.type = 4136
                }
                ur.initSourcePanner(o), ur.scheduleSourceAudio(o);
                break;
              case 4106:
                if (!Number.isFinite(n) || n < 0) return void(ur.currentCtx.err = 40963);
                o.gain.gain.value = n;
                break;
              case 4109:
                if (!Number.isFinite(n) || n < 0 || n > Math.min(o.maxGain, 1)) return void(ur.currentCtx.err = 40963);
                o.minGain = n;
                break;
              case 4110:
                if (!Number.isFinite(n) || n < Math.max(0, o.minGain) || n > 1) return void(ur.currentCtx.err = 40963);
                o.maxGain = n;
                break;
              case 4128:
                if (!Number.isFinite(n) || n < 0) return void(ur.currentCtx.err = 40963);
                o.refDistance = n, o.panner && (o.panner.refDistance = n);
                break;
              case 4129:
                if (!Number.isFinite(n) || n < 0) return void(ur.currentCtx.err = 40963);
                o.rolloffFactor = n, o.panner && (o.panner.rolloffFactor = n);
                break;
              case 4130:
                if (!Number.isFinite(n) || n < 0 || n > 1) return void(ur.currentCtx.err = 40963);
                o.coneOuterGain = n, o.panner && (o.panner.coneOuterGain = n);
                break;
              case 4131:
                if (!Number.isFinite(n) || n < 0) return void(ur.currentCtx.err = 40963);
                o.maxDistance = n, o.panner && (o.panner.maxDistance = n);
                break;
              case 4132:
                if (n < 0 || n > ur.sourceDuration(o)) return void(ur.currentCtx.err = 40963);
                ur.sourceSeek(o, n);
                break;
              case 4133:
                if ((l = ur.sourceDuration(o)) > 0) {
                  var s;
                  for (var c in o.bufQueue)
                    if (c) {
                      s = o.bufQueue[c].frequency;
                      break
                    } n /= s
                }
                if (n < 0 || n > l) return void(ur.currentCtx.err = 40963);
                ur.sourceSeek(o, n);
                break;
              case 4134:
                var l;
                if ((l = ur.sourceDuration(o)) > 0) {
                  var f;
                  for (var c in o.bufQueue)
                    if (c) {
                      var d;
                      f = (d = o.bufQueue[c]).frequency * d.bytesPerSample * d.channels;
                      break
                    } n /= f
                }
                if (n < 0 || n > l) return void(ur.currentCtx.err = 40963);
                ur.sourceSeek(o, n);
                break;
              case 4628:
                if (0 !== n && 1 !== n && 2 !== n) return void(ur.currentCtx.err = 40963);
                o.spatialize = n, ur.initSourcePanner(o);
                break;
              case 8201:
              case 8202:
              case 8203:
                ur.currentCtx.err = 40964;
                break;
              case 53248:
                switch (n) {
                  case 0:
                  case 53249:
                  case 53250:
                  case 53251:
                  case 53252:
                  case 53253:
                  case 53254:
                    o.distanceModel = n, ur.currentCtx.sourceDistanceModel && ur.updateContextGlobal(ur.currentCtx);
                    break;
                  default:
                    return void(ur.currentCtx.err = 40963)
                }
                break;
              default:
                return void(ur.currentCtx.err = 40962)
            } else ur.currentCtx.err = 40962;
            else ur.currentCtx.err = 40961
        }
      },
      captures: {},
      sharedCaptureAudioCtx: null,
      requireValidCaptureDevice: function (e, t) {
        if (0 === e) return ur.alcErr = 40961, null;
        var r = ur.captures[e];
        return r ? r.mediaStreamError ? (ur.alcErr = 40961, null) : r : (ur.alcErr = 40961, null)
      }
    };

  function sr(e, t, r) {
    switch (t) {
      case 514:
      case 4097:
      case 4098:
      case 4103:
      case 4105:
      case 4128:
      case 4129:
      case 4131:
      case 4132:
      case 4133:
      case 4134:
      case 4628:
      case 8201:
      case 8202:
      case 53248:
        ur.setSourceParam("alSourcei", e, t, r);
        break;
      default:
        ur.setSourceParam("alSourcei", e, t, null)
    }
  }

  function cr(e) {
    try {
      return g.grow(e - P.byteLength + 65535 >>> 16), V(g.buffer), 1
    } catch (t) {}
  }
  var lr = {};

  function fr() {
    if (!fr.strings) {
      var e = {
        USER: "web_user",
        LOGNAME: "web_user",
        PATH: "/",
        PWD: "/",
        HOME: "/home/web_user",
        LANG: ("object" == typeof navigator && navigator.languages && navigator.languages[0] || "C").replace("-", "_") + ".UTF-8",
        _: u || "./this.program"
      };
      for (var t in lr) e[t] = lr[t];
      var r = [];
      for (var t in e) r.push(t + "=" + e[t]);
      fr.strings = r
    }
    return fr.strings
  }
  var dr = {
      counter: 1,
      buffers: [],
      programs: [],
      framebuffers: [],
      renderbuffers: [],
      textures: [],
      shaders: [],
      vaos: [],
      contexts: [],
      offscreenCanvases: {},
      queries: [],
      samplers: [],
      transformFeedbacks: [],
      syncs: [],
      stringCache: {},
      stringiCache: {},
      unpackAlignment: 4,
      recordError: function (e) {
        dr.lastError || (dr.lastError = e)
      },
      getNewId: function (e) {
        for (var t = dr.counter++, r = e.length; r < t; r++) e[r] = null;
        return t
      },
      getSource: function (e, t, r, n) {
        for (var o = "", a = 0; a < t; ++a) {
          var i = n ? j[n + 4 * a >> 2] : -1;
          o += x(j[r + 4 * a >> 2], i < 0 ? void 0 : i)
        }
        return o
      },
      createContext: function (e, t) {
        e.getContextSafariWebGL2Fixed || (e.getContextSafariWebGL2Fixed = e.getContext, e.getContext = function (t, r) {
          var n = e.getContextSafariWebGL2Fixed(t, r);
          return "webgl" == t == n instanceof WebGLRenderingContext ? n : null
        });
        var r = t.majorVersion > 1 ? e.getContext("webgl2", t) : e.getContext("webgl", t);
        return r ? dr.registerContext(r, t) : 0
      },
      registerContext: function (e, t) {
        var r = dr.getNewId(dr.contexts),
          n = {
            handle: r,
            attributes: t,
            version: t.majorVersion,
            GLctx: e
          };
        return e.canvas && (e.canvas.GLctxObject = n), dr.contexts[r] = n, (void 0 === t.enableExtensionsByDefault || t.enableExtensionsByDefault) && dr.initExtensions(n), r
      },
      makeContextCurrent: function (t) {
        return dr.currentContext = dr.contexts[t], e.ctx = Pr = dr.currentContext && dr.currentContext.GLctx, !(t && !Pr)
      },
      getContext: function (e) {
        return dr.contexts[e]
      },
      deleteContext: function (e) {
        dr.currentContext === dr.contexts[e] && (dr.currentContext = null), "object" == typeof JSEvents && JSEvents.removeAllHandlersOnTarget(dr.contexts[e].GLctx.canvas), dr.contexts[e] && dr.contexts[e].GLctx.canvas && (dr.contexts[e].GLctx.canvas.GLctxObject = void 0), dr.contexts[e] = null
      },
      initExtensions: function (e) {
        if (e || (e = dr.currentContext), !e.initExtensionsDone) {
          e.initExtensionsDone = !0;
          var t, r = e.GLctx;
          ! function (e) {
            var t = e.getExtension("ANGLE_instanced_arrays");
            t && (e.vertexAttribDivisor = function (e, r) {
              t.vertexAttribDivisorANGLE(e, r)
            }, e.drawArraysInstanced = function (e, r, n, o) {
              t.drawArraysInstancedANGLE(e, r, n, o)
            }, e.drawElementsInstanced = function (e, r, n, o, a) {
              t.drawElementsInstancedANGLE(e, r, n, o, a)
            })
          }(r),
          function (e) {
            var t = e.getExtension("OES_vertex_array_object");
            t && (e.createVertexArray = function () {
              return t.createVertexArrayOES()
            }, e.deleteVertexArray = function (e) {
              t.deleteVertexArrayOES(e)
            }, e.bindVertexArray = function (e) {
              t.bindVertexArrayOES(e)
            }, e.isVertexArray = function (e) {
              return t.isVertexArrayOES(e)
            })
          }(r),
          function (e) {
            var t = e.getExtension("WEBGL_draw_buffers");
            t && (e.drawBuffers = function (e, r) {
              t.drawBuffersWEBGL(e, r)
            })
          }(r), (t = r).dibvbi = t.getExtension("WEBGL_draw_instanced_base_vertex_base_instance"),
            function (e) {
              e.mdibvbi = e.getExtension("WEBGL_multi_draw_instanced_base_vertex_base_instance")
            }(r), e.version >= 2 && (r.disjointTimerQueryExt = r.getExtension("EXT_disjoint_timer_query_webgl2")), (e.version < 2 || !r.disjointTimerQueryExt) && (r.disjointTimerQueryExt = r.getExtension("EXT_disjoint_timer_query")),
            function (e) {
              e.multiDrawWebgl = e.getExtension("WEBGL_multi_draw")
            }(r), (r.getSupportedExtensions() || []).forEach((function (e) {
              e.includes("lose_context") || e.includes("debug") || r.getExtension(e)
            }))
        }
      }
    },
    pr = [];

  function mr(e, t, r, n) {
    for (var o = 0; o < e; o++) {
      var a = Pr[r](),
        i = a && dr.getNewId(n);
      a ? (a.name = i, n[i] = a) : dr.recordError(1282), j[t + 4 * o >> 2] = i
    }
  }

  function hr(e, t, r) {
    if (t) {
      var n, o, a = void 0;
      switch (e) {
        case 36346:
          a = 1;
          break;
        case 36344:
          return void(0 != r && 1 != r && dr.recordError(1280));
        case 34814:
        case 36345:
          a = 0;
          break;
        case 34466:
          var i = Pr.getParameter(34467);
          a = i ? i.length : 0;
          break;
        case 33309:
          if (dr.currentContext.version < 2) return void dr.recordError(1282);
          a = 2 * (Pr.getSupportedExtensions() || []).length;
          break;
        case 33307:
        case 33308:
          if (dr.currentContext.version < 2) return void dr.recordError(1280);
          a = 33307 == e ? 3 : 0
      }
      if (void 0 === a) {
        var u = Pr.getParameter(e);
        switch (typeof u) {
          case "number":
            a = u;
            break;
          case "boolean":
            a = u ? 1 : 0;
            break;
          case "string":
            return void dr.recordError(1280);
          case "object":
            if (null === u) switch (e) {
              case 34964:
              case 35725:
              case 34965:
              case 36006:
              case 36007:
              case 32873:
              case 34229:
              case 36662:
              case 36663:
              case 35053:
              case 35055:
              case 36010:
              case 35097:
              case 35869:
              case 32874:
              case 36389:
              case 35983:
              case 35368:
              case 34068:
                a = 0;
                break;
              default:
                return void dr.recordError(1280)
            } else {
              if (u instanceof Float32Array || u instanceof Uint32Array || u instanceof Int32Array || u instanceof Array) {
                for (var s = 0; s < u.length; ++s) switch (r) {
                  case 0:
                    j[t + 4 * s >> 2] = u[s];
                    break;
                  case 2:
                    B[t + 4 * s >> 2] = u[s];
                    break;
                  case 4:
                    T[t + s >> 0] = u[s] ? 1 : 0
                }
                return
              }
              try {
                a = 0 | u.name
              } catch (c) {
                return dr.recordError(1280), void f("GL_INVALID_ENUM in glGet" + r + "v: Unknown object returned from WebGL getParameter(" + e + ")! (error: " + c + ")")
              }
            }
            break;
          default:
            return dr.recordError(1280), void f("GL_INVALID_ENUM in glGet" + r + "v: Native code calling glGet" + r + "v(" + e + ") and it returns " + u + " of type " + typeof u + "!")
        }
      }
      switch (r) {
        case 1:
          o = a, I[(n = t) >> 2] = o, I[n + 4 >> 2] = (o - I[n >> 2]) / 4294967296;
          break;
        case 0:
          j[t >> 2] = a;
          break;
        case 2:
          B[t >> 2] = a;
          break;
        case 4:
          T[t >> 0] = a ? 1 : 0
      }
    } else dr.recordError(1281)
  }

  function vr(e) {
    var t = D(e) + 1,
      r = Br(t);
    return F(e, r, t), r
  }

  function gr(e) {
    return 0 == (e -= 5120) ? T : 1 == e ? A : 2 == e ? L : 4 == e ? j : 6 == e ? B : 5 == e || 28922 == e || 28520 == e || 30779 == e || 30782 == e ? I : M
  }

  function yr(e) {
    return 31 - Math.clz32(e.BYTES_PER_ELEMENT)
  }

  function br(e, t, r, n, o, a) {
    var i = gr(e),
      u = yr(i),
      s = 1 << u,
      c = function (e, t, r, n) {
        var o;
        return t * (e * r + (o = n) - 1 & -o)
      }(r, n, function (e) {
        return {
          5: 3,
          6: 4,
          8: 2,
          29502: 3,
          29504: 4,
          26917: 2,
          26918: 2,
          29846: 3,
          29847: 4
        } [e - 6402] || 1
      }(t) * s, dr.unpackAlignment);
    return i.subarray(o >> u, o + c >> u)
  }

  function wr(e) {
    var t = Pr.currentProgram,
      r = t.uniformLocsById[e];
    return r >= 0 && (t.uniformLocsById[e] = r = Pr.getUniformLocation(t, t.uniformArrayNamesById[e] + (r > 0 ? "[" + r + "]" : ""))), r
  }
  var _r = [];

  function Er() {
    if (!Er.called) {
      Er.called = !0;
      var e = (new Date).getFullYear(),
        t = new Date(e, 0, 1),
        r = new Date(e, 6, 1),
        n = t.getTimezoneOffset(),
        o = r.getTimezoneOffset(),
        a = Math.max(n, o);
      j[qr() >> 2] = 60 * a, j[Gr() >> 2] = Number(n != o);
      var i = l(t),
        u = l(r),
        s = Y(i),
        c = Y(u);
      o < n ? (j[Wr() >> 2] = s, j[Wr() + 4 >> 2] = c) : (j[Wr() >> 2] = c, j[Wr() + 4 >> 2] = s)
    }

    function l(e) {
      var t = e.toTimeString().match(/\(([A-Za-z ]+)\)$/);
      return t ? t[1] : "GMT"
    }
  }

  function Cr(e) {
    return e % 4 == 0 && (e % 100 != 0 || e % 400 == 0)
  }

  function kr(e, t) {
    for (var r = 0, n = 0; n <= t; r += e[n++]);
    return r
  }
  var xr = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    Sr = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  function Fr(e, t) {
    for (var r = new Date(e.getTime()); t > 0;) {
      var n = Cr(r.getFullYear()),
        o = r.getMonth(),
        a = (n ? xr : Sr)[o];
      if (!(t > a - r.getDate())) return r.setDate(r.getDate() + t), r;
      t -= a - r.getDate() + 1, r.setDate(1), o < 11 ? r.setMonth(o + 1) : (r.setMonth(0), r.setFullYear(r.getFullYear() + 1))
    }
    return r
  }

  function Dr(e, t, r, n) {
    var o = j[n + 40 >> 2],
      a = {
        tm_sec: j[n >> 2],
        tm_min: j[n + 4 >> 2],
        tm_hour: j[n + 8 >> 2],
        tm_mday: j[n + 12 >> 2],
        tm_mon: j[n + 16 >> 2],
        tm_year: j[n + 20 >> 2],
        tm_wday: j[n + 24 >> 2],
        tm_yday: j[n + 28 >> 2],
        tm_isdst: j[n + 32 >> 2],
        tm_gmtoff: j[n + 36 >> 2],
        tm_zone: o ? x(o) : ""
      },
      i = x(r),
      u = {
        "%c": "%a %b %d %H:%M:%S %Y",
        "%D": "%m/%d/%y",
        "%F": "%Y-%m-%d",
        "%h": "%b",
        "%r": "%I:%M:%S %p",
        "%R": "%H:%M",
        "%T": "%H:%M:%S",
        "%x": "%m/%d/%y",
        "%X": "%H:%M:%S",
        "%Ec": "%c",
        "%EC": "%C",
        "%Ex": "%m/%d/%y",
        "%EX": "%H:%M:%S",
        "%Ey": "%y",
        "%EY": "%Y",
        "%Od": "%d",
        "%Oe": "%e",
        "%OH": "%H",
        "%OI": "%I",
        "%Om": "%m",
        "%OM": "%M",
        "%OS": "%S",
        "%Ou": "%u",
        "%OU": "%U",
        "%OV": "%V",
        "%Ow": "%w",
        "%OW": "%W",
        "%Oy": "%y"
      };
    for (var s in u) i = i.replace(new RegExp(s, "g"), u[s]);
    var c = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      l = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    function f(e, t, r) {
      for (var n = "number" == typeof e ? e.toString() : e || ""; n.length < t;) n = r[0] + n;
      return n
    }

    function d(e, t) {
      return f(e, t, "0")
    }

    function p(e, t) {
      function r(e) {
        return e < 0 ? -1 : e > 0 ? 1 : 0
      }
      var n;
      return 0 === (n = r(e.getFullYear() - t.getFullYear())) && 0 === (n = r(e.getMonth() - t.getMonth())) && (n = r(e.getDate() - t.getDate())), n
    }

    function m(e) {
      switch (e.getDay()) {
        case 0:
          return new Date(e.getFullYear() - 1, 11, 29);
        case 1:
          return e;
        case 2:
          return new Date(e.getFullYear(), 0, 3);
        case 3:
          return new Date(e.getFullYear(), 0, 2);
        case 4:
          return new Date(e.getFullYear(), 0, 1);
        case 5:
          return new Date(e.getFullYear() - 1, 11, 31);
        case 6:
          return new Date(e.getFullYear() - 1, 11, 30)
      }
    }

    function h(e) {
      var t = Fr(new Date(e.tm_year + 1900, 0, 1), e.tm_yday),
        r = new Date(t.getFullYear(), 0, 4),
        n = new Date(t.getFullYear() + 1, 0, 4),
        o = m(r),
        a = m(n);
      return p(o, t) <= 0 ? p(a, t) <= 0 ? t.getFullYear() + 1 : t.getFullYear() : t.getFullYear() - 1
    }
    var v = {
      "%a": function (e) {
        return c[e.tm_wday].substring(0, 3)
      },
      "%A": function (e) {
        return c[e.tm_wday]
      },
      "%b": function (e) {
        return l[e.tm_mon].substring(0, 3)
      },
      "%B": function (e) {
        return l[e.tm_mon]
      },
      "%C": function (e) {
        return d((e.tm_year + 1900) / 100 | 0, 2)
      },
      "%d": function (e) {
        return d(e.tm_mday, 2)
      },
      "%e": function (e) {
        return f(e.tm_mday, 2, " ")
      },
      "%g": function (e) {
        return h(e).toString().substring(2)
      },
      "%G": function (e) {
        return h(e)
      },
      "%H": function (e) {
        return d(e.tm_hour, 2)
      },
      "%I": function (e) {
        var t = e.tm_hour;
        return 0 == t ? t = 12 : t > 12 && (t -= 12), d(t, 2)
      },
      "%j": function (e) {
        return d(e.tm_mday + kr(Cr(e.tm_year + 1900) ? xr : Sr, e.tm_mon - 1), 3)
      },
      "%m": function (e) {
        return d(e.tm_mon + 1, 2)
      },
      "%M": function (e) {
        return d(e.tm_min, 2)
      },
      "%n": function () {
        return "\n"
      },
      "%p": function (e) {
        return e.tm_hour >= 0 && e.tm_hour < 12 ? "AM" : "PM"
      },
      "%S": function (e) {
        return d(e.tm_sec, 2)
      },
      "%t": function () {
        return "\t"
      },
      "%u": function (e) {
        return e.tm_wday || 7
      },
      "%U": function (e) {
        var t = new Date(e.tm_year + 1900, 0, 1),
          r = 0 === t.getDay() ? t : Fr(t, 7 - t.getDay()),
          n = new Date(e.tm_year + 1900, e.tm_mon, e.tm_mday);
        if (p(r, n) < 0) {
          var o = kr(Cr(n.getFullYear()) ? xr : Sr, n.getMonth() - 1) - 31,
            a = 31 - r.getDate() + o + n.getDate();
          return d(Math.ceil(a / 7), 2)
        }
        return 0 === p(r, t) ? "01" : "00"
      },
      "%V": function (e) {
        var t, r = new Date(e.tm_year + 1900, 0, 4),
          n = new Date(e.tm_year + 1901, 0, 4),
          o = m(r),
          a = m(n),
          i = Fr(new Date(e.tm_year + 1900, 0, 1), e.tm_yday);
        return p(i, o) < 0 ? "53" : p(a, i) <= 0 ? "01" : (t = o.getFullYear() < e.tm_year + 1900 ? e.tm_yday + 32 - o.getDate() : e.tm_yday + 1 - o.getDate(), d(Math.ceil(t / 7), 2))
      },
      "%w": function (e) {
        return e.tm_wday
      },
      "%W": function (e) {
        var t = new Date(e.tm_year, 0, 1),
          r = 1 === t.getDay() ? t : Fr(t, 0 === t.getDay() ? 1 : 7 - t.getDay() + 1),
          n = new Date(e.tm_year + 1900, e.tm_mon, e.tm_mday);
        if (p(r, n) < 0) {
          var o = kr(Cr(n.getFullYear()) ? xr : Sr, n.getMonth() - 1) - 31,
            a = 31 - r.getDate() + o + n.getDate();
          return d(Math.ceil(a / 7), 2)
        }
        return 0 === p(r, t) ? "01" : "00"
      },
      "%y": function (e) {
        return (e.tm_year + 1900).toString().substring(2)
      },
      "%Y": function (e) {
        return e.tm_year + 1900
      },
      "%z": function (e) {
        var t = e.tm_gmtoff,
          r = t >= 0;
        return t = (t = Math.abs(t) / 60) / 60 * 100 + t % 60, (r ? "+" : "-") + String("0000" + t).slice(-4)
      },
      "%Z": function (e) {
        return e.tm_zone
      },
      "%%": function () {
        return "%"
      }
    };
    for (var s in v) i.includes(s) && (i = i.replace(new RegExp(s, "g"), v[s](a)));
    var g, y, b = Ir(i, !1);
    return b.length > t ? 0 : (g = b, y = e, T.set(g, y), b.length - 1)
  }
  var Pr, Tr = function (e, t, r, n) {
      e || (e = this), this.parent = e, this.mount = e.mount, this.mounted = null, this.id = Pe.nextInode++, this.name = t, this.mode = r, this.node_ops = {}, this.stream_ops = {}, this.rdev = n
    },
    Ar = 365,
    Lr = 146;
  Object.defineProperties(Tr.prototype, {
      read: {
        get: function () {
          return (this.mode & Ar) === Ar
        },
        set: function (e) {
          e ? this.mode |= Ar : this.mode &= -366
        }
      },
      write: {
        get: function () {
          return (this.mode & Lr) === Lr
        },
        set: function (e) {
          e ? this.mode |= Lr : this.mode &= -147
        }
      },
      isFolder: {
        get: function () {
          return Pe.isDir(this.mode)
        }
      },
      isDevice: {
        get: function () {
          return Pe.isChrdev(this.mode)
        }
      }
    }), Pe.FSNode = Tr, Pe.staticInit(), e.FS_createPath = Pe.createPath, e.FS_createDataFile = Pe.createDataFile, e.FS_createPreloadedFile = Pe.createPreloadedFile, e.FS_createLazyFile = Pe.createLazyFile, e.FS_createDevice = Pe.createDevice, e.FS_unlink = Pe.unlink,
    function () {
      for (var e = new Array(256), t = 0; t < 256; ++t) e[t] = String.fromCharCode(t);
      Le = e
    }(), Ue = e.BindingError = Ne(Error, "BindingError"), $e = e.InternalError = Ne(Error, "InternalError"), at.prototype.isAliasOf = Qe, at.prototype.clone = Ze, at.prototype.delete = Je, at.prototype.isDeleted = et, at.prototype.deleteLater = ot, St.prototype.getPointee = ht, St.prototype.destructor = vt, St.prototype.argPackAdvance = 8, St.prototype.readValueFromPointer = mt, St.prototype.deleteObject = gt, St.prototype.fromWireType = xt, e.getInheritedInstanceCount = bt, e.getLiveInheritedInstances = wt, e.flushPendingDeletes = nt, e.setDelayFunction = _t, Tt = e.UnboundTypeError = Ne(Error, "UnboundTypeError"), e.count_emval_handles = zt, e.get_first_emval = $t, e.requestFullscreen = function (e, t) {
      ir.requestFullscreen(e, t)
    }, e.requestAnimationFrame = function (e) {
      ir.requestAnimationFrame(e)
    }, e.setCanvasSize = function (e, t, r) {
      ir.setCanvasSize(e, t, r)
    }, e.pauseMainLoop = function () {
      ir.mainLoop.pause()
    }, e.resumeMainLoop = function () {
      ir.mainLoop.resume()
    }, e.getUserMedia = function () {
      ir.getUserMedia()
    }, e.createContext = function (e, t, r, n) {
      return ir.createContext(e, t, r, n)
    };
  for (var Mr = 0; Mr < 32; ++Mr) pr.push(new Array(Mr));
  var jr = new Float32Array(288);
  for (Mr = 0; Mr < 288; ++Mr) _r[Mr] = jr.subarray(0, Mr + 1);

  function Ir(e, t, r) {
    var n = r > 0 ? r : D(e) + 1,
      o = new Array(n),
      a = S(e, o, 0, o.length);
    return t && (o.length = a), o
  }
  var Rr = {
    zb: function () {
      f("missing function: _ZN3bnb13serialization21frame_data_serializer11write_frameERKNS_10frame_dataE"), re(-1)
    },
    Ab: function () {
      f("missing function: _ZN3bnb13serialization21frame_data_serializer12set_featuresENSt3__26vectorINS2_12basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEENS7_IS9_EEEE"), re(-1)
    },
    Bb: function () {
      f("missing function: _ZN3bnb13serialization21frame_data_serializerC1ERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEENS0_16compression_typeERKNS2_13unordered_mapIS8_S8_NS2_4hashIS8_EENS2_8equal_toIS8_EENS6_INS2_4pairIS9_S8_EEEEEE"), re(-1)
    },
    Ub: function (e, t) {
      return pe(e, t)
    },
    a: function (e) {
      return Br(e + be) + be
    },
    wb: function () {
      var e = we.pop();
      e || re("no exception to throw");
      var t = e.get_exception_info(),
        r = e.get_base_ptr();
      throw t.get_rethrown() ? e.free() : (we.push(e), t.set_rethrown(!0), t.set_caught(!1)), r
    },
    ac: function (e, t) {},
    b: function (e, t, r) {
      throw new _e(e).init(t, r), e
    },
    Lb: function (e, t) {
      return Ee(e, t)
    },
    $a: function (e, t, r) {
      Te.varargs = r;
      try {
        var n = Te.getStreamFromFD(e);
        switch (t) {
          case 0:
            return (o = Te.get()) < 0 ? -28 : Pe.open(n.path, n.flags, 0, o).fd;
          case 1:
          case 2:
            return 0;
          case 3:
            return n.flags;
          case 4:
            var o = Te.get();
            return n.flags |= o, 0;
          case 12:
            return o = Te.get(), L[o + 0 >> 1] = 2, 0;
          case 13:
          case 14:
            return 0;
          case 16:
          case 8:
            return -28;
          case 9:
            return de(28), -1;
          default:
            return -28
        }
      } catch (a) {
        return void 0 !== Pe && a instanceof Pe.ErrnoError || re(a), -a.errno
      }
    },
    Pb: function (e, t, r, n) {
      try {
        var o = Te.get64(r, n);
        return Pe.ftruncate(e, o), 0
      } catch (a) {
        return void 0 !== Pe && a instanceof Pe.ErrnoError || re(a), -a.errno
      }
    },
    Tb: function (e, t, r) {
      try {
        var n = Te.getStreamFromFD(e);
        n.getdents || (n.getdents = Pe.readdir(n.path));
        for (var o = 280, a = 0, i = Pe.llseek(n, 0, 1), u = Math.floor(i / o); u < n.getdents.length && a + o <= r;) {
          var s, c, l = n.getdents[u];
          if ("." === l[0]) s = 1, c = 4;
          else {
            var f = Pe.lookupNode(n.node, l);
            s = f.id, c = Pe.isChrdev(f.mode) ? 2 : Pe.isDir(f.mode) ? 4 : Pe.isLink(f.mode) ? 10 : 8
          }
          ie = [s >>> 0, (ae = s, +Math.abs(ae) >= 1 ? ae > 0 ? (0 | Math.min(+Math.floor(ae / 4294967296), 4294967295)) >>> 0 : ~~+Math.ceil((ae - +(~~ae >>> 0)) / 4294967296) >>> 0 : 0)], j[t + a >> 2] = ie[0], j[t + a + 4 >> 2] = ie[1], ie = [(u + 1) * o >>> 0, (ae = (u + 1) * o, +Math.abs(ae) >= 1 ? ae > 0 ? (0 | Math.min(+Math.floor(ae / 4294967296), 4294967295)) >>> 0 : ~~+Math.ceil((ae - +(~~ae >>> 0)) / 4294967296) >>> 0 : 0)], j[t + a + 8 >> 2] = ie[0], j[t + a + 12 >> 2] = ie[1], L[t + a + 16 >> 1] = 280, T[t + a + 18 >> 0] = c, F(l, t + a + 19, 256), a += o, u += 1
        }
        return Pe.llseek(n, u * o, 0), a
      } catch (d) {
        return void 0 !== Pe && d instanceof Pe.ErrnoError || re(d), -d.errno
      }
    },
    Qb: function () {
      return 42
    },
    Mb: function (e, t, r) {
      Te.varargs = r;
      try {
        var n = Te.getStreamFromFD(e);
        switch (t) {
          case 21509:
          case 21505:
            return n.tty ? 0 : -59;
          case 21510:
          case 21511:
          case 21512:
          case 21506:
          case 21507:
          case 21508:
            return n.tty ? 0 : -59;
          case 21519:
            if (!n.tty) return -59;
            var o = Te.get();
            return j[o >> 2] = 0, 0;
          case 21520:
            return n.tty ? -28 : -59;
          case 21531:
            return o = Te.get(), Pe.ioctl(n, t, o);
          case 21523:
          case 21524:
            return n.tty ? 0 : -59;
          default:
            re("bad ioctl syscall " + t)
        }
      } catch (a) {
        return void 0 !== Pe && a instanceof Pe.ErrnoError || re(a), -a.errno
      }
    },
    ab: function (e, t) {
      try {
        return e = Te.getStr(e), Te.doStat(Pe.lstat, e, t)
      } catch (r) {
        return void 0 !== Pe && r instanceof Pe.ErrnoError || re(r), -r.errno
      }
    },
    Wb: function (e, t) {
      try {
        return e = Te.getStr(e), Te.doMkdir(e, t)
      } catch (r) {
        return void 0 !== Pe && r instanceof Pe.ErrnoError || re(r), -r.errno
      }
    },
    Nb: function (e, t, r, n, o, a) {
      try {
        return function (e, t, r, n, o, a) {
          var i;
          a <<= 12;
          var u = !1;
          if (0 != (16 & n) && e % 65536 != 0) return -28;
          if (0 != (32 & n)) {
            if (!(i = Yr(65536, t))) return -48;
            Or(i, 0, t), u = !0
          } else {
            var s = Pe.getStream(o);
            if (!s) return -8;
            var c = Pe.mmap(s, e, t, a, r, n);
            i = c.ptr, u = c.allocated
          }
          return Te.mappings[i] = {
            malloc: i,
            len: t,
            allocated: u,
            fd: o,
            prot: r,
            flags: n,
            offset: a
          }, i
        }(e, t, r, n, o, a)
      } catch (i) {
        return void 0 !== Pe && i instanceof Pe.ErrnoError || re(i), -i.errno
      }
    },
    Ob: function (e, t) {
      try {
        return function (e, t) {
          if (-1 == (0 | e) || 0 === t) return -28;
          var r = Te.mappings[e];
          if (!r) return 0;
          if (t === r.len) {
            var n = Pe.getStream(r.fd);
            n && (2 & r.prot && Te.doMsync(e, n, t, r.flags, r.offset), Pe.munmap(n)), Te.mappings[e] = null, r.allocated && Ur(r.malloc)
          }
          return 0
        }(e, t)
      } catch (r) {
        return void 0 !== Pe && r instanceof Pe.ErrnoError || re(r), -r.errno
      }
    },
    bb: function (e, t, r) {
      Te.varargs = r;
      try {
        var n = Te.getStr(e),
          o = r ? Te.get() : 0;
        return Pe.open(n, t, o).fd
      } catch (a) {
        return void 0 !== Pe && a instanceof Pe.ErrnoError || re(a), -a.errno
      }
    },
    Vb: function (e, t) {
      try {
        return e = Te.getStr(e), Te.doStat(Pe.stat, e, t)
      } catch (r) {
        return void 0 !== Pe && r instanceof Pe.ErrnoError || re(r), -r.errno
      }
    },
    Db: function (e, t, r, n, o) {},
    Zb: function (e, t, r, n, o) {
      var a = Ae(r);
      qe(e, {
        name: t = Me(t),
        fromWireType: function (e) {
          return !!e
        },
        toWireType: function (e, t) {
          return t ? n : o
        },
        argPackAdvance: 8,
        readValueFromPointer: function (e) {
          var n;
          if (1 === r) n = T;
          else if (2 === r) n = L;
          else {
            if (4 !== r) throw new TypeError("Unknown boolean type size: " + t);
            n = j
          }
          return this.fromWireType(n[e >> a])
        },
        destructorFunction: null
      })
    },
    x: function (e, t, r, n, o, a, i, u, s, c, l, f, d) {
      l = Me(l), a = Pt(o, a), u && (u = Pt(i, u)), c && (c = Pt(s, c)), d = Pt(f, d);
      var p = Be(l);
      st(p, (function () {
        Lt("Cannot construct " + l + " due to unbound types", [n])
      })), Ge([e, t, r], n ? [n] : [], (function (t) {
        var r, o;
        t = t[0], o = n ? (r = t.registeredClass).instancePrototype : at.prototype;
        var i = Oe(p, (function () {
            if (Object.getPrototypeOf(this) !== s) throw new Ue("Use 'new' to construct " + l);
            if (void 0 === f.constructor_body) throw new Ue(l + " has no accessible constructor");
            var e = f.constructor_body[arguments.length];
            if (void 0 === e) throw new Ue("Tried to invoke ctor of " + l + " with invalid number of parameters (" + arguments.length + ") - expected (" + Object.keys(f.constructor_body).toString() + ") parameters instead!");
            return e.apply(this, arguments)
          })),
          s = Object.create(o, {
            constructor: {
              value: i
            }
          });
        i.prototype = s;
        var f = new ct(l, i, s, d, r, a, u, c),
          m = new St(l, f, !0, !1, !1),
          h = new St(l + "*", f, !1, !1, !1),
          v = new St(l + " const*", f, !1, !0, !1);
        return it[e] = {
          pointerType: h,
          constPointerType: v
        }, Ft(p, i), [m, h, v]
      }))
    },
    aa: function (e, t, r, n, o, a, i) {
      var u = Rt(r, n);
      t = Me(t), a = Pt(o, a), Ge([], [e], (function (e) {
        var n = (e = e[0]).name + "." + t;

        function o() {
          Lt("Cannot call " + n + " due to unbound types", u)
        }
        var s = e.registeredClass.constructor;
        return void 0 === s[t] ? (o.argCount = r - 1, s[t] = o) : (ut(s, t, n), s[t].overloadTable[r - 1] = o), Ge([], u, (function (e) {
          var o = [e[0], null].concat(e.slice(1)),
            u = It(n, o, null, a, i);
          return void 0 === s[t].overloadTable ? (u.argCount = r - 1, s[t] = u) : s[t].overloadTable[r - 1] = u, []
        })), []
      }))
    },
    J: function (e, t, r, n, o, a) {
      _(t > 0);
      var i = Rt(t, r);
      o = Pt(n, o);
      var u = [a],
        s = [];
      Ge([], [e], (function (e) {
        var r = "constructor " + (e = e[0]).name;
        if (void 0 === e.registeredClass.constructor_body && (e.registeredClass.constructor_body = []), void 0 !== e.registeredClass.constructor_body[t - 1]) throw new Ue("Cannot register multiple constructors with identical number of parameters (" + (t - 1) + ") for class '" + e.name + "'! Overload resolution is currently only performed using the parameter count, not actual type info!");
        return e.registeredClass.constructor_body[t - 1] = function () {
          Lt("Cannot construct " + e.name + " due to unbound types", i)
        }, Ge([], i, (function (n) {
          return e.registeredClass.constructor_body[t - 1] = function () {
            arguments.length !== t - 1 && ze(r + " called with " + arguments.length + " arguments, expected " + (t - 1)), s.length = 0, u.length = t;
            for (var e = 1; e < t; ++e) u[e] = n[e].toWireType(s, arguments[e - 1]);
            var a = o.apply(null, u);
            return jt(s), n[0].fromWireType(a)
          }, []
        })), []
      }))
    },
    e: function (e, t, r, n, o, a, i, u) {
      var s = Rt(r, n);
      t = Me(t), a = Pt(o, a), Ge([], [e], (function (e) {
        var n = (e = e[0]).name + "." + t;

        function o() {
          Lt("Cannot call " + n + " due to unbound types", s)
        }
        u && e.registeredClass.pureVirtualFunctions.push(t);
        var c = e.registeredClass.instancePrototype,
          l = c[t];
        return void 0 === l || void 0 === l.overloadTable && l.className !== e.name && l.argCount === r - 2 ? (o.argCount = r - 2, o.className = e.name, c[t] = o) : (ut(c, t, n), c[t].overloadTable[r - 2] = o), Ge([], s, (function (o) {
          var u = It(n, o, e, a, i);
          return void 0 === c[t].overloadTable ? (u.argCount = r - 2, c[t] = u) : c[t].overloadTable[r - 2] = u, []
        })), []
      }))
    },
    C: function (e, t, r, n, o, a, i, u, s, c) {
      t = Me(t), o = Pt(n, o), Ge([], [e], (function (e) {
        var n = (e = e[0]).name + "." + t,
          l = {
            get: function () {
              Lt("Cannot access " + n + " due to unbound types", [r, i])
            },
            enumerable: !0,
            configurable: !0
          };
        return l.set = s ? function () {
          Lt("Cannot access " + n + " due to unbound types", [r, i])
        } : function (e) {
          ze(n + " is a read-only property")
        }, Object.defineProperty(e.registeredClass.instancePrototype, t, l), Ge([], s ? [r, i] : [r], (function (r) {
          var i = r[0],
            l = {
              get: function () {
                var t = Bt(this, e, n + " getter");
                return i.fromWireType(o(a, t))
              },
              enumerable: !0
            };
          if (s) {
            s = Pt(u, s);
            var f = r[1];
            l.set = function (t) {
              var r = Bt(this, e, n + " setter"),
                o = [];
              s(c, r, f.toWireType(o, t)), jt(o)
            }
          }
          return Object.defineProperty(e.registeredClass.instancePrototype, t, l), []
        })), []
      }))
    },
    Yb: function (e, t) {
      qe(e, {
        name: t = Me(t),
        fromWireType: function (e) {
          var t = Nt[e].value;
          return Ut(e), t
        },
        toWireType: function (e, t) {
          return Wt(t)
        },
        argPackAdvance: 8,
        readValueFromPointer: mt,
        destructorFunction: null
      })
    },
    wa: function (e, t, r, n) {
      var o = Ae(r);

      function a() {}
      t = Me(t), a.values = {}, qe(e, {
        name: t,
        constructor: a,
        fromWireType: function (e) {
          return this.constructor.values[e]
        },
        toWireType: function (e, t) {
          return t.value
        },
        argPackAdvance: 8,
        readValueFromPointer: Gt(t, o, n),
        destructorFunction: null
      }), st(t, a)
    },
    g: function (e, t, r) {
      var n = qt(e, "enum");
      t = Me(t);
      var o = n.constructor,
        a = Object.create(n.constructor.prototype, {
          value: {
            value: r
          },
          constructor: {
            value: Oe(n.name + "_" + t, (function () {}))
          }
        });
      o.values[r] = a, o[t] = a
    },
    cb: function (e, t, r) {
      var n = Ae(r);
      qe(e, {
        name: t = Me(t),
        fromWireType: function (e) {
          return e
        },
        toWireType: function (e, t) {
          if ("number" != typeof t && "boolean" != typeof t) throw new TypeError('Cannot convert "' + Qt(t) + '" to ' + this.name);
          return t
        },
        argPackAdvance: 8,
        readValueFromPointer: Yt(t, n),
        destructorFunction: null
      })
    },
    yb: function (e, t, r, n, o, a) {
      var i = Rt(t, r);
      e = Me(e), o = Pt(n, o), st(e, (function () {
        Lt("Cannot call " + e + " due to unbound types", i)
      }), t - 1), Ge([], i, (function (r) {
        var n = [r[0], null].concat(r.slice(1));
        return Ft(e, It(e, n, null, o, a), t - 1), []
      }))
    },
    O: function (e, t, r, n, o) {
      t = Me(t), -1 === o && (o = 4294967295);
      var a = Ae(r),
        i = function (e) {
          return e
        };
      if (0 === n) {
        var u = 32 - 8 * r;
        i = function (e) {
          return e << u >>> u
        }
      }
      var s = t.includes("unsigned");
      qe(e, {
        name: t,
        fromWireType: i,
        toWireType: function (e, r) {
          if ("number" != typeof r && "boolean" != typeof r) throw new TypeError('Cannot convert "' + Qt(r) + '" to ' + this.name);
          if (r < n || r > o) throw new TypeError('Passing a number "' + Qt(r) + '" from JS side to C/C++ side to an argument of type "' + t + '", which is outside the valid range [' + n + ", " + o + "]!");
          return s ? r >>> 0 : 0 | r
        },
        argPackAdvance: 8,
        readValueFromPointer: Vt(t, a, 0 !== n),
        destructorFunction: null
      })
    },
    I: function (e, t, r) {
      var n = [Int8Array, Uint8Array, Int16Array, Uint16Array, Int32Array, Uint32Array, Float32Array, Float64Array][t];

      function o(e) {
        var t = I,
          r = t[e >>= 2],
          o = t[e + 1];
        return new n(P, o, r)
      }
      qe(e, {
        name: r = Me(r),
        fromWireType: o,
        argPackAdvance: 8,
        readValueFromPointer: o
      }, {
        ignoreDuplicateRegistrations: !0
      })
    },
    $: function (e, t, r, n, o, a, i, u, s, c, l, f) {
      r = Me(r), a = Pt(o, a), u = Pt(i, u), c = Pt(s, c), f = Pt(l, f), Ge([e], [t], (function (e) {
        return e = e[0], [new St(r, e.registeredClass, !1, !1, !0, e, n, a, u, c, f)]
      }))
    },
    db: function (e, t) {
      var r = "std::string" === (t = Me(t));
      qe(e, {
        name: t,
        fromWireType: function (e) {
          var t, n = I[e >> 2];
          if (r)
            for (var o = e + 4, a = 0; a <= n; ++a) {
              var i = e + 4 + a;
              if (a == n || 0 == A[i]) {
                var u = x(o, i - o);
                void 0 === t ? t = u : (t += String.fromCharCode(0), t += u), o = i + 1
              }
            } else {
              var s = new Array(n);
              for (a = 0; a < n; ++a) s[a] = String.fromCharCode(A[e + 4 + a]);
              t = s.join("")
            }
          return Ur(e), t
        },
        toWireType: function (e, t) {
          t instanceof ArrayBuffer && (t = new Uint8Array(t));
          var n = "string" == typeof t;
          n || t instanceof Uint8Array || t instanceof Uint8ClampedArray || t instanceof Int8Array || ze("Cannot pass non-string to std::string");
          var o = (r && n ? function () {
              return D(t)
            } : function () {
              return t.length
            })(),
            a = Br(4 + o + 1);
          if (I[a >> 2] = o, r && n) F(t, a + 4, o + 1);
          else if (n)
            for (var i = 0; i < o; ++i) {
              var u = t.charCodeAt(i);
              u > 255 && (Ur(a), ze("String has UTF-16 code units that do not fit in 8 bits")), A[a + 4 + i] = u
            } else
              for (i = 0; i < o; ++i) A[a + 4 + i] = t[i];
          return null !== e && e.push(Ur, a), a
        },
        argPackAdvance: 8,
        readValueFromPointer: mt,
        destructorFunction: function (e) {
          Ur(e)
        }
      })
    },
    La: function (e, t, r) {
      var n, o, a, i, u;
      r = Me(r), 2 === t ? (n = z, o = $, i = W, a = function () {
        return M
      }, u = 1) : 4 === t && (n = G, o = q, i = Q, a = function () {
        return I
      }, u = 2), qe(e, {
        name: r,
        fromWireType: function (e) {
          for (var r, o = I[e >> 2], i = a(), s = e + 4, c = 0; c <= o; ++c) {
            var l = e + 4 + c * t;
            if (c == o || 0 == i[l >> u]) {
              var f = n(s, l - s);
              void 0 === r ? r = f : (r += String.fromCharCode(0), r += f), s = l + t
            }
          }
          return Ur(e), r
        },
        toWireType: function (e, n) {
          "string" != typeof n && ze("Cannot pass non-string to C++ string type " + r);
          var a = i(n),
            s = Br(4 + a + t);
          return I[s >> 2] = a >> u, o(n, s + 4, a + t), null !== e && e.push(Ur, s), s
        },
        argPackAdvance: 8,
        readValueFromPointer: mt,
        destructorFunction: function (e) {
          Ur(e)
        }
      })
    },
    _b: function (e, t) {
      qe(e, {
        isVoid: !0,
        name: t = Me(t),
        argPackAdvance: 0,
        fromWireType: function () {},
        toWireType: function (e, t) {}
      })
    },
    p: function (e, t, r) {
      e = Xt(e), t = qt(t, "emval::as");
      var n = [],
        o = Wt(n);
      return j[r >> 2] = o, t.toWireType(n, e)
    },
    Sb: function (e, t, r, n) {
      e = Xt(e);
      for (var o = Ht(t, r), a = new Array(t), i = 0; i < t; ++i) {
        var u = o[i];
        a[i] = u.readValueFromPointer(n), n += u.argPackAdvance
      }
      return Wt(e.apply(void 0, a))
    },
    Ma: function (e, t, r, n, o) {
      return (e = Jt[e])(t = Xt(t), r = Zt(r), function (e) {
        var t = [];
        return j[e >> 2] = Wt(t), t
      }(n), o)
    },
    ca: function (e, t, r, n) {
      (e = Jt[e])(t = Xt(t), r = Zt(r), null, n)
    },
    d: Ut,
    da: function (e) {
      return 0 === e ? Wt(er()) : (e = Zt(e), Wt(er()[e]))
    },
    N: function (e, t) {
      for (var r = Ht(e, t), n = r[0], o = n.name + "_$" + r.slice(1).map((function (e) {
          return e.name
        })).join("_") + "$", a = ["retType"], i = [n], u = "", s = 0; s < e - 1; ++s) u += (0 !== s ? ", " : "") + "arg" + s, a.push("argType" + s), i.push(r[1 + s]);
      var c = "return function " + Be("methodCaller_" + o) + "(handle, name, destructors, args) {\n",
        l = 0;
      for (s = 0; s < e - 1; ++s) c += "    var arg" + s + " = argType" + s + ".readValueFromPointer(args" + (l ? "+" + l : "") + ");\n", l += r[s + 1].argPackAdvance;
      for (c += "    var rv = handle[name](" + u + ");\n", s = 0; s < e - 1; ++s) r[s + 1].deleteObject && (c += "    argType" + s + ".deleteObject(arg" + s + ");\n");
      n.isVoid || (c += "    return retType.toWireType(destructors, rv);\n"), c += "};\n", a.push(c);
      var f, d, p = Mt(Function, a).apply(null, i);
      return f = p, d = Jt.length, Jt.push(f), d
    },
    fb: function (t) {
      return t = Zt(t), Wt(e[t])
    },
    s: function (e, t) {
      return Wt((e = Xt(e))[t = Xt(t)])
    },
    o: function (e) {
      e > 4 && (Nt[e].refcount += 1)
    },
    lc: function (e) {
      return "number" == typeof (e = Xt(e))
    },
    Na: function (t, r, n, o) {
      t = Xt(t);
      var a = tr[r];
      return a || (a = function (t) {
        for (var r = "", n = 0; n < t; ++n) r += (0 !== n ? ", " : "") + "arg" + n;
        var o = "return function emval_allocator_" + t + "(constructor, argTypes, args) {\n";
        for (n = 0; n < t; ++n) o += "var argType" + n + " = requireRegisteredType(Module['HEAP32'][(argTypes >>> 2) + " + n + '], "parameter ' + n + '");\nvar arg' + n + " = argType" + n + ".readValueFromPointer(args);\nargs += argType" + n + "['argPackAdvance'];\n";
        return o += "var obj = new constructor(" + r + ");\nreturn __emval_register(obj);\n}\n", new Function("requireRegisteredType", "Module", "__emval_register", o)(qt, e, Wt)
      }(r), tr[r] = a), a(t, n, o)
    },
    k: function (e) {
      return Wt(Zt(e))
    },
    bc: function (e) {
      return !(e = Xt(e))
    },
    m: function (e) {
      jt(Nt[e].value), Ut(e)
    },
    P: function (e, t, r) {
      e = Xt(e), t = Xt(t), r = Xt(r), e[t] = r
    },
    n: function (e, t) {
      return Wt((e = qt(e, "_emval_take_value")).readValueFromPointer(t))
    },
    c: function () {
      re()
    },
    ec: function (e, t, r, n, o) {
      if (ur.currentCtx) {
        var a = ur.buffers[e];
        if (a)
          if (o <= 0) ur.currentCtx.err = 40963;
          else {
            var i = null;
            try {
              switch (t) {
                case 4352:
                  if (n > 0)
                    for (var u = (i = ur.currentCtx.audioCtx.createBuffer(1, n, o)).getChannelData(0), s = 0; s < n; ++s) u[s] = .0078125 * A[r++] - 1;
                  a.bytesPerSample = 1, a.channels = 1, a.length = n;
                  break;
                case 4353:
                  if (n > 0)
                    for (u = (i = ur.currentCtx.audioCtx.createBuffer(1, n >> 1, o)).getChannelData(0), r >>= 1, s = 0; s < n >> 1; ++s) u[s] = 30517578125e-15 * L[r++];
                  a.bytesPerSample = 2, a.channels = 1, a.length = n >> 1;
                  break;
                case 4354:
                  if (n > 0) {
                    u = (i = ur.currentCtx.audioCtx.createBuffer(2, n >> 1, o)).getChannelData(0);
                    var c = i.getChannelData(1);
                    for (s = 0; s < n >> 1; ++s) u[s] = .0078125 * A[r++] - 1, c[s] = .0078125 * A[r++] - 1
                  }
                  a.bytesPerSample = 1, a.channels = 2, a.length = n >> 1;
                  break;
                case 4355:
                  if (n > 0)
                    for (u = (i = ur.currentCtx.audioCtx.createBuffer(2, n >> 2, o)).getChannelData(0), c = i.getChannelData(1), r >>= 1, s = 0; s < n >> 2; ++s) u[s] = 30517578125e-15 * L[r++], c[s] = 30517578125e-15 * L[r++];
                  a.bytesPerSample = 2, a.channels = 2, a.length = n >> 2;
                  break;
                case 65552:
                  if (n > 0)
                    for (u = (i = ur.currentCtx.audioCtx.createBuffer(1, n >> 2, o)).getChannelData(0), r >>= 2, s = 0; s < n >> 2; ++s) u[s] = B[r++];
                  a.bytesPerSample = 4, a.channels = 1, a.length = n >> 2;
                  break;
                case 65553:
                  if (n > 0)
                    for (u = (i = ur.currentCtx.audioCtx.createBuffer(2, n >> 3, o)).getChannelData(0), c = i.getChannelData(1), r >>= 2, s = 0; s < n >> 3; ++s) u[s] = B[r++], c[s] = B[r++];
                  a.bytesPerSample = 4, a.channels = 2, a.length = n >> 3;
                  break;
                default:
                  return void(ur.currentCtx.err = 40963)
              }
              a.frequency = o, a.audioBuf = i
            } catch (l) {
              return void(ur.currentCtx.err = 40963)
            }
          }
        else ur.currentCtx.err = 40963
      }
    },
    cc: function (e, t) {
      if (ur.currentCtx) {
        for (var r = 0; r < e; ++r)
          if (0 !== (n = j[t + 4 * r >> 2])) {
            if (!ur.buffers[n]) return void(ur.currentCtx.err = 40961);
            if (ur.buffers[n].refCount) return void(ur.currentCtx.err = 40964)
          } for (r = 0; r < e; ++r) {
          var n;
          0 !== (n = j[t + 4 * r >> 2]) && (ur.deviceRefCounts[ur.buffers[n].deviceId]--, delete ur.buffers[n], ur.freeIds.push(n))
        }
      }
    },
    dc: function (e, t) {
      if (ur.currentCtx) {
        for (var r = 0; r < e; ++r) {
          var n = j[t + 4 * r >> 2];
          if (!ur.currentCtx.sources[n]) return void(ur.currentCtx.err = 40961)
        }
        for (r = 0; r < e; ++r) n = j[t + 4 * r >> 2], ur.setSourceState(ur.currentCtx.sources[n], 4116), sr(n, 4105, 0), delete ur.currentCtx.sources[n], ur.freeIds.push(n)
      }
    },
    fc: function (e, t) {
      if (ur.currentCtx)
        for (var r = 0; r < e; ++r) {
          var n = {
            deviceId: ur.currentCtx.deviceId,
            id: ur.newId(),
            refCount: 0,
            audioBuf: null,
            frequency: 0,
            bytesPerSample: 2,
            channels: 1,
            length: 0
          };
          ur.deviceRefCounts[n.deviceId]++, ur.buffers[n.id] = n, j[t + 4 * r >> 2] = n.id
        }
    },
    gc: function (e, t) {
      if (ur.currentCtx)
        for (var r = 0; r < e; ++r) {
          var n = ur.currentCtx.audioCtx.createGain();
          n.connect(ur.currentCtx.gain);
          var o = {
            context: ur.currentCtx,
            id: ur.newId(),
            type: 4144,
            state: 4113,
            bufQueue: [ur.buffers[0]],
            audioQueue: [],
            looping: !1,
            pitch: 1,
            dopplerShift: 1,
            gain: n,
            minGain: 0,
            maxGain: 1,
            panner: null,
            bufsProcessed: 0,
            bufStartTime: Number.NEGATIVE_INFINITY,
            bufOffset: 0,
            relative: !1,
            refDistance: 1,
            maxDistance: 340282e33,
            rolloffFactor: 1,
            position: [0, 0, 0],
            velocity: [0, 0, 0],
            direction: [0, 0, 0],
            coneOuterGain: 0,
            coneInnerAngle: 360,
            coneOuterAngle: 360,
            distanceModel: 53250,
            spatialize: 2,
            get playbackRate() {
              return this.pitch * this.dopplerShift
            }
          };
          ur.currentCtx.sources[o.id] = o, j[t + 4 * r >> 2] = o.id
        }
    },
    E: function () {
      if (ur.currentCtx) {
        var e = ur.currentCtx.err;
        return ur.currentCtx.err = 0, e
      }
      return 40964
    },
    hb: function (e, t, r) {
      var n = ur.getSourceParam("alGetSourcei", e, t);
      if (null !== n)
        if (r) switch (t) {
          case 514:
          case 4097:
          case 4098:
          case 4103:
          case 4105:
          case 4112:
          case 4117:
          case 4118:
          case 4128:
          case 4129:
          case 4131:
          case 4132:
          case 4133:
          case 4134:
          case 4135:
          case 4628:
          case 8201:
          case 8202:
          case 53248:
            j[r >> 2] = n;
            break;
          default:
            return void(ur.currentCtx.err = 40962)
        } else ur.currentCtx.err = 40963
    },
    y: function (e) {
      if (!ur.currentCtx) return 0;
      if (ur.stringCache[e]) return ur.stringCache[e];
      var t;
      switch (e) {
        case 0:
          t = "No Error";
          break;
        case 40961:
          t = "Invalid Name";
          break;
        case 40962:
          t = "Invalid Enum";
          break;
        case 40963:
          t = "Invalid Value";
          break;
        case 40964:
          t = "Invalid Operation";
          break;
        case 40965:
          t = "Out of Memory";
          break;
        case 45057:
          t = "Emscripten";
          break;
        case 45058:
          t = "1.1";
          break;
        case 45059:
          t = "WebAudio";
          break;
        case 45060:
          for (var r in t = "", ur.AL_EXTENSIONS) t = (t = t.concat(r)).concat(" ");
          t = t.trim();
          break;
        default:
          return ur.currentCtx.err = 40962, 0
      }
      return t = E(Ir(t), 0), ur.stringCache[e] = t, t
    },
    hc: function (e) {
      if (ur.currentCtx) {
        var t = ur.currentCtx.sources[e];
        t ? ur.setSourceState(t, 4115) : ur.currentCtx.err = 40961
      }
    },
    ib: function (e) {
      if (ur.currentCtx) {
        var t = ur.currentCtx.sources[e];
        t ? ur.setSourceState(t, 4114) : ur.currentCtx.err = 40961
      }
    },
    ic: function (e) {
      if (ur.currentCtx) {
        var t = ur.currentCtx.sources[e];
        t ? ur.setSourceState(t, 4116) : ur.currentCtx.err = 40961
      }
    },
    gb: function (e, t, r) {
      switch (t) {
        case 4097:
        case 4098:
        case 4099:
        case 4106:
        case 4109:
        case 4110:
        case 4128:
        case 4129:
        case 4130:
        case 4131:
        case 4132:
        case 4133:
        case 4134:
        case 8203:
          ur.setSourceParam("alSourcef", e, t, r);
          break;
        default:
          ur.setSourceParam("alSourcef", e, t, null)
      }
    },
    jb: sr,
    jc: function (e) {
      return !(e in ur.deviceRefCounts) || ur.deviceRefCounts[e] > 0 ? 0 : (delete ur.deviceRefCounts[e], ur.freeIds.push(e), 1)
    },
    nc: function (e, t) {
      if (!(e in ur.deviceRefCounts)) return ur.alcErr = 40961, 0;
      var r = null,
        n = [],
        o = null;
      if (t >>= 2)
        for (var a = 0, i = 0; a = j[t++], n.push(a), 0 !== a;) switch (i = j[t++], n.push(i), a) {
          case 4103:
            r || (r = {}), r.sampleRate = i;
            break;
          case 4112:
          case 4113:
            break;
          case 6546:
            switch (i) {
              case 0:
                o = !1;
                break;
              case 1:
                o = !0;
                break;
              case 2:
                break;
              default:
                return ur.alcErr = 40964, 0
            }
            break;
          case 6550:
            if (0 !== i) return ur.alcErr = 40964, 0;
            break;
          default:
            return ur.alcErr = 40964, 0
        }
      var u = window.AudioContext || window.webkitAudioContext,
        s = null;
      try {
        s = r ? new u(r) : new u
      } catch (p) {
        return "NotSupportedError" === p.name ? ur.alcErr = 40964 : ur.alcErr = 40961, 0
      }! function (e, t) {
        t || (t = [document, document.getElementById("canvas")]), ["keydown", "mousedown", "touchstart"].forEach((function (r) {
          t.forEach((function (t) {
            t && function (e, t, r) {
              e.addEventListener(t, r, {
                once: !0
              })
            }(t, r, (function () {
              "suspended" === e.state && e.resume()
            }))
          }))
        }))
      }(s), void 0 === s.createGain && (s.createGain = s.createGainNode);
      var c = s.createGain();
      c.connect(s.destination);
      var l = {
        deviceId: e,
        id: ur.newId(),
        attrs: n,
        audioCtx: s,
        listener: {
          position: [0, 0, 0],
          velocity: [0, 0, 0],
          direction: [0, 0, 0],
          up: [0, 0, 0]
        },
        sources: [],
        interval: setInterval((function () {
          ur.scheduleContextAudio(l)
        }), ur.QUEUE_INTERVAL),
        gain: c,
        distanceModel: 53250,
        speedOfSound: 343.3,
        dopplerFactor: 1,
        sourceDistanceModel: !1,
        hrtf: o || !1,
        _err: 0,
        get err() {
          return this._err
        },
        set err(e) {
          0 !== this._err && 0 !== e || (this._err = e)
        }
      };
      if (ur.deviceRefCounts[e]++, ur.contexts[l.id] = l, null !== o)
        for (var f in ur.contexts) {
          var d = ur.contexts[f];
          d.deviceId === e && (d.hrtf = o, ur.updateContextGlobal(d))
        }
      return l.id
    },
    kc: function (e) {
      var t = ur.contexts[e];
      ur.currentCtx !== t ? (ur.contexts[e].interval && clearInterval(ur.contexts[e].interval), ur.deviceRefCounts[t.deviceId]--, delete ur.contexts[e], ur.freeIds.push(e)) : ur.alcErr = 40962
    },
    mc: function (e) {
      var t = ur.alcErr;
      return ur.alcErr = 0, t
    },
    kb: function (e, t) {
      if (ur.alcStringCache[t]) return ur.alcStringCache[t];
      var r;
      switch (t) {
        case 0:
          r = "No Error";
          break;
        case 40961:
          r = "Invalid Device";
          break;
        case 40962:
          r = "Invalid Context";
          break;
        case 40963:
          r = "Invalid Enum";
          break;
        case 40964:
          r = "Invalid Value";
          break;
        case 40965:
          r = "Out of Memory";
          break;
        case 4100:
          if ("undefined" == typeof AudioContext && "undefined" == typeof webkitAudioContext) return 0;
          r = ur.DEVICE_NAME;
          break;
        case 4101:
          r = "undefined" != typeof AudioContext || "undefined" != typeof webkitAudioContext ? ur.DEVICE_NAME.concat("\0") : "\0";
          break;
        case 785:
          r = ur.CAPTURE_DEVICE_NAME;
          break;
        case 784:
          if (0 === e) r = ur.CAPTURE_DEVICE_NAME.concat("\0");
          else {
            var n = ur.requireValidCaptureDevice(e, "alcGetString");
            if (!n) return 0;
            r = n.deviceName
          }
          break;
        case 4102:
          if (!e) return ur.alcErr = 40961, 0;
          for (var o in r = "", ur.ALC_EXTENSIONS) r = (r = r.concat(o)).concat(" ");
          r = r.trim();
          break;
        default:
          return ur.alcErr = 40963, 0
      }
      return r = E(Ir(r), 0), ur.alcStringCache[t] = r, r
    },
    lb: function (e) {
      return 0 === e ? (ur.currentCtx = null, 0) : (ur.currentCtx = ur.contexts[e], 1)
    },
    oc: function (e) {
      if (e && x(e) !== ur.DEVICE_NAME) return 0;
      if ("undefined" != typeof AudioContext || "undefined" != typeof webkitAudioContext) {
        var t = ur.newId();
        return ur.deviceRefCounts[t] = 0, t
      }
      return 0
    },
    _a: pe,
    xb: function () {
      return e.useCamTexture && e.texture ? (e.camTextureId || (e.camTextureId = dr.getNewId(dr.textures), e.texture.name = e.camTextureId, dr.textures[e.camTextureId] = e.texture), e.camTextureId) : 0
    },
    pc: function () {
      re("To use dlopen, you need to use Emscripten's linking support, see https://github.com/emscripten-core/emscripten/wiki/Linking")
    },
    nb: function (e, t) {
      re("To use dlopen, you need to use Emscripten's linking support, see https://github.com/emscripten-core/emscripten/wiki/Linking")
    },
    f: function (e, t) {
      re("To use dlopen, you need to use Emscripten's linking support, see https://github.com/emscripten-core/emscripten/wiki/Linking")
    },
    Xb: function () {
      return 2147483648
    },
    Eb: function (e, t, r) {
      A.copyWithin(e, t, t + r)
    },
    Fb: function (e) {
      var t, r, n = A.length,
        o = 2147483648;
      if ((e >>>= 0) > o) return !1;
      for (var a = 1; a <= 4; a *= 2) {
        var i = n * (1 + .2 / a);
        if (i = Math.min(i, e + 100663296), cr(Math.min(o, ((t = Math.max(e, i)) % (r = 65536) > 0 && (t += r - t % r), t)))) return !0
      }
      return !1
    },
    Kb: function (e) {
      for (var t = ue(); ue() - t < e;);
    },
    Ib: function (e, t) {
      try {
        var r = 0;
        return fr().forEach((function (n, o) {
          var a = t + r;
          j[e + 4 * o >> 2] = a,
            function (e, t, r) {
              for (var n = 0; n < e.length; ++n) T[t++ >> 0] = e.charCodeAt(n);
              r || (T[t >> 0] = 0)
            }(n, a), r += n.length + 1
        })), 0
      } catch (n) {
        return void 0 !== Pe && n instanceof Pe.ErrnoError || re(n), n.errno
      }
    },
    Jb: function (e, t) {
      try {
        var r = fr();
        j[e >> 2] = r.length;
        var n = 0;
        return r.forEach((function (e) {
          n += e.length + 1
        })), j[t >> 2] = n, 0
      } catch (o) {
        return void 0 !== Pe && o instanceof Pe.ErrnoError || re(o), o.errno
      }
    },
    xa: function (e) {
      try {
        var t = Te.getStreamFromFD(e);
        return Pe.close(t), 0
      } catch (r) {
        return void 0 !== Pe && r instanceof Pe.ErrnoError || re(r), r.errno
      }
    },
    Rb: function (e, t, r, n) {
      try {
        var o = Te.getStreamFromFD(e),
          a = Te.doReadv(o, t, r);
        return j[n >> 2] = a, 0
      } catch (i) {
        return void 0 !== Pe && i instanceof Pe.ErrnoError || re(i), i.errno
      }
    },
    Cb: function (e, t, r, n, o) {
      try {
        var a = Te.getStreamFromFD(e),
          i = 4294967296 * r + (t >>> 0),
          u = 9007199254740992;
        return i <= -u || i >= u ? -61 : (Pe.llseek(a, i, n), ie = [a.position >>> 0, (ae = a.position, +Math.abs(ae) >= 1 ? ae > 0 ? (0 | Math.min(+Math.floor(ae / 4294967296), 4294967295)) >>> 0 : ~~+Math.ceil((ae - +(~~ae >>> 0)) / 4294967296) >>> 0 : 0)], j[o >> 2] = ie[0], j[o + 4 >> 2] = ie[1], a.getdents && 0 === i && 0 === n && (a.getdents = null), 0)
      } catch (s) {
        return void 0 !== Pe && s instanceof Pe.ErrnoError || re(s), s.errno
      }
    },
    Ka: function (e, t, r, n) {
      try {
        var o = Te.getStreamFromFD(e),
          a = Te.doWritev(o, t, r);
        return j[n >> 2] = a, 0
      } catch (i) {
        return void 0 !== Pe && i instanceof Pe.ErrnoError || re(i), i.errno
      }
    },
    Gb: function e(t, r) {
      e.randomDevice || (e.randomDevice = ke());
      for (var n = 0; n < r; n++) T[t + n >> 0] = e.randomDevice();
      return 0
    },
    U: function (e) {
      var t = Date.now();
      return j[e >> 2] = t / 1e3 | 0, j[e + 4 >> 2] = t % 1e3 * 1e3 | 0, 0
    },
    za: function (e) {
      Pr.activeTexture(e)
    },
    Ta: function (e, t) {
      Pr.attachShader(dr.programs[e], dr.shaders[t])
    },
    Ra: function (e, t, r) {
      Pr.bindAttribLocation(dr.programs[e], t, x(r))
    },
    l: function (e, t) {
      35051 == e ? Pr.currentPixelPackBufferBinding = t : 35052 == e && (Pr.currentPixelUnpackBufferBinding = t), Pr.bindBuffer(e, dr.buffers[t])
    },
    rb: function (e, t, r, n, o) {
      Pr.bindBufferRange(e, t, dr.buffers[r], n, o)
    },
    q: function (e, t) {
      Pr.bindFramebuffer(e, dr.framebuffers[t])
    },
    A: function (e, t) {
      Pr.bindRenderbuffer(e, dr.renderbuffers[t])
    },
    i: function (e, t) {
      Pr.bindTexture(e, dr.textures[t])
    },
    H: function (e) {
      Pr.bindVertexArray(dr.vaos[e])
    },
    oa: function (e) {
      Pr.blendEquation(e)
    },
    ga: function (e, t) {
      Pr.blendFunc(e, t)
    },
    Pa: function (e, t, r, n, o, a, i, u, s, c) {
      Pr.blitFramebuffer(e, t, r, n, o, a, i, u, s, c)
    },
    Z: function (e, t, r, n) {
      dr.currentContext.version >= 2 ? r ? Pr.bufferData(e, A, n, r, t) : Pr.bufferData(e, t, n) : Pr.bufferData(e, r ? A.subarray(r, r + t) : t, n)
    },
    Ca: function (e, t, r, n) {
      dr.currentContext.version >= 2 ? Pr.bufferSubData(e, t, A, n, r) : Pr.bufferSubData(e, t, A.subarray(n, n + r))
    },
    ta: function (e) {
      Pr.clear(e)
    },
    F: function (e, t, r) {
      Pr.clearBufferfv(e, t, B, r >> 2)
    },
    Ia: function (e, t, r, n) {
      Pr.clearColor(e, t, r, n)
    },
    Fa: function (e) {
      Pr.clearDepth(e)
    },
    z: function (e, t, r, n) {
      Pr.colorMask(!!e, !!t, !!r, !!n)
    },
    Ua: function (e) {
      Pr.compileShader(dr.shaders[e])
    },
    W: function (e, t, r, n, o, a, i, u, s) {
      dr.currentContext.version >= 2 ? Pr.currentPixelUnpackBufferBinding ? Pr.compressedTexSubImage2D(e, t, r, n, o, a, i, u, s) : Pr.compressedTexSubImage2D(e, t, r, n, o, a, i, A, s, u) : Pr.compressedTexSubImage2D(e, t, r, n, o, a, i, s ? A.subarray(s, s + u) : null)
    },
    ob: function (e, t, r, n, o, a, i, u, s, c, l) {
      Pr.currentPixelUnpackBufferBinding ? Pr.compressedTexSubImage3D(e, t, r, n, o, a, i, u, s, c, l) : Pr.compressedTexSubImage3D(e, t, r, n, o, a, i, u, s, A, l, c)
    },
    sb: function (e, t, r, n, o, a, i, u) {
      Pr.copyTexSubImage2D(e, t, r, n, o, a, i, u)
    },
    Xa: function () {
      var e = dr.getNewId(dr.programs),
        t = Pr.createProgram();
      return t.name = e, t.maxUniformLength = t.maxAttributeLength = t.maxUniformBlockNameLength = 0, t.uniformIdCounter = 1, dr.programs[e] = t, e
    },
    Wa: function (e) {
      var t = dr.getNewId(dr.shaders);
      return dr.shaders[t] = Pr.createShader(e), t
    },
    r: function (e, t) {
      for (var r = 0; r < e; r++) {
        var n = j[t + 4 * r >> 2],
          o = dr.buffers[n];
        o && (Pr.deleteBuffer(o), o.name = 0, dr.buffers[n] = null, n == Pr.currentPixelPackBufferBinding && (Pr.currentPixelPackBufferBinding = 0), n == Pr.currentPixelUnpackBufferBinding && (Pr.currentPixelUnpackBufferBinding = 0))
      }
    },
    ja: function (e, t) {
      for (var r = 0; r < e; ++r) {
        var n = j[t + 4 * r >> 2],
          o = dr.framebuffers[n];
        o && (Pr.deleteFramebuffer(o), o.name = 0, dr.framebuffers[n] = null)
      }
    },
    G: function (e) {
      if (e) {
        var t = dr.programs[e];
        t ? (Pr.deleteProgram(t), t.name = 0, dr.programs[e] = null) : dr.recordError(1281)
      }
    },
    S: function (e, t) {
      for (var r = 0; r < e; r++) {
        var n = j[t + 4 * r >> 2],
          o = dr.renderbuffers[n];
        o && (Pr.deleteRenderbuffer(o), o.name = 0, dr.renderbuffers[n] = null)
      }
    },
    qa: function (e) {
      if (e) {
        var t = dr.shaders[e];
        t ? (Pr.deleteShader(t), dr.shaders[e] = null) : dr.recordError(1281)
      }
    },
    t: function (e, t) {
      for (var r = 0; r < e; r++) {
        var n = j[t + 4 * r >> 2],
          o = dr.textures[n];
        o && (Pr.deleteTexture(o), o.name = 0, dr.textures[n] = null)
      }
    },
    ea: function (e, t) {
      for (var r = 0; r < e; r++) {
        var n = j[t + 4 * r >> 2];
        Pr.deleteVertexArray(dr.vaos[n]), dr.vaos[n] = null
      }
    },
    fa: function (e) {
      Pr.depthFunc(e)
    },
    Q: function (e) {
      Pr.depthMask(!!e)
    },
    v: function (e) {
      Pr.disable(e)
    },
    ya: function (e, t, r) {
      Pr.drawArrays(e, t, r)
    },
    qb: function (e, t) {
      for (var r = pr[e], n = 0; n < e; n++) r[n] = j[t + 4 * n >> 2];
      Pr.drawBuffers(r)
    },
    Qa: function (e, t, r, n) {
      Pr.drawElements(e, t, r, n)
    },
    T: function (e) {
      Pr.enable(e)
    },
    Da: function (e) {
      Pr.enableVertexAttribArray(e)
    },
    Ga: function (e, t, r, n) {
      Pr.framebufferRenderbuffer(e, t, r, dr.renderbuffers[n])
    },
    ia: function (e, t, r, n, o) {
      Pr.framebufferTexture2D(e, t, r, dr.textures[n], o)
    },
    _: function (e, t) {
      mr(e, t, "createBuffer", dr.buffers)
    },
    ua: function (e, t) {
      mr(e, t, "createFramebuffer", dr.framebuffers)
    },
    Ha: function (e, t) {
      mr(e, t, "createRenderbuffer", dr.renderbuffers)
    },
    L: function (e, t) {
      mr(e, t, "createTexture", dr.textures)
    },
    Ea: function (e, t) {
      mr(e, t, "createVertexArray", dr.vaos)
    },
    sa: function (e) {
      Pr.generateMipmap(e)
    },
    eb: function () {
      var e = Pr.getError() || dr.lastError;
      return dr.lastError = 0, e
    },
    B: function (e, t) {
      hr(e, t, 0)
    },
    Aa: function (e, t, r, n) {
      var o = Pr.getProgramInfoLog(dr.programs[e]);
      null === o && (o = "(unknown error)");
      var a = t > 0 && n ? F(o, n, t) : 0;
      r && (j[r >> 2] = a)
    },
    Y: function (e, t, r) {
      if (r)
        if (e >= dr.counter) dr.recordError(1281);
        else if (e = dr.programs[e], 35716 == t) {
        var n = Pr.getProgramInfoLog(e);
        null === n && (n = "(unknown error)"), j[r >> 2] = n.length + 1
      } else if (35719 == t) {
        if (!e.maxUniformLength)
          for (var o = 0; o < Pr.getProgramParameter(e, 35718); ++o) e.maxUniformLength = Math.max(e.maxUniformLength, Pr.getActiveUniform(e, o).name.length + 1);
        j[r >> 2] = e.maxUniformLength
      } else if (35722 == t) {
        if (!e.maxAttributeLength)
          for (o = 0; o < Pr.getProgramParameter(e, 35721); ++o) e.maxAttributeLength = Math.max(e.maxAttributeLength, Pr.getActiveAttrib(e, o).name.length + 1);
        j[r >> 2] = e.maxAttributeLength
      } else if (35381 == t) {
        if (!e.maxUniformBlockNameLength)
          for (o = 0; o < Pr.getProgramParameter(e, 35382); ++o) e.maxUniformBlockNameLength = Math.max(e.maxUniformBlockNameLength, Pr.getActiveUniformBlockName(e, o).length + 1);
        j[r >> 2] = e.maxUniformBlockNameLength
      } else j[r >> 2] = Pr.getProgramParameter(e, t);
      else dr.recordError(1281)
    },
    Sa: function (e, t, r, n) {
      var o = Pr.getShaderInfoLog(dr.shaders[e]);
      null === o && (o = "(unknown error)");
      var a = t > 0 && n ? F(o, n, t) : 0;
      r && (j[r >> 2] = a)
    },
    ra: function (e, t, r) {
      if (r)
        if (35716 == t) {
          var n = Pr.getShaderInfoLog(dr.shaders[e]);
          null === n && (n = "(unknown error)");
          var o = n ? n.length + 1 : 0;
          j[r >> 2] = o
        } else if (35720 == t) {
        var a = Pr.getShaderSource(dr.shaders[e]),
          i = a ? a.length + 1 : 0;
        j[r >> 2] = i
      } else j[r >> 2] = Pr.getShaderParameter(dr.shaders[e], t);
      else dr.recordError(1281)
    },
    Ya: function (e) {
      var t = dr.stringCache[e];
      if (!t) {
        switch (e) {
          case 7939:
            var r = Pr.getSupportedExtensions() || [];
            t = vr((r = r.concat(r.map((function (e) {
              return "GL_" + e
            })))).join(" "));
            break;
          case 7936:
          case 7937:
          case 37445:
          case 37446:
            var n = Pr.getParameter(e);
            n || dr.recordError(1280), t = n && vr(n);
            break;
          case 7938:
            var o = Pr.getParameter(7938);
            t = vr(o = dr.currentContext.version >= 2 ? "OpenGL ES 3.0 (" + o + ")" : "OpenGL ES 2.0 (" + o + ")");
            break;
          case 35724:
            var a = Pr.getParameter(35724),
              i = a.match(/^WebGL GLSL ES ([0-9]\.[0-9][0-9]?)(?:$| .*)/);
            null !== i && (3 == i[1].length && (i[1] = i[1] + "0"), a = "OpenGL ES GLSL ES " + i[1] + " (" + a + ")"), t = vr(a);
            break;
          default:
            dr.recordError(1280)
        }
        dr.stringCache[e] = t
      }
      return t
    },
    X: function (e, t) {
      function r(e) {
        return "]" == e.slice(-1) && e.lastIndexOf("[")
      }
      t = x(t);
      var n, o, a, i = (e = dr.programs[e]).uniformLocsById,
        u = e.uniformSizeAndIdsByName,
        s = 0,
        c = t,
        l = r(t);
      if (!i)
        for (e.uniformLocsById = i = {}, e.uniformArrayNamesById = {}, n = 0; n < Pr.getProgramParameter(e, 35718); ++n) {
          var f = Pr.getActiveUniform(e, n),
            d = f.name,
            p = f.size,
            m = r(d),
            h = m > 0 ? d.slice(0, m) : d,
            v = e.uniformIdCounter;
          for (e.uniformIdCounter += p, u[h] = [p, v], o = 0; o < p; ++o) i[v] = o, e.uniformArrayNamesById[v++] = h
        }
      l > 0 && (a = t.slice(l + 1), s = parseInt(a) >>> 0, c = t.slice(0, l));
      var g = u[c];
      return g && s < g[0] && (i[s += g[1]] = i[s] || Pr.getUniformLocation(e, t)) ? s : -1
    },
    ma: function (e, t, r) {
      for (var n = pr[t], o = 0; o < t; o++) n[o] = j[r + 4 * o >> 2];
      Pr.invalidateFramebuffer(e, n)
    },
    Ba: function (e) {
      e = dr.programs[e], Pr.linkProgram(e), e.uniformLocsById = 0, e.uniformSizeAndIdsByName = {}
    },
    ha: function (e, t) {
      3317 == e && (dr.unpackAlignment = t), Pr.pixelStorei(e, t)
    },
    vb: function (e) {
      Pr.readBuffer(e)
    },
    Ja: function (e, t, r, n, o, a, i) {
      if (dr.currentContext.version >= 2)
        if (Pr.currentPixelPackBufferBinding) Pr.readPixels(e, t, r, n, o, a, i);
        else {
          var u = gr(a);
          Pr.readPixels(e, t, r, n, o, a, u, i >> yr(u))
        }
      else {
        var s = br(a, o, r, n, i);
        s ? Pr.readPixels(e, t, r, n, o, a, s) : dr.recordError(1280)
      }
    },
    M: function (e, t, r, n) {
      Pr.renderbufferStorage(e, t, r, n)
    },
    R: function (e, t, r, n, o) {
      Pr.renderbufferStorageMultisample(e, t, r, n, o)
    },
    Va: function (e, t, r, n) {
      var o = dr.getSource(e, t, r, n);
      Pr.shaderSource(dr.shaders[e], o)
    },
    j: function (e, t, r, n, o, a, i, u, s) {
      if (dr.currentContext.version >= 2)
        if (Pr.currentPixelUnpackBufferBinding) Pr.texImage2D(e, t, r, n, o, a, i, u, s);
        else if (s) {
        var c = gr(u);
        Pr.texImage2D(e, t, r, n, o, a, i, u, c, s >> yr(c))
      } else Pr.texImage2D(e, t, r, n, o, a, i, u, null);
      else Pr.texImage2D(e, t, r, n, o, a, i, u, s ? br(u, i, n, o, s) : null)
    },
    h: function (e, t, r) {
      Pr.texParameteri(e, t, r)
    },
    na: function (e, t, r, n, o) {
      Pr.texStorage2D(e, t, r, n, o)
    },
    pb: function (e, t, r, n, o, a) {
      Pr.texStorage3D(e, t, r, n, o, a)
    },
    w: function (e, t, r, n, o, a, i, u, s) {
      if (dr.currentContext.version >= 2)
        if (Pr.currentPixelUnpackBufferBinding) Pr.texSubImage2D(e, t, r, n, o, a, i, u, s);
        else if (s) {
        var c = gr(u);
        Pr.texSubImage2D(e, t, r, n, o, a, i, u, c, s >> yr(c))
      } else Pr.texSubImage2D(e, t, r, n, o, a, i, u, null);
      else {
        var l = null;
        s && (l = br(u, i, o, a, s)), Pr.texSubImage2D(e, t, r, n, o, a, i, u, l)
      }
    },
    Oa: function (e, t, r, n, o, a, i, u, s, c, l) {
      if (Pr.currentPixelUnpackBufferBinding) Pr.texSubImage3D(e, t, r, n, o, a, i, u, s, c, l);
      else if (l) {
        var f = gr(c);
        Pr.texSubImage3D(e, t, r, n, o, a, i, u, s, c, f, l >> yr(f))
      } else Pr.texSubImage3D(e, t, r, n, o, a, i, u, s, c, null)
    },
    pa: function (e, t) {
      Pr.uniform1i(wr(e), t)
    },
    qc: function (e, t) {
      Pr.uniform1ui(wr(e), t)
    },
    ub: function (e, t, r) {
      if (dr.currentContext.version >= 2) Pr.uniform4fv(wr(e), B, r >> 2, 4 * t);
      else {
        if (t <= 72) {
          var n = _r[4 * t - 1],
            o = B;
          r >>= 2;
          for (var a = 0; a < 4 * t; a += 4) {
            var i = r + a;
            n[a] = o[i], n[a + 1] = o[i + 1], n[a + 2] = o[i + 2], n[a + 3] = o[i + 3]
          }
        } else n = B.subarray(r >> 2, r + 16 * t >> 2);
        Pr.uniform4fv(wr(e), n)
      }
    },
    tb: function (e, t, r, n) {
      if (dr.currentContext.version >= 2) Pr.uniformMatrix4fv(wr(e), !!r, B, n >> 2, 16 * t);
      else {
        if (t <= 18) {
          var o = _r[16 * t - 1],
            a = B;
          n >>= 2;
          for (var i = 0; i < 16 * t; i += 16) {
            var u = n + i;
            o[i] = a[u], o[i + 1] = a[u + 1], o[i + 2] = a[u + 2], o[i + 3] = a[u + 3], o[i + 4] = a[u + 4], o[i + 5] = a[u + 5], o[i + 6] = a[u + 6], o[i + 7] = a[u + 7], o[i + 8] = a[u + 8], o[i + 9] = a[u + 9], o[i + 10] = a[u + 10], o[i + 11] = a[u + 11], o[i + 12] = a[u + 12], o[i + 13] = a[u + 13], o[i + 14] = a[u + 14], o[i + 15] = a[u + 15]
          }
        } else o = B.subarray(n >> 2, n + 64 * t >> 2);
        Pr.uniformMatrix4fv(wr(e), !!r, o)
      }
    },
    K: function (e) {
      e = dr.programs[e], Pr.useProgram(e), Pr.currentProgram = e
    },
    V: function (e, t, r, n, o) {
      Pr.vertexAttribIPointer(e, t, r, n, o)
    },
    u: function (e, t, r, n, o, a) {
      Pr.vertexAttribPointer(e, t, r, !!n, o, a)
    },
    ba: function (e, t, r, n) {
      Pr.viewport(e, t, r, n)
    },
    $b: Ee,
    ka: function (e, t) {
      Er();
      var r = new Date(1e3 * j[e >> 2]);
      j[t >> 2] = r.getSeconds(), j[t + 4 >> 2] = r.getMinutes(), j[t + 8 >> 2] = r.getHours(), j[t + 12 >> 2] = r.getDate(), j[t + 16 >> 2] = r.getMonth(), j[t + 20 >> 2] = r.getFullYear() - 1900, j[t + 24 >> 2] = r.getDay();
      var n = new Date(r.getFullYear(), 0, 1),
        o = (r.getTime() - n.getTime()) / 864e5 | 0;
      j[t + 28 >> 2] = o, j[t + 36 >> 2] = -60 * r.getTimezoneOffset();
      var a = new Date(r.getFullYear(), 6, 1).getTimezoneOffset(),
        i = n.getTimezoneOffset(),
        u = 0 | (a != i && r.getTimezoneOffset() == Math.min(i, a));
      j[t + 32 >> 2] = u;
      var s = j[Wr() + (u ? 4 : 0) >> 2];
      return j[t + 40 >> 2] = s, t
    },
    va: function (e) {
      Er();
      var t = new Date(j[e + 20 >> 2] + 1900, j[e + 16 >> 2], j[e + 12 >> 2], j[e + 8 >> 2], j[e + 4 >> 2], j[e >> 2], 0),
        r = j[e + 32 >> 2],
        n = t.getTimezoneOffset(),
        o = new Date(t.getFullYear(), 0, 1),
        a = new Date(t.getFullYear(), 6, 1).getTimezoneOffset(),
        i = o.getTimezoneOffset(),
        u = Math.min(i, a);
      if (r < 0) j[e + 32 >> 2] = Number(a != i && u == n);
      else if (r > 0 != (u == n)) {
        var s = Math.max(i, a),
          c = r > 0 ? u : s;
        t.setTime(t.getTime() + 6e4 * (c - n))
      }
      j[e + 24 >> 2] = t.getDay();
      var l = (t.getTime() - o.getTime()) / 864e5 | 0;
      return j[e + 28 >> 2] = l, j[e >> 2] = t.getSeconds(), j[e + 4 >> 2] = t.getMinutes(), j[e + 8 >> 2] = t.getHours(), j[e + 12 >> 2] = t.getDate(), j[e + 16 >> 2] = t.getMonth(), t.getTime() / 1e3 | 0
    },
    la: function () {
      return 6
    },
    mb: function () {
      return 28
    },
    D: function (e) {},
    Hb: function (e, t, r, n) {
      return Dr(e, t, r, n)
    },
    Za: function (e) {
      var t = Date.now() / 1e3 | 0;
      return e && (j[e >> 2] = t), t
    }
  };
  ! function () {
    var t = {
      a: Rr
    };

    function n(t, r) {
      var n, o = t.exports;
      e.asm = o, V((g = e.asm.rc).buffer), N = e.asm.tc, n = e.asm.sc, H.unshift(n), te()
    }

    function o(e) {
      n(e.instance)
    }

    function a(e) {
      return (m || "function" != typeof fetch ? Promise.resolve().then((function () {
        return ce(se)
      })) : fetch(se, {
        credentials: "same-origin"
      }).then((function (e) {
        if (!e.ok) throw "failed to load wasm binary file at '" + se + "'";
        return e.arrayBuffer()
      })).catch((function () {
        return ce(se)
      }))).then((function (e) {
        return WebAssembly.instantiate(e, t)
      })).then(e, (function (e) {
        f("failed to asynchronously prepare wasm: " + e), re(e)
      }))
    }
    if (ee(), e.instantiateWasm) try {
      return e.instantiateWasm(t, n)
    } catch (i) {
      return f("Module.instantiateWasm callback failed with error: " + i), !1
    }(m || "function" != typeof WebAssembly.instantiateStreaming || ne(se) || "function" != typeof fetch ? a(o) : fetch(se, {
      credentials: "same-origin"
    }).then((function (e) {
      return WebAssembly.instantiateStreaming(e, t).then(o, (function (e) {
        return f("wasm streaming compile failed: " + e), f("falling back to ArrayBuffer instantiation"), a(o)
      }))
    }))).catch(r)
  }(), e.___wasm_call_ctors = function () {
    return (e.___wasm_call_ctors = e.asm.sc).apply(null, arguments)
  };
  var Br = e._malloc = function () {
      return (Br = e._malloc = e.asm.uc).apply(null, arguments)
    },
    Or = e._memset = function () {
      return (Or = e._memset = e.asm.vc).apply(null, arguments)
    },
    Nr = e.___errno_location = function () {
      return (Nr = e.___errno_location = e.asm.wc).apply(null, arguments)
    },
    Ur = e._free = function () {
      return (Ur = e._free = e.asm.xc).apply(null, arguments)
    },
    zr = e.___getTypeName = function () {
      return (zr = e.___getTypeName = e.asm.yc).apply(null, arguments)
    };
  e.___embind_register_native_and_builtin_types = function () {
    return (e.___embind_register_native_and_builtin_types = e.asm.zc).apply(null, arguments)
  };
  var $r, Wr = e.__get_tzname = function () {
      return (Wr = e.__get_tzname = e.asm.Ac).apply(null, arguments)
    },
    Gr = e.__get_daylight = function () {
      return (Gr = e.__get_daylight = e.asm.Bc).apply(null, arguments)
    },
    qr = e.__get_timezone = function () {
      return (qr = e.__get_timezone = e.asm.Cc).apply(null, arguments)
    },
    Qr = e.stackAlloc = function () {
      return (Qr = e.stackAlloc = e.asm.Dc).apply(null, arguments)
    },
    Yr = e._memalign = function () {
      return (Yr = e._memalign = e.asm.Ec).apply(null, arguments)
    };

  function Vr(e) {
    this.name = "ExitStatus", this.message = "Program terminated with exit(" + e + ")", this.status = e
  }

  function Xr(r) {
    function n() {
      $r || ($r = !0, e.calledRun = !0, w || (e.noFSInit || Pe.init.initialized || Pe.init(), le(H), t(e), e.onRuntimeInitialized && e.onRuntimeInitialized(), function () {
        if (e.postRun)
          for ("function" == typeof e.postRun && (e.postRun = [e.postRun]); e.postRun.length;) t = e.postRun.shift(), K.unshift(t);
        var t;
        le(K)
      }()))
    }
    Z > 0 || (function () {
      if (e.preRun)
        for ("function" == typeof e.preRun && (e.preRun = [e.preRun]); e.preRun.length;) t = e.preRun.shift(), X.unshift(t);
      var t;
      le(X)
    }(), Z > 0 || (e.setStatus ? (e.setStatus("Running..."), setTimeout((function () {
      setTimeout((function () {
        e.setStatus("")
      }), 1), n()
    }), 1)) : n()))
  }
  if (e.dynCall_jii = function () {
      return (e.dynCall_jii = e.asm.Fc).apply(null, arguments)
    }, e.dynCall_ji = function () {
      return (e.dynCall_ji = e.asm.Gc).apply(null, arguments)
    }, e.dynCall_viij = function () {
      return (e.dynCall_viij = e.asm.Hc).apply(null, arguments)
    }, e.dynCall_vijjiii = function () {
      return (e.dynCall_vijjiii = e.asm.Ic).apply(null, arguments)
    }, e.dynCall_viiiji = function () {
      return (e.dynCall_viiiji = e.asm.Jc).apply(null, arguments)
    }, e.dynCall_viiijii = function () {
      return (e.dynCall_viiijii = e.asm.Kc).apply(null, arguments)
    }, e.dynCall_viijii = function () {
      return (e.dynCall_viijii = e.asm.Lc).apply(null, arguments)
    }, e.dynCall_vij = function () {
      return (e.dynCall_vij = e.asm.Mc).apply(null, arguments)
    }, e.dynCall_viiiiij = function () {
      return (e.dynCall_viiiiij = e.asm.Nc).apply(null, arguments)
    }, e.dynCall_viiiij = function () {
      return (e.dynCall_viiiij = e.asm.Oc).apply(null, arguments)
    }, e.dynCall_viijji = function () {
      return (e.dynCall_viijji = e.asm.Pc).apply(null, arguments)
    }, e.dynCall_viiji = function () {
      return (e.dynCall_viiji = e.asm.Qc).apply(null, arguments)
    }, e.dynCall_jijjiii = function () {
      return (e.dynCall_jijjiii = e.asm.Rc).apply(null, arguments)
    }, e.dynCall_jiii = function () {
      return (e.dynCall_jiii = e.asm.Sc).apply(null, arguments)
    }, e.dynCall_jijiii = function () {
      return (e.dynCall_jijiii = e.asm.Tc).apply(null, arguments)
    }, e.dynCall_jijiiii = function () {
      return (e.dynCall_jijiiii = e.asm.Uc).apply(null, arguments)
    }, e.dynCall_jijii = function () {
      return (e.dynCall_jijii = e.asm.Vc).apply(null, arguments)
    }, e.dynCall_jijiiiiii = function () {
      return (e.dynCall_jijiiiiii = e.asm.Wc).apply(null, arguments)
    }, e.dynCall_jijj = function () {
      return (e.dynCall_jijj = e.asm.Xc).apply(null, arguments)
    }, e.dynCall_viji = function () {
      return (e.dynCall_viji = e.asm.Yc).apply(null, arguments)
    }, e.dynCall_iijijjji = function () {
      return (e.dynCall_iijijjji = e.asm.Zc).apply(null, arguments)
    }, e.dynCall_iiiji = function () {
      return (e.dynCall_iiiji = e.asm._c).apply(null, arguments)
    }, e.dynCall_iiiij = function () {
      return (e.dynCall_iiiij = e.asm.$c).apply(null, arguments)
    }, e.dynCall_iiji = function () {
      return (e.dynCall_iiji = e.asm.ad).apply(null, arguments)
    }, e.dynCall_jijij = function () {
      return (e.dynCall_jijij = e.asm.bd).apply(null, arguments)
    }, e.dynCall_iijijji = function () {
      return (e.dynCall_iijijji = e.asm.cd).apply(null, arguments)
    }, e.dynCall_jij = function () {
      return (e.dynCall_jij = e.asm.dd).apply(null, arguments)
    }, e.dynCall_jiji = function () {
      return (e.dynCall_jiji = e.asm.ed).apply(null, arguments)
    }, e.dynCall_viiijj = function () {
      return (e.dynCall_viiijj = e.asm.fd).apply(null, arguments)
    }, e.dynCall_iiij = function () {
      return (e.dynCall_iiij = e.asm.gd).apply(null, arguments)
    }, e.dynCall_iijii = function () {
      return (e.dynCall_iijii = e.asm.hd).apply(null, arguments)
    }, e.dynCall_iiijiji = function () {
      return (e.dynCall_iiijiji = e.asm.id).apply(null, arguments)
    }, e.dynCall_iiiiij = function () {
      return (e.dynCall_iiiiij = e.asm.jd).apply(null, arguments)
    }, e.dynCall_iiiiijj = function () {
      return (e.dynCall_iiiiijj = e.asm.kd).apply(null, arguments)
    }, e.dynCall_iiiiiijj = function () {
      return (e.dynCall_iiiiiijj = e.asm.ld).apply(null, arguments)
    }, e.dynCall_jjj = function () {
      return (e.dynCall_jjj = e.asm.md).apply(null, arguments)
    }, e.dynCall_iiiijj = function () {
      return (e.dynCall_iiiijj = e.asm.nd).apply(null, arguments)
    }, e.dynCall_viijj = function () {
      return (e.dynCall_viijj = e.asm.od).apply(null, arguments)
    }, e.dynCall_viiijjjj = function () {
      return (e.dynCall_viiijjjj = e.asm.pd).apply(null, arguments)
    }, e.addRunDependency = ee, e.removeRunDependency = te, e.FS_createPath = Pe.createPath, e.FS_createDataFile = Pe.createDataFile, e.FS_createPreloadedFile = Pe.createPreloadedFile, e.FS_createLazyFile = Pe.createLazyFile, e.FS_createDevice = Pe.createDevice, e.FS_unlink = Pe.unlink, e.addFunction = function (e, t) {
      return v(e, t)
    }, e.Browser = ir, e.FS = Pe, e.GL = dr, J = function e() {
      $r || Xr(), $r || (J = e)
    }, e.run = Xr, e.preInit)
    for ("function" == typeof e.preInit && (e.preInit = [e.preInit]); e.preInit.length > 0;) e.preInit.pop()();
  return Xr(), e.ready
});
async function O({
  clientToken: e,
  canvas: t = N(),
  locateFile: r = "",
  devicePixelRatio: n = (e => null != (e = window.devicePixelRatio) ? e : 1)(),
  maxFaces: o = 1,
  cameraOrientation: a = 0,
  enableMirroring: i = !0,
  ...u
}) {
  var s;
  "string" == typeof r && (r = function (e) {
    "" === e || e.endsWith("/") || (e += "/");
    return t => e + t
  }(r)), "object" == typeof r && (s = r, r = e => s[e]), r = await async function (e) {
    if (!(await (async () => WebAssembly.validate(new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0, 1, 5, 1, 96, 0, 1, 123, 3, 2, 1, 0, 10, 10, 1, 8, 0, 65, 0, 253, 15, 253, 98, 11])))())) return console.info("The platform does not support SIMD.", 'Using "BanubaSDK.wasm"'), e;
    console.info("The platform supports SIMD.", 'Going to use "BanubaSDK.simd.wasm"');
    const t = e("BanubaSDK.simd.wasm");
    if (!t) return console.warn('"BanubaSDK.simd.wasm" is missing in the "locateFile" option.', 'Using "BanubaSDK.wasm" as a fallback'), e;
    if (!(await fetch(t).then((e => e.ok)))) return console.warn(`Unable to fetch "BanubaSDK.simd.wasm" from the "${t}".`, 'Using "BanubaSDK.wasm" as a fallback'), e;
    return t => {
      const [r, n] = t.split(".");
      if ("wasm" !== n) return e(t);
      const o = [r, "simd", n].join(".");
      return e(o)
    }
  }(r);
  const c = await B({
    canvas: t,
    locateFile: r,
    ...u
  });
  if (c.createContext(c.canvas, !0, !0, {
      alpha: !1,
      antialias: !1,
      depth: !1,
      premultipliedAlpha: !1,
      preserveDrawingBuffer: !1,
      stencil: !1
    }), "undefined" == typeof WebGL2RenderingContext) {
    ["WEBGL_depth_texture", "ANGLE_depth_texture", "GL_ANGLE_depth_texture", "OES_texture_half_float"].every((e => {
      var t;
      return null == (t = c.canvas.getContext("webgl")) ? void 0 : t.getExtension(e)
    })) || console.error("Depth textures are not supported on the current device.")
  }
  try {
    const t = new c.banuba_sdk_manager(1, 1, n, a, i, e);
    return t.set_max_faces(o), t.FS = c.FS, t.PThread = !!c.PThread, t.canvas = c.canvas, t.FaceSearchingMode = c.face_search_mode, (l = c, function (e) {
        const t = 3,
          r = [],
          n = Object.getPrototypeOf(e),
          {
            draw: o
          } = n;
        return Object.defineProperty(n, "push_frame", {
          value(e, n, o) {
            if (r.length === t) return;
            const a = r[r.push(l._malloc(e.length)) - 1];
            return l.HEAP8.set(e, a), this.push_frame_ptr(a, n, o)
          }
        }), Object.defineProperty(n, "draw", {
          value() {
            const e = o.call(this);
            return -1 !== e && l._free(r.shift()), e
          }
        }), e
      })(t),
      function (e, t = {}) {
        var r, n;
        const o = null != (n = null != (r = t.devicePixelRatio) ? r : window.devicePixelRatio) ? n : 1;
        return function (t) {
          const r = Object.getPrototypeOf(t),
            n = r.surface_changed;
          Object.defineProperty(r, "surface_changed", {
            value(t, r) {
              n.call(this, t, r), e.setCanvasSize(t * o, r * o)
            }
          })
        }
      }(c, {
        devicePixelRatio: n
      })(t),
      function (e) {
        return function (t) {
          const r = Object.getPrototypeOf(t);
          return Object.defineProperty(r, "set_duration_recognizer_callback", {
            value(...t) {
              return this.set_duration_recognizer_callback_ptr(e.addFunction(...t, "vff"))
            }
          }), t
        }
      }(c)(t),
      function (e) {
        return function (t) {
          const r = Object.getPrototypeOf(t);
          return Object.entries(r).forEach((([t, n]) => {
            "function" == typeof n && n !== r.constructor && Object.defineProperty(r, t, {
              value(...t) {
                try {
                  return n.call(this, ...t)
                } catch (r) {
                  throw "number" == typeof r && (r = new Error(e.get_exception_message(r))), r
                }
              }
            })
          })), t
        }
      }(c)(t), t.set_effect_volume(0), t
  } catch (f) {
    throw "number" == typeof f && (f = new Error(c.get_exception_message(f))), f
  }
  var l
}

function N() {
  const e = document.createElement("canvas");
  return e.style.maxWidth = "100%", e.style.objectFit = "cover", e
}
const U = {},
  z = class extends I {
    constructor(e) {
      super(), this._state = "paused", this._frames = async function* () {}(), this._push_frame = (() => {
        let e, t;
        return ({
          data: r,
          width: n,
          height: o
        }) => {
          e === n && t === o || (e = n, t = o, this._sdk.set_effect_size(n, o), this._sdk.surface_changed(n, o)), this._sdk.push_frame(r, n, o)
        }
      })(), this._draw = () => {
        -1 !== this._sdk.draw() && this.dispatchEvent(new CustomEvent("framerendered"))
      }, this._process = (() => {
        const e = e => {
            this._push_frame(e), this._draw()
          },
          t = e => {
            this._draw(), this._push_frame(e)
          };
        return r => p((() => {
          try {
            (this._sdk.PThread ? t : e)(r)
          } catch (n) {
            this.clearEffect(), console.warn("The effect was force cleared due to the exception:"), console.error(n)
          }
        }))
      })(), this._sdk = e, this._sdk.set_duration_recognizer_callback(((e, t) => this.dispatchEvent(new CustomEvent("frameprocessed", {
        detail: {
          instantDuration: e,
          averagedDuration: t
        }
      }))))
    }
    get isPlaying() {
      return "playing" === this._state
    }
    static setDefaultOptions(e) {
      Object.assign(U, e)
    }
    static async create(e) {
      const t = {
          ...U,
          ...e
        },
        r = await O(t),
        n = new z(r);
      return "onFPSUpdate" in t && (console.warn("DeprecationWarning: Using `Player.create` with `onFPSUpdate` parameter will soon stop working.", "Use `player.addEventListener('frameprocessed')` instead"), n.addEventListener("frameprocessed", (({
        detail: e
      }) => t.onFPSUpdate(1 / e.averagedDuration, 1 / e.instantDuration)))), n
    }
    use(e, {
      fps: t = 30
    } = {}) {
      this._frames = async function* (e, t) {
        const r = 1e3 / t,
          n = .1;
        let o = Date.now(),
          a = 0;
        for (;;) {
          o = Date.now();
          let t = o - a;
          if (t < r - n) {
            await new Promise((e => p(e)));
            continue
          }
          a = o - t % r;
          const {
            value: i,
            done: u
          } = await e.next();
          if (u) return i;
          yield i
        }
      }(e[Symbol.asyncIterator](), t)
    }
    async applyEffect(e) {
      const t = await e._write(this._sdk.FS);
      this._sdk.load_effect(t)
    }
    clearEffect() {
      this._sdk.unload_effect()
    }
    callJsMethod(e, t = "") {
      this._sdk.call_js_method(e, t)
    }
    setVolume(e) {
      this._sdk.set_effect_volume(e), this.dispatchEvent(new CustomEvent("volumechange", {
        detail: e
      }))
    }
    async play() {
      if ("pausing" === this._state && (this._state = "playing"), "playing" !== this._state) {
        for (this._state = "playing";
          "playing" === this._state;) {
          const {
            value: e
          } = await this._frames.next();
          if (!e) break;
          this.dispatchEvent(new CustomEvent("framereceived")), this._process(e)
        }
        this._state = "paused"
      }
    }
    pause() {
      "playing" === this._state && (this._state = "pausing")
    }
  };
let $ = z;
$.FRAME_RECEIVED_EVENT = "framereceived", $.FRAME_PROCESSED_EVENT = "frameprocessed", $.FRAME_RENDERED_EVENT = "framerendered";
const W = new WeakMap,
  G = {
    render: (e, t) => {
      const r = "string" == typeof t ? document.querySelector(t) : t;
      if (!(r instanceof HTMLElement)) throw new Error("Target container is not a DOM element");
      W.set(r, e), r.appendChild(e._sdk.canvas), e.play()
    },
    unmount: e => {
      const t = "string" == typeof e ? document.querySelector(e) : e;
      if (!(t instanceof HTMLElement)) throw new Error("Target container is not a DOM element");
      const r = W.get(t);
      r && t.removeChild(r._sdk.canvas), W.delete(t)
    }
  };
class q {
  constructor(e) {
    this._player = e
  }
  async takePhoto(e) {
    this._player.isPlaying && await new Promise((e => this._player.addEventListener("framerendered", e, {
      once: !0
    })));
    const t = Q(this._player._sdk.canvas, null == e ? void 0 : e.width, null == e ? void 0 : e.height);
    return await new Promise(((r, n) => {
      var o;
      return t.toBlob((e => e ? r(e) : n(new Error("Unexpected error: Unable to create Blob"))), null != (o = null == e ? void 0 : e.type) ? o : "image/jpeg", null == e ? void 0 : e.quality)
    }))
  }
}
const Q = (e, t = e.width, r = e.height) => {
    if (t !== e.width || r !== e.height) {
      const n = Y(t, r);
      return n.getContext("2d").drawImage(e, 0, 0, n.width, n.height), n
    }
    return e
  },
  Y = (e, t) => {
    let r;
    return "undefined" == typeof OffscreenCanvas ? (r = document.createElement("canvas"), r.width = e, r.height = t) : (r = new OffscreenCanvas(e, t), r.toBlob = function (e, t, r) {
      this.convertToBlob({
        type: t,
        quality: r
      }).then(e).catch((t => e(null)))
    }), r
  },
  V = "undefined" != typeof MediaStream ? MediaStream : class {
    constructor() {
      throw new Error("The environment does not support MediaStream API")
    }
  },
  X = class extends V {
    constructor(e) {
      return super(), X.cache.has(e) || (e._sdk.canvas.captureStream().getTracks().forEach((e => this.addTrack(e))), X.cache.set(e, this)), X.cache.get(e)
    }
    getVideoTrack(e = 0) {
      return this.getVideoTracks()[e]
    }
    getAudioTrack(e = 0) {
      return this.getAudioTracks()[e]
    }
  };
let H = X;
H.cache = new WeakMap;
const K = "undefined" != typeof MediaRecorder ? MediaRecorder : class {
  constructor() {
    throw new Error("The environment does not support MediaRecorder API")
  }
};
class Z extends K {
  constructor(e, t) {
    super(e._sdk.canvas.captureStream(), t)
  }
  async stop() {
    return new Promise(((e, t) => {
      const r = t => {
          super.removeEventListener("dataavailable", r), super.removeEventListener("error", n), e(t.data)
        },
        n = e => {
          super.removeEventListener("dataavailable", r), super.removeEventListener("error", n), t(e.error)
        };
      super.addEventListener("dataavailable", r), super.addEventListener("error", n), super.stop()
    }))
  }
}
const J = "1.0.0-11-ga371eeab1";
export {
  G as Dom, M as Effect, y as Image, q as ImageCapture, w as MediaStream, H as MediaStreamCapture, $ as Player, J as VERSION, E as Video, Z as VideoRecorder, x as Webcam, U as defaultPlayerOptions, k as defaultVideoConstraints, _ as defaultVideoOptions
};