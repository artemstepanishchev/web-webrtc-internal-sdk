import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

export default defineConfig({
  optimizeDeps: {
    include: [
      "libsfu",
      "librct/src/conference",
    ],
  },
  build: {
    commonjsOptions: {
      exclude: ['libsfu/*'],
    },
  },
  plugins: [svelte()],
})
