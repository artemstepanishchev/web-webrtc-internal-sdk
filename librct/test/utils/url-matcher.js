module.exports = (test, matchCB, testBody = null) => (url, { body }) => {
    let result = false;

    if (typeof test === 'string') {
        result = url.indexOf(test) >= 0;
    } else {
        result = test.some(s => url.indexOf(s) >= 0);
    }

    if (result && testBody) {
        result = testBody === body;
    }

    if (result && typeof matchCB === 'function') {
        matchCB();
    }

    return result;
};
