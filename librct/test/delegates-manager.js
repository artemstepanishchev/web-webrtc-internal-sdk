const DelegatesManager = require('../src/delegates-manager');
const urlMatcher = require('./utils/url-matcher');
const fetchMock = require('fetch-mock');
const fetch = (...args) => global.fetch(...args);
const BASE_URL = 'http://baseurl.com';

const accountId = 'accountId';
const extensionId = 'extensionId';
const delegateExtId = 'delegateExtId';

const delegate1 = {
    id: 'delegate1',
    name: 'delegate1',
};

describe('DelegatesManager', () => {
    let delegatesManager;

    beforeEach(() => {
        delegatesManager = new DelegatesManager({
            baseUrl: BASE_URL,
            fetch,
        });

        fetchMock.restore();

        fetchMock.get(urlMatcher(`/accounts/${accountId}/extensions/${extensionId}/delegates`), {
            status: 200,
            headers: {
                'Content-Type': 'application/json',
            },
            body: {
                items: [delegate1],
            },
        });

        fetchMock.post(urlMatcher(`/accounts/${accountId}/extensions/${extensionId}/delegates`), {
            status: 200,
            headers: {
                'Content-Type': 'application/json',
            },
            body: delegate1,
        });

        fetchMock.delete(urlMatcher(`/accounts/${accountId}/extensions/${extensionId}`), {
            status: 204,
            headers: {
                'Content-Type': 'application/json',
            },
        });
    });

    // TODO broken on Win10, RCV-48178
    xit('should add delegate correctly', done => {
        delegatesManager.addDelegate({ accountId, extensionId, delegateExtId }).then(() => done());
    });
});
