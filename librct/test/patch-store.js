var assert = require('assert');
var PatchStore = require('../src/patch-store');

describe('PatchStore', () => {
    var patchStore;
    var keepAlive = 10;

    beforeEach('create new Patch Store', () => {
        patchStore = new PatchStore({ keepAlive });
    });

    it('should add data', done => {
        const params1 = {
            a: 1,
            b: 2,
        };
        const params2 = {
            c: 3,
        };
        const result = {
            a: 1,
            b: 2,
            c: 3,
        };

        patchStore.add(params1);
        patchStore.add(params2);
        assert.deepStrictEqual(patchStore.getData(), result);
        done();
    });

    it('should remove expired data', done => {
        const params1 = {
            a: 1,
        };
        const params2 = {
            b: 2,
        };
        const result = {};

        patchStore.add(params1);
        patchStore.add(params2);
        setTimeout(() => {
            assert.deepStrictEqual(patchStore.getData(), result);
            done();
        }, keepAlive);
    });

    it('should expire data only in "keepAlive" ms', done => {
        const params1 = {
            a: 1,
        };
        const params2 = {
            b: 2,
        };
        const result = {
            a: 1,
            b: 2,
        };

        setTimeout(() => {
            assert.deepStrictEqual(patchStore.getData(), result);
            done();
        }, keepAlive - 1);
        patchStore.add(params1);
        patchStore.add(params2);
    });

    it('should remove only expired data', done => {
        const params1 = {
            a: 1,
        };
        const params2 = {
            b: 2,
        };
        const result = {
            b: 2,
        };

        patchStore.add(params1);
        setTimeout(() => {
            patchStore.add(params2);
            setTimeout(() => {
                assert.deepStrictEqual(patchStore.getData(), result);
                done();
            }, keepAlive / 2 + 1);
        }, keepAlive / 2);
    });

    it('should use patches in insertion order', done => {
        const params1 = {
            a: 1,
            b: 1,
        };
        const params2 = {
            a: 2,
        };
        const params3 = {
            b: 2,
        };
        const result = {
            a: 2,
            b: 2,
        };

        patchStore.add(params1);
        patchStore.add(params2);
        patchStore.add(params3);
        assert.deepStrictEqual(patchStore.getData(), result);
        done();
    });

    it('should always emit event when data added', done => {
        const params1 = {
            a: 1,
            b: 1,
        };
        const params2 = {
            a: 2,
        };
        const params3 = {
            b: 2,
        };
        const result = 3;
        let events = 0;

        patchStore.on(PatchStore.EVENT.DATA_ADDED, () => {
            events++;
        });

        patchStore.add(params1);
        patchStore.add(params2);
        patchStore.add(params3);
        assert.strictEqual(events, result);
        done();
    });

    it('should emit event when data removed only if final patch changed', done => {
        const params1 = {
            a: 1,
            b: 1,
        };
        const params2 = {
            a: 2,
        };
        const params3 = {
            b: 2,
        };
        // removing "params1" will not change final patch, because "a" and "b" overwritten by "params2" and "params3"
        const result = 2;
        let events = 0;

        patchStore.on(PatchStore.EVENT.DATA_REMOVED, () => {
            events++;
        });

        patchStore.add(params1);
        patchStore.add(params2);
        patchStore.add(params3);
        setTimeout(() => {
            assert.strictEqual(events, result);
            done();
        }, keepAlive);
    });
});
