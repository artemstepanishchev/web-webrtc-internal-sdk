const assert = require('assert');
const cloneDeep = require('lodash.clonedeep');
const omit = require('lodash.omit');
const { createReducer, types } = require('../src/reducer');
const { createStore } = require('../src/store');
const { getRawCall } = require('./mock/call');

describe('Reducer', () => {
    const initialCallState = {
        meeting: null,
        participants: null,
        streams: null,
        sessions: null,
        localParticipant: null,
        localSession: null,
        localStreams: null,
        myDemands: null,
        targetedToMeDemands: null,
        recordings: null,
        notifications: null,
        myDials: null,
        channels: null,
        meetingParticipants: null,
        notifyChannels: null,
        localSessionHistory: {},
    };
    let store;
    let rawCall;

    const RAW_CALL = getRawCall();

    beforeEach(() => {
        store = createStore(createReducer());
        rawCall = getRawCall();
    });

    describe('base', () => {
        it('should have correct initial state', () => {
            store.dispatch({ type: 'SOME_ACTION' });
            const state = store.getState();
            assert.deepStrictEqual(state.localIds, {}, 'wrong localIds');
            assert.deepStrictEqual(state.recalculatedEntities, {}, 'wrong recalculator state');
            assert.deepStrictEqual(state.call, initialCallState, 'wrong initial call state');
        });

        it('should pass updated state to the store subscribers', () => {
            const states = [];
            store.subscribe(state => states.push(state));

            store.dispatch({ type: 'TEST' });
            store.dispatch({ type: types.CHANGE, data: getRawCall() });

            assert.deepStrictEqual(states[0].call, initialCallState);
            assert.deepStrictEqual(states[0].recalculatedEntities, {});
            assert.notStrictEqual(states[1].call.meeting, null);
            assert.notStrictEqual(states[1].call.localParticipant, null);
            assert.notStrictEqual(states[1].call.localSession, null);
            assert.notStrictEqual(states[1].call.localStreams, null);
            assert(Object.keys(states[1].recalculatedEntities).length > 0);
        });

        it('should keep last state returned from reducer', () => {
            store.dispatch({ type: types.CHANGE, data: rawCall });
            const state = store.getState();
            assert.strictEqual(state, store.getState());
        });

        it('should change state on CHANGE event', () => {
            const data = {
                allowDials: true,
            };

            store.dispatch({ type: types.CHANGE, data });
            const state = store.getState();

            assert.deepStrictEqual(state.call, {
                ...initialCallState,
                meeting: { allowDials: true },
            });
        });

        it('should reset state on HARD_RESET event, but save localSessionHistory', () => {
            store.dispatch({ type: types.CHANGE, data: getRawCall() });
            store.dispatch({ type: types.HARD_RESET });
            const state = store.getState();

            assert.deepStrictEqual(state.localIds, {}, 'wrong localIds');
            assert.deepStrictEqual(state.recalculatedEntities, {}, 'wrong recalculator state');

            assert('local_session_id' in state.call.localSessionHistory);
            // check the rest props have been reset
            const changedProps = ['localSessionHistory'];
            assert.deepStrictEqual(
                omit(state.call, changedProps),
                omit(initialCallState, changedProps)
            );
        });

        it('should reset state on SOFT_RESET event but save important call props', () => {
            store.dispatch({ type: types.CHANGE, data: rawCall });
            store.dispatch({ type: types.SOFT_RESET });
            const { call } = store.getState();

            assert.deepStrictEqual(
                call.meeting,
                {
                    meetingPassword: RAW_CALL.meetingPassword,
                    meetingPasswordMasked: RAW_CALL.meetingPasswordMasked,
                    meetingPasswordPSTN: RAW_CALL.meetingPasswordPSTN,
                },
                'failed to save meeting data'
            );
            assert.deepStrictEqual(
                call.localParticipant,
                { shortPrtsPin: RAW_CALL.participants[0].shortPrtsPin },
                'failed to save shortPrtsPin'
            );
            assert(
                !isNaN(new Date(call.localSessionHistory['local_session_id'])),
                'wrong local session save date'
            );

            const rawSession = RAW_CALL.participants[0].sessions[0];
            assert.deepStrictEqual(call.localSession, {
                notificationSubKey: rawSession.notificationSubKey,
                notificationToken: rawSession.notificationToken,
                pingInfo: rawSession.pingInfo,
                token: rawSession.token,
            });

            // check the rest props have been reset
            const changedProps = [
                'localSessionHistory',
                'meeting',
                'localParticipant',
                'localSession',
            ];
            assert.deepStrictEqual(omit(call, changedProps), omit(initialCallState, changedProps));
        });

        it('should reset state on SOFT_RESET event but save local ids', () => {
            store.dispatch({ type: types.CHANGE, data: rawCall });
            store.dispatch({ type: types.SOFT_RESET });
            const state = store.getState();

            const participant = RAW_CALL.participants[0];

            assert.deepStrictEqual(state.localIds, {
                localParticipantId: participant.id,
                localSessionId: participant.sessions[0].id,
                localStreamIds: participant.streams.map(s => s.id),
            });
        });
    });

    describe('reducer + recalculator', () => {
        it('should detect and recalculate meeting', () => {
            store.dispatch({ type: types.CHANGE, data: rawCall });
            const { recalculatedEntities } = store.getState();
            assert.deepStrictEqual(recalculatedEntities.meeting, {
                id: 'meeting_id',
                allowDials: true,
                allowAnnotations: true,
                allowChats: true,
                allowJoin: true,
                allowScreenSharing: true,
                allowUnmute: true,
                allowUnmuteVideo: true,
                bridgeId: 'bridge_id',
                closedCaptionsEnabled: true,
                cloudRecordingsEnabled: true,
                startTime: '2021-02-12T09:37:25.857Z',
                meetingPassword: '111',
                meetingPasswordMasked: '698d51a19d8a121ce581499d7b701668',
                meetingPasswordPSTN: '111',
                mute: false,
                muteVideo: false,
                // allowInternationalDials: true,
                // musicEnabled: true,
            });
        });

        it('should detect an recalculate local participants, streams and session', () => {
            store.dispatch({ type: types.CHANGE, data: rawCall });
            const { recalculatedEntities: result } = store.getState();

            assert.deepStrictEqual(result.participants, undefined);
            assert.deepStrictEqual(result.streams, undefined);

            assert.strictEqual(result.localParticipant.id, 'local_p_id');
            assert.strictEqual(result.localParticipant.serverMute, false);

            const rawSession = cloneDeep(RAW_CALL.participants[0].sessions[0]);
            delete rawSession.justCreated;
            delete rawSession.locale;
            rawSession.participantId = 'local_p_id';

            assert.deepStrictEqual(result.localSession, rawSession);
            assert.deepStrictEqual(result.localStreams, [
                {
                    id: 'local_stream_id',
                    type: 'video/main',
                    isSessionInactive: false,
                    participantId: 'local_p_id',
                },
            ]);
        });
    });
});
