const assert = require('assert');
const cloneDeep = require('lodash.clonedeep');
const { smartMerge, isDataPatched } = require('../src/utils');

const callId = 'callId';
const participantId = 'participantId';
const streamId = 'streamId';
const displayName = 'displayName';

describe('Utils', () => {
    describe('smartMerge', () => {
        it('should merge 2 objects', done => {
            let obj1 = {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            };

            let obj2 = {
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                                muted: true,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                ],
            };

            assert(smartMerge(obj1, obj2), 'smartMerge should return true');
            assert.deepStrictEqual(obj1, {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                                muted: true,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            });

            done();
        });

        it('should add new object to array', done => {
            let obj1 = {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            };

            let obj2 = {
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}3`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                ],
            };

            assert(smartMerge(obj1, obj2), 'smartMerge should return true');
            assert.deepStrictEqual(obj1, {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                            },
                            {
                                id: `${streamId}3`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            });

            done();
        });

        it('should apply multiple changes', done => {
            let obj1 = {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            };

            let obj2 = {
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                                muted: true,
                            },
                            {
                                id: `${streamId}3`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                            {
                                id: `${streamId}4`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            };

            assert(smartMerge(obj1, obj2), 'smartMerge should return true');
            assert.deepStrictEqual(obj1, {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                                muted: true,
                            },
                            {
                                id: `${streamId}3`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                            {
                                id: `${streamId}4`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            });

            done();
        });

        it('should accept custom id key', done => {
            let obj1 = {
                myId: callId,
                participants: [
                    {
                        myId: `${participantId}1`,
                        streams: [
                            {
                                myId: `${streamId}1`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        myId: `${participantId}2`,
                        streams: [
                            {
                                myId: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            };

            let obj2 = {
                participants: [
                    {
                        myId: `${participantId}1`,
                        streams: [
                            {
                                myId: `${streamId}1`,
                                muted: true,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                ],
            };

            assert(smartMerge(obj1, obj2, 'myId'), 'smartMerge should return true');
            assert.deepStrictEqual(obj1, {
                myId: callId,
                participants: [
                    {
                        myId: `${participantId}1`,
                        streams: [
                            {
                                myId: `${streamId}1`,
                                muted: true,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        myId: `${participantId}2`,
                        streams: [
                            {
                                myId: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            });

            done();
        });

        it('should replace arrays of scalar types', done => {
            [
                {
                    src: { arr: [1, 2, 3] },
                    dst: { arr: [4, 5, 6] },
                    diff: true,
                },
                {
                    src: { arr: [1, 2, 3] },
                    dst: { arr: [1, 2, 3] },
                    diff: false,
                },
                {
                    src: { arr: ['1', '2', '3'] },
                    dst: { arr: ['4', '5', '6'] },
                    diff: true,
                },
                {
                    src: { arr: ['1', '2', '3'] },
                    dst: { arr: ['1', '2', '3'] },
                    diff: false,
                },
            ].forEach(({ src, dst, diff }) => {
                const dstCopy = cloneDeep(dst);
                assert.strictEqual(smartMerge(dstCopy, src), diff);
                assert.deepStrictEqual(dstCopy, src);
            });

            done();
        });
    });

    describe('isDataPatched', () => {
        it('should patch data', done => {
            let obj1 = {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            };

            let obj2 = {
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                                muted: true,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                ],
            };

            assert(!isDataPatched(obj1, obj2));

            done();
        });

        it('should not patch data', done => {
            let obj1 = {
                id: callId,
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                                muted: true,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                    {
                        id: `${participantId}2`,
                        streams: [
                            {
                                id: `${streamId}2`,
                            },
                        ],
                        displayName: `${displayName}2`,
                    },
                ],
            };

            let obj2 = {
                participants: [
                    {
                        id: `${participantId}1`,
                        streams: [
                            {
                                id: `${streamId}1`,
                                muted: true,
                            },
                        ],
                        displayName: `${displayName}1`,
                    },
                ],
            };

            assert(isDataPatched(obj1, obj2));

            done();
        });
    });
});
