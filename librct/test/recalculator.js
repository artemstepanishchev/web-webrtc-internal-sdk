/* eslint-disable no-undefined */
const assert = require('assert');
const { createRecalculator } = require('../src/recalculator');
const { streamType } = require('../src/types');

describe('Calculator', () => {
    let recalculator = createRecalculator();

    beforeEach(() => {
        recalculator = createRecalculator();
    });
    it('should get and set data', done => {
        recalculator.recalculate({
            meeting: {
                id: 1,
                allowDials: true,
            },
            notifications: [],
        });

        assert.deepStrictEqual(recalculator.result.meeting, { id: 1, allowDials: true });
        assert.deepStrictEqual(recalculator.result.notifications, []);

        done();
    });

    it('should return the same object if recalculated with the same source', done => {
        const srcMeeting = {
            id: 1,
            allowDials: true,
        };
        recalculator.recalculate({
            meeting: srcMeeting,
        });
        const meeting1 = recalculator.result.meeting;
        recalculator.recalculate({
            meeting: srcMeeting,
        });
        const meeting2 = recalculator.result.meeting;

        assert.strictEqual(meeting1, meeting2, 'should be the same object');

        done();
    });

    it('should return different object for different source', done => {
        const srcMeeting1 = {
            id: 1,
            allowDials: true,
        };
        const srcMeeting2 = {
            id: 1,
            allowDials: true,
        };
        recalculator.recalculate({
            meeting: srcMeeting1,
        });
        const meeting1 = recalculator.result.meeting;
        recalculator.recalculate({
            meeting: srcMeeting2,
        });
        const meeting2 = recalculator.result.meeting;

        assert.notStrictEqual(meeting1, meeting2, 'should not be the same object');

        done();
    });

    it('should filter fields not from models', () => {
        recalculator.recalculate({
            meeting: {
                id: 1,
                shouldBeFiltred: true,
            },
            localParticipant: {
                id: 'p1',
                alsoShouldBeFiltred: true,
            },
            localStreams: {
                s1: {
                    id: 's1',
                    justCreated: true,
                    type: streamType.SELF_VIDEO,
                    participantId: 'p1',
                    filterThis: 'please',
                },
            },
            localSession: {
                id: 'session1Id',
                shouldBeFiltred: true,
                participantId: 'p1',
            },
        });

        const { meeting, localParticipant, localStreams, localSession } = recalculator.result;
        assert.strictEqual(meeting.shouldBeFiltred, undefined);
        assert.strictEqual(localParticipant.alsoShouldBeFiltred, undefined);
        assert.strictEqual(localStreams[0].filterThis, undefined);
        assert.strictEqual(localSession.shouldBeFiltred, undefined);
    });

    describe('multiple dependencies', () => {
        const meeting = {
            id: 'mId',
        };
        const participant = {
            id: 'p1Id',
        };

        const session = {
            id: 'session1Id',
            participantId: 'p1Id',
            localMute: true,
        };

        const modifiedSession = {
            id: 'session1Id',
            participantId: 'p1Id',
            localMute: false,
        };

        const stream = {
            id: 'stream1Id',
            type: 'video/main',
            sessionId: 'session1Id',
            participantId: 'p1Id',
            audio: {
                isActiveIn: true,
                isActiveOut: true,
            },
        };

        it('should recalculate participant if participant session was changed', done => {
            const participants = {
                p1Id: participant,
            };

            recalculator.recalculate({
                meeting,
                participants,
                sessions: {
                    session1Id: session,
                },
                streams: {
                    stream1Id: stream,
                },
            });

            const calcedParticipants1 = recalculator.result.participants.p1Id;

            recalculator.recalculate({
                meeting,
                participants,
                sessions: {
                    session1Id: modifiedSession,
                },
                streams: {
                    stream1: stream,
                },
            });

            const calcedParticipants2 = recalculator.result.participants.p1Id;

            assert.notStrictEqual(
                calcedParticipants1,
                calcedParticipants2,
                'should not be same object'
            );
            assert.strictEqual(
                calcedParticipants1.localMute,
                true,
                'local mute calculated incorrectly for calcedParticipants1'
            );
            assert.strictEqual(
                calcedParticipants2.localMute,
                false,
                'local mute calculated incorrectly for calcedParticipants2'
            );

            done();
        });

        it('should take localMute for local participant from local session', done => {
            const participants = {
                p1Id: participant,
            };

            const streams = {
                stream1Id: {
                    id: 'stream1Id',
                    type: 'video/main',
                    participantId: 'p1Id',
                    audio: { isActiveOut: true },
                },
            };

            const sessions = {
                session1Id: session,
            };

            const localParticipant = {
                id: 'localParticipant',
            };

            const localSession = {
                id: 'localSession',
                localMute: false,
            };

            const localSessionMuted = {
                id: 'localSession',
                localMute: true,
            };

            const localStreams = [{ id: 'localStream', type: 'video/main' }];

            recalculator.recalculate({
                meeting,
                participants,
                sessions,
                streams,
                localParticipant,
                localSession,
                localStreams,
            });

            assert.strictEqual(
                recalculator.result.localParticipant.localMute,
                false,
                'should be unmuted'
            );
            const calcedMeeting = recalculator.result.meeting;
            const calcedParticipants = recalculator.result.participants;
            const calcedLocalSessions = recalculator.result.localSession;
            const calcedStreams = recalculator.result.streams;
            const calcedLocalStreams = recalculator.result.localStreams;

            recalculator.recalculate({
                meeting,
                participants,
                sessions,
                streams,
                localParticipant,
                localSession: localSessionMuted,
                localStreams,
            });

            assert.strictEqual(
                recalculator.result.localParticipant.localMute,
                true,
                'should be muted'
            );
            assert.strictEqual(
                calcedMeeting,
                recalculator.result.meeting,
                'meeting object should not be changed'
            );
            assert.strictEqual(
                calcedParticipants,
                recalculator.result.participants,
                'participants object should not be changed'
            );
            assert.notStrictEqual(
                calcedLocalSessions,
                recalculator.result.localSession,
                'localSession object should be changed'
            );
            assert.strictEqual(
                calcedStreams,
                recalculator.result.streams,
                'streams object should not be changed'
            );
            assert.strictEqual(
                calcedLocalStreams,
                recalculator.result.localStreams,
                'localStreams object should not be changed'
            );
            assert.notStrictEqual(calcedMeeting, undefined, 'Meeting should be defined');
            assert.notStrictEqual(
                calcedParticipants,
                undefined,
                'PartcalcedParticipants should be defined'
            );
            assert.notStrictEqual(calcedStreams, undefined, 'Streams should be defined');
            assert.notStrictEqual(calcedLocalStreams, undefined, 'LocalStreams should be defined');
            assert.notStrictEqual(
                calcedLocalSessions,
                undefined,
                'LocalSessions should be defined'
            );

            done();
        });
    });
});
