const assert = require('assert');
const Channel = require('../src/channel');

const CHANNEL = 'demandId';
const BRIDGE = 'bridgeId';
const CALL = 'callId';
const PARTICIPANT = 'participantId';
const SESSION = 'sessionId';
const BASE_URL = 'http://baseurl.com';

const DEMAND = 'demandId';

const NEW = 'new';
const API_VERSION = 'v1';

describe('Channel', () => {
    let channel;

    beforeEach(() => {
        channel = new Channel(
            {
                id: CHANNEL,
                bridgeId: BRIDGE,
                callId: CALL,
                participantId: PARTICIPANT,
                sessionId: SESSION,
                status: NEW,
                info: {
                    demandId: DEMAND,
                },
            },
            {
                baseUrl: BASE_URL,
                apiVersion: API_VERSION,
            }
        );
    });

    it('should return correct url with id', done => {
        const url = channel.url();

        assert.strictEqual(
            url,
            `${BASE_URL}` +
                `/${API_VERSION}` +
                `/bridges/${BRIDGE}` +
                `/meetings/${CALL}` +
                `/participants/${PARTICIPANT}` +
                `/channels/${CHANNEL}`,
            'wrong url'
        );
        done();
    });

    it('should return correct url without id', done => {
        channel = new Channel(
            {
                bridgeId: BRIDGE,
                callId: CALL,
                participantId: PARTICIPANT,
                sessionId: SESSION,
            },
            {
                baseUrl: BASE_URL,
                apiVersion: API_VERSION,
            }
        );

        const url = channel.url();

        assert.strictEqual(
            url,
            `${BASE_URL}` +
                `/${API_VERSION}` +
                `/bridges/${BRIDGE}` +
                `/meetings/${CALL}` +
                `/participants/${PARTICIPANT}` +
                '/channels',
            'wrong url'
        );
        done();
    });
});
