const getRawCall = () => ({
    id: 'meeting_id',
    allowAnnotations: true,
    allowChats: true,
    allowDials: true,
    allowInternationalDials: true,
    allowJoin: true,
    allowScreenSharing: true,
    allowUnmute: true,
    allowUnmuteVideo: true,
    bridgeId: 'bridge_id',
    channelInfo: {
        channel:
            '/localapi/cop/v1/public-api/bridge/rnd01-t11-ndb0160227c9c02fa4b744883229d/call/rnd01-t11-ndb0160227c9c02fa4b744883229d_1613122645508!vi11-02-p11@vi11-02/d/a',
        pingInterval: 60000,
        version: 1,
    },
    closedCaptionsEnabled: true,
    cloudRecordingsEnabled: true,
    hostPreferences: {
        viralFreePhoenixPromotion: true,
    },
    isOnlyAuthUserJoin: false,
    isOnlyCoworkersJoin: false,
    meetingPassword: '111',
    meetingPasswordMasked: '698d51a19d8a121ce581499d7b701668',
    meetingPasswordPSTN: '111',
    musicEnabled: true,
    mute: false,
    muteVideo: false,
    participants: [
        {
            id: 'local_p_id',
            serverMute: false,
            sessions: [
                {
                    id: 'local_session_id',
                    justCreated: true,
                    localMute: false,
                    localMuteVideo: true,
                    locale: 'en-us',
                    notificationSubKey: 'sub-c-c3f0b5f8-8ca7-11e9-8277-da7aa9a31542',
                    notificationToken:
                        '8307eec9-fa7e-45b2-b60b-ab4cabf21f93-aa20f7f7-a4ff-4afe-a619-b78182c22492-f011c7ed-68a6-4e60-bed5-6f5b108c5747',
                    operatingSystem: 'macos',
                    pingInfo: { pingInterval: 27272 },
                    serverMute: false,
                    serverMuteVideo: false,
                    token:
                        '8307eec9-fa7e-45b2-b60b-ab4cabf21f93-aa20f7f7-a4ff-4afe-a619-b78182c22492-f011c7ed-68a6-4e60-bed5-6f5b108c5747',
                    userAgent: 'rcv/web/0.10',
                },
            ],
            streams: [{ id: 'local_stream_id', justCreated: true, type: 'video/main' }],
            shortPrtsPin: 67,
            allowUnmute: true,
            allowUnmuteVideo: true,
            avatarUrl:
                '/rcvideo/v1/intapi/uas/profile-image?accountId=735285039&avatarUrl=&brandId=1210&displayName=David%20Djunaedi&extensionId=735302039&platformId=rnd-vi3-ams&type=regular',
            callerInfo: {
                accountId: '735285039',
                extensionId: '735302039',
            },
            channels: [],
            demands: [],
            dials: [],
            displayName: 'David Djunaedi',
            host: true,
            joinTime: '2021-02-12T09:37:25.743Z',
            justCreated: true,
            moderator: true,
            noanswerTime: null,
            ringing: false,
            serverMuteVideo: false,
            waitingRoomStatus: '1',
            roles: [],
        },
    ],
    startTime: '2021-02-12T09:37:25.857Z',
    time: '2021-02-12T09:37:25.926Z',
    tonesOnEnter: false,
    tonesOnExit: false,
    type: 0,
    version: 1,
    waitingRoomMode: 0,
});

const getJoinAudioPatch = () => ({
    participants: [
        {
            id: 'local_p_id',
            streams: [
                {
                    id: 'local_stream_id',
                    audio: { isActiveIn: true, isActiveOut: true },
                },
            ],
        },
    ],
});

const newParticpantPatch = ({ participantId = 'local_p_id' } = {}) => ({
    bridgeId: 'bridge_id',
    participants: [
        {
            id: participantId,
            displayName: `${participantId} name`,
            allowUnmute: true,
            allowUnmuteVideo: true,
            host: false,
            waitingRoomStatus: '1',
            roomId: 'main',
            serverMute: false,
            serverMuteVideo: false,
            roles: [],
        },
    ],
    id: 'meeting_id',
    channelInfo: {
        version: 8,
    },
    version: 8,
    time: '2021-06-24T17:29:29.459Z',
});

const getSessionPatch = ({
    audioMute = true,
    videoMute = true,
    participantId = 'local_p_id',
    sessionId = 'local_session_id',
} = {}) => ({
    bridgeId: 'bridge_id',
    participants: [
        {
            id: participantId,
            sessions: [
                {
                    id: sessionId,
                    localMute: audioMute,
                    localMuteVideo: videoMute,
                    serverMute: false,
                    serverMuteVideo: false,
                },
            ],
            roles: [],
        },
    ],
    id: 'meeting_id',
    channelInfo: {
        version: 9,
    },
    version: 9,
    time: '2021-06-24T17:29:29.490Z',
});

const newStreamPatch = ({
    participantId = 'local_p_id',
    streamId = 'local_stream_id',
    sessionId = 'local_session_id',
} = {}) => ({
    bridgeId: 'bridge_id',
    participants: [
        {
            id: participantId,
            streams: [
                {
                    id: streamId,
                    type: 'video/main',
                    sessionId: sessionId,
                    audio: {
                        isActiveIn: false,
                        isActiveOut: false,
                    },
                    video: {
                        isActiveIn: true,
                        isActiveOut: true,
                    },
                },
            ],
            roles: [],
        },
    ],
    id: 'meeting_id',
    channelInfo: {
        version: 10,
    },
    version: 10,
    time: '2021-06-24T17:29:29.517Z',
});

module.exports = {
    getRawCall,
    getJoinAudioPatch,
    newParticpantPatch,
    getSessionPatch,
    newStreamPatch,
};
