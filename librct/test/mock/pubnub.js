class PubNub {
    constructor() {
        this._listeners = {};
        this._subsctibeError = false;
        this.subscribed = false;
        this._config = {};
    }

    addListener(listeners) {
        const keys = Object.keys(listeners);

        keys.forEach(k => {
            if (this._listeners.hasOwnProperty(k)) {
                this._listeners[k].push(listeners[k]);
            } else {
                this._listeners[k] = [listeners[k]];
            }
        });
    }

    subscribe() {
        setTimeout(() => {
            this.emit('status', {
                category: 'PNConnectedCategory',
                error: this._subsctibeError,
            });
            this.subscribed = !this._subsctibeError;
        }, PubNub.SUBSCRIBE_DELAY);
    }

    unsubscribe() {
        this.subscribed = false;
        return true;
    }

    setAuthKey(val) {
        this._authKey = val;
    }

    setHeartbeatInterval() {
        return true;
    }

    stop() {
        this.unsubscribe();
    }

    // Methods for tests

    emit(name, data) {
        if (!this._listeners.hasOwnProperty(name)) return;

        this._listeners[name].forEach(func => {
            func(data);
        });
    }

    setSubscribeError(val) {
        this._subsctibeError = val;
    }

    getAuthKey() {
        return this._authKey;
    }
}

PubNub.SUBSCRIBE_DELAY = 10;
PubNub.CATEGORIES = {
    PNConnectedCategory: 'PNConnectedCategory',
    PNReconnectedCategory: 'PNReconnectedCategory',
    PNAccessDeniedCategory: 'PNAccessDeniedCategory',
    PNBadRequestCategory: 'PNBadRequestCategory',
    PNNetworkDownCategory: 'PNNetworkDownCategory',
    PNNetworkIssuesCategory: 'PNNetworkIssuesCategory',
    PNRequestMessageCountExceededCategory: 'PNRequestMessageCountExceededCategory',
    PNTimeoutCategory: 'PNTimeoutCategory',
    PNUnknownCategory: 'PNUnknownCategory',
};

module.exports = PubNub;
