const getMockLogger = () => ({
    debug: () => {},
    info: () => {},
    warn: () => {},
    error: () => {},
});

module.exports = {
    getMockLogger,
};
