const assert = require('assert');
const CancelablePromise = require('../src/cancelablePromise');

describe('CancelablePromise', () => {
    it('should execute executor', done => {
        new Promise(() => {
            done();
        });
    });

    it('should execute executor sync', done => {
        try {
            let flag = false;

            new CancelablePromise(() => {
                flag = true;
            });

            assert(flag, 'should execute executor sync');
            done();
        } catch (e) {
            done(e);
        }
    });

    it('should execute success executor async', done => {
        try {
            let flag = false;

            new CancelablePromise(resolve => {
                resolve();
            }).then(() => {
                flag = true;
            });

            assert(!flag, 'should execute executor async');
            done();
        } catch (e) {
            done(e);
        }
    });

    it('should execute error callback async', done => {
        try {
            let flag = false;

            new CancelablePromise((resolve, reject) => {
                reject();
            }).catch(() => {
                flag = true;
            });

            assert(!flag, 'should execute callback async');
            done();
        } catch (e) {
            done(e);
        }
    });

    it('should change state once', done => {
        try {
            const obj = {};
            new CancelablePromise((resolve, reject) => {
                resolve(obj);
                reject();
            }).then(
                passedObj => {
                    assert(obj === passedObj, 'should pass same object to success callback');
                    done();
                },
                () => {
                    done(new Error("shouldn't change state twice"));
                }
            );
        } catch (e) {
            done(e);
        }
    });

    it('should handle error in executor', done => {
        try {
            const err = new Error('Some error');
            new CancelablePromise(() => {
                throw err;
            })
                .then(() => {
                    done(new Error("shouldn't call success callback"));
                })
                .catch(e => {
                    assert(e === err, 'should pass same object to error callback');
                    done();
                });
        } catch (e) {
            done(e);
        }
    });

    it('should create resolved promise via CancelablePromise.resolve static method', done => {
        try {
            const obj = {};

            const p = CancelablePromise.resolve(obj);
            p.then(passedObj => {
                assert(
                    obj === passedObj,
                    'should pass same object to success callback in CancelablePromise.resolve'
                );
                done();
            }).catch(done);
        } catch (e) {
            done(e);
        }
    });

    it('should create rejected promise via CancelablePromise.reject static method', done => {
        try {
            const err = new Error('SOme Error');

            const p = CancelablePromise.reject(err);
            p.catch(e => {
                assert(
                    err === e,
                    'should pass same object to error callback in CancelablePromise.reject'
                );
                done();
            }).catch(done);
        } catch (e) {
            done(e);
        }
    });

    it('should have Promise.all', done => {
        try {
            let x = 0;
            CancelablePromise.all([
                Promise.resolve().then(() => x++),
                Promise.resolve().then(() => x++),
            ])
                .then(z => {
                    assert.strictEqual(x, 2, 'should execte both functions');
                    done();
                })
                .catch(done);
        } catch (e) {
            done(e);
        }
    });

    it('should resolve race if 1st is success', done => {
        CancelablePromise.race([Promise.resolve(), new Promise(() => {})])
            .then(() => done())
            .catch(done);
    });

    it('should reject race if 1st is error', done => {
        CancelablePromise.race([Promise.reject(), new Promise(() => {})])
            .then(() => {
                done(new Error("Shouldn't call success callback"));
            })
            .catch(() => done());
    });

    it('should return CancelablePromise in then method', done => {
        try {
            const p = CancelablePromise.resolve().then(() => {});
            const p2 = CancelablePromise.resolve().catch(() => {});

            assert(
                p instanceof CancelablePromise,
                'then method result should be instance of CancelablePromise'
            );
            assert(
                p2 instanceof CancelablePromise,
                'catch method result should be instance of CancelablePromise'
            );

            done();
        } catch (e) {
            done(e);
        }
    });

    describe('Cancelation', () => {
        let cancel = null;
        let cancelPromise = null;

        beforeEach(() => {
            cancelPromise = new Promise((resolve, reject) => {
                cancel = reject;
            });
        });

        it('should accept additional promise parameter to cancel', done => {
            try {
                const p = new CancelablePromise(() => {}, cancelPromise);

                p.then(
                    () => {
                        done(new Error("shouldn't execute promise chain after cancel"));
                    },
                    () => {
                        done(new Error("shouldn't execute promise chain after cancel"));
                    }
                ).catch(done);
                cancel();
                done();
            } catch (e) {
                done(e);
            }
        });

        it('should work with all promise chain', done => {
            try {
                let flag = false;
                CancelablePromise.resolve(null, cancelPromise)
                    .then(() => {
                        flag = true;
                    })
                    .then(
                        () =>
                            new Promise(resolve => {
                                setTimeout(() => {
                                    resolve();
                                }, 1000);
                            })
                    )
                    .then(
                        () => {
                            done(new Error("shouldn't execute promise chain after cancel"));
                        },
                        () => {
                            done(new Error("shouldn't execute promise chain after cancel"));
                        }
                    )
                    .catch(done);

                setTimeout(() => {
                    cancel();
                    assert(flag, 'should execute first promise');
                    done();
                }, 25);
            } catch (e) {
                done(e);
            }
        });
    });
});
