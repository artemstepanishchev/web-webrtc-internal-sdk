const assert = require('assert');
const { EventBus } = require('../../src/event-bus');
const sinon = require('sinon');
const { MEETING, STATE_MACHINES, PN_CHANNELS } = require('../../src/machines/constants');
const { rootEventsFabric } = require('../../src/machines/root');
const { getRawCall, getJoinAudioPatch } = require('../mock/call');
const { noop } = require('../../src/utils');
const { getMockLogger } = require('../mock/logger');
import 'regenerator-runtime';

const BRIDGE = {
    shortId: 'shortId',
    id: 'bridgeFromGetId',
};

const INIT_MEETING = {
    participants: [
        {
            sessions: [{ localMute: false, localMuteVideo: false }],
            streams: [{ audio: { isActiveIn: true, isActiveOut: true } }],
        },
    ],
};

const GET_MEETING_EMPTY_STUB = {
    id: 'meetingId', // note - id field is required due to RCV-45963 fixes
};

const meetingChannelPatch = patch => ({
    channelId: PN_CHANNELS.MEETING,
    origin: STATE_MACHINES.TRANSPORT,
    data: [patch],
});

const getEventNames = spy => spy.getCalls().map(call => call.args[0]);

const REQUEST_DELAY = 350;

const propsToReport = [
    'changed:meeting',
    'changed:participants',
    'changed:streams',
    'changed:localParticipant',
    'changed:localSession',
    'changed:localStreams',
    'changed:myDemands',
    'changed:targetedToMeDemands',
    'changed:recordings',
    'changed:notifications',
    'changed:myDials',
    'changed:channels',
    'changed:meetingParticipants',
    'changed:streamsRefs',
    'changed:localSessionHistory',
    'changed:participantBySessionMap',
];

describe('SM root + reportChanged', () => {
    let clock;

    beforeEach(() => {
        clock = sinon.useFakeTimers();
    });

    afterEach(() => {
        clock.restore();
    });

    const initRootSM = async () => {
        const fakeRequest = cb => new Promise(res => setTimeout(() => res(cb()), REQUEST_DELAY));
        const onChangeSpy = sinon.spy();
        const sm = rootEventsFabric({
            meeting: INIT_MEETING,
            onChange: onChangeSpy,
            bridgeServiceFabric: () => fakeRequest(() => BRIDGE),
            meetingServiceFabric: () => fakeRequest(getRawCall),
            eventBus: new EventBus(),
            transportFabric: noop,
            rctApi: {
                getMeeting: () => fakeRequest(() => GET_MEETING_EMPTY_STUB),
            },
            getPrefixedLogger: getMockLogger,
        });
        sm.send(MEETING.JOIN);

        await clock.tickAsync(REQUEST_DELAY);
        await clock.tickAsync(REQUEST_DELAY);

        return { sm, onChangeSpy };
    };

    it('should report changed entities with all entities in reportConnected state', async () => {
        const { onChangeSpy } = await initRootSM();
        assert.deepStrictEqual(getEventNames(onChangeSpy), propsToReport);
    });

    it('should report changed entities on state change', async () => {
        const { sm, onChangeSpy } = await initRootSM();
        sm.send(MEETING.TRANSPORT_DATA, meetingChannelPatch({ allowDials: false }));
        assert.deepStrictEqual(getEventNames(onChangeSpy), [...propsToReport, 'changed:meeting']);
    });

    it('should not report changed entities on empty patches', async () => {
        const { sm, onChangeSpy } = await initRootSM();
        sm.send(MEETING.TRANSPORT_DATA, meetingChannelPatch({ id: 'meeting_id' }));
        sm.send(MEETING.TRANSPORT_DATA, meetingChannelPatch({}));
        assert.deepStrictEqual(getEventNames(onChangeSpy), [...propsToReport]);
    });

    it('should not report changed entities if state already has the same data', async () => {
        const { sm, onChangeSpy } = await initRootSM();

        sm.send(
            MEETING.TRANSPORT_DATA,
            meetingChannelPatch({
                participants: [
                    {
                        id: 'local_p_id',
                        sessions: [
                            {
                                id: 'local_session_id',
                                localMute: false,
                                localMuteVideo: true,
                            },
                        ],
                    },
                ],
            })
        );
        sm.send(MEETING.TRANSPORT_DATA, meetingChannelPatch({ allowDials: true }));

        assert.deepStrictEqual(getEventNames(onChangeSpy), [...propsToReport]);
    });

    it('should report changes of specific entities on joining audio', async () => {
        const { sm, onChangeSpy } = await initRootSM();
        sm.send(MEETING.TRANSPORT_DATA, meetingChannelPatch(getJoinAudioPatch()));
        assert.deepStrictEqual(getEventNames(onChangeSpy), [
            ...propsToReport,
            'changed:localParticipant',
            'changed:localStreams',
            'changed:streamsRefs',
        ]);
    });

    it('should report all entities again after version recovery', async () => {
        const { sm, onChangeSpy } = await initRootSM();
        sm.send(MEETING.TIMEOUT);
        await clock.tick(REQUEST_DELAY);
        assert.deepStrictEqual(getEventNames(onChangeSpy), [...propsToReport, ...propsToReport]);
    });

    it('should report all entities after soft recovery', async () => {
        const { sm, onChangeSpy } = await initRootSM();
        sm.send(MEETING.INVALIDATE_ROOM);
        await clock.tick(REQUEST_DELAY);
        assert.deepStrictEqual(getEventNames(onChangeSpy), [...propsToReport, ...propsToReport]);
    });
});
