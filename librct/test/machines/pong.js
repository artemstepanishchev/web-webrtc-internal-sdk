const assert = require('assert');
const sinon = require('sinon');

const pongInterpretFabric = require('../../src/machines/pong').default;
const fetchMock = require('fetch-mock');
const urlMatcher = require('../utils/url-matcher');
const RctApi = require('../../src/apis/rct-api');
const { getMockLogger } = require('../mock/logger');

const logger = getMockLogger();

const fetch = (...args) => global.fetch(...args);

const DOMAIN = 'https://api-ringcentral.com';
const API = '/rcvideo/v1';
const BRIDGE_ID = 'bridgeId';
const MEETING_ID = 'meetingId';
const PARTICIPANT_ID = 'participantId';
const SESSION_ID = 'sessionId';

const MIN_DELAY = 2;

const rctApi = new RctApi({
    domain: DOMAIN,
    fetch,
});

const mockPongRequest = ({ status, headers, response = {}, delay = MIN_DELAY, cb }) => {
    fetchMock.post(
        urlMatcher(
            `${DOMAIN}${API}/bridges/${BRIDGE_ID}/meetings/${MEETING_ID}/participants/${PARTICIPANT_ID}/sessions/${SESSION_ID}/pong`,
            cb
        ),
        {
            status,
            body: response,
            headers,
        },
        {
            delay,
        }
    );
};

// TODO broken, fails when CPU is loaded, RCV-48179
xdescribe('SM Pong', () => {
    let pongMachineService;
    let statesHistory;
    let navigatorStub;

    beforeEach(() => {
        navigatorStub = sinon.stub(global, 'navigator').value({ onLine: true });
        statesHistory = [];
        pongMachineService = pongInterpretFabric({
            rctApi,
            recoveryDelay: 15,
            logger,
        }).onTransition(state => {
            statesHistory.push(state.value);
        });
    });

    afterEach(() => {
        navigatorStub.restore();
        fetchMock.reset();
    });

    it('should retry pong request on error', done => {
        let pongCalledTimes = 0;
        const matcherCb = () => {
            pongCalledTimes++;

            if (pongCalledTimes === 2) {
                pongMachineService.send('LEAVE');
                setTimeout(() => {
                    assert.deepStrictEqual(statesHistory, [
                        'idle',
                        'waiting',
                        'sendingPong',
                        'handleExponentialRetryError',
                        'sendingPong',
                        'stop',
                    ]);
                    assert.strictEqual(
                        pongCalledTimes,
                        2,
                        'should not send pong after LEAVE event'
                    );
                    done();
                }, MIN_DELAY * 2);
            }
        };
        mockPongRequest({ status: 500, delay: 4, cb: matcherCb });

        pongMachineService.send('MEETING_CONNECTED', {
            bridgeId: 'bridgeId',
            meetingId: 'meetingId',
            participantId: 'participantId',
            sessionId: 'sessionId',
            pongDelay: MIN_DELAY * 2,
        });
    });

    describe('LEAVE', () => {
        it('should move machine from idle to final state on leave', done => {
            pongMachineService.send('LEAVE');
            assert.deepStrictEqual(
                statesHistory,
                ['idle', 'stop'],
                'should not change state from idle on LEAVE'
            );
            done();
        });

        it('should not send pong request if LEAVE called during waiting state', done => {
            let pongCalledTimes = 0;
            const matcherCb = () => {
                pongCalledTimes++;
            };
            mockPongRequest(204, matcherCb);

            pongMachineService.send('MEETING_CONNECTED', {
                bridgeId: 'bridgeId',
                meetingId: 'meetingId',
                participantId: 'participantId',
                sessionId: 'sessionId',
                pongDelay: MIN_DELAY * 2,
            });

            setTimeout(() => {
                pongMachineService.send('LEAVE');
                setTimeout(() => {
                    assert.deepStrictEqual(
                        statesHistory,
                        ['idle', 'waiting', 'stop'],
                        'should reset state machine on LEAVE event'
                    );
                    assert.strictEqual(pongCalledTimes, 0, 'should not send pong');
                    done();
                }, MIN_DELAY * 10);
            }, MIN_DELAY);
        });

        it('should stop SM on LEAVE during handlePongError state', done => {
            let pongCalledTimes = 0;
            const matcherCb = () => {
                pongCalledTimes++;
                setTimeout(() => {
                    pongMachineService.send('LEAVE');
                    setTimeout(() => {
                        assert.deepStrictEqual(
                            statesHistory,
                            [
                                'idle',
                                'waiting',
                                'sendingPong',
                                'handleExponentialRetryError',
                                'stop',
                            ],
                            'should reset state machine on LEAVE event'
                        );
                        assert.strictEqual(
                            pongCalledTimes,
                            1,
                            'should not send pong after LEAVE event'
                        );
                        done();
                    }, MIN_DELAY);
                }, MIN_DELAY * 2);
            };
            mockPongRequest({ status: 500, cb: matcherCb });

            pongMachineService.send('MEETING_CONNECTED', {
                bridgeId: 'bridgeId',
                meetingId: 'meetingId',
                participantId: 'participantId',
                sessionId: 'sessionId',
                pongDelay: MIN_DELAY * 2,
            });
        });

        it('should stop during the request', done => {
            let pongCalledTimes = 0;
            const matcherCb = () => {
                pongCalledTimes++;
                pongMachineService.send('LEAVE');
                setTimeout(() => {
                    assert.deepStrictEqual(
                        statesHistory,
                        ['idle', 'waiting', 'sendingPong', 'stop'],
                        'should reset state machine on LEAVE event'
                    );
                    assert.strictEqual(
                        pongCalledTimes,
                        1,
                        'should not send pong after LEAVE event'
                    );
                    done();
                }, MIN_DELAY * 5);
            };
            mockPongRequest({ status: 204, cb: matcherCb });

            pongMachineService.send('MEETING_CONNECTED', {
                bridgeId: 'bridgeId',
                meetingId: 'meetingId',
                participantId: 'participantId',
                sessionId: 'sessionId',
                pongDelay: MIN_DELAY * 2,
            });
        });
    });
});
