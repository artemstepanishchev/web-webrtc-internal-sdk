const assert = require('assert');
const sinon = require('sinon');
const cloneDeep = require('lodash.clonedeep');

const { layoutEventsFabric } = require('../../src/machines/layouts');
const TransportWrapper = require('../../src/transport/transport-wrapper');
const { LAYOUTS, LAYOUT_IDS, MEETING, PN_CHANNELS } = require('../../src/machines/constants');
const { EventBus } = require('../../src/event-bus');
const PubNub = require('../mock/pubnub');
const { getMockLogger } = require('../mock/logger');

const SUCCESS_DEBOUNCE = 50;

const authKey = 'authKey';
const subscribeKey = 'subKey';

const TRANSPORT_INFO = { authKey, subscribeKey };

const CHANNEL = 'layout-pn-subscribe-url';
const V = 13;

const CHANNEL_INFO = {
    channel: CHANNEL,
    version: V,
    pingInterval: 60000,
    pingDelay: 5000,
};

const API_REQUEST_DELAY = 250;

const getActualContext = spy => spy.lastCall.args[0].context;
const getStates = spy => spy.getCalls().map(call => call.args[0].value);
const getLastState = spy => last(getStates(spy));
const last = xs => xs && xs[xs.length - 1];

const HOST = {
    id: 'p0',
    name: 'Lenin VI',
    roomId: 'main',
    role: 'host',
    // pending | joined | left
    status: 'joined',
};
const P1 = {
    id: 'p1',
    name: 'Pavel 2',
    roomId: 'main',
    role: 'regular',
    status: 'joined',
};
const USER_LAYOUT = {
    id: LAYOUT_IDS.USER,
    rooms: {},
};
const INITIAL_LAYOUT_PAYLOAD = {
    layouts: {},
    actual: {
        rooms: {
            main: {
                settings: { name: 'main' },
                participants: {
                    [HOST.id]: HOST,
                    [P1.id]: P1,
                },
            },
        },
    },
    channelInfo: CHANNEL_INFO,
};
const BASE_LAYOUTS_PAYLOAD = {
    ...INITIAL_LAYOUT_PAYLOAD,
    layouts: {
        [LAYOUT_IDS.USER]: USER_LAYOUT,
    },
};

const DEFAULT_ROOMS = {
    room_0: { settings: { name: 'Room 1' }, participants: {} },
    room_1: { settings: { name: 'Room 2' }, participants: {} },
};

const USER_LAYOUT_WITH_DEFAULT_ROOMS = {
    id: LAYOUT_IDS.USER,
    rooms: {
        main: { settings: { name: 'Main room' }, participants: {} },
        ...DEFAULT_ROOMS,
    },
};

const initLayoutSM = ({ ...props } = {}) => {
    let isUserLayoutCreated = false;
    const rctLayoutsApi = {
        listLayouts: sinon
            .stub()
            .callsFake(
                () =>
                    new Promise(res =>
                        setTimeout(
                            () =>
                                res(
                                    isUserLayoutCreated
                                        ? BASE_LAYOUTS_PAYLOAD
                                        : INITIAL_LAYOUT_PAYLOAD
                                ),
                            API_REQUEST_DELAY
                        )
                    )
            ),
        createLayout: sinon.stub().callsFake(() =>
            new Promise(res => setTimeout(() => res(USER_LAYOUT), API_REQUEST_DELAY)).finally(() => {
                isUserLayoutCreated = true;
            })
        ),
        updateLayoutById: sinon.stub().callsFake(() =>
            new Promise(res => setTimeout(() => res(USER_LAYOUT_WITH_DEFAULT_ROOMS), API_REQUEST_DELAY))
        ),
    };
    const eventBus = new EventBus();
    const transport = new TransportWrapper({ pubnub: PubNub });
    const onChangeSpy = sinon.spy();

    const sm = layoutEventsFabric({
        eventBus,
        rctLayoutsApi,
        transport,
        onChange: onChangeSpy,
        transportOptions: {
            successDebounce: SUCCESS_DEBOUNCE,
        },
        getPrefixedLogger: getMockLogger,
        ...props,
    });

    const transitionSpy = sinon.spy();
    sm.onTransition(transitionSpy);

    return {
        rctLayoutsApi,
        eventBus,
        transport,
        sm,
        transitionSpy,
        onChangeSpy,
    };
};

const sendMeetingConnected = (eventBus, isHost = true) => {
    eventBus.send(MEETING.CONNECTED, {
        transportInfo: TRANSPORT_INFO,
        isHost,
    });
};

const openBRModal = eventBus => {
    eventBus.send(LAYOUTS.GETTING_LAYOUTS);
};

describe('SM Layouts + transport', () => {
    let clock;
    const originalPNSubscribeDelay = PubNub.SUBSCRIBE_DELAY;

    beforeEach(() => {
        clock = sinon.useFakeTimers();
        PubNub.SUBSCRIBE_DELAY = 200;
    });

    afterEach(() => {
        clock.restore();
        PubNub.SUBSCRIBE_DELAY = originalPNSubscribeDelay;
    });

    describe('for non-host', () => {
        it('should close layout SM on MEETING.CONNECTED if the user is not host', () => {
            const { eventBus, transitionSpy } = initLayoutSM();
            sendMeetingConnected(eventBus, false);

            assert.deepStrictEqual(getStates(transitionSpy), ['idle', 'closed']);
        });
    });

    describe('after MEETING.CONNECTED', () => {
        let state;

        beforeEach(() => {
            state = initLayoutSM();
            sendMeetingConnected(state.eventBus);
            openBRModal(state.eventBus);
        });

        it('should request layouts on LAYOUTS.GETTING_LAYOUTS', () => {
            const { transitionSpy, rctLayoutsApi } = state;
            assert.deepStrictEqual(getStates(transitionSpy), [
                'idle',
                'waitingForInitialization',
                'gettingLayouts',
            ]);
            assert(rctLayoutsApi.listLayouts.calledOnce);
        });

        it('should save payload of getting layouts into store', async () => {
            const { transitionSpy } = state;

            await clock.tickAsync(API_REQUEST_DELAY);

            assert.deepStrictEqual(
                getActualContext(transitionSpy).store.getState(),
                INITIAL_LAYOUT_PAYLOAD
            );
        });

        it('should create user layout if it does not exist', async () => {
            const { transitionSpy, rctLayoutsApi } = state;

            await clock.tickAsync(API_REQUEST_DELAY);
            assert.deepStrictEqual(getStates(transitionSpy), [
                'idle',
                'waitingForInitialization',
                'gettingLayouts',
                'creatingUserLayout',
            ]);

            assert(rctLayoutsApi.createLayout.calledOnce);
        });

        it('should not create user layout if it is already created', async () => {
            const { eventBus, transitionSpy } = initLayoutSM({
                rctLayoutsApi: {
                    listLayouts: () =>
                        new Promise(res =>
                            setTimeout(() => res(BASE_LAYOUTS_PAYLOAD), API_REQUEST_DELAY)
                        ),
                },
            });
            sendMeetingConnected(eventBus);
            openBRModal(eventBus);
            await clock.tickAsync(API_REQUEST_DELAY);
            assert.deepStrictEqual(getStates(transitionSpy), [
                'idle',
                'waitingForInitialization',
                'gettingLayouts',
                'working',
            ]);
        });

        it('should perform version recovery after user layout creation', async () => {
            const { transitionSpy, eventBus } = state;

            const spy = sinon.spy();
            eventBus.listen([MEETING.TIMEOUT], spy);

            await clock.tickAsync(3 * API_REQUEST_DELAY);
            assert(spy.calledOnce);
            assert.deepStrictEqual(getStates(transitionSpy), [
                'idle',
                'waitingForInitialization',
                'gettingLayouts',
                'creatingUserLayout',
                'creatingUserLayout', // because of MEETING.TIMEOUT event from transport
                'creatingDefaultRooms',
                'gettingLayouts',
            ]);
        });

        it('should report recovered after first version recovery', async () => {
            const { eventBus, transitionSpy } = state;
            const spy = sinon.spy();
            eventBus.listen([LAYOUTS.RECOVERED], spy);
            await clock.tickAsync(4 * API_REQUEST_DELAY);

            assert(getLastState(transitionSpy), 'working');
            assert(spy.calledOnce);
        });

        describe('report changed', () => {
            const ROOMS_1 = {
                'room-1-id': {
                    settings: { name: 'room #1' },
                    participants: { [P1.id]: P1 },
                    justCreated: true,
                },
            };
            const PATCH_1 = {
                layouts: {
                    [LAYOUT_IDS.USER]: {
                        rooms: ROOMS_1,
                    },
                },
            };

            it('should not report changed if user layout is not created and saved to store', async () => {
                const { onChangeSpy } = state;
                await clock.tickAsync(2 * API_REQUEST_DELAY);
                assert(onChangeSpy.notCalled);
            });

            it('should report changed after getting user layout during first recovery', async () => {
                const { onChangeSpy } = state;
                await clock.tickAsync(4 * API_REQUEST_DELAY);

                assert.strict(onChangeSpy.calledTwice);
                assert.deepStrictEqual(onChangeSpy.firstCall.args, [
                    'changed:layouts',
                    BASE_LAYOUTS_PAYLOAD.layouts,
                ]);
                assert.deepStrictEqual(onChangeSpy.secondCall.args, [
                    'changed:actual',
                    BASE_LAYOUTS_PAYLOAD.actual,
                ]);
            });

            it('should report changed on after applying actual patch', async () => {
                const { onChangeSpy, eventBus } = state;
                await clock.tickAsync(4 * API_REQUEST_DELAY);

                eventBus.send(LAYOUTS.TRANSPORT_DATA, { data: [PATCH_1] });
                assert.strictEqual(onChangeSpy.callCount, 3);
                assert.deepStrictEqual(onChangeSpy.lastCall.args, [
                    'changed:layouts',
                    {
                        [LAYOUT_IDS.USER]: {
                            id: LAYOUT_IDS.USER,
                            rooms: ROOMS_1,
                        },
                    },
                ]);
            });

            it('should ignore empty patch', async () => {
                const { onChangeSpy, eventBus } = state;
                await clock.tickAsync(4 * API_REQUEST_DELAY);
                eventBus.send(LAYOUTS.TRANSPORT_DATA, { data: [{}] });

                assert.deepStrictEqual(
                    onChangeSpy.getCalls().map(call => call.args[0]),
                    ['changed:layouts', 'changed:actual']
                );
            });

            it('should ignore patch with the same data', async () => {
                const { onChangeSpy, transitionSpy, eventBus } = state;
                await clock.tickAsync(4 * API_REQUEST_DELAY);

                eventBus.send(LAYOUTS.TRANSPORT_DATA, { data: [PATCH_1] });
                assert.strictEqual(getLastState(transitionSpy), 'working');
                assert.strictEqual(onChangeSpy.callCount, 3);
                eventBus.send(LAYOUTS.TRANSPORT_DATA, { data: [cloneDeep(PATCH_1)] });
                assert.strictEqual(onChangeSpy.callCount, 3);
            });

            it('should remove entity with deleted key', async () => {
                const { eventBus, onChangeSpy, transitionSpy } = initLayoutSM({
                    rctLayoutsApi: {
                        listLayouts: () =>
                            new Promise(res =>
                                setTimeout(
                                    () => res({ ...BASE_LAYOUTS_PAYLOAD, ...PATCH_1 }),
                                    API_REQUEST_DELAY
                                )
                            ),
                    },
                });
                sendMeetingConnected(eventBus);
                openBRModal(eventBus);
                await clock.tickAsync(API_REQUEST_DELAY);
                assert.strictEqual(getLastState(transitionSpy), 'working');

                const patch = {
                    layouts: {
                        [LAYOUT_IDS.USER]: {
                            rooms: {
                                'room-1-id': {
                                    deleted: true,
                                },
                            },
                        },
                    },
                };
                eventBus.send(LAYOUTS.TRANSPORT_DATA, { data: [patch] });
                assert.deepStrictEqual(onChangeSpy.lastCall.args, [
                    'changed:layouts',
                    { user: { rooms: {} } },
                ]);
            });
        });
    });

    describe('listening TIMEOUT event', () => {
        it('should ignore TIMEOUT event when getting layouts', async () => {
            const { transitionSpy, eventBus } = initLayoutSM();
            sendMeetingConnected(eventBus);
            openBRModal(eventBus);

            const spy = sinon.spy();
            eventBus.listen([MEETING.TIMEOUT], spy);

            await clock.tickAsync(API_REQUEST_DELAY / 2);
            eventBus.send(MEETING.TIMEOUT, {});
            await clock.tickAsync(API_REQUEST_DELAY / 2);
            assert.deepStrictEqual(getStates(transitionSpy), [
                'idle',
                'waitingForInitialization',
                'gettingLayouts',
                'gettingLayouts',
                'creatingUserLayout',
            ]);
        });

        it('should ignore TIMEOUT event during version recovery', async () => {
            PubNub.SUBSCRIBE_DELAY = 2.5 * API_REQUEST_DELAY;
            const { transitionSpy, eventBus } = initLayoutSM();
            sendMeetingConnected(eventBus);
            openBRModal(eventBus);

            const spy = sinon.spy();
            eventBus.listen([MEETING.TIMEOUT], spy);

            await clock.tickAsync(4 * API_REQUEST_DELAY);
            assert.deepStrictEqual(getStates(transitionSpy), [
                'idle',
                'waitingForInitialization',
                'gettingLayouts',
                'creatingUserLayout',
                'creatingDefaultRooms',
                'gettingLayouts',
                'gettingLayouts',
                'working',
            ]);
        });

        it('should perform 2nd version recovery if TIMEOUT event occurs after 1st recovery', async () => {
            PubNub.SUBSCRIBE_DELAY = 3.5 * API_REQUEST_DELAY;
            const { transitionSpy, eventBus } = initLayoutSM();
            sendMeetingConnected(eventBus);
            openBRModal(eventBus);

            const spy = sinon.spy();
            eventBus.listen([MEETING.TIMEOUT], spy);

            await clock.tickAsync(API_REQUEST_DELAY + PubNub.SUBSCRIBE_DELAY + API_REQUEST_DELAY);
            assert.deepStrictEqual(getStates(transitionSpy), [
                'idle',
                'waitingForInitialization',
                'gettingLayouts',
                'creatingUserLayout',
                'creatingDefaultRooms',
                'gettingLayouts',
                'working',
                'gettingLayouts',
                'working',
            ]);
        });
    });

    describe('invoking transport', () => {
        it('should not invoke transport until layouts are loaded', () => {
            const transportEventsFabric = sinon.spy();
            const { eventBus } = initLayoutSM({ transportEventsFabric });
            sendMeetingConnected(eventBus);
            openBRModal(eventBus);

            assert(transportEventsFabric.notCalled);
        });

        it('should invoke transport with correct info', async () => {
            const transportEventsFabric = sinon.spy();
            const { eventBus, transport } = initLayoutSM({ transportEventsFabric });
            sendMeetingConnected(eventBus);
            openBRModal(eventBus);

            await clock.tickAsync(API_REQUEST_DELAY);
            assert(transportEventsFabric.calledOnce);

            assert.deepStrictEqual(transportEventsFabric.firstCall.args[0], {
                id: PN_CHANNELS.LAYOUTS,
                eventBus,
                authKey: authKey,
                subscribeKey: subscribeKey,
                transport,
                channelInfo: INITIAL_LAYOUT_PAYLOAD.channelInfo,
                successDebounce: SUCCESS_DEBOUNCE,
                getPrefixedLogger: getMockLogger,
            });
        });
    });
});
