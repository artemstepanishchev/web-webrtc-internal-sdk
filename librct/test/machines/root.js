const assert = require('assert');
const rootInterpretFabric = require('../../src/machines/root').default;
import { STATE_MACHINES, MEETING as MEETING_EVENTS } from '../../src/machines/constants';
const sinon = require('sinon');
const { getMockLogger } = require('../mock/logger');
import 'regenerator-runtime';

const logger = getMockLogger();

const MIN_DELAY = 2;

const SHORT_ID = 'shortId';
const BRIDGE = {
    shortId: SHORT_ID,
    id: 'bridgeFromGetId',
};

const ERROR = {
    status: '911',
    message: 'call 911!',
};

const BASE_LOCAL_SESSION = {
    id: 'session1',
    localMute: false,
    localMuteVideo: false,
    serverMute: false,
};

const LOCAL_SESSION = {
    ...BASE_LOCAL_SESSION,
    justCreated: true,
};

const BASE_LOCAL_STREAM = {
    id: 'stream1',
    type: 'video/main',
};

const INIT_MEETING = {
    participants: [
        {
            sessions: [{ localMute: false, localMuteVideo: false }],
            streams: [{ audio: { isActiveIn: true, isActiveOut: true } }],
        },
    ],
};

const LOCAL_STREAM = {
    ...BASE_LOCAL_STREAM,
    justCreated: true,
};

const BASE_LOCAL_PARTICIPANT = {
    id: 'p1',
    displayName: 'participant one',
};

const LOCAL_PARTICIPANT = {
    ...BASE_LOCAL_PARTICIPANT,
    sessions: [LOCAL_SESSION],
    streams: [LOCAL_STREAM],
    justCreated: true,
    channels: [],
};

const REPORTED_LOCAL_SESSION = {
    ...BASE_LOCAL_SESSION,
    participantId: LOCAL_PARTICIPANT.id,
};

const REPORTED_LOCAL_STREAMS = [
    {
        ...BASE_LOCAL_STREAM,
        participantId: LOCAL_PARTICIPANT.id,
    },
];

const REMOTE_SESSION = {
    id: 'session2',
    localMute: true,
    localMuteVideo: true,
};

const REMOTE_STREAM = {
    id: 'stream2',
    sessionId: 'session2',
    type: 'video/main',
    audio: {
        isActiveIn: true,
        isActiveOut: true,
    },
};

const BASE_REMOTE_PARTICIPANT = {
    id: 'p2',
    displayName: 'participant two',
};

const REMOTE_PARTICIPANT = {
    ...BASE_REMOTE_PARTICIPANT,
    sessions: [REMOTE_SESSION],
    streams: [REMOTE_STREAM],
    channels: [],
};

const SESSIONS = {
    [REMOTE_SESSION.id]: {
        ...REMOTE_SESSION,
        participantId: REMOTE_PARTICIPANT.id,
    },
};

const STREAMS = {
    [REMOTE_STREAM.id]: {
        ...REMOTE_STREAM,
        participantId: REMOTE_PARTICIPANT.id,
    },
};

const PARTICIPANTS = {
    [BASE_REMOTE_PARTICIPANT.id]: BASE_REMOTE_PARTICIPANT,
};

const emptyFunction = () => {};

describe('SM Root', () => {
    let statesHistory;
    let rootMachineService;
    let cbEvents;
    let clock;
    let patches;

    let MEETING;
    let PURE_MEETING;
    let rctApi;

    let GET_MEETING_EMTPY_STUB;

    // const eventBus = new EventBus();
    const reportConnected = data => {
        cbEvents.push(MEETING_EVENTS.CONNECTED);
        patches.push(data);
    };
    const reportRejected = (...data) => {
        cbEvents.push(MEETING_EVENTS.FAILED);
        patches.push(...data);
    };

    beforeEach(() => {
        clock = sinon.useFakeTimers();
        rootMachineService = null;
        statesHistory = [];
        cbEvents = [];
        patches = [];
        GET_MEETING_EMTPY_STUB = {
            id: 'meetingId', // note - id field is required due to RCV-45963 fixes
        };

        PURE_MEETING = {
            id: 'meetingId',
            allowDials: false,
        };

        MEETING = {
            ...PURE_MEETING,
            participants: [LOCAL_PARTICIPANT, REMOTE_PARTICIPANT],
        };

        rctApi = {
            getMeeting: () => Promise.resolve(GET_MEETING_EMTPY_STUB),
        };
    });

    afterEach(() => {
        clock.restore();
    });

    describe('Reject', () => {
        it('should move SM to idle state with an error report on bridge unrecoverable error', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportRejected,
                reportBridgeError: reportRejected,
                bridgeServiceFabric: () => Promise.reject(ERROR),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            assert.deepStrictEqual(cbEvents, [MEETING_EVENTS.FAILED]);
            assert.deepStrictEqual(patches, [ERROR]);
            assert.deepStrictEqual(statesHistory, ['idle', 'getBridge', 'done']);
        });

        it('should move SM to idle state with an error report on meeting unrecoverable error', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportRejected,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.reject(ERROR),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            assert.deepStrictEqual(cbEvents, [MEETING_EVENTS.FAILED]);
            assert.deepStrictEqual(patches, [ERROR]);
            assert.deepStrictEqual(statesHistory, ['idle', 'getBridge', 'getMeeting', 'done']);
        });
    });

    describe('Resolve', () => {
        it('should move SM to working state after meeting was received', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportConnected,
                invokeNotifyTransports: emptyFunction,
                reportNotifyChannelsUpdate: emptyFunction,
                asyncReportPatchApplied: emptyFunction,
                muteStateController: emptyFunction,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.resolve(MEETING),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'getBridge',
                'getMeeting',
                'working',
                'done',
            ]);
        });
    });

    describe('LEAVE', () => {
        it('should move SM to idle state from working state', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportConnected,
                reportRecoveryDelay: 200,
                invokeNotifyTransports: emptyFunction,
                reportNotifyChannelsUpdate: emptyFunction,
                asyncReportPatchApplied: emptyFunction,
                muteStateController: emptyFunction,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.resolve(MEETING),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'getBridge',
                'getMeeting',
                'working',
                'done',
            ]);
        });

        it('should move SM to idle state from getting bridge', () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                bridgeServiceFabric: () =>
                    new Promise(resolve => {
                        clock.tick(MIN_DELAY * 2);
                        resolve(BRIDGE);
                    }),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            clock.tick(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, ['idle', 'getBridge', 'done']);
        });

        it('should move SM to idle state from getting meeting', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () =>
                    new Promise(resolve => {
                        clock.tick(100);
                        resolve(MEETING);
                    }),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, ['idle', 'getBridge', 'getMeeting', 'done']);
        });

        it('should move SM to idle state from recovery meeting state', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportConnected,
                rctApi: {
                    ...rctApi,
                    getMeeting: () => {
                        new Promise(resolve => {
                            clock.tick(MIN_DELAY * 2);
                            resolve(MEETING);
                        });
                    },
                },
                invokeNotifyTransports: emptyFunction,
                reportNotifyChannelsUpdate: emptyFunction,
                asyncReportPatchApplied: emptyFunction,
                muteStateController: emptyFunction,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.resolve(MEETING),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('TIMEOUT');

            await clock.tickAsync(MIN_DELAY / 2);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'getBridge',
                'getMeeting',
                'working',
                'versionRecovery',
                'done',
            ]);
        });
    });

    describe('INVALIDATE', () => {
        it('should move SM to getMeeting state from working state', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportConnected,
                invokeNotifyTransports: emptyFunction,
                reportNotifyChannelsUpdate: emptyFunction,
                asyncReportPatchApplied: emptyFunction,
                muteStateController: emptyFunction,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.resolve(MEETING),
                rctApi,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('INVALIDATE');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'getBridge',
                'getMeeting',
                'working',
                'getMeeting',
                'working',
                'done',
            ]);
        });

        it('should move SM to getMeeting state from recovery meeting state', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportConnected,
                reportRecoveryDelay: 200,
                rctApi,
                invokeNotifyTransports: emptyFunction,
                reportNotifyChannelsUpdate: emptyFunction,
                asyncReportPatchApplied: emptyFunction,
                muteStateController: emptyFunction,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.resolve(MEETING),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY * 2);

            rootMachineService.send('TIMEOUT');
            rootMachineService.send('INVALIDATE');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'getBridge',
                'getMeeting',
                'working',
                'versionRecovery',
                'getMeeting',
                'working',
                'done',
            ]);
        });
    });

    describe('TRANSPORT_DATA', () => {
        it('should recalculate patch after recovery', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportConnected,
                rctApi,
                invokeNotifyTransports: emptyFunction,
                reportNotifyChannelsUpdate: emptyFunction,
                asyncReportPatchApplied: emptyFunction,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.resolve(MEETING),
                muteStateController: () => ({}),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('TIMEOUT');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'getBridge',
                'getMeeting',
                'working',
                'versionRecovery',
                'working',
                'done',
            ]);
        });

        it('should recalculate patch after TRANSPORT_DATA event', async () => {
            rootMachineService = rootInterpretFabric({
                meeting: INIT_MEETING,
                reportConnected,
                rctApi,
                invokeNotifyTransports: emptyFunction,
                reportNotifyChannelsUpdate: emptyFunction,
                asyncReportPatchApplied: emptyFunction,
                muteStateController: emptyFunction,
                bridgeServiceFabric: () => Promise.resolve(BRIDGE),
                meetingServiceFabric: () => Promise.resolve(MEETING),
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            rootMachineService.send('JOIN');

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('TRANSPORT_DATA', {
                data: [GET_MEETING_EMTPY_STUB],
                origin: STATE_MACHINES.TRANSPORT,
            });

            await clock.tickAsync(MIN_DELAY);

            rootMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'getBridge',
                'getMeeting',
                'working',
                'working',
                'done',
            ]);
        });
    });
});
