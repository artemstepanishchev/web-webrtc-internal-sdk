const assert = require('assert');
const sinon = require('sinon');

const bridgeInterpretFabric = require('../../src/machines/bridge').default;
const { BRIDGE_ERRORS, BRIDGE_TYPES } = require('../../src/machines/bridge');
const fetchMock = require('fetch-mock');
const urlMatcher = require('../utils/url-matcher');
const NdbApi = require('../../src/apis/ndb-api');
const { getMockLogger } = require('../mock/logger');

const logger = getMockLogger();

const fetch = (...args) => global.fetch(...args);

const DOMAIN = 'http://baseurl.com';
const API = '/rcvideo/v1';

const MIN_DELAY = 2;
const RECOVERY_DELAY = 30;
const POOR_CONNECTION_TIME = 10;

const ndbApi = new NdbApi({
    domain: DOMAIN,
    fetch,
});

const meeting = {
    id: 'bridgeId',
    shortId: 'shortId',
    participantCode: 'participantCode',
    allowJoinBeforeHost: true,
};

export const mockBridgeRequest = ({
    status = 200,
    response = meeting,
    delay = MIN_DELAY,
    cb,
    headers,
    throws,
} = {}) => {
    fetchMock.get(
        {
            functionMatcher: urlMatcher(`${DOMAIN}${API}/bridges`, cb),
            body: { type: BRIDGE_TYPES.CALL },
        },
        {
            status,
            body: response,
            headers,
            throws,
        },
        {
            delay,
        }
    );
};

export const mockBridgeCreateRequest = ({
    status = 200,
    response = meeting,
    delay = MIN_DELAY,
    cb,
    headers,
    throws,
} = {}) => {
    fetchMock.post(
        urlMatcher(`${DOMAIN}${API}/bridges`, cb),
        {
            status,
            body: response,
            headers,
            throws,
        },
        {
            delay,
        }
    );
};

// TODO broken, fails when CPU is loaded, RCV-48179
xdescribe('SM Bridge', () => {
    let bridgeMachineService;
    let statesHistory;
    let p;
    let poorConnection = false;
    let navigatorStub = null;
    let addEventListenerSpy = null;

    const simulateOnlineEvent = () => {
        const onlineHandler = addEventListenerSpy.getCalls().find(call => {
            const event = call.args[0];
            return event === 'online';
        }).args[1];
        onlineHandler();
    };

    const onPoorConnection = () => {
        poorConnection = true;
    };

    const setNavigatorOnline = val => {
        if (!navigatorStub) {
            throw new Error('Cannot set onLine value, because there is no navigator stub');
        }
        global.navigator.onLine = val;
    };

    describe('create bridge', () => {
        beforeEach(() => {
            navigatorStub = sinon.stub(global, 'navigator').value({ onLine: true });
            poorConnection = false;

            statesHistory = [];
            p = new Promise((reportSuccess, reportError) => {
                bridgeMachineService = bridgeInterpretFabric({
                    ndbApi,
                    reportError,
                    reportSuccess,
                    recoveryDelay: RECOVERY_DELAY,
                    poorConnectionTime: POOR_CONNECTION_TIME,
                    onPoorConnection,
                    onBridgeConnected: () => {},
                    autoStart: false,
                    bridge: {},
                    logger,
                }).onTransition(state => {
                    statesHistory.push(state.value);
                });
            });
        });

        afterEach(() => {
            navigatorStub.restore();
            fetchMock.reset();
        });

        it('should successfully create bridge', done => {
            mockBridgeCreateRequest();
            bridgeMachineService.start();
            p.then(
                data => {
                    assert.deepStrictEqual(data, meeting);
                    assert.deepStrictEqual(statesHistory, ['requestBridge', 'done']);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        });
    });

    describe('get bridge', () => {
        beforeEach(() => {
            navigatorStub = sinon.stub(global, 'navigator').value({ onLine: true });
            addEventListenerSpy = sinon.spy(global.window, 'addEventListener');
            poorConnection = false;

            statesHistory = [];
            p = new Promise((reportSuccess, reportError) => {
                bridgeMachineService = bridgeInterpretFabric({
                    ndbApi,
                    reportError,
                    reportSuccess,
                    recoveryDelay: RECOVERY_DELAY,
                    poorConnectionTime: POOR_CONNECTION_TIME,
                    onPoorConnection,
                    onBridgeConnected: () => {},
                    autoStart: false,
                    bridge: {
                        shortId: 123456,
                    },
                    logger,
                }).onTransition(state => {
                    statesHistory.push(state.value);
                });
            });
        });

        afterEach(() => {
            navigatorStub.restore();
            navigatorStub = null;
            addEventListenerSpy.restore();
            fetchMock.reset();
        });

        it('should succesfully return bridge', done => {
            mockBridgeRequest();
            bridgeMachineService.start();
            p.then(
                data => {
                    assert.deepStrictEqual(data, meeting);
                    assert.deepStrictEqual(statesHistory, ['requestBridge', 'done']);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        });

        it('should wait for internet connection', done => {
            setNavigatorOnline(false);
            mockBridgeRequest();
            bridgeMachineService.start();
            setTimeout(() => {
                assert.deepStrictEqual(statesHistory, ['waitForOnline']);
                simulateOnlineEvent();
                assert.deepStrictEqual(statesHistory, ['waitForOnline', 'requestBridge']);
                p.then(
                    data => {
                        assert.deepStrictEqual(data, meeting);
                        assert.deepStrictEqual(statesHistory, [
                            'waitForOnline',
                            'requestBridge',
                            'done',
                        ]);
                        done();
                    },
                    () => done(new Error('should not be rejected'))
                ).catch(done);
            }, 10);
        });

        it('should report poor connection if request is long enough', done => {
            let bridgeCalled = 0;
            mockBridgeRequest({
                delay: POOR_CONNECTION_TIME * 2,
                cb: () => bridgeCalled++,
            });
            bridgeMachineService.start();
            assert.strictEqual(poorConnection, false);
            setTimeout(() => {
                bridgeMachineService.send('LEAVE');
            }, POOR_CONNECTION_TIME);
            p.then(
                () => done(new Error('should not be resolved')),
                data => {
                    assert.strictEqual(poorConnection, true);
                    assert.strictEqual(BRIDGE_ERRORS.LEAVE, data.body);
                    assert.deepStrictEqual(statesHistory, [
                        'requestBridge',
                        'requestBridge',
                        'done',
                    ]);
                    done();
                }
            ).catch(done);
        });

        describe('Errors', () => {
            it('should handle 429 error', done => {
                mockBridgeRequest({
                    status: 429,
                    headers: {
                        'Retry-After': MIN_DELAY / 1000,
                    },
                });
                bridgeMachineService.start();
                p.then(
                    () => done(new Error('should not be resolved')),
                    data => {
                        assert.strictEqual(BRIDGE_ERRORS.LEAVE, data.body);
                        assert.deepStrictEqual(statesHistory, [
                            'requestBridge',
                            'handle429',
                            'done',
                        ]);
                        done();
                    }
                ).catch(done);
                setTimeout(() => {
                    bridgeMachineService.send('LEAVE');
                }, MIN_DELAY * 1.5);
            });

            it('should detect conenction error as unknown error response', done => {
                mockBridgeRequest({
                    throws: new TypeError('Failed to fetch'),
                });
                bridgeMachineService.start();
                setNavigatorOnline(false);
                setTimeout(() => {
                    assert.deepStrictEqual(statesHistory, ['requestBridge', 'waitForOnline']);
                    bridgeMachineService.send('LEAVE');
                    p.then(
                        () => done(new Error('should not be resolved')),
                        data => {
                            assert.strictEqual(BRIDGE_ERRORS.LEAVE, data.body);
                            assert.deepStrictEqual(statesHistory, [
                                'requestBridge',
                                'waitForOnline',
                                'done',
                            ]);
                            done();
                        }
                    ).catch(done);
                }, MIN_DELAY);
            });
        });

        describe('LEAVE', () => {
            it('should leave while waiting for online', done => {
                setNavigatorOnline(false);
                bridgeMachineService.start();
                setTimeout(() => {
                    bridgeMachineService.send('LEAVE');
                    p.then(
                        () => done(new Error('should not be resolved')),
                        data => {
                            assert.strictEqual(BRIDGE_ERRORS.LEAVE, data.body);
                            assert.deepStrictEqual(statesHistory, ['waitForOnline', 'done']);
                            done();
                        }
                    ).catch(done);
                }, MIN_DELAY);
            });

            it('should leave during bridge request', done => {
                mockBridgeRequest({ delay: 1000 });
                bridgeMachineService.start();
                setTimeout(() => {
                    bridgeMachineService.send('LEAVE');
                    p.then(
                        () => done(new Error('should not be resolved')),
                        data => {
                            assert.strictEqual(BRIDGE_ERRORS.LEAVE, data.body);
                            assert.deepStrictEqual(statesHistory, ['requestBridge', 'done']);
                            done();
                        }
                    ).catch(done);
                }, MIN_DELAY);
            });

            it('should correctly leave while waiting 429 error', done => {
                mockBridgeRequest({
                    status: 429,
                    headers: {
                        'Retry-After': (10 * MIN_DELAY) / 1000,
                    },
                });
                bridgeMachineService.start();

                setTimeout(() => {
                    bridgeMachineService.send('LEAVE');
                    p.then(
                        () => done(new Error('should not be resolved')),
                        data => {
                            assert.strictEqual(BRIDGE_ERRORS.LEAVE, data.body);
                            assert.deepStrictEqual(statesHistory, [
                                'requestBridge',
                                'handle429',
                                'done',
                            ]);
                            done();
                        }
                    ).catch(done);
                }, MIN_DELAY * 2);
            });
        });
    });
});
