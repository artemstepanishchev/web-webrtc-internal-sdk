const assert = require('assert');

const { EventBus } = require('../../src/event-bus');

const EVENT_NAME_1 = 'event1';
const EVENT_NAME_2 = 'event2';

const DATA_1 = [1, 2, 3];
const DATA_2 = [4, 5, 6];

describe('Event Bus', () => {
    let eventBus = null;
    let cbData;

    beforeEach(() => {
        eventBus = new EventBus();
        cbData = [];
    });

    it('should delete event subscribes by cb', done => {
        const cb1 = (name, ...data) => {
            cbData.push(...data);
        };
        const cb2 = (name, ...data) => {
            cbData.push(...data);
        };
        const unlistenCb1 = eventBus.listen([EVENT_NAME_1, EVENT_NAME_2], cb1);
        const unlistenCb2 = eventBus.listen([EVENT_NAME_1], cb2);

        eventBus.send(EVENT_NAME_1, DATA_1);
        assert.deepStrictEqual(cbData, [DATA_1, DATA_1], 'should add DATA_1 twice');
        unlistenCb2();

        eventBus.send(EVENT_NAME_1, DATA_2);
        assert.deepStrictEqual(cbData, [DATA_1, DATA_1, DATA_2], 'should add DATA_2 once');
        unlistenCb1();

        eventBus.send(EVENT_NAME_1, DATA_1);
        assert.deepStrictEqual(cbData, [DATA_1, DATA_1, DATA_2], 'should not add new data');
        done();
    });
});
