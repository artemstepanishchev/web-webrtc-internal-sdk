const assert = require('assert');
const PubNub = require('../mock/pubnub');
const sinon = require('sinon');
import 'regenerator-runtime';

const chatTransportInterpretFabric = require('../../src/machines/chat-transport').default;

const TransportWrapper = require('../../src/transport/transport-wrapper');
const { getMockLogger } = require('../mock/logger');

const logger = getMockLogger();

const MIN_DELAY = 4;
const RECONNECT_DELAY = 8;
const RECOVERY_DELAY = 64;

const META = {
    channels: 'meta-channel',
    subscribeKey: 'meta-subkey',
    authKey: 'meta-token',
};

const MESSAGE_FROM_TRANSPORT_0 = {
    channel: 'chatChannel-123',
    seq: 0,
};

const MESSAGE_FROM_TRANSPORT_1 = {
    channel: 'chatChannel-123',
    seq: 1,
};

const CHAT_PUBLIC = {
    id: 'chatId-123',
    channel: 'chatChannel-123',
    type: 'public',
};

const CHATS = {
    ['chatId-123']: CHAT_PUBLIC,
};

const MESSAGES = {
    ['chatId-123']: [MESSAGE_FROM_TRANSPORT_0],
};

describe('SM Chat-transport', () => {
    let statesHistory;
    // let cbEvents;
    let patchNames;
    // let patches;
    let chatTransportMachineService;
    let transport;
    let clock;
    const onChange = name => {
        patchNames.push(name);
    };

    beforeEach(() => {
        clock = sinon.useFakeTimers();

        transport = new TransportWrapper({ pubnub: PubNub });
        chatTransportMachineService = null;
        statesHistory = [];
        // cbEvents = [];
        patchNames = [];
        // patches = [];

        chatTransportMachineService = chatTransportInterpretFabric({
            transport,
            transportReconnectDelay: RECONNECT_DELAY,
            recoveryDelay: RECOVERY_DELAY,
            onChange,
            reportRecovery: () => {},
            logger,
        }).onTransition(state => {
            statesHistory.push(state.value);
        });
    });
    afterEach(async () => {
        chatTransportMachineService.send('LEAVE');
        clock.restore();
    });

    it('should correctly join after failed join', async () => {
        chatTransportMachineService.send('GET_META', { data: META });

        transport._pn.setSubscribeError(true);
        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);

        transport._pn.setSubscribeError(false);
        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + RECONNECT_DELAY);

        assert.deepStrictEqual(statesHistory, [
            'idle',
            'activatingTransport',
            'handleConnectError',
            'activatingTransport',
            'working',
        ]);
    });

    describe('LEAVE', () => {
        it('should move SM to idle state from idle state', () => {
            chatTransportMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, ['idle', 'stop']);
        });

        it('should move SM to idle state from activatingTransport state', async () => {
            chatTransportMachineService.send('GET_META', { data: META });

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY / 2);

            chatTransportMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'stop',
            ]);

        });

        it('should move SM to final state from handleConnectError state', async () => {
            chatTransportMachineService.send('GET_META', { data: META });
            transport._pn.setSubscribeError(true);

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + MIN_DELAY);

            chatTransportMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'handleConnectError',
                'stop',
            ]);
        });

        it('should move SM to final state from working state', async () => {
            chatTransportMachineService.send('GET_META', { data: META });

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + MIN_DELAY);

            chatTransportMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'working',
                'stop',
            ]);
        });
        it('should move SM to final state from awaitForRecovery state', async () => {
            chatTransportMachineService.send('GET_META', { data: META });

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + MIN_DELAY);

            chatTransportMachineService.send('NEW_CHAT_FROM_TRANSPORT', {
                data: CHAT_PUBLIC.id,
            });

            await clock.tickAsync(MIN_DELAY);

            chatTransportMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'working',
                'awaitForRecovery',
                'stop',
            ]);
        });
        it('should move SM to final state from waitForGap state', async () => {
            chatTransportMachineService.send('GET_META', { data: META });

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + MIN_DELAY);

            transport.emit(TransportWrapper.EVENTS.DATA, {
                channel: MESSAGE_FROM_TRANSPORT_1.channel,
                data: MESSAGE_FROM_TRANSPORT_1,
            });

            await clock.tickAsync(MIN_DELAY);

            chatTransportMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'working',
                'waitForGap',
                'stop',
            ]);
        });
    });

    describe('after connect wrapper', () => {
        it('should ask for recovery if no messages from transport', async () => {
            chatTransportMachineService.send('GET_META', { data: META });

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
            await clock.tickAsync(RECOVERY_DELAY + MIN_DELAY);

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'working',
                'awaitForRecovery',
            ]);
        });
        it('should ignore messages with the same existed version', async () => {
            chatTransportMachineService.send('GET_META', { data: META });
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + MIN_DELAY);

            chatTransportMachineService.send('RECEIVE_NEW_DATA', {
                data: {
                    chatsById: CHATS,
                    successMessages: MESSAGES,
                },
            });
            await clock.tickAsync(MIN_DELAY);

            transport.emit(TransportWrapper.EVENTS.DATA, {
                channel: MESSAGE_FROM_TRANSPORT_0.channel,
                data: MESSAGE_FROM_TRANSPORT_0,
            });
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'working',
                'working',
                'working',
            ]);
            assert.deepStrictEqual(patchNames, ['chat:creation:success', 'chat:messages']);

        });
        it('should send new message from transport', async () => {
            chatTransportMachineService.send('GET_META', { data: META });
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + MIN_DELAY);

            chatTransportMachineService.send('RECEIVE_NEW_DATA', {
                data: {
                    chatsById: CHATS,
                    successMessages: MESSAGES,
                },
            });
            await clock.tickAsync(MIN_DELAY);

            transport.emit(TransportWrapper.EVENTS.DATA, {
                channel: MESSAGE_FROM_TRANSPORT_1.channel,
                data: MESSAGE_FROM_TRANSPORT_1,
            });
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'working',
                'working',
                'working',
            ]);
            assert.deepStrictEqual(patchNames, [
                'chat:creation:success',
                'chat:messages',
                'chat:messages',
            ]);

        });
    });

    describe('PN subscribe error', () => {
        it('should move SM to idle from idle state on UNKNOWN_ERROR', async () => {

            chatTransportMachineService.send('GET_META', { data: META });
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY + MIN_DELAY);

            chatTransportMachineService.send('RECEIVE_NEW_DATA', {
                data: {
                    chatsById: CHATS,
                    successMessages: MESSAGES,
                },
            });
            await clock.tickAsync(MIN_DELAY);

            transport.emit(TransportWrapper.EVENTS.UNKNOWN_ERROR);
            await clock.tickAsync(MIN_DELAY);

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'activatingTransport',
                'working',
                'working',
                'idle',
            ]);
        });
    });
});
