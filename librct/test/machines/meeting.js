const assert = require('assert');
const sinon = require('sinon');

const meetingInterpretFabric = require('../../src/machines/meeting').default;
const { meetingErrors } = require('../../src/machines/meeting');
const fetchMock = require('fetch-mock');
const urlMatcher = require('../utils/url-matcher');
const RctApi = require('../../src/apis/rct-api');
const { PASSWORD_STATE } = require('../../src/machines/constants');
const { getMockLogger } = require('../mock/logger');

const logger = getMockLogger();

const fetch = (...args) => global.fetch(...args);

const DOMAIN = 'https://api-ringcentral.com';
const API = '/rcvideo/v1';
const BRIDGE_ID = 'bridgeId';
const MEETING_PARAMS = {
    participants: [
        {
            sessions: [{}],
            streams: [{ type: 'video/main' }],
        },
    ],
};
const PASSWORD = '1234';

const MIN_DELAY = 2;
const RECOVERY_DELAY = 15;
const JBH_DELAY = 10;
const POOR_CONNECTION_TIME = 10;

const rctApi = new RctApi({
    domain: DOMAIN,
    fetch,
});

const MEETING = {
    id: 'bridgeId',
    allowJoinBeforeHost: true,
};

const mockMeetingRequest = ({
    status = 200,
    response = MEETING,
    delay = MIN_DELAY,
    cb,
    headers,
    throws,
    bodyCheck = JSON.stringify(MEETING_PARAMS),
} = {}) => {
    fetchMock.post(
        urlMatcher(`${DOMAIN}${API}/bridges/${BRIDGE_ID}/meetings`, cb, bodyCheck),
        {
            status,
            body: response,
            headers,
            throws,
        },
        {
            delay,
        }
    );
};

// TODO broken, fails when CPU is loaded, RCV-48179
xdescribe('SM Meeting', () => {
    let meetingMachineService;
    let statesHistory;
    let p;
    let poorConnection = false;
    let passwordChangeHandler = null;
    let joinBeforeHostHandler = null;
    let lockedHandler = null;
    let navigatorStub = null;
    let addEventListenerSpy = null;

    const onPoorConnection = () => {
        poorConnection = true;
    };

    const onPasswordChange = data => {
        if (passwordChangeHandler) {
            passwordChangeHandler(data);
        }
    };

    const onJoinBeforeHostError = shouldUpdate => {
        if (joinBeforeHostHandler && shouldUpdate) {
            joinBeforeHostHandler();
        }
    };

    const onLockedError = value => {
        if (lockedHandler) {
            lockedHandler(value);
        }
    };

    const onMeetingServerError = () => {};

    const simulateOnlineEvent = () => {
        const onlineHandler = addEventListenerSpy.getCalls().find(call => {
            const event = call.args[0];
            return event === 'online';
        }).args[1];
        onlineHandler();
    };

    const setNavigatorOnline = val => {
        if (!navigatorStub) {
            throw new Error('Cannot set onLine value, because there is no navigator stub');
        }
        global.navigator.onLine = val;
    };

    beforeEach(() => {
        navigatorStub = sinon.stub(global, 'navigator').value({ onLine: true });
        addEventListenerSpy = sinon.spy(global.window, 'addEventListener');
        sinon.stub(global.sessionStorage, 'getItem').callsFake(() => '{}');
        poorConnection = false;
        passwordChangeHandler = null;
        joinBeforeHostHandler = null;
        lockedHandler = null;

        statesHistory = [];
        p = new Promise((reportSuccess, reportError) => {
            meetingMachineService = meetingInterpretFabric({
                rctApi,
                reportError,
                reportSuccess,
                recoveryDelay: RECOVERY_DELAY,
                JBHTimeout: JBH_DELAY,
                poorConnectionTime: POOR_CONNECTION_TIME,
                onPoorConnection,
                autoStart: false,
                bridgeId: BRIDGE_ID,
                meeting: MEETING_PARAMS,
                onPasswordChange,
                onJoinBeforeHostError,
                onLockedError,
                onMeetingServerError,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
        });
    });

    afterEach(() => {
        navigatorStub.restore();
        navigatorStub = null;
        addEventListenerSpy.restore();
        global.sessionStorage.getItem.restore();
        fetchMock.reset();
    });

    it('should succesfully return meeting', done => {
        mockMeetingRequest();
        meetingMachineService.start();
        p.then(data => {
            assert.deepStrictEqual(data, MEETING);
            assert.deepStrictEqual(statesHistory, ['requestMeeting', 'done']);
            done();
        }).catch(() => done(new Error('should not be rejected')));
    });

    it('should wait for internet connection', done => {
        setNavigatorOnline(false);
        mockMeetingRequest();
        meetingMachineService.start();
        setTimeout(() => {
            assert.deepStrictEqual(statesHistory, ['waitForOnline']);
            simulateOnlineEvent();
            assert.deepStrictEqual(statesHistory, ['waitForOnline', 'requestMeeting']);
            p.then(
                data => {
                    assert.deepStrictEqual(data, MEETING);
                    assert.deepStrictEqual(statesHistory, [
                        'waitForOnline',
                        'requestMeeting',
                        'done',
                    ]);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        }, 10);
    });

    it('should report poor connection if request is long enough', done => {
        let meetingCalled = 0;
        mockMeetingRequest({
            delay: POOR_CONNECTION_TIME * 2,
            cb: () => meetingCalled++,
        });
        meetingMachineService.start();
        assert.strictEqual(poorConnection, false);
        setTimeout(() => {
            meetingMachineService.send('LEAVE');
        }, POOR_CONNECTION_TIME);
        p.then(
            () => done(new Error('should not be resolved')),
            data => {
                assert.strictEqual(poorConnection, true);
                assert.strictEqual(meetingErrors.LEAVE, data.body);
                assert.deepStrictEqual(statesHistory, ['requestMeeting', 'requestMeeting', 'done']);
                done();
            }
        ).catch(done);
    });

    describe('Errors', () => {
        it('should handle 429 error', done => {
            mockMeetingRequest({
                status: 429,
                headers: {
                    'Retry-After': MIN_DELAY / 1000,
                },
            });
            meetingMachineService.start();
            p.then(
                () => done(new Error('should not be resolved')),
                data => {
                    assert.strictEqual(meetingErrors.LEAVE, data.body);
                    assert.deepStrictEqual(statesHistory, ['requestMeeting', 'handle429', 'done']);
                    done();
                }
            ).catch(done);
            setTimeout(() => {
                meetingMachineService.send('LEAVE');
            }, MIN_DELAY * 1.5);
        });

        it('should detect conenction error as unknown error response', done => {
            mockMeetingRequest({
                throws: new TypeError('Failed to fetch'),
            });
            meetingMachineService.start();
            setNavigatorOnline(false);
            setTimeout(() => {
                assert.deepStrictEqual(statesHistory, ['requestMeeting', 'waitForOnline']);
                meetingMachineService.send('LEAVE');
                p.then(
                    () => done(new Error('should not be resolved')),
                    data => {
                        assert.strictEqual(meetingErrors.LEAVE, data.body);
                        assert.deepStrictEqual(statesHistory, [
                            'requestMeeting',
                            'waitForOnline',
                            'done',
                        ]);
                        done();
                    }
                ).catch(done);
            }, MIN_DELAY);
        });

        it('should exponential retry after 5xx error', function (done) {
            this.slow(200);
            const REQUEST_DELAY = 4;
            mockMeetingRequest({
                status: 501,
                delay: REQUEST_DELAY,
            });
            meetingMachineService.start();
            p.then(
                () => done(new Error('should not be resolved')),
                data => {
                    assert.strictEqual(meetingErrors.LEAVE, data.body);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handleExponentialRetryError',
                        'requestMeeting',
                        'handleExponentialRetryError',
                        'requestMeeting',
                        'handleExponentialRetryError',
                        'done',
                    ]);
                    done();
                }
            ).catch(done);
            setTimeout(() => {
                assert.deepStrictEqual(statesHistory, [
                    'requestMeeting',
                    'handleExponentialRetryError',
                ]);
            }, RECOVERY_DELAY);
            setTimeout(() => {
                meetingMachineService.send('LEAVE');
            }, RECOVERY_DELAY * 5 + 3 * REQUEST_DELAY);
        });
    });

    describe('LEAVE', () => {
        it('should leave while waiting for online', done => {
            setNavigatorOnline(false);
            meetingMachineService.start();
            setTimeout(() => {
                meetingMachineService.send('LEAVE');
                p.then(
                    () => done(new Error('should not be resolved')),
                    data => {
                        assert.strictEqual(meetingErrors.LEAVE, data.body);
                        assert.deepStrictEqual(statesHistory, ['waitForOnline', 'done']);
                        done();
                    }
                ).catch(done);
            }, MIN_DELAY);
        });

        it('should leave during meeting request', done => {
            mockMeetingRequest({ delay: 1000 });
            meetingMachineService.start();
            setTimeout(() => {
                meetingMachineService.send('LEAVE');
                p.then(
                    () => done(new Error('should not be resolved')),
                    data => {
                        assert.strictEqual(meetingErrors.LEAVE, data.body);
                        assert.deepStrictEqual(statesHistory, ['requestMeeting', 'done']);
                        done();
                    }
                ).catch(done);
            }, MIN_DELAY);
        });

        it('should correctly leave while waiting 429 error', done => {
            mockMeetingRequest({
                status: 429,
                headers: {
                    'Retry-After': (10 * MIN_DELAY) / 1000,
                },
            });
            meetingMachineService.start();

            setTimeout(() => {
                meetingMachineService.send('LEAVE');
                p.then(
                    () => done(new Error('should not be resolved')),
                    data => {
                        assert.strictEqual(meetingErrors.LEAVE, data.body);
                        assert.deepStrictEqual(statesHistory, [
                            'requestMeeting',
                            'handle429',
                            'done',
                        ]);
                        done();
                    }
                ).catch(done);
            }, MIN_DELAY * 2);
        });

        it('should correctly leave while waiting 5xx error', done => {
            mockMeetingRequest({
                status: 500,
            });
            meetingMachineService.start();

            setTimeout(() => {
                meetingMachineService.send('LEAVE');
                p.then(
                    () => done(new Error('should not be resolved')),
                    data => {
                        assert.strictEqual(meetingErrors.LEAVE, data.body);
                        assert.deepStrictEqual(statesHistory, [
                            'requestMeeting',
                            'handleExponentialRetryError',
                            'done',
                        ]);
                        done();
                    }
                ).catch(done);
            }, MIN_DELAY * 2);
        });
    });

    describe('password', () => {
        beforeEach(() => {
            mockMeetingRequest({
                bodyCheck: JSON.stringify({
                    ...MEETING_PARAMS,
                    meetingPassword: PASSWORD,
                }),
            });

            mockMeetingRequest({
                status: 400,
                response: {
                    success: false,
                    error: {
                        code: 'c283d8cb-81ac-4cbd-a09d-07871577c7a1',
                        message: 'A conference password is invalid.',
                        resource: 'password',
                        errors: [],
                    },
                },
            });
        });

        it('should ask for password', done => {
            passwordChangeHandler = data => {
                if (data.type === PASSWORD_STATE.REQUIRED) {
                    meetingMachineService.send('PASSWORD', { password: PASSWORD });
                }
            };
            meetingMachineService.start();

            p.then(
                data => {
                    assert.deepStrictEqual(data, MEETING);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handlePasswordRequire',
                        'requestMeeting',
                        'done',
                    ]);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        });

        it('should correctly leave during await for password', done => {
            passwordChangeHandler = data => {
                if (data.type === PASSWORD_STATE.REQUIRED) {
                    meetingMachineService.send('LEAVE');
                }
            };
            meetingMachineService.start();

            p.then(
                () => done(new Error('should not be resolved')),
                data => {
                    assert.strictEqual(meetingErrors.LEAVE, data.body);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handlePasswordRequire',
                        'done',
                    ]);
                    done();
                }
            ).catch(done);
        });
    });

    describe('join before host', () => {
        const JBHError = {
            success: false,
            error: {
                code: 'f128a1b3-940b-4b1a-bdbf-4b7a02ba6c9c',
                message: 'Moderator has not joined yet.',
                resource: 'Call',
                errors: [],
            },
        };

        it('should report JBH and the join', done => {
            mockMeetingRequest({
                status: 400,
                response: JBHError,
                cb: () => {
                    fetchMock.reset();

                    mockMeetingRequest({});
                },
            });

            let joinBeforeHost = false;
            joinBeforeHostHandler = () => {
                joinBeforeHost = true;
            };
            meetingMachineService.start();

            p.then(
                data => {
                    assert.deepStrictEqual(data, MEETING);
                    assert.strictEqual(joinBeforeHost, true);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handleJBHError',
                        'requestMeeting',
                        'done',
                    ]);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        });

        it('should report JBH only once', function (done) {
            this.slow(200);
            let joinBeforeHostCalledTimes = 0;
            mockMeetingRequest({
                status: 400,
                response: JBHError,
                cb: () => {
                    if (joinBeforeHostCalledTimes === 1) {
                        fetchMock.reset();

                        mockMeetingRequest({});
                    }
                },
            });
            joinBeforeHostHandler = () => {
                joinBeforeHostCalledTimes++;
            };
            meetingMachineService.start();

            p.then(
                data => {
                    assert.deepStrictEqual(data, MEETING);
                    assert.strictEqual(joinBeforeHostCalledTimes, 1);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handleJBHError',
                        'requestMeeting',
                        'handleJBHError',
                        'requestMeeting',
                        'done',
                    ]);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        });

        it('should correctly leave during await for password', done => {
            mockMeetingRequest({
                status: 400,
                response: JBHError,
            });
            joinBeforeHostHandler = () => {
                meetingMachineService.send('LEAVE');
            };
            meetingMachineService.start();

            p.then(
                () => done(new Error('should not be resolved')),
                data => {
                    assert.strictEqual(meetingErrors.LEAVE, data.body);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handleJBHError',
                        'done',
                    ]);
                    done();
                }
            ).catch(done);
        });
    });

    describe('locked', () => {
        const lockedError = {
            success: false,
            error: {
                code: 'c898e3ac-816d-46b7-97fc-0d66fb09cdd2',
                message: 'A conference is locked.',
                errors: [],
            },
        };

        it('should report locked and the join', done => {
            mockMeetingRequest({
                status: 400,
                response: lockedError,
                cb: () => {
                    fetchMock.reset();

                    mockMeetingRequest({});
                },
            });

            const lockedValues = [];
            lockedHandler = value => {
                lockedValues.push(value);
            };
            meetingMachineService.start();

            p.then(
                data => {
                    assert.deepStrictEqual(data, MEETING);
                    assert.deepStrictEqual(lockedValues, [true, false]);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handleExponentialRetryError',
                        'requestMeeting',
                        'done',
                    ]);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        });

        it('should report JBH only once', function (done) {
            this.slow(200);
            let lockedCalledTimes = 0;
            mockMeetingRequest({
                status: 400,
                response: lockedError,
                cb: () => {
                    if (lockedCalledTimes === 1) {
                        fetchMock.reset();

                        mockMeetingRequest({});
                    }
                },
            });
            lockedHandler = () => {
                lockedCalledTimes++;
            };
            meetingMachineService.start();

            p.then(
                data => {
                    assert.deepStrictEqual(data, MEETING);
                    assert.strictEqual(lockedCalledTimes, 2);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handleExponentialRetryError',
                        'requestMeeting',
                        'handleExponentialRetryError',
                        'requestMeeting',
                        'done',
                    ]);
                    done();
                },
                () => done(new Error('should not be rejected'))
            ).catch(done);
        });

        it('should correctly leave during await for password', done => {
            mockMeetingRequest({
                status: 400,
                response: lockedError,
            });
            lockedHandler = () => {
                meetingMachineService.send('LEAVE');
            };
            meetingMachineService.start();

            p.then(
                () => done(new Error('should not be resolved')),
                data => {
                    assert.strictEqual(meetingErrors.LEAVE, data.body);
                    assert.deepStrictEqual(statesHistory, [
                        'requestMeeting',
                        'handleExponentialRetryError',
                        'done',
                    ]);
                    done();
                }
            ).catch(done);
        });
    });
});
