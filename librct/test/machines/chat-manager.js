const assert = require('assert');
const chatManagerInterpretFabric = require('../../src/machines/chat-manager').default;
const { getMockLogger } = require('../mock/logger');
const sinon = require('sinon');
import 'regenerator-runtime';

const logger = getMockLogger();

const MIN_DELAY = 2;

const META = {
    channel: 'meta-channel',
    notificationSubKey: 'meta-subkey',
    notificationToken: 'meta-token',
};
const CHAT = {
    channel: 'channel-public',
    id: '123-public',
    type: 'public',
};

const rchApi = {
    getMeta: () => Promise.resolve(META),
    addChat: () => Promise.resolve(CHAT),
    getChats: () => Promise.resolve([CHAT]),
    getPublicChats: () => Promise.resolve([CHAT]),
    getMessages: () => Promise.resolve([]),
};

describe('SM Chat-manager', () => {
    let clock;

    let statesHistory;
    let chatManagerMachineService;

    beforeEach(() => {
        clock = sinon.useFakeTimers();

        chatManagerMachineService = null;
        statesHistory = [];
    });

    afterEach(async () => {
        chatManagerMachineService.send('LEAVE');
        clock.restore();
    });

    it('should correctly handle 429 error', async () => {
        const responses = [[], {
            status: 429,
            headers: {
                get: () => MIN_DELAY / 1000,
            },
        }];
        let i = 0;
        const chatIdHistory = [];

        chatManagerMachineService = chatManagerInterpretFabric({
            rchApi: {
                ...rchApi,
                getChats: () => Promise.resolve([
                    CHAT,
                    {
                        channel: 'channel-public',
                        id: '1234-public',
                        type: 'public',
                    }]),
                getMessages: ({ chatId }) =>
                    new Promise((resolve, reject) => {
                        setTimeout(() => {
                            chatIdHistory.push(chatId);
                            if (i === 0) {
                                resolve(responses[i]);
                            } else {
                                reject(responses[i]);
                            }
                            i = (i + 1) % 2;
                        }, MIN_DELAY * 2);
                    }),
            },
            isPrivateChatsEnabled: true,
            logger,
        }).onTransition(state => {
            statesHistory.push(state.value);
        });
        chatManagerMachineService.send('MEETING_CONNECTED');

        await clock.tickAsync(MIN_DELAY);
        chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

        await clock.tickAsync(5 * MIN_DELAY);

        assert.deepStrictEqual(statesHistory, [
            'idle',
            'gettingMeta',
            'creatingInitChat',
            'activateTransport',
            'readingNewChats',
            'gettingChatMessages',
            'working',
            'gettingRejectedChatMessages',
            'working',
        ]);

        assert.deepStrictEqual(chatIdHistory, [
            '123-public',
            '1234-public',
            '1234-public',
        ]);
    });


    describe('TIMEOUT', () => {
        it('should move SM to readingNewChats state from readingNewChats state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getChats: () =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve([CHAT]);
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_TIMEOUT');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'readingNewChats',
            ]);

        });

        it('should move SM to readingNewChats state from gettingChatMessages state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getMessages: () =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve([]);
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_TIMEOUT');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'readingNewChats',
            ]);
        });

        it('should move SM to readingNewChats state from working state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi,
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_TIMEOUT');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'working',
                'readingNewChats',
            ]);
        });

        it('should correctly handle timeout while waiting 429 error', async () => {
            const responses = [[], {
                status: 429,
                headers: {
                    get: () => MIN_DELAY / 1000,
                },
            }];
            let i = 0;
            const chatIdHistory = [];

            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getChats: () => Promise.resolve([
                        CHAT,
                        {
                            channel: 'channel-public',
                            id: '1234-public',
                            type: 'public',
                        }]),
                    getMessages: ({ chatId }) =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                chatIdHistory.push(chatId);
                                if (i === 0) {
                                    resolve(responses[i]);
                                } else {
                                    reject(responses[i]);
                                }
                                i = (i + 1) % 2;
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(2 * MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_TIMEOUT');
            await clock.tickAsync(5 * MIN_DELAY);

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'gettingRejectedChatMessages',
                'working',
            ]);

            assert.deepStrictEqual(chatIdHistory, [
                '123-public',
                '1234-public',
                '123-public',
                '1234-public',
                '1234-public',
            ]);
        });

        it('should correctly handle timeout while recalling rejected messages bcs 429', async () => {
            const responses = [[], {
                status: 429,
                headers: {
                    get: () => MIN_DELAY / 1000,
                },
            }];
            let i = 0;
            const chatIdHistory = [];

            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getChats: () => Promise.resolve([
                        CHAT,
                        {
                            channel: 'channel-public',
                            id: '1234-public',
                            type: 'public',
                        }]),
                    getMessages: ({ chatId }) =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                chatIdHistory.push(chatId);
                                if (i === 0) {
                                    resolve(responses[i]);
                                } else {
                                    reject(responses[i]);
                                }
                                i = (i + 1) % 2;
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(3 * MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_TIMEOUT');
            await clock.tickAsync(5 * MIN_DELAY);

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'gettingRejectedChatMessages',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'gettingRejectedChatMessages',
                'working',
            ]);

            assert.deepStrictEqual(chatIdHistory, [
                '123-public',
                '1234-public',
                "1234-public",
                '123-public',
                '1234-public',
                '123-public',
            ]);
        });
    });

    describe('LEAVE', () => {
        it('should move SM to final state from idle state', done => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi,
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });

            chatManagerMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, ['idle', 'stop']);
            done();
        });

        it('should move SM to final state from gettingMeta state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getMeta: () =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve(META);
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, ['idle', 'gettingMeta', 'stop']);
        });
        it('should move SM to final state from handleGetMetaError state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getMeta: () => Promise.reject(),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'handleGetMetaError',
                'stop',
            ]);
        });

        it('should move SM to final state from creatingInitChat state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    addChat: () =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve(CHAT);
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'stop',
            ]);
        });
        it('should move SM to final state from handleCreatingChatError state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    addChat: () => Promise.reject(),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'handleCreatingChatError',
                'stop',
            ]);
        });

        it('should move SM to final state from activateTransport state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi,
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'stop',
            ]);
        });
        it('should move SM to final state from readingNewChats state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getChats: () =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve([CHAT]);
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'stop',
            ]);
        });
        it('should move SM to final state from handleReadingNewChatsError state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getChats: () => Promise.reject(),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'handleReadingNewChatsError',
                'stop',
            ]);
        });

        it('should move SM to final state from gettingChatMessages state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getMessages: () =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                reject({
                                    status: 429,
                                    headers: {
                                        'Retry-After': MIN_DELAY / 1000,
                                    },
                                });
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'stop',
            ]);
        });

        it('should move SM to final state from working state', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi,
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');
            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'working',
                'stop',
            ]);
        });

        it('should correctly leave while waiting 429 error', async () => {
            const responses = [[], {
                status: 429,
                headers: {
                    get: () => MIN_DELAY / 1000,
                },
            }];
            let i = 0;
            const chatIdHistory = [];

            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getChats: () => Promise.resolve([
                        CHAT,
                        {
                            channel: 'channel-public',
                            id: '1234-public',
                            type: 'public',
                        }]),
                    getMessages: ({ chatId }) =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                chatIdHistory.push(chatId);
                                if (i === 0) {
                                    resolve(responses[i]);
                                } else {
                                    reject(responses[i]);
                                }
                                i = (i + 1) % 2;
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(2 * MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'stop',
            ]);

            assert.deepStrictEqual(chatIdHistory, [
                '123-public',
                '1234-public',
            ]);
        });

        it('should correctly leave while recalling rejected messages bcs 429', async () => {
            const responses = [[], {
                status: 429,
                headers: {
                    get: () => MIN_DELAY / 1000,
                },
            }];
            let i = 0;
            const chatIdHistory = [];

            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi: {
                    ...rchApi,
                    getChats: () => Promise.resolve([
                        CHAT,
                        {
                            channel: 'channel-public',
                            id: '1234-public',
                            type: 'public',
                        }]),
                    getMessages: ({ chatId }) =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                chatIdHistory.push(chatId);
                                if (i === 0) {
                                    resolve(responses[i]);
                                } else {
                                    reject(responses[i]);
                                }
                                i = (i + 1) % 2;
                            }, MIN_DELAY * 2);
                        }),
                },
                isPrivateChatsEnabled: true,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });
            chatManagerMachineService.send('MEETING_CONNECTED');

            await clock.tickAsync(MIN_DELAY);
            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');

            await clock.tickAsync(3 * MIN_DELAY);
            chatManagerMachineService.send('LEAVE');

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'gettingRejectedChatMessages',
                'stop',
            ]);

            assert.deepStrictEqual(chatIdHistory, [
                '123-public',
                '1234-public',
            ]);
        });
    });

    describe('PN subscribe error', () => {
        it('should move SM to activateTransport state from working state on TRANSPORT_REACTIVATE', async () => {
            chatManagerMachineService = chatManagerInterpretFabric({
                rchApi,
                logger,
            }).onTransition(state => {
                statesHistory.push(state.value);
            });

            chatManagerMachineService.send('MEETING_CONNECTED');
            await clock.tickAsync(MIN_DELAY);

            chatManagerMachineService.send('CHAT_TRANSPORT_ACTIVATED');
            await clock.tickAsync(MIN_DELAY);

            chatManagerMachineService.send('TRANSPORT_REACTIVATE');
            await clock.tickAsync(MIN_DELAY);

            assert.deepStrictEqual(statesHistory, [
                'idle',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
                'readingNewChats',
                'gettingChatMessages',
                'working',
                'working',
                'gettingMeta',
                'creatingInitChat',
                'activateTransport',
            ]);
        });
    });
});
