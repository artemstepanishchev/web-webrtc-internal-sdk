/* global fetch */
const assert = require('assert');
const syncServiceFabric = require('../../src/machines/sync').default;
const fetchMock = require('fetch-mock');
const urlMatcher = require('../utils/url-matcher');
const { getMockLogger } = require('../mock/logger');

const logger = getMockLogger();

const MIN_DELAY = 2;
const MIN_ERROR_TO = 4;
const SUCCESS_TO = 10;

const changeAction = 'CHANGE';

const sync = (requestData, signal) =>
    fetch(`https://api-rc.com/test?value=${requestData.value}`, {
        method: 'PATCH',
        body: JSON.stringify({ requestData }),
        signal,
    }).then(data => {
        if (!data.ok) {
            throw data;
        }
        return data;
    });

const mockPongRequest = (value, status, cb, delay = MIN_DELAY) => {
    fetchMock.patch(
        urlMatcher(`https://api-rc.com/test?value=${value}`, cb),
        {
            status,
        },
        {
            delay,
        }
    );
};

// TODO broken, fails when CPU is loaded, RCV-48179
xdescribe('SM Sync', () => {
    let syncMachineService;
    let statesHistory;
    beforeEach(() => {
        statesHistory = [];
        syncMachineService = syncServiceFabric({
            sync,
            successTO: SUCCESS_TO,
            minErrorTO: MIN_ERROR_TO,
            changeAction,
            logger,
        }).onTransition(state => {
            statesHistory.push(state.value);
        });
    });

    afterEach(() => {
        fetchMock.reset();
    });

    it('should go to the synced state if value is equal', done => {
        const matcherCb = () => {
            done(new Error('should not call sync method'));
        };
        mockPongRequest(true, 200, matcherCb);

        syncMachineService.send('MEETING_CONNECTED', {
            local: false,
            remote: false,
        });

        setTimeout(() => {
            assert.deepStrictEqual(statesHistory, ['idle', 'synced']);
            done();
        }, 10);
    });

    it('should start syncing if initial values are different', function (done) {
        let syncCalledTimes = 0;
        const matcherCb = () => {
            syncCalledTimes++;
            setTimeout(() => {
                syncMachineService.send(changeAction, {
                    local: false,
                    remote: false,
                });
                assert.deepStrictEqual(statesHistory, [
                    'idle',
                    'syncing',
                    'successWaiting',
                    'synced',
                ]);
                assert.strictEqual(syncCalledTimes, 1);
                done();
            }, 10);
        };
        mockPongRequest(false, 200, matcherCb);

        syncMachineService.send('MEETING_CONNECTED', {
            local: false,
            remote: true,
        });
    });

    it('should try retry failed sync request', done => {
        let syncCalledTimes = 0;
        const matcherCb = () => {
            syncCalledTimes++;
            if (syncCalledTimes === 3) {
                syncMachineService.send('LEAVE');
                // interpret onTransition is async so we need to w8 until it report state changes
                setTimeout(() => {
                    assert.deepStrictEqual(statesHistory, [
                        'idle',
                        'syncing',
                        'handleError',
                        'syncing',
                        'handleError',
                        'syncing',
                        'stop',
                    ]);

                    done();
                }, MIN_DELAY);
            }
        };
        mockPongRequest(false, 500, matcherCb);

        syncMachineService.send('MEETING_CONNECTED', {
            local: false,
            remote: true,
        });
    });

    it('should retry request after success TO if there is no update from outside', done => {
        let syncCalledTimes = 0;
        const matcherCb = () => {
            syncCalledTimes++;
            if (syncCalledTimes === 2) {
                syncMachineService.send('LEAVE');
                // interpret onTransition is async so we need to w8 until it report state changes
                setTimeout(() => {
                    assert.deepStrictEqual(statesHistory, [
                        'idle',
                        'syncing',
                        'successWaiting',
                        'syncing',
                        'stop',
                    ]);

                    done();
                }, MIN_DELAY);
            }
        };
        mockPongRequest(false, 200, matcherCb);

        syncMachineService.send('MEETING_CONNECTED', {
            local: false,
            remote: true,
        });
    });

    it('should return back to synced if quickly changed state', done => {
        let syncCalledTimes = 0;
        const matcherCb = () => {
            syncCalledTimes++;
        };
        mockPongRequest(false, 200, matcherCb, 1000);

        syncMachineService.send('MEETING_CONNECTED', {
            local: false,
            remote: true,
        });

        setTimeout(() => {
            syncMachineService.send('CHANGE', {
                local: true,
                remote: true,
            });

            syncMachineService.send('LEAVE');
            // interpret onTransition is async so we need to w8 until it report state changes
            setTimeout(() => {
                assert.deepStrictEqual(statesHistory, ['idle', 'syncing', 'synced', 'stop']);
                assert.strictEqual(syncCalledTimes, 1);

                done();
            }, MIN_DELAY);
        }, MIN_DELAY);
    });

    it('should allow to change only remote value on START', done => {
        const matcherCb = () => {
            done(new Error('should not call sync method'));
        };
        mockPongRequest(true, 200, matcherCb);

        syncMachineService.send('MEETING_CONNECTED', {
            remote: false,
        });

        setTimeout(() => {
            syncMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, ['idle', 'synced', 'stop']);
            done();
        }, 10);
    });

    it('should allow to change only remote value on CHANGE', done => {
        const matcherCb = () => {
            syncMachineService.send('LEAVE');
            done();
        };
        mockPongRequest(false, 200, matcherCb);

        syncMachineService.send('MEETING_CONNECTED', {
            remote: false,
        });

        syncMachineService.send('CHANGE', {
            remote: true,
        });
    });

    it('should allow to change the values before START', done => {
        const matcherCb = () => {
            done(new Error('should not call sync method'));
        };
        mockPongRequest(true, 200, matcherCb);

        syncMachineService.send('CHANGE', {
            local: true,
        });

        syncMachineService.send('CHANGE', {
            local: false,
        });

        setTimeout(() => {
            syncMachineService.send('MEETING_CONNECTED', {
                remote: false,
            });
        }, MIN_DELAY);

        setTimeout(() => {
            syncMachineService.send('LEAVE');
            assert.deepStrictEqual(statesHistory, ['idle', 'idle', 'idle', 'synced', 'stop']);
            done();
        }, 10);
    });

    it('should stop machine from idle state', done => {
        syncMachineService.send('LEAVE');
        assert.deepStrictEqual(statesHistory, ['idle', 'stop']);
        done();
    });
});
