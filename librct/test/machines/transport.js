const assert = require('assert');
const PubNub = require('../mock/pubnub');
const sinon = require('sinon');

const transportServiceFabric = require('../../src/machines/transport').default;
const { transportEventsFabric } = require('../../src/machines/transport');
const TRANSPORT_EVENTS = require('../../src/machines/transport').TRANSPORT_EVENTS;

const TransportWrapper = require('../../src/transport/transport-wrapper');
const { MEETING, TRANSPORT } = require('../../src/machines/constants');
const { EventBus } = require('../../src/event-bus');
const { getMockLogger } = require('../mock/logger');

const logger = getMockLogger();

const RECONNECT_DELAY = 3000;
const CONNECT_RECOVERY_DELAY = 10000;
const SUCCESS_DEBOUNCE = 50;
const GAP_DELAY = 1000;

const CHANNEL_ID = 'meeting';
const authKey = 'authKey';
const subscribeKey = 'subKey';

const CHANNEL = 'pn-subscribe-url';
const V = 13;

const CHANNEL_INFO = {
    channel: CHANNEL,
    version: V,
    pingInterval: 60000,
    pingDelay: 5000,
};

const NEW_CHANNEL_INFO = {
    pingInterval: 30000,
    pingDelay: 5000,
    version: V + 4,
    channel: CHANNEL,
};

const RECOVERY_DELAY = CHANNEL_INFO.pingInterval + CHANNEL_INFO.pingDelay;

const dataEvent = (version = V + 1, data = {}, channel = CHANNEL) => [
    TransportWrapper.EVENTS.DATA,
    { version, data, channel },
];
const pingEvent = (version = V + 1, channel = CHANNEL) => [
    TransportWrapper.EVENTS.PING,
    { version, channel },
];

const getActualContext = spy => spy.lastCall.args[0].context;
const getArgs = spy => spy.getCalls().map(call => call.args);
const getStates = spy => spy.getCalls().map(call => call.args[0].value);
const getLastState = spy => last(getStates(spy));
const last = xs => xs && xs[xs.length - 1];

// states
const REACHING_WAIT_FOR_MSGS = ['connecting', 'awaitForRecovery', 'waitForMessages'];
const REACHING_SUCCESS_DEBOUNCE = [...REACHING_WAIT_FOR_MSGS, 'successDebounce'];

const createTransportService = ({
    id,
    reportData,
    reportTimeout,
    reportInvalidate,
    transport,
    ...rest
}) =>
    transportServiceFabric({
        id,
        authKey,
        subscribeKey,
        channelInfo: CHANNEL_INFO,
        gapDelay: GAP_DELAY,
        transportReconnectDelay: RECONNECT_DELAY,
        connectRecoveryDelay: CONNECT_RECOVERY_DELAY,
        successDebounce: SUCCESS_DEBOUNCE,
        transport,
        reportData,
        reportTimeout,
        reportInvalidate,
        logger,
        ...rest,
    });

describe('SM Transport (single)', () => {
    let transportMachineService;
    let transport;
    let statesHistory;
    let cbEvents;
    let patches;
    let clock;
    let transitionSpy = null;

    const originalPNSubscribeDelay = PubNub.SUBSCRIBE_DELAY;
    const lastState = () => last(statesHistory);

    const reportData = (...data) => {
        cbEvents.push(TRANSPORT_EVENTS.DATA);
        patches.push(...data);
    };
    const reportTimeout = () => {
        cbEvents.push(TRANSPORT_EVENTS.TIMEOUT);
    };
    const reportInvalidate = () => {
        cbEvents.push(TRANSPORT_EVENTS.INVALIDATE);
    };

    beforeEach(() => {
        clock = sinon.useFakeTimers();

        transport = new TransportWrapper({ pubnub: PubNub });
        statesHistory = [];
        cbEvents = [];
        patches = [];
        PubNub.SUBSCRIBE_DELAY = 200;
        transitionSpy = sinon.spy();

        transportMachineService = createTransportService({
            id: CHANNEL_ID,
            transport,
            reportData,
            reportTimeout,
            reportInvalidate,
        });

        transportMachineService.onTransition(state => {
            statesHistory.push(state.value);
        });

        transportMachineService.onTransition(transitionSpy);
    });

    afterEach(async () => {
        PubNub.SUBSCRIBE_DELAY = originalPNSubscribeDelay;
        transitionSpy = null;
        transportMachineService.send(MEETING.LEAVE);
        clock.restore();
    });

    describe('Before connect', () => {
        it('should close transport when receiving MEETING.INVALIDATE', async () => {
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY / 2);
            transportMachineService.send(MEETING.INVALIDATE);

            assert.deepStrictEqual(statesHistory, ['connecting', 'closed']);
            assert.strictEqual(transport._pn, null);
            assert.strictEqual(transport.listenerCount(), 0);
        });

        it('should send MEETING.TIMEOUT after connecting success', async () => {
            assert.deepStrictEqual(cbEvents, []);

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);

            assert.deepStrictEqual(statesHistory, ['connecting', 'awaitForRecovery']);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT]);
        });

        it('should report MEETING.TIMEOUT event from connecting state', async () => {
            PubNub.SUBSCRIBE_DELAY = CONNECT_RECOVERY_DELAY * 10;

            const handleTransition = sinon.spy();
            const reportTimeout = sinon.spy();

            const tsm = createTransportService({
                id: 'another-channel-url',
                transport: new TransportWrapper({ pubnub: PubNub }),
                reportTimeout,
            });
            tsm.onTransition(handleTransition);

            assert.deepStrictEqual(getStates(handleTransition), ['connecting']);
            assert.deepStrictEqual(reportTimeout.callCount, 0);

            await clock.tickAsync(CONNECT_RECOVERY_DELAY);
            assert.deepStrictEqual(getStates(handleTransition), ['connecting', 'connecting']);
            assert.deepStrictEqual(reportTimeout.callCount, 1);

            await clock.tickAsync(CONNECT_RECOVERY_DELAY);
            assert.deepStrictEqual(getStates(handleTransition), [
                'connecting',
                'connecting',
                'connecting',
            ]);
            assert.deepStrictEqual(reportTimeout.callCount, 2);
        });

        it('should reconnect after RECONNECT_DELAY', async () => {
            transport._pn.setSubscribeError(true);

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
            assert.deepStrictEqual(statesHistory, ['connecting', 'handleConnectError']);
            transport._pn.setSubscribeError(false);

            await clock.tick(RECONNECT_DELAY);
            assert.deepStrictEqual(statesHistory, [
                'connecting',
                'handleConnectError',
                'connecting',
            ]);

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
            assert.deepStrictEqual(statesHistory, [
                'connecting',
                'handleConnectError',
                'connecting',
                'awaitForRecovery',
            ]);
            assert(transport._pn.subscribed);
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.DATA) > 0,
                'should listen data events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.PING) > 0,
                'should listen ping events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.INVALIDATE) > 0,
                'should listen invalidate events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.RECONNECTED) > 0,
                'should listen reconnected events'
            );
        });

        it('should had listeners after recoveryDuringConnecting node', async () => {
            const customStatesHistory = [];
            transportMachineService = transportServiceFabric({
                id: CHANNEL_ID,
                transport,
                authKey,
                subscribeKey,
                channelInfo: CHANNEL_INFO,
                gapDelay: GAP_DELAY,
                transportReconnectDelay: RECONNECT_DELAY,
                connectRecoveryDelay: PubNub.SUBSCRIBE_DELAY / 2,
                successDebounce: SUCCESS_DEBOUNCE,
                reportData,
                reportTimeout,
                reportInvalidate,
                logger,
            });

            transportMachineService.onTransition(state => {
                customStatesHistory.push(state.value);
            });

            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY / 2);

            assert(
                transport.listenerCount(TransportWrapper.EVENTS.DATA) > 0,
                'should listen data events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.PING) > 0,
                'should listen ping events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.INVALIDATE) > 0,
                'should listen invalidate events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.RECONNECTED) > 0,
                'should listen reconnected events'
            );

            transportMachineService.send(MEETING.LEAVE);

            assert.deepStrictEqual(customStatesHistory, ['connecting', 'connecting', 'closed']);
        });
    });

    describe(MEETING.LEAVE, () => {
        it('should correctly close transport and unsubscribe to PN', async () => {
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY / 2);
            transportMachineService.send(MEETING.LEAVE);

            assert.deepStrictEqual(statesHistory, ['connecting', 'closed']);
            assert.strictEqual(transport._pn, null);
            assert.strictEqual(transport.listenerCount(), 0);
        });

        it('should correctly close transport after success connect', async () => {
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);

            transportMachineService.send(MEETING.LEAVE);
            assert.deepStrictEqual(statesHistory, ['connecting', 'awaitForRecovery', 'closed']);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT]);
        });

        it('should close transport after recovery', async () => {
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
            transportMachineService.send(MEETING.RECOVERED);

            assert.deepStrictEqual(statesHistory, REACHING_WAIT_FOR_MSGS);

            transportMachineService.send(MEETING.LEAVE);
            assert.deepStrictEqual(statesHistory, [...REACHING_WAIT_FOR_MSGS, 'closed']);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT]);
        });

        it('should correctly leave on gap waiting', async () => {
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);

            transportMachineService.send(MEETING.RECOVERED);
            transport.emit(...dataEvent(V + 2));

            await clock.tickAsync(SUCCESS_DEBOUNCE);
            transportMachineService.send(MEETING.LEAVE);

            assert.deepStrictEqual(statesHistory, [
                ...REACHING_SUCCESS_DEBOUNCE,
                'waitForGap',
                'closed',
            ]);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT]);
        });
    });

    describe('After connect', () => {
        beforeEach(async () => {
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
        });

        it('should correctly reorder messages received in awaitForRecovery', () => {
            transport.emit(...dataEvent(V + 2, { patch: 22 }));
            transport.emit(...dataEvent(V + 1, { patch: 11 }));
            transportMachineService.send(MEETING.RECOVERED);

            assert.deepStrictEqual(statesHistory, [
                'connecting',
                'awaitForRecovery',
                'awaitForRecovery',
                'awaitForRecovery',
                'waitForMessages',
            ]);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT, TRANSPORT_EVENTS.DATA]);
            assert.deepStrictEqual(patches, [[{ patch: 11 }, { patch: 22 }]]);
        });

        it('should set new version and recovery delay on UPDATE_NOTIFY_CHANNELS event', () => {
            transportMachineService.send(MEETING.NOTIFY_CHANNELS_UPDATE, {
                channelInfo: { ...NEW_CHANNEL_INFO, version: V - 2 },
            });

            const ctx = getActualContext(transitionSpy);
            assert.strictEqual(ctx.version, V - 2);
            assert.strictEqual(
                ctx.recoveryDelay,
                NEW_CHANNEL_INFO.pingInterval + NEW_CHANNEL_INFO.pingDelay
            );
        });

        describe('gap & debounce', () => {
            it('should not visit waitForMessages state if gap exists right after recovery', async () => {
                transport.emit(...dataEvent(V + 10));
                transportMachineService.send(MEETING.RECOVERED);
                assert.deepStrictEqual(statesHistory, [
                    'connecting',
                    'awaitForRecovery',
                    'awaitForRecovery',
                    'waitForGap',
                ]);
                assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT]);
            });

            it('should leave waitForGap if the missed (next) message received', async () => {
                transport.emit(...dataEvent(V + 2));
                transportMachineService.send(MEETING.RECOVERED);

                assert.strictEqual(lastState(), 'waitForGap');
                clock.tick(GAP_DELAY / 2);

                transport.emit(...pingEvent(V + 1));
                assert.strictEqual(lastState(), 'waitForMessages');
            });

            it('should not reset gap timeout when receiving non-next message', async () => {
                transportMachineService.send(MEETING.RECOVERED);
                transport.emit(...dataEvent(V + 2));

                clock.tick(SUCCESS_DEBOUNCE);

                transport.emit(...dataEvent(V + 3));
                assert.deepStrictEqual(statesHistory, [
                    ...REACHING_SUCCESS_DEBOUNCE,
                    'waitForGap',
                    'waitForGap',
                ]);

                clock.tick(GAP_DELAY / 2);
                transport.emit(...pingEvent(V + 10));
                assert.deepStrictEqual(statesHistory, [
                    ...REACHING_SUCCESS_DEBOUNCE,
                    'waitForGap',
                    'waitForGap',
                    'waitForGap',
                ]);

                clock.tick(GAP_DELAY / 2);
                assert.strictEqual(lastState(), 'awaitForRecovery');
            });
        });
    });

    describe('After recovered', () => {
        beforeEach(async () => {
            await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
            transportMachineService.send(MEETING.RECOVERED);
        });

        it('should correctly batch and reorder messages during SUCCESS_DEBOUNCE', () => {
            transport.emit(...dataEvent(V + 2, { patch: 22 }));

            assert.deepStrictEqual(statesHistory, REACHING_SUCCESS_DEBOUNCE);
            assert.deepStrictEqual(patches, []);

            clock.tick(SUCCESS_DEBOUNCE - 1);
            transport.emit(...dataEvent(V + 1, { patch: 11 }));

            clock.tick(1);

            assert.deepStrictEqual(statesHistory, [
                ...REACHING_SUCCESS_DEBOUNCE,
                'successDebounce',
                'waitForMessages',
            ]);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT, TRANSPORT_EVENTS.DATA]);
            assert.deepStrictEqual(patches, [[{ patch: 11 }, { patch: 22 }]]);
        });

        it('should report TO if gap is not filled after timeout', () => {
            transport.emit(...dataEvent(V + 2));

            clock.tick(SUCCESS_DEBOUNCE);
            assert.deepStrictEqual(statesHistory, [...REACHING_SUCCESS_DEBOUNCE, 'waitForGap']);

            clock.tick(GAP_DELAY);
            assert.deepStrictEqual(statesHistory, [
                ...REACHING_SUCCESS_DEBOUNCE,
                'waitForGap',
                'awaitForRecovery',
            ]);

            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT, TRANSPORT_EVENTS.TIMEOUT]);
            assert.deepStrictEqual(patches, []);
        });

        it('should ask for recovery if no messages', () => {
            assert.deepStrictEqual(statesHistory, REACHING_WAIT_FOR_MSGS);
            clock.tick(RECOVERY_DELAY);

            assert.deepStrictEqual(statesHistory, [...REACHING_WAIT_FOR_MSGS, 'awaitForRecovery']);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT, TRANSPORT_EVENTS.TIMEOUT]);
            assert.deepStrictEqual(patches, []);
        });

        it('should ask for recovery on transport reconnect', () => {
            transport.emit(TransportWrapper.EVENTS.RECONNECTED);
            assert.deepStrictEqual(statesHistory, [...REACHING_WAIT_FOR_MSGS, 'awaitForRecovery']);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT, TRANSPORT_EVENTS.TIMEOUT]);
            assert.deepStrictEqual(patches, []);
            assert(transport._pn.subscribed);
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.DATA) > 0,
                'should listen data events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.PING) > 0,
                'should listen ping events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.INVALIDATE) > 0,
                'should listen invalidate events'
            );
            assert(
                transport.listenerCount(TransportWrapper.EVENTS.RECONNECTED) > 0,
                'should listen reconnected events'
            );
        });

        it('should increase version on ping messages, but should NOT emit patch', () => {
            transport.emit(...pingEvent(V + 1));
            transport.emit(...pingEvent(V + 2));

            assert.deepStrictEqual(statesHistory, [
                ...REACHING_SUCCESS_DEBOUNCE,
                'successDebounce',
            ]);

            clock.tick(SUCCESS_DEBOUNCE);
            assert.deepStrictEqual(statesHistory, [
                ...REACHING_SUCCESS_DEBOUNCE,
                'successDebounce',
                'waitForMessages',
            ]);

            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT]);
            assert.deepStrictEqual(patches, []);
            assert.strictEqual(getActualContext(transitionSpy).version, V + 2);
        });

        it('should ignore messages with the same existed version', () => {
            transport.emit(...dataEvent(V));
            clock.tick(SUCCESS_DEBOUNCE);
            assert.deepStrictEqual(statesHistory, [
                ...REACHING_SUCCESS_DEBOUNCE,
                'waitForMessages',
            ]);
            assert.deepStrictEqual(cbEvents, [TRANSPORT_EVENTS.TIMEOUT]);
            assert.deepStrictEqual(patches, []);
            assert.strictEqual(getActualContext(transitionSpy).version, V);
        });

        describe('recovery delay', () => {
            it('should set new recoveryDelay on MEETING.PATCH_APPLIED', () => {
                assert.strictEqual(getActualContext(transitionSpy).recoveryDelay, RECOVERY_DELAY);

                transportMachineService.send(MEETING.PATCH_APPLIED, {
                    channelInfo: NEW_CHANNEL_INFO,
                });
                assert.strictEqual(
                    getActualContext(transitionSpy).recoveryDelay,
                    NEW_CHANNEL_INFO.pingInterval + NEW_CHANNEL_INFO.pingDelay
                );
            });

            it('should not update version on MEETING.PATCH_APPLIED', () => {
                transportMachineService.send(MEETING.PATCH_APPLIED, {
                    channelInfo: NEW_CHANNEL_INFO,
                });
                assert.strictEqual(getActualContext(transitionSpy).version, V);
            });

            it('should use new recoveryDelay on next visit of waitForMessages vertex', () => {
                transportMachineService.send(MEETING.PATCH_APPLIED, {
                    channelInfo: { ...NEW_CHANNEL_INFO, pingInterval: 15000, pingDelay: 5000 },
                });
                const newRecoveryDelay = 20000;
                clock.tick(newRecoveryDelay);
                assert.strictEqual(lastState(), 'waitForMessages');

                // Despite the context, the timer still has the old recoveryDelay value
                // until it's recreated on the next waitForMessages visit.
                clock.tick(RECOVERY_DELAY - newRecoveryDelay);
                assert.strictEqual(lastState(), 'awaitForRecovery');

                transportMachineService.send(MEETING.RECOVERED);
                // next visit, ther timer has an updated value
                assert.strictEqual(lastState(), 'waitForMessages');
                clock.tick(newRecoveryDelay);
                assert.strictEqual(lastState(), 'awaitForRecovery');
            });

            it('should decrease recovery delay on sequential recovery', () => {
                clock.tick(RECOVERY_DELAY);

                transportMachineService.send(MEETING.RECOVERED);
                assert.strictEqual(
                    getActualContext(transitionSpy).recoveryDelay,
                    RECOVERY_DELAY / 2
                );
                clock.tick(RECOVERY_DELAY / 2);

                transportMachineService.send(MEETING.RECOVERED);
                assert.strictEqual(
                    getActualContext(transitionSpy).recoveryDelay,
                    TRANSPORT.MIN_RECOVERY_DELAY_MS
                );
            });
        });
    });
});

describe('SM Transport (parallel)', () => {
    let transport;
    let eventBus;
    let clock;

    beforeEach(() => {
        transport = new TransportWrapper({ pubnub: PubNub });
        eventBus = new EventBus();
        clock = sinon.useFakeTimers();

        sinon.spy(eventBus, 'send');
    });

    afterEach(() => {
        eventBus.send.restore();
        clock.restore();
    });

    const createTransport = (id, channelInfo = CHANNEL_INFO, ...rest) => {
        const transitionSpy = sinon.spy();
        const sm = transportEventsFabric({
            id,
            authKey,
            subscribeKey,
            channelInfo,
            gapDelay: GAP_DELAY,
            transportReconnectDelay: RECONNECT_DELAY,
            connectRecoveryDelay: CONNECT_RECOVERY_DELAY,
            successDebounce: SUCCESS_DEBOUNCE,
            transport,
            eventBus,
            getPrefixedLogger: getMockLogger,
            ...rest,
        });
        sm.onTransition(transitionSpy);
        return { transitionSpy, sm };
    };

    it('should ignore MEETING.TIMEOUT sent by itself', async () => {
        const t1 = createTransport('channel-A');
        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
        assert.deepStrictEqual(getStates(t1.transitionSpy), ['connecting', 'awaitForRecovery']);
        const [[event, data]] = getArgs(eventBus.send);
        assert.strictEqual(event, MEETING.TIMEOUT);
        assert.strictEqual(data.channelId, 'channel-A');
    });

    it('should consistently transit to awaitForRecovery and emit TIMEOUTs', async () => {
        const t1 = createTransport('channel-A', { ...CHANNEL_INFO, channel: 'ch-url-A' });
        const t2 = createTransport('channel-B', { ...CHANNEL_INFO, channel: 'ch-url-B' });

        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);

        assert.deepStrictEqual(getStates(t1.transitionSpy), [
            'connecting',
            'awaitForRecovery', // transition after subscribe delay
            'awaitForRecovery', // caused by TIMEOUT event from t2 SM
        ]);
        assert.deepStrictEqual(getStates(t2.transitionSpy), [
            'connecting',
            'connecting', // caused by TIMEOUT event from t1 SM
            'awaitForRecovery', // transition after subscribe delay
        ]);
    });

    it('should asynchronously connect 2nd transport SM', async () => {
        const t1 = createTransport('ch-A', { ...CHANNEL_INFO, channel: 'ch-url-A' });
        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
        eventBus.send(MEETING.RECOVERED);

        const t2 = createTransport('ch-B', { ...CHANNEL_INFO, channel: 'ch-url-B' });
        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);

        assert.deepStrictEqual(getStates(t1.transitionSpy), [
            ...REACHING_WAIT_FOR_MSGS,
            'awaitForRecovery', // caused by TIMEOUT event from t2 SM
        ]);

        assert.deepStrictEqual(getStates(t2.transitionSpy), ['connecting', 'awaitForRecovery']);
        eventBus.send(MEETING.RECOVERED);

        assert.strictEqual(getLastState(t1.transitionSpy), 'waitForMessages');
        assert.strictEqual(getLastState(t2.transitionSpy), 'waitForMessages');
    });

    it('should independently handle messages', async () => {
        const t1 = createTransport('ch-A', { ...CHANNEL_INFO, channel: 'ch-url-A', version: 15 });
        const t2 = createTransport('ch-B', { ...CHANNEL_INFO, channel: 'ch-url-B', version: 7 });

        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
        eventBus.send(MEETING.RECOVERED);

        transport.emit(...dataEvent(16, {}, 'ch-url-A'));

        assert.strictEqual(getLastState(t1.transitionSpy), 'successDebounce');
        assert.strictEqual(getLastState(t2.transitionSpy), 'waitForMessages');

        clock.tick(SUCCESS_DEBOUNCE / 2);
        transport.emit(...pingEvent(8, 'ch-url-B'));

        clock.tick(SUCCESS_DEBOUNCE / 2);
        assert.strictEqual(getLastState(t1.transitionSpy), 'waitForMessages');
        assert.strictEqual(getLastState(t2.transitionSpy), 'successDebounce');

        transport.emit(...pingEvent(9, 'ch-url-B'));
        clock.tick(SUCCESS_DEBOUNCE / 2);

        assert.strictEqual(getLastState(t1.transitionSpy), 'waitForMessages');
        assert.strictEqual(getLastState(t2.transitionSpy), 'waitForMessages');

        const [event, data] = eventBus.send.lastCall.args;
        assert.strictEqual(event, MEETING.TRANSPORT_DATA);
        assert.strictEqual(data.channelId, 'ch-A');
    });

    it('all transport SMs should wait for recovery if one them emits TIMEOUT', async () => {
        const t1 = createTransport('ch-A', { ...CHANNEL_INFO, channel: 'ch-url-A', version: 15 });
        const t2 = createTransport('ch-B', { ...CHANNEL_INFO, channel: 'ch-url-B', version: 7 });
        const t3 = createTransport('ch-C', { ...CHANNEL_INFO, channel: 'ch-url-C', version: 3 });

        await clock.tickAsync(PubNub.SUBSCRIBE_DELAY);
        eventBus.send(MEETING.RECOVERED);

        transport.emit(...dataEvent(17, {}, 'ch-url-A'));
        clock.tick(SUCCESS_DEBOUNCE);
        clock.tick(GAP_DELAY);

        const [event, data] = eventBus.send.lastCall.args;
        assert.strictEqual(event, MEETING.TIMEOUT);
        assert.strictEqual(data.channelId, 'ch-A');

        assert.strictEqual(getLastState(t1.transitionSpy), 'awaitForRecovery');
        assert.strictEqual(getLastState(t2.transitionSpy), 'awaitForRecovery');
        assert.strictEqual(getLastState(t3.transitionSpy), 'awaitForRecovery');
    });
});
