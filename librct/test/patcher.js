const assert = require('assert');
const cloneDeep = require('lodash.clonedeep');
const { splitPatch, tearOutArray, isPatchEmpty, patchEntity, patch } = require('../src/patcher');
const { getSessionPatch, newParticpantPatch, newStreamPatch } = require('./mock/call');

describe('Patcher', () => {
    describe('tearOutArray', () => {
        it('should replace obj field with ids array', done => {
            const src = {
                id: 'id',
                field: [
                    { internalId: 1, x: 5 },
                    { internalId: 2, x: 6 },
                    { internalId: 3, x: 7 },
                ],
                field2: 'test',
            };
            const dest = {};
            const classified = tearOutArray(
                src,
                'field',
                dest,
                item => ({ ...item, objId: 'id' }),
                'internalId'
            );

            assert.deepStrictEqual(src, { id: 'id', field2: 'test' }, 'wrong obj in result');
            assert.deepStrictEqual(
                classified,
                {
                    1: { internalId: 1, x: 5, objId: 'id' },
                    2: { internalId: 2, x: 6, objId: 'id' },
                    3: { internalId: 3, x: 7, objId: 'id' },
                },
                'wrong classified obj'
            );
            assert.strictEqual(dest, classified, 'should not modify the link to dest object');
            done();
        });
    });
    describe('splitPatch', () => {
        it('should split patch to meeting, participants, sessions and streams patches', done => {
            const {
                meeting,
                participants,
                sessions,
                streams,
                localParticipant,
                localSession,
                localStreams,
            } = splitPatch({
                allowDials: false,
                participants: [
                    {
                        id: 'p1Id',
                        displayName: 'p1',
                        sessions: [{ id: 'p1Session', localMute: true }],
                        streams: [{ id: 'p1Stream', type: 'video/main' }],
                    },
                    {
                        id: 'p2Id',
                        displayName: 'p2',
                        sessions: [{ id: 'p2Session', localMute: false }],
                        streams: [{ id: 'p2Stream', type: 'video/main' }],
                    },
                ],
            });
            assert.deepStrictEqual(meeting, { allowDials: false }, 'meeting has wrong data');
            assert.deepStrictEqual(
                participants,
                {
                    p1Id: {
                        id: 'p1Id',
                        displayName: 'p1',
                    },
                    p2Id: {
                        id: 'p2Id',
                        displayName: 'p2',
                    },
                },
                'participants has wrong data'
            );
            assert.deepStrictEqual(
                sessions,
                {
                    p1Session: {
                        id: 'p1Session',
                        localMute: true,
                        participantId: 'p1Id',
                        hasLeftBreakoutRoom: false,
                    },
                    p2Session: {
                        id: 'p2Session',
                        localMute: false,
                        participantId: 'p2Id',
                        hasLeftBreakoutRoom: false,
                    },
                },
                'sessions has wrong data'
            );
            assert.deepStrictEqual(
                streams,
                {
                    p1Stream: { id: 'p1Stream', type: 'video/main', participantId: 'p1Id' },
                    p2Stream: { id: 'p2Stream', type: 'video/main', participantId: 'p2Id' },
                },
                'sessions has wrong data'
            );
            assert.strictEqual(localParticipant, null, 'should not return localParticipant');
            assert.strictEqual(localSession, null, 'should not return localSession');
            assert.strictEqual(localStreams, null, 'should not return localStreams');
            done();
        });

        it('should return entities as local using justCreated field', done => {
            const {
                meeting,
                participants,
                sessions,
                streams,
                localParticipant,
                localSession,
                localStreams,
            } = splitPatch({
                allowDials: false,
                participants: [
                    {
                        id: 'p1Id',
                        displayName: 'p1',
                        sessions: [{ id: 'p1Session', localMute: true, justCreated: true }],
                        streams: [{ id: 'p1Stream', type: 'video/main', justCreated: true }],
                        justCreated: true,
                    },
                    {
                        id: 'p2Id',
                        displayName: 'p2',
                        sessions: [{ id: 'p2Session', localMute: false }],
                        streams: [{ id: 'p2Stream', type: 'video/main' }],
                    },
                ],
            });
            assert.deepStrictEqual(meeting, { allowDials: false }, 'meeting has wrong data');
            assert.deepStrictEqual(
                participants,
                {
                    p2Id: {
                        id: 'p2Id',
                        displayName: 'p2',
                    },
                },
                'participants has wrong data'
            );
            assert.deepStrictEqual(
                sessions,
                {
                    p2Session: {
                        id: 'p2Session',
                        localMute: false,
                        participantId: 'p2Id',
                        hasLeftBreakoutRoom: false,
                    },
                },
                'sessions has wrong data'
            );
            assert.deepStrictEqual(
                streams,
                {
                    p2Stream: { id: 'p2Stream', type: 'video/main', participantId: 'p2Id' },
                },
                'sessions has wrong data'
            );
            assert.deepStrictEqual(
                localParticipant,
                {
                    id: 'p1Id',
                    displayName: 'p1',
                },
                'wrong localParticipant'
            );
            assert.deepStrictEqual(
                localSession,
                {
                    id: 'p1Session',
                    localMute: true,
                    participantId: 'p1Id',
                    hasLeftBreakoutRoom: false,
                },
                'wrong localSession'
            );
            assert.deepStrictEqual(
                localStreams,
                { p1Stream: { id: 'p1Stream', type: 'video/main', participantId: 'p1Id' } },
                'wrong localStreams'
            );
            done();
        });

        it('should mark entities as local using passed local ids', done => {
            const {
                meeting,
                participants,
                sessions,
                streams,
                localParticipant,
                localSession,
                localStreams,
            } = splitPatch(
                {
                    allowDials: false,
                    participants: [
                        {
                            id: 'p1Id',
                            displayName: 'p1',
                            sessions: [{ id: 'p1Session', localMute: true }],
                            streams: [{ id: 'p1Stream', type: 'video/main' }],
                        },
                        {
                            id: 'p2Id',
                            displayName: 'p2',
                            sessions: [{ id: 'p2Session', localMute: false }],
                            streams: [{ id: 'p2Stream', type: 'video/main' }],
                        },
                    ],
                },
                {
                    localParticipantId: 'p1Id',
                    localSessionId: 'p1Session',
                    localStreamIds: ['p1Stream'],
                }
            );
            assert.deepStrictEqual(meeting, { allowDials: false }, 'meeting has wrong data');
            assert.deepStrictEqual(
                participants,
                {
                    p2Id: {
                        id: 'p2Id',
                        displayName: 'p2',
                    },
                },
                'participants has wrong data'
            );
            assert.deepStrictEqual(
                sessions,
                {
                    p2Session: {
                        id: 'p2Session',
                        localMute: false,
                        participantId: 'p2Id',
                        hasLeftBreakoutRoom: false,
                    },
                },
                'sessions has wrong data'
            );
            assert.deepStrictEqual(
                streams,
                {
                    p2Stream: { id: 'p2Stream', type: 'video/main', participantId: 'p2Id' },
                },
                'sessions has wrong data'
            );
            assert.deepStrictEqual(
                localParticipant,
                {
                    id: 'p1Id',
                    displayName: 'p1',
                },
                'wrong localParticipant'
            );
            assert.deepStrictEqual(
                localSession,
                {
                    id: 'p1Session',
                    localMute: true,
                    participantId: 'p1Id',
                    hasLeftBreakoutRoom: false,
                },
                'wrong localSession'
            );
            assert.deepStrictEqual(
                localStreams,
                { p1Stream: { id: 'p1Stream', type: 'video/main', participantId: 'p1Id' } },
                'wrong localStreams'
            );
            done();
        });

        it('should not return empty entities', done => {
            const {
                meeting,
                participants,
                sessions,
                streams,
                localParticipant,
                localSession,
                localStreams,
            } = splitPatch(
                {
                    participants: [
                        {
                            id: 'p1Id',
                            sessions: [{ id: 'p1Session', localMute: true }, { id: 'p1Session2' }],
                            streams: [{ id: 'p1Stream' }, { id: 'p1Stream2' }],
                        },
                        {
                            id: 'p2Id',
                            sessions: [{ id: 'p2Session' }],
                            streams: [{ id: 'p2Stream' }],
                        },
                    ],
                },
                {
                    localParticipantId: 'p1Id',
                    localSessionId: 'p1Session',
                    localStreamIds: ['p1Stream'],
                }
            );

            assert.strictEqual(meeting, null, 'should ignore empty meeting patch');
            assert.strictEqual(participants, null, 'should ignore empty participants patch');
            // assert.strictEqual(sessions, null, 'should ignore empty sessions patch');  // TODO: this needs more consideration
            assert.strictEqual(streams, null, 'should ignore empty streams patch');
            assert.strictEqual(
                localParticipant,
                null,
                'should ignore empty localParticipants patch'
            );
            assert.deepStrictEqual(
                localSession,
                {
                    id: 'p1Session',
                    localMute: true,
                    participantId: 'p1Id',
                    hasLeftBreakoutRoom: false,
                },
                'should not ignore not-empty localSession patch'
            );
            assert.deepStrictEqual(
                localStreams,
                { p1Stream: { id: 'p1Stream', participantId: 'p1Id' } },
                'should not ignore not-empty localStreams patch'
            );
            done();
        });
    });

    describe('isPatchEmpty', () => {
        it('should return false if patch has information to change', done => {
            const p = { id: 5, test: 'xyz' };
            const result = isPatchEmpty(p);
            assert.strictEqual(result, false, 'should return false');
            done();
        });

        it('should accept fields to check', done => {
            const empty = { abc: 5, xyz: 6 };
            const full = { abc: 5, xyz: 6, id: 1 };
            assert.strictEqual(
                isPatchEmpty(empty, ['abc', 'xyz']),
                true,
                'should return true for empty'
            );
            assert.strictEqual(
                isPatchEmpty(full, ['abc', 'xyz']),
                false,
                'should return false for full'
            );
            done();
        });

        it('should return true for null patch', done => {
            const p = null;
            const result = isPatchEmpty(p);
            assert.strictEqual(result, true, 'should return true');
            done();
        });
    });

    describe('patchEntity', () => {
        it('should apply simple patch (modify property)', done => {
            const obj = { x: 1, y: 2 };
            const p = { x: 3 };
            const result = patchEntity(obj, p);
            assert.deepStrictEqual(result, { x: 3, y: 2 }, 'should modify x property');
            assert.notStrictEqual(result, obj, 'should create new object');
            done();
        });

        it('should apply simple patch (add new property)', done => {
            const obj = { x: 1, y: 2 };
            const p = { z: 3 };
            const result = patchEntity(obj, p);
            assert.deepStrictEqual(result, { x: 1, y: 2, z: 3 }, 'should add z property');
            assert.notStrictEqual(result, obj, 'should create new object');
            done();
        });

        it('should return the same object if patch is null', done => {
            const obj = { x: 1, y: 2 };
            const p = null;
            const result = patchEntity(obj, p);
            assert.deepStrictEqual(result, { x: 1, y: 2 }, 'should not modify the object');
            assert.strictEqual(result, obj, 'should not create new object');
            done();
        });

        it('should return the same object if patch does not modify data', done => {
            const obj = { x: 1, y: 2 };
            const p = { x: 1 };
            const result = patchEntity(obj, p);
            assert.deepStrictEqual(result, { x: 1, y: 2 }, 'should not modify the object');
            assert.strictEqual(result, obj, 'should not create new object');
            done();
        });

        it('should return the patch if obj is null', done => {
            const obj = null;
            const p = { x: 1, y: 2 };
            const result = patchEntity(obj, p);
            assert.deepStrictEqual(p, { x: 1, y: 2 }, 'should not modify the object');
            assert.strictEqual(result, p, 'should not create new object');
            done();
        });

        it('should combine deep objects', done => {
            const obj = { x: { y: 2 } };
            const p = { x: { y: 3, z: 4 } };
            const result = patchEntity(obj, p);
            assert.notStrictEqual(result, obj, 'should create new object');
            assert.deepStrictEqual({ x: { y: 3, z: 4 } }, result, 'should modify the deep object');
            done();
        });

        it('should save links inside deep objects', done => {
            const deepPart = { y: 2, z: 3 };
            const obj = { x: deepPart };
            const p = { x: { y: 2 } };
            const result = patchEntity(obj, p);
            assert.strictEqual(result, obj, 'should not create new object');
            assert.deepStrictEqual(
                result,
                { x: { y: 2, z: 3 } },
                'should not modify the deep object'
            );
            assert.strictEqual(result.x, deepPart, 'should not create new object for deepPart');
            done();
        });
    });

    describe('patch', () => {
        let call;
        const localIds = {
            localParticipantId: 'local_p_id',
            localSessionId: 'local_session_id',
            localStreamIds: ['local_stream_id'],
        };

        beforeEach(() => {
            call = {
                meeting: { allowDials: false },
                participants: { p_id_1: { id: 'p_id_1', roles: [] } },
                sessions: {
                    session_id_1: {
                        id: 'session_id_1',
                        participantId: 'p_id_1',
                        localMute: true,
                        hasLeftBreakoutRoom: false,
                    },
                },
                streams: { stream_id_1: { id: 'stream_id_1', type: 'video/main' } },
                localParticipant: { id: 'local_p_id', roles: [] },
                localSession: {
                    id: 'local_session_id',
                    participantId: 'local_p_id',
                    localMute: false,
                    hasLeftBreakoutRoom: false,
                },
                localSessionHistory: { local_session_id: '2021-02-10T10:47:13.044Z' },
                localStreams: { local_stream_id: { id: 'local_stream_id', type: 'video/main' } },
            };
        });

        it('should patch meeting entity only', () => {
            const p = {
                allowDials: true,
            };
            const result = patch(call, p);

            assert.notStrictEqual(call.meeting, result.meeting);
            assert.strictEqual(call.participants, result.participants);
            assert.strictEqual(call.sessions, result.sessions);
            assert.strictEqual(call.streams, result.streams);
            assert.strictEqual(call.localParticipant, result.localParticipant);
            assert.strictEqual(call.localSession, result.localSession);
            assert.strictEqual(call.localStreams, result.localStreams);
            assert.deepStrictEqual(result.meeting, { allowDials: true });
        });

        it('should apply patch with local mute to localSession only', () => {
            const p = getSessionPatch();
            const sessionPart = getSessionPatch().participants[0].sessions[0];
            const result = patch(call, p, localIds);

            assert.strictEqual(call.participants, result.participants);
            assert.strictEqual(call.sessions, result.sessions);
            assert.strictEqual(call.streams, result.streams);
            assert.strictEqual(call.localParticipant, result.localParticipant);
            assert.strictEqual(call.localStreams, result.localStreams);
            assert.deepStrictEqual(result.localSession, { ...call.localSession, ...sessionPart });
        });

        it("should apply patch with non-local participant's mute to sessions entity only", () => {
            const p = getSessionPatch({ participantId: 'p_id_1', sessionId: 'session_id_1' });
            // TODO: remove deep cloning after RCv-46262
            const sessionPart = cloneDeep(p.participants[0].sessions[0]);
            const result = patch(call, p, localIds);

            assert.strictEqual(call.participants, result.participants);
            assert.strictEqual(call.localSession, result.localSession);
            assert.strictEqual(call.streams, result.streams);
            assert.strictEqual(call.localParticipant, result.localParticipant);
            assert.strictEqual(call.localStreams, result.localStreams);
            assert.deepStrictEqual(result.sessions['session_id_1'], {
                ...call.sessions['session_id_1'],
                ...sessionPart,
            });
        });

        it("should keep links to non-local participant's entities when new participant joins meeting", () => {
            const p1 = newParticpantPatch({ participantId: 'p_id_N' });
            const p2 = getSessionPatch({ participantId: 'p_id_N', sessionId: 'session_id_N' });
            const p3 = newStreamPatch({
                participantId: 'p_id_N',
                sessionId: 'session_id_N',
                streamId: 'stream_id_N',
            });
            const result = patch(patch(patch(call, p1), p2), p3);
            assert.strictEqual(call.sessions['session_id_1'], result.sessions['session_id_1']);
            assert.strictEqual(call.participants['p_id_1'], result.participants['p_id_1']);
            assert.strictEqual(call.streams['stream_id_1'], result.streams['stream_id_1']);
        });

        it('should patch only localStreams entity', () => {
            const p = {
                participants: [
                    { id: 'local_p_id', streams: [{ id: 'local_stream_id', startTime: '123' }] },
                ],
            };
            const result = patch(call, p, localIds);

            assert.strictEqual(call.meeting, result.meeting);
            assert.strictEqual(call.participants, result.participants);
            assert.strictEqual(call.sessions, result.sessions);
            assert.strictEqual(call.streams, result.streams);
            assert.strictEqual(call.localParticipant, result.localParticipant);
            assert.strictEqual(call.localSession, result.localSession);
            assert.notStrictEqual(call.localStreams, result.localStreams);
            assert.deepStrictEqual(result.localStreams, {
                local_stream_id: {
                    id: 'local_stream_id',
                    participantId: 'local_p_id',
                    startTime: '123',
                    type: 'video/main',
                },
            });
        });

        it('should save new session to the localSessionHistory', () => {
            const startTime = Date.now();
            const p = {
                participants: [
                    {
                        id: 'local_p_id',
                        sessions: [
                            { id: 'local_session_id_2', localMute: false, justCreated: true },
                        ],
                    },
                ],
            };
            const result = patch(call, p, {
                localParticipantId: 'local_p_id',
                localSessionId: 'local_session_id',
            });
            const endTime = Date.now();
            const saved = new Date(result.localSessionHistory['local_session_id_2']);

            assert.strictEqual(Object.keys(result.localSessionHistory).length, 2);
            assert(saved >= startTime && saved <= endTime, 'wrong time in session history');
        });

        it('should not override session save time', () => {
            const patch1 = {
                participants: [
                    {
                        id: 'local_p_id',
                        sessions: [
                            { id: 'local_session_id_2', localMute: false, justCreated: true },
                        ],
                    },
                ],
            };
            let result = patch(call, patch1, {
                localParticipantId: 'local_p_id',
                localSessionId: 'local_session_id',
            });
            const saved = result.localSessionHistory.localSessionId2;

            const patch2 = {
                participants: [
                    {
                        id: 'local_p_id',
                        sessions: [{ id: 'local_session_id_2', localMute: true }],
                    },
                ],
            };
            result = patch(result, patch2, {
                localParticipantId: 'local_p_id',
                localSessionId: 'local_session_id_2',
            });
            assert.strictEqual(Object.keys(result.localSessionHistory).length, 2);
            assert.strictEqual(result.localSessionHistory.localSessionId2, saved);
        });
    });
});
