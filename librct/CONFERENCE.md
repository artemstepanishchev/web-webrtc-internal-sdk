# Conference API

## Classes

<dl>
<dt><a href="#Conference">Conference</a></dt>
<dd></dd>
</dl>

## Typedefs

<dl>
<dt><a href="#TapSession">TapSession</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#StreamSubtype">StreamSubtype</a> : <code>&#x27;window&#x27;</code> | <code>&#x27;screen&#x27;</code> | <code>&#x27;display&#x27;</code> | <code>&#x27;tab&#x27;</code></dt>
<dd></dd>
<dt><a href="#TrackMeta">TrackMeta</a> : <code>object</code></dt>
<dd></dd>
<dt><a href="#UpdateStreamData">UpdateStreamData</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#JoinResponse">JoinResponse</a> : <code>Object</code></dt>
<dd></dd>
</dl>

<a name="Conference"></a>

## Conference
**Kind**: global class

* [Conference](#Conference)
    * [.getCall([bridgeParams], [callParams])](#Conference+getCall) ⇒ <code>Promise.&lt;Call&gt;</code>
    * [.updateLocalMuteState(localMute, localMuteVideo)](#Conference+updateLocalMuteState) ⇒ <code>Promise.&lt;Participant&gt;</code>
    * [.getPersonalMeeting()](#Conference+getPersonalMeeting) ⇒ <code>Promise.&lt;Bridge, Error&gt;</code>
    * [.getChat([chatId])](#Conference+getChat)
    * [.createChat(call, chatParams, options)](#Conference+createChat)
    * _Bridge_
        * [.getBridgeInfo(options)](#Conference+getBridgeInfo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.createBridge(options)](#Conference+createBridge) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.updateBridge(options)](#Conference+updateBridge) ⇒ <code>Promise.&lt;Object&gt;</code>
    * _Call_
        * [.getCallInfo()](#Conference+getCallInfo) ⇒ <code>Promise.&lt;Object&gt;</code>
    * _Moderator_
        * [.endCall()](#Conference+endCall) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.muteParticipant(id)](#Conference+muteParticipant) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unmuteParticipant(id)](#Conference+unmuteParticipant) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.muteParticipantVideo(id)](#Conference+muteParticipantVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unmuteParticipantVideo(id)](#Conference+unmuteParticipantVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.provideModeratorRole(id)](#Conference+provideModeratorRole) ⇒ <code>Promise</code>
        * [.revokeModeratorRole(id)](#Conference+revokeModeratorRole) ⇒ <code>Promise</code>
        * [.muteCall(allowUnmute)](#Conference+muteCall) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unmuteCall()](#Conference+unmuteCall) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.allowScreenSharing()](#Conference+allowScreenSharing) ⇒ <code>Promise</code>
        * [.disallowScreenSharing()](#Conference+disallowScreenSharing) ⇒ <code>Promise</code>
        * [.unlockCall()](#Conference+unlockCall) ⇒ <code>Promise</code>
        * [.lockCall()](#Conference+lockCall) ⇒ <code>Promise</code>
        * [.kickParticipant(id)](#Conference+kickParticipant) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.allowAnnotations()](#Conference+allowAnnotations) ⇒ <code>Promise</code>
        * [.disallowAnnotations()](#Conference+disallowAnnotations) ⇒ <code>Promise</code>
    * _Participant_
        * [.join(args)](#Conference+join) ⇒ [<code>Promise.&lt;JoinResponse&gt;</code>](#JoinResponse)
        * [.leave()](#Conference+leave) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.createStream(type, subtype, [data])](#Conference+createStream) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
        * [.deleteStream(id)](#Conference+deleteStream) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.deleteParticipantStreamsByType(participantId, type)](#Conference+deleteParticipantStreamsByType) ⇒ <code>Promise</code>
        * [.localMute()](#Conference+localMute) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.localUnmute()](#Conference+localUnmute) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.localMuteVideo()](#Conference+localMuteVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.localUnmuteVideo()](#Conference+localUnmuteVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.leaveViaBeacon()](#Conference+leaveViaBeacon) ⇒ <code>Boolean</code>
        * [.toggleParticipantRingStatus()](#Conference+toggleParticipantRingStatus) ⇒ <code>Boolean</code>
        * [.createDemand()](#Conference+createDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandControl()](#Conference+demandControl) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmute([participantToMuteId])](#Conference+demandUnmute) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmuteAll()](#Conference+demandUnmuteAll) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmuteVideo([participantToMuteId])](#Conference+demandUnmuteVideo) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmuteVideoAll()](#Conference+demandUnmuteVideoAll) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.approveDemand()](#Conference+approveDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.rejectDemand()](#Conference+rejectDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.cancelDemand()](#Conference+cancelDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.deleteDemand()](#Conference+deleteDemand) ⇒ <code>Promise.&lt;Object, Error&gt;</code>
        * [.createChannel()](#Conference+createChannel) ⇒ <code>Promise.&lt;Channel, Error&gt;</code>
        * [.deleteChannel()](#Conference+deleteChannel) ⇒ <code>Promise.&lt;Object, Error&gt;</code>
    * _Recording_
        * [.getRecording(id)](#Conference+getRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.startRecording()](#Conference+startRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.stopRecording(id)](#Conference+stopRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.pauseRecording(id)](#Conference+pauseRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unpauseRecording(id)](#Conference+unpauseRecording) ⇒ <code>Promise.&lt;Object&gt;</code>

<a name="Conference+getCall"></a>

### conference.getCall([bridgeParams], [callParams]) ⇒ <code>Promise.&lt;Call&gt;</code>
get call

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- [bridgeParams] <code>Object</code> <code> = {}</code> - [Bridge](Bridge)
- [callParams] <code>Object</code> <code> = {}}</code> - [Call](Call)

<a name="Conference+updateLocalMuteState"></a>

### conference.updateLocalMuteState(localMute, localMuteVideo) ⇒ <code>Promise.&lt;Participant&gt;</code>
update local mutes for current participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- localMute <code>Boolean</code> - mute audio
- localMuteVideo <code>Boolean</code> - mute video

<a name="Conference+getPersonalMeeting"></a>

### conference.getPersonalMeeting() ⇒ <code>Promise.&lt;Bridge, Error&gt;</code>
get personal meeting info {@lonk Bridge}

**Kind**: instance method of [<code>Conference</code>](#Conference)
**See**: Bridge
<a name="Conference+getChat"></a>

### conference.getChat([chatId])
Get chat by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- [chatId] <code>String</code>

<a name="Conference+createChat"></a>

### conference.createChat(call, chatParams, options)
Creates new chat or returns existing instance

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- call <code>Call</code>
- chatParams <code>Object</code>
- options <code>Object</code> - internal params and methods

<a name="Conference+getBridgeInfo"></a>

### conference.getBridgeInfo(options) ⇒ <code>Promise.&lt;Object&gt;</code>
Get [Bridge](Bridge) attributes by options

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Bridge
**See**: [bridge](bridge)
**Params**

- options <code>BridgeParams</code>

**Example**
```js
librct.conference().getBridgeInfo({ id: 'bridgeId' })
  .then(json => console.log(json));
```
<a name="Conference+createBridge"></a>

### conference.createBridge(options) ⇒ <code>Promise.&lt;Object&gt;</code>
Create new [Bridge](Bridge)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Bridge
**See**: [bridge](bridge)
**Params**

- options <code>BridgeParams</code>

**Example**
```js
librct.conference().createBridge()
  .then(json => console.log(json));
```
<a name="Conference+updateBridge"></a>

### conference.updateBridge(options) ⇒ <code>Promise.&lt;Object&gt;</code>
Update [Bridge](Bridge)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Bridge
**See**: [bridge](bridge)
**Params**

- options <code>BridgeParams</code>

**Example**
```js
librct.conference().createBridge()
  .then(json => console.log(json));
```
<a name="Conference+getCallInfo"></a>

### conference.getCallInfo() ⇒ <code>Promise.&lt;Object&gt;</code>
Get [Call](Call) info

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Call
**See**: [Call](Call)
<a name="Conference+endCall"></a>

### conference.endCall() ⇒ <code>Promise.&lt;Object&gt;</code>
End call (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Moderator
**Example**
```js
librct.conference().endCall();
```
<a name="Conference+muteParticipant"></a>

### conference.muteParticipant(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Mute participant audio by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().muteParticipant('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+unmuteParticipant"></a>

### conference.unmuteParticipant(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Unmute participant audio by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().unmuteParticipant('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+muteParticipantVideo"></a>

### conference.muteParticipantVideo(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Mute participant video by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().muteParticipantVideo('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+unmuteParticipantVideo"></a>

### conference.unmuteParticipantVideo(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Unmute participant video by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().unmuteParticipantVideo('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+provideModeratorRole"></a>

### conference.provideModeratorRole(id) ⇒ <code>Promise</code>
Provide moderator role to participant (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().provideModeratorRole('participantId')
  .then(() => console.log('moderator role granted for participant'));
```
<a name="Conference+revokeModeratorRole"></a>

### conference.revokeModeratorRole(id) ⇒ <code>Promise</code>
Revoke moderator role from participant (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().revokeModeratorRole('participantId')
  .then(() => console.log('moderator role revoked from participant'));
```
<a name="Conference+muteCall"></a>

### conference.muteCall(allowUnmute) ⇒ <code>Promise.&lt;Object&gt;</code>
Enable call mute (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Moderator
**Params**

- allowUnmute <code>boolean</code> <code> = true</code>

**Example**
```js
librct.conference().muteCall();
```
<a name="Conference+unmuteCall"></a>

### conference.unmuteCall() ⇒ <code>Promise.&lt;Object&gt;</code>
Disable call mute (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Moderator
**Example**
```js
librct.conference().unmuteCall();
```
<a name="Conference+allowScreenSharing"></a>

### conference.allowScreenSharing() ⇒ <code>Promise</code>
Enable screensharing (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().allowScreenSharing();
```
<a name="Conference+disallowScreenSharing"></a>

### conference.disallowScreenSharing() ⇒ <code>Promise</code>
Disable screensharing (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().disallowScreenSharing();
```
<a name="Conference+unlockCall"></a>

### conference.unlockCall() ⇒ <code>Promise</code>
Unlock call (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().unlockCall();
```
<a name="Conference+lockCall"></a>

### conference.lockCall() ⇒ <code>Promise</code>
Lock call (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().lockCall();
```
<a name="Conference+kickParticipant"></a>

### conference.kickParticipant(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Kick participant from conference (*requires moderator role*)
This method will remove all devises of participant from conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

<a name="Conference+allowAnnotations"></a>

### conference.allowAnnotations() ⇒ <code>Promise</code>
Enable annotations (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().allowAnnotations();
```
<a name="Conference+disallowAnnotations"></a>

### conference.disallowAnnotations() ⇒ <code>Promise</code>
Enable annotations (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().disallowAnnotations();
```
<a name="Conference+join"></a>

### conference.join(args) ⇒ [<code>Promise.&lt;JoinResponse&gt;</code>](#JoinResponse)
Join to the conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**Params**

- args <code>Object</code> - join args
    - .call <code>Call</code> - [Call](Call)  - if specified call and bridge will not be requested
    - .bridgeParams <code>Object</code> - [Bridge](Bridge) - required with shortId for recovery
    - .sessionParams <code>Object</code> - [Session](Session)
    - .callParams <code>Object</code> - [Call](Call)
    - .participantParams <code>Object</code> - [Participant](Participant)
    - .streamParams <code>Object</code> - [Stream](Stream)
                                    (no stream will be created if not defined)
    - .prefetchCall <code>Boolean</code> - prefetch call data
    - .subscribeToPubNub <code>Boolean</code> - subscribe to PN channel
    - .cache <code>Boolean</code> - set to false if You want to work with internal classes

**Example**
```js
librct.join({ bridgeParams: { shortId: 'myMeeting' }}).then(result => console.log(result))
```
<a name="Conference+leave"></a>

### conference.leave() ⇒ <code>Promise.&lt;Object&gt;</code>
Leave conference (unsubscribe and delete session)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Participant
**Example**
```js
librct.conference().leave().then(() => console.log('done'));
```
<a name="Conference+createStream"></a>

### conference.createStream(type, subtype, [data]) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
Create [Stream](Stream) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Stream
**Params**

- type <code>string</code> - stream type
- subtype [<code>StreamSubtype</code>](#StreamSubtype) - stream type
- [data] [<code>UpdateStreamData</code>](#UpdateStreamData) - -

**Example**
```js
participant.createStream({ type: 'video/main' })
  .then(stream => console.log(stream));
```
<a name="Conference+deleteStream"></a>

### conference.deleteStream(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Delete [Stream](Stream) for local participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - stream JSON
**Category**: Participant
**Params**

- id <code>String</code> - stream id

**Example**
```js
participant.deleteStream('streamId')
  .then(stream => console.log(stream));
```
<a name="Conference+deleteParticipantStreamsByType"></a>

### conference.deleteParticipantStreamsByType(participantId, type) ⇒ <code>Promise</code>
Delete [Stream](Stream) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**Params**

- participantId <code>String</code> - participant id
- type <code>String</code> - stream type

**Example**
```js
participant.deleteParticipantStreamsByType('participantId', 'video/screensharing');
```
<a name="Conference+localMute"></a>

### conference.localMute() ⇒ <code>Promise.&lt;Object&gt;</code>
Local mute audio

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localMute();
```
<a name="Conference+localUnmute"></a>

### conference.localUnmute() ⇒ <code>Promise.&lt;Object&gt;</code>
Local unmute audio

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localUnmute();
```
<a name="Conference+localMuteVideo"></a>

### conference.localMuteVideo() ⇒ <code>Promise.&lt;Object&gt;</code>
Local mute video

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localMuteVideo();
```
<a name="Conference+localUnmuteVideo"></a>

### conference.localUnmuteVideo() ⇒ <code>Promise.&lt;Object&gt;</code>
Local unmute video

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localUnmuteVideo();
```
<a name="Conference+leaveViaBeacon"></a>

### conference.leaveViaBeacon() ⇒ <code>Boolean</code>
Delete current participant by sync request

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
<a name="Conference+toggleParticipantRingStatus"></a>

### conference.toggleParticipantRingStatus() ⇒ <code>Boolean</code>
Performing redial either hangup for given participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
<a name="Conference+createDemand"></a>

### conference.createDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .type <code>Object</code> - demanding type
    - .resource <code>Object</code> - demanding resource
    - .grant <code>Object</code> - demanding permissions on resource

<a name="Conference+demandControl"></a>

### conference.demandControl() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create remote control [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .resource <code>Object</code> - demanding resource
    - .grant <code>Object</code> - demanding permissions on resource

<a name="Conference+demandUnmute"></a>

### conference.demandUnmute([participantToMuteId]) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

- [participantToMuteId] <code>String</code> - participant to be muted

<a name="Conference+demandUnmuteAll"></a>

### conference.demandUnmuteAll() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute all [Demand](Demand) for conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
<a name="Conference+demandUnmuteVideo"></a>

### conference.demandUnmuteVideo([participantToMuteId]) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute video [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

- [participantToMuteId] <code>String</code> - participant to be muted

<a name="Conference+demandUnmuteVideoAll"></a>

### conference.demandUnmuteVideoAll() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute all [Demand](Demand) for conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
<a name="Conference+approveDemand"></a>

### conference.approveDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Aproves [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .participantId <code>String</code> - who's demanding
    - .demandId <code>String</code>

<a name="Conference+rejectDemand"></a>

### conference.rejectDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Rejects [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .participantId <code>String</code> - who's demanding
    - .demandId <code>String</code>

<a name="Conference+cancelDemand"></a>

### conference.cancelDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Cancels [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .demandId <code>String</code>

<a name="Conference+deleteDemand"></a>

### conference.deleteDemand() ⇒ <code>Promise.&lt;Object, Error&gt;</code>
Deletes [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .participantId <code>String</code>
    - .demandId <code>String</code>

<a name="Conference+createChannel"></a>

### conference.createChannel() ⇒ <code>Promise.&lt;Channel, Error&gt;</code>
Create [Channel](Channel) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Channel
**Params**

    - .type <code>String</code>
    - .direction <code>String</code>
    - .info <code>Object</code>

<a name="Conference+deleteChannel"></a>

### conference.deleteChannel() ⇒ <code>Promise.&lt;Object, Error&gt;</code>
Deletes [Channel](Channel)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Channel
**Params**

    - .channelId <code>String</code>

<a name="Conference+getRecording"></a>

### conference.getRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Get recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - recording JSON
**Category**: Recording
**Params**

- id <code>Number</code> - recording id

**Example**
```js
librct.conference().getRecording(id)
  .then(recording => console.log(`recordingId: ${recording.id}`))
```
<a name="Conference+startRecording"></a>

### conference.startRecording() ⇒ <code>Promise.&lt;Object&gt;</code>
Start recording (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - recording JSON
**Category**: Recording
**Example**
```js
librct.conference().startRecording()
  .then(recording => console.log(`recordingId: ${recording.recordingId}`))
```
<a name="Conference+stopRecording"></a>

### conference.stopRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Stop recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Recording
**Params**

- id <code>String</code> - recording id

**Example**
```js
librct.conference().stopRecording('recordingId')
  .then(() => console.log('recording stopped'));
```
<a name="Conference+pauseRecording"></a>

### conference.pauseRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Pause recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Recording
**Params**

- id <code>String</code> - recording id

**Example**
```js
librct.conference().pauseRecording('recordingId')
  .then(() => console.log('recording paused'));
```
<a name="Conference+unpauseRecording"></a>

### conference.unpauseRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Unpause recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Recording
**Params**

- id <code>String</code> - recording id

**Example**
```js
librct.conference().unpauseRecording('recordingId')
  .then(() => console.log('recording paused'));
```
<a name="TapSession"></a>

## TapSession : <code>Object</code>
**Kind**: global typedef
**Properties**

- id <code>String</code>
- notificationToken <code>String</code> - token for subscribe to PubNub channel
- token <code>String</code> - token for create session request on VAS

<a name="StreamSubtype"></a>

## StreamSubtype : <code>&#x27;window&#x27;</code> \| <code>&#x27;screen&#x27;</code> \| <code>&#x27;display&#x27;</code> \| <code>&#x27;tab&#x27;</code>
**Kind**: global typedef
<a name="TrackMeta"></a>

## TrackMeta : <code>object</code>
**Kind**: global typedef
**Properties**

- isActiveIn <code>boolean</code> - -
- isActiveOut <code>boolean</code> - -

<a name="UpdateStreamData"></a>

## UpdateStreamData : <code>Object</code>
**Kind**: global typedef
**Properties**

- audio [<code>TrackMeta</code>](#TrackMeta) - -
- video [<code>TrackMeta</code>](#TrackMeta) - -

<a name="JoinResponse"></a>

## JoinResponse : <code>Object</code>
**Kind**: global typedef
**Properties**

-  <code>Call</code> - [Call](Call) plain object
-  <code>Participant</code> - Current [Participant](Participant) plain object
-  <code>Session</code> - Created [Session](Session) plain object
-  <code>Stream</code> - Created [Stream](Stream) plain object

