# RCT Web Client

## Links

* [Simple join and leave](#example-simple)
* [Join conference with PubNub subscription](#example-pubnub)
* [Join to multiple calls](#example-multiple)
* [Local mute](#example-local-mute)
* [Moderator methods](#example-moderator)
* [Kick participant](#example-kick-participant)
* [Base methods](./CONFERENCE.md)
* [Full API](./DOCS.md)

## Simple join and leave
<a name="example-simple"></a>

```javascript
import LibRCT from 'librct';

const librct = new LibRCT();

librct.conference().join({
    brdigeOption: {
        meetingId: 'abcdef'
    }
}).then(result => console.log(result));
```

## Join conference with PubNub subscription
<a name="example-pubnub"></a>

```javascript
import LibRCT from 'librct';

const librct = new LibRCT({
    subscribeKey: 'yourSubscribeKey'
});

librct.conference().join({
    brdigeOption: {
        meetingId: 'abcdef'
    },
    subscribeToPubNub: true
}).then(result => {
    setTimeout(() => {
        // leave conference after 1 min
        librct.conference().leave();
    }, 60000);
});
```

## Join to multiple calls
<a name="example-multiple"></a>

**Warning:** Conference API caches call and current participant so You can't use
it except `librct.conference().join(params)` method with `cache: false`
But You still can use methods from inner classes

```javascript
import LibRCT from 'librct';

const librct = new LibRCT({
    subscribeKey: 'yourSubscribeKey'
});

for (const i = 0; i < 5; i++) {
    librct.conference().join({
        brdigeOption: {
            meetingId: 'abcdef'
        },
        cache: false
    })
}

librct.conference().join({
    brdigeOption: {
        meetingId: 'abcdef'
    },
    cache: false
}).then(result => {
    // console log list of participants
    console.log(result.call.get('participants'));
    // console log participant by id
    const participant = result.call.getParticipant('participantId');
    console.log(participant);
    // mute participant
    participant.mute();
    setTimeout(() => {
        // leave conference after 1 min
        result.call.leave();
    }, 60000);
});
```

## Local mute
<a name="example-local-mute"></a>

```javascript
import LibRCT from 'librct';

const librct = new LibRCT({
    subscribeKey: 'yourSubscribeKey'
});

librct.conference().on('change', data => console.log('call changed', data));
librct.conference().on('recovery', data => console.log('recovery start', data));
librct.conference().on('recovery:success', data => console.log('recovery success', data));

librct.conference().join({
    brdigeOption: {
        meetingId: 'abcdef'
    },
    subscribeToPubNub: true
}).then(result => {
    librct.conference().localMute();
    // librct.conference().localMuteVideo();

    setTimeout(() => {
        librct.conference().localUnmute();
        // librct.conference().localUnmuteVideo();
    }, 10000);
});
```

# Moderator methods (*requires moderator role*)
<a name="example-moderator"></a>

```javascript
import LibRCT from 'librct';

const librct = new LibRCT({
    subscribeKey: 'yourSubscribeKey'
});

librct.conference().join({
    brdigeOption: {
        meetingId: 'abcdef'
    },
    subscribeToPubNub: true
}).then(() => {
    librct.conference().allowScreenSharing();
    // librct.conference().muteCall();
    // librct.conference().lockCall();
    // librct.conference().startRecording();
    // librct.conference().muteParticipant('participantId');
    // librct.conference().muteParticipantVideo('participantId');

    setTimeout(() => {
        librct.conference().disallowScreenSharing();
        // librct.conference().unmuteCall();
        // librct.conference().unlockCall();
        // librct.conference().stopRecording();
        // librct.conference().unmuteParticipant('participantId');
        // librct.conference().unmuteParticipantVideo('participantId');
    }, 10000);
});
```

# Kick participant (*requires moderator role*)
<a name="example-kick-participant"></a>

```javascript
import LibRCT from 'librct';

const librct = new LibRCT({
    subscribeKey: 'yourSubscribeKey'
});

librct.conference().join({
    brdigeOption: {
        meetingId: 'abcdef'
    },
    subscribeToPubNub: true
}).then(() => {
    librct.conference().kickParticipant('participantId');
});
```
