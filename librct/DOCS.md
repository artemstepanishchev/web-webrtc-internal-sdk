# RCT Web Client API

## Classes

<dl>
<dt><a href="#Bridge">Bridge</a></dt>
<dd></dd>
<dt><a href="#Call">Call</a></dt>
<dd></dd>
<dt><a href="#Callout">Callout</a></dt>
<dd></dd>
<dt><a href="#CancelablePromise">CancelablePromise</a></dt>
<dd><p>Promise wrapeper with cancel promise parameter.
Affects all promise chain, do not returnanything on cancel</p>
</dd>
<dt><a href="#Conference">Conference</a></dt>
<dd></dd>
<dt><a href="#History">History</a></dt>
<dd></dd>
<dt><a href="#LibRCT">LibRCT</a></dt>
<dd></dd>
<dt><a href="#Participant">Participant</a></dt>
<dd></dd>
<dt><a href="#Ping">Ping</a></dt>
<dd></dd>
<dt><a href="#PlatformUserSearch">PlatformUserSearch</a></dt>
<dd></dd>
<dt><a href="#Recording">Recording</a></dt>
<dd></dd>
<dt><a href="#Session">Session</a></dt>
<dd></dd>
<dt><a href="#UserSettings">UserSettings</a></dt>
<dd></dd>
<dt><a href="#Stream">Stream</a></dt>
<dd></dd>
</dl>

## Typedefs

<dl>
<dt><a href="#BridgeParams">BridgeParams</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#InvitationParams">InvitationParams</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#CallParams">CallParams</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#TapSession">TapSession</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#StreamSubtype">StreamSubtype</a> : <code>&#x27;window&#x27;</code> | <code>&#x27;screen&#x27;</code> | <code>&#x27;display&#x27;</code> | <code>&#x27;tab&#x27;</code></dt>
<dd></dd>
<dt><a href="#TrackMeta">TrackMeta</a> : <code>object</code></dt>
<dd></dd>
<dt><a href="#UpdateStreamData">UpdateStreamData</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#JoinResponse">JoinResponse</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#ParticipantParams">ParticipantParams</a> : <code>Object</code></dt>
<dd></dd>
<dt><a href="#TrackMeta">TrackMeta</a> : <code>object</code></dt>
<dd></dd>
<dt><a href="#TracksMeta">TracksMeta</a> : <code>object</code></dt>
<dd></dd>
</dl>

<a name="Bridge"></a>

## Bridge
**Kind**: global class

* [Bridge](#Bridge)
    * [new Bridge(params, options)](#new_Bridge_new)
    * [.getCall(options)](#Bridge+getCall) ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)

<a name="new_Bridge_new"></a>

### new Bridge(params, options)
Bridge class

**Params**

- params [<code>BridgeParams</code>](#BridgeParams) - internal params
- options <code>Object</code> - options with internal methods

<a name="Bridge+getCall"></a>

### bridge.getCall(options) ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)
Get [Call](#Call) by options

**Kind**: instance method of [<code>Bridge</code>](#Bridge)
**Params**

- options [<code>CallParams</code>](#CallParams)

**Example**
```js
bridge.getCall().then(call => console.log(call))
```
<a name="Call"></a>

## Call
**Kind**: global class

* [Call](#Call)
    * [new Call(params, options)](#new_Call_new)
    * [.url()](#Call+url) ⇒ <code>String</code>
    * [.hasChannelInfo()](#Call+hasChannelInfo) ⇒ <code>Boolean</code>
    * [.getChannel()](#Call+getChannel) ⇒ <code>Promise.&lt;Object&gt;</code>
    * [.join()](#Call+join) ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)
    * [.leave(participantId)](#Call+leave) ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)
    * [.createParticipant(options)](#Call+createParticipant) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
    * [.getParticipant(options)](#Call+getParticipant) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
    * [.muteAll(allowUnmute)](#Call+muteAll) ⇒ <code>Promise</code>
    * [.unmuteAll()](#Call+unmuteAll) ⇒ <code>Promise</code>
    * [.allowScreenSharing()](#Call+allowScreenSharing) ⇒ <code>Promise</code>
    * [.disallowScreenSharing()](#Call+disallowScreenSharing) ⇒ <code>Promise</code>
    * [.allowAnnotations()](#Call+allowAnnotations) ⇒ <code>Promise</code>
    * [.disallowAnnotations()](#Call+disallowAnnotations) ⇒ <code>Promise</code>
    * [.allowJoin()](#Call+allowJoin) ⇒ <code>Promise</code>
    * [.disallowJoin()](#Call+disallowJoin) ⇒ <code>Promise</code>
    * [.getRecording(options)](#Call+getRecording) ⇒ [<code>Promise.&lt;Recording&gt;</code>](#Recording)
    * [.startRecording()](#Call+startRecording) ⇒ [<code>Promise.&lt;Recording&gt;</code>](#Recording)

<a name="new_Call_new"></a>

### new Call(params, options)
Call class

**Params**

- params [<code>CallParams</code>](#CallParams)
- options <code>Object</code> - internal params and methods

<a name="Call+url"></a>

### call.url() ⇒ <code>String</code>
Get call url

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
console.log(call.url());
```
<a name="Call+hasChannelInfo"></a>

### call.hasChannelInfo() ⇒ <code>Boolean</code>
Check for channel ignoreBeforeUnload

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
console.log(call.hasChannelInfo());
```
<a name="Call+getChannel"></a>

### call.getChannel() ⇒ <code>Promise.&lt;Object&gt;</code>
Get channel

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.getChannel().then(channel => console.log(channel));
```
<a name="Call+join"></a>

### call.join() ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)
Subscribe to call PubNub channel

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.join().then(() => console.log('subscribed'));
```
<a name="Call+leave"></a>

### call.leave(participantId) ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)
Leave conference (unsubscribe and delete session)

**Kind**: instance method of [<code>Call</code>](#Call)
**Params**

- participantId <code>String</code> - id of participant

**Example**
```js
call.leave().then(() => console.log('done'));
```
<a name="Call+createParticipant"></a>

### call.createParticipant(options) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
Create [Participant](#Participant)

**Kind**: instance method of [<code>Call</code>](#Call)
**See**: Participant
**Params**

- options <code>Object</code>
    - .displayName <code>String</code> - participant display name
    - .profileImage <code>String</code> - participant avatar

**Example**
```js
call.createParticipant({
  displayName: 'myUser',
  profileImage: 'http://example.com/some-image.png'
}).then(participant => console.log(participant));
```
<a name="Call+getParticipant"></a>

### call.getParticipant(options) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
Get [Participant](#Participant)

**Kind**: instance method of [<code>Call</code>](#Call)
**See**: Participant
**Params**

- options [<code>ParticipantParams</code>](#ParticipantParams)

<a name="Call+muteAll"></a>

### call.muteAll(allowUnmute) ⇒ <code>Promise</code>
Enable call mute (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Params**

- allowUnmute <code>boolean</code>

**Example**
```js
call.muteAll();
```
<a name="Call+unmuteAll"></a>

### call.unmuteAll() ⇒ <code>Promise</code>
Disable call mute (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.unmuteAll();
```
<a name="Call+allowScreenSharing"></a>

### call.allowScreenSharing() ⇒ <code>Promise</code>
Enable screensharing (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.allowScreenSharing();
```
<a name="Call+disallowScreenSharing"></a>

### call.disallowScreenSharing() ⇒ <code>Promise</code>
Disable screensharing (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.disallowScreenSharing();
```
<a name="Call+allowAnnotations"></a>

### call.allowAnnotations() ⇒ <code>Promise</code>
Enable annotations (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.allowAnnotations();
```
<a name="Call+disallowAnnotations"></a>

### call.disallowAnnotations() ⇒ <code>Promise</code>
Disable annotations (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.disallowAnnotations();
```
<a name="Call+allowJoin"></a>

### call.allowJoin() ⇒ <code>Promise</code>
Unlock call (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.allowJoin();
```
<a name="Call+disallowJoin"></a>

### call.disallowJoin() ⇒ <code>Promise</code>
Lock call (optimistic response)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.disallowJoin();
```
<a name="Call+getRecording"></a>

### call.getRecording(options) ⇒ [<code>Promise.&lt;Recording&gt;</code>](#Recording)
Get recording

**Kind**: instance method of [<code>Call</code>](#Call)
**Params**

- options <code>Object</code>
    - .id <code>String</code> - recording id

**Example**
```js
call.getRecording({ Id: 'recordngId' })
  .then(recording => console.log(recording));
```
<a name="Call+startRecording"></a>

### call.startRecording() ⇒ [<code>Promise.&lt;Recording&gt;</code>](#Recording)
Start recording (*requires moderator role*)

**Kind**: instance method of [<code>Call</code>](#Call)
**Example**
```js
call.startRecording()
  .then(() => console.log('recording in progress'));
```
<a name="Callout"></a>

## Callout
**Kind**: global class
<a name="new_Callout_new"></a>

### new Callout(options)
Callout class to provide post message in glip functional

**Params**

- options <code>Object</code> - inner properties and methods

<a name="CancelablePromise"></a>

## CancelablePromise
Promise wrapeper with cancel promise parameter.
Affects all promise chain, do not returnanything on cancel

**Kind**: global class
<a name="new_CancelablePromise_new"></a>

### new CancelablePromise(executor, promise)
**Params**

- executor <code>function</code>
- promise <code>Promise</code> - canceler. Cancel promise on resolve/reject

<a name="Conference"></a>

## Conference
**Kind**: global class

* [Conference](#Conference)
    * [.getCall([bridgeParams], [callParams])](#Conference+getCall) ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)
    * [.updateLocalMuteState(localMute, localMuteVideo)](#Conference+updateLocalMuteState) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
    * [.getPersonalMeeting()](#Conference+getPersonalMeeting) ⇒ <code>Promise.&lt;Bridge, Error&gt;</code>
    * [.getChat([chatId])](#Conference+getChat)
    * [.createChat(call, chatParams, options)](#Conference+createChat)
    * _Bridge_
        * [.getBridgeInfo(options)](#Conference+getBridgeInfo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.createBridge(options)](#Conference+createBridge) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.updateBridge(options)](#Conference+updateBridge) ⇒ <code>Promise.&lt;Object&gt;</code>
    * _Call_
        * [.getCallInfo()](#Conference+getCallInfo) ⇒ <code>Promise.&lt;Object&gt;</code>
    * _Moderator_
        * [.endCall()](#Conference+endCall) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.muteParticipant(id)](#Conference+muteParticipant) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unmuteParticipant(id)](#Conference+unmuteParticipant) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.muteParticipantVideo(id)](#Conference+muteParticipantVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unmuteParticipantVideo(id)](#Conference+unmuteParticipantVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.provideModeratorRole(id)](#Conference+provideModeratorRole) ⇒ <code>Promise</code>
        * [.revokeModeratorRole(id)](#Conference+revokeModeratorRole) ⇒ <code>Promise</code>
        * [.muteCall(allowUnmute)](#Conference+muteCall) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unmuteCall()](#Conference+unmuteCall) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.allowScreenSharing()](#Conference+allowScreenSharing) ⇒ <code>Promise</code>
        * [.disallowScreenSharing()](#Conference+disallowScreenSharing) ⇒ <code>Promise</code>
        * [.unlockCall()](#Conference+unlockCall) ⇒ <code>Promise</code>
        * [.lockCall()](#Conference+lockCall) ⇒ <code>Promise</code>
        * [.kickParticipant(id)](#Conference+kickParticipant) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.allowAnnotations()](#Conference+allowAnnotations) ⇒ <code>Promise</code>
        * [.disallowAnnotations()](#Conference+disallowAnnotations) ⇒ <code>Promise</code>
    * _Participant_
        * [.join(args)](#Conference+join) ⇒ [<code>Promise.&lt;JoinResponse&gt;</code>](#JoinResponse)
        * [.leave()](#Conference+leave) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.createStream(type, subtype, [data])](#Conference+createStream) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
        * [.deleteStream(id)](#Conference+deleteStream) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.deleteParticipantStreamsByType(participantId, type)](#Conference+deleteParticipantStreamsByType) ⇒ <code>Promise</code>
        * [.localMute()](#Conference+localMute) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.localUnmute()](#Conference+localUnmute) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.localMuteVideo()](#Conference+localMuteVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.localUnmuteVideo()](#Conference+localUnmuteVideo) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.leaveViaBeacon()](#Conference+leaveViaBeacon) ⇒ <code>Boolean</code>
        * [.toggleParticipantRingStatus()](#Conference+toggleParticipantRingStatus) ⇒ <code>Boolean</code>
        * [.createDemand()](#Conference+createDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandControl()](#Conference+demandControl) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmute([participantToMuteId])](#Conference+demandUnmute) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmuteAll()](#Conference+demandUnmuteAll) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmuteVideo([participantToMuteId])](#Conference+demandUnmuteVideo) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.demandUnmuteVideoAll()](#Conference+demandUnmuteVideoAll) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.approveDemand()](#Conference+approveDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.rejectDemand()](#Conference+rejectDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.cancelDemand()](#Conference+cancelDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
        * [.deleteDemand()](#Conference+deleteDemand) ⇒ <code>Promise.&lt;Object, Error&gt;</code>
        * [.createChannel()](#Conference+createChannel) ⇒ <code>Promise.&lt;Channel, Error&gt;</code>
        * [.deleteChannel()](#Conference+deleteChannel) ⇒ <code>Promise.&lt;Object, Error&gt;</code>
    * _Recording_
        * [.getRecording(id)](#Conference+getRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.startRecording()](#Conference+startRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.stopRecording(id)](#Conference+stopRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.pauseRecording(id)](#Conference+pauseRecording) ⇒ <code>Promise.&lt;Object&gt;</code>
        * [.unpauseRecording(id)](#Conference+unpauseRecording) ⇒ <code>Promise.&lt;Object&gt;</code>

<a name="Conference+getCall"></a>

### conference.getCall([bridgeParams], [callParams]) ⇒ [<code>Promise.&lt;Call&gt;</code>](#Call)
get call

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- [bridgeParams] <code>Object</code> <code> = {}</code> - [Bridge](#Bridge)
- [callParams] <code>Object</code> <code> = {}}</code> - [Call](#Call)

<a name="Conference+updateLocalMuteState"></a>

### conference.updateLocalMuteState(localMute, localMuteVideo) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
update local mutes for current participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- localMute <code>Boolean</code> - mute audio
- localMuteVideo <code>Boolean</code> - mute video

<a name="Conference+getPersonalMeeting"></a>

### conference.getPersonalMeeting() ⇒ <code>Promise.&lt;Bridge, Error&gt;</code>
get personal meeting info {@lonk Bridge}

**Kind**: instance method of [<code>Conference</code>](#Conference)
**See**: Bridge
<a name="Conference+getChat"></a>

### conference.getChat([chatId])
Get chat by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- [chatId] <code>String</code>

<a name="Conference+createChat"></a>

### conference.createChat(call, chatParams, options)
Creates new chat or returns existing instance

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Params**

- call [<code>Call</code>](#Call)
- chatParams <code>Object</code>
- options <code>Object</code> - internal params and methods

<a name="Conference+getBridgeInfo"></a>

### conference.getBridgeInfo(options) ⇒ <code>Promise.&lt;Object&gt;</code>
Get [Bridge](#Bridge) attributes by options

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Bridge
**See**: [bridge](bridge)
**Params**

- options [<code>BridgeParams</code>](#BridgeParams)

**Example**
```js
librct.conference().getBridgeInfo({ id: 'bridgeId' })
  .then(json => console.log(json));
```
<a name="Conference+createBridge"></a>

### conference.createBridge(options) ⇒ <code>Promise.&lt;Object&gt;</code>
Create new [Bridge](#Bridge)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Bridge
**See**: [bridge](bridge)
**Params**

- options [<code>BridgeParams</code>](#BridgeParams)

**Example**
```js
librct.conference().createBridge()
  .then(json => console.log(json));
```
<a name="Conference+updateBridge"></a>

### conference.updateBridge(options) ⇒ <code>Promise.&lt;Object&gt;</code>
Update [Bridge](#Bridge)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Bridge
**See**: [bridge](bridge)
**Params**

- options [<code>BridgeParams</code>](#BridgeParams)

**Example**
```js
librct.conference().createBridge()
  .then(json => console.log(json));
```
<a name="Conference+getCallInfo"></a>

### conference.getCallInfo() ⇒ <code>Promise.&lt;Object&gt;</code>
Get [Call](#Call) info

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Call
**See**: [Call](#Call)
<a name="Conference+endCall"></a>

### conference.endCall() ⇒ <code>Promise.&lt;Object&gt;</code>
End call (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Moderator
**Example**
```js
librct.conference().endCall();
```
<a name="Conference+muteParticipant"></a>

### conference.muteParticipant(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Mute participant audio by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().muteParticipant('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+unmuteParticipant"></a>

### conference.unmuteParticipant(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Unmute participant audio by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().unmuteParticipant('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+muteParticipantVideo"></a>

### conference.muteParticipantVideo(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Mute participant video by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().muteParticipantVideo('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+unmuteParticipantVideo"></a>

### conference.unmuteParticipantVideo(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Unmute participant video by id

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().unmuteParticipantVideo('participantId')
  .then(participant => console.log(participant))
```
<a name="Conference+provideModeratorRole"></a>

### conference.provideModeratorRole(id) ⇒ <code>Promise</code>
Provide moderator role to participant (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().provideModeratorRole('participantId')
  .then(() => console.log('moderator role granted for participant'));
```
<a name="Conference+revokeModeratorRole"></a>

### conference.revokeModeratorRole(id) ⇒ <code>Promise</code>
Revoke moderator role from participant (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

**Example**
```js
librct.conference().revokeModeratorRole('participantId')
  .then(() => console.log('moderator role revoked from participant'));
```
<a name="Conference+muteCall"></a>

### conference.muteCall(allowUnmute) ⇒ <code>Promise.&lt;Object&gt;</code>
Enable call mute (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Moderator
**Params**

- allowUnmute <code>boolean</code> <code> = true</code>

**Example**
```js
librct.conference().muteCall();
```
<a name="Conference+unmuteCall"></a>

### conference.unmuteCall() ⇒ <code>Promise.&lt;Object&gt;</code>
Disable call mute (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Moderator
**Example**
```js
librct.conference().unmuteCall();
```
<a name="Conference+allowScreenSharing"></a>

### conference.allowScreenSharing() ⇒ <code>Promise</code>
Enable screensharing (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().allowScreenSharing();
```
<a name="Conference+disallowScreenSharing"></a>

### conference.disallowScreenSharing() ⇒ <code>Promise</code>
Disable screensharing (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().disallowScreenSharing();
```
<a name="Conference+unlockCall"></a>

### conference.unlockCall() ⇒ <code>Promise</code>
Unlock call (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().unlockCall();
```
<a name="Conference+lockCall"></a>

### conference.lockCall() ⇒ <code>Promise</code>
Lock call (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().lockCall();
```
<a name="Conference+kickParticipant"></a>

### conference.kickParticipant(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Kick participant from conference (*requires moderator role*)
This method will remove all devises of participant from conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Moderator
**Params**

- id <code>String</code> - participant id

<a name="Conference+allowAnnotations"></a>

### conference.allowAnnotations() ⇒ <code>Promise</code>
Enable annotations (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().allowAnnotations();
```
<a name="Conference+disallowAnnotations"></a>

### conference.disallowAnnotations() ⇒ <code>Promise</code>
Enable annotations (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Moderator
**Example**
```js
librct.conference().disallowAnnotations();
```
<a name="Conference+join"></a>

### conference.join(args) ⇒ [<code>Promise.&lt;JoinResponse&gt;</code>](#JoinResponse)
Join to the conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**Params**

- args <code>Object</code> - join args
    - .call [<code>Call</code>](#Call) - [Call](#Call)  - if specified call and bridge will not be requested
    - .bridgeParams <code>Object</code> - [Bridge](#Bridge) - required with shortId for recovery
    - .sessionParams <code>Object</code> - [Session](#Session)
    - .callParams <code>Object</code> - [Call](#Call)
    - .participantParams <code>Object</code> - [Participant](#Participant)
    - .streamParams <code>Object</code> - [Stream](#Stream)
                                    (no stream will be created if not defined)
    - .prefetchCall <code>Boolean</code> - prefetch call data
    - .subscribeToPubNub <code>Boolean</code> - subscribe to PN channel
    - .cache <code>Boolean</code> - set to false if You want to work with internal classes

**Example**
```js
librct.join({ bridgeParams: { shortId: 'myMeeting' }}).then(result => console.log(result))
```
<a name="Conference+leave"></a>

### conference.leave() ⇒ <code>Promise.&lt;Object&gt;</code>
Leave conference (unsubscribe and delete session)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - call JSON
**Category**: Participant
**Example**
```js
librct.conference().leave().then(() => console.log('done'));
```
<a name="Conference+createStream"></a>

### conference.createStream(type, subtype, [data]) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
Create [Stream](#Stream) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Stream
**Params**

- type <code>string</code> - stream type
- subtype [<code>StreamSubtype</code>](#StreamSubtype) - stream type
- [data] [<code>UpdateStreamData</code>](#UpdateStreamData) - -

**Example**
```js
participant.createStream({ type: 'video/main' })
  .then(stream => console.log(stream));
```
<a name="Conference+deleteStream"></a>

### conference.deleteStream(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Delete [Stream](#Stream) for local participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - stream JSON
**Category**: Participant
**Params**

- id <code>String</code> - stream id

**Example**
```js
participant.deleteStream('streamId')
  .then(stream => console.log(stream));
```
<a name="Conference+deleteParticipantStreamsByType"></a>

### conference.deleteParticipantStreamsByType(participantId, type) ⇒ <code>Promise</code>
Delete [Stream](#Stream) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**Params**

- participantId <code>String</code> - participant id
- type <code>String</code> - stream type

**Example**
```js
participant.deleteParticipantStreamsByType('participantId', 'video/screensharing');
```
<a name="Conference+localMute"></a>

### conference.localMute() ⇒ <code>Promise.&lt;Object&gt;</code>
Local mute audio

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localMute();
```
<a name="Conference+localUnmute"></a>

### conference.localUnmute() ⇒ <code>Promise.&lt;Object&gt;</code>
Local unmute audio

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localUnmute();
```
<a name="Conference+localMuteVideo"></a>

### conference.localMuteVideo() ⇒ <code>Promise.&lt;Object&gt;</code>
Local mute video

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localMuteVideo();
```
<a name="Conference+localUnmuteVideo"></a>

### conference.localUnmuteVideo() ⇒ <code>Promise.&lt;Object&gt;</code>
Local unmute video

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - participant JSON
**Category**: Participant
**Example**
```js
librct.conference().localUnmuteVideo();
```
<a name="Conference+leaveViaBeacon"></a>

### conference.leaveViaBeacon() ⇒ <code>Boolean</code>
Delete current participant by sync request

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
<a name="Conference+toggleParticipantRingStatus"></a>

### conference.toggleParticipantRingStatus() ⇒ <code>Boolean</code>
Performing redial either hangup for given participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
<a name="Conference+createDemand"></a>

### conference.createDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .type <code>Object</code> - demanding type
    - .resource <code>Object</code> - demanding resource
    - .grant <code>Object</code> - demanding permissions on resource

<a name="Conference+demandControl"></a>

### conference.demandControl() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create remote control [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .resource <code>Object</code> - demanding resource
    - .grant <code>Object</code> - demanding permissions on resource

<a name="Conference+demandUnmute"></a>

### conference.demandUnmute([participantToMuteId]) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

- [participantToMuteId] <code>String</code> - participant to be muted

<a name="Conference+demandUnmuteAll"></a>

### conference.demandUnmuteAll() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute all [Demand](Demand) for conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
<a name="Conference+demandUnmuteVideo"></a>

### conference.demandUnmuteVideo([participantToMuteId]) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute video [Demand](Demand) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

- [participantToMuteId] <code>String</code> - participant to be muted

<a name="Conference+demandUnmuteVideoAll"></a>

### conference.demandUnmuteVideoAll() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute all [Demand](Demand) for conference

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
<a name="Conference+approveDemand"></a>

### conference.approveDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Aproves [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .participantId <code>String</code> - who's demanding
    - .demandId <code>String</code>

<a name="Conference+rejectDemand"></a>

### conference.rejectDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Rejects [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .participantId <code>String</code> - who's demanding
    - .demandId <code>String</code>

<a name="Conference+cancelDemand"></a>

### conference.cancelDemand() ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Cancels [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .demandId <code>String</code>

<a name="Conference+deleteDemand"></a>

### conference.deleteDemand() ⇒ <code>Promise.&lt;Object, Error&gt;</code>
Deletes [Demand](Demand)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Demand
**Params**

    - .participantId <code>String</code>
    - .demandId <code>String</code>

<a name="Conference+createChannel"></a>

### conference.createChannel() ⇒ <code>Promise.&lt;Channel, Error&gt;</code>
Create [Channel](Channel) for participant

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Channel
**Params**

    - .type <code>String</code>
    - .direction <code>String</code>
    - .info <code>Object</code>

<a name="Conference+deleteChannel"></a>

### conference.deleteChannel() ⇒ <code>Promise.&lt;Object, Error&gt;</code>
Deletes [Channel](Channel)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Participant
**See**: Channel
**Params**

    - .channelId <code>String</code>

<a name="Conference+getRecording"></a>

### conference.getRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Get recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - recording JSON
**Category**: Recording
**Params**

- id <code>Number</code> - recording id

**Example**
```js
librct.conference().getRecording(id)
  .then(recording => console.log(`recordingId: ${recording.id}`))
```
<a name="Conference+startRecording"></a>

### conference.startRecording() ⇒ <code>Promise.&lt;Object&gt;</code>
Start recording (*requires moderator role*)

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Returns**: <code>Promise.&lt;Object&gt;</code> - recording JSON
**Category**: Recording
**Example**
```js
librct.conference().startRecording()
  .then(recording => console.log(`recordingId: ${recording.recordingId}`))
```
<a name="Conference+stopRecording"></a>

### conference.stopRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Stop recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Recording
**Params**

- id <code>String</code> - recording id

**Example**
```js
librct.conference().stopRecording('recordingId')
  .then(() => console.log('recording stopped'));
```
<a name="Conference+pauseRecording"></a>

### conference.pauseRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Pause recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Recording
**Params**

- id <code>String</code> - recording id

**Example**
```js
librct.conference().pauseRecording('recordingId')
  .then(() => console.log('recording paused'));
```
<a name="Conference+unpauseRecording"></a>

### conference.unpauseRecording(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Unpause recording

**Kind**: instance method of [<code>Conference</code>](#Conference)
**Category**: Recording
**Params**

- id <code>String</code> - recording id

**Example**
```js
librct.conference().unpauseRecording('recordingId')
  .then(() => console.log('recording paused'));
```
<a name="History"></a>

## History
**Kind**: global class

* [History](#History)
    * [new History(options)](#new_History_new)
    * [.url()](#History+url) ⇒ <code>String</code>
    * [.getMeetings(params)](#History+getMeetings) ⇒ <code>Promise.&lt;Object&gt;</code>
    * [.getMeeting(id)](#History+getMeeting) ⇒ <code>Promise.&lt;Object&gt;</code>
    * [.getShares(meetingId, recordingId)](#History+getShares) ⇒ <code>Promise.&lt;Array&gt;</code>
    * [.deleteRecording(id)](#History+deleteRecording) ⇒ <code>Promise</code>
    * [.shareRecording(meetingId, recording, accountId, extensionId)](#History+shareRecording) ⇒ <code>Promise</code>
    * [.unshareRecording(meetingId, recordingId, shareId)](#History+unshareRecording) ⇒ <code>Promise</code>
    * [.trashRecording(meetingId, recordingId)](#History+trashRecording) ⇒ <code>Promise</code>
    * [.restoreRecording(meetingId, recordingId)](#History+restoreRecording) ⇒ <code>Promise</code>

<a name="new_History_new"></a>

### new History(options)
History class to work with CHS

**Params**

- options <code>Object</code> - inner properties and methods

<a name="History+url"></a>

### history.url() ⇒ <code>String</code>
Get history url

**Kind**: instance method of [<code>History</code>](#History)
<a name="History+getMeetings"></a>

### history.getMeetings(params) ⇒ <code>Promise.&lt;Object&gt;</code>
Get meetings

**Kind**: instance method of [<code>History</code>](#History)
**Returns**: <code>Promise.&lt;Object&gt;</code> - response with list of recordings
**Params**

- params <code>Object</code>
    - .type <code>String</code> - ['All', 'My', 'Shared', 'Deleted', null]
    - .perPage <code>Number</code> - page limit
    - .text <code>String</code> - filter
    - .offsetToken <code>String</code> - offset token (for next page)

**Example**
```js
librct.history().getMeetings()
  .then(result => console.log(result));
```
<a name="History+getMeeting"></a>

### history.getMeeting(id) ⇒ <code>Promise.&lt;Object&gt;</code>
Get meeting by id

**Kind**: instance method of [<code>History</code>](#History)
**Params**

- id <code>string</code> - recording id

**Example**
```js
librct.history().getMeeting('recordingId')
  .then(result => console.log(resutl));
```
<a name="History+getShares"></a>

### history.getShares(meetingId, recordingId) ⇒ <code>Promise.&lt;Array&gt;</code>
Get list of recording shareWithExtension

**Kind**: instance method of [<code>History</code>](#History)
**Returns**: <code>Promise.&lt;Array&gt;</code> - list of shares
**Params**

- meetingId <code>String</code> - meeting id
- recordingId <code>String</code> - recording id

**Example**
```js
librct.history().getShares('meetingId', 'recordingId')
  .then(result => console.log(resutl));
```
<a name="History+deleteRecording"></a>

### history.deleteRecording(id) ⇒ <code>Promise</code>
Delete recording

**Kind**: instance method of [<code>History</code>](#History)
**Params**

- id <code>String</code> - recording id

**Example**
```js
librct.history().deleteRecording('meetingId', 'recordingId')
  .then(result => console.log('recording deleted'));
```
<a name="History+shareRecording"></a>

### history.shareRecording(meetingId, recording, accountId, extensionId) ⇒ <code>Promise</code>
Share recording

**Kind**: instance method of [<code>History</code>](#History)
**Params**

- meetingId <code>String</code> - meeting id
- recording <code>String</code> - recording id
- accountId <code>String</code> - target account id
- extensionId <code>String</code> - target extension id

**Example**
```js
librct.history().shareRecording('meetingId', 'recordingId', 'accountId', 'extensionId')
  .then(result => console.log('shared'));
```
<a name="History+unshareRecording"></a>

### history.unshareRecording(meetingId, recordingId, shareId) ⇒ <code>Promise</code>
Unshare recording

**Kind**: instance method of [<code>History</code>](#History)
**Params**

- meetingId <code>String</code> - meeting id
- recordingId <code>String</code> - recording id
- shareId <code>String</code> - share id

**Example**
```js
librct.history().unshareRecording('meetingId', 'recordingId', 'shareId')
  .then(result => console.log('unshared'));
```
<a name="History+trashRecording"></a>

### history.trashRecording(meetingId, recordingId) ⇒ <code>Promise</code>
Move recording to trash

**Kind**: instance method of [<code>History</code>](#History)
**Params**

- meetingId <code>String</code> - meeting id
- recordingId <code>String</code> - recording id

**Example**
```js
librct.history().trashRecording('meetingId', 'recordingId')
  .then(result => console.log('trashed'));
```
<a name="History+restoreRecording"></a>

### history.restoreRecording(meetingId, recordingId) ⇒ <code>Promise</code>
Restore recording

**Kind**: instance method of [<code>History</code>](#History)
**Params**

- meetingId <code>String</code> - meeting id
- recordingId <code>String</code> - recording id

**Example**
```js
librct.history().restoreRecording('meetingId', 'recordingId')
  .then(result => console.log('restored'));
```
<a name="LibRCT"></a>

## LibRCT
**Kind**: global class

* [LibRCT](#LibRCT)
    * [new LibRCT(options)](#new_LibRCT_new)
    * [.conference()](#LibRCT+conference) ⇒ [<code>Conference</code>](#Conference)

<a name="new_LibRCT_new"></a>

### new LibRCT(options)
Create LibRCT client

**Params**

- options
    - .fetch - function to make requests. Default:
                               <code>fetch</code> from
                               [isomorphic-fetch](https://github.com/matthew-andrews/isomorphic-fetch)
    - .baseUrl - base url for all requests. Default: <code>/flock/v1</code>
    - .subscribeKey - subscribe key for PubNub
    - .sendBeacon - function to send sync request

<a name="LibRCT+conference"></a>

### libRCT.conference() ⇒ [<code>Conference</code>](#Conference)
**Kind**: instance method of [<code>LibRCT</code>](#LibRCT)
**Returns**: [<code>Conference</code>](#Conference) - -
<a name="Participant"></a>

## Participant
**Kind**: global class

* [Participant](#Participant)
    * [new Participant(params, options)](#new_Participant_new)
    * [.url()](#Participant+url) ⇒ <code>string</code>
    * [.getLocalSession()](#Participant+getLocalSession)
    * [.deleteViaBeacon()](#Participant+deleteViaBeacon) ⇒ <code>Boolean</code>
    * [.deleteLocalSessions()](#Participant+deleteLocalSessions) ⇒ <code>Promise</code>
    * [.updateLocalMuteState(localMute, localMuteVideo)](#Participant+updateLocalMuteState) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
    * [.localMute()](#Participant+localMute) ⇒ <code>Promise</code>
    * [.localUnmute()](#Participant+localUnmute) ⇒ <code>Promise</code>
    * [.localMuteVideo()](#Participant+localMuteVideo) ⇒ <code>Promise</code>
    * [.localUnmuteVideo()](#Participant+localUnmuteVideo) ⇒ <code>Promise</code>
    * [.mute()](#Participant+mute) ⇒ <code>Promise</code>
    * [.unmute()](#Participant+unmute) ⇒ <code>Promise</code>
    * [.muteVideo()](#Participant+muteVideo) ⇒ <code>Promise</code>
    * [.unmuteVideo()](#Participant+unmuteVideo) ⇒ <code>Promise</code>
    * [.provideModeratorRole()](#Participant+provideModeratorRole) ⇒ <code>Promise</code>
    * [.revokeModeratorRole()](#Participant+revokeModeratorRole) ⇒ <code>Promise</code>
    * [.getStream(streamId)](#Participant+getStream) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
    * [.createStream(options)](#Participant+createStream) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
    * [.createSession()](#Participant+createSession) ⇒ <code>Promise.&lt;Session, Error&gt;</code>
    * [.deleteStream(streamId)](#Participant+deleteStream) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
    * [.getDemand(demandId)](#Participant+getDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
    * [.createDemand(options)](#Participant+createDemand) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
    * [.demandControl(options)](#Participant+demandControl) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
    * [.demandUnmute(participantToMuteId)](#Participant+demandUnmute) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
    * [.demandUnmuteVideo(participantToMuteId)](#Participant+demandUnmuteVideo) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
    * [.offerControl(options)](#Participant+offerControl) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
    * [.createChannel(options)](#Participant+createChannel) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
    * [.getChannel(channelId)](#Participant+getChannel) ⇒ <code>Promise.&lt;channel, Error&gt;</code>

<a name="new_Participant_new"></a>

### new Participant(params, options)
Participant class

**Params**

- params <code>Object</code> - internal params (defined automatically)
- options <code>Object</code>

<a name="Participant+url"></a>

### participant.url() ⇒ <code>string</code>
Get participant url

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
console.log(participant.url())
```
<a name="Participant+getLocalSession"></a>

### participant.getLocalSession()
temporary workaround

**Kind**: instance method of [<code>Participant</code>](#Participant)
<a name="Participant+deleteViaBeacon"></a>

### participant.deleteViaBeacon() ⇒ <code>Boolean</code>
Send sync delete request. Usable for delete participant on page close

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Returns**: <code>Boolean</code> - true if all is OK
**Example**
```js
participant.deleteViaBeacon(); // true
```
<a name="Participant+deleteLocalSessions"></a>

### participant.deleteLocalSessions() ⇒ <code>Promise</code>
Delete local sessions for participant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.deleteLocalSessions()
  .then(() => console.log('deleted'));
```
<a name="Participant+updateLocalMuteState"></a>

### participant.updateLocalMuteState(localMute, localMuteVideo) ⇒ [<code>Promise.&lt;Participant&gt;</code>](#Participant)
update local mutes in single request

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Params**

- localMute <code>Boolean</code> - mute audio
- localMuteVideo <code>Boolean</code> - mute video

<a name="Participant+localMute"></a>

### participant.localMute() ⇒ <code>Promise</code>
local mute participant (only for current participant). Optimistic response

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.localMute();
```
<a name="Participant+localUnmute"></a>

### participant.localUnmute() ⇒ <code>Promise</code>
local unmute participant (only for current participant). Optimistic response

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.localUnmute();
```
<a name="Participant+localMuteVideo"></a>

### participant.localMuteVideo() ⇒ <code>Promise</code>
local mute participant video (only for current participant). Optimistic response

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.localMuteVideo();
```
<a name="Participant+localUnmuteVideo"></a>

### participant.localUnmuteVideo() ⇒ <code>Promise</code>
local unmute participant video (only for current participant). Optimistic response

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.localUnmuteVideo();
```
<a name="Participant+mute"></a>

### participant.mute() ⇒ <code>Promise</code>
Mute participant (update with optimistic response)

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.mute()
  .then(() => console.log('participant is server muted'));
```
<a name="Participant+unmute"></a>

### participant.unmute() ⇒ <code>Promise</code>
Unmute participant (update with optimistic response)

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.unmute()
  .then(() => console.log('participant is server unmuted'));
```
<a name="Participant+muteVideo"></a>

### participant.muteVideo() ⇒ <code>Promise</code>
Mute participant video (update with optimistic response)

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.muteVideo()
  .then(() => console.log('participant video is server muted'));
```
<a name="Participant+unmuteVideo"></a>

### participant.unmuteVideo() ⇒ <code>Promise</code>
Unmute participant video (update with optimistic response)

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.unmuteVideo()
  .then(() => console.log('participant video is server unmuted'));
```
<a name="Participant+provideModeratorRole"></a>

### participant.provideModeratorRole() ⇒ <code>Promise</code>
Provide moderator role to participant (update with optimistic response)

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.provideModeratorRole()
  .then(() => console.log('moderator role provided for participant'));
```
<a name="Participant+revokeModeratorRole"></a>

### participant.revokeModeratorRole() ⇒ <code>Promise</code>
Revoke moderator role from participant (update with optimistic response)

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Example**
```js
participant.revokeModeratorRole()
  .then(() => console.log('moderator role revoked from participant'));
```
<a name="Participant+getStream"></a>

### participant.getStream(streamId) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
Get participant stream by id

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Stream
**Params**

- streamId <code>String</code> - Id of participant stream

**Example**
```js
participant.getStream('streamId')
  .then(stream => console.log(stream));
```
<a name="Participant+createStream"></a>

### participant.createStream(options) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
Create [Stream](#Stream) for participant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Stream
**Params**

- options <code>Object</code>
    - .type <code>Object</code> - stream type
    - .info <code>Object</code> - stream info
    - .sessionId <code>Object</code> - sessionId
    - .sessionId <code>Object</code> - sessionId
    - [.audio] [<code>TrackMeta</code>](#TrackMeta) - -
    - [.video] [<code>TrackMeta</code>](#TrackMeta) - -

**Example**
```js
participant.createStream({ type: 'video/main' })
  .then(stream => console.log(stream));
```
<a name="Participant+createSession"></a>

### participant.createSession() ⇒ <code>Promise.&lt;Session, Error&gt;</code>
Create [Session](#Session) for paricipant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Session
**Example**
```js
participant.createSession()
  .then(session => console.log(session));
```
<a name="Participant+deleteStream"></a>

### participant.deleteStream(streamId) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
Delete [Stream](#Stream) for participant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**Params**

- streamId <code>String</code> - id of participant stream

**Example**
```js
participant.deleteStream('streamId')
  .then(() => console.log('stream deleted'))
```
<a name="Participant+getDemand"></a>

### participant.getDemand(demandId) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Get participant's demand by id

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Stream
**Params**

- demandId <code>String</code> - Id of participant's demand

**Example**
```js
participant.getDemand('demandId')
  .then(demand => console.log(demand));
```
<a name="Participant+createDemand"></a>

### participant.createDemand(options) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create [Demand](Demand) for participant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Demand
**Params**

- options <code>Object</code>
    - .type <code>Object</code> - demanding type
    - .resource <code>Object</code> - demanding resource
    - .grant <code>Object</code> - demanding permissions on resource

<a name="Participant+demandControl"></a>

### participant.demandControl(options) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create remote control [Demand](Demand) for participant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Demand
**Params**

- options <code>Object</code>
    - .resource <code>Object</code> - demanding resource
    - .grant <code>Object</code> - demanding permissions on resource

<a name="Participant+demandUnmute"></a>

### participant.demandUnmute(participantToMuteId) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute [Demand](Demand) for participant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Demand
**Params**

- participantToMuteId <code>String</code> - participant to be muted

<a name="Participant+demandUnmuteVideo"></a>

### participant.demandUnmuteVideo(participantToMuteId) ⇒ <code>Promise.&lt;Demand, Error&gt;</code>
Create unmute video [Demand](Demand) for participant

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Demand
**Params**

- participantToMuteId <code>String</code> - participant to be muted

<a name="Participant+offerControl"></a>

### participant.offerControl(options) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
Create [Demand](Demand) by presenter

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Demand
**Params**

- options <code>Object</code>
    - .participantId <code>Object</code> - presenter id
    - .streamId <code>Object</code> - SS stream id
    - .sessionId <code>Object</code> - -

**Example**
```js
participant.createStream({ type: 'video/main' })
  .then(stream => console.log(stream));
```
<a name="Participant+createChannel"></a>

### participant.createChannel(options) ⇒ <code>Promise.&lt;Stream, Error&gt;</code>
Create a [Channel](Channel)

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: Channel
**Params**

- options <code>Object</code>
    - .resource <code>Object</code> - demanding resource
    - .grant <code>Object</code> - demanding permissions on resource

<a name="Participant+getChannel"></a>

### participant.getChannel(channelId) ⇒ <code>Promise.&lt;channel, Error&gt;</code>
Get participant channel by id

**Kind**: instance method of [<code>Participant</code>](#Participant)
**See**: channel
**Params**

- channelId <code>String</code> - Id of participant channel

**Example**
```js
participant.getChannel('channelId')
  .then(channel => console.log(channel));
```
<a name="Ping"></a>

## Ping
**Kind**: global class

* [Ping](#Ping)
    * [new Ping(options)](#new_Ping_new)
    * [.touch()](#Ping+touch)
    * [.setTimeout(timeout)](#Ping+setTimeout)
    * [.clearTimeout()](#Ping+clearTimeout)
    * [.emitTimeout()](#Ping+emitTimeout)

<a name="new_Ping_new"></a>

### new Ping(options)
Ping class

**Params**

- options <code>Object</code> - inner properties and methods

<a name="Ping+touch"></a>

### ping.touch()
Touch ping to reset timeout

**Kind**: instance method of [<code>Ping</code>](#Ping)
**Example**
```js
ping.touch();
```
<a name="Ping+setTimeout"></a>

### ping.setTimeout(timeout)
Set ping timeout

**Kind**: instance method of [<code>Ping</code>](#Ping)
**Params**

- timeout <code>Number</code> - timeout in ms

**Example**
```js
ping.setTimeout(50); // 50ms
```
<a name="Ping+clearTimeout"></a>

### ping.clearTimeout()
Clear ping timeout

**Kind**: instance method of [<code>Ping</code>](#Ping)
**Example**
```js
ping.clearTimeout();
```
<a name="Ping+emitTimeout"></a>

### ping.emitTimeout()
Emit timeout events

**Kind**: instance method of [<code>Ping</code>](#Ping)
**Example**
```js
ping.emitTimeout();
```
<a name="PlatformUserSearch"></a>

## PlatformUserSearch
**Kind**: global class
<a name="new_PlatformUserSearch_new"></a>

### new PlatformUserSearch(options)
PlatformUserSearch class to work with platform fulltext user search

**Params**

- options <code>Object</code> - inner properties and methods

<a name="Recording"></a>

## Recording
**Kind**: global class

* [Recording](#Recording)
    * [new Recording(params, options)](#new_Recording_new)
    * [.url()](#Recording+url) ⇒ <code>string</code>
    * [.pause()](#Recording+pause) ⇒ <code>Promise</code>
    * [.unpause()](#Recording+unpause) ⇒ <code>Promise</code>
    * [.stop()](#Recording+stop) ⇒ <code>Promise</code>

<a name="new_Recording_new"></a>

### new Recording(params, options)
Recording class

**Params**

- params <code>Object</code>
- options <code>Object</code> - inner properties and methods

<a name="Recording+url"></a>

### recording.url() ⇒ <code>string</code>
Get recording url

**Kind**: instance method of [<code>Recording</code>](#Recording)
**Example**
```js
console.log(recording.url());
```
<a name="Recording+pause"></a>

### recording.pause() ⇒ <code>Promise</code>
Pause recording

**Kind**: instance method of [<code>Recording</code>](#Recording)
**Returns**: <code>Promise</code> - - update with optimistic response
<a name="Recording+unpause"></a>

### recording.unpause() ⇒ <code>Promise</code>
Unpause recording with unpause event.
This method have to force push 'pause End' event to record events.
Otherwise user will wait for event from BE during timer count.
That will cause unnecessary amendments on call initiator side.

**Kind**: instance method of [<code>Recording</code>](#Recording)
**Returns**: <code>Promise</code> - - update with optimistic response
<a name="Recording+stop"></a>

### recording.stop() ⇒ <code>Promise</code>
Stop recording

**Kind**: instance method of [<code>Recording</code>](#Recording)
**Returns**: <code>Promise</code> - - update with optimistic response

<a name="Session"></a>

## Session
**Kind**: global class

* [Session](#Session)
    * [new Session(params, options)](#new_Session_new)
    * [.url()](#Session+url) ⇒ <code>String</code>
    * [.startPong(interval)](#Session+startPong)
    * [.stopPong()](#Session+stopPong)
    * [.pong()](#Session+pong) ⇒ <code>Promise</code>
    * [.delete()](#Session+delete) ⇒ <code>Promise</code>
    * [.localMute()](#Session+localMute) ⇒ <code>Promise</code>
    * [.localUnmute()](#Session+localUnmute) ⇒ <code>Promise</code>
    * [.localMuteVideo()](#Session+localMuteVideo) ⇒ <code>Promise</code>
    * [.localUnmuteVideo()](#Session+localUnmuteVideo) ⇒ <code>Promise</code>
    * [.mute()](#Session+mute) ⇒ <code>Promise</code>
    * [.unmute()](#Session+unmute) ⇒ <code>Promise</code>
    * [.muteVideo()](#Session+muteVideo) ⇒ <code>Promise</code>
    * [.unmuteVideo()](#Session+unmuteVideo) ⇒ <code>Promise</code>

<a name="new_Session_new"></a>

### new Session(params, options)
Session class

**Params**

- params <code>Object</code>
- options <code>Object</code> - inner properties and methods

<a name="Session+url"></a>

### session.url() ⇒ <code>String</code>
Get session url

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
console.log(session.url());
```
<a name="Session+startPong"></a>

### session.startPong(interval)
Start pong for session
Should be called for each session created by <code>session.create()</code>

**Kind**: instance method of [<code>Session</code>](#Session)
**Params**

- interval <code>Number</code> - overrides default interval

**Example**
```js
session.startPong()
```
<a name="Session+stopPong"></a>

### session.stopPong()
Stop pong for sessionId
Should be called exactly for same object as <code>session.startPong</code>

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.stopPong();
```
<a name="Session+pong"></a>

### session.pong() ⇒ <code>Promise</code>
Pong method

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.pong();
```
<a name="Session+delete"></a>

### session.delete() ⇒ <code>Promise</code>
Stop pong and delete session

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.delete()
  .then(() => console.log('deleted'));
```
<a name="Session+localMute"></a>

### session.localMute() ⇒ <code>Promise</code>
Local mute session (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.localMute()
  .then(() => console.log('local muted'));
```
<a name="Session+localUnmute"></a>

### session.localUnmute() ⇒ <code>Promise</code>
Local unmute session (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.localUnmute()
  .then(() => console.log('local unmuted'));
```
<a name="Session+localMuteVideo"></a>

### session.localMuteVideo() ⇒ <code>Promise</code>
Local mute session video (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.localMuteVideo()
  .then(() => console.log('local muted'));
```
<a name="Session+localUnmuteVideo"></a>

### session.localUnmuteVideo() ⇒ <code>Promise</code>
Local unmute session video (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.localUnmuteVideo()
  .then(() => console.log('local unmuted'));
```
<a name="Session+mute"></a>

### session.mute() ⇒ <code>Promise</code>
Mute session (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.mute()
  .then(() => console.log('muted'));
```
<a name="Session+unmute"></a>

### session.unmute() ⇒ <code>Promise</code>
Unmute session (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.unmute()
  .then(() => console.log('unmuted'));
```
<a name="Session+muteVideo"></a>

### session.muteVideo() ⇒ <code>Promise</code>
Mute session video (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.muteVideo()
  .then(() => console.log('video muted'));
```
<a name="Session+unmuteVideo"></a>

### session.unmuteVideo() ⇒ <code>Promise</code>
Unmute session video (update with optimistic response)

**Kind**: instance method of [<code>Session</code>](#Session)
**Example**
```js
session.unmuteVideo()
  .then(() => console.log('video unmuted'));
```
<a name="UserSettings"></a>

## UserSettings
**Kind**: global class
<a name="new_UserSettings_new"></a>

### new UserSettings(options)
UserSettings class to work with USS

**Params**

- options <code>Object</code> - inner properties and methods

<a name="Stream"></a>

## Stream
**Kind**: global class

* [Stream](#Stream)
    * [new Stream(params, options)](#new_Stream_new)
    * [.url()](#Stream+url) ⇒ <code>String</code>

<a name="new_Stream_new"></a>

### new Stream(params, options)
Stream class

**Params**

- params <code>Object</code>
- options <code>Object</code> - inner properties and methods

<a name="Stream+url"></a>

### stream.url() ⇒ <code>String</code>
Get stream url

**Kind**: instance method of [<code>Stream</code>](#Stream)
**Example**
```js
console.log(stream.url());
```
<a name="BridgeParams"></a>

## BridgeParams : <code>Object</code>
**Kind**: global typedef
**Params**

- id <code>String</code> - can be used alone
- shortId <code>String</code> - can be used alone
- phoneNumber <code>String</code> - should be used with <options.code>
- code <code>String</code> - should be used with <options.phoneNumber>

<a name="Numbers"></a>

## Numbers : <code>Object</code>
**Kind**: global typedef
**Params**

- number <code>String</code>
- country <code>String</code>
- location <code>String</code>

<a name="NumbersList"></a>

## NumbersList : <code>Object</code>
**Kind**: global typedef
**Params**

- list [<code>Array.&lt;Numbers&gt;</code>](#Numbers) - -

<a name="InvitationParams"></a>

## InvitationParams : <code>Object</code>
**Kind**: global typedef
**Params**

- hostName <code>String</code> - host name
- meetingName <code>String</code> - meeting name
- meetingId <code>String</code> - meeting id
- meetingUrl <code>String</code> - meeting url
- internationalNumbersUrl <code>String</code> - international number
- mainPhoneNumber <code>String</code> - main phone number (deprecated, use numbers instead)
- numbers [<code>NumbersList</code>](#NumbersList) - phone numbers

<a name="CallParams"></a>

## CallParams : <code>Object</code>
**Kind**: global typedef
**Params**

- id <code>String</code> - call id
- channel <code>String</code> - call channel

<a name="TapSession"></a>

## TapSession : <code>Object</code>
**Kind**: global typedef
**Properties**

- id <code>String</code>
- notificationToken <code>String</code> - token for subscribe to PubNub channel
- token <code>String</code> - token for create session request on VAS

<a name="StreamSubtype"></a>

## StreamSubtype : <code>&#x27;window&#x27;</code> \| <code>&#x27;screen&#x27;</code> \| <code>&#x27;display&#x27;</code> \| <code>&#x27;tab&#x27;</code>
**Kind**: global typedef
<a name="TrackMeta"></a>

## TrackMeta : <code>object</code>
**Kind**: global typedef
**Properties**

- isActiveIn <code>boolean</code> - -
- isActiveOut <code>boolean</code> - -

<a name="UpdateStreamData"></a>

## UpdateStreamData : <code>Object</code>
**Kind**: global typedef
**Properties**

- audio [<code>TrackMeta</code>](#TrackMeta) - -
- video [<code>TrackMeta</code>](#TrackMeta) - -

<a name="JoinResponse"></a>

## JoinResponse : <code>Object</code>
**Kind**: global typedef
**Properties**

-  [<code>Call</code>](#Call) - [Call](#Call) plain object
-  [<code>Participant</code>](#Participant) - Current [Participant](#Participant) plain object
-  [<code>Session</code>](#Session) - Created [Session](#Session) plain object
-  [<code>Stream</code>](#Stream) - Created [Stream](#Stream) plain object

<a name="ParticipantParams"></a>

## ParticipantParams : <code>Object</code>
**Kind**: global typedef
**Params**

- id <code>String</code> - participant id
- localMute <code>Boolean</code> - local audio mute
- localMuteVideo <code>Boolean</code> - local video mute
- callerInfo <code>Object</code> - participant info
    - .displayName <code>String</code> - participant display name
    - .profileImage <code>String</code> - participant avatar

<a name="TrackMeta"></a>

## TrackMeta : <code>object</code>
**Kind**: global typedef
**Properties**

- isActiveIn <code>boolean</code> - -
- isActiveOut <code>boolean</code> - -

<a name="TracksMeta"></a>

## TracksMeta : <code>object</code>
**Kind**: global typedef
**Properties**

- audio [<code>TrackMeta</code>](#TrackMeta) - -
- video [<code>TrackMeta</code>](#TrackMeta) - -

