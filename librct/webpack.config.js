var path = require('path');
var webpack = require('webpack');

module.exports = {
    // devtool: 'source-map',
    entry: [
        './src/librct'
    ],
    output: {
        path: path.join(__dirname, 'dist-var'),
        filename: 'index.js',
        libraryTarget: 'var',
        library: 'LibRCT'
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: path.join(__dirname, 'src'),
                loader: 'babel-loader'
            }
        ]
    },
    devServer: {
        contentBase: './src'
    },
    externals: {
        'pubnub': false
    },
    resolve: {
        alias: {
            'node-fetch': path.resolve('./node_modules/whatwg-fetch/fetch.js')
        }
    }
};
