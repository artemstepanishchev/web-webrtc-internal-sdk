var Model               = require('./model');
var TransportHttp       = require('./transport').TransportHttp;
var serializeParams     = require('./utils').serializeParams;

/**
 * Get Countries List class to get list of available countries
 *
 * @param       {Object} options inner properties and methods
 * @constructor
 */
class Countries extends Model {
    constructor (options) {
        super({}, options);
        this._transport = new TransportHttp(options);

        this._modelFields = [];
    }

    url () {
        return '/dictionary/country';
    }

    getList (params = {}) {
        var query = Object.assign({}, { perPage: 1000 }, params);
        return this._transport._read(this.url() + serializeParams(query));
    }
}

module.exports = Countries;
