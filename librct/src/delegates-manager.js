const Model = require('./model');
const TransportHttp = require('./transport').TransportHttp;

class DelegatesManager extends Model {
    constructor(options) {
        super({}, options);
        this._transport = new TransportHttp(options);

        this.registerModelFields([]);
    }

    getDelegates({ accountId, extensionId }) {
        return this.apiCall(`${this.getVersionedBaseUrl()}/accounts/${accountId}/extensions/${extensionId}/delegates`, {
            method: 'GET',
        });
    }

    getDelegators({ accountId, extensionId }) {
        return this.apiCall(`${this.getVersionedBaseUrl()}/accounts/${accountId}/extensions/${extensionId}/delegators`, {
            method: 'GET',
        });
    }

    addDelegate({ accountId, extensionId, delegateExtId }) {
        return this.apiCall(`${this.getVersionedBaseUrl()}/accounts/${accountId}/extensions/${extensionId}/delegates`, {
            method: 'POST',
            body: { extensionId: delegateExtId },
        });
    }

    removeDelegate({ accountId, extensionId, delegateExtId }) {
        return this.apiCall(
            `${this.getVersionedBaseUrl()}/accounts/${accountId}/extensions/${extensionId}/delegates/${delegateExtId}`,
            {
                method: 'DELETE',
            }
        );
    }
}

module.exports = DelegatesManager;
