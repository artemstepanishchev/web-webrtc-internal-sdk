var Model = require('./model');
var TransportHttp = require('./transport').TransportHttp;

class Privileges extends Model {
    constructor(options) {
        super({}, options);
        this._transport = new TransportHttp(options);
    }

    getUrl(accountId, extensionId) {
        const baseUrl = this.getVersionedBaseUrl();
        return `${baseUrl}/account/${accountId}/extension/${extensionId}/privileges`;
    }

    get(accountId, extensionId, privilegeId) {
        return this.apiCall(`${this.getUrl(accountId, extensionId)}/${privilegeId}`);
    }

    getAll(accountId, extensionId) {
        return this.apiCall(this.getUrl(accountId, extensionId));
    }
}

module.exports = Privileges;
