
const get = require('lodash.get');

class NetworkError extends Error {
    constructor (message, response = {}) {
        super(message);

        this.response = response;
        this.status = response.status;
        this.url = get(response, 'response.url');
        this.code = get(response, 'json.error.code', response.status);
        this.headers = get(response, 'response.headers');
        this.conferenceActive = get(response, 'json.conferenceActive');
    }
}

module.exports = NetworkError;
