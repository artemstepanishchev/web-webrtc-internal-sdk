const findIndex = require('lodash.findindex');
const mergeWith = require('lodash.mergewith');
const isEqual = require('lodash.isequal');
const cloneDeep = require('lodash.clonedeep');
const isNumber = require('lodash.isnumber');
const isString = require('lodash.isstring');
const generate = require('nanoid/generate');

const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

function isDumbList(arr, arrIdKey) {
    return (
        arr.length > 0 && (isNumber(arr[0]) || isString(arr[0]) || arr[0][arrIdKey] === undefined)
    );
}

function smartMerge(...args) {
    let arrIdKey = 'id';
    if (typeof args[args.length - 1] === 'string') {
        arrIdKey = args.splice(-1)[0];
    }
    let isChanged = false;

    mergeWith(...args, (dst, src) => {
        if (Array.isArray(src)) {
            if (!Array.isArray(dst)) {
                isChanged = true;
                dst = [dst].filter(Boolean);
            }

            if (!isDumbList(dst, arrIdKey)) {
                src.forEach(srcEl => {
                    const index = findIndex(dst, { [arrIdKey]: srcEl[arrIdKey] });
                    if (index === -1) {
                        dst.push(srcEl);
                        isChanged = true;
                    } else if (typeof dst[index].toJSON === 'function') {
                        const isChanged2 = dst[index].set(srcEl.toJSON(true));
                        isChanged = isChanged || isChanged2;
                    } else {
                        const isChanged2 = smartMerge(dst[index], srcEl, arrIdKey);
                        isChanged = isChanged || isChanged2;
                    }
                });
            } else {
                isChanged = !isEqual(src, dst);
                return src;
            }

            return dst;
        } else if (src !== dst) {
            isChanged = true;
        }

        return void 0;
    });

    return isChanged;
}

function isDataPatched(data, patch) {
    const patched = cloneDeep(data);
    smartMerge(patched, patch);
    return isEqual(data, patched);
}

function serializeParams(obj) {
    const query = Object.keys(obj)
        .map(key => `${key}=${obj[key]}`)
        .join('&');

    return '?' + query;
}

function generateId(length = 8) {
    return generate(alphabet, length);
}

/**
 * classsify an array by the key
 * @param {string} key - key to classify by
 * @param {Array<Object>} arr - source array
 * @param {Function?} merge - function to create item or merge items with the same keys
 * @param {Object?} dest - destination object, optional
 * @returns {Object}
 */
const classify = (key, arr, merge = x => x, dest = {}) =>
    arr.reduce((obj, item) => {
        obj[item[key]] = merge(item, item[key], obj);
        return obj;
    }, dest);

/**
 * Casts argument to array
 * @param {any} arg - maybe array
 * @returns {Array}
 */
const arrify = arg => (Array.isArray(arg) ? arg : [arg]);

const noop = () => void 0;

/**
 * Transforms camel or snake cased string into kebab case.
 * @param {string} str
 * @returns {string} kebab cased string
 */
const toKebabCase = str =>
    str
        .replace(/([a-z])([A-Z])/g, '$1-$2')
        .replace(/[\s_]+/g, '-')
        .toLowerCase();


const participantLeftRoom = (participant, meeting) =>
    meeting?.roomId && participant?.roomId && meeting.roomId !== participant.roomId;

module.exports = {
    smartMerge,
    isDataPatched,
    serializeParams,
    generateId,
    classify,
    arrify,
    noop,
    toKebabCase,
    participantLeftRoom,
};
