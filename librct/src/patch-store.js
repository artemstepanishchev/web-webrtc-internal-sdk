var util                = require('util');
var EventEmitter        = require('events').EventEmitter;
var smartMerge          = require('./utils').smartMerge;
var isEqual             = require('lodash.isequal');

function PatchStore(options) {
    EventEmitter.call(this);
    this._options = options || {};
    this._keepAlive = this._options.keepAlive || 2000;
    this._optimisticUpdate = this._options.optimisticUpdate !== false;
    this._data = [];
}
util.inherits(PatchStore, EventEmitter);

PatchStore.prototype._getData = function () {
    return this._data;
};

PatchStore.prototype._getLength = function () {
    return this._getData().length;
}

PatchStore.prototype.isEmpty = function () {
    return this._getLength() === 0;
};

PatchStore.prototype._remove = function (params) {
    const prevData = this.getData(); 
    const index = this._data.indexOf(params);

    if (index > -1) {
        this._data.splice(index, 1);
    }
    
    if (!isEqual(prevData, this.getData())) {
        this.emit(PatchStore.EVENT.DATA_REMOVED, params);
    }
};

PatchStore.prototype.add = function (params) {
    if (this._optimisticUpdate) {
        this._data.push(params);
    }
    this.emit(PatchStore.EVENT.DATA_ADDED, params);

    setTimeout(() => {
        this._remove(params);
    }, this._keepAlive);
};


PatchStore.prototype.getData = function () {
    return this._getData().reduce((acc, el) => {
        smartMerge(acc, el);
        return acc;
    }, {});
};

PatchStore.EVENT = {
    DATA_ADDED: 'data_added',
    DATA_REMOVED: 'data_removed'
};

module.exports = PatchStore;
