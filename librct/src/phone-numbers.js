var Model = require('./model');
var TransportHttp = require('./transport').TransportHttp;

/**
 * Get Phone number List class to get list of available phone numbers
 *
 * @param       {Object} options inner properties and methods
 * @constructor
 */
class PhoneNumbers extends Model {
    constructor(options) {
        super({}, options);
        this._transport = new TransportHttp(options);

        this._modelFields = [];
    }

    url() {
        return '/rcvideo/v1/dial-in-numbers';
    }

    parseUrl() {
        return '/restapi/v1.0/number-parser/parse';
    }

    getList() {
        return this._transport._read(this.url());
    }

    parse(phoneNumbers) {
        return this.apiCall(this.parseUrl(), {
            method: 'POST',
            body: JSON.stringify({ originalStrings: phoneNumbers }),
        });
    }
}

module.exports = PhoneNumbers;