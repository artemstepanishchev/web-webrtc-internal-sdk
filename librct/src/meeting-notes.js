const Model = require('./model');
const TransportHttp = require('./transport').TransportHttp;

/**
 * @typedef MeetingSummaries
 *
 * @property {string} short – short summary
 * @property {Array<string>} [long] - long summary
 */

/**
 * @typedef SummariesResponse
 *
 * @property {string} meetingId – id of the meeting
 * @property {Array<string>} [keywords] - list of keywords of the meeting
 * @property {MeetingSummaries} summary - long and short summary of the meeting
 */

/**
 * @typedef Phrases
 *
 * @property {number} id – id of the phrase
 * @property {string} participantId – participant id
 * @property {number} startTime – phrase start time
 * @property {number} endTime – phrase end time
 * @property {string} text – whole transcript phrase
 */

/**
 * @typedef TranscriptPhrasesResponse
 *
 * @property {string} meetingId – id of the meeting
 * @property {Array<Phrases>} [phrases] - list of transcript phrases
 */

class MeetingNotes extends Model {
    constructor(options) {
        super({}, options);

        this._transport = new TransportHttp(options);
    }

    url() {
        return this.getVersionedBaseUrl('meeting-notes/v1/meetings') + '/';
    }

    /**
     * Get long/short summaries and keywords by meeting id
     *
     * @param  {string} meetingId - meeting id
     *
     * @return {Promise.<SummariesResponse>}
     *
     * @example librct.meetingNotes().getSummary('meetingId').then(response => console.log(response));
     */
    getSummary(meetingId) {
        return this.apiCall(`${this.url()}${meetingId}/summary`, {
            method: 'GET'
        });
    }

    /**
     * Get full transcript of the meeting
     *
     * @param  {string} meetingId - meeting id
     *
     * @return {Promise.<TranscriptPhrasesResponse>}
     *
     * @example librct.meetingNotes().getTranscriptPhrases('meetingId').then(response => console.log(response));
     */
    getTranscriptPhrases(meetingId) {
        return this.apiCall(`${this.url()}${meetingId}/transcript/phrases`, {
            method: 'GET'
        });
    }
}

module.exports = MeetingNotes;
