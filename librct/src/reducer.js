const { patch } = require('./patcher');
const { createRecalculator } = require('./recalculator');

/**
 * @typedef {import('./apis/rct-api').CallOnCreation} RCTCallOnCreation
 * @typedef {import('./apis/rct-api').Call} RCTCall
 * @typedef {import('./patcher').RCTCallAcc} RCTCallAcc
 */

/**
 * @param {RCTCallAcc} customProps
 * @returns {RCTCallAcc}
 */
const getInitialCallState = customProps => ({
    meeting: null,
    participants: null,
    streams: null,
    sessions: null,
    localParticipant: null,
    localSession: null,
    localStreams: null,
    myDemands: null,
    targetedToMeDemands: null,
    recordings: null,
    notifications: null,
    myDials: null,
    channels: null,
    meetingParticipants: null,
    notifyChannels: null,
    localSessionHistory: {},
    ...customProps,
});

/**
 * action's type enum
 * @enum {string}
 */
const ActionTypes = {
    CHANGE: 'change',
    CHANGE_BATCH: 'change_batch',
    HARD_RESET: 'hard_reset',
    SOFT_RESET: 'soft_reset',
};

const createCachedReporter = (propsToReport, reportChanged) => {
    let lastState = {};
    return (state, forceReport) => {
        propsToReport.forEach(prop => {
            if (lastState[prop] !== state[prop] || forceReport) {
                reportChanged(`changed:${prop}`, state[prop]);
            }
        });
        lastState = state;
    };
};

const extractLocalIds = state => ({
    localParticipantId: state.localParticipant?.id || null,
    localSessionId: state.localSession?.id || null,
    localStreamIds: Object.keys(state.localStreams || {}),
});

const callBackup = state => {
    const { meeting, localParticipant, localSession } = state;

    // The following props exist only in response to the POST meeting request, but not to GET meeting.
    // So we need to keep them in state for soft and version recovery.
    const backup = {
        meeting: meeting && {
            meetingPassword: meeting.meetingPassword,
            meetingPasswordPSTN: meeting.meetingPasswordPSTN,
            meetingPasswordMasked: meeting.meetingPasswordMasked,
        },
        localParticipant: localParticipant && {
            shortPrtsPin: localParticipant.shortPrtsPin,
        },
        localSession: localSession && {
            notificationSubKey: localSession.notificationSubKey,
            notificationToken: localSession.notificationToken,
            pingInfo: localSession.pingInfo,
            token: localSession.token,
        },
    };

    return Object.fromEntries(Object.entries(backup).filter(([, value]) => Boolean(value)));
};

const initialState = () => ({
    // We need to keep local ids to be able to retrieve related entities from
    // PubNub patches and RCT responses to GET-meeting requests, which we send
    // during SOFT and VERSION recovery.
    localIds: {},
    call: getInitialCallState(),
    recalculatedEntities: {},
});

/**
 * @callback RootReducer
 * @param {Object} state
 * @param {{ type: string, data?: any }} action
 * @param {RCTCallOnCreation|RCTCall|Array<RCTCall>} action.data - GET/POST meeting payload or array of PubNub event data
 * @returns {Object}
 */

/** @returns {RootReducer} */
const createReducer = () => {
    let recalculator = createRecalculator();

    return (state = initialState(), { type, data }) => {
        switch (type) {
            case ActionTypes.CHANGE: {
                const call = patch(state.call, data, state.localIds);
                const localIds = extractLocalIds(call);
                const recalculatedEntities = recalculator.recalculate(call);
                return {
                    ...state,
                    localIds,
                    call,
                    recalculatedEntities,
                };
            }
            case ActionTypes.CHANGE_BATCH: {
                let { call, localIds } = state;
                data.forEach(d => {
                    call = patch(call, d, localIds);
                    localIds = extractLocalIds(call);
                });
                const recalculatedEntities = recalculator.recalculate(call);
                return {
                    ...state,
                    localIds,
                    call,
                    recalculatedEntities,
                };
            }
            case ActionTypes.HARD_RESET: {
                const { localSessionHistory } = state.call;
                recalculator = createRecalculator();
                return {
                    ...initialState(),
                    call: getInitialCallState({ localSessionHistory }),
                };
            }
            case ActionTypes.SOFT_RESET: {
                const { call, localIds } = state;
                recalculator = createRecalculator();
                const newState = {
                    ...initialState(),
                    localIds,
                    call: getInitialCallState({
                        localSessionHistory: call.localSessionHistory,
                        ...callBackup(call),
                    }),
                };
                return newState;
            }
            default:
                return state;
        }
    };
};

module.exports = {
    createReducer,
    createCachedReporter,
    types: ActionTypes,
};
