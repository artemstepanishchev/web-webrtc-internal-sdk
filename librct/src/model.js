var util                          = require('util');
var EventEmitter                  = require('events').EventEmitter;
var { smartMerge, isDataPatched } = require('./utils');
var omit                          = require('lodash.omit');
var pick                          = require('lodash.pick');
var cloneDeep                     = require('lodash.clonedeep');
var debug                         = require('debug')('rwc:model');
var PatchStore                    = require('./patch-store');

function Model (params, options) {
    EventEmitter.call(this);
    this._options = options || {};
    params = params || {};
    this._baseUrl = this._options.baseUrl;
    this._modelData = {};
    this.id = params.id;
    this._patchStore = this._options.patchStore || PatchStore;
    this._ps = new this._patchStore(options);
    this._dropped = false;

    this._patchStoreDataAdded = data => {
        this._onDataPatch('change', data);
    };

    this._patchStoreDataRemoved = data => {
        this._onDataRemovePatch('change', data);
    };

    this.addEventListeners();
}
util.inherits(Model, EventEmitter);

Model.prototype._read = function () {
    return this._transport._read(this.url());
};

Model.prototype._addPatch = function (params) {
    this._ps.add(this._makePatch(params));
};

Model.prototype.addPatch = Model.prototype._addPatch;

Model.prototype._makePatch = function (params) {
    return params;
};

Model.prototype.apiCall = function (url, options) {
    return this._transport.apiCall(url, options);
};

Model.prototype.getVersionedBaseUrl = function (apiVersion) {
    return `${this._baseUrl}/${apiVersion || this._options.apiVersion}`;
};

Model.prototype.maintainedUrl = function (url) {
    const { maintainance, maintainancePath } = this._options;
    return maintainance ? `${url}/${maintainancePath}` : url;
};

/**
 * 
 * @param {object} params -
 * @param {object} [options] http request options
 * 
 * @returns {Promise.<any>} -
 */
Model.prototype.create = function (params, options) {
    this.set('id', null);
    return this._transport._create(
        this.url(),
        omit(
            Object.assign(
                {},
                this.toJSON(),
                params
            ),
            this._keys
        ),
        options
    );
};

Model.prototype.read = function () {
    return this._transport._read(this.url()).then(json => {
        if (!this.hasId() && json.id) {
            this.id = json.id;
        }
        return this._transport._setData(json);
    });
};

Model.prototype._update = function (params) {
    return this._transport._update(this.url(), params);
};

Model.prototype.strictUpdate = Model.prototype._update;

Model.prototype.update = function (params) {
    this._addPatch(params);

    return this._update(params);
};

Model.prototype._put = function (params) {
    return this._transport._put(this.url(), params);
};

Model.prototype.put = function (params) {
    this._addPatch(params);
    return this._put(params);
};

Model.prototype.delete = function (options) {
    this._addPatch({ deleted: true });

    return this._transport._delete(this.url(), options)
        .then(data => {
            this.removeAllListeners();
            this.removeEventListeners();

            if (typeof this._transport.removeAllListeners === 'function') {
                this._transport.removeAllListeners();
            }

            return data;
        });
};

Model.prototype.clear = function () {
    debug('clear', this.constructor.name);
    this._modelData = this._defaults ? cloneDeep(this._defaults) : {};
};

Model.prototype.registerModelFields = function (fields, subClasses, overrides, keys) {
    this._modelFields = fields;
    this._subClasses = subClasses || {};
    this._overrides = overrides || {};
    this._keys = keys || ['id'];
    this.clear();
};

Model.prototype.set = function (obj, val) {
    let isChanged = false;
    const alreadyDeleted = this._modelData.deleted;

    if (typeof obj !== 'object') {
        obj = {
            [obj]: val
        };
    }

    if (!this.id && obj.id) {
        this.id = obj.id;
    }

    obj = pick(obj, this._modelFields);
    for (let k in obj) {
        if (typeof this._subClasses[k] === 'function') continue;
        isChanged = smartMerge(this._modelData, {
            [k]: obj[k]
        }) || isChanged;
    }
    for (let k in obj) {
        if (typeof this._subClasses[k] !== 'function') continue;
        if (Array.isArray(obj[k])) {
            obj[k] = obj[k].map(el => this._subClasses[k](el, this._options));
        } else {
            obj[k] = this._subClasses[k](obj[k], this._options);
        }
        isChanged = smartMerge(this._modelData, {
            [k]: obj[k]
        }) || isChanged;
    }

    if (
        isChanged &&
        this.id &&
        obj.deleted &&
        !alreadyDeleted &&
        !this._dropped
    ) {
        this._drop();
        this._dropped = true;
    }

    debug('set', this.constructor.name, obj);
    return isChanged;
};

Model.prototype.get = function (key, doNotFilterDeleted = false) {
    if (typeof this._overrides[key] === 'function') {
        return this._overrides[key].call(this);
    }

    if (typeof this._modelData[key] === 'object') {
        if (Array.isArray(this._modelData[key]) && !doNotFilterDeleted) {
            return this._modelData[key].filter(
                item => !((item instanceof Model) && item.get('deleted'))
            );
        }
        return this._modelData[key];
    }
    const psData = this._ps.getData();
    return psData.hasOwnProperty(key) ? this._ps.getData()[key] : this._modelData[key];
};

Model.prototype.modelToJSON = function (doNotFilterDeleted = false) {
    // TODO: do real immutable this._modelData in smartMerge instead use lodash.cloneDeep
    return cloneDeep(this._modelFields.reduce((ret, cur) => {
        if (typeof this._modelData[cur] != 'undefined') {
            ret[cur] = this.get(cur, doNotFilterDeleted);
            if (Array.isArray(ret[cur])) {
                ret[cur] = ret[cur].map(el => {
                    if (!el.toJSON) return el;
                    return el.toJSON();
                });
            } else if (ret[cur] && ret[cur].toJSON) {
                ret[cur] = ret[cur].toJSON();
            }
        }
        return ret;
    }, {}));
};

Model.prototype.getOverrides = function () {
    const result = {};

    for (const prop in this._overrides) {
        if (!this._overrides.hasOwnProperty(prop)) continue;

        result[prop] = this._overrides[prop].call(this);
    }

    return result;
};

Model.prototype.toJSON = function (doNotFilterDeleted = false) {
    const modelJSON = this.modelToJSON(doNotFilterDeleted);
    const overrides = this.getOverrides();

    if (typeof this._ps !== 'undefined' && !this._ps.isEmpty()) {
        smartMerge(modelJSON, overrides, this._ps.getData());
    }

    if (JSON.stringify(overrides) !== JSON.stringify({})) {
        smartMerge(modelJSON, overrides);
    }

    return modelJSON;
};

Model.prototype.hasId = function () {
    return !!this.id;
};

Model.prototype.getId = function () {
    return new Promise((resolve, reject) => {
        debug('getId id', this.constructor.name, this.hasId(), this.id);
        if (this.hasId()) {
            resolve(this.id);
        } else {
            this._read().then(json => {
                if (typeof json.id == 'undefined') {
                    throw new Error('Empty id');
                }
                this.set({ id: json.id });
                this.id = json.id;
                resolve(this.id);
            }).catch(err => {
                reject(err);
            });
        }
    });
};

Model.prototype.addEventListeners = function () {
    this._ps.on(this._patchStore.EVENT.DATA_ADDED, this._patchStoreDataAdded);
    this._ps.on(PatchStore.EVENT.DATA_REMOVED, this._patchStoreDataRemoved);
};

Model.prototype.removeEventListeners = function () {
    this._ps.removeListener(PatchStore.EVENT.DATA_ADDED, this._patchStoreDataAdded);
    this._ps.removeListener(PatchStore.EVENT.DATA_REMOVED, this._patchStoreDataRemoved);
};

Model.prototype._onDataPatch = function (event) {
    this.emit(event, this.toJSON());
};

Model.prototype._onDataRemovePatch = function (event, data) {
    var isChanged = !isDataPatched(this.modelToJSON(), data);

    if (isChanged) {
        this.emit(event, this.toJSON());
    }
};

Model.prototype._drop = function () {
};

module.exports = Model;
