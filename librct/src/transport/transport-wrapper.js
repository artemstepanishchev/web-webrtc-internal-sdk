const EventEmitter = require('events').EventEmitter;
const debug = require('debug')('librct:PN');

const RCT_MESSAGE_TYPES = {
    PING: 'ping',
    DATA: 'data',
    INVALIDATE: 'invalidate',
    INVALIDATE_ROOM: 'invalidate_room',
    TIMEOUT: 'timeout',
};

/**
 * @class Transport
 * Class wrapper to any sync transport
 * @extends EventEmitter
 */
class TransportWrapper extends EventEmitter {
    /**
     * Create a transport wrapper.
     * @param {Object} options - transport options
     * @param {string} options.authKey - auth key
     * @param {string} options.subscribeKey - subscribe key
     * @param {Function} options.pubnub - PubNub sdk
     * @param {string} [options.pubnubOrigin] - custom pubnub origin
     */
    constructor(options = {}) {
        super();

        this._onMessage = this._onMessage.bind(this);
        this._onStatus = this._onStatus.bind(this);
        this._pn = null;
        this._resetConnectPromises();

        if (options.pubnub) {
            this._pubnub = options.pubnub;
            this._pubnubOrigin = options.pubnubOrigin;
            this.PN_CATEGORIES = this._pubnub.CATEGORIES;
        } else {
            debug('\x1b[31m PubNub sdk is not found in options \x1b[0m');
            throw new Error('PubNub sdk not found!');
        }
    }

    _resetConnectPromises() {
        this._connectResolvers = [];
        this._connectRejecters = [];
    }

    /**
     * Initialize PN SDK
     * @param {string} subscribeKey - subscribe key for PN
     * @param {string} authKey - auth key for PN
     */
    init(subscribeKey, authKey) {
        debug('Init PN. subscribeKey: ', subscribeKey, 'authKey: ', authKey);
        if (!subscribeKey) {
            debug('\x1b[31m subscribeKey is not found in options \x1b[0m');

            throw new Error('subscribeKey is required!');
        }

        if (!authKey) {
            debug('\x1b[31m authKey is not found in options \x1b[0m');

            throw new Error('authKey is required!');
        }

        this._pn = new this._pubnub({
            origin: this._pubnubOrigin,
            subscribeKey: subscribeKey,
            authKey: authKey,
            restore: true,
            suppressLeaveEvents: true,
        });

        this._pn.setHeartbeatInterval(0);

        this._pn.addListener({
            status: this._onStatus,
            message: this._onMessage,
        });
    }

    initIfNeeded(subscribeKey, authKey) {
        if (!this._pn) {
            return this.init(subscribeKey, authKey);
        }
    }

    /**
     * Status handler for PN
     * @param {Object} e - status from PN
     */
    _onStatus(e) {
        switch (e.category) {
            case this.PN_CATEGORIES.PNConnectedCategory:
                if (e.error) {
                    debug('Connect error', e);
                    this._reportConnectError(e);
                } else {
                    debug('Connect success');
                    this._reportConnected();
                }
                return;
            case this.PN_CATEGORIES.PNReconnectedCategory:
                if (e.error) {
                    debug('Reconnect error', e);
                } else {
                    debug('Reconnect success');
                    this.emit(TransportWrapper.EVENTS.RECONNECTED);
                }
                return;
            case this.PN_CATEGORIES.PNAccessDeniedCategory:
            case this.PN_CATEGORIES.PNBadRequestCategory:
            case this.PN_CATEGORIES.PNTimeoutCategory:
            case this.PN_CATEGORIES.PNUnknownCategory:
                debug('PubNub unknown error', e);
                this.emit(TransportWrapper.EVENTS.UNKNOWN_ERROR);
                return;
            default:
                return;
        }
    }

    /**
     * Message handler for PN
     * @param {Object} data - message from PN
     */
    _onMessage(data) {
        const { type, body } = data.message;
        const channel = data.channel;
        switch (type) {
            case RCT_MESSAGE_TYPES.DATA:
                debug(`Data received, new v: ${data.message.body.version}`, 'data: ', body);
                this.emit(TransportWrapper.EVENTS.DATA, {
                    version: body.version,
                    data: body,
                    channel,
                });
                return;

            case RCT_MESSAGE_TYPES.PING:
                debug(`Ping received, new v: ${data.message.body.version}`);
                this.emit(TransportWrapper.EVENTS.PING, { version: body.version, channel });
                return;

            case RCT_MESSAGE_TYPES.INVALIDATE:
                debug('Invalidate received');
                this.emit(TransportWrapper.EVENTS.INVALIDATE, { channel });
                return;

            case RCT_MESSAGE_TYPES.INVALIDATE_ROOM:
                debug('Invalidate room received');
                this.emit(TransportWrapper.EVENTS.INVALIDATE_ROOM, { channel, data: body });
                return;

            case RCT_MESSAGE_TYPES.TIMEOUT:
                debug('Timeout received');
                this.emit(TransportWrapper.EVENTS.TIMEOUT, { channel });
                return;

            default:
                break;
        }
    }

    /** wrapper for connect resolver */
    _reportConnected() {
        this._connectResolvers.forEach(resolve => resolve());
        this._resetConnectPromises();
    }

    /** wrapper for connect rejecter */
    _reportConnectError(e) {
        this._connectRejecters.forEach(reject => reject(e));
        this._resetConnectPromises();
    }

    subscribe(channels) {
        if (!channels) {
            throw new Error('channels required');
        }
        if (!Array.isArray(channels)) {
            channels = [channels];
        }
        this._pn.subscribe({
            channels,
            withPresence: false,
        });
        return Promise.resolve();
    }

    /**
     * Connect to transport
     * @param {Array<string>|string} channels - channels to connect
     * @returns {Promise}
     */
    connect(channels) {
        this.subscribe(channels);

        return new Promise((resolve, reject) => {
            this._connectResolvers.push(resolve);
            this._connectRejecters.push(reject);
        }).catch(error => {
            debug('PN connect error', (error || {}).message || error);
            throw error;
        });
    }

    /**
     * Disconnect to transport
     */
    disconnect() {
        this._reportConnectError(new Error('Transport disconnect called'));

        this._pn.stop();
        this._pn = null;
    }

    disconnectIfNeeded() {
        if (this._pn) {
            this.disconnect();
        }
    }
}

TransportWrapper.EVENTS = {
    // event emitted after transport in recovered
    RECONNECTED: 'reconnected',
    // data event
    DATA: 'data',
    // ping (empty data event)
    PING: 'ping',
    // invalidate event should trigger recovery
    INVALIDATE: 'invalidate',
    INVALIDATE_ROOM: 'invalidate_room',
    TIMEOUT: 'timeout',
    UNKNOWN_ERROR: 'unknown_error',
};

module.exports = TransportWrapper;
