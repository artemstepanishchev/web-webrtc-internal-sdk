var util                = require('util');
var EventEmitter        = require('events').EventEmitter;
var debug               = require('debug')('rwc:transport');


function Transport (options) {
    EventEmitter.call(this);
    options = options || {};
    this._options = options;

    this._isPause = false;
    this._dataQueue = [];
    this._v = null;
    this._lastData = null;
}
util.inherits(Transport, EventEmitter);

Transport.prototype.pause = function () {
    debug('pause', this.constructor.name);
    this._isPause = true;
    this.emit(Transport.EVENT.PAUSE);
    return this;
};

Transport.prototype.resume = function () {
    debug('resume', this.constructor.name);
    this._isPause = false;
    this.emit(Transport.EVENT.RESUME);
    this._releaseData();
    return this;
};

Transport.prototype.isPaused = function () {
    return !!this._isPause;
};

Transport.prototype.curVersion = function () {
    return this._v;
};

Transport.prototype.dataQueue = function () {
    return this._dataQueue;
};

Transport.prototype._getQueueItemData = function (queueItem) {
    return queueItem.data;
};

Transport.prototype._getQueueItemVersion = function (queueItem) {
    return this._getDataVersion(this._getQueueItemData(queueItem));
};

Transport.prototype._getDataVersion = function (data) {
    return this._version(data);
};

Transport.prototype._version = function (data) {
    var v = (data || {})[Transport.FIELD.VERSION];
    return typeof v == 'undefined' ? null : v;
};

Transport.prototype._addData = function (data) {
    return new Promise((resolve, reject) => {
        var d = {
            data: data,
            resolve: resolve,
            reject: reject
        };
        debug('_addData', this.constructor.name, this.isPaused(), data);
        this._dataQueue.push(d);
        try {
            if (typeof this._sort == 'function') {
                this._dataQueue.sort(this._sort.bind(this));
            }
            this._releaseData();
        } catch (err) {
            console.error('Error on _addData', err, err.stack);
            this.emit(Transport.EVENT.DATA_REJECT, data, this._v);
            reject(err);
        }
    });
};

Transport.prototype._setData = function (data) {
    debug('_setData data', this.constructor.name, data);
    var v = this._getDataVersion(data);
    this._clearDataUntil(v);
    debug('_setData', this.constructor.name, this.isPaused());
    return this._addData(data);
};

Transport.prototype._afterPause = function (fn) {
    return new Promise((resolve, reject) => {
        if (this.isPaused()) {
            this.once(Transport.EVENT.RESUME, () => {
                this._afterPause(fn).then(resolve).catch(reject);
            });
        } else {
            fn(resolve, reject);
        }
    });
};


Transport.prototype._afterPauseFake = function (fn) {
    return new Promise((resolve, reject) => fn(resolve, reject));
};

Transport.prototype._clearDataUntil = function (v) {
    debug('_clearDataUntil', v);
    this._v = typeof v != 'undefined' ? v : Infinity;
    debug('_clearDataUntil', 'before do..while', this._v, v);
    var next;
    do {
        debug('_clearDataUntil', 'before nextData', this._v, v, next ? next.v < v : 'empty-next');
        next = this._hasNextData();
        debug('_clearDataUntil', 'after nextData', this._v, v, next);
        if (next && next.item && !next.isOk) {
            this._nextData();
            debug('_clearDataUntil', 'reject', next.data, next.v);
            this.emit(Transport.EVENT.DATA_REJECT, next.data, this._v);
            next.item.reject('old_data');
        }
    } while (next && !next.isOk);
    this._v = null;
};

Transport.prototype._releaseData = function () {
    if (this.isPaused()) return;
    var next = this._nextData();
    if (next === null) return;

    if (!next.isOk) {
        debug('_releaseData', 'reject', this._v);
        this.emit(Transport.EVENT.DATA_REJECT, next.data, this._v);
        next.item.reject(next.error || 'unknown_reason');
    } else {
        this._v = next.v;
        debug('_releaseData', 'resolve', this._v);
        this.emit(Transport.EVENT.DATA, next.data, this._v);
        next.item.resolve(next.data);
    }
    setTimeout(() => {
        this._releaseData();
    }, 0);
};


Transport.prototype._hasNextData = function () {
    debug('_hasNextData', this.constructor.name, 'qLength', this._dataQueue.length, this._v, this.isPaused());
    if (!this._dataQueue.length) return null;
    var queueItem = this._dataQueue[0];
    var firstData = this._getQueueItemData(queueItem);
    var firstVersion = this._getDataVersion(firstData);
    debug('_hasNextData', this.constructor.name, { firstVersion, curVersion: this._v, firstData });
    if (this._v !== null && firstVersion > this._v + 1) return null;

    if (this._v !== null && firstVersion < this._v) {
        return {
            item: queueItem,
            isOk: 0,
            error: 'old_data',
            data: firstData,
            v: firstVersion
        };
    }

    return {
        item: queueItem,
        isOk: 1,
        data: firstData,
        v: firstVersion
    };
};

Transport.prototype._nextData = function () {
    var ret = this._hasNextData();
    if (ret !== null) {
        this._dataQueue.shift();
    }
    return ret;
};

Transport.EVENT = {
    PAUSE: 'pause',
    RESUME: 'resume',
    DATA: 'data',
    DATA_REJECT: 'data_reject'
};

Transport.FIELD = {
    VERSION: 'version'
};


module.exports = Transport;
