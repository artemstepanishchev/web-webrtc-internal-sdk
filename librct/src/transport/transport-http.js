var URL                 = require('url');
var util                = require('util');
var debug               = require('debug')('rwc:transport:http');
var fetch               = require('isomorphic-fetch');
var Transport           = require('./transport');
var NetworkError        = require('../network-error');
var generateId          = require('../utils').generateId;

const DEFAULT_CONNECTION_TIMEOUT = 7000;

function TransportHttp (options) {
    Transport.call(this, options);

    this._pendingRequests = {};

    this._fetch = null;
    this.setFetch(this._options.fetch || fetch);
    this._token = this._options.token;
}
util.inherits(TransportHttp, Transport);

TransportHttp.prototype.setFetch = function (_fetch) {
    this._fetch = _fetch;
};

TransportHttp.prototype.getFetch = function () {
    return this._fetch;
};

/**
 * Aborts fetch request by timeout
 * @param {string} url
 * @param {AbortController} abortController
 * @param {number} connectionTimeout
 * @returns {Promise}
 * @private
 */
TransportHttp.prototype._failOnTimeout = function (url, abortController, connectionTimeout) {
    return new Promise((resolve, reject) => {
        this._pendingRequests[url] = setTimeout(() => {
            delete this._pendingRequests[url];
            if (abortController) {
                abortController.abort();
            }
            reject(new Error(`Connection timeout error ${url}`));
        }, connectionTimeout);
    });
};

TransportHttp.prototype.apiCall = function (url, options) {
    options = options || {};
    return this._afterPauseFake((resolve, reject) => {
        if (options && typeof options.body == 'object') {
            options.body = JSON.stringify(options.body);
        }
        options.headers = options.headers || {};
        options.headers['content-type'] = options.headers['content-type'] || 'application/json';

        if (typeof this._token != 'undefined') {
            if (typeof options.headers.authorization === 'undefined') {
                options.headers.authorization = `Bearer ${this._token}`;
            }
        }

        for (var k in options.headers) {
            if (options.headers.hasOwnProperty(k)) {
                if (options.headers[k] === null || typeof options.headers[k] == 'undefined') {
                    delete options.headers[k];
                }
            }
        }

        const abortController = typeof AbortController !== 'undefined' ? new AbortController() : null;
        if (abortController) {
            options.signal = abortController.signal;
        }

        url = URL.parse(url, true);
        url.query = url.query || {};
        delete url.search;
        url = URL.format(url);
        debug('apiCall', this.constructor.name, options.method || 'GET', url);

        const connectionTimeout = options.connectionTimeout || DEFAULT_CONNECTION_TIMEOUT;

        Promise.race([
            this._failOnTimeout(url, abortController, connectionTimeout),
            this._fetch(url, options).then(response => {
                debug('apiCall response', this.constructor.name, { status: response.status });
                clearTimeout(this._pendingRequests[url]);
                delete this._pendingRequests[url];
                if (!response.ok) {
                    var err = new NetworkError('Bad response status code: ' + response.status, response);

                    throw err;
                } else {
                    var contentType = response.headers.get('Content-Type') || '';
                    if (contentType.indexOf('application/json') >= 0 || contentType.indexOf('multipart/mixed') >= 0) {
                        return response.text().then(body => {
                            debug('apiCall response text', this.constructor.name, body, body.length);
                            if (contentType.indexOf('application/json') !== -1 && body && body.length) {
                                var json = JSON.parse(body);
                                debug('apiCall response json', this.constructor.name, json);
                                return json;
                            }
                            return body;
                        });
                    }

                    return response;
                }
            })
        ]).then(json => {
            resolve(json);
        }).catch(err => {
            reject(err);
        });
    });
};

TransportHttp.prototype._read = function (url) {
    return this.apiCall(url, { method: 'GET' });
};

/**
 *
 * @param {string} url -
 * @param {object} [options] http request options
 *
 * @returns {Promise.<any>} -
 */
TransportHttp.prototype._delete = function (url, options) {
    const requestOptions = Object.assign({}, options, {
        method: 'DELETE'
    });

    return this.apiCall(url, requestOptions);
};

TransportHttp.prototype._update = function (url, params) {
    return this.apiCall(url, {
        method: 'PATCH',
        body: params
    });
};

TransportHttp.prototype._put = function (url, params) {
    return this.apiCall(url, {
        method: 'PUT',
        body: params
    });
};

/**
 *
 * @param {string} url -
 * @param {object} params -
 * @param {object} [options] http request options
 *
 * @returns {Promise.<any>} -
 */
TransportHttp.prototype._create = function (url, params, options) {
    const requestOptions = Object.assign({}, options, {
        method: 'POST',
        body: params
    });

    return this.apiCall(url, requestOptions);
};

TransportHttp.EVENT = Transport.EVENT;
TransportHttp.FIELD = Transport.FIELD;

module.exports = TransportHttp;
