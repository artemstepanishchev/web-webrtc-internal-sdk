/**
 * Promise wrapeper with cancel promise parameter. 
 * Affects all promise chain, do not returnanything on cancel
 * @param { Function } executor
 * @param { Promise } promise canceler. Cancel promise on resolve/reject
 */
class CancelablePromise {
    static resolve (value, cancelPromise) {
        return new CancelablePromise((resolve, reject) => {
            Promise.resolve(value).then(resolve, reject);
        }, cancelPromise);
    }

    static reject (value, cancelPromise) {
        return new CancelablePromise((resolve, reject) => {
            Promise.reject(value).then(resolve, reject);
        }, cancelPromise);
    }

    static all(iterable, cancelPromise) {
        return new CancelablePromise((resolve, reject) => {
            Promise.all(iterable).then(resolve, reject);
        }, cancelPromise);
    }

    static race(iterable) {
        return new CancelablePromise((resolve, reject) => {
            Promise.race(iterable).then(resolve, reject);
        });
    }

    constructor (executor, cancelPromise) {
        const handleCancel = () => this._canceled = true;

        if (cancelPromise) {
            this._cancelPromise = cancelPromise.then(handleCancel, handleCancel);

            this._promise = Promise.race([new Promise(executor), this._cancelPromise]);
        } else {
            this._promise = new Promise(executor);
        }
    }

    _handleCallback (resolve, reject, callback, data) {
        try {
            resolve(callback(data));
        } catch (e) {
            reject(e);
        }
    }

    then(successCB, errorCB) {
        return new CancelablePromise((resolve, reject) => {
            this._promise.then(data => {
                if (this._canceled) return;
                if (successCB) {
                    this._handleCallback(resolve, reject, successCB, data);
                } else {
                    resolve(data);
                }
            }, e => {
                if (this._canceled) return;
                if (errorCB) {
                    this._handleCallback(resolve, reject, errorCB, e);
                } else {
                    reject(e);
                }
            })
        }, this._cancelPromise);
    }

    catch(errorCB) {
        return this.then(undefined, errorCB);
    }
}

module.exports = CancelablePromise;