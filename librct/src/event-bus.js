const debug = require('debug')('librct:event-bus');

class EventEmitter {
    constructor() {
        this.events = {};
    }

    on(event, listener) {
        if (!Array.isArray(this.events[event])) {
            this.events[event] = [];
        }
        this.events[event].push(listener);
        return () => this.removeListener(event, listener);
    }

    removeListener(event, cb) {
        this.events[event] = cb ? this.events[event].filter(listener => listener !== cb) : [];
    }

    removeAllListeners() {
        Object.keys(this.events).forEach(event => {
            this.events[event] = [];
        });
    }

    emit(event, ...args) {
        if (Array.isArray(this.events[event])) {
            this.events[event].forEach(listener => listener.apply(this, args));
        }
    }
}

class EventBus {
    constructor() {
        this._emitter = new EventEmitter();
    }

    listen(events = [], cb) {
        const callbacks = events.map(event => [event, (...args) => cb(event, ...args)]);
        callbacks.forEach(([event, callback]) => this._emitter.on(event, callback));
        return () => {
            callbacks.forEach(([event, callback]) => this._emitter.removeListener(event, callback));
        };
    }

    send(event, ...data) {
        debug(event, ...data);
        this._emitter.emit(event, ...data);
    }
}

module.exports = { EventBus, EventEmitter };
