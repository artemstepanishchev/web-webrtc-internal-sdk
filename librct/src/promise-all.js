function All (val) {
    if (Array.isArray(val)) {
        return Promise.all(val);
    } else if (typeof val != 'object') {
        return Promise.resolve(val);
    }
    var keys = Object.keys(val);
    var promises = keys.map(key => val[key]);
    return Promise.all(promises)
        .then(results => {
            return keys.reduce((ret, key, idx) => {
                ret[key] = results[idx];
                return ret;
            }, {});
        });
}

module.exports = All;
