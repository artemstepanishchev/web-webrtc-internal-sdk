// var debug               = require('debug')('rwc:bridge');
var Model               = require('./model');
var util                = require('util');
var TransportHttp       = require('./transport').TransportHttp;
var URL                 = require('url');

/**
 * @typedef {('Recording')} Section
 */
/**
 * @typedef {('Extension'|'Group'|'Guest')} SharingType
 */
/**
 * @typedef {Object} Share
 * @property {Section}  section              - section
 * @property {SharingType} type              - type
 * @property {String} accountId              - target account id
 * @property {String}  extensionId           - target extension id
 */

/**
 * History class to work with CHS
 * @param       {Object} options inner properties and methods
 * @constructor
 */
function History (options) {
    Model.call(this, {}, options);

    this._transport = new TransportHttp(options);
}

util.inherits(History, Model);

/**
 * Get history url
 * @return {String}
 */
History.prototype.url = function (apiVersion = 'v1') {
    return this.getVersionedBaseUrl(apiVersion) + '/history';
};

/**
 * Get meetings
 * @param   {Object} params
 * @param   {String} params.type        - ['All', 'My', 'Shared', 'Deleted', null]
 * @param   {Number} params.perPage     - page limit
 * @param   {String} params.text        - filter
 * @param   {String} params.offsetToken - offset token (for next page)
 * @return {Promise.<Object>} response with list of recordings
 * @example librct.history().getMeetings()
 *   .then(result => console.log(result));
 */
History.prototype.getMeetings = function ({
    type = null,
    perPage = 20,
    filter = '',
    pageToken = ''
}) {
    var url = URL.format({
        pathname: this.url() + '/meetings',
        query: {
            type,
            perPage,
            text: filter,
            pageToken
        }
    });

    return this.apiCall(url, { method: 'GET' });
};

/**
 * Get meeting by id
 * @param  {string} id - recording id
 * @return {Promise.<Object>}
 * @example librct.history().getMeeting('recordingId')
 *   .then(result => console.log(resutl));
 */
History.prototype.getMeeting = function (id) {
    var url = this.url() + '/meetings/' + id;

    return this.apiCall(url, { method: 'GET' });
};

/**
 * Get list of recording shareWithExtension
 * @param  {String} meetingId - meeting id
 * @param  {String} recordingId - recording id
 * @return {Promise.<Array>}    list of shares
 * @example librct.history().getShares('meetingId', 'recordingId')
 *   .then(result => console.log(resutl));
 */
History.prototype.getShares = function (meetingId) {
    var url = `${this.url()}/meetings/${meetingId}/recordings/shares`;

    return this.apiCall(url, { method: 'GET' });
};

/**
 * Delete recording
 * @param  {String} id - recording id
 * @return {Promise}
 * @example librct.history().deleteRecording('meetingId', 'recordingId')
 *   .then(result => console.log('recording deleted'));
 */
History.prototype.deleteRecording = function (meetingId, recordingId) {
    var url = `${this.url()}/meetings/${meetingId}/recordings/${recordingId}`;

    return this.apiCall(url, {
        method: 'DELETE'
    });
};

/**
 * Share recording
 * @param  {String} meetingId          meeting id
 * @param  {String} recording          recording id
 * @param  {String} accountId          target account id
 * @param  {String} extensionId        target extension id
 * @return {Promise}
 * @example librct.history().shareRecording('meetingId', 'recordingId', 'accountId', 'extensionId')
 *   .then(result => console.log('shared'));
 */
History.prototype.shareRecording = function (meetingId, accountId, extensionId, name) {
    var url = `${this.url()}/meetings/${meetingId}/recordings/shares`;

    return this.apiCall(url, {
        method: 'POST',
        body: {
            accountId,
            extensionId,
            name
        }
    });
};

/**
 * Unshare recording
 * @param  {String} meetingId       meeting id
 * @param  {String} recordingId     recording id
 * @param  {String} shareId         share id
 * @return {Promise}
 * @example librct.history().unshareRecording('meetingId', 'recordingId', 'shareId')
 *   .then(result => console.log('unshared'));
 */
History.prototype.unshareRecording = function (meetingId, shareId) {
    var url = `${this.url()}/meetings/${meetingId}/recordings/shares/${shareId}`;

    return this.apiCall(url, {
        method: 'DELETE'
    });
};

/**
 * Bulk share recording
 * @param  {String} meetingId       meeting id
 * @param  {Array.<Share>} list     share list
 * @return {Promise}
 * @example librct.history().bulkShareRecording('meetingId', [{'section', 'type', 'accountId', 'extensionId'}])
 *   .then(result => console.log('shared'));
 */
History.prototype.bulkShareRecording = function (meetingId, list) {
    const url = `${this.url('v2')}/meetings/${meetingId}/shares`;

    return this.apiCall(url, {
        method: 'POST',
        body: {
            list
        }
    });
};

/**
 * Bulk unshare recording
 * @param  {String} meetingId           meeting id
 * @param  {Array.<String>} shareIds     list of ids
 * @return {Promise}
 * @example librct.history().bulkUnshareRecording('meetingId', ['id1', 'id2'])
 *   .then(result => console.log('unshared'));
 */
History.prototype.bulkUnshareRecording = function (meetingId, shareIds) {
    const url = `${this.url('v2')}/meetings/${meetingId}/shares`;

    return this.apiCall(url, {
        method: 'DELETE',
        body: {
            shareIds
        }
    });
};

/**
 * @private
 */
History.prototype._baseTrashRecording = function (meetingId, recordingId, trash) {
    var url = `${this.url()}/meetings/${meetingId}/recordings/${recordingId}`;

    return this.apiCall(url, {
        method: 'POST',
        body: {
            availabilityStatus: trash ? 'Deleted' : 'Alive'
        },
        headers: {
            'X-HTTP-Method-Override': 'PATCH'
        }
     });
};

/**
 * Move recording to trash
 * @param  {String} meetingId                 meeting id
 * @param  {String} recordingId               recording id
 * @return {Promise}
 * @example librct.history().trashRecording('meetingId', 'recordingId')
 *   .then(result => console.log('trashed'));
 */
History.prototype.trashRecording = function (meetingId, recordingId) {
    return this._baseTrashRecording(meetingId, recordingId, true);
};

/**
 * Restore recording
 * @param  {String} meetingId                 meeting id
 * @param  {String} recordingId               recording id
 * @return {Promise}
 * @example librct.history().restoreRecording('meetingId', 'recordingId')
 *   .then(result => console.log('restored'));
 */
History.prototype.restoreRecording = function (meetingId, recordingId) {
    return this._baseTrashRecording(meetingId, recordingId, false);
};

module.exports = History;
