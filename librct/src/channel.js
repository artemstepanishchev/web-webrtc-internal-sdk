var Model               = require('./model');
var util                = require('util');
var TransportHttp       = require('./transport').TransportHttp;


function ChannelControlInfo (params) {
    Model.call(this, params);

    this.registerModelFields([
        'demandId'
    ]);
    this.set(params);
}

util.inherits(ChannelControlInfo, Model);


function Channel (params, options = {}) {
    Model.call(this, params, options);
    this._transport = new TransportHttp(options);

    this.registerModelFields([
        'id',
        'bridgeId',
        'callId',
        'participantId',
        'sessionId',
        'type',
        'direction',
        'createTime',
        'info',
        'deleted',
        'allowAnnotations'
    ], {
        info: p => {
            const info = new ChannelControlInfo(p);

            info.on('change', () => {
                this._onDataPatch('change');
            });

            return info;
        }
    });
    this.set(params);
}

util.inherits(Channel, Model);

Channel.prototype.url = function () {
    const bridgeId = this.get('bridgeId');
    const callId = this.get('callId');
    const participantId = this.get('participantId');

    return `${this.getVersionedBaseUrl()}/bridges/${
        bridgeId
    }/meetings/${
        callId
    }/participants/${
        participantId
    }/channels${this.hasId() ? '/' + this.id : ''}`;
};

module.exports = Channel;
