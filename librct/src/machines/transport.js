const { Machine, assign, interpret } = require('xstate');
const TransportWrapper = require('../transport/transport-wrapper');
const {
    abortPendingRequest,
    ignorePromiseActions,
    withAsyncAutoStop,
    getLoggableAction,
} = require('./utils');
const { MEETING, LAYOUTS, TRANSPORT, STATE_MACHINES, REASON, CB_EVENTS } = require('./constants');
const { noop } = require('../utils');

const leaveEdges = {
    [MEETING.LEAVE]: 'leaving',
    [MEETING.INVALIDATE]: 'leaving',
    [MEETING.INVALIDATE_ROOM]: 'leaving',
};
const commonEdges = {
    [MEETING.TIMEOUT]: 'awaitForRecovery',
    [MEETING.PATCH_APPLIED]: { actions: 'setRecoveryDelay' },
};

const EVENTS = {
    DATA: 'TRANSPORT:DATA',
    INVALIDATE: 'TRANSPORT:INVALIDATE',
    TIMEOUT: 'TRANSPORT:TIMEOUT',
};

const getRecoveryDelay = ({ pingInterval, pingDelay }) =>
    (pingInterval || 60 * 1000) + (pingDelay || 5 * 1000);

const recalcDelays = (context, newRecoveryDelay) => ({
    recoveryDelay:
        newRecoveryDelay === context.originalRecoveryDelay
            ? context.recoveryDelay
            : newRecoveryDelay,
    originalRecoveryDelay: newRecoveryDelay,
});

/**
 * @typedef {Object} ChannelInfo
 * @property {string} channel - PubNub channel url
 * @property {number} version - channel version (increases on each received message)
 * @property {number} pingInterval - ms to wait without new messages
 * @property {number=} pingDelay - ms, potential network delay for PubNub messages
 */

/**
 * Initialize transport SM
 * @param {Object} params
 * @param {TransportWrapper} params.transport - instance of TransportWrapper
 * @param {string} params.authKey - for PubNub authentication
 * @param {string} params.subscribeKey - for subscription to PubNub
 * @param {ChannelInfo} params.channelInfo - channel info
 * @param {number} params.gapDelay - ms to wait on gap found
 * @param {number} params.transportReconnectDelay - ms to wait on transport connection error
 * @param {number} params.connectRecoveryDelay - ms to wait on transport connecting
 * @param {number} params.successDebounce - ms to wait on transport debounce
 * @param {Function} params.reportData - callback to report data from transport
 * @param {Function} params.reportTimeout - callback to report transport timeout
 *
 * @return {object} transport SM
 */
const transportMachineFabric = ({
    id,
    transport,
    authKey,
    subscribeKey,
    channelInfo = {},
    gapDelay = 1000,
    transportReconnectDelay = 3000,
    connectRecoveryDelay = 30000,
    successDebounce = 50,
    reportData,
    reportTimeout,
    reportVersionMismatch = noop,
} = {}) => {
    /**
     * Initialize the transport and connect to channels
     * @param {Object} context - SM context
     * @return {Promise}
     */
    const connectTransport = () => {
        transport.initIfNeeded(subscribeKey, authKey);
        return transport.connect(channelInfo.channel);
    };

    const initialRecoveryDelay = getRecoveryDelay(channelInfo);
    const isInitialInfoValid = channelInfo.channel && Number.isInteger(channelInfo.version);

    return Machine(
        {
            id: `transport:${id}`,
            initial: isInitialInfoValid ? 'connecting' : 'closed',
            context: {
                id,
                queue: {},
                version: channelInfo.version,
                recoveryDelay: initialRecoveryDelay,
                originalRecoveryDelay: initialRecoveryDelay,
            },
            states: {
                connecting: {
                    invoke: {
                        id: 'connectTransport',
                        src: connectTransport,
                        onDone: 'reportTO',
                        onError: 'handleConnectError',
                    },
                    on: leaveEdges,
                    after: {
                        CONNECT_RECOVERY_DELAY: 'recoveryDuringConnecting',
                    },
                },
                recoveryDuringConnecting: {
                    entry: 'reportTimeout',
                    always: {
                        target: 'connecting',
                    },
                },
                handleConnectError: {
                    after: {
                        TRANSPORT_RECONNECT_DELAY: 'connecting',
                    },
                },
                reportTO: {
                    // reportTimeout is called only after transition to awaitForRecovery state as non-assigner action
                    entry: 'reportTimeout',
                    always: {
                        target: 'awaitForRecovery',
                    },
                },
                awaitForRecovery: {
                    on: {
                        ...leaveEdges,
                        [MEETING.RECOVERED]: 'hasNextVersionCheck',
                        [MEETING.NOTIFY_CHANNELS_UPDATE]: {
                            actions: ['setVersion', 'setRecoveryDelay', 'clearOutdated'],
                        },
                        [TRANSPORT.DATA]: {
                            actions: 'addToQueue',
                        },
                        [TRANSPORT.PING]: {
                            actions: 'addToQueue',
                        },
                    },
                },
                hasNextVersionCheck: {
                    always: [
                        { target: 'waitForMessages', cond: 'isQueueEmpty' },
                        { target: 'reportPatch', cond: 'hasNextVersion' },
                        { target: 'waitForGap' },
                    ],
                },
                reportPatch: {
                    entry: 'maybeReportPatch',
                    always: 'hasNextVersionCheck',
                },
                waitForMessages: {
                    after: {
                        RECOVERY_DELAY: { actions: 'adjustTransportTimeout', target: 'reportTO' },
                    },
                    on: {
                        ...leaveEdges,
                        ...commonEdges,
                        [TRANSPORT.DATA]: {
                            actions: ['addToQueue', 'resetTransportTimeout'],
                            target: 'successDebounce',
                        },
                        [TRANSPORT.PING]: {
                            actions: ['addToQueue', 'resetTransportTimeout'],
                            target: 'successDebounce',
                        },
                        [TRANSPORT.RECONNECTED]: {
                            actions: 'resetTransportTimeout',
                            target: 'reportTO',
                        },
                    },
                },
                successDebounce: {
                    after: {
                        SUCCESS_DEBOUNCE: 'hasNextVersionCheck',
                    },
                    on: {
                        ...leaveEdges,
                        ...commonEdges,
                        [TRANSPORT.DATA]: {
                            actions: 'addToQueue',
                        },
                        [TRANSPORT.PING]: {
                            actions: 'addToQueue',
                        },
                        [TRANSPORT.RECONNECTED]: 'reportTO',
                    },
                },
                waitForGap: {
                    after: {
                        GAP_DELAY: {
                            target: 'reportTO',
                            actions: 'reportVersionMismatch',
                        },
                    },
                    on: {
                        ...leaveEdges,
                        ...commonEdges,
                        [TRANSPORT.DATA]: [
                            {
                                actions: 'addToQueue',
                                target: 'reportPatch',
                                cond: 'isNextVersionMsg',
                            },
                            { actions: 'addToQueue' },
                        ],
                        [TRANSPORT.PING]: [
                            {
                                actions: 'addToQueue',
                                target: 'reportPatch',
                                cond: 'isNextVersionMsg',
                            },
                            { actions: 'addToQueue' },
                        ],
                        [TRANSPORT.RECONNECTED]: 'reportTO',
                    },
                },
                leaving: {
                    entry: 'maybeDisconnectTransport',
                    always: {
                        target: 'closed',
                    },
                },
                closed: {
                    type: 'final',
                },
            },
        },
        {
            actions: {
                abortPendingRequest,
                reportTimeout,
                reportVersionMismatch: context =>
                    reportVersionMismatch({ version: context.version + 1 }),
                setVersion: assign((context, event) => ({
                    version: event.channelInfo.version,
                })),
                setRecoveryDelay: assign((context, event) => {
                    const newDelay = getRecoveryDelay(event.channelInfo);
                    return recalcDelays(context, newDelay);
                }),
                adjustTransportTimeout: assign(context => ({
                    recoveryDelay: Math.max(
                        context.recoveryDelay / 2,
                        TRANSPORT.MIN_RECOVERY_DELAY_MS
                    ),
                })),
                resetTransportTimeout: assign(context => ({
                    recoveryDelay: context.originalRecoveryDelay,
                })),
                clearContext: assign(() => ({
                    queue: {},
                    version: -1,
                    notifyChannels: {},
                    subscribeKey: null,
                    authKey: null,
                })),
                maybeDisconnectTransport: ignorePromiseActions(() =>
                    transport.disconnectIfNeeded()
                ),
                clearOutdated: assign({
                    queue: context =>
                        Object.keys(context.queue).reduce((acc, version) => {
                            if (version > context.version) {
                                acc[version] = context.queue[version];
                            }
                            return acc;
                        }, {}),
                }),
                addToQueue: assign((context, { version, data }) => {
                    if (context.version >= version) {
                        return context;
                    }

                    context.queue[version] = data;
                    return context;
                }),
                maybeReportPatch: assign(context => {
                    const result = [];
                    const queue = context.queue;
                    let nextVersion = context.version + 1;

                    while (queue.hasOwnProperty(nextVersion)) {
                        if (queue[nextVersion]) {
                            result.push(queue[nextVersion]);
                        }
                        delete queue[nextVersion];
                        nextVersion++;
                    }

                    if (result.length > 0) {
                        reportData(result);
                    }

                    return { queue, version: nextVersion - 1 };
                }),
            },
            delays: {
                TRANSPORT_RECONNECT_DELAY: transportReconnectDelay,
                GAP_DELAY: gapDelay,
                RECOVERY_DELAY: context => context.recoveryDelay,
                CONNECT_RECOVERY_DELAY: connectRecoveryDelay,
                SUCCESS_DEBOUNCE: successDebounce,
            },
            guards: {
                isQueueEmpty: context => Object.keys(context.queue).length === 0,
                hasNextVersion: context => context.queue.hasOwnProperty(context.version + 1),
                isNextVersionMsg: (context, { version }) => version === context.version + 1,
            },
        }
    );
};

/**
 * Initialize transport SM service
 * @param {Object} params
 * @param {TransportWrapper} params.transport - instance of TransportWrapper (can be send instead of pubnub sdk for tests)
 * @param {number} params.gapDelay - ms to wait on gap found
 * @param {number} params.transportReconnectDelay - ms to wait on transport connection error
 * @param {number} params.recoveryDelay - ms to wait without new messages
 * @param {number} params.connectRecoveryDelay - ms to wait on transport connecting
 * @param {number} params.successDebounce - ms to wait on transport debounce
 * @param {Function} params.reportData - callback to report data from transport
 * @param {Function} params.reportTimeout - callback to report transport timeout
 * @param {Function} params.reportInvalidate - callback to report INVALIDATE event
 * @param {Function} params.reportInvalidateRoom - callback to report INVALIDATE_ROOM event
 *
 * @return {object} transport SM interpret
 */
const transportInterpretFabric = ({
    transport,
    reportInvalidate,
    reportInvalidateRoom,
    logger,
    ...rest
}) => {
    const inter = interpret(
        transportMachineFabric({
            transport,
            ...rest,
        })
    )
        .onTransition((state, action) => {
            logger.debug(
                'TRANSITION:',
                state.value,
                getLoggableAction(action),
                'QUEUE:',
                state.context.queue,
                'VERSION:',
                state.context.version
            );
        })
        .start();

    const byChannel = send => event => {
        if (event.channel === rest.channelInfo.channel) {
            send(event);
        }
    };

    transport.on(
        TransportWrapper.EVENTS.DATA,
        byChannel(data => inter.send(TRANSPORT.DATA, data))
    );
    transport.on(
        TransportWrapper.EVENTS.PING,
        byChannel(data => inter.send(TRANSPORT.PING, data))
    );
    transport.on(
        TransportWrapper.EVENTS.INVALIDATE,
        byChannel(() => reportInvalidate({ reason: REASON.TRANSPORT_RECOVERY_FAIL }))
    );
    transport.on(
        TransportWrapper.EVENTS.INVALIDATE_ROOM,
        byChannel(event =>
            reportInvalidateRoom({
                reason: REASON.RCT_INVALIDATE_ROOM,
                roomId: event.data.roomId,
                roomName: event.data.roomName,
            })
        )
    );
    transport.on(TransportWrapper.EVENTS.RECONNECTED, () => inter.send(TRANSPORT.RECONNECTED));

    return inter;
};

/**
 * Initialize transport SM events
 * @param {Object} params
 * @param {import('../event-bus').EventBus} params.eventBus - event bus for all running state machines
 * @param {Function} params.pubnub - PubNub SDK
 * @param {TransportWrapper} params.transport - instance of TransportWrapper (can be send instead of pubnub sdk for tests)
 * @param {number} params.gapDelay - ms to wait on gap found
 * @param {number} params.transportReconnectDelay - ms to wait on transport connection error
 * @param {number} params.recoveryDelay - ms to wait without new messages
 * @param {number} params.connectRecoveryDelay - ms to wait on transport connecting
 * @param {number} params.successDebounce - ms to wait on transport debounce
 *
 * @return {object} transport SM interpret
 */
const transportEventsFabric = ({ id, eventBus, transport, getPrefixedLogger, ...rest }) => {
    const logger = getPrefixedLogger(STATE_MACHINES.TRANSPORT + ':' + id);
    const report = event => payload => {
        eventBus.send(event, { ...payload, channelId: id });
    };
    const reportData = data => {
        eventBus.send(MEETING.TRANSPORT_DATA, {
            channelId: id,
            origin: STATE_MACHINES.TRANSPORT,
            data,
        });
    };

    const inter = transportInterpretFabric({
        ...rest,
        id,
        transport,
        reportData,
        logger,
        reportTimeout: report(MEETING.TIMEOUT),
        reportInvalidate: report(MEETING.INVALIDATE),
        reportInvalidateRoom: report(MEETING.INVALIDATE_ROOM),
        reportVersionMismatch: report(CB_EVENTS.VERSION_MISMATCH),
    });

    const unlisteners = [
        eventBus.listen(
            [
                MEETING.LEAVE,
                MEETING.RECOVERED,
                MEETING.INVALIDATE,
                MEETING.INVALIDATE_ROOM,
                TRANSPORT.RECONNECTED,
            ],
            inter.send.bind(inter)
        ),
        eventBus.listen(
            [MEETING.PATCH_APPLIED, MEETING.NOTIFY_CHANNELS_UPDATE],
            (event, { notifyChannels }) => {
                if (notifyChannels[id]) {
                    inter.send(event, { channelInfo: notifyChannels[id] });
                }
            }
        ),
        eventBus.listen([MEETING.TIMEOUT], (event, data) => {
            if (data.channelId !== id) {
                inter.send(event, data);
            }
        }),
    ];

    inter.onDone(() => unlisteners.forEach(unlisten => unlisten()));

    return withAsyncAutoStop(inter, eventBus, logger);
};

const layoutTransportEventsFabric = ({ id, eventBus, transport, getPrefixedLogger, ...rest }) => {
    const logger = getPrefixedLogger(STATE_MACHINES.TRANSPORT + ':' + id);
    const report = event => payload => {
        eventBus.send(event, { ...payload, channelId: id });
    };
    const reportData = data => {
        eventBus.send(LAYOUTS.TRANSPORT_DATA, {
            channelId: id,
            origin: STATE_MACHINES.TRANSPORT,
            data,
        });
    };

    const inter = transportInterpretFabric({
        ...rest,
        id,
        transport,
        reportData,
        logger,
        reportTimeout: report(MEETING.TIMEOUT),
        reportInvalidate: report(MEETING.INVALIDATE),
        reportInvalidateRoom: report(MEETING.INVALIDATE_ROOM),
    });

    const unlisteners = [
        eventBus.listen(
            [MEETING.LEAVE, MEETING.INVALIDATE, MEETING.INVALIDATE_ROOM, TRANSPORT.RECONNECTED],
            inter.send.bind(inter)
        ),
        eventBus.listen([LAYOUTS.RECOVERED], (event, data) => inter.send(MEETING.RECOVERED, data)),
        eventBus.listen([LAYOUTS.PATCH_APPLIED], (event, data) =>
            inter.send(MEETING.PATCH_APPLIED, data)
        ),
        eventBus.listen([LAYOUTS.NOTIFY_CHANNELS_UPDATE], (event, data) =>
            inter.send(MEETING.NOTIFY_CHANNELS_UPDATE, data)
        ),
    ];

    inter.onDone(() => unlisteners.forEach(unlisten => unlisten()));

    return withAsyncAutoStop(inter, eventBus, logger);
};

module.exports = {
    default: transportInterpretFabric,
    transportEventsFabric,
    layoutTransportEventsFabric,
    TRANSPORT_EVENTS: EVENTS,
};
