const { Machine, assign, interpret } = require('xstate');
const {
    abortPendingRequest,
    abortControllerWrapper,
    withAsyncAutoStop,
    logTransition,
    timeout429,
} = require('./utils');
const { MEETING, CHAT, STATE_MACHINES } = require('./constants');
const { noop } = require('../utils');

const leaveEdges = {
    [MEETING.LEAVE]: 'stop',
    [MEETING.INVALIDATE]: 'reset',
    [MEETING.INVALIDATE_ROOM]: 'reset',
    [CHAT.TRANSPORT_REACTIVATE]: 'reactivate',
};

/**
 * @typedef {import('../apis/rch-api')} RchApi
 * @typedef {import('../event-bus').EventBus} EventBus
 */

/**
 * Initialize chat manager SM
 * @param {object} parameters
 * @param {RchApi} parameters.rchApi - initialized rchApi wrapper
 * @param {number} parameters.maxGetMetaDelay - max delay for getting meta
 * @param {number} parameters.reconnectDelay - base time to wait on network errors
 * @param {boolean} parameters.isPrivateChatsEnabled - flag for getting only public or all chats
 * @param {Function} parameters.reportActivateTransport - callback to report activated transport
 * @param {Function} parameters.reportReceiveData - callback to receive new data
 *
 * @return {object} chat manager SM
 */
const chatManagerMachineFabric = ({
    rchApi,
    maxGetMetaDelay = 60000,
    reconnectDelay = 3000,
    isPrivateChatsEnabled = false,
    reportActivateTransport = noop,
    reportReceiveData = noop,
    onChange = () => { },
    onChatMetaError = noop,
    logger,
} = {}) => {
    const getChatMessages = (chatIds, bridgeId, meetingId) => {
        return Promise.allSettled(
            chatIds.map(chatId =>
                rchApi
                    .getMessages({
                        bridgeId,
                        meetingId,
                        chatId,
                        limit: CHAT.MESSAGES_LIMIT,
                    })
                    .then(
                        data => ({ messages: data, chatId }),
                        err => {
                            err.chatId = chatId;
                            throw err;
                        })
            )
        )
    };
    // Calling chat messages for new chats is required in case when cached chat was recreated for participant who returned to room
    const getCommonChatMessages = ({ chatsById, newChatId, bridgeId, meetingId }) => newChatId
        ? getChatMessages([newChatId], bridgeId, meetingId)
        : getChatMessages(Object.keys(chatsById), bridgeId, meetingId);

    const getRejectedChatMessages = ({ bridgeId, meetingId, rejectedMessageRequests }) =>
        getChatMessages(rejectedMessageRequests.map(({ chatId }) => chatId), bridgeId, meetingId);


    return Machine(
        {
            id: 'chatManager',
            initial: 'idle',
            context: { connectAttempt: 0, chatsById: {} },
            states: {
                idle: {
                    on: {
                        [MEETING.CONNECTED]: {
                            target: 'gettingMeta',
                            actions: 'saveInitContext',
                        },
                        ...leaveEdges,
                    },
                },
                gettingMeta: {
                    exit: 'abortPendingRequest',
                    invoke: {
                        id: 'gettingMeta',
                        src: abortControllerWrapper(context =>
                            rchApi.getMeta(
                                {
                                    meetingId: context.meetingId,
                                    bridgeId: context.bridgeId,
                                },
                                context.signal
                            )
                        ),
                        onDone: {
                            target: 'creatingInitChat',
                            actions: ['resetConnectAttempt', 'saveContext'],
                        },
                        onError: {
                            target: 'handleGetMetaError',
                        },
                    },
                    on: {
                        ...leaveEdges,
                    },
                },
                handleGetMetaError: {
                    onEntry: ['incrementConnectAttempt', 'reportChatMetaError'],
                    on: {
                        ...leaveEdges,
                    },
                    after: {
                        GET_META_DELAY: {
                            target: 'gettingMeta',
                        },
                    },
                },
                creatingInitChat: {
                    invoke: {
                        id: 'creatingInitChat',
                        src: abortControllerWrapper(context =>
                            Promise.all([
                                rchApi.addChat(
                                    {
                                        bridgeId: context.bridgeId,
                                        meetingId: context.meetingId,
                                        chat: { type: 'public', roomId: context.roomId },
                                    },
                                    context.signal
                                ),
                                rchApi.addChat(
                                    {
                                        bridgeId: context.bridgeId,
                                        meetingId: context.meetingId,
                                        chat: { type: 'broadcast' },
                                    },
                                    context.signal
                                ),
                            ])
                        ),
                        onDone: {
                            target: 'activateTransport',
                        },
                        onError: {
                            target: 'handleCreatingChatError',
                        },
                    },
                    on: {
                        ...leaveEdges,
                    },
                },
                handleCreatingChatError: {
                    on: {
                        ...leaveEdges,
                    },
                    after: {
                        RECONNECT_DELAY: 'creatingInitChat',
                    },
                },
                activateTransport: {
                    onEntry: 'reportActivateTransport',
                    on: {
                        [CHAT.TRANSPORT_ACTIVATED]: {
                            actions: 'reportCreated',
                            target: 'readingNewChats',
                        },
                        ...leaveEdges,
                    },
                },
                readingNewChats: {
                    exit: 'abortPendingRequest',
                    invoke: {
                        id: 'readingNewChats',
                        src: abortControllerWrapper((context, event) => {
                            if (event.newChatId) {
                                context.newChatId = event.newChatId;
                            }
                            return isPrivateChatsEnabled
                                ? rchApi.getChats(
                                    {
                                        bridgeId: context.bridgeId,
                                        meetingId: context.meetingId,
                                    },
                                    context.signal
                                )
                                : rchApi.getPublicChats(
                                    {
                                        bridgeId: context.bridgeId,
                                        meetingId: context.meetingId,
                                    },
                                    context.signal
                                );
                        }),
                        onDone: {
                            target: 'gettingChatMessages',
                            actions: ['saveChats'],
                        },
                        onError: {
                            target: 'handleReadingNewChatsError',
                        },
                    },
                    on: {
                        ...leaveEdges,
                        [CHAT.TRANSPORT_TIMEOUT]: 'readingNewChats',
                    },
                },
                handleReadingNewChatsError: {
                    on: {
                        ...leaveEdges,
                    },
                    after: {
                        RECONNECT_DELAY: 'readingNewChats',
                    },
                },
                gettingChatMessages: {
                    exit: 'abortPendingRequest',
                    invoke: {
                        id: 'gettingMessages',
                        src: getCommonChatMessages,
                        onDone: {
                            target: 'receiveData',
                            actions: 'saveMessages',
                        },
                    },
                    on: {
                        ...leaveEdges,
                        [CHAT.TRANSPORT_TIMEOUT]: 'readingNewChats',
                    },
                },
                gettingRejectedChatMessages: {
                    exit: 'abortPendingRequest',
                    invoke: {
                        id: 'gettingRejectedChatMessages',
                        src: getRejectedChatMessages,
                        onDone: {
                            target: 'receiveData',
                            actions: 'saveMessages',
                        },
                    },
                    on: {
                        ...leaveEdges,
                        [CHAT.TRANSPORT_TIMEOUT]: 'readingNewChats',
                    },
                },
                receiveData: {
                    always: [{ actions: ['reportReceiveData', 'clearChatId'], target: 'working' }],
                },
                working: {
                    on: {
                        ...leaveEdges,
                        [CHAT.TRANSPORT_TIMEOUT]: 'readingNewChats',
                    },
                    after: {
                        TIMEOUT_429: { target: 'gettingRejectedChatMessages', cond: 'isRejectedRequestExists' },
                    }
                },
                reset: {
                    entry: 'reportLeave',
                    always: {
                        target: 'idle',
                    },
                },
                reactivate: {
                    entry: 'reportLeave',
                    always: 'gettingMeta',
                },
                stop: {
                    type: 'final',
                },
            },
        },
        {
            actions: {
                clearChatId: context => {
                    if (context.newChatId) {
                        delete context.newChatId;
                    }
                },
                reportReceiveData: ({ successMessages, chatsById }) => {
                    reportReceiveData({ successMessages, chatsById });
                },
                reportActivateTransport: context => {
                    reportActivateTransport({
                        channels: context.channels,
                        subscribeKey: context.subscribeKey,
                        authKey: context.authKey,
                    });
                },
                abortPendingRequest,
                saveInitContext: assign({
                    bridgeId: (context, event) => event.bridgeId,
                    meetingId: (context, event) => event.meetingId,
                    roomId: (context, event) => event.roomId,
                    participantId: (context, event) => event.participantId,
                    sessionId: (context, event) => event.sessionId,
                }),
                saveContext: assign({
                    channels: (context, event) => event.data.channel,
                    subscribeKey: (context, event) => event.data.notificationSubKey,
                    authKey: (context, event) => event.data.notificationToken,
                }),
                saveMessages: assign({
                    successMessages: (context, event) => {
                        const rejectedMessages = event.data.filter(el => el.status === 'rejected');
                        if (rejectedMessages.length > 0) {
                            logger.error(
                                'Error with getting chats messages',
                                JSON.stringify(rejectedMessages)
                            );
                        }
                        return event.data
                            .filter(el => el.status === 'fulfilled')
                            .reduce((acc, { value }) => {
                                acc[value['chatId']] = acc[value['chatId']]
                                    ? value
                                    : [...value.messages];

                                return acc;
                            }, {});
                    },
                    rejectedMessageRequests: (_, { data }) => data
                        .filter(({ status, reason }) => status === 'rejected' && reason.status === 429)
                        .map(el => ({
                            ...el.reason,
                            timeout: timeout429(el.reason)
                        }))
                }),
                saveChats: assign({
                    chatsById: (context, event) => {
                        const chatData = Array.isArray(event.data) ? event.data : [event.data];
                        return chatData.reduce((acc, el) => {
                            acc[el['id']] = el;

                            return acc;
                        }, {});
                    },
                }),
                incrementConnectAttempt: assign({
                    connectAttempt: context => context.connectAttempt + 1,
                }),
                resetConnectAttempt: assign({
                    connectAttempt: 0,
                }),
                reportCreated: () => onChange('chats-manager:created'),
                reportLeave: () => onChange('chats-manager:leave'),
                reportChatMetaError: (_, { data }) => {
                    onChatMetaError({ status: data?.status });
                },
            },
            delays: {
                GET_META_DELAY: context =>
                    Math.min(100 * Math.pow(2, context.connectAttempt), maxGetMetaDelay),
                RECONNECT_DELAY: reconnectDelay,
                TIMEOUT_429: context =>
                    Math.max(...context.rejectedMessageRequests
                        .map(req => req.timeout)),
            },
            guards: {
                isRejectedRequestExists: context =>
                    context.rejectedMessageRequests.length > 0
            },
        }
    );
};

/**
 * Initialize chat manager SM service
 * @param {object} parameters
 * @param {RchApi} parameters.rchApi - initialized rchApi wrapper
 * @param {number} parameters.maxGetMetaDelay - max delay for getting meta
 * @param {number} parameters.reconnectDelay - base time to wait on network errors
 * @param {boolean} parameters.isPrivateChatsEnabled - flag for getting only public or all chats
 * @param {Function} parameters.reportActivateTransport - callback to report activated transport
 * @param {Function} parameters.reportReceiveData - callback to receive new data
 *
 * @return {object} chat manager SM
 */
const chatManagerInterpretFabric = parameters => {
    const { logger } = parameters;

    return interpret(chatManagerMachineFabric(parameters))
        .onTransition(logTransition(logger))
        .start();
};

/**
 * Initialize chat manager SM events
 * @param {object} parameters
 * @property {EventBus} eventBus - emitter to listen events from outside
 *
 * @param {RchApi} parameters.rchApi - initialized rchApi wrapper
 * @param {number} parameters.maxGetMetaDelay - max delay for getting meta
 * @param {number} parameters.reconnectDelay - base time to wait on network errors
 * @param {boolean} parameters.isPrivateChatsEnabled - flag for getting only public or all chats
 * @param {Function} parameters.reportActivateTransport - callback to report activated transport
 * @param {Function} parameters.reportReceiveData - callback to receive new data
 *
 * @return {object} chat manager SM
 */
const chatManagerEventsFabric = ({ eventBus, getPrefixedLogger, ...rest }) => {
    const logger = getPrefixedLogger(STATE_MACHINES.CHAT_MANAGER);
    const reportActivateTransport = meta => {
        eventBus.send(CHAT.GET_META, { data: meta });
    };

    const reportReceiveData = data => {
        eventBus.send(CHAT.RECEIVE_NEW_DATA, { data });
    };

    const inter = chatManagerInterpretFabric({
        ...rest,
        reportActivateTransport,
        reportReceiveData,
        logger,
    });

    const unlisten = eventBus.listen(
        [
            MEETING.LEAVE,
            MEETING.CONNECTED,
            CHAT.TRANSPORT_TIMEOUT,
            CHAT.TRANSPORT_ACTIVATED,
            MEETING.INVALIDATE,
            MEETING.INVALIDATE_ROOM,
            CHAT.TRANSPORT_REACTIVATE,
        ],
        inter.send.bind(inter)
    );

    inter.onDone(unlisten);

    return withAsyncAutoStop(inter, eventBus, logger);
};

module.exports = {
    default: chatManagerInterpretFabric,
    chatManagerEventsFabric,
};
