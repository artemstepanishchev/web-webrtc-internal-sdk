const { Machine, interpret } = require('xstate');
const {
    abortPendingRequest,
    abortControllerWrapper,
    waitForOnline,
    isErrorStatus,
    timeout429,
    withAsyncAutoStop,
    logTransition,
} = require('./utils');
const { MEETING, STATE_MACHINES } = require('./constants');

const TYPES = {
    MEETING: 0,
    CALL: 1,
};

const defaultBridgeParams = {
    type: TYPES.MEETING,
};

const ERRORS = {
    LEAVE: 'Leave',
};

/**
 * @typedef {import('../apis/ndb-api').Bridge} Bridge
 * @typedef {import('../apis/ndb-api')} NdbApi
 * @typedef {import('../event-bus').EventBus} EventBus
 */

/**
 * Initialize bridge SM
 * @param {object} parameters
 * @param {NdbApi} parameters.ndbApi - initialized ndbApi wrapper
 * @param {Function} parameters.reportSuccess - success callback
 * @param {Function} parameters.reportError - error callback
 * @param {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @param {number} parameters.poorConnectionTime - time to report poor connection after
 * @param {Bridge} parameters.bridge - bridge params to create
 * @param {number} parameters.recoveryDelay - base time to wait on 5xx error
 *
 * @return {object} bridge SM
 */
const bridgeMachineFabric = ({
    ndbApi,
    reportSuccess,
    reportError,
    bridge,
    onPoorConnection,
    onBridgeConnected,
    poorConnectionTime,
}) =>
    Machine(
        {
            id: 'bridge',
            initial: 'checkOnline',
            context: {},
            states: {
                leaving: {
                    onEntry: 'reportLeaveCalled',
                    always: {
                        target: 'done',
                    },
                },
                done: {
                    type: 'final',
                },
                checkOnline: {
                    always: [
                        { target: 'requestBridge', cond: 'isOnline' },
                        { target: 'waitForOnline' },
                    ],
                },
                waitForOnline: {
                    invoke: {
                        id: 'waitForOnlineService',
                        src: waitForOnline,
                    },
                    on: {
                        [MEETING.ONLINE]: 'requestBridge',
                        [MEETING.LEAVE]: 'leaving',
                    },
                },
                requestBridge: {
                    exit: 'abortPendingRequest',
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                    },
                    after: {
                        POOR_CONNECTION: {
                            actions: 'onPoorConnection',
                        },
                    },
                    invoke: {
                        id: 'bridgeRequest',
                        src: abortControllerWrapper(context =>
                            // add logic for getting bridge by id
                            bridge.shortId
                                ? ndbApi.getBridgeByParams(bridge, context.signal)
                                : ndbApi.createBridge(
                                      {
                                          bridge: {
                                              ...defaultBridgeParams,
                                              ...bridge,
                                          },
                                      },
                                      context.signal
                                  )
                        ),
                        onDone: {
                            target: 'done',
                            actions: 'bridgeRequestSuccess',
                        },
                        onError: [
                            { target: 'checkOnline', cond: 'isOffline' },
                            { target: 'handle429', cond: 'is429' },
                            { target: 'done', actions: 'reportUnhandledError' },
                        ],
                    },
                },
                handle429: {
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                    },
                    after: {
                        TIMEOUT_429: 'checkOnline',
                    },
                },
            },
        },
        {
            actions: {
                abortPendingRequest,
                bridgeRequestSuccess: (context, event) => {
                    const { data: bridgeData } = event;
                    const {
                        id,
                        shortId,
                        name,
                        participantCode,
                        hostCode,
                        type,
                        isMeetingSecret,
                        accountId,
                        waitingRoomMode,
                        allowJoinBeforeHost,
                        joinUri,
                        e2ee,
                    } = bridgeData;

                    onBridgeConnected({
                        id,
                        shortId,
                        name,
                        participantCode,
                        hostCode,
                        type,
                        isMeetingSecret,
                        accountId,
                        waitingRoomMode,
                        allowJoinBeforeHost,
                        joinUri,
                        e2ee,
                    });
                    reportSuccess(bridgeData);
                },
                reportUnhandledError: (context, event) => reportError(event.data),
                reportLeaveCalled: () => reportError({ body: ERRORS.LEAVE }),
                onPoorConnection,
            },
            delays: {
                TIMEOUT_429: (context, { data }) => timeout429(data),
                POOR_CONNECTION: () => poorConnectionTime,
            },
            guards: {
                is429: isErrorStatus(429),
                isOnline: () => navigator.onLine,
                isOffline: () => !navigator.onLine,
            },
        }
    );

/**
 * Initialize bridge SM service
 * @param {object} parameters
 * @param {boolean} parameters.autoStart - auto start interpret (default true)
 *
 * @param {NdbApi} parameters.ndbApi - initialized ndbApi wrapper
 * @param {Bridge} parameters.bridge - bridge params to create
 * @param {Function} parameters.reportSuccess - success callback
 * @param {Function} parameters.reportError - error callback
 * @param {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @param {number} parameters.poorConnectionTime - time to report poor connection after
 * @param {number} parameters.recoveryDelay - base time to wait on 5xx error
 *
 * @return {object} bridge SM
 */
const bridgeInterpretFabric = ({ autoStart = true, logger, ...rest }) => {
    const inter = interpret(bridgeMachineFabric(rest)).onTransition(logTransition(logger));

    if (autoStart) {
        inter.start();
    }

    return inter;
};

/**
 * @typedef {Object} BridgeFabricParams
 * @property {NdbApi} ndbApi - initialized ndbApi wrapper
 * @property {Bridge} bridge - bridge params to create
 * @property {Function} onPoorConnection - callback to call when connection is poor
 * @property {number} poorConnectionTime - time to report poor connection after
 * @property {number} recoveryDelay - base time to wait on 5xx error
 * @property {EventBus} eventBus - emitter to listen events from outside
 */

/**
 * Promisified bridge SM service
 * @param {BridgeFabricParams} parameters
 *
 * @return {Promise.<Bridge>} that resolves when bridge information received, rejects unrecoverable error
 */
const bridgePromisifiedFabric = ({ eventBus, getPrefixedLogger, ...rest }) =>
    new Promise((reportSuccess, reject) => {
        const logger = getPrefixedLogger(STATE_MACHINES.BRIDGE);
        const reportError = errorData => reject({ origin: STATE_MACHINES.BRIDGE, ...errorData });
        const inter = bridgeInterpretFabric({
            ...rest,
            reportError,
            reportSuccess,
            logger,
        });

        const unlisten = eventBus.listen([MEETING.LEAVE], inter.send.bind(inter));

        inter.onDone(unlisten);

        return withAsyncAutoStop(inter, eventBus, logger);
    });

module.exports = {
    default: bridgeInterpretFabric,
    bridgePromisifiedFabric,
    BRIDGE_ERRORS: ERRORS,
    BRIDGE_TYPES: TYPES,
};
