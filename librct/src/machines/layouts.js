const { Machine, interpret, assign } = require('xstate');
const isEqual = require('lodash.isequal');
const { layoutTransportEventsFabric } = require('./transport');
const { createStore } = require('../store');
const { createCachedReporter } = require('../reducer');
const {
    abortControllerWrapper,
    timeout429,
    assignSameCtx,
    withAsyncAutoStop,
    logTransition,
} = require('./utils');
const { MEETING, STATE_MACHINES, LAYOUTS, LAYOUT_IDS, PN_CHANNELS } = require('./constants');
const { isErrorStatus, errorStartsWith } = require('./utils');

const leaveEdges = {
    [MEETING.LEAVE]: 'closed',
    [MEETING.INVALIDATE]: 'reset',
    [MEETING.INVALIDATE_ROOM]: 'reset',
};

/**
 * @typedef {import('../apis/rct-layouts-api')} RctLayoutsApi
 * @typedef {import('../event-bus').EventBus} EventBus
 * @typedef {import('./transport').ChannelInfo} ChannelInfo
 */

/**
 * @typedef {Object} LayoutsState
 * @property {ChannelInfo=} channelInfo - layout channel info
 * @property {Object=} layouts - layout state of a meeting, template of rooms and participants
 * @property {Object=} actual - actual rooms state of a meeting
 */

/**
 * Merges current state of layouts and PubNub patch
 * @param {LayoutsState} state - current state
 * @param {LayoutsState} patch - updated part of layouts state
 * @returns {LayoutsState} new layouts state
 */
const merge = (state, patch) => {
    if (!patch) return state;
    if (!state) return patch;

    const result = { ...state, ...patch };

    Object.keys(patch).forEach(key => {
        if (patch[key] && typeof patch[key] === 'object') {
            // TODO: create separate model and mapper if additional filtering will be required (see recalculator & mapper)
            if (patch[key].deleted) {
                delete result[key];
            } else {
                result[key] = merge(state[key], patch[key]);
            }
        }
    });

    return isEqual(result, state) ? state : result;
};

const extractSignificantData = patch => {
    const { channelInfo, actual, layouts } = patch;
    const significantData = {};
    if (channelInfo) significantData.channelInfo = channelInfo;
    if (actual) significantData.actual = actual;
    if (layouts) significantData.layouts = layouts;
    return significantData;
};

const layoutReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SET_STATE': {
            return merge(null, extractSignificantData(action.data));
        }
        case 'APPLY_PATCHES': {
            let newState = state;
            action.data.forEach(patch => {
                newState = merge(newState, extractSignificantData(patch));
            });
            return newState;
        }
        case 'RESET':
            return {};
        default:
            return state;
    }
};

const commonErrorHandlers = [
    { target: 'reset', cond: 'isOffline' },
    { target: 'handle5xx', cond: 'is5xx' },
    { target: 'handle429', cond: 'is429' },
    { target: 'idle' },
];

const layoutMachineFabric = ({
    rctLayoutsApi,
    invokeTransport,
    asyncReportPatchApplied,
    reportNotifyChannelsUpdate,
    reportRecovered,
    reportChanged,
    recoveryDelay = 3000,
}) => {
    const maybeReportChanged = state => {
        // layout state isn't ready until the user layout is created
        if (state.layouts?.[LAYOUT_IDS.USER]) {
            reportChanged(state);
        }
    };

    const getInitialContext = context => {
        const store = createStore(layoutReducer);
        store.subscribe(maybeReportChanged);
        return {
            store,
            retryCount: 0,
            channelInfo: null,
            isVersionRecovery: false,
            isInitialized: context?.isInitialized,
        };
    };

    return Machine(
        {
            id: 'layouts',
            initial: 'idle',
            context: getInitialContext(),
            states: {
                idle: {
                    on: {
                        ...leaveEdges,
                        [MEETING.CONNECTED]: [
                            {
                                target: 'waitingForInitialization',
                                actions: 'saveContext',
                                cond: 'isHost',
                            },
                            { target: 'closed' },
                        ],
                    },
                },
                waitingForInitialization: {
                    on: {
                        ...leaveEdges,
                        [LAYOUTS.GETTING_LAYOUTS]: {
                            target: 'gettingLayouts',
                            actions: 'setInitialized',
                        },
                    },
                    always: [{ target: 'gettingLayouts', cond: 'isReadyToGetLayouts' }],
                },
                gettingLayouts: {
                    invoke: {
                        id: 'gettingLayouts',
                        src: abortControllerWrapper(({ bridgeId, meetingId, signal }) =>
                            rctLayoutsApi.listLayouts({ bridgeId, meetingId }, signal)
                        ),
                        onDone: [
                            { target: 'completeVersionRecovery', cond: 'isVersionRecovery' },
                            { target: 'subcribeToPNChannel' },
                        ],
                        onError: commonErrorHandlers,
                    },
                    on: leaveEdges,
                },
                subcribeToPNChannel: {
                    entry: ['setState', 'invokeTransport'],
                    always: 'checkUserLayout',
                },
                completeVersionRecovery: {
                    entry: ['setState', 'reportNotifyChannelsUpdate'],
                    always: 'checkUserLayout',
                },
                checkUserLayout: {
                    always: [
                        { target: 'working', cond: 'userLayoutExists' },
                        { target: 'creatingUserLayout' },
                    ],
                },
                creatingUserLayout: {
                    invoke: {
                        id: 'creatingUserLayout',
                        src: abortControllerWrapper(({ bridgeId, meetingId, signal }) =>
                            rctLayoutsApi.createLayout({ bridgeId, meetingId }, signal)
                        ),
                        onDone: 'creatingDefaultRooms',
                        onError: [{ target: 'handle400', cond: 'is400' }, ...commonErrorHandlers],
                    },
                    on: leaveEdges,
                },
                creatingDefaultRooms: {
                    invoke: {
                        id: 'creatingDefaultRooms',
                        src: abortControllerWrapper(
                            ({ bridgeId, meetingId, signal, defaultRooms = {} }, event) => {
                                const layout = {
                                    rooms: {
                                        ...event.data.rooms,
                                        ...defaultRooms,
                                    },
                                };

                                return rctLayoutsApi.updateLayoutById(
                                    {
                                        bridgeId,
                                        meetingId,
                                        layoutId: LAYOUT_IDS.USER,
                                        layout,
                                    },
                                    signal
                                );
                            }
                        ),
                        // We need to re-request layouts in both cases:
                        // - after user layout and default rooms are created (to get the actual state and channel version)
                        // - after transport subscribes to layout PN channel it emits MEETING.TIMEOUT which should be listened to in the `working` state
                        onDone: 'versionRecovery',
                        onError: commonErrorHandlers,
                    },
                    on: leaveEdges,
                },
                working: {
                    entry: 'maybeReportRecovered',
                    on: {
                        ...leaveEdges,
                        [LAYOUTS.TRANSPORT_DATA]: { actions: 'applyPatch' },
                        [MEETING.TIMEOUT]: 'versionRecovery',
                    },
                },
                versionRecovery: {
                    entry: ['setVersionRecoveryInProgress', 'resetStore'],
                    always: 'gettingLayouts',
                },
                handle429: {
                    on: leaveEdges,
                    after: {
                        TIMEOUT_429: 'versionRecovery',
                    },
                },
                handle400: {
                    always: 'versionRecoveryAfterTimeout',
                },
                handle5xx: {
                    always: 'versionRecoveryAfterTimeout',
                },
                versionRecoveryAfterTimeout: {
                    on: leaveEdges,
                    after: {
                        RETRY_TIMEOUT: 'versionRecovery',
                    },
                    exit: 'incrementRetryCount',
                },
                reset: {
                    entry: 'resetContext',
                    always: 'idle',
                },
                closed: {
                    entry: ['resetStore', 'resetContext'],
                    type: 'final',
                },
            },
        },
        {
            actions: {
                saveContext: assign((context, event) => ({
                    bridgeId: event.bridgeId,
                    meetingId: event.meetingId,
                    authKey: event.transportInfo.authKey,
                    subscribeKey: event.transportInfo.subscribeKey,
                    appliedLayoutId: event.appliedLayoutId,
                })),
                setState: assignSameCtx((context, event) => {
                    context.store.dispatch({ type: 'SET_STATE', data: event.data });
                }),
                setVersionRecoveryInProgress: assign({ isVersionRecovery: true }),
                applyPatch: assignSameCtx((context, event) => {
                    const { channelInfo } = context.store.getState();
                    context.store.dispatch({ type: 'APPLY_PATCHES', data: event.data });
                    const newState = context.store.getState();

                    if (channelInfo !== newState.channelInfo) {
                        asyncReportPatchApplied({ channelInfo: newState.channelInfo });
                    }
                }),
                invokeTransport: ({ authKey, subscribeKey, store }) => {
                    invokeTransport(authKey, subscribeKey, store.getState().channelInfo);
                },
                reportNotifyChannelsUpdate: assignSameCtx(({ store }) =>
                    reportNotifyChannelsUpdate({
                        channelInfo: store.getState().channelInfo,
                    })
                ),
                incrementRetryCount: assign({
                    retryCount: context => context.retryCount + 1,
                }),
                resetContext: assign(getInitialContext),
                resetStore: assignSameCtx(context => {
                    context.store.dispatch({ type: 'RESET' });
                }),
                maybeReportRecovered: assign(({ isVersionRecovery }) => {
                    if (isVersionRecovery) {
                        reportRecovered();
                    }
                    return { isVersionRecovery: false };
                }),
                setInitialized: assign((context, event) => ({
                    isInitialized: true,
                    defaultRooms: event.defaultRooms,
                })),
            },
            guards: {
                isReadyToGetLayouts: ({ isInitialized, appliedLayoutId }) =>
                    isInitialized || appliedLayoutId === LAYOUT_IDS.USER,
                isHost: (context, event) => Boolean(event.isHost),
                is400: isErrorStatus(400),
                is429: isErrorStatus(429),
                is5xx: errorStartsWith('5'),
                isOffline: () => !navigator.onLine,
                isVersionRecovery: context => context.isVersionRecovery,
                userLayoutExists: context => LAYOUT_IDS.USER in context.store.getState().layouts,
            },
            delays: {
                RETRY_TIMEOUT: context =>
                    Math.min(20000, Math.pow(2, context.retryCount) * recoveryDelay),
                TIMEOUT_429: (context, { data }) => timeout429(data),
            },
        }
    );
};

const layoutInterpretFabric = ({ logger, ...rest }) => {
    const inter = interpret(layoutMachineFabric(rest))
        .onTransition(logTransition(logger))
        .start();

    return inter;
};

const layoutEventsFabric = ({
    eventBus,
    transport,
    onChange,
    // only for tests
    transportOptions,
    interpretFabric = layoutInterpretFabric,
    transportEventsFabric = layoutTransportEventsFabric,
    getPrefixedLogger,
    ...rest
}) => {
    const logger = getPrefixedLogger(STATE_MACHINES.LAYOUTS);

    const report = event => data => {
        eventBus.send(event, data);
    };

    const asyncReportPatchApplied = data => eventBus.send(LAYOUTS.PATCH_APPLIED, data);

    const invokeTransport = (authKey, subscribeKey, channelInfo) =>
        transportEventsFabric({
            id: PN_CHANNELS.LAYOUTS,
            eventBus,
            authKey: authKey,
            subscribeKey: subscribeKey,
            transport,
            channelInfo,
            getPrefixedLogger,
            ...transportOptions,
        });

    const inter = interpretFabric({
        ...rest,
        invokeTransport,
        asyncReportPatchApplied,
        reportChanged: createCachedReporter(['layouts', 'actual'], onChange),
        reportNotifyChannelsUpdate: report(LAYOUTS.NOTIFY_CHANNELS_UPDATE),
        reportRecovered: report(LAYOUTS.RECOVERED),
        logger,
    });

    const unlisten = eventBus.listen(
        [
            MEETING.CONNECTED,
            MEETING.LEAVE,
            MEETING.INVALIDATE,
            MEETING.INVALIDATE_ROOM,
            MEETING.TIMEOUT,
            LAYOUTS.TRANSPORT_DATA,
            LAYOUTS.GETTING_LAYOUTS,
        ],
        inter.send.bind(inter)
    );

    inter.onDone(unlisten);

    return withAsyncAutoStop(inter, eventBus, logger);
};

module.exports = { layoutInterpretFabric, layoutEventsFabric };
