const { assign, ActionTypes } = require('xstate');
require('abort-controller/polyfill');
const AbortController = require('abort-controller');
const { MEETING } = require('./constants');

/**
 * Wrapp API call with AbortController
 * @param {Function} cb - API call to wrap
 * @returns {Function} - SM action cb
 */
const abortControllerWrapper = cb => (context, ...args) => {
    context.controller = new AbortController();
    context.signal = context.controller.signal;

    return cb(context, ...args);
};

/**
 * Abort pending API request (ignores onDone and onError actions)
 * @param {Object} context - SM context
 * @param {Object} action - SM action
 */
const abortPendingRequest = (context, action) => {
    switch (true) {
        case action.type.startsWith(ActionTypes.DoneInvoke):
        case action.type.startsWith(ActionTypes.ErrorPlatform):
            context.controller = null;
            context.signal = null;
            return;
        default:
            if (context.controller) {
                context.controller.abort();
                context.controller = null;
                context.signal = null;
            }
            return;
    }
};

/**
 * Ignores onDone and onError action of SM state, triggers cb on other actions
 * @param {Function} cb - function to wrap
 * @returns {Function} - SM action cb
 */
const ignorePromiseActions = cb => (context, action) => {
    switch (true) {
        case action.type.startsWith(ActionTypes.DoneInvoke):
        case action.type.startsWith(ActionTypes.ErrorPlatform):
            return null;
        default:
            return cb(context, action);
    }
};

/**
 * Wait for online SM service
 * sends ONLINE actions on online
 */
const waitForOnline = () => callback => {
    const listener = () => callback(MEETING.ONLINE);
    window.addEventListener('online', listener);

    return () => window.removeEventListener('online', listener);
};

const isErrorStatus = status => (context, event) =>
    event?.data?.status === status;

const errorStartsWith = s => (context, event) =>
    event && event.data && event.data.status && event.data.status.toString().startsWith(s);

const timeout429 = (data, defaultTimeout = 5) =>
    ((data && data.headers && data.headers.get('Retry-After')) || defaultTimeout) * 1000;

/**
 * Asynchronously stops state machine after MEETING.LEAVE event
 * @param {Object} inter - SM interpret
 * @param {import('../event-bus').EventBus} eventBus - event bus for all running state machines
 * @param {PrefixedLogger} logger
 * @returns {Object} SM interpret
 */
const withAsyncAutoStop = (inter, eventBus, logger) => {
    const unlisten = eventBus.listen([MEETING.LEAVE], () => {
        unlisten();
        Promise.resolve().then(() => {
            inter.stop();
            logger.debug('State machine has been stopped');
        });
    });

    return inter;
};

/**
 * Returns the `assign` action, which runs the given function with the supplied context and event, then returns the same context.
 * Useful when it's required to perform an action in the current state, before transition to the next state (`assign` wrapper increases an action priority).
 * @param {Function} f - custom action
 * @returns {Function} - `assign` action
 */
const assignSameCtx = f =>
    assign((context, event) => {
        f(context, event);
        return context;
    });

/** Prepares SM action to be correctly logged */
const getLoggableAction = action =>
    // xstate internal actions always have only type, data and toString properties
    action?.hasOwnProperty('toString') ? { type: action.type, data: action.data } : action;

/** Properly logs machine transitions */
const logTransition = logger => (state, action) => {
    logger.debug('TRANSITION:', state.value, getLoggableAction(action));
};

module.exports = {
    abortPendingRequest,
    abortControllerWrapper,
    ignorePromiseActions,
    waitForOnline,
    isErrorStatus,
    errorStartsWith,
    timeout429,
    withAsyncAutoStop,
    assignSameCtx,
    getLoggableAction,
    logTransition,
};
