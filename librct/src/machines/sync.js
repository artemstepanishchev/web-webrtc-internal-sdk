const { Machine, assign, interpret } = require('xstate');
const {
    abortPendingRequest,
    abortControllerWrapper,
    withAsyncAutoStop,
    logTransition,
} = require('./utils');
const { MEETING, STATE_MACHINES } = require('./constants');

// null checking used to prevent sync without entity
const isSynced = c => c.remote === null || c.local === c.remote;

const leaveEdges = {
    [MEETING.LEAVE]: 'stop',
    [MEETING.INVALIDATE]: 'idle',
    [MEETING.INVALIDATE_ROOM]: 'idle',
};

const TYPES = {
    AUDIO: 'AUDIO_MUTE_CHANGE',
    VIDEO: 'VIDEO_MUTE_CHANGE',
    AUDIO_JOIN: 'AUDIO_JOIN_CHANGE',
};

/**
 * Initialize sync SM
 * @param {object} parameters
 * @param {Function} parameters.sync - function to sync property
 * @param {number} [parameters.successTO] - timeout after success request (default 10s)
 * @param {number} [parameters.minErrorTO] - timeout after failed request (default 1s)
 * @param {*} [parameters.initValue] - initial value to sync
 * @param {string} parameters.changeAction - action name for trigger sync check
 *
 * @return {object} sync SM
 */
const syncMachineFabric = ({
    sync,
    successTO = 10000,
    minErrorTO = 1000,
    initValue = false,
    changeAction,
}) =>
    Machine(
        {
            id: `sync:${changeAction}`,
            initial: 'idle',
            context: { remote: initValue, local: initValue, retryCount: 0 },
            states: {
                idle: {
                    on: {
                        [MEETING.CONNECTED]: {
                            target: 'syncCheck',
                            actions: 'saveContext',
                        },
                        [changeAction]: {
                            actions: 'saveContext',
                        },
                        ...leaveEdges,
                    },
                },
                stop: {
                    type: 'final',
                },
                syncCheck: {
                    always: [{ target: 'synced', cond: 'isSynced' }, { target: 'syncing' }],
                },
                synced: {
                    on: {
                        ...leaveEdges,
                        [changeAction]: { target: 'syncCheck', actions: 'saveContext' },
                    },
                },
                syncing: {
                    exit: ['abortPendingRequest'],
                    invoke: {
                        id: 'sync',
                        src: abortControllerWrapper(context =>
                            sync(
                                {
                                    value: context.local,
                                    bridgeId: context.bridgeId,
                                    meetingId: context.meetingId,
                                    participantId: context.participantId,
                                    sessionId: context.sessionId,
                                    streamId: context.streamId,
                                    isPstn: context.isPstn,
                                },
                                context.signal
                            )
                        ),
                        onDone: {
                            target: 'successWaiting',
                            action: 'syncSuccess',
                        },
                        onError: {
                            target: 'handleError',
                        },
                    },
                    on: {
                        ...leaveEdges,
                        [changeAction]: { target: 'syncCheck', actions: 'saveContext' },
                    },
                },
                successWaiting: {
                    after: {
                        SUCCESS_WAITING: 'syncCheck',
                    },
                    on: {
                        ...leaveEdges,
                        [changeAction]: { target: 'syncCheck', actions: 'saveContext' },
                    },
                },
                handleError: {
                    exit: 'incrementRetryCount',
                    after: {
                        RETRY_DELAY: 'syncCheck',
                    },
                    on: {
                        ...leaveEdges,
                        [changeAction]: { target: 'syncCheck', actions: 'saveContext' },
                    },
                },
            },
        },
        {
            actions: {
                abortPendingRequest,
                saveContext: assign((context, event) => ({
                    local: event.hasOwnProperty('local') ? event.local : context.local,
                    remote: event.hasOwnProperty('remote') ? event.remote : context.remote,
                    retryCount: 0,

                    streamId: event.hasOwnProperty('streamId') ? event.streamId : context.streamId,
                    sessionId: event.hasOwnProperty('sessionId')
                        ? event.sessionId
                        : context.sessionId,
                    bridgeId: event.hasOwnProperty('bridgeId') ? event.bridgeId : context.bridgeId,
                    participantId: event.hasOwnProperty('participantId')
                        ? event.participantId
                        : context.participantId,
                    meetingId: event.hasOwnProperty('meetingId')
                        ? event.meetingId
                        : context.meetingId,

                    isPstn: event.hasOwnProperty('isPstn') ? event.isPstn : context.isPstn,
                })),
                syncSuccess: assign({
                    retryCount: 0,
                }),
                incrementRetryCount: assign({ retryCount: context => context.retryCount + 1 }),
            },
            delays: {
                SUCCESS_WAITING: () => successTO,
                RETRY_DELAY: context =>
                    Math.min(Math.pow(2, context.retryCount) * minErrorTO, 10000),
            },
            guards: {
                isSynced,
            },
        }
    );

/**
 * Initialize sync SM service
 * @param {object} parameters
 * @param {Function} parameters.sync - function to sync property
 * @param {number} parameters.successTO - timeout after success request (default 10s)
 * @param {number} parameters.minErrorTO - timeout after failed request (default 1s)
 * @param {*} [parameters.initValue] - initial value to sync
 * @param {string} parameters.changeAction - action name for trigger sync check
 *
 * @return {object} sync SM interpret
 */
const syncInterpretFabric = ({
    sync,
    successTO = 10000,
    minErrorTO = 1000,
    initValue = false,
    changeAction,
    logger,
}) =>
    interpret(syncMachineFabric({ sync, successTO, minErrorTO, initValue, changeAction }))
        .onTransition(logTransition(logger))
        .start();

/**
 * Initialize sync SM events
 * @param {object} parameters
 * @param {import('../event-bus').EventBus} parameters.eventBus - event bus for all running state machines
 *
 * @param {Function} parameters.sync - function to sync property
 * @param {number} parameters.successTO - timeout after success request (default 10s)
 * @param {number} parameters.minErrorTO - timeout after failed request (default 1s)
 * @param {*} [parameters.initValue] - initial value to sync
 * @param {string} parameters.changeAction - action name for trigger sync check
 *
 * @return {object} sync SM interpret
 */
const syncEventsFabric = ({ eventBus, type, getPrefixedLogger, ...rest }) => {
    const logger = getPrefixedLogger(`${STATE_MACHINES.SYNC}:${type}`);

    const inter = syncInterpretFabric({
        changeAction: type,
        logger,
        ...rest,
    });

    const unlisten = eventBus.listen(
        [MEETING.LEAVE, MEETING.CONNECTED, MEETING.INVALIDATE, MEETING.INVALIDATE_ROOM, type],
        inter.send.bind(inter)
    );

    inter.onDone(unlisten);

    return withAsyncAutoStop(inter, eventBus, logger);
};

module.exports = {
    default: syncInterpretFabric,
    syncEventsFabric,
    TYPES,
};
