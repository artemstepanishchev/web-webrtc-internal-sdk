// event bus
const MEETING = {
    CONNECTED: 'MEETING_CONNECTED',
    RECOVERED: 'MEETING_RECOVERED',
    FAILED: 'MEETING_FAILED',
    LEAVE: 'LEAVE',
    INVALIDATE: 'INVALIDATE',
    INVALIDATE_ROOM: 'INVALIDATE_ROOM',
    TRANSPORT_DATA: 'TRANSPORT_DATA',
    TIMEOUT: 'TIMEOUT',
    PASSWORD: 'PASSWORD',
    ONLINE: 'ONLINE',
    JOIN: 'JOIN',
    NOTIFY_CHANNELS_UPDATE: 'NOTIFY_CHANNELS_UPDATE',
    PATCH_APPLIED: 'PATCH_APPLIED',
};

// https://wiki.ringcentral.com/display/RCV/RCV+API+-+Recovery+modes
const RECOVERY = {
    FULL: 'full', // initiated by INVALIDATE event
    SOFT: 'soft', // - by INVALIDATE_ROOM event
    VERSION: 'version', // - when some of PubNub messages are lost
};

const TRANSPORT = {
    DATA: 'TRANSPORT_DATA',
    PING: 'TRANSPORT_PING',
    RECONNECTED: 'TRANSPORT_RECONNECTED',
    UNKNOWN_ERROR: 'TRANSPORT_UNKNOWN_ERROR',
    MIN_RECOVERY_DELAY_MS: 20 * 1000,
};

const CHAT = {
    TRANSPORT_TIMEOUT: 'CHAT_TRANSPORT_TIMEOUT',
    TRANSPORT_ACTIVATED: 'CHAT_TRANSPORT_ACTIVATED',
    TRANSPORT_REACTIVATE: 'TRANSPORT_REACTIVATE',
    GET_META: 'GET_META',
    RECEIVE_NEW_DATA: 'RECEIVE_NEW_DATA',
    NEW_CHAT_FROM_TRANSPORT: 'NEW_CHAT_FROM_TRANSPORT',
    RECEIVE_CHAT_MESSAGE: 'RECEIVE_CHAT_MESSAGE',
    MESSAGES_LIMIT: 1000,
};

const LAYOUTS = {
    RECOVERED: 'LAYOUTS_RECOVERED',
    TRANSPORT_DATA: 'LAYOUTS_TRANSPORT_DATA',
    NOTIFY_CHANNELS_UPDATE: 'LAYOUTS_NOTIFY_CHANNELS_UPDATE',
    PATCH_APPLIED: 'LAYOUTS_PATCH_APPLIED',
    GETTING_LAYOUTS: 'GETTING_LAYOUTS',
};

const LAYOUT_IDS = {
    MAIN: 'main',
    USER: 'user',
};

const CB_EVENTS = {
    PASSWORD_CHANGE: 'passwordChange',
    ONLY_COWORKERS_JOIN: 'onlyCoworkersJoin',
    ONLY_AUTH_USER_JOIN: 'onlyAuthUserJoin',
    CAPACITY_LIMIT: 'capacityLimit',
    SIMULTANEOUS_LIMIT: 'simultaneousLimit',
    WAITING_FOR_HOST: 'waitingForHost',
    WAITING_ROOM_AWAIT: 'waitingRoomAwait',
    WAITING_ROOM_REJECT: 'waitingRoomReject',
    WAITING_ROOM_APPROVED: 'waitingRoomApproved',
    LEAVE: 'leave',
    POOR_CONNECTION: 'poorConnection',
    LOCKED_ERROR: 'lockedError',
    RECOVERY: 'recovery',
    RECOVERY_SUCCESS: 'recovery:success',
    RECOVERY_FAILURE: 'recovery:failure',
    BRIDGE_CONNECTED: 'bridgeConnected',
    MEETING_SERVER_ERROR: 'meetingServerError',
    CHAT_META_ERROR: 'chatMetaError',
    VERSION_MISMATCH: 'versionMismatch',
};

const PASSWORD_STATE = {
    REJECTED: 'REJECTED',
    REQUIRED: 'REQUIRED',
    ACCEPTED: 'ACCEPTED',
};

const SESSION_DELETED_CODES = {
    TIMEOUT: 3,
};

const STATE_MACHINES = {
    MEETING: 'librct:SM:Meeting',
    BRIDGE: 'librct:SM:Bridge',
    ROOT: 'librct:SM:Root',
    TRANSPORT: 'librct:SM:transport',
    CHAT_MANAGER: 'librct:SM:chat-manager',
    CHAT_TRANSPORT: 'librct:SM:chat-transport',
    PONG: 'librct:SM:Pong',
    SYNC: 'librct:SM:Sync',
    LAYOUTS: 'librct:SM:layouts',
};

const PN_CHANNELS = {
    MEETING: 'meeting',
    MEETING_PARTICIPANTS: 'meetingParticipants',
    PRIVATE: 'private',
    LAYOUTS: 'layouts',
};

const REASON = {
    TRANSPORT_RECOVERY_FAIL: 'Failed to recover transport',
    MEETING_RECOVERY_FAIL: 'Failed to recover meeting state',
    RCT_INVALIDATE_ROOM: 'invalidate_room event received from private channel',
    ROOM_ID_CHANGED:
        'Room id has changed unexpectedly. The PubNub private channel event was probably lost.',
    LOCAL_SESSION_TIMEOUT: 'Local session timeout',
};

module.exports = {
    MEETING,
    RECOVERY,
    TRANSPORT,
    CHAT,
    LAYOUTS,
    LAYOUT_IDS,
    CB_EVENTS,
    PASSWORD_STATE,
    SESSION_DELETED_CODES,
    STATE_MACHINES,
    PN_CHANNELS,
    REASON,
};
