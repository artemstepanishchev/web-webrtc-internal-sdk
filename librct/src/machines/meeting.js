const { Machine, interpret, assign } = require('xstate');
const {
    abortPendingRequest,
    abortControllerWrapper,
    waitForOnline,
    isErrorStatus,
    errorStartsWith,
    timeout429,
    withAsyncAutoStop,
    logTransition,
} = require('./utils');
const get = require('lodash.get');
const { MEETING, PASSWORD_STATE, STATE_MACHINES, CB_EVENTS } = require('./constants');

const defaultMeetingParams = {};

const ERRORS = {
    LEAVE: 'Leave',
    ONLY_AUTH_USER_JOIN: CB_EVENTS.ONLY_AUTH_USER_JOIN,
    ONLY_COWORKERS_JOIN: CB_EVENTS.ONLY_COWORKERS_JOIN,
    CAPACITY_LIMIT: CB_EVENTS.CAPACITY_LIMIT,
    SIMULTANEOUS_LIMIT: CB_EVENTS.SIMULTANEOUS_LIMIT,
};

const ERROR_CODES = {
    PASSWORD_REQUIRED: 'c283d8cb-81ac-4cbd-a09d-07871577c7a1',
    LOCKED: 'c898e3ac-816d-46b7-97fc-0d66fb09cdd2',
    JOIN_BEFORE_HOST: 'f128a1b3-940b-4b1a-bdbf-4b7a02ba6c9c',
    ONLY_COWORKERS_JOIN_ERROR: '1625375d-58b3-456e-b9cd-6a4f0443c9d6',
    ONLY_AUTH_USER_JOIN_ERROR: '572e191f-5cea-4dec-ba80-2539584b24d9',
    CAPACITY_LIMIT_ERROR: '88cc509a-bf72-4277-ba2b-3bb6c87ffdf6',
    SIMULTANEOUS_LIMIT_ERROR: 'ad797797-9269-457f-b445-0e96e3916cd7',
    WAITING_ROOM_AWAIT: 'b8c643e0-1be2-43e8-8a19-3cf86f821725',
    WAITING_ROOM_REJECT: '6f01ebb2-1a6f-4b36-82af-b87cb708c415',
};

const checkError = errorCode => (context, event) => {
    const status = get(event, 'data.status', -1);
    const code = get(event, 'data.body.error.code', '');

    return (status === 400 || status === 403 || status === 423) && code === errorCode;
};

const getInitialPassword = bridgeId => {
    const passwordData = JSON.parse(sessionStorage.getItem('password_')) || {};
    return passwordData.id === bridgeId && passwordData.isAccepted ? passwordData.value : null;
};

const calculateCapabilities = () => {
    const e2eeSupported = !!(
        typeof RTCRtpSender !== 'undefined' &&
        RTCRtpSender &&
        RTCRtpSender?.prototype?.createEncodedStreams
    );

    return e2eeSupported ? 'e2ee' : '';
};
/**
 * typedefs imports for vscode
 * @typedef {import('../apis/rct-api').Call} Call
 * @typedef {import('../apis/rct-api')} Rct
 * @typedef {import('../event-bus').EventBus} EventBus
 */

/**
 * Initialize bridge SM
 * @param {object} parameters
 * @param {Rct} parameters.rctApi - initialized rctApi wrapper
 * @param {Call} parameters.meeting - meeting params to create
 * @param {string} parameters.bridgeId - bridge id from ndb
 * @param {Function} parameters.reportSuccess - success callback
 * @param {Function} parameters.reportError - error callback
 * @param {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @param {Function} parameters.onPasswordRequire - callback that called when meeting is locked by password
 * @param {Function} parameters.onJoinBeforeHostError - callback that called when meeting is locked by join before host check
 * @param {Function} parameters.onLockedError - callback that called when meeting is locked
 * @param {Function} parameters.onOnlyCoworkersJoin - callback to report only coworkers restriction
 * @param {Function} parameters.onOnlyAuthUserJoin - callback to report only auth user restriction
 * @param {number} parameters.poorConnectionTime - time to report poor connection after
 * @param {number} parameters.recoveryDelay - base time to wait on 5xx error
 *
 * @return {object} bridge SM
 */
const meetingMachineFabric = ({
    rctApi,
    reportSuccess,
    reportError,
    bridgeId,
    meeting,
    onPoorConnection,
    onPasswordChange,
    onJoinBeforeHostError,
    onLockedError,
    onOnlyCoworkersJoin,
    onOnlyAuthUserJoin,
    onCapacityLimit,
    onSimultaneousLimit,
    poorConnectionTime,
    recoveryDelay = 3000,
    JBHTimeout = 5000,
    waitingRoomTimeout = 5000,
    onWaitingRoomAwait,
    onWaitingRoomReject,
    onWaitingRoomApproved,
    parentId,
    onMeetingServerError,
}) =>
    Machine(
        {
            id: 'meeting',
            initial: 'checkOnline',
            context: { retryCount: 0, password: getInitialPassword(bridgeId) },
            states: {
                leaving: {
                    onEntry: 'reportLeaveCalled',
                    always: {
                        target: 'done',
                    },
                },
                done: {
                    type: 'final',
                },
                checkOnline: {
                    always: [
                        { target: 'requestMeeting', cond: 'isOnline' },
                        { target: 'waitForOnline' },
                    ],
                },
                waitForOnline: {
                    invoke: {
                        id: 'waitForOnlineService',
                        src: waitForOnline,
                    },
                    on: {
                        [MEETING.ONLINE]: 'requestMeeting',
                        [MEETING.LEAVE]: 'leaving',
                    },
                },
                requestMeeting: {
                    exit: 'abortPendingRequest',
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                    },
                    after: {
                        POOR_CONNECTION: {
                            actions: 'onPoorConnection',
                        },
                    },
                    invoke: {
                        id: 'meetingRequest',
                        src: abortControllerWrapper(context =>
                            rctApi.createMeeting(
                                {
                                    bridgeId,
                                    call: {
                                        ...defaultMeetingParams,
                                        ...meeting,
                                        ...(context.password
                                            ? { meetingPassword: context.password }
                                            : {}),
                                        ...(parentId ? { parentId } : {}),
                                    },
                                    xRcvInMeetingCapabilities: calculateCapabilities(),
                                },
                                context.signal
                            )
                        ),
                        onDone: {
                            target: 'done',
                            actions: 'meetingRequestSuccess',
                        },
                        onError: [
                            { target: 'checkOnline', cond: 'isOffline' },
                            { target: 'handleOnlyCoworkersJoin', cond: 'isOnlyCoworkersJoinError' },
                            { target: 'handleOnlyAuthUserJoin', cond: 'isOnlyAuthUserJoinError' },
                            { target: 'handleCapacityLimit', cond: 'isCapacityLimit' },
                            { target: 'handleSimultaneousLimit', cond: 'isSimultaneousLimit' },
                            { target: 'handlePasswordRequire', cond: 'isPasswordRequired' },
                            { target: 'handleWaitingRoomAwait', cond: 'isWaitingRoomAwait' },
                            { target: 'handleWaitingRoomReject', cond: 'isWaitingRoomReject' },
                            {
                                target: 'handleJoinBeforeHostError',
                                cond: 'isJoinBeforeHostError',
                            },
                            {
                                target: 'handleLockedError',
                                cond: 'isLockedError',
                            },
                            {
                                target: 'handleExponentialRetryError',
                                actions: 'reportServerError',
                                cond: 'isServerError',
                            },
                            { target: 'handle429', cond: 'is429' },
                            { target: 'done', actions: 'reportUnhandledError' },
                        ],
                    },
                },
                handleOnlyCoworkersJoin: {
                    onEntry: 'reportOnlyCoworkersJoin',
                    always: {
                        target: 'leaving',
                    },
                },
                handleOnlyAuthUserJoin: {
                    onEntry: 'reportOnlyAuthUserJoin',
                    always: {
                        target: 'leaving',
                    },
                },
                handleCapacityLimit: {
                    onEntry: 'reportCapacityLimit',
                    always: {
                        target: 'leaving',
                    },
                },
                handleSimultaneousLimit: {
                    onEntry: 'reportSimultaneousLimit',
                    always: {
                        target: 'leaving',
                    },
                },
                handlePasswordRequire: {
                    onEntry: 'reportPasswordRequired',
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                        [MEETING.PASSWORD]: {
                            target: 'checkOnline',
                            actions: 'savePassword',
                        },
                    },
                },
                handleJoinBeforeHostError: {
                    onEntry: 'reportJoinBeforeHostError',
                    always: {
                        target: 'handleJBHError',
                    },
                },
                handleLockedError: {
                    onEntry: 'reportLockedError',
                    always: {
                        target: 'handleExponentialRetryError',
                    },
                },
                handle429: {
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                    },
                    after: {
                        TIMEOUT_429: 'checkOnline',
                    },
                },
                handleWaitingRoomAwait: {
                    onEntry: 'reportWaitingRoomAwait',
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                    },
                    after: {
                        WAITING_ROOM_TIMEOUT: 'checkOnline',
                    },
                },
                handleWaitingRoomReject: {
                    onEntry: 'reportWaitingRoomReject',
                    always: {
                        target: 'done',
                    },
                },
                handleJBHError: {
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                    },
                    after: {
                        JBH_TIMEOUT: 'checkOnline',
                    },
                },
                handleExponentialRetryError: {
                    on: {
                        [MEETING.LEAVE]: 'leaving',
                    },
                    after: {
                        TIMEOUT_5XX: 'checkOnline',
                    },
                    onExit: 'incrementRetryCount',
                },
            },
        },
        {
            actions: {
                abortPendingRequest,
                meetingRequestSuccess: (context, event) => {
                    if (context.joinBeforeHostReported) {
                        onJoinBeforeHostError(false);
                    }
                    if (context.lockedReported) {
                        onLockedError(false);
                    }
                    if (context.waitingRoomAwaitReported) {
                        context.waitingRoomAwaitReported = false;
                        context.waitingRoomAwaitReportedConferenceActive = null;
                        onWaitingRoomApproved();
                    }
                    if (!context.passwordAcceptedReported) {
                        context.passwordAcceptedReported = true;
                        onPasswordChange({
                            type: PASSWORD_STATE.ACCEPTED,
                            data: { id: bridgeId, password: context.password },
                        });
                    }
                    reportSuccess(event.data);
                },
                reportUnhandledError: (context, event) => reportError(event.data),
                reportLeaveCalled: context =>
                    reportError({
                        body: context.joinErrorBody ?? ERRORS.LEAVE,
                    }),
                reportPasswordRequired: context => {
                    if (!context.passwordRequiredReported) {
                        context.passwordRequiredReported = true;
                        onPasswordChange({ type: PASSWORD_STATE.REQUIRED });
                    } else {
                        onPasswordChange({ type: PASSWORD_STATE.REJECTED });
                    }
                },
                reportWaitingRoomAwait: (context, event) => {
                    if (!context.passwordAcceptedReported && context.passwordRequiredReported) {
                        context.passwordAcceptedReported = true;
                        onPasswordChange({
                            type: PASSWORD_STATE.ACCEPTED,
                            data: { id: bridgeId, password: context.password },
                        });
                    }
                    const isConferenceActive = event?.data?.body?.conferenceActive;

                    if (
                        !context.waitingRoomAwaitReported ||
                        isConferenceActive !== context.waitingRoomAwaitReportedConferenceActive
                    ) {
                        context.waitingRoomAwaitReported = true;
                        context.waitingRoomAwaitReportedConferenceActive = isConferenceActive;
                        onWaitingRoomAwait(isConferenceActive);
                    }
                },
                reportWaitingRoomReject: () => onWaitingRoomReject(),
                reportJoinBeforeHostError: context => {
                    if (!context.passwordAcceptedReported) {
                        context.passwordAcceptedReported = true;
                        onPasswordChange({
                            type: PASSWORD_STATE.ACCEPTED,
                            data: { id: bridgeId, password: context.password },
                        });
                    }
                    if (!context.joinBeforeHostReported) {
                        context.joinBeforeHostReported = true;
                        onJoinBeforeHostError(true);
                    }
                },
                reportLockedError: context => {
                    if (!context.lockedReported) {
                        context.lockedReported = true;
                        onLockedError(true);
                    }
                },
                reportOnlyCoworkersJoin: context => {
                    context.joinErrorBody = ERRORS.ONLY_COWORKERS_JOIN;
                    onOnlyCoworkersJoin();
                },
                reportOnlyAuthUserJoin: context => {
                    context.joinErrorBody = ERRORS.ONLY_AUTH_USER_JOIN;
                    onOnlyAuthUserJoin();
                },
                reportCapacityLimit: context => {
                    context.joinErrorBody = ERRORS.CAPACITY_LIMIT;
                    onCapacityLimit();
                },
                reportSimultaneousLimit: (context, event) => {
                    context.joinErrorBody = ERRORS.SIMULTANEOUS_LIMIT;
                    onSimultaneousLimit({
                        hostingLimit: event.data?.body?.error?.resource?.hostingLimit ?? null,
                    });
                },
                reportServerError: (_, { data }) => {
                    onMeetingServerError({
                        status: data?.status,
                        bridgeId,
                    });
                },
                onPoorConnection,
                incrementRetryCount: assign({
                    retryCount: context => context.retryCount + 1,
                }),
                savePassword: assign({
                    password: (context, event) => get(event, 'password', null),
                }),
            },
            delays: {
                TIMEOUT_5XX: context =>
                    Math.min(20000, Math.pow(2, context.retryCount) * recoveryDelay),
                TIMEOUT_429: (context, { data }) => timeout429(data),
                POOR_CONNECTION: () => poorConnectionTime,
                JBH_TIMEOUT: () => JBHTimeout,
                WAITING_ROOM_TIMEOUT: () => waitingRoomTimeout,
            },
            guards: {
                isPasswordRequired: checkError(ERROR_CODES.PASSWORD_REQUIRED),
                isWaitingRoomAwait: checkError(ERROR_CODES.WAITING_ROOM_AWAIT),
                isWaitingRoomReject: checkError(ERROR_CODES.WAITING_ROOM_REJECT),
                isJoinBeforeHostError: checkError(ERROR_CODES.JOIN_BEFORE_HOST),
                isOnlyCoworkersJoinError: checkError(ERROR_CODES.ONLY_COWORKERS_JOIN_ERROR),
                isOnlyAuthUserJoinError: checkError(ERROR_CODES.ONLY_AUTH_USER_JOIN_ERROR),
                isCapacityLimit: checkError(ERROR_CODES.CAPACITY_LIMIT_ERROR),
                isSimultaneousLimit: checkError(ERROR_CODES.SIMULTANEOUS_LIMIT_ERROR),
                isLockedError: checkError(ERROR_CODES.LOCKED),
                is429: isErrorStatus(429),
                isServerError: (context, event) =>
                    errorStartsWith('5')(context, event) || isErrorStatus(449)(context, event),
                isOnline: () => navigator.onLine,
                isOffline: () => !navigator.onLine,
            },
        }
    );

/**
 * Initialize meeting SM service
 * @param {object} parameters
 * @param {boolean} parameters.autoStart - auto start interpret (default true)
 *
 * @param {RctApi} parameters.rctApi - initialized rctApi wrapper
 * @param {Call} parameters.meeting - meeting params to create
 * @param {string} parameters.bridgeId - bridge id from ndb
 * @param {Function} parameters.reportSuccess - success callback
 * @param {Function} parameters.reportError - error callback
 * @param {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @param {Function} parameters.onPasswordRequire - callback that called when meeting is locked by password
 * @param {Function} parameters.onJoinBeforeHostError - callback that called when meeting is locked by join before host check
 * @param {Function} parameters.onLockedError - callback that called when meeting is locked
 * @param {number} parameters.poorConnectionTime - time to report poor connection after
 * @param {number} parameters.recoveryDelay - base time to wait on 5xx error
 *
 * @return {object} meeting SM
 */
const meetingInterpretFabric = ({ autoStart = true, logger, ...rest }) => {
    const inter = interpret(meetingMachineFabric(rest)).onTransition(logTransition(logger));

    if (autoStart) {
        inter.start();
    }

    return inter;
};

/**
 * @typedef {Object} MeetingFabricParams
 *
 * @property {RctApi} parameters.rctApi - initialized rctApi wrapper
 * @property {Meeting} parameters.meeting - meeting params to create
 * @property {string} parameters.bridgeId - bridge id from ndb
 * @property {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @property {Function} parameters.onPasswordRequire - callback that called when meeting is locked by password
 * @property {Function} parameters.onJoinBeforeHostError - callback that called when meeting is locked by join before host check
 * @property {Function} parameters.onLockedError - callback that called when meeting is locked
 * @property {number} parameters.poorConnectionTime - time to report poor connection after
 * @property {number} parameters.recoveryDelay - base time to wait on 5xx error
 * @property {EventBus} eventBus - emitter to listen events from outside
 */

/**
 * Promisified meeting SM service
 * @param {MeetingFabricParams} parameters
 *
 * @return {Promise} that resolves when meeting information received, rejects unrecoverable error
 */
const meetingPromisifiedFabric = ({ eventBus, getPrefixedLogger, ...rest }) =>
    new Promise((reportSuccess, reject) => {
        const logger = getPrefixedLogger(STATE_MACHINES.MEETING);
        const reportError = errorData => reject({ origin: STATE_MACHINES.MEETING, ...errorData });
        const inter = meetingInterpretFabric({
            ...rest,
            reportError,
            reportSuccess,
            logger,
        });

        const unlisten = eventBus.listen([MEETING.LEAVE, MEETING.PASSWORD], inter.send.bind(inter));

        inter.onDone(unlisten);

        return withAsyncAutoStop(inter, eventBus, logger);
    });

module.exports = {
    default: meetingInterpretFabric,
    meetingPromisifiedFabric,
    meetingErrors: ERRORS,
};
