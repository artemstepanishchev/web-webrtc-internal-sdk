const { Machine, interpret, assign } = require('xstate');
const { bridgePromisifiedFabric } = require('./bridge');
const { meetingPromisifiedFabric } = require('./meeting');
const transportEventsFabric = require('./transport').transportEventsFabric;
const { TYPES: SYNC_TYPES } = require('./sync');
const {
    abortPendingRequest,
    abortControllerWrapper,
    isErrorStatus,
    errorStartsWith,
    timeout429,
    withAsyncAutoStop,
    logTransition,
} = require('./utils');
const { muteStateController: simpleMuteStateController } = require('./misc');
const { types, createReducer, createCachedReporter } = require('../reducer');
const { createStore } = require('../store');
const {
    MEETING,
    SESSION_DELETED_CODES,
    STATE_MACHINES,
    PN_CHANNELS,
    RECOVERY,
    REASON,
} = require('./constants');
const { arrify, noop } = require('../utils');

const ERRORS = {
    LEAVE: 'Leave',
};

const goToAfterDelay = (dest, delay) => ({
    on: {
        [MEETING.LEAVE]: 'done',
    },
    after: {
        [delay]: dest,
    },
});

// common parts of recovery logic
const recoveryParts = {
    state: {
        exit: 'abortPendingRequest',
        on: {
            [MEETING.LEAVE]: 'done',
            [MEETING.INVALIDATE]: 'fullRecovery',
        },
        after: {
            POOR_CONNECTION: {
                actions: 'onPoorConnection',
            },
        },
    },
    errHandlers: [
        { target: 'reportInvalidate', cond: 'isOffline' },
        { target: 'reportInvalidate', cond: 'is5xx' },
        { target: 'reportInvalidate', cond: 'is410' },
        { target: 'reportRecoveryError' },
    ],
    requestMeeting: (rctApi, context) =>
        rctApi.getMeeting(
            {
                bridgeId: context.bridge.id,
                meetingId: context.meetingId,
            },
            context.signal
        ),
};

const propsToReport = [
    'meeting',
    'participants',
    'streams',
    'localParticipant',
    'localSession',
    'localStreams',
    'myDemands',
    'targetedToMeDemands',
    'recordings',
    'notifications',
    'myDials',
    'channels',
    'meetingParticipants',
    'streamsRefs',
    'localSessionHistory',
    'participantBySessionMap',
];

/**
 * @typedef {import('../apis/ndb-api').Bridge} Bridge
 * @typedef {import('../apis/rct-api').Call} Meeting
 * @typedef {import('../apis/ndb-api')} NdbApi
 * @typedef {import('../apis/rct-api')} RctApi
 * @typedef {import('./bridge').BridgeFabricParams} BridgeFabricParams
 * @typedef {import('./meeting').MeetingFabricParams} MeetingFabricParams
 * @typedef {import('../event-bus').EventBus} EventBus
 */

/**
 * Initialize bridge SM
 * @param {object} parameters
 * @param {NdbApi} parameters.ndbApi - initialized ndbApi wrapper
 * @param {RctApi} parameters.rctApi - initialized rctApi wrapper
 * @param {Bridge} parameters.bridge - bridge params to create
 * @param {Meeting} parameters.meeting - meeting params to create
 * @param {function(BridgeFabricParams):Promise.<Bridge>} parameters.bridgeServiceFabric - fabric to create bridge service
 * @param {function(MeetingFabricParams):Promise.<Meeting>} parameters.meetingServiceFabric - fabric to create meeting service
 * @param {EventBus} parameters.eventBus - event bus for all running state machines
 * @param {number} parameters.poorConnectionTime - time to report poor connection after
 * @param {number} parameters.recoveryDelay - base time to wait on 5xx error
 * @param {Function} parameters.reportConnected - callback to report success connection
 * @param {Function} parameters.reportRecovered - callback to report recovery
 * @param {Function} parameters.reportRejected - callback to report connection failure
 * @param {Function} parameters.reportInvalidate - callback to report failure on recovery
 * @param {Function} parameters.reportLocalMuteChanged - callback to report local mute audio state changed to Event Bus
 * @param {Function} parameters.reportLocalMuteVideoChanged - callback to report local mute video state changed to Event Bus
 * @param {Function} parameters.reportJoinAudioChanged - callback to report join audio state changed to Event Bus
 * @param {Function} parameters.reportRecoveryError - callback to report failure on recovery to Event Bus
 * @param {Function} parameters.reportChanged - callback with recalculated data
 * @param {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @param {Function} parameters.onPasswordRequire - callback to report password required
 * @param {Function} parameters.onJoinBeforeHostError - callback to report JBH error
 * @param {Function} parameters.onMutedByModeratorAudio - callback to report muted audio by moderator
 * @param {Function} parameters.onMutedByModeratorVideo - callback to report muted video by moderator
 * @param {Function} parameters.onOnlyCoworkersJoin - callback to report only coworkers restriction
 * @param {Function} parameters.onOnlyAuthUserJoin - callback to report only auth user restriction
 *
 * @return {object} bridge SM
 */
const rootMachineFabric = ({
    ndbApi,
    rctApi,
    bridge,
    meeting,
    invokeNotifyTransports,
    bridgeServiceFabric = bridgePromisifiedFabric,
    meetingServiceFabric = meetingPromisifiedFabric,
    eventBus = null,
    poorConnectionTime = 7000,
    recoveryDelay = 3000,
    reportConnected = noop,
    reportRecovered = noop,
    reportRejected = noop,
    reportInvalidate = noop,
    reportInvalidateRoom = noop,
    reportLocalMuteVideoChanged = noop,
    reportJoinAudioChanged = noop,
    reportRecoveryError = noop,
    reportChanged = noop,
    onPoorConnection = noop,
    onPasswordChange = noop,
    onJoinBeforeHostError = noop,
    onLockedError = noop,
    onOnlyCoworkersJoin = noop,
    onOnlyAuthUserJoin = noop,
    onWaitingRoomAwait = noop,
    onWaitingRoomReject = noop,
    onBridgeConnected = noop,
    reportSyncMuteState = noop,
    onCapacityLimit = noop,
    onSimultaneousLimit = noop,
    onWaitingRoomApproved = noop,
    reportNotifyChannelsUpdate,
    asyncReportPatchApplied,
    muteStateController = simpleMuteStateController,
    logger,
    getPrefixedLogger,
    onMeetingServerError = noop,
}) => {
    const setRecoveryInProgress = type => assign({ recoveryInProgress: type });
    const store = createStore(createReducer());

    return Machine(
        {
            id: 'root',

            initial: 'idle',
            context: {
                bridge: null,
                transportInfo: null,
                lastNotifyChannels: null,
                recoveryInProgress: null,
            },

            states: {
                done: {
                    type: 'final',
                },
                idle: {
                    entry: 'saveInitRemotes',
                    on: {
                        [MEETING.JOIN]: 'getBridge',
                    },
                },
                getBridge: {
                    on: {
                        [MEETING.LEAVE]: 'done',
                    },
                    invoke: {
                        src: () =>
                            bridgeServiceFabric({
                                bridge,
                                ndbApi,
                                poorConnectionTime,
                                recoveryDelay,
                                onPoorConnection,
                                onBridgeConnected,
                                eventBus,
                                getPrefixedLogger,
                            }),
                        onDone: {
                            actions: 'saveBridge',
                            target: 'getMeeting',
                        },
                        onError: 'error',
                    },
                },
                getMeeting: {
                    on: {
                        [MEETING.LEAVE]: 'done',
                    },
                    invoke: {
                        src: ({ bridge: { id: bridgeId }, meetingId }) =>
                            meetingServiceFabric({
                                bridgeId,
                                meeting,
                                rctApi,
                                poorConnectionTime,
                                recoveryDelay,
                                onPoorConnection,
                                eventBus,
                                onPasswordChange,
                                onJoinBeforeHostError,
                                onLockedError,
                                onOnlyCoworkersJoin,
                                onOnlyAuthUserJoin,
                                onCapacityLimit,
                                onSimultaneousLimit,
                                onWaitingRoomAwait,
                                onWaitingRoomReject,
                                onWaitingRoomApproved,
                                parentId: meetingId,
                                getPrefixedLogger,
                                onMeetingServerError,
                            }),
                        onDone: 'reportConnected',
                        onError: 'error',
                    },
                },
                reportConnected: {
                    entry: ['reportConnected', 'invokeTransports'],
                    always: 'working',
                },
                working: {
                    entry: 'maybeReportRecovered',
                    on: {
                        [MEETING.LEAVE]: 'done',
                        [MEETING.INVALIDATE]: 'fullRecovery',
                        [MEETING.INVALIDATE_ROOM]: 'softRecovery',
                        [MEETING.TIMEOUT]: 'versionRecovery',
                        [MEETING.TRANSPORT_DATA]: 'transportData',
                    },
                },
                transportData: {
                    entry: 'recalculatePatch',
                    always: 'working',
                },
                reportInvalidate: {
                    entry: 'reportInvalidate',
                    always: 'fullRecovery',
                },
                reportInvalidateRoom: {
                    entry: 'reportInvalidateRoom',
                    always: 'softRecovery',
                },
                fullRecovery: {
                    entry: [setRecoveryInProgress(RECOVERY.FULL), 'hardResetReducer'],
                    always: {
                        target: 'getMeeting',
                    },
                },
                softRecovery: {
                    ...recoveryParts.state,
                    entry: [setRecoveryInProgress(RECOVERY.SOFT), 'softResetReducer'],
                    invoke: {
                        id: 'softRecovery',
                        src: abortControllerWrapper(context =>
                            recoveryParts.requestMeeting(rctApi, context)
                        ),
                        onDone: [
                            { target: 'reportInvalidate', cond: 'isInvalidRecoveryResponse' },
                            { target: 'reportConnected' }
                        ],
                        onError: [
                            { target: 'softRecoveryAfterDelay', cond: 'is429' },
                            ...recoveryParts.errHandlers,
                        ],
                    },
                },
                versionRecovery: {
                    ...recoveryParts.state,
                    entry: [setRecoveryInProgress(RECOVERY.VERSION), 'softResetReducer'],
                    on: {
                        ...recoveryParts.state.on,
                        [MEETING.INVALIDATE_ROOM]: 'softRecovery',
                    },
                    invoke: {
                        id: 'versionRecovery',
                        src: abortControllerWrapper(context =>
                            recoveryParts.requestMeeting(rctApi, context)
                        ),
                        onDone: [
                            { target: 'reportInvalidate', cond: 'isInvalidRecoveryResponse' },
                            { target: 'transportData', cond: 'isTheSameRoom' },
                            // quite rare case where the invalidate_room message from PubNub is lost
                            { target: 'reportInvalidateRoom' },
                        ],
                        onError: [
                            { target: 'versionRecoveryAfterDelay', cond: 'is429' },
                            ...recoveryParts.errHandlers,
                        ],
                    },
                },
                softRecoveryAfterDelay: goToAfterDelay('softRecovery', 'TIMEOUT_429'),
                versionRecoveryAfterDelay: goToAfterDelay('versionRecovery', 'TIMEOUT_429'),
                error: {
                    entry: 'reportError',
                    always: {
                        target: 'done',
                    },
                },
                reportRecoveryError: {
                    entry: 'reportRecoveryError',
                    always: {
                        target: 'done',
                    },
                },
            },
        },
        {
            actions: {
                saveInitRemotes: assign({
                    remoteAudioState: () => meeting.participants[0].sessions[0].localMute,
                    remoteVideoState: () => meeting.participants[0].sessions[0].localMuteVideo,
                    joinAudioState: () => {
                        const streams = meeting.participants[0].streams;
                        return (streams || []).length > 0
                            ? (streams[0].audio || {}).isActiveIn
                            : null;
                    },
                }),
                reportRecoveryError: (context, event) => {
                    reportRecoveryError(event.data);
                },
                abortPendingRequest,
                saveBridge: assign({ bridge: (context, event) => event.data }),
                reportError: (context, event) => reportRejected(event.data),
                reportConnected: assign((context, event) => {
                    store.dispatch({
                        type: types.CHANGE,
                        data: event.data,
                    });

                    const { recalculatedEntities } = store.getState();

                    reportChanged(recalculatedEntities, true);

                    const {
                        meeting = {},
                        participants = {},
                        streams = {},
                        localParticipant = {},
                        localSession = {},
                        localStreams = [],
                        myDemands = null,
                        targetedToMeDemands = null,
                        recordings = null,
                        notifications = null,
                        myDials = null,
                        channels = null,
                        streamsRefs = {},
                        meetingParticipants = {},
                        notifyChannels = {},
                        participantBySessionMap = {},
                    } = recalculatedEntities;

                    const transportInfo = {
                        notifyChannels,
                        subscribeKey: localSession.notificationSubKey,
                        authKey: localSession.notificationToken,
                    };

                    reportConnected({
                        meeting,
                        participants,
                        streams,
                        localParticipant,
                        localSession,
                        localStreams,
                        myDemands,
                        targetedToMeDemands,
                        recordings,
                        notifications,
                        myDials,
                        channels,
                        streamsRefs,
                        meetingParticipants,
                        transportInfo,
                        participantBySessionMap,
                    });

                    return {
                        transportInfo,
                        serverMute: localParticipant.serverMute,
                        serverMuteVideo: localParticipant.serverMuteVideo,
                        meetingId: meeting.id,
                        roomId: event.roomId,
                    };
                }),
                invokeTransports: assign(context => {
                    invokeNotifyTransports(context.transportInfo);
                    return { transportInfo: null };
                }),
                maybeReportRecovered: assign(context => {
                    if (context.recoveryInProgress) {
                        reportRecovered(context.recoveryInProgress);
                    }
                    return { recoveryInProgress: null };
                }),
                recalculatePatch: assign((context, event) => {
                    if (event.channelId === PN_CHANNELS.PRIVATE) {
                        logger.error('Unexpected DATA event from private channel', event);
                        return;
                    }

                    store.dispatch({
                        type: types.CHANGE_BATCH,
                        data: arrify(event.data),
                    });

                    const { recalculatedEntities } = store.getState();
                    const {
                        localSession,
                        localParticipant,
                        notifyChannels = {},
                    } = recalculatedEntities;
                    const isRecoveryEvent = event.origin !== STATE_MACHINES.TRANSPORT;

                    if (
                        localSession &&
                        localSession.deleted &&
                        localSession.deleteReason === SESSION_DELETED_CODES.TIMEOUT
                    ) {
                        reportInvalidate('Local session timeout');
                    } else {
                        reportChanged(recalculatedEntities, isRecoveryEvent);
                    }

                    // We should emit PATCH_APPLIED and NOTIFY_CHANNELS_UPDATE events here, exactly after
                    // calculating a new state, to make waiting of them in the transport SMs as transparent
                    // and predictable as possible.
                    if (isRecoveryEvent) {
                        reportNotifyChannelsUpdate(notifyChannels);
                    } else if (notifyChannels !== context.lastNotifyChannels) {
                        asyncReportPatchApplied(notifyChannels || {});
                    }

                    const muteState = muteStateController(
                        context,
                        localParticipant,
                        localSession,
                        reportJoinAudioChanged,
                        reportSyncMuteState,
                        reportLocalMuteVideoChanged
                    );

                    return { ...muteState, lastNotifyChannels: notifyChannels };
                }),
                hardResetReducer: () => {
                    store.dispatch({ type: types.HARD_RESET });
                },
                softResetReducer: () => {
                    store.dispatch({ type: types.SOFT_RESET });
                },
                resetReducerRemoteEntities: () => {
                    store.dispatch({ type: types.RESET_REMOTE_ENTITIES });
                },
                reportInvalidate: () => reportInvalidate(REASON.MEETING_RECOVERY_FAIL),
                reportInvalidateRoom: (ctx, event) => reportInvalidateRoom(event.data),
                onPoorConnection,
            },
            delays: {
                TIMEOUT_429: (context, { data }) => timeout429(data),
                POOR_CONNECTION: () => poorConnectionTime,
            },
            guards: {
                is429: isErrorStatus(429),
                isOffline: () => !navigator.onLine,
                is5xx: errorStartsWith('5'),
                is410: isErrorStatus(410),
                isTheSameRoom: (context, event) => context.roomId === event.roomId,

                // Potential fix of RCV-45963, checking incorrect data from response
                // according our conviction that correct patch should has id field in body
                isInvalidRecoveryResponse: (_, { data }) => !data?.id,
            },
        }
    );
};

/**
 * Initialize bridge SM service
 * @param {object} parameters
 * @param {boolean} parameters.autoStart - auto start interpret (default true)
 *
 * @param {NdbApi} parameters.ndbApi - initialized ndbApi wrapper
 * @param {RctApi} parameters.rctApi - initialized rctApi wrapper
 * @param {Bridge} parameters.bridge - bridge params to create
 * @param {Meeting} parameters.meeting - meeting params to create
 * @param {function(BridgeFabricParams):Promise.<Bridge>} parameters.bridgeServiceFabric - fabric to create bridge service
 * @param {function(MeetingFabricParams):Promise.<Meeting>} parameters.meetingServiceFabric - fabric to create meeting service
 * @param {EventBus} parameters.eventBus - event bus for all running state machines
 * @param {number} parameters.poorConnectionTime - time to report poor connection after
 * @param {number} parameters.recoveryDelay - base time to wait on 5xx error
 * @param {Function} parameters.reportConnected - callback to report success connection
 * @param {Function} parameters.reportRecovered - callback to report recovery
 * @param {Function} parameters.reportRejected - callback to report connection failure
 * @param {Function} parameters.reportInvalidate - callback to report failure on recovery
 * @param {Function} parameters.reportLocalMuteChanged - callback to report local mute audio state changed to Event Bus
 * @param {Function} parameters.reportLocalMuteVideoChanged - callback to report local mute video state changed to Event Bus
 * @param {Function} parameters.reportJoinAudioChanged - callback to report join audio state changed to Event Bus
 * @param {Function} parameters.reportRecoveryError - callback to report failure on recovery to Event Bus
 * @param {Function} parameters.reportChanged - callback with recalculated data
 * @param {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @param {Function} parameters.onPasswordRequire - callback to report password required
 * @param {Function} parameters.onJoinBeforeHostError - callback to report JBH error
 * @param {Function} parameters.onMutedByModeratorAudio - callback to report muted audio by moderator
 * @param {Function} parameters.onMutedByModeratorVideo - callback to report muted video by moderator
 *
 * @return {object} bridge SM
 */
const rootInterpretFabric = ({ autoStart = true, ...rest }) => {
    const inter = interpret(rootMachineFabric(rest)).onTransition(logTransition(rest.logger));

    if (autoStart) {
        inter.start();
    }

    return inter;
};

/**
 * Promisified bridge SM service
 * @param {object} parameters
 * @param {Function} parameters.onSuccess - callback to report success connection to conference
 * @param {Function} parameters.onError - callback to report failure connection to conference
 * @param {Function} parameters.onLeave - callback to report meeting leave to conference
 *
 * @param {NdbApi} parameters.ndbApi - initialized ndbApi wrapper
 * @param {RctApi} parameters.rctApi - initialized rctApi wrapper
 * @param {Bridge} parameters.bridge - bridge params to create
 * @param {Meeting} parameters.meeting - meeting params to create
 * @param {function(BridgeFabricParams):Promise.<Bridge>} parameters.bridgeServiceFabric - fabric to create bridge service
 * @param {function(MeetingFabricParams):Promise.<Meeting>} parameters.meetingServiceFabric - fabric to create meeting service
 * @param {EventBus} parameters.eventBus - event bus for all running state machines
 * @param {number} parameters.poorConnectionTime - time to report poor connection after
 * @param {number} parameters.recoveryDelay - base time to wait on 5xx error
 * @param {Function} parameters.reportConnected - callback to report success connection
 * @param {Function} parameters.reportRecovered - callback to report recovery
 * @param {Function} parameters.reportRejected - callback to report connection failure
 * @param {Function} parameters.reportInvalidate - callback to report failure on recovery
 * @param {Function} parameters.reportLocalMuteChanged - callback to report local mute audio state changed
 * @param {Function} parameters.reportLocalMuteVideoChanged - callback to report local mute video state changed
 * @param {Function} parameters.reportJoinAudioChanged - callback to report join audio state changed
 * @param {Function} parameters.reportRecoveryError - callback to report failure on recovery
 * @param {Function} parameters.onChange - callback with recalculated data
 * @param {Function} parameters.onPoorConnection - callback to call when connection is poor
 * @param {Function} parameters.onPasswordRequire - callback to report password required
 * @param {Function} parameters.onJoinBeforeHostError - callback to report JBH error
 * @param {Function} parameters.onMutedByModeratorAudio - callback to report muted audio by moderator
 * @param {Function} parameters.onMutedByModeratorVideo - callback to report muted video by moderator
 * @param {string} [parameters.pubnubOrigin] - custom pubnub origin
 *
 * @return {Promise} that resolves when bridge information received, rejects unrecoverable error
 */
const rootEventsFabric = ({
    eventBus,
    transport,
    onSuccess = noop,
    onError = noop,
    onLeave = noop,
    onChange,
    transportFabric = transportEventsFabric,
    ...rest
}) => {
    const { getPrefixedLogger } = rest;
    const logger = getPrefixedLogger(STATE_MACHINES.ROOT);
    const reportLocalMuteVideoChanged = ({ muteState, isLocal = false }) => {
        eventBus.send(SYNC_TYPES.VIDEO, isLocal ? { local: muteState } : { remote: muteState });
    };
    const reportJoinAudioChanged = ({ joinState, isLocal = false }) => {
        eventBus.send(
            SYNC_TYPES.AUDIO_JOIN,
            isLocal ? { local: joinState } : { remote: joinState }
        );
    };
    const reportSyncMuteState = ({
        local = null,
        remote = null,
        sessionId = null,
        isPstn = null,
    }) => {
        eventBus.send(SYNC_TYPES.AUDIO, {
            ...(local !== null ? { local } : {}),
            ...(remote !== null ? { remote } : {}),
            ...(sessionId !== null ? { sessionId } : {}),
            ...(isPstn !== null ? { isPstn } : {}),
        });
    };
    const reportConnected = data => {
        eventBus.send(MEETING.CONNECTED, {
            // pong & sync & layouts
            bridgeId: data.meeting.bridgeId,
            meetingId: data.meeting.id,
            participantId: data.localParticipant.id,
            sessionId: data.localSession.id,
            pongDelay: data.localSession.pingInfo.pingInterval,
            streamId: data.localStreams ? data.localStreams[0].id : null,
            // for layout SM
            transportInfo: data.transportInfo,
            isHost: data.localParticipant.host,
            roomId: data.meeting.roomId,
            appliedLayoutId: data.meeting.appliedLayoutId,
        });
        onSuccess(data);
    };
    const reportRecovered = recoveryType => {
        eventBus.send(MEETING.RECOVERED, { recoveryType });
    };
    const reportRejected = (...data) => {
        eventBus.send(MEETING.FAILED, ...data);
        onError(...data);
    };
    const reportInvalidate = reason => {
        eventBus.send(MEETING.INVALIDATE, {
            reason,
        });
    };
    const reportRecoveryError = reason => {
        eventBus.send(MEETING.LEAVE, { reason });
        onLeave(reason);
    };
    const reportInvalidateRoom = ({ roomId, roomName }) => {
        logger.warn(
            REASON.ROOM_ID_CHANGED,
            '\nSending INVALIDATE_ROOM manually...',
            JSON.stringify({
                roomId,
                roomName,
            })
        );
        eventBus.send(MEETING.INVALIDATE_ROOM, {
            reason: REASON.ROOM_ID_CHANGED,
            roomId,
            roomName,
        });
    };
    const reportNotifyChannelsUpdate = notifyChannels => {
        eventBus.send(MEETING.NOTIFY_CHANNELS_UPDATE, { notifyChannels });
    };
    const asyncReportPatchApplied = notifyChannels => {
        // We need to deliver a patch confirmation to the origin transport only after
        // its transition completes and it's ready to listen for this event.
        Promise.resolve().then(() => {
            eventBus.send(MEETING.PATCH_APPLIED, { notifyChannels });
        });
    };
    const invokeNotifyTransports = ({ notifyChannels, authKey, subscribeKey }) => {
        if (!notifyChannels || !authKey || !subscribeKey) return;

        return Object.keys(notifyChannels).map(id =>
            transportFabric({
                id,
                transport,
                eventBus,
                authKey,
                subscribeKey,
                channelInfo: notifyChannels[id],
                getPrefixedLogger,
            })
        );
    };

    const reportChanged = createCachedReporter(propsToReport, onChange);

    const inter = rootInterpretFabric({
        ...rest,
        reportRejected,
        reportConnected,
        reportRecovered,
        reportChanged,
        reportRecoveryError,
        reportLocalMuteVideoChanged,
        reportJoinAudioChanged,
        reportInvalidate,
        reportInvalidateRoom,
        reportNotifyChannelsUpdate,
        asyncReportPatchApplied,
        eventBus,
        reportSyncMuteState,
        invokeNotifyTransports,
        logger,
    });

    const unlisten = eventBus.listen(
        [
            MEETING.LEAVE,
            MEETING.INVALIDATE,
            MEETING.INVALIDATE_ROOM,
            MEETING.TIMEOUT,
            MEETING.TRANSPORT_DATA,
        ],
        inter.send.bind(inter)
    );

    inter.onDone(unlisten);

    return withAsyncAutoStop(inter, eventBus, logger);
};
module.exports = {
    default: rootInterpretFabric,
    rootEventsFabric,
    BRIDGE_ERRORS: ERRORS,
};
