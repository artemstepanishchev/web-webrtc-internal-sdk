const { Machine, interpret, assign } = require('xstate');
const {
    abortPendingRequest,
    abortControllerWrapper,
    isErrorStatus,
    errorStartsWith,
    timeout429,
    withAsyncAutoStop,
    logTransition,
} = require('./utils');
const { MEETING, STATE_MACHINES } = require('./constants');
const { noop } = require('../utils');

const pongContextFabric = () => ({
    bridgeId: null,
    meetingId: null,
    participantId: null,
    sessionId: null,
    retryCount: 0,
    controller: null,
});

const leaveEdges = {
    [MEETING.LEAVE]: 'stop',
    [MEETING.INVALIDATE]: 'idle',
    [MEETING.INVALIDATE_ROOM]: 'idle',
};

/**
 * Initialize pong SM
 * @param {object} parameters
 * @param {import('../apis/rct-api')} parameters.rctApi - initialized rctApi wrapper
 *
 * @return {object} pong SM
 */
const pongMachineFabric = ({
    rctApi,
    reconnectDelay = 5000,
    recoveryDelay = 1000,
    reportInvalidate = noop,
}) =>
    Machine(
        {
            id: 'pong',
            initial: 'idle',
            context: pongContextFabric(),
            states: {
                idle: {
                    on: {
                        ...leaveEdges,
                        [MEETING.CONNECTED]: {
                            target: 'waiting',
                            actions: 'saveContext',
                        },
                    },
                },
                stop: {
                    type: 'final',
                },
                waiting: {
                    after: {
                        PONG_DELAY: 'sendingPong',
                    },
                    on: {
                        ...leaveEdges,
                    },
                },
                waitingForOnline: {
                    after: {
                        RECONNECT_DELAY: 'checkOnline',
                    },
                    on: {
                        ...leaveEdges,
                    },
                },
                checkOnline: {
                    always: [
                        { target: 'sendingPong', cond: 'isOnline' },
                        { target: 'waitingForOnline' },
                    ],
                },
                reportInvalidate: {
                    onEntry: 'reportInvalidate',
                    always: {
                        target: 'waiting',
                    },
                },
                sendingPong: {
                    exit: ['abortPendingRequest'],
                    invoke: {
                        id: 'sendPong',
                        src: abortControllerWrapper(context =>
                            rctApi.pong(context, context.signal)
                        ),
                        onDone: {
                            target: 'waiting',
                            actions: 'pongSuccess',
                        },
                        onError: [
                            { target: 'waitingForOnline', cond: 'isOffline' },
                            { target: 'handleExponentialRetryError', cond: 'is5xx' },
                            { target: 'handle429', cond: 'is429' },
                            { target: 'reportInvalidate', cond: 'is410' },
                            { target: 'handlePongError' },
                        ],
                    },
                    on: {
                        ...leaveEdges,
                    },
                },
                handle429: {
                    on: {
                        ...leaveEdges,
                    },
                    after: {
                        TIMEOUT_429: 'sendingPong',
                    },
                },
                handleExponentialRetryError: {
                    on: {
                        ...leaveEdges,
                    },
                    after: {
                        TIMEOUT_5XX: 'sendingPong',
                    },
                    onExit: 'incrementRetryCount',
                },
                handlePongError: {
                    exit: 'incrementRetryCount',
                    after: {
                        RETRY_DELAY: 'sendingPong',
                    },
                    on: {
                        ...leaveEdges,
                    },
                },
            },
        },
        {
            actions: {
                abortPendingRequest,
                saveContext: assign({
                    bridgeId: (context, event) => event.bridgeId,
                    meetingId: (context, event) => event.meetingId,
                    participantId: (context, event) => event.participantId,
                    sessionId: (context, event) => event.sessionId,
                    pongDelay: (context, event) => event.pongDelay,
                }),
                pongSuccess: assign({
                    retryCount: 0,
                }),
                incrementRetryCount: assign({ retryCount: context => context.retryCount + 1 }),
                reportInvalidate: () => reportInvalidate('Failed to recover pongs'),
            },
            delays: {
                PONG_DELAY: context => context.pongDelay,
                RETRY_DELAY: context =>
                    Math.min(Math.pow(2, context.retryCount) * 1000, context.pongDelay),
                TIMEOUT_5XX: context =>
                    Math.min(Math.pow(2, context.retryCount) * recoveryDelay, 5000),
                TIMEOUT_429: (context, { data }) => timeout429(data),
                RECONNECT_DELAY: () => reconnectDelay,
            },
            guards: {
                isOnline: () => navigator.onLine,
                isOffline: () => !navigator.onLine,
                is410: isErrorStatus(410),
                is429: isErrorStatus(429),
                is5xx: errorStartsWith('5'),
            },
        }
    );

/**
 * Initialize pong SM service
 * @param {object} parameters
 * @param {import('../apis/rct-api')} parameters.rctApi - initialized rctApi wrapper
 *
 * @return {object} pong SM
 */
const pongInterpretFabric = ({ rctApi, recoveryDelay, logger }) =>
    interpret(pongMachineFabric({ rctApi, recoveryDelay }))
        .onTransition(logTransition(logger))
        .start();

/**
 * Initialize pong SM events
 * @param {object} parameters
 * @param {import('../apis/rct-api')} parameters.rctApi - initialized rctApi wrapper
 * @param {import('../event-bus').EventBus} parameters.eventBus - event bus for all running state machines
 *
 * @return {object} pong SM
 */
const pongEventsFabric = ({ eventBus, getPrefixedLogger, ...rest }) => {
    const logger = getPrefixedLogger(STATE_MACHINES.PONG);

    const reportInvalidate = reason => {
        eventBus.send(MEETING.INVALIDATE, {
            reason,
        });
    };

    const inter = pongInterpretFabric({
        ...rest,
        reportInvalidate,
        logger,
    });

    const unlisten = eventBus.listen(
        [MEETING.LEAVE, MEETING.CONNECTED, MEETING.INVALIDATE, MEETING.INVALIDATE_ROOM],
        inter.send.bind(inter)
    );

    inter.onDone(unlisten);

    return withAsyncAutoStop(inter, eventBus, logger);
};

module.exports = {
    default: pongInterpretFabric,
    pongEventsFabric,
};
