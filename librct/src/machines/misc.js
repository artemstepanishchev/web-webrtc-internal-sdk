const muteStateController = (
    context,
    localParticipant,
    localSession,
    reportJoinAudioChanged,
    reportSyncMuteState,
    reportLocalMuteVideoChanged,
) => {
    const {
        localMute,
        pstnSessionId,
        serverMute,
        serverMuteVideo,
        localMuteVideo,
        joinedAudio,
    } = localParticipant;

    const {
        id: sessionId,
        localMute: sessionLocalMute,
        serverMute: sessionServerMute,
    } = localSession;

    const {
        pstnSessionId: prevPstnSessionId,
        serverMuteVideo: prevServerMuteVideo,
        remoteVideoState: prevLocalMuteVideo,
        joinAudioState: prevJoinedAudio,
    } = context;

    let {
        serverMute: prevServerMute,
        remoteAudioState: prevLocalMute,
        localAudioJoined: prevlocalAudioJoined,
    } = context;

    if (pstnSessionId) {
        // if pstn just merged we should leave audio
        if (!prevPstnSessionId) {
            reportJoinAudioChanged({
                joinState: false,
                isLocal: true,
            });
            prevlocalAudioJoined = joinedAudio;
        }
        // pstn just merged or has been muted/unmuted from moderator
        if (!prevPstnSessionId || prevServerMute !== localMute) {
            reportSyncMuteState({
                local: localMute,
                remote: localMute,
                sessionId: pstnSessionId,
                isPstn: true,
            });
            prevLocalMute = localMute;
            prevServerMute = localMute;
        }
    } else {
        // if pstn just left we restore join audio state back and report old params from local session to audio sync machine
        if (prevPstnSessionId) {
            if (prevlocalAudioJoined) {
                reportJoinAudioChanged({
                    joinState: true,
                    isLocal: true,
                });
            }

            reportSyncMuteState({
                sessionId,
                local: sessionLocalMute,
                remote: sessionLocalMute,
                isPstn: false,
            });
            prevLocalMute = sessionLocalMute;
            prevServerMute = sessionServerMute;
        }

        // case for non-pstn users for changing mute state from moderator
        if (prevServerMute !== serverMute) {

            if (serverMute) {
                reportSyncMuteState({ local: serverMute });
            }
            prevServerMute = serverMute;
        }
    }

    if (prevServerMuteVideo !== serverMuteVideo) {
        if (serverMuteVideo) {
            reportLocalMuteVideoChanged({
                muteState: serverMuteVideo,
                isLocal: true,
            });
        }
    }

    if (prevLocalMute !== localMute) {
        reportSyncMuteState({
            remote: localMute,
        });
        prevLocalMute = localMute;
    }
    if (prevLocalMuteVideo !== localMuteVideo) {
        reportLocalMuteVideoChanged({
            muteState: localMuteVideo,
        });
    }
    if (prevJoinedAudio !== joinedAudio) {
        reportJoinAudioChanged({
            joinState: joinedAudio,
        });
    }

    return {
        serverMute: prevServerMute,
        remoteAudioState: prevLocalMute,
        serverMuteVideo,
        pstnSessionId,
        remoteVideoState: localMuteVideo,
        joinAudioState: joinedAudio,
        localAudioJoined: prevlocalAudioJoined,
    };
};

module.exports = {
    muteStateController,
};
