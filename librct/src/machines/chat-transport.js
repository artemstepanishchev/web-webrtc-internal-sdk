const { Machine, assign, interpret } = require('xstate');
const TransportWrapper = require('../transport/transport-wrapper');
const {
    abortPendingRequest,
    ignorePromiseActions,
    withAsyncAutoStop,
    logTransition,
} = require('./utils');
const { MEETING, CHAT, STATE_MACHINES, TRANSPORT } = require('./constants');
const { noop } = require('../utils');

const leaveEdges = {
    [MEETING.LEAVE]: 'finish',
    [MEETING.INVALIDATE]: 'reset',
    [MEETING.INVALIDATE_ROOM]: 'reset',
    [TRANSPORT.UNKNOWN_ERROR]: 'reactivate',
};

const resetActions = ['maybeDisconnectTransport', 'resetSubscribedChats'];

/**
 * Initialize chat transport SM
 * @param {Object} params
 * @param {TransportWrapper} params.transport - instance of TransportWrapper
 * @param {number} params.gapDelay - ms to wait on gap found
 * @param {number} params.transportReconnectDelay - ms to wait on transport connection error
 * @param {number} params.recoveryDelay - ms to wait without new messages
 * @param {number} params.connectRecoveryDelay - ms to wait on transport connecting
 * @param {Function} params.onChange - callback to report chat changes to conference
 * @param {Function} params.reportRecovery - callback to report recovery from transport
 * @param {Function} params.reportActivated - callback to report transport activation
 *
 * @return {object} chat transport SM
 */
const chatTransportMachineFabric = ({
    transport,
    connectRecoveryDelay = 10000,
    transportReconnectDelay = 3000,
    recoveryDelay = 2.5 * 60 * 1000,
    gapDelay = 1000,
    onChange = noop,
    reportRecovery = noop,
    reportActivated = noop,
    reportReactivate = noop,
} = {}) => {
    const connectTransport = context => {
        const { subscribeKeyMeta, authKeyMeta, channelsMeta } = context;
        transport.init(subscribeKeyMeta, authKeyMeta);

        return transport.connect(channelsMeta);
    };

    return Machine(
        {
            id: 'chatTransport',
            initial: 'idle',
            context: {
                subscribedChats: {},
                messages: {},
                chatQueue: [],
                messagesQueueByChannel: {},
                chatIdsByChannel: {},
            },
            states: {
                idle: {
                    on: {
                        [CHAT.GET_META]: {
                            actions: 'saveMeta',
                            target: 'activatingTransport',
                        },
                        ...leaveEdges,
                    },
                },
                activatingTransport: {
                    exit: 'maybeDisconnectTransport',
                    invoke: {
                        id: 'activatingTransport',
                        src: connectTransport,
                        onDone: {
                            target: 'working',
                            actions: 'reportActivated',
                        },
                        onError: {
                            target: 'handleConnectError',
                        },
                    },
                    on: {
                        ...leaveEdges,
                    },
                    after: {
                        CONNECT_RECOVERY_DELAY: 'recoveryDuringConnecting',
                    },
                },
                handleConnectError: {
                    on: {
                        ...leaveEdges,
                    },
                    after: {
                        TRANSPORT_RECONNECT_DELAY: 'activatingTransport',
                    },
                },
                recoveryDuringConnecting: {
                    entry: 'maybeDisconnectTransport',
                    always: {
                        target: 'activatingTransport',
                    },
                },
                working: {
                    on: {
                        ...leaveEdges,
                        [CHAT.NEW_CHAT_FROM_TRANSPORT]: {
                            target: 'awaitForRecovery',
                            actions: 'reportNewChat',
                        },
                        [CHAT.RECEIVE_CHAT_MESSAGE]: {
                            target: 'hasNextVersionCheck',
                            actions: 'addToMessagesQueue',
                        },
                        [CHAT.RECEIVE_NEW_DATA]: {
                            actions: ['saveData', 'subscribeNewChats', 'reportNewMessages'],
                        },
                    },
                    after: {
                        RECOVERY_DELAY: {
                            target: 'awaitForRecovery',
                            actions: 'reportRecovery',
                        },
                    },
                },
                awaitForRecovery: {
                    on: {
                        ...leaveEdges,
                        [CHAT.RECEIVE_NEW_DATA]: {
                            target: 'hasNextVersionCheck',
                            actions: ['saveData', 'subscribeNewChats', 'reportNewMessages'],
                        },
                        [CHAT.NEW_CHAT_FROM_TRANSPORT]: {
                            actions: 'addToChatQueue',
                        },
                        [CHAT.RECEIVE_CHAT_MESSAGE]: {
                            actions: 'addToMessagesQueue',
                        },
                    },
                    after: {
                        RECOVERY_DELAY: {
                            actions: 'reportRecovery',
                        },
                    },
                },
                hasNextVersionCheck: {
                    always: [
                        {
                            target: 'working',
                            cond: 'isQueuesEmpty',
                        },
                        {
                            target: 'awaitForRecovery',
                            actions: ['clearChatQueue', 'clearMessagesQueue', 'reportRecovery'],
                            cond: 'isNewChat',
                        },
                        {
                            target: 'reportUpdate',
                            cond: 'hasAllMessagesNextVersion',
                        },
                        {
                            target: 'waitForGap',
                        },
                    ],
                },
                reportUpdate: {
                    entry: 'reportMessagesPatch',
                    always: {
                        target: 'hasNextVersionCheck',
                    },
                },
                waitForGap: {
                    on: {
                        ...leaveEdges,
                        [CHAT.RECEIVE_NEW_DATA]: {
                            target: 'working',
                            actions: ['saveData', 'subscribeNewChats', 'reportNewMessages'],
                        },
                        [CHAT.NEW_CHAT_FROM_TRANSPORT]: {
                            actions: 'addToChatQueue',
                            target: 'hasNextVersionCheck',
                        },
                        [CHAT.RECEIVE_CHAT_MESSAGE]: {
                            actions: 'addToMessagesQueue',
                            target: 'hasNextVersionCheck',
                        },
                    },
                    after: {
                        GAP_DELAY: {
                            target: 'awaitForRecovery',
                            actions: 'reportRecovery',
                        },
                    },
                },
                reset: {
                    entry: resetActions,
                    always: 'idle',
                },
                reactivate: {
                    entry: resetActions,
                    always: 'idle',
                    exit: 'reportReactivate',
                },
                finish: {
                    entry: resetActions,
                    always: 'stop',
                },
                stop: {
                    type: 'final',
                },
            },
        },
        {
            actions: {
                saveData: assign({
                    chatsById: (context, event) => event.data.chatsById,
                    messages: ({ messages }, { data }) =>
                        Object.keys(data.successMessages).length > 0
                            ? data.successMessages
                            : messages,
                }),
                reportRecovery,
                reportNewChat: (context, event) => {
                    reportRecovery({
                        newChatId: event.data.data,
                    });
                },
                reportActivated,
                reportReactivate,
                subscribeNewChats: context => {
                    Object.keys(context.chatsById).forEach(chatId => {
                        if (!context.subscribedChats[chatId]) {
                            transport.subscribe(context.chatsById[chatId].channel);

                            context.subscribedChats[chatId] = context.chatsById[chatId];
                            context.chatIdsByChannel[context.chatsById[chatId].channel] = chatId;
                            onChange('chat:creation:success', context.chatsById[chatId]);
                        }
                    });
                },
                reportMessagesPatch: assign(context => {
                    Object.keys(context.messagesQueueByChannel).forEach(channel => {
                        const result = [];

                        const chatId = context.chatIdsByChannel[channel];
                        const lastMessageSeq =
                            context.messages[chatId] && context.messages[chatId].length > 0
                                ? context.messages[chatId][context.messages[chatId].length - 1].seq
                                : -1;
                        let nextVersion = lastMessageSeq + 1;
                        while (
                            context.messagesQueueByChannel[channel].hasOwnProperty(nextVersion)
                        ) {
                            if (context.messagesQueueByChannel[channel][nextVersion]) {
                                result.push(context.messagesQueueByChannel[channel][nextVersion]);
                            }
                            delete context.messagesQueueByChannel[channel][nextVersion];
                            nextVersion++;
                        }
                        delete context.messagesQueueByChannel[channel];

                        if (result.length > 0) {
                            if (Array.isArray(context.messages[chatId])) {
                                context.messages[chatId].push(...result);
                            } else {
                                context.messages[chatId] = [...result];
                            }
                            onChange('chat:messages', {
                                id: chatId,
                                messages: result,
                            });
                        }
                    });
                    return {
                        messagesQueueByChannel: context.messagesQueueByChannel,
                        messages: context.messages,
                    };
                }),
                reportNewMessages: context => {
                    Object.keys(context.messages).forEach(chatId => {
                        onChange('chat:messages', {
                            id: chatId,
                            messages: context.messages[chatId],
                        });
                    });
                },
                addToChatQueue: assign({
                    chatQueue: (context, event) => [...context.chatQueue, { id: event.data.data }],
                }),
                clearChatQueue: context => {
                    context.chatQueue = [];
                },
                addToMessagesQueue: assign({
                    messagesQueueByChannel: (context, event) => {
                        const { data } = event;

                        const chatId = context.chatIdsByChannel[data.channel];
                        const prevMessages = context.messages[chatId] ?? [];
                        const lastSeq = prevMessages[prevMessages.length - 1]?.seq;
                        if (lastSeq >= data.data.seq) {
                            return context.messagesQueueByChannel;
                        }
                        if (context.messagesQueueByChannel[data.channel]) {
                            if (!context.messagesQueueByChannel[data.channel][data.data.seq]) {
                                context.messagesQueueByChannel[data.channel][data.data.seq] =
                                    data.data;
                            }
                        } else {
                            context.messagesQueueByChannel[data.channel] = {
                                [data.data.seq]: data.data,
                            };
                        }

                        return context.messagesQueueByChannel;
                    },
                }),
                clearMessagesQueue: assign({
                    messagesQueueByChannel: {},
                }),
                abortPendingRequest,
                saveMeta: assign({
                    channelsMeta: (context, event) => event.data.channels,
                    subscribeKeyMeta: (context, event) => event.data.subscribeKey,
                    authKeyMeta: (context, event) => event.data.authKey,
                }),
                maybeDisconnectTransport: ignorePromiseActions(() =>
                    transport.disconnectIfNeeded()
                ),
                resetSubscribedChats: context => {
                    context.subscribedChats = {};
                },
            },
            delays: {
                TRANSPORT_RECONNECT_DELAY: transportReconnectDelay,
                CONNECT_RECOVERY_DELAY: connectRecoveryDelay,
                RECOVERY_DELAY: recoveryDelay,
                GAP_DELAY: gapDelay,
            },
            guards: {
                isQueuesEmpty: context =>
                    context.chatQueue.length === 0 &&
                    Object.keys(context.messagesQueueByChannel).length === 0,
                isNewChat: context => context.chatQueue.length > 0,
                hasAllMessagesNextVersion: context => {
                    const hasNoNextVersion = [];
                    Object.keys(context.messagesQueueByChannel).forEach(channel => {
                        const chatId = context.chatIdsByChannel[channel];
                        const lastMessageSeq =
                            context.messages[chatId] && context.messages[chatId].length > 0
                                ? context.messages[chatId][context.messages[chatId].length - 1].seq
                                : -1;

                        if (!context.messagesQueueByChannel[channel][lastMessageSeq + 1]) {
                            hasNoNextVersion.push(channel);
                        }
                    });
                    return hasNoNextVersion.length === 0;
                },
            },
        }
    );
};

/**
 * Initialize chat transport SM service
 * @param {Object} params
 * @param {Function} params.pubnub - PubNub SDK
 * @param {string} [params.pubnubOrigin] - custom pubnub origin
 * @param {TransportWrapper} params.transport - instance of TransportWrapper
 * @param {number} params.gapDelay - ms to wait on gap found
 * @param {number} params.transportReconnectDelay - ms to wait on transport connection error
 * @param {number} params.recoveryDelay - ms to wait without new messages
 * @param {number} params.connectRecoveryDelay - ms to wait on transport connecting
 * @param {Function} params.onChange - callback to report chat changes to conference
 * @param {Function} params.reportRecovery - callback to report recovery from transport
 * @param {Function} params.reportActivated - callback to report transport activation
 *
 * @return {object} chat transport SM
 */
const chatTransportInterpretFabric = ({
    pubnub,
    pubnubOrigin,
    transport = null,
    reportRecovery,
    logger,
    ...rest
}) => {
    if (!transport) {
        transport = new TransportWrapper({ pubnub, pubnubOrigin });
    }

    const inter = interpret(
        chatTransportMachineFabric({
            transport,
            reportRecovery,
            ...rest,
        })
    )
        .onTransition(logTransition(logger))
        .start();

    transport.on(TransportWrapper.EVENTS.TIMEOUT, reportRecovery);
    transport.on(TransportWrapper.EVENTS.UNKNOWN_ERROR, () => inter.send(TRANSPORT.UNKNOWN_ERROR));

    transport.on(TransportWrapper.EVENTS.DATA, data => {
        if (typeof data.data === 'string') {
            inter.send(CHAT.NEW_CHAT_FROM_TRANSPORT, { data });
        } else {
            inter.send(CHAT.RECEIVE_CHAT_MESSAGE, {
                data: { channel: data.channel, data: data.data },
            });
        }
    });

    return inter;
};

/**
 * Initialize chat transport SM events
 * @param {Object} params
 * @param {import('../event-bus').EventBus} params.eventBus - event bus for all running state machines
 * @param {Function} params.pubnub - PubNub SDK
 * @param {TransportWrapper} params.transport - instance of TransportWrapper
 * @param {number} params.gapDelay - ms to wait on gap found
 * @param {number} params.transportReconnectDelay - ms to wait on transport connection error
 * @param {number} params.recoveryDelay - ms to wait without new messages
 * @param {number} params.connectRecoveryDelay - ms to wait on transport connecting
 * @param {Function} params.onChange - callback to report chat changes to conference
 * @param {Function} params.reportRecovery - callback to report recovery from transport
 * @param {Function} params.reportActivated - callback to report transport activation
 *
 * @return {object} chat transport SM
 */
const chatTransportEventsFabric = ({ eventBus, getPrefixedLogger, ...rest }) => {
    const logger = getPrefixedLogger(STATE_MACHINES.CHAT_TRANSPORT);
    const reportRecovery = (newChatId = null) => eventBus.send(CHAT.TRANSPORT_TIMEOUT, newChatId);
    const reportActivated = () => eventBus.send(CHAT.TRANSPORT_ACTIVATED);
    const reportReactivate = () => eventBus.send(CHAT.TRANSPORT_REACTIVATE);
    const inter = chatTransportInterpretFabric({
        ...rest,
        reportRecovery,
        reportActivated,
        reportReactivate,
        logger,
    });

    const unlisten = eventBus.listen(
        [
            MEETING.LEAVE,
            CHAT.GET_META,
            MEETING.INVALIDATE,
            MEETING.INVALIDATE_ROOM,
            CHAT.RECEIVE_NEW_DATA,
        ],
        inter.send.bind(inter)
    );

    inter.onDone(unlisten);

    return withAsyncAutoStop(inter, eventBus, logger);
};

module.exports = {
    default: chatTransportInterpretFabric,
    chatTransportEventsFabric,
};
