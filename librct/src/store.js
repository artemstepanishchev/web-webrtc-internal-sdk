const createStore = (reducer, initialState) => {
    let state = initialState;
    let listeners = [];

    const dispatch = action => {
        state = reducer(state, action);
        listeners.forEach(listener => listener(state));
        return action;
    };

    const getState = () => state;

    const subscribe = listener => {
        listeners.push(listener);
        return () => {
            listeners = listeners.filter(l => l !== listener);
        };
    };

    return Object.freeze({ dispatch, getState, subscribe });
};

module.exports = { createStore };
