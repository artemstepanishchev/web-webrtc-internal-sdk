const demandType = {
    UNMUTE_DEMAND: 'unmute',
    UNMUTE_VIDEO_DEMAND: 'unmute_video',
    CONTROL_DEMAND: 'control',
    ANNOTATE_DEMAND: 'annotate',
};

const demandDecision = {
    APPROVE_DEMAND: 'approve',
    REJECT_DEMAND: 'reject',
};

const streamType = {
    SELF_VIDEO: 'video/main',
    SELF_SCREEN: 'video/screensharing',
    AUX_VIDEO: 'video/aux',
    AUX_AUDIO: 'audio/aux',
};

const loading = {
    IDLE: 'idle',
    SUCCESS: 'success',
    LOADING: 'loading',
    FAILURE: 'failure',
};

const sessionType = {
    PSTN: 'rcv/pstn',
};

module.exports = {
    demandType,
    streamType,
    demandDecision,
    loading,
    sessionType,
};
