var Model               = require('./model');
var util                = require('util');
var TransportHttp       = require('./transport').TransportHttp;

function UserSetting(params, options={}) {
    Model.call(this, params, options);
    this._transport = new TransportHttp(options);

    this.registerModelFields([
        'id',
        'value',
        'readOnly',
        'canModifyAccess'
    ]);
    this.set(params);
}

util.inherits(UserSetting, Model);

UserSetting.prototype.url = function () {
    return `${this.getVersionedBaseUrl()}/account/${this._options.accountId}/extension/${this._options.extensionId}/preferences/${this.get('id')}`;
};


/**
 * UserSettings class to work with USS
 * @param {Object} options inner properties and methods
 * @constructor
 */
function UserSettings (params, options) {
    Model.call(this, params, options);

    this._transport = new TransportHttp(options);
    this.registerModelFields([
        'values',
    ], {
        values: (p, o) => {
            const s = new UserSetting(p, o);

            s.on('change', () => {
                this._onDataPatch('change');
            });

            return s;
        }
    });
    this.set(params);
}

util.inherits(UserSettings, Model);

UserSettings.prototype.url = function () {
    return `${this.getVersionedBaseUrl()}/account/${this._options.accountId}/extension/${this._options.extensionId}/preferences`;
};

UserSettings.prototype.setSettings = function (params) {
    return Promise.all(
        this.get('values').map(prop => {
            const id = prop.get('id');

            if (params.hasOwnProperty(id)) {
                const patch = { value: params[id] };

               return prop.update(patch).then(
                    () => {
                        prop.set(patch);
                    }
                );
            }
        }).filter(
            Boolean
        )
    );
};

class SettingsInterface {
    constructor(options={}) {
        this._options = options;
    }

    init (accountId, extensionId) {
        this._ss = new UserSettings({
            values: []
        }, {
            ...this._options,
            accountId,
            extensionId
        });

        return this._ss.read().then(values => {
            this._ss.set({ values });

            return this._ss.toJSON();
        });
    }

    subscribe (eventType, cb) {
        if (this._ss) {
            this._ss.on(eventType, cb);
        }
    }

    push (data) {
        return this._ss.setSettings(data);
    }
}

module.exports = SettingsInterface;
