var Model               = require('./model');
var TransportHttp       = require('./transport').TransportHttp;
var serializeParams       = require('./utils').serializeParams;

/**
 * Get User Conferencing Settings
 * {@link https://developers.ringcentral.com/api-reference#User-Settings-loadConferencingInfo}
 *
 * @param       {Object} options inner properties and methods
 * @constructor
 */

class ConferencingSettings extends Model {
    constructor (options) {
        super({}, options);
        this._transport = new TransportHttp(options);
        this._modelFields = [];
    }

    url () {
        return '/account/~/extension/~/conferencing';
    }

    getCountryPhoneNumbers (countryId = 1) {
        const query = { countryId };
        return this._transport._read(this.url() + serializeParams(query));
    }
}

module.exports = ConferencingSettings;
