const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const CodeGen = require('swagger-js-codegen').CodeGen;
const cloneDeep = require('lodash.clonedeep');
const { toKebabCase } = require('./utils');

const SRC_DIR = __dirname;
const SWAGGER_DIR = path.join(SRC_DIR, 'swagger-specs');
const TEMPLATES_DIR = path.join(SRC_DIR, 'templates');

const readTemplate = name => fs.readFileSync(path.join(TEMPLATES_DIR, name), 'utf-8');

const RCT_OPERATION_IDS = {
    '/bridges/{bridgeId}/meetings': { post: 'createMeeting' },
    '/bridges/{bridgeId}/meetings/{meetingId}': {
        get: 'getMeeting',
        delete: 'deleteMeeting',
        patch: 'updateMeeting',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/channels': {
        post: 'createChannel',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/channels/{channelId}': {
        delete: 'deleteChannel',
        patch: 'updateChannel',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{requesterId}/demands': {
        post: 'createDemandRequest',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/demands/{demandId}': {
        patch: 'updateDemandRequest',
        delete: 'cancelDemandRequest',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/dials': {
        post: 'createDial',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/dials/{dialId}': {
        get: 'getDial',
        delete: 'deleteDial',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}': {
        delete: 'deleteParticipant',
        patch: 'updateParticipant',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/recordings': {
        post: 'createRecording',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/recordings/{recordingId}': {
        patch: 'updateRecording',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/sessions/{sessionId}': {
        patch: 'updateMuteState',
        delete: 'deleteSession',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/sessions/{sessionId}/pong': {
        post: 'pong',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/streams': {
        post: 'createStream',
    },
    '/bridges/{bridgeId}/meetings/{meetingId}/participants/{participantId}/streams/{streamId}': {
        patch: 'updateStream',
        delete: 'deleteStream',
    },
};

const patchRctApiWithOperationIds = api => {
    Object.keys(RCT_OPERATION_IDS).forEach(path => {
        if (!api.paths[path]) {
            throw new Error(`RCT API does not contain the following path: ${path}`);
        }

        Object.keys(RCT_OPERATION_IDS[path]).forEach(method => {
            if (!api.paths[path][method]) {
                throw new Error(
                    `Method '${method}' is not defined for the following path of RCT API:${path}`
                );
            }

            api.paths[path][method].operationId = RCT_OPERATION_IDS[path][method];
        });
    });
};

const patchWithBasePath = api => {
    api.paths = Object.keys(api.paths).reduce((acc, path) => {
        acc[api.basePath + path] = api.paths[path];
        return acc;
    }, {});
};

const patchRctApi = api => {
    const apiToPatch = cloneDeep(api);

    patchRctApiWithOperationIds(apiToPatch);
    patchWithBasePath(apiToPatch);

    return apiToPatch;
};

function generateJSApi(swaggerFile, name, patch) {
    const filePath = path.join(SWAGGER_DIR, swaggerFile);
    const json = yaml.safeLoad(fs.readFileSync(filePath, 'utf8'));
    const patchedJson = patch ? patch(json) : json;

    const code =
        CodeGen.getReactCode({
            className: name,
            swagger: patchedJson,
            template: {
                class: readTemplate('class.mustache'),
                method: readTemplate('method.mustache'),
                type: readTemplate('type.mustache'),
            },
        }) + '\r\n';

    fs.writeFile(`src/apis/${toKebabCase(name)}-api.js`, code, err => {
        if (err) throw err;

        // eslint-disable-next-line no-console
        console.log(`${name} api is ready!`);
    });
}

generateJSApi('ndb.yml', 'Ndb');
generateJSApi('rct.yml', 'Rct', patchRctApi);
generateJSApi('rch.yml', 'Rch');
generateJSApi('rct-layouts.yml', 'RctLayouts');
