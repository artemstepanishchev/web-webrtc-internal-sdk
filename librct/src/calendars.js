const Model               = require('./model');
const TransportHttp       = require('./transport').TransportHttp;
const GOOGLE = 'google';

/**
 * @typedef {Object} Numbers
 * @param {String}  number
 * @param {String}  country
 * @param {String}  location
 */

/**
 * @typedef {Object} InvitationParams
 * @param {String}  hostName                - host name
 * @param {String}  meetingName             - meeting name
 * @param {String}  meetingId               - meeting id
 * @param {String}  [meetingUrl]            - meeting url
 * @param {String}  internationalNumbersUrl - international number
 * @param {String}  mainPhoneNumber         - main phone number (deprecated, use numbers instead)
 * @param {Array.<Numbers>} [numbers]       - phone numbers
 */


/**
 * @typedef {Object} UrlParams
 * @param {String} [provider=GOOGLE]
 * @param {String}  accountId
 * @param {String}  extensionId
 * @param {String}  calendarId
 */

/**
 * @typedef {Object} EventCreationParams
 * @param {String} [title = ''] - Title of event. Max length: 255
 * @param {String} [location = ''] - Location of event
 * @param {String} [description = ''] - Description of event. Max length: 8191
 * @param {String} [startTime=''] - Start date of event in format: yyyy-MM-dd'T'HH:mm:ss[.SSS]'Z'
 * @param {String} [endTime=''] - End date of event in format: yyyy-MM-dd'T'HH:mm:ss[.SSS]'Z'. Should be after startTime
 */

class Calendars extends Model {
    constructor (options) {
        super({}, options);

        this._transport = new TransportHttp(options);
    }

    url () {
        return this.getVersionedBaseUrl() + '/scheduling';
    }

    /**
     * get providers ids
     * @return {Promise.<Object>} object with providers
     * @example const PROVIDERS = librct.calendars().getProviders();
     */
    getProviders () {
        return this.apiCall(this.url() + '/providers', {
            method: 'GET'
        });
    }
    /**
     * connect to provider by id
     * @param {string} providerId
     * @param {string} redirectUrl
     * @return {Promise.<string>}
     */
    connect (providerId, redirectUri) {
        return this.apiCall(`${this.url()}/providers/${providerId}/auth?redirectUri=${redirectUri}`, {
            method: 'GET'
        });
    }
    /**
     * disconnect from provider by id
     * @param {string} providerId
     * @return {Promise}
     */
    disconnect (providerId) {
        return this.apiCall(`${this.url()}/providers/${providerId}/auth`, {
            method: 'DELETE'
        });
    }
    /**
     * get calendars by provider id
     * @param {string} providerId
     * @return {Promise.<Array>}
     */
    getCalendars (providerId) {
        return this.apiCall(`${this.url()}/providers/${providerId}/calendars`, {
            method: 'GET'
        });
    }
    /**
     * get calendar events
     * @param {string}  providerId
     * @param {string}  calendarId
     * @param {boolean} [includeNonRcEvents=false]
     * @return {Promise.<Array>}
     */
    getCalendarEvents (providerId, calendarId, includeNonRcEvents = false) {
        calendarId = encodeURIComponent(calendarId);
        const startTimeTo = new Date();
        startTimeTo.setDate(startTimeTo.getDate() + 7);
        const startTimeFrom = new Date();

        return this.apiCall(
            `${this.url()}/providers/${providerId}/calendars/${calendarId}` +
            `/events?startTimeFrom=${startTimeFrom.toISOString()}&startTimeTo=${startTimeTo.toISOString()}&includeNonRcEvents=${includeNonRcEvents}`, {
                method: 'GET',
                connectionTimeout: 10000,
        });
    }

    /**
     * generate ics file for mail clients
     * @param  {InvitationParams} [params={}]
     * @return {Promise}    - .ics file as attachment
     * @example librct.calendars().generateEventFile({ hostName: 'My name', meetingName: 'meeting1'});
     */
    generateEventFile (params = {}) {
        const { meetingUrl, ...rest } = params;

        return this.getPlainTextInvitation(rest)
            .then(({ body, subject }) => {
                const icsUrl = `/restapi/v1.0/invitations/ics?subject=${subject}&location=${meetingUrl}`;
                return this.apiCall(icsUrl, {
                    method: 'POST',
                    body
                });
            });
    }

    /**
     * get event creation url
     * @param  {String} [provider=GOOGLE] provider id from static calendars.getProviders() method
     * @param  {InvitationParams} [params={}]
     * @return {Promise.<String>}   - redirect url
     * @example const PROVIDERS = librct.calendars().getProviders();
     * librct.calendars().getEventCreationUrl(PROVIDERS.GOOGLE, {
     *   hostName: 'My name',
     *   meetingName: 'meeting1'
     * }).then(url => console.log(url));
     */
    getEventCreationUrl (provider = GOOGLE, params = {}) {
        return this.apiCall(this.url() + `/invitations/providers/${provider}`, {
            method: 'POST',
            body: params
        }).then(result => result.url);
    }

    /**
     * Create schedule event on platform CIN
     *
     * @param {UrlParams} - params for url creating
     * @param {EventCreationParams}
     * @return {Promise.<EventObject>}
     */
    platformCreateEvent ({
            provider = GOOGLE,
            accountId,
            extensionId,
            calendarId
        },
        {
            title = '',
            location = '',
            description = '',
            startTime = '',
            endTime = ''
        }) {
        calendarId = encodeURIComponent(calendarId);
        const now = new Date();
        const nhours = now.getHours();

        if (!startTime) {
            startTime = new Date(now);
            startTime.setHours(nhours + 1, 0, 0);
            startTime = startTime.toISOString();
        }
        if (!endTime) {
            endTime = new Date(now);
            endTime.setHours(nhours + 2, 0, 0);
            endTime = endTime.toISOString();
        }

        const url = `/account/${accountId}/extension/${extensionId}/scheduling/providers/${provider}/calendars/${calendarId}/events`;

        return this.apiCall(url, {
            method: 'POST',
            body: {
                title,
                location,
                description,
                startTime,
                endTime
            }
        });
    }

    /**
     * get plain text invitations
     * @param  {InvitationParams} params
     * @return {Promise.<Object>} object with subject<text> and body<text>
     */
    getPlainTextInvitation (params, isolatedMode = true) {
        params.numbers?.forEach((numberObject) => {
            numberObject.unformattedNumber = numberObject.number.replace(/[\(\)\s]/g, ''); // e.g. +19998887755
        });

        return this.apiCall('/uns/render-document', {
            method: 'POST',
            body: {
                'notificationId': 'meetingInvite',
                'parameters': Object.entries(params).map(([parameterName, parameterValue]) => ({
                    parameterName,
                    parameterValue
                })),
                'plainTextPreferred': true,
                isolatedMode,
            }
        })
            .then(response => response.formData())
            .then(response => Object.fromEntries(response.entries()));
    }

    /**
     * Disable calendars
     * @param {String} provider provider id
     * @param {Array.<String>} ids calendars ids to disable
     * @return {Promise}
     */
    disable (providerId, ids) {
        return this.apiCall(`${this.url()}/providers/${providerId}/calendars/disable`, {
            method: 'POST',
            body: ids
        });
    }

    /**
     * Delete events from calendar
     * @param {String} providerId provider id
     * @param {String} calendarId calendar id
     * @param {String} eventId event id
     * @return {Promise}
     */
    deleteEvent (providerId, calendarId, eventId) {
        return this.apiCall(`${this.url()}/providers/${providerId}/calendars/${calendarId}/events/${eventId}`, {
            method: 'DELETE'
        });
    }
}

module.exports = Calendars;
