const { PN_CHANNELS, LAYOUT_IDS } = require('../machines/constants');

// TODO: revert this commit after RCV-46294 resolving
const MAXIMUM_PING_INTERVAL = 3 * 24 * 60 * 60 * 1000;

const notifyChannelFields = {
    version: true,
    pingInterval: ({ notifyChannel, channelId, meeting }) => {
        const areRoomsOpened = meeting.appliedLayoutId === LAYOUT_IDS.USER;
        const isMeetingParticipantsChannel = channelId === PN_CHANNELS.MEETING_PARTICIPANTS;
        if (isMeetingParticipantsChannel) {
            return areRoomsOpened ? notifyChannel.pingInterval : MAXIMUM_PING_INTERVAL;
        }
        return notifyChannel.pingInterval;
    },
    pingDelay: true,
    channel: ({ notifyChannel }) => notifyChannel.channel || notifyChannel.channelId
};

module.exports = notifyChannelFields;
