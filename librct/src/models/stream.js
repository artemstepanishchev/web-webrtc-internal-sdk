/*
 * Please note: some fields here may be calculated with various entities, e.g. 'sessions' or 'streams'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const streamFields = {
    id: true,
    audio: true,
    bridgeId: true,
    callId: true,
    participantId: true,
    sessionId: true,
    startTime: true,
    subtype: true,
    type: true,
    video: true,
    deleted: true,
    isSessionInactive: ({ session = {} }) => Boolean(session.deleted) || Boolean(session.hasLeftBreakoutRoom),
};

module.exports = streamFields;
