/*
 * Please note: some fields here may be calculated with various entities, e.g. 'sessions' or 'streams'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const localSessionFields = {
    id: true,
    bridgeId: true,
    callId: true,
    callstatsToken: true,
    deleted: true,
    deleteReason: true,
    localMute: true,
    localMuteVideo: true,
    notificationToken: true,
    notificationSubKey: true,
    onHold: true,
    operatingSystem: true,
    participantId: true,
    pingInfo: true,
    serverMute: true,
    serverMuteVideo: true,
    token: true,
    userAgent: true,
    uri: true,
};

module.exports = localSessionFields;
