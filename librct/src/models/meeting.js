/*
 * Please note: some fields here may be calculated with various entities, e.g. 'meeting' or 'streams'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const meetingFields = {
    id: true,
    allowDials: true,
    allowJoin: true,
    allowUnmute: true,
    allowUnmuteVideo: true,
    allowScreenSharing: true,
    allowAnnotations: true,
    allowChats: true,
    bridgeId: true,
    cloudRecordingsEnabled: true,
    deleted: true,
    deleteReason: true,
    deletedChangedBy: true,
    durationLimit: true,
    meetingPassword: true,
    meetingPasswordPSTN: true,
    meetingPasswordMasked: true,
    mute: true,
    muteVideo: true,
    parentId: true,
    participants: true,
    recordings: true,
    startTime: true,
    wsConnectionUrl: true,
    closedCaptionsEnabled: true,
    e2ee: false,    roomId: true,
    roomName: true,
    appliedLayoutId: true,
    layoutIds: true,
    features: true
};

module.exports = meetingFields;
