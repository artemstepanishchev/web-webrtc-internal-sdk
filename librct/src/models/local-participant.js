const types = require('../types');

const getActivePstnSession = session =>
    session.userAgent === types.sessionType.PSTN && !session.deleted;

/*
 * Please note: some fields here may be calculated with various entities, e.g. 'sessions' or 'streams'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const participantFields = {
    id: true,
    allowUnmute: true,
    allowUnmuteVideo: true,
    bridgeId: true,
    callerInfo: true,
    callId: true,
    deleted: true,
    deleteReason: ({ localSession }) => localSession?.deleteReason || null,
    displayName: true,
    joinTime: true,
    host: true,
    moderator: true,
    noanswerTime: true,
    serverMute: true,
    serverMuteVideo: true,
    rejectReason: true,
    rejectTime: true,
    ringing: true,
    temporaryModerator: true,
    shortPrtsPin: true,
    uri: true,
    localMute: ({ localSession, restParticipantSessions: sessions }) => {
        const activePstnSession = Object.values(sessions).find(getActivePstnSession);
        if (activePstnSession) {
            return activePstnSession.serverMute;
        }
        return localSession ? localSession.localMute : false;
    },
    hasActiveSessions: ({ localSession }) => !localSession?.deleted,
    localMuteVideo: ({ localSession }) => (localSession ? localSession.localMuteVideo : false),

    onHold: ({ localSession }) => (localSession ? localSession.onHold : false),
    joinedAudio: ({ localStreams }) => {
        const selfVideoStreams =
            localStreams &&
            Object.values(localStreams).filter(
                stream => stream.type === types.streamType.SELF_VIDEO
            );
        return (selfVideoStreams || []).length > 0
            ? selfVideoStreams.some(stream => (stream.audio || {}).isActiveOut)
            : null;
    },
    pstnSessionId: ({ restParticipantSessions: sessions }) => {
        const activePstnSession = Object.values(sessions).find(getActivePstnSession);
        return activePstnSession ? activePstnSession.id : null;
    },
    alreadyJoinedAudio: ({ restParticipantStreams: streams }) =>
        Object.values(streams).some(
            stream =>
                !stream.deleted &&
                stream.type !== types.streamType.SELF_SCREEN &&
                stream.audio.isActiveOut
        ),
    local: () => true,
};

module.exports = participantFields;
