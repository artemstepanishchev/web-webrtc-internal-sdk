const meetingParticipantFields = {
    id: ({ participantId }) => participantId,
    status: true, // init|join|leave
    displayName: true,
    roomId: true,
};

module.exports = meetingParticipantFields;
