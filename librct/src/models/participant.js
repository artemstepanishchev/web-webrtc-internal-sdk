const types = require('../types');
const { participantLeftRoom } = require('../utils');

const onlyExisting = val => !val.deleted;

/*
 * Please note: some fields here may be calculated with various entities, e.g. 'sessions' or 'streams'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const participantFields = {
    id: true,
    allowUnmute: true,
    allowUnmuteVideo: true,
    bridgeId: true,
    callerInfo: true,
    callId: true,
    deleted: true,
    deleteReason: true,
    displayName: true,
    joinTime: true,
    host: true,
    moderator: true,
    noanswerTime: true,
    serverMute: ({ sessions, participant }) => {
        const pstnSession = Object.values(sessions)
            .filter(onlyExisting)
            .find(session => session.userAgent === types.sessionType.PSTN);

        if (participant.serverMute) return participant.serverMute;

        return pstnSession ? pstnSession.serverMute : participant.serverMute;
    },
    serverMuteVideo: true,
    rejectReason: true,
    rejectTime: true,
    ringing: true,
    temporaryModerator: true,
    shortPrtsPin: true,
    uri: true,
    waitingRoomStatus: true,
    roomId: true,
    hasLeftBreakoutRoom: ({ meeting, participant }) => participantLeftRoom(participant, meeting),
    localMute: ({ sessions, participant, streams }) => {
        if (participant.serverMute) {
            return participant.serverMute;
        }
        const streamSessions = Object.values(streams)
            // isActiveOut - is indicates the connection of mic.
            .filter(stream => stream?.audio?.isActiveOut && onlyExisting(stream))
            .map(stream => sessions[stream.sessionId])
            .filter(Boolean)
            .filter(onlyExisting);

        return streamSessions.length && streamSessions.every(session => session.localMute);
    },
    localMuteVideo: ({ sessions, participant }) =>
        participant.serverMuteVideo
            ? participant.serverMuteVideo
            : Object.values(sessions)
                  .filter(onlyExisting)
                  .every(session => session.localMuteVideo),
    hasInactiveSessions: ({ sessions }) =>
        Object.values(sessions).some(session => session.deleted || session.hasLeftBreakoutRoom),
    hasActiveSessions: ({ sessions }) =>
        Object.values(sessions).some(session => !session.deleted && !session.hasLeftBreakoutRoom),

    onHold: ({ sessions }) => {
        const sessionsArray = Object.values(sessions);
        return sessionsArray.length > 0 && sessionsArray.every(session => session.onHold);
    },
    // Indicates if one of participant streams has joined audio.
    joinedAudio: ({ streams }) =>
        Object.values(streams).some(
            stream =>
                stream.type === types.streamType.SELF_VIDEO && (stream.audio || {}).isActiveOut
        ),
    audioFromPstn: ({ sessions }) =>
        Object.values(sessions).some(
            session => session.userAgent === types.sessionType.PSTN && !session.deleted
        ),
    isPurePSTN: ({ sessions }) =>
        Object.values(sessions)
            .filter(onlyExisting)
            .every(session => session.userAgent === types.sessionType.PSTN),
    pstnSessionId: ({ sessions }) => {
        const activePstnSession = Object.values(sessions).find(
            session => session.userAgent === types.sessionType.PSTN && !session.deleted
        );
        return activePstnSession ? activePstnSession.id : null;
    },
};

module.exports = participantFields;
