/*
 * Please note: some fields here may be calculated with various entities, e.g. 'sessions' or 'streams'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const recordingsFields = {
    id: true,
    started: true,
    paused: true,
    startTime: true,
    downloadPageUri: true,
    bridgeId: true,
    callId: true,
    ownerInfo: true,
    recordingEvents: true,
};

module.exports = recordingsFields;
