/*
 * Please note: some fields here may be calculated with various entities, e.g. 'demand' or 'localParticipantId'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const dialFields = {
    id: true,
    status: true,
    type: true,
    number: true,
    callerId: true,
    calleeId: true,
    deleted: true,
};

module.exports = dialFields;
