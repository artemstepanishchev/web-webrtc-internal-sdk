/*
 * Please note: some fields here may be calculated with various entities, e.g. 'demand' or 'localParticipantId'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const channelsFields = {
    id: true,
    bridgeId: true,
    callId: true,
    participantId: true,
    sessionId: true,
    type: true,
    direction: true,
    createTime: true,
    info: true,
    deleted: true,
    allowAnnotations: true
};

module.exports = channelsFields;
