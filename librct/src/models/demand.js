/*
 * Please note: some fields here may be calculated with various entities, e.g. 'demand' or 'localParticipantId'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const demandFields = {
    id: true,
    participantId: true,
    type: true,
    status: true,
    createTime: true,
    isForAll: ({ demand }) => !demand.resource,
    decisions: true,
    myDecision: ({ demand, localParticipantId }) => {
        if (demand.decisions) {
            const myDecision = Object.values(demand.decisions).find(
                dec => dec.id === localParticipantId
            );
            if (myDecision) {
                return myDecision.status;
            }
        }

        return null;
    },
};

module.exports = demandFields;
