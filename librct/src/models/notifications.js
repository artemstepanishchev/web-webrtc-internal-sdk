/*
 * Please note: some fields here may be calculated with various entities, e.g. 'sessions' or 'streams'.
 * If you want to add or change it - go to recalculator.js and find the corresponding mapper.
 * */
const notificationsFields = {
    meta: true,
    time: true,
    type: true,
};

module.exports = notificationsFields;
