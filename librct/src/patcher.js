const isEqual = require('lodash.isequal');

const { classify, participantLeftRoom } = require('./utils');
const { streamType } = require('./types');
const { PN_CHANNELS } = require('./machines/constants');

/**
 * Deletes entity in the passed object by key and returns it.
 * @param {Object} obj
 * @param {string} key
 * @returns {any} torn out entity
 */
const tearOutEntity = (obj, key) => {
    const entity = obj[key];
    if (!entity) return null;

    delete obj[key];
    return entity;
};

/**
 * mutates an obj by replacing array field with array of ids and also classified old array into dest object
 * @param {Object} obj - source object
 * @param {string} key - source object key to replace
 * @param {Object?} dest - destination object for classified array
 * @param {Object?} additionalFields - fields to add to each object from obj[key] array
 * @param {string?} idKey - id key in obj[key] array
 * @returns {Object} torn out and classified array
 */
const tearOutArray = (obj, key, dest, merge, idKey = 'id') => {
    const entity = tearOutEntity(obj, key);
    return entity ? classify(idKey, entity, merge, dest) : {};
};

const tearOutNotifyChannels = (meeting, localParticipant) => {
    const meetingMainChannel = tearOutEntity(meeting, 'channelInfo');
    const notifyChannels = tearOutEntity(meeting, 'notifyChannels') || {};
    const participantChannels =
        (localParticipant && tearOutEntity(localParticipant, 'notifyChannels')) || {};

    return {
        [PN_CHANNELS.MEETING]: meetingMainChannel,
        [PN_CHANNELS.MEETING_PARTICIPANTS]: notifyChannels.meetingParticipants,
        [PN_CHANNELS.PRIVATE]: participantChannels.private,
    };
};

/**
 * Cheks if patch contains any information to change
 * @param {Object} p - patch
 * @param {Object} keysToIgnore - keys to ignore
 * @returns {boolean} - result
 */
const isPatchEmpty = (p, keysToIgnore = ['id']) =>
    !p || !Object.keys(p).some(key => !keysToIgnore.includes(key));

/**
 * Returns null if patch has no information to change, returns patch overwise
 * @param {Object} p - patch
 * @param {Object} keysToIgnore - keys to ignore
 * @returns {?Object} - result
 */
const ignoreEmptyPatch = (p, keysToIgnore = ['id']) => (isPatchEmpty(p, keysToIgnore) ? null : p);

/**
 * returns only valuable patch elements from object
 * @param {Object} p - patch
 * @param {Object} keysToIgnore - keys to ignore
 * @returns {?Object} - result
 */
const removeEmptyPatchProps = (p, keysToIgnore = ['id']) => {
    let valuable = false;
    Object.keys(p).forEach(key => {
        if (isPatchEmpty(p[key], keysToIgnore)) {
            delete p[key];
        } else {
            valuable = true;
        }
    });

    return valuable ? p : null;
};

/** Prevents redundant updates by deleting meeting unused props */
const preventAdditionalUpdates = meeting => {
    delete meeting.time;
    delete meeting.version;

    return meeting;
};

/**
 * @typedef {Object} SplittedPatches
 * @property {?Object} meeting - pure meeting patch
 * @property {?Object} participants - pure participants patch (by ids)
 * @property {?Object} sessions - pure sessions patch (by ids)
 * @property {?Object} streams - pure streams patch (by ids)
 * @property {?Object} localParticipant - local participant
 * @property {?Object} localSession - local participant
 * @property {?Array<Object>} localStreams - local participant
 */

/**
 * Split meeting patch to pure meeting, participants, sessions, streams, etc. patches
 * @param {Object} patch - meeting patch to split
 * @param {Object.<string, (string|string[])>} localIds
 * @param {Object.<string, string>} localSessionHistory
 * @returns {SplittedPatches}
 */
const splitPatch = (
    patch,
    { localParticipantId = null, localSessionId = null, localStreamIds = [] } = {},
    localSessionHistory = {},
    prevMeeting = {}
) => {
    const meeting = JSON.parse(JSON.stringify(patch)); // prevent patch mutation by making a deep copy of it
    const participants = tearOutArray(meeting, 'participants');
    const sessions = {};
    const streams = {};
    const channels = {};
    const demands = {};
    const dials = {};
    let localParticipant = null;
    const localStreams = {};
    let localSession = null;
    let localSessionHistoryFrame = null;
    const targetedToMeDemands = {};
    const myDemands = {};
    const myDials = {};
    const recordings = tearOutArray(meeting, 'recordings');
    const notifications = tearOutArray(meeting, 'notifications');
    const meetingParticipants = tearOutEntity(meeting, 'meetingParticipants') || {};

    Object.keys(participants).forEach(pId => {
        const p = participants[pId];
        // pls note: justCreated first!
        if (p.justCreated || pId === localParticipantId) {
            // we split localParticipant and other ones to improve performance during localParticipant updates
            delete p.justCreated;
            localParticipant = p;
            localParticipantId = p.id;
        }
        // we delete these properties because participant moves between rooms with the same session and
        // it shouldn't be marked as deleted. RCT sends such patches to support old clients.
        if (participantLeftRoom(p, prevMeeting)) {
            delete p.deleted;
            delete p.deleteReason;
        }
        const withParticipantId = item => ({ ...item, participantId: p.id });
        tearOutArray(p, 'sessions', sessions, withParticipantId);
        tearOutArray(p, 'streams', streams, withParticipantId);
        tearOutArray(p, 'channels', channels, withParticipantId);
        tearOutArray(p, 'demands', demands, withParticipantId);
        tearOutArray(p, 'dials', dials, withParticipantId);
    });

    const notifyChannels = tearOutNotifyChannels(meeting, localParticipant);

    Object.values(streams).forEach(s => {
        if (
            s.justCreated ||
            localStreamIds.includes(s.id) ||
            (s.type === streamType.SELF_SCREEN &&
                !localStreamIds.includes(s.id) &&
                localSessionId === s.sessionId)
        ) {
            delete s.justCreated;
            localStreams[s.id] = s;
        }
    });

    Object.values(sessions).forEach(s => {
        if (s.justCreated || s.id === localSessionId) {
            delete s.justCreated;
            localSession = s;
        }

        const participant = participants[s.participantId];

        if (participantLeftRoom(participant, prevMeeting)) {
            delete s.deleted;
            delete s.deleteReason;
            s.hasLeftBreakoutRoom = true;
        } else {
            s.hasLeftBreakoutRoom = false;
        }
    });

    if (localParticipantId) {
        Object.values(demands).forEach(d => {
            if (d.participantId === localParticipantId) {
                myDemands[d.id] = d;
            }

            // target all || target localParticipant
            if (!d.resource || d.resource.participantId === localParticipantId) {
                targetedToMeDemands[d.id] = d;
            }
        });
        Object.values(dials).forEach(dial => {
            if (dial.participantId === localParticipantId) {
                myDials[dial.id] = dial;
            }
        });
    }

    // split local from others
    if (localParticipant) delete participants[localParticipant.id];
    if (localSession) {
        delete sessions[localSession.id];

        if (!localSessionHistory[localSession.id]) {
            localSessionHistoryFrame = { [localSession.id]: new Date().toISOString() };
        }
    }

    if (Object.keys(localStreams).length > 0) {
        Object.keys(localStreams).forEach(id => {
            delete streams[id];
        });
    }

    preventAdditionalUpdates(meeting);

    return {
        meeting: ignoreEmptyPatch(meeting),
        participants: removeEmptyPatchProps(participants),
        sessions: removeEmptyPatchProps(sessions, ['id', 'participantId']),
        streams: removeEmptyPatchProps(streams, ['id', 'participantId']),
        localParticipant: ignoreEmptyPatch(localParticipant),
        localSession: ignoreEmptyPatch(localSession, ['id', 'participantId']),
        localStreams: ignoreEmptyPatch(localStreams, ['id', 'participantId']),
        myDemands: ignoreEmptyPatch(myDemands, ['id', 'participantId']),
        targetedToMeDemands: ignoreEmptyPatch(targetedToMeDemands, ['id', 'participantId']),
        recordings: ignoreEmptyPatch(Object.values(recordings)),
        notifications: ignoreEmptyPatch(Object.values(notifications)),
        myDials: ignoreEmptyPatch(myDials, ['id', 'participantId']),
        channels: removeEmptyPatchProps(channels, [
            'id',
            'participantId',
            'allowAnnotationsChangedBy',
        ]),
        meetingParticipants: removeEmptyPatchProps(meetingParticipants),
        notifyChannels: removeEmptyPatchProps(notifyChannels, ['version']),
        localSessionHistory: ignoreEmptyPatch(localSessionHistoryFrame),
    };
};

/**
 * Aplly patch to the obj, with save-link check
 * @param {Object?} obj - source object
 * @param {Object?} patch - patch object
 * @return {Object|null} new (patched) entity if the patch contains new values, otherwise returns the old entity
 */
const patchEntity = (entity = null, patch = null) => {
    if (!patch) return entity;
    if (!entity) return patch;

    let newEntity = null;
    Object.keys(patch).forEach(key => {
        const newValue =
            patch[key] && typeof patch[key] === 'object'
                ? patchEntity(entity[key], patch[key])
                : patch[key];
        if (entity[key] === newValue) return;

        newEntity = newEntity || { ...entity };
        newEntity[key] = newValue;
    });

    return newEntity || entity;
};

/**
 * @typedef {Object} RCTCallAcc - an accumulator of splitted and classified RCT call data.
 * Stores all entities as collections.
 * @property {Object} meeting
 * @property {Object} participants
 * @property {Object} streams
 * @property {Object} sessions
 * @property {Object} localParticipant
 * @property {Object} localStreams
 * @property {Object} localSession
 * @property {Object} myDemands
 * @property {Object} targetedToMeDemands
 * @property {Object} recordings
 * @property {Object} notifications
 * @property {Object} myDials
 * @property {Object} channels
 * @property {Object} meetingParticipants
 * @property {Object} notifyChannels
 * @property {Object} localSessionHistory
 */

/**
 * Split into entities and apply the patch to call accumulator
 * @param {RCTCallAcc} call - accumulated splitted and classified RCT call data
 * @param {import('./apis/rct-api').Call} p - patch from RCT (full meeting or patch from sync transport)
 * @param {Object} localIds
 * @returns {RCTCallAcc} new state of the call
 */
const patch = (call, p, localIds) => {
    const splittedPatch = splitPatch(p, localIds, call.localSessionHistory, call.meeting);

    const entityKeys = Object.keys(splittedPatch);
    return Object.fromEntries(
        entityKeys.map(key => [key, patchEntity(call[key], splittedPatch[key])])
    );
};

module.exports = {
    patchEntity,
    splitPatch,
    tearOutArray,
    isPatchEmpty,
    patch,
};
