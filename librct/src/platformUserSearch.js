var Model               = require('./model');
var util                = require('util');
var TransportHttp       = require('./transport').TransportHttp;

/**
 * PlatformUserSearch class to work with platform fulltext user search
 *
 * @param       {Object} options inner properties and methods
 * @constructor
 */
function PlatformUserSearch (options) {
    Model.call(this, {}, options);

    this._transport = new TransportHttp(options);
}

util.inherits(PlatformUserSearch, Model);

PlatformUserSearch.prototype._modelFields = [];

PlatformUserSearch.prototype.url = function url () {
    // return '/account/~/directory/contacts/search';
    return '/account/~/directory/entries/search';
};

const searchParams = {
    extensionType: 'User',
    orderBy: [{
        index: 1,
        fieldName: 'firstName',
        direction: 'Asc'
    }, {
        index: 2,
        fieldName: 'email',
        direction: 'Asc'
    }],
    perPage: 50
};

PlatformUserSearch.prototype.search = function search (params) {
    return this._transport._create(this.url(), Object.assign({}, searchParams, params));
};

module.exports = PlatformUserSearch;
