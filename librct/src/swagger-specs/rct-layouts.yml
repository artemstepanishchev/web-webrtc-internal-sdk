swagger: '2.0'
info:
  title: RCT Layout Management API
  version: '1.0'
paths:
  '/rcvideo/v1/bridges/{bridgeId}/meetings/{meetingId}/layouts':
    parameters:
      - name: bridgeId
        in: path
        required: true
        type: string
      - name: meetingId
        in: path
        required: true
        type: string
    get:
      summary: List existing meeting layouts and a set of additional info
      operationId: listLayouts
      tags:
        - Layout Management
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/ConferenceInfo'
    post:
      summary: Create a new layout
      operationId: createLayout
      tags:
        - Layout Management
      parameters:
        - name: layout
          in: body
          schema:
            $ref: '#/definitions/ConferenceLayout'
      responses:
        '201':
          description: Created
          schema:
            $ref: '#/definitions/ConferenceLayout'
  '/rcvideo/v1/bridges/{bridgeId}/meetings/{meetingId}/layouts/{layoutId}':
    parameters:
      - name: bridgeId
        in: path
        required: true
        type: string
      - name: meetingId
        in: path
        required: true
        type: string
      - name: layoutId
        in: path
        required: true
        type: string
        enum:
          - user
    put:
      summary: Update layout by id (only applicable when layoutId = "user")
      operationId: updateLayoutById
      tags:
        - Layout Management
      parameters:
        - name: layout
          in: body
          schema:
            $ref: '#/definitions/ConferenceLayout'
      responses:
        '200':
          description: Updated
          schema:
            $ref: '#/definitions/ConferenceLayout'
    delete:
      summary: Delete layout by id (only applicable when layoutId = "user")
      operationId: deleteLayoutById
      tags:
        - Layout Management
      responses:
        '200':
          description: OK
  '/rcvideo/v1/bridges/{bridgeId}/meetings/{meetingId}/layouts/{layoutId}/apply':
    parameters:
      - name: bridgeId
        in: path
        required: true
        type: string
      - name: meetingId
        in: path
        required: true
        type: string
      - name: layoutId
        in: path
        required: true
        type: string
        enum:
          - user
          - main
    post:
      summary: Apply layout to the whole meeting. Only applicable to "user" layout.
      operationId: applyLayout
      tags:
        - Layout Management
      responses:
        '200':
          description: OK
  '/rcvideo/v1/bridges/{bridgeId}/meetings/{meetingId}/layouts/{layoutId}/applyToParticipants':
    parameters:
      - name: bridgeId
        in: path
        required: true
        type: string
      - name: meetingId
        in: path
        required: true
        type: string
      - name: layoutId
        in: path
        required: true
        type: string
        enum:
          - user
          - main
    post:
      summary: Apply layout to participants (move participants).
      operationId: applyLayoutToParticipants
      tags:
        - Layout Management
      parameters:
        - name: payload
          in: body
          schema:
            $ref: '#/definitions/ApplyToParticipantsPayload'
      responses:
        '200':
          description: OK
definitions:
  ConferenceInfo:
    description: Consolidated info about meeting state and available layouts
    properties:
      channelInfo:
        description: PubNub channel associated with this state
        $ref: '#/definitions/CallChannelInfo'
        readOnly: true
      actual:
        $ref: '#/definitions/ConferenceState'
      layouts:
        properties:
          user:
            $ref: '#/definitions/ConferenceLayout'
  ConferenceRoom:
    description: General representation of a room in a meeting (to be inherited)
    properties:
      id:
        type: string
        readOnly: true
        maxLength: 32
        minLength: 1
      settings:
        $ref: '#/definitions/ConferenceRoomSettings'
  ConferenceRoomSettings:
    description: Mutable room options (e.g. flags, access rights etc.)
    properties:
      name:
        type: string
        maxLength: 64
        minLength: 1
  ConferenceRoomParticipant:
    description: General representation of a room participant (to be inherited)
    properties:
      id:
        readOnly: true
        type: string
      name:
        readOnly: true
        type: string
      roomId:
        readOnly: true
        type: string
  ConferenceState:
    description: The actual state of the meeting in terms of rooms and room members
    properties:
      rooms:
        type: object
        additionalProperties:
          $ref: '#/definitions/ConferenceStateRoom'
  ConferenceStateRoom:
    description: State of a particular room in a meeting (inherits ConferenceRoom)
    allOf:
      - $ref: '#/definitions/ConferenceRoom'
      - properties:
          participants:
            type: object
            additionalProperties:
              $ref: '#/definitions/ConferenceStateRoomParticipant'
  ConferenceStateRoomParticipant:
    description: State of a particular room member in a meeting (inherits from ConferenceRoomParticipant)
    allOf:
      - $ref: '#/definitions/ConferenceRoomParticipant'
      - properties:
          status:
            readOnly: true
            type: string
            enum:
              - pending
              - joined
              - left
          role:
            readOnly: true
            type: string
            enum:
              - host
              - moderator
              - regular
  ConferenceLayout:
    description: Shape of an applicable layout in terms of rooms and room members
    required:
      - rooms
    properties:
      id:
        readOnly: true
        type: string
        enum:
          - user
      rooms:
        type: object
        additionalProperties:
          $ref: '#/definitions/ConferenceLayoutRoom'
  ConferenceLayoutRoom:
    description: Desired state of a particular room (inherits ConferenceRoom)
    required:
      - participants
    allOf:
      - $ref: '#/definitions/ConferenceRoom'
      - properties:
          deleted:
            type: boolean
          createdAt:
            description: Numeric timestamp in milliseconds of the date when the room first appeared in the layout
            type: number
            readOnly: true
          participants:
            type: object
            additionalProperties:
              $ref: '#/definitions/ConferenceLayoutRoomParticipant'
  ConferenceLayoutRoomParticipant:
    description: Desired state of a particular room member (inherits ConferenceRoomParticipant)
    allOf:
      - $ref: '#/definitions/ConferenceRoomParticipant'
      - properties:
          deleted:
            type: boolean
          assignedAt:
            description: Numeric timestamp in milliseconds of the date when the participant was added to the layout room
            type: number
            readOnly: true
          role:
            type: string
            enum:
              - moderator
              - regular
  ApplyToParticipantsPayload:
    description: Payload expected to be sent to POST /layouts/{layoutId}/applyToParticipants
    properties:
      participants:
        type: array
        items:
          type: string
  CallChannelInfo:
    description: A set of information for a client to be able to subscribe to a proper Pubnub channel
    readOnly: true
    properties:
      channel:
        type: string
        description: PubNub channel for layout notifications
        readOnly: true
      pingInterval:
        type: number
        description: description
        readOnly: true