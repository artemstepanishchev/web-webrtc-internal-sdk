const Rct = require('./apis/rct-api');
const Ndb = require('./apis/ndb-api');
const Rch = require('./apis/rch-api');
const RctLayouts = require('./apis/rct-layouts-api');

const generateId = require('./utils').generateId;

const { EventBus, EventEmitter } = require('./event-bus');
const rootEventsFabric = require('./machines/root').rootEventsFabric;
const pongEventsFabric = require('./machines/pong').pongEventsFabric;
const { syncEventsFabric, TYPES: SYNC_TYPES } = require('./machines/sync');
const { bridgePromisifiedFabric } = require('./machines/bridge');
const { meetingPromisifiedFabric } = require('./machines/meeting');
const { chatManagerEventsFabric } = require('./machines/chat-manager');
const { chatTransportEventsFabric } = require('./machines/chat-transport');
const TransportWrapper = require('./transport/transport-wrapper');
const {
    MEETING,
    CB_EVENTS,
    TRANSPORT,
    RECOVERY,
    LAYOUT_IDS,
    LAYOUTS,
} = require('./machines/constants');
const { layoutEventsFabric } = require('./machines/layouts');
const { demandType, demandDecision } = require('./types');

const getDefaultLogger = () => console;

// TODO add js dock, like in previous conference.js
class Conference extends EventEmitter {
    constructor(options = {}) {
        super();

        const { fetch, origin: domain } = options;

        this.resetMeetingData();

        this._options = options;

        this._sendBeacon =
            options.sendBeacon ||
            (typeof window !== 'undefined' ? window : global).navigator.sendBeacon;

        this.rct = new Rct({ fetch, domain, vcgSupported: true });
        this.ndb = new Ndb({ fetch, domain });
        this.rch = new Rch({ fetch, domain });
        this.rctLayouts = new RctLayouts({ fetch, domain });

        this.eventBus = new EventBus();

        this.eventBus.listen([MEETING.LEAVE], () => {
            this.resetMeetingData();
        });
        this.eventBus.listen([MEETING.INVALIDATE], (event, { reason }) =>
            this.emit(CB_EVENTS.RECOVERY, { type: RECOVERY.FULL, reason })
        );
        this.eventBus.listen([MEETING.INVALIDATE_ROOM], (event, { reason, roomId, roomName }) =>
            this.emit(CB_EVENTS.RECOVERY, { type: RECOVERY.SOFT, reason, roomId, roomName })
        );
        this.eventBus.listen([MEETING.RECOVERED], (event, data) =>
            this.emit(CB_EVENTS.RECOVERY_SUCCESS, data)
        );
        this.eventBus.listen([MEETING.FAILED], (event, errorResponse) => {
            if (errorResponse?.status && String(errorResponse.status).startsWith('4')) {
                this.emit(CB_EVENTS.RECOVERY_FAILURE, errorResponse);
            }
        });
        // Proxying VERSION_MISMATCH event from eventBus to vc-web
        this.eventBus.listen([CB_EVENTS.VERSION_MISMATCH], (event, data) =>
            this.emit(CB_EVENTS.VERSION_MISMATCH, data)
        );

        this._getPrefixedLogger = options.getPrefixedLogger || getDefaultLogger;

        this.promisifyRootSM = (bridgeParams, meeting, transport) =>
            new Promise((resolve, reject) => {
                this.interRootSM = rootEventsFabric({
                    rctApi: this.rct,
                    ndbApi: this.ndb,
                    bridge: bridgeParams,
                    meeting,
                    transport,
                    bridgeServiceFabric: bridgePromisifiedFabric,
                    meetingServiceFabric: meetingPromisifiedFabric,
                    eventBus: this.eventBus,
                    getPrefixedLogger: this._getPrefixedLogger,
                    onSuccess: resolve,
                    onError: reject,
                    onLeave: data => this.emit(CB_EVENTS.LEAVE, data),
                    onChange: (...data) => {
                        const [eventName, eventBody] = data;
                        // Hack for dials until PN doesn't send 'number' field in responses
                        // TODO remove this code after RCV-37218 will be done
                        if (
                            eventName === 'changed:myDials' &&
                            eventBody &&
                            this.currentDialNumber
                        ) {
                            eventBody[eventBody.length - 1].number = this.currentDialNumber;
                            this.currentDialNumber = null;
                            this.emit(eventName, eventBody);
                        } else this.emit(...data);
                    },
                    onPoorConnection: () => this.emit(CB_EVENTS.POOR_CONNECTION),
                    onPasswordChange: data => this.emit(CB_EVENTS.PASSWORD_CHANGE, data),
                    onJoinBeforeHostError: value => this.emit(CB_EVENTS.WAITING_FOR_HOST, value),
                    onOnlyCoworkersJoin: () => this.emit(CB_EVENTS.ONLY_COWORKERS_JOIN),
                    onOnlyAuthUserJoin: () => this.emit(CB_EVENTS.ONLY_AUTH_USER_JOIN),
                    onCapacityLimit: () => this.emit(CB_EVENTS.CAPACITY_LIMIT),
                    onSimultaneousLimit: data => this.emit(CB_EVENTS.SIMULTANEOUS_LIMIT, data),
                    onWaitingRoomAwait: conferenceActive =>
                        this.emit(CB_EVENTS.WAITING_ROOM_AWAIT, conferenceActive),
                    onWaitingRoomReject: () => this.emit(CB_EVENTS.WAITING_ROOM_REJECT),
                    onWaitingRoomApproved: () => this.emit(CB_EVENTS.WAITING_ROOM_APPROVED),
                    onLockedError: value => this.emit(CB_EVENTS.LOCKED_ERROR, value),
                    onBridgeConnected: bridgeData =>
                        this.emit(CB_EVENTS.BRIDGE_CONNECTED, bridgeData),
                    onMeetingServerError: data => this.emit(CB_EVENTS.MEETING_SERVER_ERROR, data),
                });

                this.interRootSM.send(MEETING.JOIN);
            });
    }

    /**
     * initialize chat Manager and Transport machines
     */
    initChatSMs({ meeting, localParticipant, localSession }) {
        // TODO: remove chatLocalStorageAvailable after hide/show chats feature is tested normally
        const chatLocalStorageAvailable = localStorage.getItem('chatAvailable') !== 'false';
        const chatAvailable = meeting?.features?.chat ?? chatLocalStorageAvailable;

        if (!chatAvailable) return;

        const onChange = (...data) => this.emit(...data);

        chatManagerEventsFabric({
            eventBus: this.eventBus,
            rchApi: this.rch,
            isPrivateChatsEnabled: this._options.isPrivateChatsEnabled,
            onChange,
            onChatMetaError: data => this.emit(CB_EVENTS.CHAT_META_ERROR, data),
            getPrefixedLogger: this._getPrefixedLogger,
        }).send(MEETING.CONNECTED, {
            bridgeId: meeting.bridgeId,
            meetingId: meeting.id,
            roomId: meeting.roomId,
            participantId: localParticipant.id,
            sessionId: localSession.id,
        });

        chatTransportEventsFabric({
            eventBus: this.eventBus,
            pubnub: this._options.pubnub,
            pubnubOrigin: this._options.pubnubOrigin,
            onChange,
            chatsListRecoveryTimeout: this._options.chatsListRecoveryTimeout,
            getPrefixedLogger: this._getPrefixedLogger,
        });
    }

    /**
     * initialize pong state machine
     */
    initPongSM() {
        if (!this._options.disablePongs) {
            pongEventsFabric({
                eventBus: this.eventBus,
                rctApi: this.rct,
                getPrefixedLogger: this._getPrefixedLogger,
            });
        }
    }

    /**
     * initialize sync audio state machine
     * @returns {Object} machine instance
     */
    createAudioSyncSM() {
        return syncEventsFabric({
            eventBus: this.eventBus,
            type: SYNC_TYPES.AUDIO,
            getPrefixedLogger: this._getPrefixedLogger,
            sync: ({ value, isPstn, ...rest }, signal) =>
                this.rct.updateMuteState(
                    {
                        ...rest,
                        session: {
                            ...(isPstn ? { serverMute: value } : { localMute: value }),
                        },
                    },
                    signal
                ),
        });
    }

    /**
     * initialize sync video state machine
     * @returns {Object} machine instance
     */
    createVideoSyncSM() {
        return syncEventsFabric({
            eventBus: this.eventBus,
            type: SYNC_TYPES.VIDEO,
            getPrefixedLogger: this._getPrefixedLogger,
            sync: ({ value, ...rest }, signal) =>
                this.rct.updateMuteState(
                    {
                        ...rest,
                        session: {
                            localMuteVideo: value,
                        },
                    },
                    signal
                ),
        });
    }

    /**
     * initialize sync join-audio state machine
     * @returns {Object} machine instance
     */
    createAudioJoinSyncSM() {
        return syncEventsFabric({
            eventBus: this.eventBus,
            type: SYNC_TYPES.AUDIO_JOIN,
            getPrefixedLogger: this._getPrefixedLogger,
            sync: ({ value, ...rest }, signal) =>
                this.rct.updateStream(
                    {
                        ...rest,
                        stream: {
                            audio: { isActiveIn: value, isActiveOut: value },
                        },
                    },
                    signal
                ),
        });
    }

    /**
     * initialize state machines
     */
    initSMs() {
        this.initPongSM();
        this.audioSyncSM = this.createAudioSyncSM();
        this.videoSyncSM = this.createVideoSyncSM();
        this.joinAudioSyncSM = this.createAudioJoinSyncSM();
    }

    terminateSMs() {
        this.audioSyncSM = null;
        this.videoSyncSM = null;
        this.joinAudioSyncSM = null;
        this.eventBus.send(MEETING.LEAVE);
    }

    removeAllListeners() {
        super.removeAllListeners();
    }

    resetMeetingData() {
        this.bridgeId = null;
        this.meetingId = null;
        this.participantId = null;
        this.participants = null;
        this.sessionId = null;
        this.targetedToMeDemands = [];
        this.localParticipant = {};
        this.currentDialNumber = null;
    }

    join({ bridgeParams, meetingParams, participantParams, sessionParams, streamParams }) {
        this.initSMs();

        const meeting = {
            ...meetingParams,
            participants: [
                {
                    displayName: participantParams.displayName,
                    sessions: [{ ...sessionParams }],
                    ...(streamParams ? { streams: [{ ...streamParams }] } : {}),
                },
            ],
        };
        const { pubnub, pubnubOrigin } = this._options;

        this.mainPNTransport = new TransportWrapper({ pubnub, pubnubOrigin });

        this.interLayouts = layoutEventsFabric({
            eventBus: this.eventBus,
            transport: this.mainPNTransport,
            rctLayoutsApi: this.rctLayouts,
            onChange: this.emit.bind(this),
            getPrefixedLogger: this._getPrefixedLogger,
        });

        // syncing local and remote values with SMs
        if (sessionParams.hasOwnProperty('localMute')) {
            this.audioSyncSM?.send(SYNC_TYPES.AUDIO, {
                local: sessionParams.localMute,
                remote: sessionParams.localMute,
            });
        }
        if (sessionParams.hasOwnProperty('localMuteVideo')) {
            this.videoSyncSM?.send(SYNC_TYPES.VIDEO, {
                local: sessionParams.localMuteVideo,
                remote: sessionParams.localMuteVideo,
            });
        }
        if (streamParams && streamParams.audio) {
            this.joinAudioSyncSM?.send(SYNC_TYPES.AUDIO_JOIN, {
                local: streamParams.audio.isActiveIn,
                remote: streamParams.audio.isActiveIn,
            });
        } else {
            this.joinAudioSyncSM?.send(SYNC_TYPES.AUDIO_JOIN, {
                local: null,
                remote: null,
            });
        }

        // init root sm
        return this.promisifyRootSM(bridgeParams, meeting, this.mainPNTransport)
            .then(data => {
                const {
                    localParticipant,
                    localSession,
                    meeting: localMeeting,
                    participants,
                    streams,
                    targetedToMeDemands,
                } = data;

                this.initChatSMs(data);

                this.bridgeId = localMeeting.bridgeId;
                this.meetingId = localMeeting.id;
                this.localParticipant = localParticipant;
                this.participantId = localParticipant.id;
                this.participants = participants;
                this.sessionId = !localSession.deleted ? localSession.id : null;
                this.streams = streams;
                this.targetedToMeDemands = targetedToMeDemands;

                this.on('changed:targetedToMeDemands', data => {
                    this.targetedToMeDemands = data;
                });
                this.on('changed:streams', data => {
                    this.streams = data;
                });
                this.on('changed:localParticipant', data => {
                    this.localParticipant = data;
                    this.participantId = data.id;
                });
                this.on('changed:localSession', ({ id, deleted }) => {
                    this.sessionId = !deleted ? id : null;
                });
                this.on('changed:participants', data => {
                    this.participants = data;
                });
                this.on('changed:meeting', ({ id }) => {
                    this.meetingId = id;
                });

                return data;
            })
            .catch(error => {
                this.terminateSMs();
                throw error;
            });
    }

    providePassword(password) {
        this.eventBus.send('PASSWORD', { password });
    }

    updateMuteStateBySessionId(sessionId, params) {
        const { bridgeId, meetingId, participantId } = this;

        return this.rct.updateMuteState({
            bridgeId,
            meetingId,
            participantId,
            sessionId,
            session: params,
        });
    }

    muteAudio() {
        const { pstnSessionId } = this.localParticipant;

        const unmuteAudioDemands = this.targetedToMeDemands?.filter(
            demand => demand?.type === demandType.UNMUTE_DEMAND
        );

        if (unmuteAudioDemands?.length > 0 && pstnSessionId) {
            this.cancelDemandRequest(demandType.UNMUTE_DEMAND);
        }

        this.audioSyncSM?.send(SYNC_TYPES.AUDIO, { local: true });
    }

    unmuteAudio() {
        const { allowUnmute, serverMute } = this.localParticipant;

        const unmuteAudioDemands = this.targetedToMeDemands?.filter(
            demand => demand?.type === demandType.UNMUTE_DEMAND
        );
        const unmuteAudioDemandsForMe = unmuteAudioDemands?.filter(demand => !demand.isForAll);
        const unmuteAudioDemandsForAll = unmuteAudioDemands?.filter(demand => demand.isForAll);

        if (unmuteAudioDemandsForMe?.length > 0) {
            this.cancelDemandRequest(demandType.UNMUTE_DEMAND);
        }
        if (unmuteAudioDemandsForAll?.length > 0) {
            this.answerAllDemand(demandType.UNMUTE_DEMAND, demandDecision.APPROVE_DEMAND);
        }
        if (allowUnmute && serverMute) {
            this.unmuteParticipant(this.participantId);
        }

        this.audioSyncSM?.send(SYNC_TYPES.AUDIO, { local: false });
    }

    muteVideo() {
        this.videoSyncSM?.send(SYNC_TYPES.VIDEO, { local: true });
    }

    unmuteVideo() {
        const { allowUnmuteVideo = false, serverMuteVideo = false } = this.localParticipant;

        const unmuteVideoDemands = this.targetedToMeDemands?.filter(
            demand => demand?.type === demandType.UNMUTE_VIDEO_DEMAND
        );
        const unmuteVideoDemandsForMe = unmuteVideoDemands?.filter(demand => !demand.isForAll);
        const unmuteVideoDemandsForAll = unmuteVideoDemands?.filter(demand => demand.isForAll);

        if (unmuteVideoDemandsForMe?.length > 0) {
            this.cancelDemandRequest(demandType.UNMUTE_VIDEO_DEMAND);
        }
        if (unmuteVideoDemandsForAll?.length > 0) {
            this.answerAllDemand(demandType.UNMUTE_VIDEO_DEMAND, demandDecision.APPROVE_DEMAND);
        }
        if (serverMuteVideo && allowUnmuteVideo) {
            this.unmuteParticipantVideo(this.participantId);
        }

        this.videoSyncSM?.send(SYNC_TYPES.VIDEO, { local: false });
    }

    leaveAudio() {
        this.joinAudioSyncSM?.send(SYNC_TYPES.AUDIO_JOIN, { local: false });
    }

    joinAudio() {
        this.joinAudioSyncSM?.send(SYNC_TYPES.AUDIO_JOIN, { local: true });
    }

    cancelDemandRequest(demandType) {
        return Promise.all(
            this.targetedToMeDemands
                ?.filter(demand => demand?.type === demandType && !demand?.isForAll)
                .map(demand =>
                    this.rct.cancelDemandRequest({
                        bridgeId: this.bridgeId,
                        meetingId: this.meetingId,
                        participantId: this.participantId,
                        demandId: demand.id,
                    })
                )
        );
    }

    leave() {
        const { bridgeId, meetingId, participantId, sessionId, localParticipant } = this;

        const deletionPromises = [];

        if (bridgeId && meetingId && participantId && sessionId) {
            deletionPromises.push(
                this.rct.deleteSession({
                    bridgeId,
                    meetingId,
                    participantId,
                    sessionId,
                })
            );
        }

        if (localParticipant?.pstnSessionId) {
            deletionPromises.push(
                this.rct.deleteSession({
                    bridgeId,
                    meetingId,
                    participantId,
                    sessionId: localParticipant.pstnSessionId,
                })
            );
        }

        this.terminateSMs();
        return Promise.all(deletionPromises);
    }

    leaveViaBeacon() {
        const domain = this._options.origin;
        const path =
            `/rcvideo/v1/bridges/${this.bridgeId}` +
            `/meetings/${this.meetingId}` +
            `/participants/${this.participantId}` +
            `/sessions/${this.sessionId}?deleteReason=12`;
        const url = domain + path;
        this._sendBeacon(url);
    }

    createChat(params) {
        return this.rch.addChat({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            chat: params ? params : { type: 'public' },
        });
    }

    sendMessage(chatId, message) {
        if (this._options.e2eeEnabled) {
            const mlsSdkClient = this.getMlsSdkClient();
            message = mlsSdkClient.encryptMessage(message);
        }

        return this.rch.addMessage({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            chatId,
            message: { msg: message },
            uniqueId: generateId(16),
        });
    }

    createBridge(params) {
        return this.ndb.createBridge({
            bridge: params,
        });
    }

    updateBridge(params) {
        return this.ndb.updateBridge({
            bridgeId: params.id,
            bridge: params,
        });
    }

    getBridgeInfo(params) {
        return this.ndb.getBridgeByParams(params);
    }

    getBridge(params) {
        return this.ndb.getBridge(params);
    }

    getPersonalMeeting() {
        return this.ndb.getBridgeByParams({
            default: true,
        });
    }

    createChannel(params) {
        return this.rct.createChannel({
            channel: { ...params, sessionId: this.sessionId },
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
        });
    }

    updateChannel(channelId, params) {
        return this.rct.updateChannel({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
            channelId,
            channel: params,
        });
    }

    deleteChannel({ channelId }) {
        return this.rct.deleteChannel({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
            channelId,
        });
    }

    startRecording() {
        return this.rct.createRecording({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
        });
    }

    pauseRecording(recordingId) {
        return this.rct.updateRecording({
            recording: { paused: true },
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            recordingId,
        });
    }

    unpauseRecording(recordingId) {
        return this.rct.updateRecording({
            recording: { paused: false },
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            recordingId,
        });
    }

    callMe(number) {
        this.currentDialNumber = number;
        return this.rct.createDial({
            dial: {
                number,
                calleeId: this.participantId,
                shouldPlayWelcomePrompt: true,
            },
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
        });
    }

    callOut(number) {
        this.currentDialNumber = number;
        return this.rct.createDial({
            dial: { number, shouldPlayWelcomePrompt: true },
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
        });
    }

    getDial(dialId) {
        return this.rct.getDial({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
            dialId,
        });
    }

    deleteDial(dialId) {
        return this.rct.deleteDial({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
            dialId,
        });
    }

    createStream(params) {
        return this.rct.createStream({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId: this.participantId,
            stream: { ...params, sessionId: this.sessionId },
        });
    }

    deleteStream(streamId, participantId = this.participantId) {
        return this.rct.deleteStream({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            streamId,
        });
    }

    kickParticipant(participantId) {
        return this.rct.deleteParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
        });
    }

    deleteParticipantStreamsByType(participantId, type) {
        return Promise.all(
            Object.keys(this.streams)
                .filter(
                    streamId =>
                        this.streams[streamId].participantId === participantId &&
                        this.streams[streamId].type === type &&
                        !this.streams[streamId].deleted
                )
                .map(streamId => this.deleteStream(streamId, participantId))
        );
    }

    provideModeratorRole(participantId) {
        return this.rct
            .updateParticipant({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
                participantId,
                participant: { moderator: true },
            })
            .then(() => {
                this.participants[participantId].moderator = true;
                return this.participants[participantId];
            });
    }

    revokeModeratorRole(participantId) {
        return this.rct.updateParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            participant: { moderator: false },
        });
    }

    allowScreenSharing() {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { allowScreenSharing: true },
        });
    }

    disallowScreenSharing() {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { allowScreenSharing: false },
        });
    }

    unlockCall() {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { allowJoin: true },
        });
    }

    lockCall() {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { allowJoin: false },
        });
    }

    endCall() {
        return this.rct
            .deleteMeeting({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
            })
            .then(res => {
                this.terminateSMs();
                return res;
            });
    }

    turnOnParticipantRingStatus(participantId) {
        return this.rct.updateParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            participant: { ringing: true },
        });
    }

    turnOffParticipantRingStatus(participantId) {
        return this.rct.updateParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            participant: { ringing: false },
        });
    }

    muteCall(allowUnmute = true) {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { mute: true, allowUnmute },
        });
    }

    unmuteCall() {
        return Promise.all([
            this.rct.updateMeeting({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
                call: { mute: false, allowUnmute: true },
            }),
            this.rct.createDemandRequest({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
                requesterId: this.participantId,
                demand: {
                    bridgeId: this.bridgeId,
                    callId: this.meetingId,
                    participantId: this.participantId,
                    sessionId: this.sessionId,
                    status: 'new',
                    info: {
                        participantId: this.participantId,
                        sessionId: this.sessionId,
                    },
                    type: 'unmute',
                    resource: {},
                },
            }),
        ]);
    }

    muteVideoCall(allowUnmuteVideo = true) {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { muteVideo: true, allowUnmuteVideo },
        });
    }

    unmuteVideoCall() {
        return Promise.all([
            this.rct.updateMeeting({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
                call: { muteVideo: false, allowUnmuteVideo: true },
            }),
            this.rct.createDemandRequest({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
                requesterId: this.participantId,
                demand: {
                    bridgeId: this.bridgeId,
                    callId: this.meetingId,
                    participantId: this.participantId,
                    sessionId: this.sessionId,
                    status: 'new',
                    info: {
                        participantId: this.participantId,
                        sessionId: this.sessionId,
                    },
                    type: 'unmute_video',
                    resource: {},
                },
            }),
        ]);
    }

    muteParticipant(participantId) {
        return this.rct.updateParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            participant: { serverMute: true },
        });
    }

    unmuteParticipant(participantId) {
        const { moderator, temporaryModerator } = this.localParticipant;
        return Promise.all([
            this.rct.updateParticipant({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
                participantId,
                participant: {
                    serverMute: false,
                    ...(moderator || temporaryModerator ? { allowUnmute: true } : {}),
                },
            }),
            ...(participantId !== this.participantId
                ? [
                      this.rct.createDemandRequest({
                          bridgeId: this.bridgeId,
                          meetingId: this.meetingId,
                          requesterId: this.participantId,
                          demand: {
                              bridgeId: this.bridgeId,
                              callId: this.meetingId,
                              participantId: this.participantId,
                              sessionId: this.sessionId,
                              status: 'new',
                              info: {
                                  participantId: this.participantId,
                                  sessionId: this.sessionId,
                              },
                              type: 'unmute',
                              resource: { participantId },
                          },
                      }),
                  ]
                : []),
        ]);
    }

    muteParticipantVideo(participantId) {
        return this.rct.updateParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            participant: { serverMuteVideo: true },
        });
    }

    unmuteParticipantVideo(participantId) {
        const { moderator, temporaryModerator } = this.localParticipant;
        return Promise.all([
            this.rct.updateParticipant({
                bridgeId: this.bridgeId,
                meetingId: this.meetingId,
                participantId,
                participant: {
                    serverMuteVideo: false,
                    ...(moderator || temporaryModerator ? { allowUnmuteVideo: true } : {}),
                },
            }),
            ...(participantId !== this.participantId
                ? [
                      this.rct.createDemandRequest({
                          bridgeId: this.bridgeId,
                          meetingId: this.meetingId,
                          requesterId: this.participantId,
                          demand: {
                              bridgeId: this.bridgeId,
                              callId: this.meetingId,
                              participantId: this.participantId,
                              sessionId: this.sessionId,
                              status: 'new',
                              info: {
                                  participantId: this.participantId,
                                  sessionId: this.sessionId,
                              },
                              type: 'unmute_video',
                              resource: { participantId },
                          },
                      }),
                  ]
                : []),
        ]);
    }

    answerAllDemand(demandType, answer) {
        return Promise.all(
            this.targetedToMeDemands
                ?.filter(demand => demand.type === demandType && demand.isForAll)
                .map(demand =>
                    this.rct.updateDemandRequest({
                        bridgeId: this.bridgeId,
                        meetingId: this.meetingId,
                        participantId: demand.participantId,
                        demandId: demand.id,
                        demand: {
                            decisions: Object.values(demand.decisions || {}).concat([
                                { id: this.participantId, status: answer },
                            ]),
                        },
                    })
                )
        );
    }

    toggleParticipantRingStatus(participantId) {
        return this.rct.updateParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            participant: { ringing: !this.participants[participantId].ringing },
        });
    }

    allowChats() {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { allowChats: true },
        });
    }

    disallowChats() {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { allowChats: false },
        });
    }

    updateWaitingRoomStatus(participantId, waitingRoomStatus) {
        return this.rct.updateParticipant({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            participantId,
            participant: { waitingRoomStatus },
        });
    }

    allowEnterAwaiting() {
        return this.rct.updateMeeting({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            call: { allowEnterAwaiting: true },
        });
    }

    getSessionId() {
        return this.sessionId;
    }

    getParticipantId() {
        return this.participantId;
    }

    getMeetingId() {
        return this.meetingId;
    }

    getBridgeId() {
        return this.bridgeId;
    }

    deleteSession(sessionId, { bridgeId, meetingId, participantId } = {}) {
        return this.rct.deleteSession({
            bridgeId: this.bridgeId ?? bridgeId,
            meetingId: this.meetingId ?? meetingId,
            participantId: this.participantId ?? participantId,
            sessionId: sessionId,
        });
    }

    forceCallReload() {
        this.eventBus.send(TRANSPORT.RECONNECTED);
    }

    fullCallReload(reason) {
        this.eventBus.send(MEETING.INVALIDATE, { reason });
    }

    gettingLayouts(defaultRooms) {
        this.eventBus.send(LAYOUTS.GETTING_LAYOUTS, { defaultRooms });
    }

    /***************************** BREAKOUT ROOMS *****************************/

    applyLayout(layoutId) {
        return this.rctLayouts.applyLayout({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            layoutId,
        });
    }

    applyLayoutToParticipants(layoutId, participantIds) {
        return this.rctLayouts.applyLayoutToParticipants({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            layoutId,
            payload: { participants: participantIds },
        });
    }

    updateUserLayout(layout) {
        return this.rctLayouts.updateLayoutById({
            bridgeId: this.bridgeId,
            meetingId: this.meetingId,
            layoutId: LAYOUT_IDS.USER,
            layout,
        });
    }

    openBreakoutRooms() {
        return this.applyLayout(LAYOUT_IDS.USER);
    }

    applyUserLayoutToParticipants(participantIds) {
        return this.applyLayoutToParticipants(LAYOUT_IDS.USER, participantIds);
    }

    leaveBreakoutRoom() {
        return this.applyLayoutToParticipants(LAYOUT_IDS.MAIN, [this.participantId]);
    }

    closeBreakoutRooms() {
        return this.applyLayout(LAYOUT_IDS.MAIN);
    }

    getMlsSdkClient() {
        return this._options.getMlsSdkClient();
    }
}

module.exports = Conference;
