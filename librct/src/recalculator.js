const meetingFields = require('./models/meeting');
const participantFields = require('./models/participant');
const localParticipantFields = require('./models/local-participant');
const streamFields = require('./models/stream');
const localSessionFields = require('./models/local-session');
const demandFields = require('./models/demand');
const recordingsFields = require('./models/recordings');
const notificationsFields = require('./models/notifications');
const dialFields = require('./models/dials');
const channelFields = require('./models/channels');
const meetingParticipantFields = require('./models/meeting-participant');
const notifyChannelFields = require('./models/notify-channel');
const { classify } = require('./utils');

const onlyExisting = val => !val.deleted;

/**
 * filter and calculate fields using src obj
 * @param {Object} deps dependencies that will be used for model calculation
 * @param {Object} fields model description
 * @param {string=} mainKey key of main dependency
 */
const mapper = (deps, fields, mainKey) => {
    const result = {};
    const mainDep = mainKey ? deps[mainKey] : deps;

    Object.keys(fields).forEach(key => {
        if (typeof fields[key] === 'function') {
            result[key] = fields[key](deps);
            // eslint-disable-next-line no-undefined
        } else if (mainDep[key] !== undefined) {
            result[key] = mainDep[key];
        }
    });

    return result;
};

/**
 * add second classify level to the object
 * @param {Array<Object>} arr - arr of objects to classify
 * @param {string} idKey1 - key for 1st level
 * @param {string} idKey2 - key for 2nd level
 */
const classifyDouble = (arr, idKey1, idKey2) =>
    arr.reduce((acc, el) => {
        if (!acc[el[idKey1]]) {
            acc[el[idKey1]] = {
                [el[idKey2]]: el,
            };
        } else {
            acc[el[idKey1]][el[idKey2]] = el;
        }

        return acc;
    }, {});

/**
 * @callback RecalculatorFn
 * @param {Object} deps - dependencies that will be available during calculation of model
 * @param {WeakMap} cache - link to WeakMap cache object
 * @param {Object} recalculatedEntities - link recalculated entities
 */

/**
 * Creates recalculator for specific entity. Uses cache and prevents additional recalculations.
 * @param {string} entityKey - key of the entity to recalculate
 * @param {Function} calculate - function describing the calculation process
 * @returns {RecalculatorFn} function to recalculate the entity
 */
const getCachingRecalculator = (entityKey, calculate) => (deps, cache, recalculatedEntities) => {
    const entity = deps[entityKey];

    if (!entity || cache.has(entity)) return;

    const result = calculate(entity, deps, cache);

    cache.set(entity, true);
    recalculatedEntities[entityKey] = result;
};

/** @type {RecalculatorFn} */
const recalculateMeeting = getCachingRecalculator('meeting', meeting =>
    mapper(meeting, meetingFields)
);

/** @type {RecalculatorFn} */
const recalculateParticipants = (deps, cache, recalculatedEntities) => {
    const { participants, sessions, streams, meeting } = deps;

    if (!participants) return;
    if (
        cache.has(participants) &&
        (!streams || cache.has(streams)) &&
        (!sessions || cache.has(sessions))
    ) {
        return;
    }

    const sessionsPerParticipants = sessions
        ? classifyDouble(Object.values(sessions), 'participantId', 'id')
        : {};

    const streamsPerParticipants = streams
        ? classifyDouble(Object.values(streams), 'participantId', 'id')
        : {};

    const result = {};

    Object.keys(participants).forEach(key => {
        result[key] = mapper(
            {
                sessions: sessionsPerParticipants[key] || [],
                streams: streamsPerParticipants[key] || [],
                participant: participants[key],
                meeting,
            },
            participantFields,
            'participant'
        );
    });

    cache.set(participants, true);
    recalculatedEntities.participants = result;
};

/** @type {RecalculatorFn} */
const recalculateStreamsRefs = (deps, cache, recalculatedEntities) => {
    const { localParticipant, streams, localStreams } = deps;

    if (
        (!localParticipant?.id || cache.has(localParticipant)) &&
        (!streams || cache.has(streams)) &&
        (!localStreams || cache.has(localStreams))
    ) {
        return;
    }

    const liveLocalStreams = Object.values(localStreams || {}).filter(s => !s.deleted);

    const liveStreams = Object.values(streams || {}).filter(s => !s.deleted);

    const streamsPerParticipants =
        liveStreams.length > 0 ? classifyDouble(liveStreams, 'participantId', 'id') : {};

    const result = {};

    Object.keys(streamsPerParticipants).forEach(key => {
        result[key] = Object.values(streamsPerParticipants[key]).map(s => s.id);
    });

    if (liveLocalStreams.length > 0) {
        result[localParticipant.id] = [
            ...liveLocalStreams.map(s => s.id),
            ...(Array.isArray(result[localParticipant.id]) ? result[localParticipant.id] : []),
        ];
    }

    recalculatedEntities.streamsRefs = result;
};

/** @type {RecalculatorFn} */
const recalculateLocalParticipant = (deps, cache, recalculatedEntities) => {
    const {
        localParticipant,
        localSession,
        localStreams,
        sessions,
        streams,
        localSessionHistory,
    } = deps;

    if (!localParticipant?.id) return;

    if (
        cache.has(localParticipant) &&
        (!localSession || cache.has(localSession)) &&
        (!localStreams || cache.has(localStreams)) &&
        (!sessions || cache.has(sessions)) &&
        (!streams || cache.has(streams))
    ) {
        return;
    }

    const restParticipantSessions = Object.values(sessions || {}).filter(
        session => session.participantId === localParticipant.id && !localSessionHistory[session.id]
    );
    const restParticipantStreams = Object.values(streams || {}).filter(
        stream =>
            stream.participantId === localParticipant.id && !localSessionHistory[stream.sessionId]
    );

    const result = mapper(
        {
            participant: localParticipant,
            localSession,
            localStreams,
            restParticipantSessions: classify('id', restParticipantSessions),
            restParticipantStreams: classify('id', restParticipantStreams),
        },
        localParticipantFields,
        'participant'
    );

    cache.set(localParticipant, true);
    recalculatedEntities.localParticipant = result;
};

/** @type {RecalculatorFn} */
const recalculateStreams = (deps, cache, recalculatedEntities) => {
    const { streams, sessions } = deps;

    if (!streams) return;

    if (cache.has(streams) && (!sessions || cache.has(sessions))) return;

    const result = {};
    Object.keys(streams).forEach(key => {
        const streamSessionId = Object.keys(sessions || {}).find(
            sessionId => sessionId === streams[key].sessionId
        );
        result[key] = mapper(
            {
                stream: streams[key],
                session: (sessions || {})[streamSessionId],
            },
            streamFields,
            'stream'
        );
    });

    cache.set(streams, true);
    recalculatedEntities.streams = result;
};

/** @type {RecalculatorFn} */
const recalculateLocalStreams = getCachingRecalculator('localStreams', localStreams =>
    Object.values(localStreams).map(stream => mapper(stream, streamFields))
);

/** @type {RecalculatorFn} */
const recalculateParticipantBySessionMap = (deps, cache, recalculatedEntities) => {
    const { sessions, localSession } = deps;

    if (!localSession?.id && !localSession?.participantId) return;

    if ((!sessions || cache.has(sessions)) && (!localSession || cache.has(localSession))) return;

    const result = {};

    result[localSession.id] = localSession.participantId;

    Object.keys(sessions || {}).forEach(key => {
        result[key] = sessions[key].participantId;
    });

    recalculatedEntities.participantBySessionMap = result;
};

/** @type {RecalculatorFn} */
const recalculateLocalSession = getCachingRecalculator('localSession', localSession =>
    mapper(localSession, localSessionFields)
);

/** @type {RecalculatorFn} */
const recalculateMyDemands = (deps, cache, recalculatedEntities) => {
    const { myDemands, localParticipant } = deps;

    if (!localParticipant?.id || !myDemands || cache.has(myDemands)) return;

    const result = Object.values(myDemands)
        .filter(onlyExisting)
        .map(demand =>
            mapper(
                {
                    demand,
                    localParticipantId: localParticipant.id,
                },
                demandFields,
                'demand'
            )
        );

    cache.set(myDemands, true);
    recalculatedEntities.myDemands = result;
};

/** @type {RecalculatorFn} */
const recalculateTargetedToMeDemands = (deps, cache, recalculatedEntities) => {
    const { targetedToMeDemands, localParticipant } = deps;

    if (!localParticipant?.id || !targetedToMeDemands || cache.has(targetedToMeDemands)) return;

    const result = Object.values(targetedToMeDemands)
        .filter(onlyExisting)
        .map(demand =>
            mapper(
                {
                    demand,
                    localParticipantId: localParticipant.id,
                },
                demandFields,
                'demand'
            )
        );

    Object.keys(result).forEach(id => {
        if (result[id].myDecision) {
            delete result[id];
        }
    });

    cache.set(targetedToMeDemands, true);
    recalculatedEntities.targetedToMeDemands = result;
};

/** @type {RecalculatorFn} */
const recalculateRecordings = getCachingRecalculator('recordings', recordings =>
    Object.values(recordings).map(recording => mapper(recording, recordingsFields))
);

/** @type {RecalculatorFn} */
const recalculateNotifications = getCachingRecalculator('notifications', notifications =>
    Object.values(notifications).map(notification => mapper(notification, notificationsFields))
);

/** @type {RecalculatorFn} */
const recalculateMyDials = getCachingRecalculator('myDials', myDials =>
    Object.values(myDials).map(myDial => mapper(myDial, dialFields))
);

/** @type {RecalculatorFn} */
const recalculateChannels = getCachingRecalculator('channels', channels =>
    Object.values(channels)
        .filter(onlyExisting)
        .map(channel => mapper(channel, channelFields))
);

/** @type {RecalculatorFn} */
const recalculateMeetingParticipants = getCachingRecalculator(
    'meetingParticipants',
    meetingParticipants =>
        Object.fromEntries(
            Object.keys(meetingParticipants).map(participantId => [
                participantId,
                mapper({
                    meetingParticipant: meetingParticipants[participantId],
                    participantId,
                }, meetingParticipantFields, 'meetingParticipant'),
            ])
        )
);

// TODO: revert this commit after RCV-46294 resolving
/** @type {RecalculatorFn} */
const recalculateNotifyChannels = (deps, cache, recalculatedEntities) => {
    const { notifyChannels, meeting } = deps;

    if (
        (!notifyChannels || cache.has(notifyChannels))
        && (!meeting || cache.has(meeting))
    ) return;

    const result = Object.fromEntries(
        Object.keys(notifyChannels).map(channel => [
            channel,
            mapper({
                notifyChannel: notifyChannels[channel],
                channelId: channel,
                meeting,
            }, notifyChannelFields, 'notifyChannel'),
        ])
    );

    cache.set(notifyChannels, true);
    recalculatedEntities.notifyChannels = result;
};

/** @type {RecalculatorFn} */
const recalculateLocalSessionHistory = getCachingRecalculator(
    'localSessionHistory',
    (localSessionHistory, deps) =>
        Object.entries(localSessionHistory)
            .map(([id, saved]) => {
                const session =
                    (deps.localSession?.id === id ? deps.localSession : deps.sessions?.[id]);
                return { id, saved, ...session };
            })
            .sort((a, b) => new Date(a.saved) - new Date(b.saved))
);

const saveSessionsToCache = ({ sessions }, cache) => {
    // we save sessions to cache to allow recalculation precheck
    if (!sessions) return;

    cache.set(sessions, null);
};

/**
 * recalculate all models
 * @param {Object} deps - dependencies that will be available during calculation of model
 * @param {WeakMap} cache - link to WeakMap cache object
 */
const recalculate = (deps, cache) => {
    const recalculatedEntities = {};

    // Please note: order of method's invocation is important!
    // It depends of cache checking in each recalculation.
    recalculateMeeting(deps, cache, recalculatedEntities);
    recalculateParticipants(deps, cache, recalculatedEntities);
    recalculateStreamsRefs(deps, cache, recalculatedEntities);
    recalculateLocalParticipant(deps, cache, recalculatedEntities);
    recalculateStreams(deps, cache, recalculatedEntities);
    recalculateLocalStreams(deps, cache, recalculatedEntities);
    recalculateParticipantBySessionMap(deps, cache, recalculatedEntities);
    recalculateLocalSession(deps, cache, recalculatedEntities);
    recalculateMyDemands(deps, cache, recalculatedEntities);
    recalculateTargetedToMeDemands(deps, cache, recalculatedEntities);
    recalculateRecordings(deps, cache, recalculatedEntities);
    recalculateNotifications(deps, cache, recalculatedEntities);
    recalculateMyDials(deps, cache, recalculatedEntities);
    recalculateChannels(deps, cache, recalculatedEntities);
    recalculateMeetingParticipants(deps, cache, recalculatedEntities);
    recalculateNotifyChannels(deps, cache, recalculatedEntities);
    recalculateLocalSessionHistory(deps, cache, recalculatedEntities);
    // Please note: saving sessions to cache should be lower than the recalculations that use it
    saveSessionsToCache(deps, cache);

    return recalculatedEntities;
};

/**
 * 1. Takes accumulated RCT call data.
 * 2. Recalculates each entity based on cache, its actual data and model.
 *    If the entity isn't modified (patched), recalculator won't recalculate it.
 * 3. Accumulates all recalculated entities.
 */
class Recalculator {
    constructor(cache = new WeakMap()) {
        this._cache = cache;
        this._allRecalculatedEntities = {};
    }

    get result() {
        return this._allRecalculatedEntities;
    }

    /** @param {import('./patcher').RCTCallAcc} call */
    recalculate(call) {
        const justRecalculatedEntities = recalculate(call, this._cache);
        this._allRecalculatedEntities = {
            ...this._allRecalculatedEntities,
            ...justRecalculatedEntities,
        };
        return this._allRecalculatedEntities;
    }
}

/**
 * recalculator fabric
 * @returns {Recalculator} recalculator object
 */
const createRecalculator = () => new Recalculator();

module.exports = { createRecalculator };
