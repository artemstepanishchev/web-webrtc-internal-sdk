const History = require('./history');
const Calendars = require('./calendars');
const UserSettings = require('./settings');
const UserPrivileges = require('./privileges');
const PlatformUserSearch = require('./platformUserSearch');
const ConferencingSettings = require('./conferencingSettings');
const Countries = require('./countries');
const DelegatesManager = require('./delegates-manager');
const PhoneNumbers = require('./phone-numbers');
const Conference = require('./conference');
const MeetingNotes = require('./meeting-notes');
const machineConstants = require('./machines/constants');

/**
 * Create LibRCT client
 * @param options
 * @param options.fetch         - function to make requests. Default:
 *                                <code>fetch</code> from
 *                                [isomorphic-fetch](https://github.com/matthew-andrews/isomorphic-fetch)
 * @param options.baseUrl       - base url for all requests. Default: <code>/flock/v1</code>
 * @param options.subscribeKey  - subscribe key for PubNub
 * @param option.sendBeacon     - function to send sync request
 * @constructor
 */
function LibRCT(options = {}) {
    const defaultOpts = {
        apiVersion: 'v1',
        maintenance: false, // maintenance mode
        maintenancePath: 'maintenance',
        chatsListRecoveryTimeout: 30000,
        chatMessagesRecoveryTimeout: 3 * 60 * 1000,
        isPrivateChatsEnabled: false,
        disablePongs: false,
    };

    this._options = { ...defaultOpts, ...options };
    this._history = new History(this._options);
    this._calendars = new Calendars(this._options);
    this._settings = new UserSettings(this._options);
    this._privileges = new UserPrivileges(this._options);
    this._platformUserSearch = new PlatformUserSearch(this._options);
    this._conferencingSettings = new ConferencingSettings(this._options);
    this._countries = new Countries(this._options);
    this._phoneNumbers = new PhoneNumbers(this._options);
    this._delegatesManager = new DelegatesManager(this._options);
    this._conference = new Conference(this._options);
    this._meetingNotes = new MeetingNotes(this._options);

    window[Symbol.for('rwcRctConf')] = this._conference;
}

LibRCT.prototype.history = function () {
    return this._history;
};

LibRCT.prototype.calendars = function () {
    return this._calendars;
};

LibRCT.prototype.settings = function () {
    return this._settings;
};

LibRCT.prototype.privileges = function () {
    return this._privileges;
};

LibRCT.prototype.setMaintenanceMode = function (value) {
    this._options.maintenance = value;
};

LibRCT.prototype.setE2eeEnabled = function (value) {
    this._options.e2eeEnabled = value;
};

LibRCT.prototype.platformUserSearch = function () {
    return this._platformUserSearch.search(...arguments);
};

LibRCT.prototype.getCountryPhoneNumbers = function () {
    return this._conferencingSettings.getCountryPhoneNumbers(...arguments);
};

LibRCT.prototype.getCountriesList = function () {
    return this._countries.getList(...arguments);
};

LibRCT.prototype.delegatesManager = function () {
    return this._delegatesManager;
};

LibRCT.prototype.phoneNumbers = function () {
    return this._phoneNumbers;
};

LibRCT.prototype.conference = function () {
    return this._conference;
};

LibRCT.prototype.meetingNotes = function () {
    return this._meetingNotes;
};

LibRCT.constants = {
    machines: machineConstants,
};

module.exports = LibRCT;
